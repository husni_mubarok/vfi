@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    You are logged in!
                </div>
                <table class="table">
                <tr>
                    <th>A</th>
                    <th>B</th>
                </tr>
                <tr>
                    <td>C</td>
                    <td>D</td>
                </tr>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
