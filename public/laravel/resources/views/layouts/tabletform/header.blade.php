<style type="text/css">
    .background_ims {
        background: #eaeaea;
        height: 66px;
        width: 100%;
        padding: 5px 0px;
    }
    .marquee {
        background: #000;
        height: 25px;
        width: 100%;
        padding: 5px 0px;
        color: #000000;
        border-top: 1px solid gray;
        color: #fff;
    }
    .header_menu {
        background: #eaeaea;
        height: 30px;
        width: 100%;
        padding: 5px 10px;
        color: #000000;
        border-bottom: 1px solid gray;
    }
    .header_menu a{color:black;
        font-family: verdana;font-weight: bold;
        margin:0 5px;
    }

    /*MOBILE HIDE*/
    @media (max-width: 767px) {    
        .mobile-hide{
            display: none !important;
        }
    }
</style>
<div class="background_ims mobile-hide">
    <a href="http://36.68.225.13:85/ims99dummyvfi">
        <img src="{{Config::get('constants.path.uploads')}}/logo/logo_vfi.png" alt="" height="55px">
        <img src="{{Config::get('constants.path.uploads')}}/logo/logo_ims.png" alt="" height="25px" style="margin-bottom: -30px">
    </a>
    <div style="float:right;text-align:right;margin:10px">
        <span>
            Username: {{@Auth::user()->username}}
            <br>
            {{-- Division: admin --}}
        </span>
    </div>
</div>
<div class="marquee mobile-hide">
    <marquee id="running_text"></marquee>
</div>
