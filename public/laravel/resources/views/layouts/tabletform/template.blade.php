<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>VFI-Fish-System</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- CSRF Token -->
  <meta name="csrf_token" content="{{ csrf_token() }}" >
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{Config::get('constants.path.bootstrap')}}/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{Config::get('constants.path.css')}}/AdminLTETabletForm.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
  folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{Config::get('constants.path.css')}}/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <!-- <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/morris/morris.css"> -->
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- dataTables -->
  <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.css" />
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/daterangepicker/daterangepicker.css">
  <!-- Datetime Picker -->
  <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/datetimepicker/bootstrap-datetimepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- ANIMATION BUTTON CSS -->
  <link rel="stylesheet" href="{{url('/')}}/assets/tablet/css/animation_button.css" />
  <link rel="stylesheet" href="{{Config::get('constants.path.css')}}/animation_button.css">
  <!-- VFI CSS -->
  <link rel="stylesheet" href="{{url('/')}}/assets/touch/css/vfi.css" />

  <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.6/sweetalert2.common.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.6/sweetalert2.common.min.js"></script> 
  <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.6/sweetalert2.common.min.js"></script>  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.6/sweetalert2.common.min.js.map"></script>  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.6/sweetalert2.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.6/sweetalert2.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.6/sweetalert2.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.6/sweetalert2.min.js"></script>   


  <style>
    td.details-control {
      background: url("{{Config::get('constants.path.img')}}/details_open.png") no-repeat center center;
      cursor: pointer;
    }

    tr.details td.details-control {
      background: url("{{Config::get('constants.path.img')}}/details_close.png") no-repeat center center;
    }
    table.dataTable tbody th,
    table.dataTable tbody td {
      white-space: nowrap;
    }

 
/* Center the loader */
  #loader {
    position: absolute;
    left: 50%;
    top: 50%;
    z-index: 1;
    width: 150px;
    height: 150px;
    margin: -75px 0 0 -75px;
    border: 16px solid #f3f3f3;
    border-radius: 50%;
    border-top: 16px solid #3498db;
    width: 120px;
    height: 120px;
    -webkit-animation: spin 2s linear infinite;
    animation: spin 2s linear infinite;
  }

  @-webkit-keyframes spin {
    0% { -webkit-transform: rotate(0deg); }
    100% { -webkit-transform: rotate(360deg); }
  }

  @keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
  }

  /* Add animation to "page content" */
  .animate-bottom {
    position: relative;
    -webkit-animation-name: animatebottom;
    -webkit-animation-duration: 1s;
    animation-name: animatebottom;
    animation-duration: 1s
  }

  @-webkit-keyframes animatebottom {
    from { bottom:-100px; opacity:0 } 
    to { bottom:0px; opacity:1 }
  }

  @keyframes animatebottom { 
    from{ bottom:-100px; opacity:0 } 
    to{ bottom:0; opacity:1 }
  }

  #myDiv {
    display: none;
    text-align: center;
  }

  
</style> 

<style type="text/css"> 
.differentTable, .differentTable td{
   border-color: black;
}
</style>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-black-light sidebar-mini">


  <!-- jQuery 2.2.3 -->
  <script src="{{Config::get('constants.path.plugin')}}/jQuery/jquery-2.2.3.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button);
    $(document).ready(function() {
      $.ajax({
        url : 'http://36.68.225.13:85/ims99dummyvfi/general/running_text_api.php',
        data : null, 
        type : 'GET',
        headers : {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
        success : function(data, textStatus, jqXHR){
          $("#running_text").text(data);
        },
        error : function()
        {
          $("#running_text").text('No Message');
        },
      })
    });
  </script>
  @yield('modal')

  @yield('scripts')
  <!-- Bootstrap 3.3.6 -->
  <script src="{{Config::get('constants.path.bootstrap')}}/js/bootstrap.min.js"></script>
  <!-- Laravel-CKEditor -->
  <script src="{{Config::get('constants.path.plugin')}}/ckeditor/ckeditor.js"></script>
  <script src="{{Config::get('constants.path.plugin')}}/ckeditor/adapters/jquery.js"></script>
  <!-- Morris.js charts -->
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script> -->
  <!-- <script src="{{Config::get('constants.path.plugin')}}/morris/morris.min.js"></script> -->
  <!-- Sparkline -->
  <script src="{{Config::get('constants.path.plugin')}}/sparkline/jquery.sparkline.min.js"></script>
  <!-- jvectormap -->
  <script src="{{Config::get('constants.path.plugin')}}/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
  <script src="{{Config::get('constants.path.plugin')}}/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
  <!-- jQuery Knob Chart -->
  <script src="{{Config::get('constants.path.plugin')}}/knob/jquery.knob.js"></script>
  <!-- daterangepicker -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
  <script src="{{Config::get('constants.path.plugin')}}/daterangepicker/daterangepicker.js"></script>
  <!-- datepicker -->
  <script src="{{Config::get('constants.path.plugin')}}/datepicker/bootstrap-datepicker.js"></script>
  <!-- datetimepicker -->
  <script src="{{Config::get('constants.path.plugin')}}/datetimepicker/bootstrap-datetimepicker.min.js"></script>
  <!-- Bootstrap WYSIHTML5 -->
  <script src="{{Config::get('constants.path.plugin')}}/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
  <!-- Slimscroll -->
  <script src="{{Config::get('constants.path.plugin')}}/slimScroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script src="{{Config::get('constants.path.plugin')}}/fastclick/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="{{Config::get('constants.path.js')}}/app.min.js"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <!-- <script src="{{Config::get('constants.path.js')}}/pages/dashboard.js"></script> -->
  <!-- AdminLTE for demo purposes -->
  <script src="{{Config::get('constants.path.js')}}/demo.js"></script>
  <script src="{{Config::get('constants.path.plugin')}}/datatables/handlebars-v4.0.5.js"></script> 

</body>
</html>
