<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.6
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="http://aeon-rt.com">AEON Research & Technology</a>.</strong> All rights
    reserved.
  </footer>