<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          @if(Auth::user()->filename == null)
            <img src="{{Config::get('constants.path.uploads')}}/user/thumbnail/placeholder.png" class="img-circle" alt="User Image">
          @else
            <img src="{{Config::get('constants.path.uploads')}}/user/{{Auth::user()->username}}/thumbnail/{{Auth::user()->filename}}" class="img-circle" alt="User Image">
          @endif
        </div>
        <div class="pull-left info">
          <p>{{Auth::user()->username}}</p>
          <a href="#"> {{Auth::user()->first_name}} {{Auth::user()->last_name}}</a>
        </div>
      </div>
      <!-- search form -->
      {{-- <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> --}}
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li>
          <a href="{{url('/')}}">
            <i class="fa fa-home"></i>
            <span>Home</span>
          </a>
        </li>

        <li>
          <a href="{{url('/')}}/editor">
            <i class="fa fa-dashboard"></i>
            <span>Dashboard</span>
          </a>
        </li>

        <li>
          <a href="http://ims99.vesselfreshfish.com">
            <i class="fa fa-sign-out"></i>
            <span>IMS.99</span>
          </a>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i>
            <span>User Menu</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-double-down pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @actionStart('user', 'read')
            <li><a href="{{ URL::route('editor.user.index') }}"><i class="fa fa-users"></i> User List</a></li>
            @actionEnd

            @actionStart('role', 'read')
            <li><a href="{{ URL::route('editor.role.index') }}"><i class="fa fa-address-card"></i> Role List</a></li>
            @actionEnd

            @actionStart('module', 'read')
            <li><a href="{{ URL::route('editor.module.index') }}"><i class="fa fa-gears"></i> Module List</a></li>
            @actionEnd

            @actionStart('action', 'read')
            <li><a href="{{ URL::route('editor.action.index') }}"><i class="fa fa-arrow-circle-o-right"></i> Action List</a></li>
            @actionEnd
            {{-- <li><a href="{{ URL::route('editor.privilege.index') }}"><i class="fa fa-user-secret"></i> Privilege List</a></li> --}}
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dropbox"></i>
            <span>Storage Menu</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-double-down pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @actionStart('placement_storage', 'read')
            <li><a href="{{ URL::route('editor.placement_storage.index') }}"><i class="fa fa-dropbox"></i> Placement Storage</a></li>
            @actionEnd

            @actionStart('storage', 'read')
            <li><a href="{{ URL::route('editor.storage.index') }}"><i class="fa fa-dropbox"></i> Storage</a></li>
            @actionEnd
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dollar"></i>
            <span>Purchasing Menu</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-double-down pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @actionStart('supplier', 'read')
            <li><a href="{{ URL::route('editor.supplier.index') }}"><i class="fa fa-truck"></i> Supplier</a></li>
            @actionEnd

            @actionStart('product_type', 'read')
            <li><a href="{{ URL::route('editor.product_type.index') }}"><i class="fa fa-cube"></i> Product Type</a></li>
            @actionEnd

            @actionStart('product', 'read')
            <li><a href="{{ URL::route('editor.product.index') }}"><i class="fa fa-cubes"></i> Product</a></li>
            @actionEnd

            @actionStart('size', 'read')
            <li><a href="{{ URL::route('editor.size.index') }}"><i class="fa fa-bar-chart"></i> Size</a></li>
            @actionEnd

            @actionStart('doc_code', 'read')
            <li><a href="{{ URL::route('editor.doc_code.index') }}"><i class="fa fa-file-code-o"></i> Code</a></li>
            @actionEnd
          </ul>
        </li>

        @actionStart('purchase', 'read')
        <li>
          <a href="{{ URL::route('editor.purchase.index') }}">
            <i class="fa fa-shopping-cart"></i>
            <span>Purchase</span>
          </a>
        </li>
        @actionEnd

        <li class="treeview">
          <a href="#">
            <i class="fa fa-eject"></i>
            <span>Production Menu</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-double-down pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @actionStart('fish', 'read')
            <li><a href="{{ URL::route('editor.fish.index') }}"><i class="fa fa-cube"></i> Sea Product</a></li>
            @actionEnd

            {{-- @actionStart('variable', 'read')
            <li><a href="{{ URL::route('editor.variable.index') }}"><i class="fa fa-dollar"></i> Variable</a></li>
            @actionEnd --}}

            @actionStart('production', 'read')
            <li><a href="{{ URL::route('editor.production.index') }}"><i class="fa fa-cubes"></i> Product</a></li>
            @actionEnd

            @actionStart('reject_reason', 'read')
            <li><a href="{{ URL::route('editor.reject_reason.index') }}"><i class="fa fa-ban"></i> Reject Reason</a></li>
            @actionEnd

            @actionStart('reject_type', 'read')
            <li><a href="{{ URL::route('editor.reject_type.index') }}"><i class="fa fa-ban"></i> Reject Type</a></li>
            @actionEnd

            @actionStart('system_option', 'read')
            <li><a href="{{ URL::route('editor.system_option.index') }}"><i class="fa fa-cog"></i> System Option</a></li>
            @actionEnd
          </ul>
        </li>

        @actionStart('production_result', 'read')
        <li>
          <a href="{{ URL::route('editor.production_result.index') }}">
            <i class="fa fa-cogs"></i>
            <span>Production</span>
          </a>
        </li>
        @actionEnd


        @actionStart('end_product', 'read')
        <li>
          <a href="{{ URL::route('editor.end_product.index') }}">
            <i class="fa fa-cube"></i>
            <span>End Product</span>
          </a>
        </li>
        @actionEnd

        {{-- <li class="treeview">
          <a href="#">
            <i class="fa fa-eject"></i>
            <span>Prepared Product Menu</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-double-down pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @actionStart('ingredient', 'read')
            <li><a href="{{ URL::route('editor.ingredient.index') }}"><i class="fa fa-cube"></i> Ingredient</a></li>
            @actionEnd

            @actionStart('m_prep_product', 'read')
            <li><a href="{{ URL::route('editor.m_prep_product.index') }}"><i class="fa fa-dollar"></i> Catalog</a></li>
            @actionEnd
          </ul>
        </li> --}}

        {{-- <li class="treeview">
          <a href="#">
            <i class="fa fa-eject"></i>
            <span>Item Menu</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-double-down pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @actionStart('category_item', 'read')
            <li><a href="{{ URL::route('editor.category_item.index') }}"><i class="fa fa-th-large"></i> Category Item</a></li>
            @actionEnd

            @actionStart('type_item', 'read')
            <li><a href="{{ URL::route('editor.type_item.index') }}"><i class="fa fa-indent"></i> Type Item</a></li>
            @actionEnd

             @actionStart('unit', 'read')
            <li><a href="{{ URL::route('editor.unit.index') }}"><i class="fa fa-table"></i> Unit</a></li>
            @actionEnd

            @actionStart('master_item', 'read')
            <li><a href="{{ URL::route('editor.master_item.index') }}"><i class="fa fa-list-ul"></i> Master Item</a></li>
            @actionEnd

            @actionStart('item', 'read')
            <li><a href="{{ URL::route('editor.item.index') }}"><i class="fa fa-table"></i> Item</a></li>
            @actionEnd

            @actionStart('custom_detail', 'read')
            <li><a href="{{ URL::route('editor.custom_detail.index') }}"><i class="fa fa-table"></i> Custom Detail</a></li>
            @actionEnd

            @actionStart('conversion_item', 'read')
            <li><a href="{{ URL::route('editor.conversion_item.index') }}"><i class="fa fa-table"></i> Conversion Item</a></li>
            @actionEnd
          </ul>
        </li> --}}

        {{-- @actionStart('production_result', 'read')
        <li>
          <a href="{{ URL::route('editor.prepared_product.index') }}">
            <i class="fa fa-cogs"></i>
            <span>Prepared Production</span>
          </a>
        </li>
        @actionEnd --}}

        {{-- <li>
          <a href="{{ URL::route('editor.uom_category.index') }}">
            <i class="fa fa-bar-chart"></i>
            <span>Unit of Measure Category</span>
          </a>
        </li> --}}

        {{-- <li>
          <a href="{{ URL::route('editor.uom.index') }}">
            <i class="fa fa-bar-chart"></i>
            <span>Unit of Measure</span>
          </a>
        </li> --}}

        {{-- @actionStart('cart', 'read')
        <li>
          <a href="{{ URL::route('editor.cart.index') }}">
            <i class="fa fa-cart-plus"></i>
            <span>Business Type</span>
          </a>
        </li>
        @actionEnd --}}
        
        {{-- @actionStart('customer', 'read')
        <li>
          <a href="{{ URL::route('editor.customer.index') }}">
            <i class="fa fa-users"></i>
            <span>Customer</span>
          </a>
        </li>
        @actionEnd --}}
        
        {{-- @actionStart('production_export', 'read')
        <li>
          <a href="{{ URL::route('editor.production_export.index') }}">
            <i class="fa fa-fighter-jet"></i>
            <span>Exported</span>
          </a>
        </li>
        @actionEnd --}}

        @actionStart('rnd', 'read')
        <li>
          <a href="{{ URL::route('editor.rnd.index') }}">
            <i class="fa fa-reorder"></i>
            <span>R n D</span>
          </a>
        </li>
        @actionEnd

        @actionStart('marketing_cost', 'read')
        <li>
          <a href="{{ URL::route('editor.marketing_cost.index') }}">
            <i class="fa fa-money"></i>
            <span>Marketing Cost</span>
          </a>
        </li>
        @actionEnd

         @actionStart('marketing', 'read')
        <li>
          <a href="{{ URL::route('editor.marketing.index') }}">
            <i class="fa fa-address-card"></i>
            <span>Marketing</span>
          </a>
        </li>
        @actionEnd

        <li class="treeview">
          <a href="#">
            <i class="fa fa-eject"></i>
            <span>Utilities Menu</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-double-down pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @actionStart('item_migration', 'read')
            <li><a href="{{ URL::route('editor.item_migration.index') }}"><i class="fa fa-th-large"></i>  Item Migration</a></li>
            @actionEnd
          </ul>
        </li>
    </section>
    <!-- /.sidebar -->
  </aside>
