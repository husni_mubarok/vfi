<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>VFI | Fish Production</title>    
    {{-- <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'> --}}
    <!-- Bootstrap 3.3.4 -->
    <link href="{{url('/')}}/assets/touch/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link href="{{url('/')}}/assets/touch/plugins/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{url('/')}}/assets/touch/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link href="{{url('/')}}/assets/touch/dist/css/skins/_all-skins.css" rel="stylesheet" type="text/css" />

    {{-- <link rel="stylesheet" href="{{url('/')}}/assets/touch/css/sortable-theme-light.css" /> --}}
    <link rel="stylesheet" href="{{url('/')}}/assets/touch/css/vfi_touch.css" />
    <link rel="stylesheet" href="{{url('/')}}/assets/touch/plugins/datatables/dataTables.bootstrap.css" />
    <link rel="stylesheet" href="{{url('/')}}/assets/touch/plugins/datetimepicker/bootstrap-datetimepicker.css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    {{-- KEYBOARD --}}
    <link href="{{url('/')}}/assets/touch/docs/css/jquery-ui.css" rel="stylesheet">
   

    <!-- keyboard widget css & script (required) -->
    <link href="{{url('/')}}/assets/touch/css/keyboard.css" rel="stylesheet">

    <link href="{{url('/')}}/assets/touch/css/all.css" rel="stylesheet" />


    @yield('styles')

</head>

<body> 
    <header class="main-header h_touch" >
         <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{url('/')}}/assets/touch/img/ic_user.png" class="user-image" alt="User Image"/>
                            <span class="hidden-xs">Operator Name</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-list"></i></a>
                    </li>
                </ul>

            </div>
        </nav>

        <aside class="main-sidebar">
            <section class="sidebar">
                <div class="user-panel">
                    <a href="{{ url('/') }}/touch"><center><img src="{{ url('/')}}/assets/touch/img/vfi-small.png" height="60px"></center></a>
                </div>
                        <ul class="menu_side_menu">
                            <a href="{{url('/')}}/touch">
                                <li><i class="fa fa-home"></i>&nbsp;&nbsp;Home</li>
                            </a>
                           
                            {{-- @if(Auth::user()->user_role->role->name != 'operation') --}}
                            <a href="{{url('/')}}/touch/projects">
                                <li><i class="fa fa-clock-o"></i> All Projects</li>
                            </a>
                            {{-- @endif --}}
                           
                            <form class="form-horizontal" role="form" method="POST" action="{{ url('/logout') }}">
                            <li>  

                            {{ csrf_field() }}
                                <button type="submit" class="btn_clear"><i class="fa fa-sign-out"></i> LOGOUT</button>
                            </li>
                            </form>
                            
                        </ul>
            </section>
        </aside>
         <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark" style="z-index: 0;background: #2b1711;min-height:100%;position: fixed;">
            <div style="min-height:100%">
            <div class="list_vfi">
                <ul>
                    @if($production_results->count() > 0)
                    @foreach($production_results as $key => $production_result)
                    @if($production_result->status == 'On Going')
                    <a href="{{ URL::route('production_result.create_waste', $production_result->id) }}">
                        <li>
                            <p>{{$production_result->production->name}}
                            <span class="badge pull-right">{{number_format($production_result->weight, 2)}}&nbsp;kg</span></p>
                            <i class="fa fa-play" title="Create Project Time"></i>&nbsp;&nbsp;{{date("D, d-m-Y", strtotime($production_result->started_at))}}
                        </li>
                    </a>
                    @endif
                    @endforeach
                    {{-- @else
                    <a href="">
                        <li class="">
                            <p>Dori Tanpa Kulit <span class="badge pull-right">354 Kg</span></p>
                            <i class="fa fa-play" title="Create Project Time"></i>&nbsp;&nbsp;Rabu, 12/10/2016
                        </li>
                    </a> --}}
                    @endif
                </ul>
            </div>
            </div>
        </aside><!-- /.control-sidebar -->


    </header>

       <div class="row full">
            <div class="col-md-2 vfileft">
                <div id="content_full">
                    <div class="side_menu">
                        <h3>
                            <center>
                                <a href="{{ URL::route('home') }}"><img src="{{ url('/')}}/assets/touch/img/vfi-small.png" height="100px" style="margin-top:-15px"></a>
                            </center>
                        </h3>
                        <div class="label_home">HOME</div>
                        <hr>
                        <ul class="menu_side_menu">
                            <a href="{{ URL::route('home') }}">
                                <li><i class="fa fa-home"></i>&nbsp;&nbsp;Home</li>
                            </a>

                            <a href="{{ URL::route('purchase.index') }}">
                                <li><i class="fa fa-shopping-cart"></i>&nbsp;&nbsp;Purchase</li>
                            </a>

                            <a href="{{ URL::route('production') }}">
                                <li><i class="fa fa-gears"></i>&nbsp;&nbsp;Production</li>
                            </a>

                            <a href="{{ URL::route('production_result') }}">
                                <li><i class="fa fa-hand-pointer-o"></i>&nbsp;&nbsp;Production&nbsp;(Touch)</li>
                            </a>
                           
                            {{-- @if(Auth::user()->user_role->role->name != 'operation') --}}
                            <a href="{{ URL::route('production_results') }}">
                                <li><i class="fa fa-cogs"></i> All Projects</li>
                            </a>
                            {{-- @endif --}}
                           
                            {{-- <form class="form-horizontal" role="form" method="POST" action="{{ url('/logout') }}">
                            <li>  

                            {{ csrf_field() }}
                                <button type="submit" class="btn_clear"><i class="fa fa-sign-out"></i> LOGOUT</button>
                            </li>
                            </form> --}}
                            
                        </ul>
                    </div>
                </div>
            </div>

            @yield('content')

            <div class="col-md-3 vfiright">
                {{-- FORM TRIGGER NUMPAD --}}
                <input id="text" type="hidden">


                <div id="content_full">
                     <div class="on_going">
                        <div class="list_vfi">
                            <ul>
                                @if($production_results->count() > 0)
                                @foreach($production_results as $key => $production_result)
                                @if($production_result->status == 'On Going')
                                <a href="{{ URL::route('production_result.create_waste', $production_result->id) }}">
                                <li>
                                    <p>{{$production_result->production->name}}
                                    <span class="badge pull-right">{{number_format($production_result->weight, 2)}}&nbsp;kg</span></p>
                                    <i class="fa fa-play" title="Create Project Time"></i>&nbsp;&nbsp;{{date("D, d-m-Y", strtotime($production_result->started_at))}}
                                </li>
                                @endif
                                @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>

                     <div class="lg_user_info"> 
                        <img src="{{ url('/') }}/assets/touch/img/ic_user.png" class="user_image" alt="User Image">
                        <p class="user_name">{{-- {{Auth::user()->name}} --}}</p>
                        <p class="user_email">{{-- {{Auth::user()->email}} --}}</p>
                    </div>
                    <div class="labelvfi">
                        ON GOING PROJECTS
                    </div>

                    
                    
                    <div class="datentime">
                        <center>
                            <span id="clock" class="time"></span>
                            <small id="clock2" class="time2"></small>
                        </center>
                        <p class="date"><?php echo date('l, j F Y') ;?></p>
                    </div>
                    <script   src="https://code.jquery.com/jquery-3.1.0.min.js"></script>  

                    <script>
                        function updateClock ( )
                        {
                          var currentTime = new Date (<?php strtotime(date('Y-m-d H:i:s'))*1000 ?>);
                          var currentHours = currentTime.getHours ( );
                          var currentMinutes = currentTime.getMinutes ( );
                          var currentSeconds = currentTime.getSeconds ( );

                          // Pad the minutes and seconds with leading zeros, if required
                          currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;
                          currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;

                          // Choose either "AM" or "PM" as appropriate
                          var timeOfDay = ( currentHours < 12 ) ? "AM" : "PM";

                          // Convert the hours component to 12-hour format if needed
                          currentHours = ( currentHours > 12 ) ? currentHours - 12 : currentHours;

                          // Convert an hours component of "0" to "12"
                          currentHours = ( currentHours == 0 ) ? 12 : currentHours;

                          // Compose the string for display
                          var currentTimeString = "" + currentHours + ":" + currentMinutes;
                          var currentTimeString2 = ":" + " " + currentSeconds + " " + timeOfDay;
                          $("#clock").html(currentTimeString);
                          $("#clock2").html(currentTimeString2);
                              
                        }

                        $(document).ready(function()
                        {
                            setInterval('updateClock()', 1000);
                        });
                    </script>
                        
                     {{-- <div class="fr_numpad">
                        <table class="tb_numpad">
                            <tr>
                                <td width="25%">7</td>
                                <td width="25%">8</td>
                                <td width="25%">9</td>
                                <td width="25%">Del</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>5</td>
                                <td>6</td>
                                <td>Next</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>2</td>
                                <td>3</td>
                                <td rowspan="2">Enter</td>
                            </tr>
                            <tr>
                                <td colspan=2>0</td>
                                <td> . </td>
                            </tr>
                        </table>
                    </div>--}}
                </div> 
            </div>
        </div>
</div>

<!-- jQuery 2.1.4 -->

    <script src="{{url('/')}}/assets/touch/js/jquery-2.1.1.min.js"></script>
    <script src="{{url('/')}}/assets/touch/docs/js/jquery-latest.min.js"></script>
    <script src="{{url('/')}}/assets/touch/docs/js/jquery-ui.min.js"></script>
    <script src="{{url('/')}}/assets/touch/js/jquery.keyboard.js"></script>
    <!-- keyboard extensions (optional) -->
    <script src="{{url('/')}}/assets/touch/js/jquery.keyboard.extension-caret.js"></script>
    <!-- demo only -->
    <script src="{{url('/')}}/assets/touch/docs/js/demo.js"></script>


{{-- HIDE KAYBOARD --}}
<script type="text/javascript">
    // $(window).on('resize', function () {
        if($(window).width() < 1024){
            $('input').removeClass('default_input_number');
            $('#full_keyboard').removeClass('default_input_keyboard');
        }
        // $('input').toggleClass('default_input_number ', $(window).width() > 468);
        // $('input').text($(window).width());
    // }).trigger('resize')
</script>

{{-- <script src="{{url('/')}}/js/all.js"></script> --}}
<!-- Bootstrap 3.3.2 JS -->
<script type="text/javascript" src="{{url('/')}}/assets/touch/plugins/daterangepicker/moment.min.js"></script>
{{-- <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script> --}}
<script src="{{url('/')}}/assets/touch/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- FastClick -->
<script src='{{url('/')}}/assets/touch/plugins/fastclick/fastclick.min.js'></script>
<!-- AdminLTE App -->
<script src="{{url('/')}}/assets/touch/dist/js/app.min.js" type="text/javascript"></script>
<!-- Sparkline -->
<script src="{{url('/')}}/assets/touch/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
<!-- jvectormap -->
{{-- <script src="{{url('/')}}/assets/touch/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script> --}}
{{-- <script src="{{url('/')}}/assets/touch/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script> --}}
<!-- SlimScroll 1.3.0 -->
<script src="{{url('/')}}/assets/touch/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<!-- ChartJS 1.0.1 -->
<script src="{{url('/')}}/assets/touch/plugins/chartjs/Chart.min.js" type="text/javascript"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!--<script src="{{url('/')}}/assets/touch/dist/js/pages/dashboard2.js" type="text/javascript"></script>-->

{{--<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>--}}
<!-- AdminLTE for demo purposes -->
<script src="{{url('/')}}/assets/touch/dist/js/demo.js" type="text/javascript"></script>
{{-- <script src="{{url('/')}}/assets/touch/js/sortable.min.js"></script> --}}

<script src="{{url('/')}}/assets/touch/plugins/datetimepicker/bootstrap-datetimepicker.js"></script>
{{-- <script src="{{url('/')}}/assets/touch/vfi.js"></script> --}}
<script>
    $('div.alert').not('.alert-important').delay(3000).slideUp(300);
//    $('#flash-overlay-modal').modal();
</script>

@yield('scripts')
</body>
</html>