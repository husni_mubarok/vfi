<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
            <img src="{{Config::get('constants.path.uploads')}}/user/thumbnail/placeholder.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>VFI</p>
          <a href="#">Fish System</a>
        </div>
      </div>
      <!-- search form -->
      {{-- <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> --}}
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        {{-- <li>
          <a href="{{url('/')}}">
            <i class="fa fa-home"></i> 
            <span>Home</span>
          </a>
        </li> --}}

        <li>
          <a href="http://ims99.vesselfreshfish.com">
            <i class="fa fa-home"></i> 
            <span>Home</span>
          </a>
        </li>
        
        @actionStart('purchase', 'read')
        <li>
          <a href="{{ URL::route('purchase.index') }}">
            <i class="fa fa-shopping-cart"></i> 
            <span>Purchase</span>
          </a>
        </li>
        @actionEnd

        @actionStart('production', 'read')
        <li class="treeview">
          <a href="#">
            <i class="fa fa-cog"></i>
            <span>Production</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-double-down pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li>
              <a href="{{ URL::route('production') }}">
                <i class="fa fa-gears"></i> 
                <span>Working</span>
              </a>
            </li>
            <li>
              <a href="{{ URL::route('production.unstored') }}">
                <i class="fa fa-cube"></i> 
                <span>Unstored</span>
              </a>
            </li>
            <li>
              <a href="{{ URL::route('production.bank') }}">
                <i class="fa fa-archive"></i> 
                <span>Bank</span>
              </a>
            </li>
          </ul>
        </li>
        @actionEnd

        {{-- @actionStart('production', 'read')
        <li>
          <a href="{{ URL::route('production_result') }}">
            <i class="fa fa-hand-pointer-o"></i> 
            <span>Production&nbsp;(Touch)</span>
          </a>
        </li>
        @actionEnd --}}

        {{-- @actionStart('storage', 'read')
        <li>
          <a href="{{ URL::route('storage.index') }}">
            <i class="fa fa-dropbox"></i> 
            <span>Storage</span>
          </a>
        </li>
        @actionEnd --}}

        {{-- @actionStart('export', 'read')
        <li>
          <a href="{{ URL::route('cart.index') }}">
            <i class="fa fa-share-square-o"></i> 
            <span>Export</span>
          </a>
        </li>
        @actionEnd --}}

        @actionStart('end_product', 'read')
        <li>
          <a href="{{ URL::route('end_product') }}">
            <i class="fa fa-gift"></i>
            <span>End Product</span>
          </a>
        </li>
        @actionEnd

        @if(Auth::check())
        <li>
          <a href="{{ URL::route('editor.index') }}">
            <i class="fa fa-user-secret"></i>
            <span>Administrator</span>
          </a>
        </li>
        @endif

    </section>
    <!-- /.sidebar -->
  </aside>