@extends('layouts.home.template')
@section('content')
<section class="content box box-solid">
    <h1> VFI System</h1>
    <div class="row">
        
        @actionStart('purchase', 'read')
        <div class="col-md-3 col-sm-6 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3>PURCHASE</h3>
                <p>Records purchasing history</p>
            </div>
            <div class="icon">
                <i class="fa fa-shopping-cart"></i>
            </div>
            <a href="{{URL::route('purchase.index')}}" class="small-box-footer">GO TO PURCHASE <i class="fa fa-arrow-circle-right"></i></a>
        </div>
        </div><!-- /.col -->
        @actionEnd

        @actionStart('production', 'read')
        <div class="col-md-3 col-sm-6 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3>PRD WORKING</h3>
                <p>Production on progress</p>
            </div>
            <div class="icon">
                <i class="fa fa-gears"></i>
            </div>
            <a href="{{URL::route('production')}}" class="small-box-footer">GO TO PRODUCTION <i class="fa fa-arrow-circle-right"></i></a>
        </div>
        </div><!-- /.col -->
        @actionEnd

        {{-- @actionStart('production', 'read')
        <div class="col-md-3 col-sm-6 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3>PRD UNSTORED</h3>
                <p>Production that has not been stored</p>
            </div>
            <div class="icon">
                <i class="fa fa-cube"></i>
            </div>
            <a href="{{URL::route('production.unstored')}}" class="small-box-footer">GO TO PRODUCTION <i class="fa fa-arrow-circle-right"></i></a>
        </div>
        </div><!-- /.col -->
        @actionEnd --}}

        {{-- @actionStart('production', 'read')
        <div class="col-md-3 col-sm-6 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3>PRD BANK</h3>
                <p>Production that has been stored</p>
            </div>
            <div class="icon">
                <i class="fa fa-archive"></i>
            </div>
            <a href="{{URL::route('production.bank')}}" class="small-box-footer">GO TO PRODUCTION <i class="fa fa-arrow-circle-right"></i></a>
        </div>
        </div><!-- /.col -->
        @actionEnd --}}

        {{-- @actionStart('production', 'read')
        <div class="col-md-3 col-sm-6 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-orange">
            <div class="inner">
                <h3>TOUCH</h3>
                <p>Touch screen version of Production</p>
            </div>
            <div class="icon">
                <i class="fa fa-hand-pointer-o"></i>
            </div>
            <a href="{{URL::route('production_result')}}" class="small-box-footer">GO TO PRODUCTION (TOUCH) <i class="fa fa-arrow-circle-right"></i></a>
        </div>
        </div><!-- /.col -->
        @actionEnd --}}

        {{-- @actionStart('storage', 'read')
        <div class="col-md-3 col-sm-6 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-blue">
            <div class="inner">
                <h3>STORAGE</h3>
                <p>Touch screen version of Production</p>
            </div>
            <div class="icon">
                <i class="fa fa-dropbox"></i>
            </div>
            <a href="{{URL::route('storage.index')}}" class="small-box-footer">GO TO STORAGE <i class="fa fa-arrow-circle-right"></i></a>
        </div>
        </div><!-- /.col -->
        @actionEnd --}}

        {{-- @actionStart('export', 'read')
        <div class="col-md-3 col-sm-6 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3>EXPORT</h3>
                <p>Touch screen version of Production</p>
            </div>
            <div class="icon">
                <i class="fa fa-share-square-o"></i>
            </div>
            <a href="{{URL::route('cart.index')}}" class="small-box-footer">GO TO EXPORT <i class="fa fa-arrow-circle-right"></i></a>
        </div>
        </div><!-- /.col -->
        @actionEnd --}}

        @actionStart('end_product', 'read')
        <div class="col-md-3 col-sm-6 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-orange">
            <div class="inner">
                <h3>END PRODUCT</h3>
                <p>Create sashimi or value added products</p>
            </div>
            <div class="icon">
                <i class="fa fa-gift"></i>
            </div>
            <a href="{{URL::route('end_product')}}" class="small-box-footer">GO TO END PRODUCT <i class="fa fa-arrow-circle-right"></i></a>
        </div>
        </div><!-- /.col -->
        @actionEnd

        @if(Auth::check())
        <div class="col-md-3 col-sm-6 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-red">
            <div class="inner">
                <h3>ADMINISTRATOR</h3>
                <p>Page to manage the system</p>
            </div>
            <div class="icon">
                <i class="fa fa-user-secret"></i>
            </div>
            <a href="{{URL::route('editor.index')}}" class="small-box-footer">GO TO ADMIN PAGE <i class="fa fa-arrow-circle-right"></i></a>
        </div>
        </div><!-- /.col -->
        @endif

    </div>
</section>
@stop