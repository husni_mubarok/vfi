@extends('layouts.editor.template')
@section('content')
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><a href="#"><i class="fa fa-cubes"></i> Type Item</a></li>
	</ol>
</section>
<section class="content">
	<section class="content box mobile box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-1"></div>
				<div class="col-md-12">
					<div class="x_panel">
						<h2>
							<i class="fa fa-cubes"></i> Item Type List
							<a href="{{ URL::route('editor.item_type.create') }}" class="btn btn-success btn-lg pull-right"><i class="fa fa-plus"></i> Add</a>
						</h2>
						<hr>
						<div class="x_content">
							<table id="drinkTablex" class="table rwd-table dataTable">
								<thead>
									<tr>
										<th>#</th>
										<th>Name</th>
										<th>Description</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									@foreach($item_types as $key => $item_type)
									<tr>
										<td data-th="#">{{$number++}}</td>
										<td data-th="Name">{{$item_type->type_name}}</td>
										<td data-th="Description">{{$item_type->type_desc}}</td>
										<td align="center">
											<div class="act_tb">
												<div >
													<a href="{{ URL::route('editor.item_type.edit', [$item_type->id]) }}" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i></a>
												</div>
												<div >
													{!! Form::open(array('route' => ['editor.item_type.delete', $item_type->id], 'method' => 'delete', 'class'=>'delete'))!!}
													{{ csrf_field() }}	                    				
													<button type="submit" class="btn btn-default btn-sm"><i class="fa fa-trash"></i></a></button>
													{!! Form::close() !!}
												</div>
											</div>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
						{{ $item_types->links() }}
					</div>
				</div>
			</div>
		</div>
	</section>
</section>
<!-- 
<div id="dialog" title="Confirmation Required">
  Are you sure about this?
</div> -->

@stop

@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
	$(document).ready(function () {
		$("#drinkTable").DataTable();
	});
</script>

<!-- Add fancyBox -->
<link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$(".fancybox").fancybox();
	});
</script>

<script>
	$(".delete").on("submit", function(){
		return confirm("Do you want to delete this item?");
	});
</script> 
@stop