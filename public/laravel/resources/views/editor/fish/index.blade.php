@extends('layouts.home.template')
@section('content')
<div class="header_menu">
    <a href="{{ URL::route('editor.fish.index') }}">FISH</a>
    <a href="{{ URL::route('editor.production.index') }}">PRODUCT</a>
</div>
@actionStart('fish', 'read')
<section class="content">
	<div class="row">
	    <div class="col-md-12 col-sm-12 col-xs-12">
	    	<div class="col-md-1"></div>
	    	<div class="col-md-10">
		        <div class="x_panel">
	                <h2>
	                	<i class="fa fa-cube"></i> Sea Product List
	                	@actionStart('fish', 'create')
	                	<a href="{{ URL::route('editor.fish.create') }}" class="btn btn-default btn-lg pull-right"><i class="fa fa-plus"></i> Add</a>
	                	@actionEnd
                	</h2>
	                <hr>
		            <div class="x_content">
		                <table id="fishTable" class="table table-striped dataTable">
						  	<thead>
						  	  	<tr>
							      	<th width="5%">#</th>
							      	<th>Name</th>
							      	<th>Purchase of</th>
							      	<th width="10%"></th>
						    	</tr>
						  	</thead>
						  	<tbody>
						    @foreach($fishes as $key => $fish)
						    	<tr>
						      		<td>{{$key+1}}</td>
							      	<td>{{$fish->name}}</td>
							      	<td>
								      	<table class="table">
					      				<thead>
					      					<tr>
				      							<th>
			      									@actionStart('product', 'update')
			      									<a href="{{ URL::route('editor.fish.edit_product', [$fish->id]) }}" class="btn btn-primary btn-sm">
				      								<i class="fa fa-pencil"></i> Edit
				      								</a>
				      								@actionEnd
			      								</th>
					      					</tr>
					      				</thead>
					      				<tbody>
					      					@foreach($fish->product_fish as $product)
					      					<tr>
					      						<td>{{$product->product->name}}</td>
					      					</tr>
					      					@endforeach
					      				</tbody>
							      		</table>
						      		</td>
							      	<td align="center">
							      		<div class="col-md-2 nopadding">
							      			@actionStart('fish', 'update')
							      			<a href="{{ URL::route('editor.fish.edit', [$fish->id]) }}" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i></a>
							      			@actionEnd
							      		</div>
						      			<div class="col-md-2 nopadding">
						      				@actionStart('fish', 'delete')
							      			{!! Form::open(array('route' => ['editor.fish.delete', $fish->id], 'method' => 'delete'))!!}
	                    					{{ csrf_field() }}	                    				
							      			<button type="submit" class="btn btn-default btn-sm" onclick="return confirm('Confirm delete?')"><i class="fa fa-trash"></i></a></button>
							      			{!! Form::close() !!}
							      			@actionEnd
						      			</div>
						      		</td>
							    </tr>
						    @endforeach
							</tbody>
						</table>
		            </div>
		        </div>
	        </div>
	    </div>
	</div>
</section>
@actionEnd
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function () {
    $("#fishTable").DataTable(
    	{
    	});
    });
</script>
@stop