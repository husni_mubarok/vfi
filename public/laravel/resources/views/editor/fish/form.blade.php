@extends('layouts.home.template')
@section('content')
<div class="header_menu">
    <a href="{{ URL::route('editor.fish.index') }}">FISH</a>
    <a href="{{ URL::route('editor.production.index') }}">PRODUCT</a>
</div>
@actionStart('fish', 'create|update')
<section class="content">
	<div class="row">
	    <div class="col-md-12 col-sm-12 col-xs-12">
	    	<div class="col-md-1"></div>
	    	<div class="col-md-5">
		        <div class="x_panel">
	                <h2>
	                	@if(isset($fish))
	                	<i class="fa fa-pencil"></i>
	                	@else
	                	<i class="fa fa-plus"></i> 
	                	@endif
	                	<i class="fa fa-cube"></i> Sea Product
                	</h2>
	                <hr>
		            <div class="x_content">
		                @include('errors.error')
                        @if(isset($fish))
		                {!! Form::model($fish, array('route' => ['editor.fish.update', $fish->id], 'method' => 'PUT'))!!}
	                    @else
	                    {!! Form::open(array('route' => 'editor.fish.store'))!!}
	                    @endif
	                    {{ csrf_field() }}
	                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
	                    	{{ Form::label('name', 'Name') }}
	                        {{ Form::text('name', old('name'), array('class' => 'form-control', 'placeholder' => 'Name*', 'required' => 'true')) }}<br/>

                            <button type="submit" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
                    	</div>
                        {!! Form::close() !!}
		            </div>
		        </div>
	        </div>
	    </div>
	</div>
</section>
@actionEnd
@stop