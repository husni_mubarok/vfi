@extends('layouts.home.template')
@section('content')
<div class="header_menu">
    <a href="{{ URL::route('editor.fish.index') }}">FISH</a>
    <a href="{{ URL::route('editor.production.index') }}">PRODUCT</a>
</div>
<section class="content">
	<div class="row">
	    <div class="col-md-12 col-sm-12 col-xs-12">
	    	<div class="col-md-1"></div>
	    	<div class="col-md-5">
		        <div class="x_panel">
	                <h2>
	                	<i class="fa fa-cubes"></i>
	                	<i class="fa fa-cube"></i> {{$fish->name}} Product
                	</h2>
	                <hr>
		            <div class="x_content">
		                @include('errors.error')
	                    {!! Form::open(array('route' => ['editor.fish.update_product', $fish->id]))!!}
	                    {{ csrf_field() }}
	                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                    	<table id="productTable" class="table table-sm table-hover">
                    	<thead>
	                    	<tr>
	                    		<th><i class="fa fa-check-square-o"></i></th>
	                    		<th>Name.</th>
	                    		<th>Description</th>
	                    		{{-- <th>UoM</th> --}}
	                    	</tr>
                    	</thead>
                    	<tbody>
	                    	@foreach($products as $key => $product)
	                    	<tr>
	                    		<td>
		                    		@if(in_array($product->id, $fish->product_fish->pluck('product_id')->toArray()))
			                    	{{ Form::checkbox('product[]', $product->id, true) }}
			                    	@else
			                    	{{ Form::checkbox('product[]', $product->id) }}
			                    	@endif
		                    	</td>
		                    	<td>{{$product->name}}</td>
		                    	<td>{{$product->description}}</td>
		                    	{{-- <td>{{$product->uom}}</td> --}}
                    		</tr>
	                    	@endforeach	
                		</tbody>
						</table>	                    		
                        <button type="submit" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
                    	</div>
                        {!! Form::close() !!}
		            </div>
		        </div>
	        </div>
	    </div>
	</div>
</section>
@stop