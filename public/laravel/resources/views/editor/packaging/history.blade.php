@extends('layouts.home.template')
@section('content')
<section class="content box box-solid">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			{{-- <div class="col-md-1"></div> --}}
			<div class="col-md-5">
				<div class="x_panel">
					<h2>
						@if(isset($item))
						<i class="fa fa-pencil"></i>
						@else
						<i class="fa fa-plus"></i> 
						@endif
						<i class="fa fa-bar-chart"></i> Detail Item
					</h2>
					<hr>
					<div class="x_content">
						@include('errors.error')
						{!! Form::model($item, array('route' => ['editor.item.storedetail', $item->id, 'method' => 'PUT']))!!}
						{{ csrf_field() }}
						<div class="col-md-12 col-sm-12 col-xs-12 form-group">
							{{ Form::label('name', 'Name') }}
							{{ Form::text('name', old('name'), array('class' => 'form-control', 'placeholder' => 'Name*', 'required' => 'true', 'disabled' => 'disabled')) }}<br/>
							
							{{ Form::label('Master Item') }}
							{{ Form::select('id_master_item', $master_item_list, old('id_master_item'), array('class' => 'form-control', 'required' => 'true', 'disabled' => 'disabled')) }}<br/>

							{{ Form::label('description', 'Description') }}
							{{ Form::text('description', old('description'), array('class' => 'form-control', 'placeholder' => 'Description*', 'required' => 'true', 'disabled' => 'disabled')) }}<br/>
						</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<h4><b>- STORAGE </b></h4>
				<div class="col-md-12 col-sm-12 col-xs-12">          
					<table class="table table-bordered" id="detail_table">
						<thead>
							<tr>
								<th>#</th>
								<th>Type</th>
								<th>Doc Code</th>
								<th>Date Time</th>
								<th>Qty</th>
								<th>UoM</th>
							</tr>
						</thead>
						<tbody>
							@foreach($item_history_storage as $key => $item_history_storages)
							<tr>
								<td>{{$key+1}}</td>
								@if($item_history_storages->id_type_transaction_storage == '1')
								<td><span class='label label-success'>In</span></td>
								@elseif($item_history_storages->id_type_transaction_storage == '2')
								<td><span class='label label-danger'>Out</span></td>
								@endif
								<td>{{$item_history_storages->doc_code}}</td>
								<td>{{$item_history_storages->datetime}}</td>
								<td>{{$item_history_storages->quantity}}</td>
								<td>{{$item_history_storages->unit}}</td>
							</tr>
							@endforeach
						</tbody>{{-- 
						<tfoot>
							<tr>
								<td>Stock</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</tfoot> --}}
					</table> 
				</div>
			</div>
			<br/>

			<div class="col-md-12">
				<h4><b>- TRANSACTION</b></h4>
				<div class="col-md-12 col-sm-12 col-xs-12">          
					<table class="table table-bordered" id="detail_table_export">
						<thead>
							<tr>
								<th>#</th> 
								<th>Doc Code</th>
								<th>Date Time</th>
								<th>Customer</th>
								<th>PO No</th>
								<th>PO date</th>
								<th>Qty</th>
								<th>UoM</th>
								<th>Type Transaction</th>
							</tr>
						</thead>
						<tbody>
							@foreach($item_history_export as $key => $item_history_exports)
							<tr>
								<td>{{$key+1}}</td>
								
								
								<td>{{$item_history_exports->doc_code}}</td>
								<td>{{$item_history_exports->date}}</td>
								<td>{{$item_history_exports->customer_name}}</td>
								<td>{{$item_history_exports->customer_pono}}</td>
								<td>{{$item_history_exports->customer_podate}}</td>
								<td>{{$item_history_exports->quantity}}</td>
								<td>{{$item_history_exports->unit}}</td>
								<td><span class='label label-default'>{{$item_history_exports->description}}</span></td>
							</tr>
							@endforeach
						</tbody>
					</table> 
				</div>
			</div>
			<a href="{{ URL::route('editor.item.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
		</div>
	</div>
</div>
</div>
</section>
@stop

@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script>
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>

<script>
	$(document).ready(function () {
		$("#detail_table").DataTable(
		{
			"scrollX": true
		});

		$("#detail_table_export").DataTable(
		{
			"scrollX": true
		});


		$("#detail_table_rnd").DataTable(
		{
			"scrollX": true
		});
	});
</script>
@stop