@extends('layouts.editor.template')
@section('content')
<style type="text/css">
  #myModal .modal-dialog
  {
    width: 90%;
  }

  #myModalEdit .modal-dialog
  {
    width: 50%;
  }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li class="active"><a href="{{ URL::route('editor.production_export.index') }}"><i class="fa fa-cube"></i> Export</a></li>
  </ol>
</section>
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-5">
			        <div class="x_panel">
		                <h2>
		                	@if(isset($production_export))
		                	<i class="fa fa-pencil"></i>
		                	@else
		                	<i class="fa fa-plus"></i> 
		                	@endif
		                	<i class="fa fa-cube"></i> Export
	                	</h2>

		                <hr>
			               <div class="x_content">
  			                @include('errors.error')
  	                        @if(isset($production_export))
  			                {!! Form::model($production_export, array('route' => ['editor.production_export.update', $production_export->id], 'method' => 'PUT'))!!}
  		                    @else
  		                    {!! Form::open(array('route' => 'editor.production_export.store'))!!}
  		                    @endif
  		                    {{ csrf_field() }}
                     </div>
		             
                          {{ Form::label('doc_code', 'Doc Code (auto)') }}
                          {{ Form::text('doc_code', old('doc_code'), array('class' => 'form-control', 'placeholder' => 'Doc Code*', 'required' => 'true', 'disabled')) }}

                          {{ Form::label('date', 'Date') }}
                          {{ Form::text('date', old('date'), array('class' => 'form-control', 'placeholder' => 'Date & Time*', 'required' => 'true', 'id' => 'dateprod')) }}
                      
                          <hr/>

                          <a href="#myModal" class="btn btn-primary" id="lookup"><i class="fa fa-folder-open"></i>  Lookup</a><br/>

                          {{ Form::hidden('source_id', old('source_id'), array('class' => 'form-control', 'placeholder' => 'source_id *', 'required' => 'true', 'id' => 'source_id')) }}

                          
                          {{ Form::label('name', 'Name') }}
                          {{ Form::text('name', old('name'), array('class' => 'form-control', 'placeholder' => 'Name*', 'required' => 'true', 'id' => 'name')) }}
                          
                          {{ Form::label('criteria', 'Criteria') }}
                          {{ Form::text('criteria', old('criteria'), array('class' => 'form-control', 'placeholder' => 'Criteria*', 'required' => 'true', 'id' => 'criteria')) }}

                          {{ Form::label('size', 'Size') }}
                          {{ Form::text('size', old('size'), array('class' => 'form-control', 'placeholder' => 'Size*', 'required' => 'true', 'id' => 'size')) }}

                          {{ Form::label('block', 'Block') }}
                          {{ Form::text('block', old('block'), array('class' => 'form-control', 'placeholder' => 'Block*', 'required' => 'true', 'id' => 'block')) }}
                                                          
                          {{ Form::label('quantity', 'Qty') }}
                          {{ Form::number('quantity', old('quantity'), array('class' => 'form-control', 'placeholder' => 'Qty', 'id' => 'quantity', 'onchange' => 'stockval()')) }}

                          {{ Form::hidden ('quantity1', old('quantity1'), array('class' => 'form-control', 'placeholder' => 'Qty', 'id' => 'quantity1')) }}
                          <br/>
                          <button type="submit" id="btnadd" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-plus"></i> Add</button>
                          <br/>
	                    	</div>
	                        {!! Form::close() !!}
			            </div>
			        
                  <div class="col-md-12 col-sm-12 col-xs-12">          
                      <table class="table table-bordered" id="export_table">
                          <thead>
                                  <tr>
                                  <th>#</th>
                                  <th>Name</th>
                                  <th>Criteria</th>
                                  <th>Size</th>
                                  <th>Block</th>
                                  <th>Qty</th>
                                  <th>Action</th>
                          </tr>
                          </thead>
                          <tbody>
                        @foreach($production_exportdets as $key => $production_exportdet)
                            <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$production_exportdet->name}}</td>
                                    <td>{{$production_exportdet->criteria}}</td>
                                    <td>{{$production_exportdet->size}}</td>
                                    <td>{{$production_exportdet->block}}</td>
                                    <td>{{$production_exportdet->quantity}}</td>
                                    <td align="center">
                                        <div class="col-md-2 nopadding">                            
                                                <a href="javascript:void(0)" onclick="editValue(this, {{ $production_exportdet->id }}); showeditmodal();" class="btn btn-default btn-sm">
                                                <i class="fa fa-edit"></i></a>

                                        </div>
                                        <div class="col-md-2 nopadding">
                                                {!! Form::open(array('route' => ['editor.production_export.deletedet', $production_exportdet->id], 'method' => 'delete'))!!}
                                                {{ csrf_field() }}                              
                                                <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-trash"></i></a></button>
                                                {!! Form::close() !!}
                                        </div>
                                    </td>
                                </tr>
                        @endforeach
                            </tbody>
                    </table>  
                    @if(isset($production_export))
                    {!! Form::model($production_export, array('route' => ['editor.production_export.storeconfirm', $production_export->id], 'method' => 'PUT'))!!}
                      <button type="submit" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Export</button> 
                      @else
                      {!! Form::open(array('route' => 'editor.production_export.store'))!!}
                      <button type="submit" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();" disabled="disabled"><i class="fa fa-check"></i> Export</button> 
                      @endif
                      {{ csrf_field() }} 
                    {!! Form::close() !!} 
                    <a href="{{ URL::route('editor.production_export.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a></td>
                 </div> 
             </div>   
		        
		    </div>

		</div>
	</section>
</section>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Production</h4>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-responsive" id="productionResult">
          <thead>
          <tr>
            <th>No</th>
            <th>Name</th>
            <th>Criteria</th>
            <th>Block Weight</th>
            <th>Size</th>
            <th>Block</th>
            <th>Start At</th>
            <th>Finish At</th>
            <th>Prod Qty</th>
            <th>Export Qty</th>
            <th>Remain Qty</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @foreach($tempstorage as $tempstorages)
          <tr>
            <td>{{$tempstorages->resultdetailid}}</td>
            <td>{{$tempstorages->name}}</td>
            <td>{{$tempstorages->criteria}}</td>
            <td>{{$tempstorages->block_weight}}</td>
            <td>{{$tempstorages->size}}</td>
            <td>{{$tempstorages->block}}</td>
            <td>{{$tempstorages->started_at}}</td>
            <td>{{$tempstorages->finished_at}}</td>
            <td>{{$tempstorages->prodqty}}</td>
            <td>{{$tempstorages->exportqty}}</td>
            <td>{{$tempstorages->remainqty}}</td>
            <td><a href="javascript:void(0)" class="btn btn-success" id="btnstore" onclick="addValue(this, {{ $tempstorages->id }})">Store</a></td>
          </tr>
          @endforeach

        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Back</button>
      </div>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Edit Item </h4>
      </div>
      <div class="modal-body">
      
            {!! Form::model($production_exportdet, array('route' => ['editor.production_export.updatedetail', $production_exportdet->id], 'method' => 'PUT'))!!}
                {{ Form::text('iddetail', old('iddetail'), array('class' => 'form-control', 'placeholder' => 'iddetail *', 'required' => 'true', 'id' => 'iddetail')) }}
                
                {{ Form::label('nameedit', 'Name') }}
                {{ Form::text('nameedit', old('nameedit'), array('class' => 'form-control', 'placeholder' => 'Name*', 'required' => 'true', 'id' => 'nameedit')) }}
                
                {{ Form::label('criteriaedit', 'Criteria') }}
                {{ Form::text('criteriaedit', old('criteriaedit'), array('class' => 'form-control', 'placeholder' => 'Criteria*', 'required' => 'true', 'id' => 'criteriaedit')) }}

                {{ Form::label('sizeedit', 'Size') }}
                {{ Form::text('sizeedit', old('sizeedit'), array('class' => 'form-control', 'placeholder' => 'Size*', 'required' => 'true', 'id' => 'sizeedit')) }}

                {{ Form::label('blockedit', 'Block') }}
                {{ Form::text('block', old('blockedit'), array('class' => 'form-control', 'placeholder' => 'Block*', 'required' => 'true', 'id' => 'blockedit')) }}
                                                
                {{ Form::label('quantityedit', 'Quantity') }}
                {{ Form::text('block', old('quantityedit'), array('class' => 'form-control', 'placeholder' => 'Block*', 'required' => 'true', 'id' => 'quantityedit')) }}
               
                <br/>
                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Back</button>
                <button type="submit" id="btnadd" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-plus"></i> Add</button>

                <br/>
              </div>
          {!! Form::close() !!} 
        </div>


      </div>      
    </div>
  </div>
</div>


@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script>
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>

<script>
$(document).ready(function () {
	$('#dateprod').datetimepicker({
		sideBySide: true,
		format: 'YYYY-MM-DD HH:mm',
	});
});

function addValue(str, id){
  var prd = id;
  var store_id = $(str).closest('tr').find('td:eq(0)').text();
  var source_id = $(str).closest('tr').find('td:eq(0)').text();
  var name = $(str).closest('tr').find('td:eq(1)').text();
  var criteria = $(str).closest('tr').find('td:eq(2)').text(); 
  var size = $(str).closest('tr').find('td:eq(4)').text(); 
  var block = $(str).closest('tr').find('td:eq(5)').text(); 
  var remainqty = $(str).closest('tr').find('td:eq(10)').text();

  $("#source_id").val(source_id);
  $("#quantity").val(remainqty);
  $("#name").val(name);
  $("#criteria").val(criteria);
  $("#size").val(size);
  $("#block").val(block);
  $("#quantity1").val(remainqty);
  $("#iddetail").val('')
  console.log(id);
  $('#myModal').modal("hide");
}

function editValue(str, id){
  var prd = id;

  var nameedit = $(str).closest('tr').find('td:eq(1)').text();
  var criteriaedit = $(str).closest('tr').find('td:eq(2)').text(); 
  var sizeedit = $(str).closest('tr').find('td:eq(3)').text(); 
  var blockedit = $(str).closest('tr').find('td:eq(4)').text(); 
  var quantityedit = $(str).closest('tr').find('td:eq(5)').text();

  $("#iddetail").val(prd)
  $("#quantityedit").val(quantityedit);
  $("#nameedit").val(nameedit);
  $("#criteriaedit").val(criteriaedit);
  $("#sizeedit").val(sizeedit);
  $("#blockedit").val(blockedit);
  console.log(id);
}

function showeditmodal(){
  $('#myModalEdit').modal();
}

$(document).ready(function(){
  $("#productionResult").DataTable({
    "pageLength": 3
  });

  $("#lookup").click(function(){
    console.log('testing');
    $('#myModal').modal();
  });

  $('#datetime').datetimepicker({
		sideBySide: true,
		format: 'YYYY-MM-DD HH:mm',
  });

});

function stockval() {   
    
    var quantity = $('#quantity').val();
    var quantity1 = $('#quantity1').val();
    if ( parseInt(quantity) > parseInt(quantity1) ) {
        $('#quantity').val(quantity1);
        alert("over stock"); 
    }
};
</script>
@stop