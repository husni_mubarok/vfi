@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <h3>Purchasing</h3>
  <!-- Info boxes -->
  <div class="row">

    @actionStart('size', 'read')
    <div class="col-md-3 col-sm-6 col-xs-12">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>{{$sizes->count()}}</h3>
          <p>SIZES</p>
        </div>
        <div class="icon">
          <i class="fa fa-bar-chart"></i>
        </div>
        <a href="{{ URL::route('editor.size.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- /.col -->
    @actionEnd

    @actionStart('product_type', 'read')
    <div class="col-md-3 col-sm-6 col-xs-12">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
          <h3>{{$product_types->count()}}</h3>
          <p>PRODUCT TYPES</p>
        </div>
        <div class="icon">
          <i class="fa fa-cube"></i>
        </div>
        <a href="{{ URL::route('editor.product_type.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- /.col -->
    @actionEnd

    @actionStart('product', 'read')
    <div class="col-md-3 col-sm-6 col-xs-12">
      <!-- small box -->
      <div class="small-box bg-blue">
        <div class="inner">
          <h3>{{$products->count()}}</h3>
          <p>PRODUCTS</p>
        </div>
        <div class="icon">
          <i class="fa fa-cubes"></i>
        </div>
        <a href="{{ URL::route('editor.product.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- /.col -->
    @actionEnd

    @actionStart('supplier', 'read')
    <div class="col-md-3 col-sm-6 col-xs-12">
      <!-- small box -->
      <div class="small-box bg-red">
        <div class="inner">
          <h3>{{$suppliers->count()}}</h3>
          <p>SUPPLIERS</p>
        </div>
        <div class="icon">
          <i class="fa fa-truck"></i>
        </div>
        <a href="{{ URL::route('editor.supplier.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- /.col -->
    @actionEnd

    {{-- @actionStart('purchase', 'read')
    <div class="col-md-3 col-sm-6 col-xs-12">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
          <h3>{{$purchases->count()}}</h3>
          <p>PURCHASES</p>
        </div>
        <div class="icon">
            <i class="fa fa-shopping-cart"></i>
        </div>
        <a href="{{ URL::route('editor.purchase.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- /.col -->
    @actionEnd --}}
  </div> 


  <h3>Production</h3>
  <!-- Info boxes -->
  <div class="row">

    {{-- @actionStart('variable', 'read')
    <div class="col-md-3 col-sm-6 col-xs-12">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>{{$variables->count()}}</h3>
          <p>VARIABLES</p>
        </div>
        <div class="icon">
          <i class="fa fa-dollar"></i>
        </div>
        <a href="{{ URL::route('editor.variable.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- /.col -->
    @actionEnd --}}

    @actionStart('fish', 'read')
    <div class="col-md-3 col-sm-6 col-xs-12">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
          <h3>{{$fishes->count()}}</h3>
          <p>FISHES</p>
        </div>
        <div class="icon">
          <i class="fa fa-cube"></i>
        </div>
        <a href="{{ URL::route('editor.fish.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- /.col -->
    @actionEnd

    @actionStart('production', 'read')
    <div class="col-md-3 col-sm-6 col-xs-12">
      <!-- small box -->
      <div class="small-box bg-blue">
        <div class="inner">
          <h3>{{$productions->count()}}</h3>
          <p>PRODUCTS</p>
        </div>
        <div class="icon">
          <i class="fa fa-cubes"></i>
        </div>
        <a href="{{ URL::route('editor.production.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- /.col -->
    @actionEnd

    @actionStart('reject_reason', 'read')
    <div class="col-md-3 col-sm-6 col-xs-12">
      <!-- small box -->
      <div class="small-box bg-red">
        <div class="inner">
          <h3>{{$reject_reasons->count()}}</h3>
          <p>REJECT REASONS</p>
        </div>
        <div class="icon">
          <i class="fa fa-ban"></i>
        </div>
        <a href="{{ URL::route('editor.reject_reason.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- /.col -->
    @actionEnd
  </div>

  <h3>Marketing</h3>
  <!-- Info boxes -->
  <div class="row">

    @actionStart('export', 'read')
    <div class="col-md-3 col-sm-6 col-xs-12">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>EXPORT</h3><br/>
        </div>
        <div class="icon">
          <i class="fa fa-share-alt-square"></i>
        </div>
        <a href="{{ URL::route('editor.cart.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- /.col -->
    @actionEnd

    @actionStart('production_export', 'read')
    <div class="col-md-3 col-sm-6 col-xs-12">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
        <h3>EXPORTED LIST</h3><br/>
        </div>
        <div class="icon">
          <i class="fa fa-dollar"></i>
        </div>
        <a href="{{ URL::route('editor.production_export.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- /.col -->
    @actionEnd

    @actionStart('customer', 'read')
    <div class="col-md-3 col-sm-6 col-xs-12">
      <!-- small box -->
      <div class="small-box bg-blue">
        <div class="inner">
          <h3>CUSTOMER</h3><br/>
        </div>
        <div class="icon">
          <i class="fa fa-suitcase"></i>
        </div>
        <a href="{{ URL::route('editor.customer.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- /.col -->
    @actionEnd

     @actionStart('equipment', 'read')
    <div class="col-md-3 col-sm-6 col-xs-12">
      <!-- small box -->
      <div class="small-box bg-blue">
        <div class="inner">
          <h3>EQUIPMENT</h3><br/>
        </div>
        <div class="icon">
          <i class="fa fa-cab"></i>
        </div>
        <a href="{{ URL::route('editor.equipment.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- /.col -->
    @actionEnd

     @actionStart('consummable', 'read')
    <div class="col-md-3 col-sm-6 col-xs-12">
      <!-- small box -->
      <div class="small-box bg-blue">
        <div class="inner">
          <h3>CONSUMMABLE</h3><br/>
        </div>
        <div class="icon">
          <i class="fa fa-cab"></i>
        </div>
        <a href="{{ URL::route('editor.consummable.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- /.col -->
    @actionEnd

     @actionStart('packaging', 'read')
    <div class="col-md-3 col-sm-6 col-xs-12">
      <!-- small box -->
      <div class="small-box bg-blue">
        <div class="inner">
          <h3>PACKAGING</h3><br/>
        </div>
        <div class="icon">
          <i class="fa fa-cab"></i>
        </div>
        <a href="{{ URL::route('editor.packaging.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- /.col -->
    @actionEnd



  </div>

  <h3>Higher Authority</h3>
  <!-- Info boxes -->
  <div class="row">

    @actionStart('rnd', 'read')
    <div class="col-md-3 col-sm-6 col-xs-12">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>RnD</h3><br/>
        </div>
        <div class="icon">
          <i class="fa fa-dollar"></i>
        </div>
        <a href="{{ URL::route('editor.rnd.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- /.col -->
    @actionEnd
  </div>

  <h3>Asset</h3>
  <!-- Info boxes -->
  <div class="row">
    @actionStart('placement_storage', 'read')
    <div class="col-md-3 col-sm-6 col-xs-12">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
          <h3>STORAGE LIST</h3><br/>
        </div>
        <div class="icon">
          <i class="fa fa-dollar"></i>
        </div>
        <a href="{{ URL::route('editor.placement_storage.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- /.col -->
    @actionEnd
  
    @actionStart('storage', 'read')
    <div class="col-md-3 col-sm-6 col-xs-12">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>STORAGE ITEMS</h3><br/>
        </div>
        <div class="icon">
          <i class="fa fa-dollar"></i>
        </div>
        <a href="{{ URL::route('editor.storage.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- /.col -->
    @actionEnd
  </div>

  <h3>Item</h3>
  <!-- Info boxes -->
  <div class="row">
    @actionStart('category_item', 'read')
    <div class="col-md-3 col-sm-6 col-xs-12">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>CATEGORY <br/>ITEM</h3>
        </div>
        <div class="icon">
          <i class="fa fa-reorder"></i>
        </div>
        <a href="{{ URL::route('editor.category_item.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- /.col -->
    @actionEnd


    @actionStart('type_item', 'read')
    <div class="col-md-3 col-sm-6 col-xs-12">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
          <h3>TYPE <br/>ITEM</h3>
        </div>
        <div class="icon">
          <i class="fa fa-sticky-note"></i>
        </div>
        <a href="{{ URL::route('editor.type_item.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- /.col -->
    @actionEnd

    @actionStart('master_item', 'read')
    <div class="col-md-3 col-sm-6 col-xs-12">
      <!-- small box -->
      <div class="small-box bg-blue">
        <div class="inner">
          <h3>MASTER <br/>ITEM</h3>
        </div>
        <div class="icon">
          <i class="fa fa-television"></i>
        </div>
        <a href="{{ URL::route('editor.master_item.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- /.col -->
    @actionEnd

    @actionStart('unit', 'read')
    <div class="col-md-3 col-sm-6 col-xs-12">
      <!-- small box -->
      <div class="small-box bg-red">
        <div class="inner">
          <h3>UNIT <br/>ITEM</h3>
        </div>
        <div class="icon">
          <i class="fa fa-sitemap"></i>
        </div>
        <a href="{{ URL::route('editor.unit.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- /.col -->
    @actionEnd

    @actionStart('item', 'read')
    <div class="col-md-3 col-sm-6 col-xs-12">
      <!-- small box -->
      <div class="small-box bg-red">
        <div class="inner">
          <h3>ITEM<br/> DETAIL</h3>
        </div>
        <div class="icon">
          <i class="fa fa-server"></i>
        </div>
        <a href="{{ URL::route('editor.item.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- /.col -->
    @actionEnd

    @actionStart('custom_detail', 'read')
    <div class="col-md-3 col-sm-6 col-xs-12">
      <!-- small box -->
      <div class="small-box" style="background-color:brown">
        <div class="inner">
          <h3 style="color:white">CUSTOM<br/> DETAIL</h3>
        </div>
        <div class="icon">
          <i class="fa fa-navicon"></i>
        </div>
        <a href="{{ URL::route('editor.custom_detail.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- /.col -->
    @actionEnd

    @actionStart('conversion_item', 'read')
    <div class="col-md-3 col-sm-6 col-xs-12">
      <!-- small box -->
      <div class="small-box" style="background-color:purple">
        <div class="inner">
          <h3 style="color: white">CONVERSION <br/>ITEM</h3>
        </div>
        <div class="icon">
          <i class="fa fa-hourglass-half"></i>
        </div>
        <a href="{{ URL::route('editor.conversion_item.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- /.col -->
    @actionEnd

    @actionStart('unit', 'read')
    <div class="col-md-3 col-sm-6 col-xs-12">
      <!-- small box -->
      <div class="small-box" style="background-color:purple">
        <div class="inner">
          <h3 style="color: white">UNIT <br/>ITEM</h3>
        </div>
        <div class="icon">
          <i class="fa fa-hourglass-half"></i>
        </div>
        <a href="{{ URL::route('editor.unit.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- /.col -->
    @actionEnd

  </div>
  <h3>User Management</h3>
  <!-- Info boxes -->
  <div class="row">
    @actionStart('user', 'read')
    <div class="col-md-3 col-sm-6 col-xs-12">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>USER <br/>LIST</h3>
        </div>
        <div class="icon">
          <i class="fa fa-group"></i>
        </div>
        <a href="{{ URL::route('editor.user.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- /.col -->
    @actionEnd

    @actionStart('role', 'read')
    <div class="col-md-3 col-sm-6 col-xs-12">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
          <h3>ROLE <br/>LIST</h3>
        </div>
        <div class="icon">
          <i class="fa fa-unlock"></i>
        </div>
        <a href="{{ URL::route('editor.role.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- /.col -->
    @actionEnd

    @actionStart('module', 'read')
    <div class="col-md-3 col-sm-6 col-xs-12">
      <!-- small box -->
      <div class="small-box bg-blue">
        <div class="inner">
          <h3>MODULE <br/>LIST</h3>
        </div>
        <div class="icon">
          <i class="fa fa-tachometer"></i>
        </div>
        <a href="{{ URL::route('editor.module.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- /.col -->
    @actionEnd

    @actionStart('action', 'read')
    <div class="col-md-3 col-sm-6 col-xs-12">
      <!-- small box -->
      <div class="small-box bg-red">
        <div class="inner">
          <h3>ACTION <br/>LIST</h3>
        </div>
        <div class="icon">
          <i class="fa fa-send"></i>
        </div>
        <a href="{{ URL::route('editor.action.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- /.col -->
    @actionEnd

  </div>

  <h3>Utilities</h3>
  <!-- Info boxes -->
  <div class="row">
    @actionStart('item_migration', 'read')
    <div class="col-md-3 col-sm-6 col-xs-12">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>ITEM <br/>MIGRATION</h3>
        </div>
        <div class="icon">
          <i class="fa fa-plane"></i>
        </div>
        <a href="{{ URL::route('editor.item_migration.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- /.col -->
    @actionEnd


  </div>

</section>
<!-- /.content -->

@stop