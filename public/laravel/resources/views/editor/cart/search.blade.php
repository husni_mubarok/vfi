@extends('layouts.editor.template')
@section('content')
<style type="text/css">
  #myModal .modal-dialog
  {
    width: 90%;
  }

  #myModalEdit .modal-dialog
  {
    width: 50%;
  }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li class="active"><a href="{{ URL::route('editor.cart.index') }}"><i class="fa fa-cube"></i> Cart Export</a></li>
  </ol>
</section>
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-5">
			        <div class="x_panel">
		                <h2>
		                	@if(isset($cart))
		                	<i class="fa fa-pencil"></i>
		                	@else
		                	<i class="fa fa-plus"></i> 
		                	@endif
		                	<i class="fa fa-cube"></i> Cart Export
	                	</h2>

		                <hr>
			               <div class="x_content">
  			                @include('errors.error')
  			                {!! Form::model($cart, array('route' => ['editor.cart.export', $cart->id], 'method' => 'PUT'))!!}
  		                    {{ csrf_field() }}
                     </div>
		             
                          {{ Form::label('doc_code', 'Doc Code (auto)') }}
                          {{ Form::text('doc_code', old('doc_code'), array('class' => 'form-control', 'placeholder' => 'Doc Code*', 'required' => 'true', 'disabled')) }}
                          {{ Form::label('date', 'Date') }}
                          {{ Form::text('date', old('date'), array('class' => 'form-control', 'placeholder' => 'Date & Time*', 'required' => 'true', 'id' => 'date', 'disabled')) }}
                          <hr/>

                          <h4>Customer Detail</h4>
                          <hr/>
                           
                          {{ Form::label('Customer') }}
                          {{ Form::select('id_customer', $customer_list, old('id_customer'), array('class' => 'form-control', 'required' => 'true', 'disabled')) }}<br/>

                          {{ Form::label('customer_pono', 'PO No') }}
                          {{ Form::text('customer_pono', old('customer_pono'), array('class' => 'form-control', 'placeholder' => 'PO No', 'id' => 'customer_pono', 'disabled')) }}

                          {{ Form::label('customer_podate', 'PO Date') }}
                          {{ Form::text('customer_podate', old('customer_podate'), array('class' => 'form-control', 'placeholder' => 'PO Date', 'required' => 'true', 'id' => 'customer_podate', 'disabled')) }}{{-- 

                          {{ Form::label('driver', 'Driver') }}
                          {{ Form::text('driver', old('driver'), array('class' => 'form-control', 'placeholder' => 'Driver', 'id' => 'driver', 'disabled')) }} --}}


                          {{-- {{ Form::label('customer', 'Customer') }}
                          {{ Form::text('customer', old('customer'), array('class' => 'form-control', 'placeholder' => 'Customer', 'id' => 'customer', 'disabled')) }}

                          {{ Form::label('address', 'Address') }}
                          {{ Form::text('address', old('address'), array('class' => 'form-control', 'placeholder' => 'Address', 'id' => 'address', 'disabled')) }} --}}
                          <br/>
	                    </div>
	                        {!! Form::close() !!}
			            </div>
			        
                  <div class="col-md-12 col-sm-12 col-xs-12">          
                      <table class="table table-bordered" id="Cart_table">
                          <thead>
                                  <tr>
                                  <th>#</th>
                                  <th>Name</th>
                                  <th>Criteria</th>
                                  <th>Size</th>
                                  <th>UoM</th>
                                  <th>Weight</th>
                                  <th>Qty</th>
                          </tr>
                          </thead>
                          <tbody>
                              @foreach($cartdets as $key => $cartdet)
                                  <tr>
                                          <td>{{$key+1}}</td>
                                          <td>{{$cartdet->name}}</td>
                                          <td>{{$cartdet->criteria}}</td>
                                          <td>{{$cartdet->size}}</td>
                                          <td>{{$cartdet->block}}</td>
                                          <td>{{$cartdet->weight}}</td>
                                          <td>{{$cartdet->quantity}}</td>
                                      </tr>
                              @endforeach
                            </tbody>
                    </table> 
                    <a href="{{ URL::route('editor.cart.pdf', $cartdet->id_cart) }}" class="btn btn-default  btn-sm pull-right"><i class="fa fa-file-pdf-o"></i>&nbsp;PDF</a>
                    <a href="{{ URL::route('editor.cart.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a></td>
                 </div> 
             </div>   
		        
		    </div>

		</div>
	</section>
</section>

@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script>
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function () {
    $("#Cart_table").DataTable(
      {
        "scrollX": true
      });
    });
</script>
<script>
$(document).ready(function () {
	$('#dateprod').datetimepicker({
		sideBySide: true,
		format: 'YYYY-MM-DD HH:mm',
	});
});

$(document).ready(function(){
  $("#productionResult").DataTable({
    "pageLength": 3
  });

  $('#datetime').datetimepicker({
		sideBySide: true,
		format: 'YYYY-MM-DD HH:mm',
  });

});
</script>
@stop