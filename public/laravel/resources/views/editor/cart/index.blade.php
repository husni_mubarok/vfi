@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System </small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li class="active"><a href="#"><i class="fa fa-cubes"></i> Export</a></li>
  </ol>
</section>
@actionStart('export', 'read')
<section class="content">
  <section class="content box box-solid">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-1"></div>
        <div class="col-md-12">
          <div class="x_panel">
            <h2>
              <i class="fa fa-cubes"></i> Export List    
              @actionStart('export', 'create')              
              <a href="{{ URL::route('editor.cart.create') }}" class="btn btn-primary btn-lg pull-right"><i class="fa fa-plus"></i> Add</a>
              @actionEnd
            </h2>
            <hr>
            <form method="POST" id="search-form" class="form-inline" role="form">
              <div class="form-group">
                <input type="text" class="form-control" name="datefrom" id="datefrom" placeholder="Date From">
              </div>
              <div class="form-group">
                <label> &nbsp-&nbsp</label>
                <input type="text" class="form-control" name="dateto" id="dateto" placeholder="Date To">
              </div>
              <a href="javascript:void(0)" class="btn btn-primary" id="searchbtn">Search</a>
            </form>
            <hr>
            <div class="x_content">
              <table id="cart-table" class="table table-condensed">
                <thead>
                  <tr>
                    <th></th>
                    <th>Doc No</th>
                    <th>Date</th>
                    <th>Customer</th>
                    <th>Process</th>
                    <th>Status</th>
                    <th>Delete</th>
                    <th>Action</th>
                     @actionStart('cart', 'approve') 
                    <th>Process</th>
                     @actionEnd
                  </tr>
                </thead>
              </table>
              <script id="details-template" type="text/x-handlebars-template">
                {{-- <div class="label label-info">Doc Code @{{doc_code}}</div> --}}
                <table class="table details" id="posts-@{{id}}">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Criteria</th>
                      <th>Size</th>
                      <th>UoM</th>
                      <th>Weight</th>
                      <th>Qty</th>
                    </tr>
                  </thead>
                </table>
              </script>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</section>
@actionEnd
@stop
@section('scripts')

<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script> 

@stack('scripts')
<script>
  $(document).ready(function () {
    $("#cart_table").DataTable(
    {
      "scrollX": true
    });
  });
</script>
<script>
  $(document).ready(function () {
   $('#datefrom').datetimepicker({
    sideBySide: true,
    format: 'YYYY-MM-DD',
  });
   $('#dateto').datetimepicker({
    sideBySide: true,
    format: 'YYYY-MM-DD',
  });
 });
</script>
<script>
  $(".delete").on("submit", function(){
    return confirm("Do you want to delete this item?");
  });

</script>
<script>
  $(document).ready(function () {
   var template = Handlebars.compile($("#details-template").html());
    //console.log(Handlebars.compile);
    console.log(template);
    var table = $("#cart-table").DataTable({
      processing: true,
      serverSide: true,
      "pageLength": 25,
      "scrollY": "400px",
      ajax: {
        url: '{{ url('editor/data') }}',
        data: function (d) {
          d.datefrom = $('input[name=datefrom]').val();
          d.dateto = $('input[name=dateto]').val();
        }
      },
      columns: [
      {
        "className": 'details-control',
        "orderable": false,
        "searchable": false,
        "data": null,
        "defaultContent": ''
      },
      { data: 'doc_code', name: 'doc_code', 'searchable': true, 'orderable': true },
      { data: 'date', name: 'date', 'searchable': true, 'orderable': true },
      { data: 'customer', name: 'customer', 'searchable': true, 'orderable': true },
      { data: 'action', 'searchable': false, 'orderable':false },
      { data: 'dnstatus', 'searchable': false, 'orderable':false },
      { data: 'actiondl', 'searchable': false, 'orderable':false },
      { data: 'actioned', 'searchable': false, 'orderable':false },
      @actionStart('cart', 'approve') 
      { data: 'process', 'searchable': false, 'orderable':false }
       @actionEnd
      ]
    });
// Add event listener for opening and closing details
    //$('#cart-table').on('click', 'td.details-control', function () {
     // $('#cart-table tbody').on('click', 'tbody > tr > td', function (e){
        //$('.cart-table tbody > tbody > tr').click(function() {
      $('#cart-table tbody').on( 'click', 'tr td.details-control', function () {
        //alert("test");
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var tableId = 'posts-' + row.data().id;

        //console.log(tableId);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass( 'details' );
          } else {
            // Open this row
            row.child(template(row.data())).show();
            initTable(tableId, row.data());
            tr.addClass( 'details' );
            tr.next().find('td').addClass('no-padding bg-gray');
          }
        });

          function initTable(tableId, data) {
            $('#' + tableId).DataTable({
              processing: true,
              serverSide: true,
              ajax: data.details_url,
              columns: [
              { data: 'name', name: 'name', 'searchable': true, 'orderable':true  },
              { data: 'criteria', name: 'criteria', 'searchable': true, 'orderable':true  },
              { data: 'size', name: 'size', 'searchable': true, 'orderable':true  },
              { data: 'block', name: 'block', 'searchable': true, 'orderable':true  },
              { data: 'weight', name: 'weight', 'searchable': true, 'orderable':true  },
              { data: 'quantity', name: 'quantity', 'searchable': true, 'orderable':true  }
              ]
            })
          }

          $('#searchbtn').click(function() {
            table.draw();
            e.stopImmediatePropagation();
          }); 
        });

      </script>
      @stop