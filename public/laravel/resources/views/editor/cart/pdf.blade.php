<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title></title>
</head>
<body>
<style>
@page { margin: 0px; }
body { margin: 15px; }

body{font-family:  Arial, 'Helvetica' ; font-size:10px;}
h1,h2,h3,h4,p{margin:0;}
table.thin,table.thin tr td { border-collapse: collapse; border:1px solid gray; border-color: gray;border-width:thin;}
table.thin2, table.thin2 tr td {  border-collapse: collapse; border:1px solid black; border-width:thin;}
</style>
<table
 style="text-align: left; width: 100%; border-collapse: collapse; font-family: Arial;"
 border="0" cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
      <td align="left" width="180px">
        <img src="" width="120px">
      </td>
      <td align="center">
        <h1 style="font-size: 18px;margin:0">DELIVERY ORDER</h1>
      </td>
      <td width="250px">
      <b>PT VESSEL FRESHFISH INDOMAKMUR</b><br>
      Jl. Industri Raya II Blok J No. 5 Jatake,Tanggerang Banten<br/>
      Phone : 021-593049502 / 021-59015383<br>
      marketing@vesselfreshfish.com<br>
      <b>www.vesselfreshfish.com</b>

    </td>
    </tr>
  </tbody>
</table>
<br>
<table class="thin" style="font-family: Arial; margin-bottom: 0px;" width="100%"
 border="1" cellpadding="5" cellspacing="2">
  <tbody>
    <tr>
      <td width="25%">
        <h4>CUSTOMER P.O. NUMBER : </h4>
      </td>
      <td width="25%">{{$cart->customer_pono}}</td>
      <td width="25%">
        <h4>CUSTOMER P.O. DATE : </h4>
      </td>
      <td width="25%">{{date("d-m-Y", strtotime($cart->customer_podate))}}</td>
    </tr>
    <tr  style="background-color: #eaeaea;">
      <td style="vertical-align: top" width="50%" colspan="2">
        <h4>DELIVERED TO :</h4>
        <p>
          {{$cart->customer}}, <br/>
          {{$cart->address}}
        </p>
      </td>
      <td >
        <b>DELIVERED BY :</b>&nbsp;VFI Delivery<br>
      </td>
      <td><b>Driver :</b>&nbsp;{{$cart->driver}}</td>
    </tr>
    <tr>
      <td>
        <h4>D.O. NUMBER : </h4>
      </td>
      <td>{{$cart->doc_code}}</td>
      <td>
        <h4>D.O. DATE :</h4>
      </td>
      <td>{{date("d-m-Y", strtotime($cart->date))}}</td>
    </tr>
  </tbody>
</table>
<br>
<table
 style="text-align: left; width: 100%; clear: both; font-family: Arial;"
 border="1" cellpadding="5" cellspacing="2" class="thin2" >
  <tbody>
    <tr>
      <td style="background-color: rgb(163, 204, 245); font-weight: bold;text-align:center">#</td>
      <td style="background-color: rgb(163, 204, 245); font-weight: bold;">Name</td>
      <td style="background-color: rgb(163, 204, 245); font-weight: bold;">Criteria</td>
      <td style="background-color: rgb(163, 204, 245); font-weight: bold;text-align:center">Size</td>
      <td style="background-color: rgb(163, 204, 245); font-weight: bold;text-align:center">Block</td>
      <td style="background-color: rgb(163, 204, 245); font-weight: bold;text-align:center">Weight (Kg)</td>
      <td style="background-color: rgb(163, 204, 245); font-weight: bold;text-align:center">Qty</td>
    </tr>
    @foreach($cartdets as $key => $cartdet)
    <tr>
      <td style="text-align:center">{{$key+1}}</td>
      <td>{{$cartdet->name}}</td>
      <td>{{$cartdet->criteria}}</td>
      <td style="text-align:center">{{$cartdet->size}}</td>
      <td style="text-align:center">{{$cartdet->block}}</td>
      <td style="text-align:center">{{$cartdet->weight}}</td>
      <td style="text-align:center">{{$cartdet->quantity}}</td>
    </tr>
    @endforeach
  </tbody>



  <tfoot>
    <tr>
      <td style="text-align:right;font-weight: bold;background-color: #eaeaea;" colspan="5" >TOTAL</td>
      <td style="text-align:center;font-weight: bold;background-color: #eaeaea">{{$cartdetstwg}}</td>
      <td style="text-align:center;font-weight: bold;background-color: #eaeaea">{{$cartdetstqty}}</td>
    </tr>
  </tfoot>
</table>


<br style="font-family: Arial;">
<span style="font-family: Arial;">IMPORTANT</span>:<br
 style="font-family: Arial;">
<ul style="font-family: Arial;">
  {{-- <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
  tempor incididunt ut labore et dolore magna aliqua. </li> --}}
  
</ul>
<br>
<table style="text-align: left; width: 100%; font-family: Arial; margin-top: 0px;"
 border="0" cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
      <td style="width: 33%;text-align:center;font-weight:bold">Create by</td>
      <td style="width: 33%;text-align:center;font-weight:bold">Courier</td>
      <td style="width: 33%;text-align:center;font-weight:bold">Received by :</td>
    </tr>
    <tr>
      <td style="height: 50px;"></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td style="text-align:center">Name</td>
      <td style="text-align:center">Name</td>
      <td style="text-align:center">Name</td>
    </tr>
  </tbody>
</table>

</body>
</html>
