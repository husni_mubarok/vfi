@extends('layouts.home.template')
@section('content')
	<section class="content box box-solid">
		<div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
             <div class="col-md-1"></div>
             <div class="col-md-12">
                 <div class="x_panel">
                  <h2>
                     <i class="fa fa-cubes"></i> RnD List
                 </h2>
                 <hr>
                 <div class="x_content">
                     <table class="table table-bordered" id="export_table">
                        <thead>
                            <tr>
                                <th>Detail</th>
                                <th>Doc No</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                                    {{-- <tbody>
                                    @foreach($rnds as $key => $rnd)
                                        <tr>
                                                <td>{{$key+1}}</td>
                                                <td>{{$rnd->doc_code}}</td>
                                                <td>{{$rnd->date}}</td>
                                                @if($production_export->cart->status == 2)
                                                    <td><p style="color: green"> Export </p></td>    
                                                @else 
                                                     <td><p style="color: red"> R n D </p></td>
                                                @endif
                                                <td align="center">
                                                    <div class="col-md-2 nopadding">
                                                            <a href="{{ URL::route('editor.rnd.edit', [$rnd->id]) }}" class="btn btn-default btn-sm"><i class="fa fa-search"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                    @endforeach
                                </tbody> --}}
                            </table>
                            <script id="details-template" type="text/x-handlebars-template">
                                {{--  <div class="label label-info">Name @{{id}}</div>  --}}
                                <table class="table details" id="posts-@{{id}}">
                                  <thead>
                                    <tr>
                                      <th>Name</th>
                                      <th>Criteria</th>
                                      <th>Size</th>
                                      <th>Block</th>
                                      <th>Qty</th>
                                  </tr>
                              </thead>
                          </table>
                      </script>
                  </div>
              </div>
          </div>
      </div>
  </div>
</section>
</section>

@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>

<script>
    $(function() {
        var template = Handlebars.compile($("#details-template").html());
        var table = $('#export_table').DataTable({
          processing: true,
          serverSide: true,
          "pageLength": 25,
          "scrollY": "400px",
          ajax: {
            url: '{{ url('editor/rnd/data') }}'
        },
        columns: [
        {
            "className": 'details-control',
            "orderable": false,
            "searchable": false,
            "data": null,
            "defaultContent": ''
        },
        { data: 'doc_code', name: 'doc_code'},
        { data: 'date', name: 'date'}
        ],
        order: [[1, 'asc']]
    });

    // Add event listener for opening and closing details
    $('#export_table tbody').on( 'click', 'tr td.details-control', function () {
        //alert("test");
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var tableId = 'posts-' + row.data().id;

        //console.log(tableId);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass( 'details' );
        } else {
            // Open this row
            row.child(template(row.data())).show();
            initTable(tableId, row.data());
            tr.addClass( 'details' );
            tr.next().find('td').addClass('no-padding bg-gray');
        }
    });

    function initTable(tableId, data) {
        $('#' + tableId).DataTable({
          processing: true,
          serverSide: true,
          ajax: data.details_url,
          columns: [
          { data: 'name'},
          { data: 'criteria'},
          { data: 'size'},
          { data: 'block'},
          { data: 'quantity'}
          ]
      })
    }
});
</script>
@stop