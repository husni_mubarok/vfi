@extends('layouts.home.template')
@section('content')
<section class="content box box-solid">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
     <div class="col-md-1"></div>
     <div class="col-md-5">
       <div class="x_panel">
        <h2>
         @if(isset($rnd))
         <i class="fa fa-pencil"></i>
         @else
         <i class="fa fa-plus"></i> 
         @endif
         <i class="fa fa-cube"></i> RnD
       </h2>

       <hr>
       <div class="x_content">
         @include('errors.error')
         {!! Form::model($rnd, array('route' => ['editor.rnd.update', $rnd->id], 'method' => 'PUT'))!!}
         {{ csrf_field() }}
       </div>
       
       {{ Form::label('doc_code', 'Doc Code (auto)') }}
       {{ Form::text('doc_code', old('doc_code'), array('class' => 'form-control', 'placeholder' => 'Doc Code*', 'required' => 'true', 'disabled')) }}

       {{ Form::label('date', 'Date') }}
       {{ Form::text('date', old('date'), array('class' => 'form-control', 'placeholder' => 'Date & Time*', 'required' => 'true', 'id' => 'dateprod', 'disabled')) }}
       
       <hr/>
     </div>
   </div>
   
   <div class="col-md-12 col-sm-12 col-xs-12">          
    <table class="table table-bordered" id="export_table">
      <thead>
        <tr>
          <th>#</th>
          <th>Name</th>
          <th>Criteria</th>
          <th>Size</th>
          <th>Block</th>
          <th>Qty</th>
        </tr>
      </thead>
      <tbody>
        @foreach($rnddets as $key => $rnddet)
        <tr>
          <td>{{$key+1}}</td>
          <td>{{$rnddet->name}}</td>
          <td>{{$rnddet->criteria}}</td>
          <td>{{$rnddet->size}}</td>
          <td>{{$rnddet->block}}</td>
          <td>{{$rnddet->quantity}}</td>
        </tr>
        @endforeach
      </tbody>
    </table>  
    <a href="{{ URL::route('editor.rnd.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a></td>
  </div> 
</div>           
</div>
</div>
</section>

@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script>
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>

<script>
  $(document).ready(function () {
   $('#dateprod').datetimepicker({
    sideBySide: true,
    format: 'YYYY-MM-DD HH:mm',
  });
 });
</script>
@stop