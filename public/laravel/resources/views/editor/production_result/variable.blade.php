@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li><a href="{{ URL::route('editor.production_result.index') }}"><i class="fa fa-cogs"></i> Production Result</a></li>
    <li class="active"><a href="#"><i class="fa fa-dollar"></i> Waste Products</a></li>
  </ol>
</section>

<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-10">
			        <div class="x_panel">
		                <h2>
		                	@if(isset($production_result_variables))
		                	<i class="fa fa-pencil"></i>
		                	@else 
		                	<i class="fa fa-plus"></i>
		                	@endif
		                	<i class="fa fa-dollar"></i>&nbsp;Waste Products
	                	</h2>
		                <hr>
			            <div class="x_content">
			            	{{-- Waste Product Form --}}
						  	<div class="col-md-6">
				        		<h4>Waste Products</h4>
				          		@include('errors.error')
				          		{!! Form::open(array('route' => ['editor.production_result.store_waste', $production_result->id])) !!}
				          		<table class="table table-bordered table-hover">
				          		<thead>
						            <tr>
							            <th>Name</th>
							            <th>UoM</th>
							            <th>Type</th>
							            <th>Value</th>
				          			</tr>
					          	</thead>
					          	<tbody>
			          			@if(isset($production_result_variables))
				           		 	@foreach($production_result_variables as $key => $production_result_variable)
				            		@if($production_result_variable->variable->type == 'Reduction')
					            	<tr>
						              	<td>{{$production_result_variable->variable->name}}</td>
						              	<td>{{$production_result_variable->variable->uom}}</td>
						              	<td>{{$production_result_variable->variable->type}}</td>
						              	<td>
						                	<div class="input-group">
						                  		{{ Form::number('value['.$production_result_variable->variable->id.']', old('value['.$production_result_variable->variable->id.']', $production_result_variable->value), array('required' => 'true', 'min' => '0', 'step' => '0.01', 'class' => 'form-control')) }}
						                  		<div class="input-group-addon"><span>kg</span></div>
						                	</div>
						              	</td>
				            		</tr>
					            	@endif
					            	@endforeach
				          		@else
				            		@foreach($production_result->production->production_variable as $key => $production_variable)
				            		@if($production_variable->variable->type == 'Reduction')
					              	<tr>
					                	<td>{{$production_variable->variable->name}}</td>
					                	<td>{{$production_variable->variable->uom}}</td>
					                	<td>{{$production_variable->variable->type}}</td>
					                	<td>
						                  	<div class="input-group">
					                    		{{ Form::number('value['.$production_variable->variable->id.']', old('value['.$production_variable->variable->id.']'), array('required' => 'true', 'min' => '0', 'step' => '0.01', 'class' => 'form-control')) }}
					                    		<div class="input-group-addon"><span>kg</span></div>
					                	  	</div>
					                	</td>
					              	</tr>
					            	@endif
					            	@endforeach
				          		@endif
				          		</tbody>
				        		</table>
				        		<button type="button" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
				        		{!! Form::close() !!}
					      	</div>
					      	{{-- Waste Product Form --}}

				      		{{-- Production Details --}}
				      		<div class="col-md-6">
				        		<h4>Production Detail</h4>
				        		<table class="table table-bordered">
				          			<tr>
					            		<th>Product</th>
				            			<td>{{$production_result->production->name}}</td>
				          			</tr>
				          			<tr>
				            			<th>Weight</th>
				            			<td>{{number_format($production_result->weight, 2)}} kg</td>
				          			</tr>
				          			<tr>
				            			<th>Block Weight</th>
				            			<td>{{number_format($production_result->block_weight, 2)}} kg</td>
				          			</tr>
				          			<tr>
				            			<th>Production Date</th>
				            			<td>
				              				{{date("D, d M Y", strtotime($production_result->started_at))}}
				              				&nbsp;<i class="fa fa-long-arrow-right"></i>&nbsp;
				              				@if($production_result->finished_at == null)
				              				<i class="fa fa-question-circle-o"></i>
				              				@else
				              				{{date("D, d M Y", strtotime($production_result->finished_at))}} 
				              				@endif
				            			</td>
				          			</tr>
				          			<tr>
				            			<th>Criteria</th>
				            			<td>{{$production_result->criteria}}</td>
				          			</tr>
		        				</table>
				      		</div>
				      		{{-- Production Details --}}
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@stop