@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li class="active"><a href="{{ URL::route('editor.production_result.index') }}"><i class="fa fa-cogs"></i> Production Result</a></li>
  </ol>
</section>

<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-5">
			        <div class="x_panel">
		                <h2>
		                	@if(isset($production_result))
		                	<i class="fa fa-pencil"></i>
		                	@else
		                	<i class="fa fa-plus"></i> 
		                	@endif
		                	<i class="fa fa-cogs"></i> Production Result
	                	</h2>
		                <hr>
			            <div class="x_content">
			                @include('errors.error')
	                        @if(isset($production_result))
			                {!! Form::model($production_result, array('route' => ['editor.production_result.update', $production_result->id], 'method' => 'PUT'))!!}
		                    @else
		                    {!! Form::open(array('route' => 'editor.production_result.store'))!!}
		                    @endif
		                    {{ csrf_field() }}
		                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
		                        {{ Form::label('fish_id', 'Sea Product') }}
		                        @if(isset($production_result))
		                        <p>{{$production_result->production->fish->name}}</p>
		                        @else
		                        {{ Form::select('fish_id', $fishes, old('fish_id'), ['class' => 'form-control', 'placeholder' => 'Select a Sea Product']) }}
		                        @endif
		                        <br/>

		                        {{-- {{ Form::label('purchase_id', 'Purchase') }}
		                        {{ Form::select('purchase_id', $purchases, old('purchase_id'), ['class' => 'form-control', 'placeholder' => 'Select a Purchase']) }}<br/> --}}

		                        {{ Form::label('production_id', 'Production') }}
		                        @if(isset($production_result))
		                        <p>{{$production_result->production->name}}</p>
		                        @else
		                        <select name="production_id" id="production_id" class="form-control" disabled>
		                        </select>
		                        @endif
		                        <br/>

								{{ Form::label('weight', 'Weight') }}
								<div class="input-group">
		                        	{{ Form::number('weight', old('weight'), array('class' => 'form-control', 'placeholder' => 'Weight*', 'required' => 'true', 'min' => '0', 'step' => '0.01', 'id' => 'weight')) }}
			                        <div class="input-group-addon">
			                        	<span>kg</span>
			                        </div>
		                        </div><br/>

		                        {{ Form::label('block_weight', 'Block Weight') }}
								<div class="input-group">
		                        	{{ Form::number('block_weight', old('block_weight'), array('class' => 'form-control', 'placeholder' => 'Block Weight*', 'required' => 'true', 'min' => '0', 'step' => '0.01', 'id' => 'block_weight')) }}
			                        <div class="input-group-addon">
			                        	<span>kg</span>
			                        </div>
		                        </div><br/>

		                        {{ Form::label('mst_carton', 'Master Carton') }}
								<div class="input-group">
		                        	{{ Form::number('mst_carton', old('mst_carton'), array('class' => 'form-control', 'placeholder' => 'Master Carton*', 'required' => 'true', 'min' => '1', 'step' => '1', 'id' => 'mst_carton')) }}
			                        <div class="input-group-addon">
			                        	<span>packs</span>
			                        </div>
		                        </div><br/>

		                        {{ Form::label('started_at', 'Start Date') }}
		                        {{ Form::text('started_at', old('started_at'), array('class' => 'form-control', 'placeholder' => 'Date & Time*', 'required' => 'true', 'id' => 'started_at')) }}<br/>

		                        {{ Form::label('criteria', 'Criteria') }}
		                        {{ Form::text('criteria', old('criteria'), array('class' => 'form-control', 'placeholder' => 'Criteria',)) }}<br/>

	                            <button type="submit" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
	                    	</div>
	                        {!! Form::close() !!}
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@stop

@section('scripts')
<script>
$(document).ready(function () {
	$('#started_at').datetimepicker({
		sideBySide: true,
		format: 'YYYY-MM-DD',
	});
});
</script>
<script>
$("#fish_id").on('change', function()
{
    $.ajax({
        url : '{{ URL::route('get.production') }}',
        data : {'fish_id':$("#fish_id").val()},
        type : 'GET',
        headers : {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
        success : function(data, textStatus, jqXHR){
            $('#production_id').empty();
            jQuery.each(data, function(i, val)
            {
            	$('#production_id').append($('<option>', { 
			        value: val.id,
			        text : val.name 
			    }));
            });
            $("#production_id").attr('disabled', false);
        },
        error : function()
        {
        	$('#production_id').empty();
        	$('#production_id').attr('disabled', true);
        },
    })

    // $.ajax({
    //     url : '{{ URL::route('get.production') }}',
    //     data : {'purchase_id':$("#purchase_id").val()},
    //     type : 'GET',
    //     headers : {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
    //     success : function(data, textStatus, jqXHR){
    //     	console.log(data);
    //         $('#production_id').empty();
    //         jQuery.each(data['product'], function(i, val)
    //         {
    //         	$('#production_id').append($('<option>', { 
			 //        value: i,
			 //        text : val['name'] 
			 //    }));
    //         });
    //         $("#production_id").attr('disabled', false);
    //         $("#weight").val(data['weight']);
    //     },
    //     error : function()
    //     {
    //     	$('#production_id').empty();
    //     	$('#production_id').attr('disabled', true);
    //     },
    // })
});
</script>
@stop