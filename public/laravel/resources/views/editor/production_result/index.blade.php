@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li class="active"><a href="#"><i class="fa fa-cogs"></i> Production Result</a></li>
  </ol>
</section>

<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-10">
			        <div class="x_panel">
		                <h2>
		                	<i class="fa fa-cogs"></i> Production Result List
		                	<a href="{{ URL::route('editor.production_result.create') }}" class="btn btn-default btn-lg pull-right"><i class="fa fa-plus"></i> Add</a>
	                	</h2>
		                <hr>
			            <div class="x_content">
			                <table class="table table-hover" id="production_result_table">
				                <thead>
				                	<tr>
				                		<th>#</th>
				                		<th>Purchase</th>
				                		<th>Production</th>
				                		<th>Weight (kg)</th>
				                		<th>Criteria</th>
				                		<th>Waste</th>
				                		<th>Result</th>
				                		<th>Additional</th>
				                		<th>Action</th>
				                	</tr>
				                </thead>
				                <tbody>
				                	@foreach($production_results as $key => $result)
									<tr>
				                		<td>{{$key+1}}</td>
				                		<td>
				                			@if($result->purchase)
				                			{{$result->purchase->description}}
				                			<br>
				                			<?php 
				                			$total_price = 0;
			                				foreach($result->purchase->purchase_detail as $purchase_detail)
			                				{
			                					$total_price += $purchase_detail->price * $purchase_detail->amount;
			                				}
				                			?>
				                			[{{ $result->purchase->currency }} {{number_format($total_price, 2)}}]
				                			@endif
				                		</td>
				                		<td>{{$result->production->name}}</td>
				                		<td>{{number_format($result->weight, 2)}}</td>
				                		<td>{{$result->criteria}}</td>
				                		<td>
				                			@if($result->status == 0)
				                			<a href="{{ URL::route('editor.production_result.create_waste', [$result->id]) }}" class="btn btn-sm btn-primary">
				                			<i class="fa fa-plus"></i> <i class="fa fa-minus"></i></a><br>
				                			@else
				                			@if($result->production_result_variable->count() > 0)
				                			<table class="table table-bordered">
				                			<thead>
				                			<tr>
				                				<th>Part</th>
				                				<th>Weight (kg)</th>
				                			</tr>
				                			</thead>
				                			<tbody>
				                			@foreach($result->production_result_variable as $variable)
				                			<tr>
				                				<td>{{$variable->variable->name}}</td>
				                				<td>{{number_format($variable->value, 2)}}</td>
				                			</tr>
				                			@endforeach
				                			</tbody>
				                			</table>
				                			@else
				                			No Waste Product
				                			@endif
				                			@endif
			                			</td>
				                		<td>
				                			@if($result->status == 1)
				                			<a href="{{ URL::route('editor.production_result.create_result', [$result->id]) }}" class="btn btn-sm btn-primary">
				                			<i class="fa fa-plus"></i> <i class="fa fa-dropbox"></i></a>
				                			@else
				                			@if($result->production_result_detail->count() > 0)
				                			<table class="table table-bordered">
				                			<thead>
				                			<tr>
				                				<th>Size</th>
				                				<th>Block</th>
				                			</tr>
				                			</thead>
				                			<tbody>
				                			@foreach($result->production_result_detail as $detail)
				                			<tr>
				                				<td>{{$detail->size}}</td>
				                				<td>{{$detail->block}}</td>
				                			</tr>
				                			@endforeach
				                			</tbody>
				                			</table>
				                			@else
				                			No Result Product
				                			@endif
				                			@endif
				                		</td>
				                		<td>
				                			@if($result->status == 2)
											<a href="{{ URL::route('editor.production_result.create_reject', $result->id) }}" class="btn-sm btn-primary">
											<i class="fa fa-plus"></i> <i class="fa fa-plus-square-o"></i></a>
											@else
											@if($result->production_result_addition->count() > 0)
				                			<table class="table table-bordered">
				                			<thead>
				                			<tr>
				                				<th>Size</th>
				                				<th>Block</th>
				                			</tr>
				                			</thead>
				                			<tbody>
				                			@foreach($result->production_result_addition as $addition)
			                				<tr>
			                					<td>{{$addition->size}}</td>
			                					<td>{{$addition->block}}</td>
			                				</tr>
			                				@endforeach
			                				</tbody>
				                			</table>
				                			@else
				                			No Additional Product
											@endif
											@endif
			                			</td>
				                		<td>
				                			@if($result->status == 4)
				                			<a href="{{ URL::route('editor.production_result.edit_waste', [$result->id]) }}" class="btn btn-sm btn-primary">
				                			<i class="fa fa-pencil"></i> <i class="fa fa-minus"></i></a><br>

				                			<a href="{{ URL::route('editor.production_result.edit_reject', [$result->id]) }}" class="btn btn-sm btn-primary">
				                			<i class="fa fa-pencil"></i><i class="fa fa-plus-square-o"></i></a><br>

				                			<a href="{{ URL::route('editor.production_result.edit_result', [$result->id]) }}" class="btn btn-sm btn-primary">
				                			<i class="fa fa-pencil"></i><i class="fa fa-dropbox"></i></a><br>
				                			@endif

				                			<a href="{{ URL::route('editor.production_result.summary', [$result->id]) }}" class="btn btn-md btn-primary">
				                			<i class="fa fa-file-text-o"></i></a>

				                			{!! Form::open(['route' => ['editor.production_result.delete', $result->id], 'method' => 'DELETE']) !!}
				                			<button type="submit" class="btn btn-danger" onclick="return confirm('Yakin hapus data?')"><i class="fa fa-trash"></i></button>
				                			{!! Form::close() !!}
				                		</td>
			                		</tr>
				                	@endforeach
				                </tbody>
			                </table>
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>

@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function () {
    $("#production_result_table").DataTable(
    	{
    		"scrollX": true
    	});
    });
</script>
@stop