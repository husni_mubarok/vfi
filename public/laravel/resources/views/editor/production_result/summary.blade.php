@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li><a href="{{ URL::route('editor.production_result.index') }}"><i class="fa fa-cogs"></i> Production Result</a></li>
    <li><a href="#"><i class="fa fa-file-text-o"></i> Summary</a></li>
  </ol>
</section>

<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-5">
			        <div class="x_panel">
		                <h2> 
		                	<i class="fa fa-file-text-o"></i> Production Result
	                	</h2>
		                <hr>
			            <div class="x_content">
			                <div class="col-md-12">
			                	@php
			                	$total_waste = 0;
			                	$total_addition = 0;
			                	// $total_reject = 0;
			                	$total_ready = 0;
			                	$total_miss = 0;
			                	foreach($production_result->production_result_variable as $waste)
			                	{
			                		if($waste->variable->type == 'Reduction')
			                		{
			                			$total_waste += $waste->value;
			                		}
			                	}
			                	foreach($production_result->production_result_addition as $addition)
			                	{
			                		$total_addition += ($addition->block * $production_result->block_weight);
			                	}
			                	// foreach($production_result->production_result_reject as $reject)
			                	// {
			                	// 	$total_reject += $reject->value;
			                	// }
			                	foreach($production_result->production_result_detail as $detail)
			                	{
			                		$total_ready += ($detail->block * $production_result->block_weight);
			                	}
			                	$total_miss = $production_result->weight - ($total_waste + $total_addition + $total_ready);
			                	@endphp
			                    <table class="table">
			                    <tr>
			                    	<td colspan="3">
			                    		<b>{{$production_result->production->fish->name}}</b> 
			                    		<small>{{$production_result->status}}</small>
		                    		</td>
			                    	<td align="right">
			                    		<b>{{number_format($production_result->weight, 2)}} kg</b>
			                    	</td>
			                    	<td>
			                    		@if($production_result->status != 'Done')
			                    		<a href="{{ URL::route('editor.production_result.edit', [$production_result->id]) }}" class="btn btn-sm btn-success"><i class="fa fa-pencil"></i></a>
			                    		@endif
			                    	</td>
			                    </tr>
			                    <tr>
			                    	<td colspan="3"><span>{{$production_result->production->name}}</span></td>
			                    	<td></td>
			                    	<td></td>
								</tr>
								<tr>
									<td colspan="5">Block weight = {{number_format($production_result->block_weight, 2)}} kg</td>
								</tr>
								<tr>
									<td colspan="5">
										{{date("D, d M Y", strtotime($production_result->started_at))}} 
										<i class="fa fa-caret-right"></i> 
										{{date("D, d M Y", strtotime($production_result->finished_at))}}
									</td>
								</tr>

								{{-- Waste Products --}}
								<tr>
									<td colspan="2"><b>Waste Product</b></td>
									<td></td>
									<td align="right"><b>{{number_format($total_waste, 2)}} kg</b></td>
									<td>
										<button type="button" class="btn btn-default btn-sm" data-toggle="collapse" data-target=".waste_rows">
										<i class="fa fa-chevron-down"></i></button>
									</td>
								</tr>
								<tr class="collapse waste_rows">
									<th></th>
									<th>Part</th>
									<th></th>
									<th align="right">Weight</th>
									<th></th>
								</tr>
								@foreach($production_result->production_result_variable as $waste)
								@if($waste->variable->type == 'Reduction')
								<tr class="collapse waste_rows">
									<td></td>
									<td>{{$waste->variable->name}}</td>
									<td></td>
									<td align="right">{{number_format($waste->value, 2)}} kg</td>
									<td></td>
								</tr>
								@endif
								@endforeach
								{{-- Waste Products --}}

								{{-- Additional Products --}}
								<tr>
									<td colspan="2"><b>Additional Product</b></td>
									<td></td>
									<td align="right"><b>{{number_format($total_addition, 2)}} kg</b></td>
									<td>
										<button type="button" class="btn btn-default btn-sm" data-toggle="collapse" data-target=".additional_rows">
										<i class="fa fa-chevron-down"></i></button>
									</td>
								</tr>
								<tr class="collapse additional_rows">
									<th></th>
									<th>Size</th>
									<th>Type, Reason</th>
									<th align="right">Block</th>
									<th></th>
								</tr>
								@foreach($production_result->production_result_addition as $addition)
								<tr class="collapse additional_rows">
									<td></td>
									<td>{{$addition->size}}</td>
									<td>{{$addition->reject_type->name}}, {{$addition->reject_reason->name}}</td>
									<td align="right">{{number_format($addition->block)}}</td>
									<td></td>
								</tr>
								@endforeach
								{{-- Additional Products --}}

								{{-- Reject Products --}}
								{{-- <tr>
					              <td colspan="2"><b>Reject Product</b></td>
					              <td></td>
					              <td align="right"><b>{{number_format($total_reject, 2)}} kg</b></td>
					              <td>
					                <button type="button" class="btn btn-default btn-sm" data-toggle="collapse" data-target=".reject_rows">
					                <i class="fa fa-chevron-down"></i></button>
					              </td>
					            </tr>
					            @foreach($production_result->production_result_reject as $reject)
					            <tr class="collapse reject_rows">
					              <td></td>
					              <td>{{$reject->size}}</td>
					              <td>{{number_format($reject->value, 2)}}</td>
					              <td colspan="2">{{$reject->reject_reason->name}}</td>
					            </tr>
					            @endforeach --}}
								{{-- Reject Products --}}

								{{-- Ready Products --}}
								<tr>
									<td colspan="2"><b>Ready Product</b></td>
									<td></td>
									<td align="right"><b>{{number_format($total_ready, 2)}} kg</b></td>
									<td>
										<button type="button" class="btn btn-default btn-sm" data-toggle="collapse" data-target=".ready_rows">
										<i class="fa fa-chevron-down"></i></button>
									</td>
								</tr>
								<tr class="collapse ready_rows">
									<th></th>
									<th>Size</th>
									<th></th>
									<th align="right">Block</th>
									<th></th>
								</tr>
								@foreach($production_result->production_result_detail as $detail)
								<tr class="collapse ready_rows">
									<td></td>
									<td>{{$detail->size}}</td>
									<td></td>
									<td align="right">{{$detail->block}} block</td>
									<td></td>
								</tr>
								@endforeach
								{{-- Ready Products --}}

								{{-- Miss Products --}}
								<tr>
									<td colspan="2"><b>Miss Product</b></td>
									<td></td>
									<td align="right"><b>{{number_format($total_miss, 2)}} kg</b></td>
									<td></td>
								</tr>
								{{-- Miss Products --}}

								{{-- Yield --}}
								<tr>
									<td colspan="2"><b>Yield</b></td>
									<td></td>
									<td></td>
									<td>
										<button type="button" class="btn btn-default btn-sm" data-toggle="collapse" data-target=".yield_rows">
										<i class="fa fa-chevron-down"></i></button>
									</td>
								</tr>
								<tr class="collapse yield_rows">
									<td></td>
									<td>Waste Product</td>
									<td></td>
									<td align="right">{{number_format($total_waste / $production_result->weight * 100, 2)}} %</td>
									<td></td>
								</tr>
								<tr class="collapse yield_rows">
									<td></td>
									<td>Addition Product</td>
									<td></td>
									<td align="right">{{number_format($total_addition / $production_result->weight * 100, 2)}} %</td>
									<td></td>
								</tr>
								{{-- <tr class="collapse yield_rows">
									<td></td>
									<td>Reject Product</td>
									<td></td>
									<td align="right">{{number_format($total_reject / $production_result->weight * 100, 2)}} %</td>
									<td></td>
								</tr> --}}
								<tr class="collapse yield_rows">
									<td></td>
									<td>Ready Product</td>
									<td></td>
									<td align="right">
										@if(($total_ready / $production_result->weight * 100) < 40)
										<span class="badge badge-warning">
										{{number_format($total_ready / $production_result->weight * 100, 2)}} %
										</span>
										@else
										{{number_format($total_ready / $production_result->weight * 100, 2)}} %
										@endif
									</td>
									<td></td>
								</tr>
								<tr class="collapse yield_rows">
									<td></td>
									<td>Miss Product</td>
									<td></td>
									<td align="right">{{number_format($total_miss / $production_result->weight * 100, 2)}} %</td>
									<td></td>
								</tr>
								{{-- Yield --}}		

			                    </table>
			                    <br>
			                    <div class="pull-right">
			                    	@if($production_result->status == 'Done')
			                      	<a href="{{ URL::route('production_result.pdf', $production_result->id) }}" class="btn btn-default btn-lg"><i class="fa fa-file-pdf-o"></i>&nbsp;PDF</a>
			                      	@else
			                      	<a href="{{ URL::route('editor.production_result.close', $production_result->id)
			                      	}}" class="btn btn-default btn-lg" onclick="return confirm('Close Production?')"><i class="fa fa-lock"></i>&nbsp;Close</a>
			                      	@endif
			                      	<a href="{{ URL::route('editor.production_result.index') }}" class="btn btn-primary btn-lg" >
		                          	<i class="fa fa-list"></i> Go to List</a> 
			                  	</div>
			                    <br>
			                </div> 
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@stop