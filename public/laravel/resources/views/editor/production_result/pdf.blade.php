<!DOCTYPE html>
<html>
<head>
	<style>
		body{
			font-family: helvetica;
		}
		h3{
			text-transform: uppercase;
		}
		h2, h3, h4 {
			margin:5px;
			
		}
		hr{
			border:0px solid black;
			border-top:1px solid #e6e6e6;
			margin: 20px 0;

		}
		.page-break {
		    page-break-after: always;
		}
		table.table_print{
			width:100%;
			border:1px solid #e6e6e6;
			border-collapse: collapse;

		}
		table.table_print tr th, table.table_print tr td{
			text-align: left;
			padding:10px 15px;
			font-size:12px;
			/*border:1px solid black;*/
		}
		table.table_print tr th{
			background: #e6e6e6;
		}
		th.uppercase{
			text-transform: uppercase;
		}

		table.table_print tr td.tab{
			padding-left:40px;
		}
		table.table_print tr td.blank{
			border-right:0px solid red;
			border-left:0px solid red;
		}
		table.table_print tr:nth-child(odd) td{
		    background: #f9f9f9;
		}

		table.table_print tr:nth-child(even) td{
		    background: #fff;
		}
		table.table_print tr td p{
			display: block
		}
		.text_red{
			color:red;
		}
		.text_blue{
			color:blue;
		}
	</style>
	
</head>
<body>	
	<table  width="100%" border="0">
		<tr>
			<td width="180px"><center><img src="assets/img/vfi-small.png" width="130px"></center></td>
			<td>
				<center>
					<h3>SUMMARY</h3>					
					<h4>{{$production_result->production->name}}</h4>
					<h5>{{date("d-m-Y", strtotime($production_result->started_at))}} ~ {{date("d-m-Y", strtotime($production_result->finished_at))}}</h5>
				</center>
			</td>
			<td  width="180px">
				<p style="font-size:8px">
				<b>PT VESSEL FRESHFISH INDOMAKMUR</b><br>
				Jl. Industri Raya II Blok J No. 5 Jatake Tanggerang Banten<br>Tlp. 021-593049502 / 021-59015383<br>marketing@vesselfreshfish.com
				</p>
			</td>
		</tr>
	</table>

	<hr>
	<table class="table_print" border="0">
		<tr>
			<th class="uppercase">{{$production_result->production->name}}&nbsp;{{$production_result->criteria}}</th>
			<th style="text-align:right">{{number_format($production_result->weight, 2)}} kg</th>
			<th></th><th></th>
		</tr>
		<tr>
			<td><b>Waste Product<b></td>
			<td style="text-align:right">
				@php $total_waste = 0; @endphp
				@foreach($production_result->production_result_variable as $production_result_variable)
				@if($production_result_variable->variable->type == 'Reduction' && $production_result_variable->variable->uom == 'kg')
				@php
				$total_waste += $production_result_variable->value;
				@endphp
				@endif
				@endforeach 
				{{number_format($total_waste, 2)}}&nbsp;kg
			</td>
			<td></td>
			<td></td>
		</tr>
		@foreach($production_result->production_result_variable as $production_result_variable)
        @if($production_result_variable->variable->type == 'Reduction')
        <tr>
            <td>{{$production_result_variable->variable->name}}</td>
            <td style="text-align:right">{{number_format($production_result_variable->value, 2)}}&nbsp;{{$production_result_variable->variable->uom}}</td>
            <td></td>
            <td></td>
        </tr>
        @endif
        @endforeach		
		<tr>
			<td><b>Additional Product<b></td>
			<td style="text-align:right">
				@php $total_addition = 0; @endphp
            	@foreach($production_result->production_result_variable as $production_result_variable)
            	@if($production_result_variable->variable->type == 'Addition' && $production_result_variable->variable->uom == 'kg')
            	@php
            	$total_addition += $production_result_variable->value;
            	@endphp
            	@endif
            	@endforeach 
            	{{number_format($total_addition, 2)}}&nbsp;kg
			</td>
			<td></td>
			<td></td>
		</tr>
		@foreach($production_result->production_result_variable as $production_result_variable)
        @if($production_result_variable->variable->type == 'Addition')
        <tr>
            <td>{{$production_result_variable->variable->name}}</td>
            <td style="text-align:right">{{number_format($production_result_variable->value, 2)}}&nbsp;{{$production_result_variable->variable->uom}}</td>
            <td></td>
            <td></td>
        </tr>
        @endif
        @endforeach
		<tr>
			<td><b>Reject Product</b></td>
			<td class="text_red" style="text-align:right">{{number_format($production_result->reject->reject_product, 2)}} kg</td>
			<td></td>
			<td></td>
		</tr>
		@if($production_result->reject->reject_product > 0)
		@foreach($production_result->reject->reject_reject_reason as $reason)
		<tr>
			<td class="tab" colspan="3">{{$reason->reject_reason->name}}</td>
		</tr>
		@endforeach
		@endif
		<tr>
			<td><b>Ready Product (Block @ {{number_format($production_result->block_weight, 2)}}</b></td>
			<td class="text_red" style="text-align:right">{{number_format($production_result->reject->reject_product, 2)}} kg</td>
			<td></td>
			<td></td>
		</tr>
		@foreach($production_result->production_result_detail as $detail)
		<tr>
			<td>Size {{$detail->size}}</td>
            <td style="text-align:right">{{($detail->block)}}</td>
            <td></td>
            <td></td>
		</tr>
		@endforeach
		<tr>
			<td><b>Product Summary</b></td>
			<td class="text_blue" style="text-align:right">{{number_format($production_result->sample_kg + $production_result->production_result_kg, 2)}}&nbsp;kg</td>
			<td></td><td></td>
		</tr>
		<tr>
			<td class="tab">Ready Product (Block weight: {{$production_result->block_weight}})</td>
			<td style="text-align:right">
				@php 
				$total_result = 0; 
				foreach($production_result->production_result_detail as $detail)
				{
					$total_result += $production_result->block_weight * $detail->block;
				}
				@endphp
				{{number_format($total_result)}} kg
				&nbsp;kg
			</td>
			<td style="text-align:center"></td>
			<td ></td>
		</tr>
		<tr>
			<td class="tab">Miss Product</td>
			<td style="text-align:right">
           		{{number_format($production_result->miss_product, 2)}}&nbsp;kg
			</td>
			<td style="text-align:center"></td>
			<td ></td>
		</tr>
		<tr>
			<td class="tab text_blue">TOTAL</td>
			<td style="text-align:right" class="text_blue">{{number_format($production_result->production_result_kg + $production_result->sample_kg, 2)}} kg</td>
			<td style="text-align:center" class="text_blue"></td>
			<td class="text_blue"></td>
		</tr>
		<tr>
			<td><b>Yield</b></td>
			<td style="text-align:right"></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td class="tab">Waste Product</td>
			<td style="text-align:right">{{number_format(($total_waste * 100 / $production_result->weight), 2)}}&nbsp;%</td>
			<td style="text-align:center"></td>
			<td></td>
		</tr>
		<tr>
			<td class="tab">Additional Product</td>
			<td style="text-align:right">{{number_format(($total_addition * 100 / $production_result->weight), 2)}}&nbsp;%</td>
			<td style="text-align:center"></td>
			<td></td>
		</tr>
		<tr>
			<td class="tab">Reject Product</td>
			<td style="text-align:right">{{number_format(($production_result->reject->reject_product * 100 / $production_result->weight), 2)}}&nbsp;%</td>
			<td style="text-align:center"></td>
			<td></td>
		</tr>
		<tr>
			<td class="tab">Sample Product</td>
			<td style="text-align:right">{{number_format(($production_result->sample_kg * 100 / $production_result->weight), 2)}}&nbsp;%</td>
			<td style="text-align:center"></td>
			<td></td>
		</tr>
		<tr>
			<td class="tab">Ready Product</td>
			<td style="text-align:right">{{number_format(($total_result * 100 / $production_result->weight), 2)}}&nbsp;%</td>
			<td style="text-align:center"></td>
			<td ></td>
		</tr>
		<tr>
			<td class="tab">Miss Product</td>
			<td style="text-align:right" class="text_red">{{number_format($production_result->miss_product * 100 / $production_result->weight, 2)}}&nbsp;%</td>
			<td style="text-align:center"></td>
			<td ></td>
		</tr>
	</table>
</body>
</html>