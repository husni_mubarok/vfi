@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li class="active"><a href="{{ URL::route('editor.privilege.index') }}"><i class="fa fa-user-secret"></i> Privilege</a></li>
  </ol>
</section>
@actionStart('privilege', 'create|update')
<section class="content">
  <section class="content box box-solid">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-1"></div>
        <div class="col-md-5">
          <div class="x_panel">
            <h2>
              @if(isset($privilege))
              <i class="fa fa-pencil"></i>
              @else
              <i class="fa fa-plus"></i> 
              @endif
              <i class="fa fa-user-secret"></i> Privilege
            </h2>
            <hr>
            <div class="x_content">
              @include('errors.error')
              @if(isset($privilege))
              {{ Form::model($privilege, array('route' => array('editor.privilege.update', $privilege->id), 'method' => 'put')) }}
              @else
              {!! Form::open(array('route' => ['editor.privilege.store']))!!}
              @endif
              {{ csrf_field() }}
              <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                {{ Form::label('name', 'Name') }}
				{{ Form::text('name', old('name'), array('class' => 'form-control', 'placeholder' => 'Name*')) }}<br/>

                {{ Form::label('description', 'Description') }}
				{{ Form::text('description', old('description'), array('class' => 'form-control', 'placeholder' => 'Description*')) }}<br/>

                <button type="submit" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>

              </div>
              {!! Form::close() !!} 
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</section>
@stop
@actionEnd