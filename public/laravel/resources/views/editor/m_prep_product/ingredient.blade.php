@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li><a href="{{ URL::route('editor.m_prep_product.index') }}"><i class="fa fa-cubes"></i> Catalog</a></li>
    <li class="active"><a href="#"><i class="fa fa-dollar"></i> Ingredient</a></li>
  </ol>
</section>
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-5">
			        <div class="x_panel">
		                <h2>
		                	<i class="fa fa-cubes"></i>
		                	<i class="fa fa-dollar"></i> {{$m_prep_product->name}} Ingredient
	                	</h2>
		                <hr>
			            <div class="x_content">
			                @include('errors.error')
		                    {!! Form::open(array('route' => ['editor.m_prep_product.update_ingredient', $m_prep_product->id]))!!}
		                    {{ csrf_field() }}
		                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
	                    	<table id="productTable" class="table table-sm table-hover">
	                    	<thead>
		                    	<tr>
		                    		<th><i class="fa fa-check-square-o"></i></th>
		                    		<th>Name.</th>
		                    		<th>Description</th>
		                    		<th>UoM</th>
		                    	</tr>
	                    	</thead>
	                    	<tbody>
		                    	@foreach($ingredients as $key => $ingredient)
		                    	<tr>
		                    		<td>
			                    		@if(in_array($ingredient->id, $m_prep_product->m_prep_product_ingredient->pluck('ingredient_id')->toArray()))
				                    	{{ Form::checkbox('ingredient[]', $ingredient->id, true) }}
				                    	@else
				                    	{{ Form::checkbox('ingredient[]', $ingredient->id) }}
				                    	@endif
			                    	</td>
			                    	<td>{{$ingredient->name}}</td>
			                    	<td>{{$ingredient->description}}</td>
			                    	<td>{{$ingredient->uom}}</td>
	                    		</tr>
		                    	@endforeach	
                    		</tbody>
							</table>	                    		
                            <button type="submit" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
	                    	</div>
	                        {!! Form::close() !!}
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@stop