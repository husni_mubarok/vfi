@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li><a href="{{ URL::route('editor.m_prep_product.index') }}"><i class="fa fa-cube"></i> Catalog</a></li>
  </ol>
</section>
@actionStart('m_prep_product', 'read')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-10">
			        <div class="x_panel">
		                <h2>
		                	<i class="fa fa-cube"></i> Catalog List 
		                	@actionStart('m_prep_product', 'create')
		                	<a href="{{ URL::route('editor.m_prep_product.create') }}" class="btn btn-primary btn-lg pull-right"><i class="fa fa-plus"></i> Add</a>
		                	@actionEnd
	                	</h2>
		                <hr>
			            <div class="x_content">
			                <table id="mPrepProductTable" class="table table-striped dataTable">
							  	<thead>
							  	  	<tr>
								      	<th>#</th>
								      	<th>Name</th>
								      	<th>Description</th>
								      	<th>UoM</th>
								      	<th>Made By</th>
								      	<th>Ingredients</th>
								      	<th></th>
							    	</tr>
							  	</thead>
							  	<tbody>
							    @foreach($m_prep_products as $key => $m_prep_product)
							    	<tr>
							      		<td>{{$key+1}}</td>
								      	<td>{{$m_prep_product->name}}</td>
								      	<td>{{$m_prep_product->description}}</td>
								      	<td>{{$m_prep_product->uom}}</td>
								      	<td>
								      		@actionStart('m_prep_product', 'update')
								      		<div class="col-md-2 nopadding">
								      			<a href="{{ URL::route('editor.m_prep_product.edit_material', [$m_prep_product->id]) }}" class="btn btn-primary btn-sm">
								      			{{$m_prep_product->production_m_prep_product->count()}}&nbsp;
								      			<i class="fa fa-cube"></i></a>
								      		</div>
								      		@actionEnd
							      		</td>
								      	<td>
								      		@actionStart('m_prep_product', 'update')
								      		<div class="col-md-2 nopadding">
								      			<a href="{{ URL::route('editor.m_prep_product.edit_ingredient', [$m_prep_product->id]) }}" class="btn btn-primary btn-sm">
								      			{{$m_prep_product->m_prep_product_ingredient->count()}}&nbsp;
								      			<i class="fa fa-cube"></i></a>
								      		</div>
								      		@actionEnd
								      	</td>
								      	<td align="center">
								      		@actionStart('m_prep_product', 'update')
								      		<div class="col-md-2 nopadding">
								      			<a href="{{ URL::route('editor.m_prep_product.edit', [$m_prep_product->id]) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
								      		</div>
								      		@actionEnd

								      		@actionStart('m_prep_product', 'delete')
							      			<div class="col-md-2 nopadding">
								      			{!! Form::open(array('route' => ['editor.m_prep_product.delete', $m_prep_product->id], 'method' => 'delete'))!!}
		                    					{{ csrf_field() }}	                    				
								      			<button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-trash"></i></a></button>
								      			{!! Form::close() !!}
							      			</div>
							      			@actionEnd
							      		</td>
								    </tr>
							    @endforeach
								</tbody>
							</table>
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@actionEnd
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function () {
    $("#mPrepProductTable").DataTable();
    });
</script>
@stop