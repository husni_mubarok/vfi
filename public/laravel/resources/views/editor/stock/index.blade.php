@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li class="active"><a href="#"><i class="fa fa-bar-chart"></i> Unit of Measure Category</a></li>
  </ol>
</section>
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-10">
			        <div class="x_panel">
		                <h2>
		                	<i class="fa fa-bar-chart"></i> Stock
	                	</h2>
		                <hr>
			            <div class="x_content">
			                <table id="uomCategoryTable" class="table table-striped dataTable">
							  	<thead>
							  	  	<tr>
								      	<th>#</th>
								      	<th>Name</th>
								      	<th>Description</th>
								      	<th></th>
							    	</tr>
							  	</thead>
							  	<tbody>
							    @foreach($uom_categories as $key => $uom_category)
							    	<tr>
							      		<td>{{$key+1}}</td>
								      	<td>{{$uom_category->name}}</td>
								      	<td>{{$uom_category->description}}</td>
								      	<td align="center">
								      		<div class="col-md-2 nopadding">
								      			<a href="{{ URL::route('editor.uom_category.edit', [$uom_category->id]) }}" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i></a>
								      		</div>
							      			<div class="col-md-2 nopadding">
								      			{!! Form::open(array('route' => ['editor.uom_category.delete', $uom_category->id], 'method' => 'delete'))!!}
		                    					{{ csrf_field() }}	                    				
								      			<button type="submit" class="btn btn-default btn-sm"><i class="fa fa-trash"></i></a></button>
								      			{!! Form::close() !!}
							      			</div>
							      		</td>
								    </tr>
							    @endforeach
								</tbody>
							</table>
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function () {
    $("#uomCategoryTable").DataTable(
    	{
    		"scrollX": true
    	});
    });
</script>
@stop