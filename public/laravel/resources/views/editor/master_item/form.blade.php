@extends('layouts.home.template')
@section('content') 
<div class="header_menu mobile-hide">
  <a href="{{ URL::route('editor.type_item.index') }}">TYPE ITEM</a> |
  <a href="{{ URL::route('editor.category_item.index') }}">CATEGORY ITEM</a> |
  <a href="{{ URL::route('editor.master_item.index') }}">MASTER ITEM</a> |
  <a href="{{ URL::route('editor.unit.index') }}">SATUAN</a> |
  <a href="{{ URL::route('editor.custom_detail.index') }}">CUSTOM DETAIL</a>
</div> 
<section class="content box box-solid">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-1"></div>
			<div class="col-md-5">
				<div class="x_panel">
					<h2>
						@if(isset($master_item))
						<i class="fa fa-pencil"></i>
						@else
						<i class="fa fa-plus"></i> 
						@endif
						<i class="fa fa-bar-chart"></i> Master Item
					</h2>
					<hr>
					<div class="x_content">
						@include('errors.error')
						@if(isset($master_item))
						{!! Form::model($master_item, array('route' => ['editor.master_item.update', $master_item->id], 'method' => 'PUT', 'class'=>'update'))!!}
						@else
						{!! Form::open(array('route' => 'editor.master_item.store', 'class'=>'create'))!!}
						@endif
						{{ csrf_field() }}
						<div class="col-md-12 col-sm-12 col-xs-12 form-group">
							{{ Form::label('name', 'Name') }}
							{{ Form::text('name', old('name'), array('class' => 'form-control', 'placeholder' => 'Name*', 'required' => 'true')) }}<br/>
							
							{{ Form::label('Category Item') }}
							{{ Form::select('id_category_item', $category_item_list, old('id_category_item'), array('class' => 'form-control', 'required' => 'true')) }}<br/>

							{{ Form::label('Type Item') }}
							{{ Form::select('id_type_item', $type_item_list, old('id_type_item'), array('class' => 'form-control', 'required' => 'true')) }}<br/>

							{{ Form::label('description', 'Description') }}
							{{ Form::text('description', old('description'), array('class' => 'form-control', 'placeholder' => 'Description*', 'required' => 'true')) }}<br/>

							<button type="submit" class="btn btn-success pull-right"><i class="fa fa-check"></i> Save</button>
							<a href="{{ URL::route('editor.master_item.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
						</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@stop

@section('scripts')
<script>
	$(".update").on("submit", function(){
		return confirm("Do you want to update this master item?");
	});

	$(".create").on("submit", function(){
		return confirm("Do you want to create this master item?");
	});
</script>
@stop