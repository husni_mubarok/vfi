@extends('layouts.home.template')
@section('content') 
<div class="header_menu mobile-hide">
  <a href="{{ URL::route('editor.type_item.index') }}">TYPE ITEM</a> |
  <a href="{{ URL::route('editor.category_item.index') }}">CATEGORY ITEM</a> |
  <a href="{{ URL::route('editor.master_item.index') }}">MASTER ITEM</a> |
  <a href="{{ URL::route('editor.unit.index') }}">SATUAN</a> |
  <a href="{{ URL::route('editor.custom_detail.index') }}">CUSTOM DETAIL</a>
</div> 
<section class="content box box-solid">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
     <div class="col-md-1"></div>
     <div class="col-md-12">
       <div class="x_panel">
        <h2>
         <i class="fa fa-bar-chart"></i> Master Item
         <a href="{{ URL::route('editor.master_item.create') }}" class="btn btn-default btn-lg pull-right"><i class="fa fa-plus"></i> Add</a>
       </h2>
       <hr>
       <div class="x_content">
         <table id="masterItemTable" class="table table-striped dataTable">
          <thead>
           <tr>
             <th></th>
             <th>Category</th>
             <th>Type</th>
             <th>Master Item</th>
             <th>Description</th>
             <th>Edit</th>
             <th>Delete</th>
           </tr>
         </thead>
       </table>
       <script id="details-template" type="text/x-handlebars-template">
         {{--  <div class="label label-info">Name @{{id}}</div>  --}}
         <table class="table details" id="posts-@{{id}}">
           <thead>
             <tr>
               <th>Storage</th>
               <th>Item</th>
               <th>Size</th>
               <th>Stock</th>
               <th>Unit Min</th>
               <th>Desc</th>
             </tr>
           </thead>
         </table>
       </script>
     </div>
   </div>
 </div>
</div>
</div>
</section>
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function() {
   var template = Handlebars.compile($("#details-template").html());
   var table = $('#masterItemTable').DataTable({
    processing: true,
    serverSide: true,
    "pageLength": 25,
    "scrollY": "400px",
    ajax: {
      url: '{{ url('editor/master_item/data') }}'
    },
    columns: [
    {
      "className": 'details-control',
      "orderable": false,
      "searchable": false,
      "data": null,
      "defaultContent": ''
    },
    { data: 'categoryitem', name: 'name', orderable:false},
    { data: 'typeitem', name: 'name', orderable:false},
    { data: 'masteritem', name: 'name', orderable:false},
    { data: 'description'},
    { data: 'edit', name: 'edit', orderable:false, searchable: false },
    { data: 'delete', name: 'delete', orderable:false, searchable: false }
    ],
    order: [[1, 'asc']]
  });

    // Add event listener for opening and closing details
    $('#masterItemTable tbody').on( 'click', 'tr td.details-control', function () {
        //alert("test");
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var tableId = 'posts-' + row.data().id;

        //console.log(tableId);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass( 'details' );
          } else {
            // Open this row
            row.child(template(row.data())).show();
            initTable(tableId, row.data());
            tr.addClass( 'details' );
            tr.next().find('td').addClass('no-padding bg-gray');
          }
        });

    function initTable(tableId, data) {
      $('#' + tableId).DataTable({
        processing: true,
        serverSide: true,
        ajax: data.details_url,
        columns: [
        { data: 'address'},
        { data: 'name'},
        { data: 'size'},
        { data: 'stock_unit_min'},
        { data: 'unit_min'},
        { data: 'rs_concat'}
        ]
      })
    }
  });
</script>

<script>
 var el = document.getElementById('myCoolForm');

 el.addEventListener('submit', function(){
  return confirm('Are you sure you want to submit this form?');
}, false);
</script>
@stop