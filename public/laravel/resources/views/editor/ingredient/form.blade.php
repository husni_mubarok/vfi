@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li class="active"><a href="{{ URL::route('editor.ingredient.index') }}"><i class="fa fa-cube"></i> Product Type</a></li>
  </ol>
</section>
@actionStart('ingredient', 'create|update')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-5">
			        <div class="x_panel">
		                <h2>
		                	@if(isset($ingredient))
		                	<i class="fa fa-pencil"></i>
		                	@else
		                	<i class="fa fa-plus"></i> 
		                	@endif
		                	<i class="fa fa-cube"></i> Product Type
	                	</h2>
		                <hr>
			            <div class="x_content">
			                @include('errors.error')
	                        @if(isset($ingredient))
			                {!! Form::model($ingredient, array('route' => ['editor.ingredient.update', $ingredient->id], 'method' => 'PUT'))!!}
		                    @else
		                    {!! Form::open(array('route' => 'editor.ingredient.store'))!!}
		                    @endif
		                    {{ csrf_field() }}
		                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
		                    	{{ Form::label('name', 'Name') }}
		                        {{ Form::text('name', old('name'), array('class' => 'form-control', 'placeholder' => 'Name*', 'required' => 'true')) }}<br/>

		                        {{ Form::label('description', 'Description') }}
		                        {{ Form::text('description', old('description'), array('class' => 'form-control', 'placeholder' => 'Description')) }}<br/>

		                        {{ Form::label('uom', 'Unit of Measurement') }}
		                        {{ Form::select('uom', ['gram' => 'gram', 'pcs' => 'pcs'], old('uom'), ['placeholder' => 'Select a Unit', 'class' => 'form-control']) }}<br/>

	                            <button type="submit" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
	                    	</div>
	                        {!! Form::close() !!}
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@actionEnd
@stop