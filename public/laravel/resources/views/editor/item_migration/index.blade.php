@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li class="active"><a href="#"><i class="fa fa-bar-chart"></i>  Item</a></li>
  </ol>
</section>
<section class="content">
	<section class="content box box-solid">
		<div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
       <div class="col-md-1"></div>
       <div class="col-md-10">
         <div class="x_panel">
          <h2>
           <i class="fa fa-bar-chart"></i>  Item Migration!
         </h2>
         <hr>
         <div class="x_content">
          <center>
            @if(Session::has('success'))
            <div class="alert alert-info">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <strong>Item Migration </strong> {{ Session::get('message', '') }}
            </div>
            @endif
            <br/>
            {!! Form::open(array('route' => 'editor.item_migration.store'))!!}
            {{ csrf_field() }}	                    				
            <button type="submit" class="btn btn-success btn-lg"><i class="fa fa-magic"></i></a> Start Migration!</button>
            {!! Form::close() !!}
          </center>
        </div>
      </div>
    </div>
  </div>
</div>
</section>
</section>



@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>

<script>

  $(function() {
    $('#itemTable').DataTable({

      processing: true,
      serverSide: true,
      "pageLength": 25,
      "scrollY": "400px",
      ajax: "{{ url('editor/item/data') }}",
      columns: [
      { data: 'categoryitem', name: 'categoryitem' },
      { data: 'typeitem', name: 'typeitem' },
      { data: 'masteritem', name: 'masteritem' },
      { data: 'item', name: 'item' },
      { data: 'size', name: 'size' },
      { data: 'detail', name: 'detail' },
      { data: 'edit', name: 'edit' },
      { data: 'delete', name: 'delete' }
      ]
    });
  });
</script>
@stop
