@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li class="active"><a href="{{ URL::route('editor.conversion_item.index') }}"><i class="fa fa-bar-chart"></i> Conversion Item</a></li>
  </ol>
</section>

<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-5">
			        <div class="x_panel">
		                <h2>
		                	@if(isset($stock))
		                	<i class="fa fa-pencil"></i>
		                	@else
		                	<i class="fa fa-plus"></i> 
		                	@endif
		                	<i class="fa fa-bar-chart"></i> Conversion Item
	                	</h2>
		                <hr>
			            <div class="x_content">
			                @include('errors.error')
	                        @if(isset($stock))
			                {!! Form::model($stock, array('route' => ['editor.conversion_item.update', $stock->id_item], 'method' => 'PUT'))!!}
		                    
		                    @endif
		                    {{ csrf_field() }}
		                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
		                    	{{ Form::label('address', 'Storage') }}
		                        {{ Form::text('address', old('address'), array('class' => 'form-control', 'placeholder' => 'Item*', 'required' => 'true', 'disabled')) }}<br/>

		                        {{ Form::label('name', 'Item') }}
		                        {{ Form::text('name', old('name'), array('class' => 'form-control', 'placeholder' => 'Item*', 'required' => 'true', 'disabled')) }}<br/>

		                        {{ Form::label('value', 'Size') }}
		                        {{ Form::text('value', old('value'), array('class' => 'form-control', 'placeholder' => 'Size*', 'required' => 'true', 'disabled')) }}<br/>

		                        {{ Form::label('rs_concat', 'Stock') }}
		                        {{ Form::text('rs_concat', old('rs_concat'), array('class' => 'form-control', 'placeholder' => 'Stock*', 'required' => 'true', 'disabled')) }}<br/>

								<hr>
								<h4> Convert to:</h4>
		                        {{ Form::label('UOM') }}
		                    	{{ Form::select('value', $list_detail_uom, old('value'), array('class' => 'form-control', 'required' => 'true')) }}<br/>

		                    	<h4>Result: {{ number_format($rs_conversion, 0) }} {{$rs_uom}}</h4>

	                            <button type="submit" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Convert</button>
	                            <a href="{{ URL::route('editor.conversion_item.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>

	                    	</div>
	                        {!! Form::close() !!}
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@stop