@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li class="active"><a href="#"><i class="fa fa-bar-chart"></i> Conversion Item</a></li>
  </ol>
</section>
<section class="content">
  <section class="content box box-solid">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="col-md-1"></div>
          <div class="col-md-10">
              <div class="x_panel">
                    <h2>
                      <i class="fa fa-bar-chart"></i> Conversion Item
                    </h2>
                    <hr>
                  <div class="x_content">
                      <table id="conversionItemTable" class="table table-striped dataTable">
                  <thead>
                      <tr>
                        <th>#</th>
                        <th>Storage</th>
                        <th>Item</th>
                        <th>Size</th>
                        <th>Stock</th>
                        <th>Conversion</th>
                    </tr>
                  </thead>
                  <tbody>
                  @foreach($stock as $key => $stocks)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$stocks->address}}</td>
                        <td>{{$stocks->name}}</td>
                        <td>{{$stocks->value}}</td>
                        <td>{{$stocks->rs_concat}}</td>
                        <td align="center">
                          <div class="col-md-2 nopadding">
                            <a href="{{ URL::route('editor.conversion_item.edit', [$stocks->id_item]) }}" class="btn btn-default btn-sm"><i class="fa fa-magic"></i></a>
                          </div>
                        </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
                  </div>
              </div>
            </div>
        </div>
    </div>
  </section>
</section>
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function () {
    $("#conversionItemTable").DataTable(
      {
        "scrollX": true
      });
    });
</script>
@stop