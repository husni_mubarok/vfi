@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="{{ URL::route('editor.purchase.index') }}"><i class="fa fa-shopping-cart"></i> Purchase</a></li>
    <li class="active"><a href="#"><i class="fa fa-dollar"></i> Price</a></li>
  </ol>
</section>
@actionStart('price', 'create|update')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-10">
			        <div class="x_panel">
		                <h2>
		                	<i class="fa fa-dollar"></i>&nbsp;Price List
		                	<small>{{$purchase->description}}</small>
	                	</h2>
		                <hr>
			            <div class="x_content">
			            	<div class="col-md-6">
			            		@include('errors.error')
			            		{{-- @if(isset($prices))
				            	{!! Form::open(array('route' => ['editor.price.update', $purchase->id], 'method' => 'PUT'))!!}
				            	@else --}}
				            	{!! Form::open(array('route' => ['editor.price.store', $purchase->id])) !!}
				            	{{-- @endif --}}
				                <table class="table table-bordered">
								  	<thead>
								  	  	<tr>
											<th>Size</th>
											<th>Tag</th>
											<th>Price ({{$purchase->currency}})</th>
											@if(isset($previous_price) && $previous_price->count() > 0)
											<th>Latest Price</th>
											@endif
										</tr>
								  	</thead>
								  	<tbody>

								  	@if(isset($prices))
								  	@foreach($prices as $key => $price)
								  	<tr>
								  		<td>{{$price->size->min_value}}&nbsp;~&nbsp;{{$price->size->max_value}}</td>
								  		<td>{{$price->size->note}}</td>
								  		<td align="right">
							  				{{ Form::number('value['.$price->size->id.']', old('value['.$price->size->id.']', $price->value)) }}
								  		</td>
								  	</tr>
								  	@endforeach

								  	@else
								    @foreach($purchase->purchase_product->product->product_type->product_type_size as $key => $product_type_size)
									<tr>
										<td>{{$product_type_size->size->min_value}}&nbsp;~&nbsp;{{$product_type_size->size->max_value}}</td>
										<td>{{$product_type_size->size->note}}</td>
										<td align="right">
											{{ Form::number('value['.$product_type_size->size->id.']', old('value['.$product_type_size->size->id.']', '0')) }}
										</td>
										@if(isset($previous_price) && $previous_price->count() > 0)
										<td>
											@if($key+1 <= $previous_price->count())
											{{$previous_price[$key]->value}}
											@else
											- 
											@endif
										</td>
										@endif
									</tr>
									@endforeach
									
									@endif
									</tbody>
								</table>
								<button type="button" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
								{!! Form::close() !!}
							</div>
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@actionEnd
@stop