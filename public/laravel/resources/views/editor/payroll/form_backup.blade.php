@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li><a href="{{ URL::route('editor.payroll.index') }}"><i class="fa fa-list-ul"></i> Payroll List</a></li>
		<li class="active">
			@if(isset($payroll))
			<i class="fa fa-pencil"></i> Edit
			@else
			<i class="fa fa-plus"></i> Create
			@endif
		</li>
	</ol>
</section>

<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12"> 
				<div class="col-md-12">
					<div class="x_panel">
						<h2>
							@if(isset($payroll))
							<i class="fa fa-pencil"></i>
							@else
							<i class="fa fa-plus"></i> 
							@endif
							<i class="fa fa-list-ul"></i> Payroll List
						</h2>
					</div>
					<hr>
					<div class="col-md-6">
					@include('errors.error')
					@if(isset($payroll))
					{!! Form::model($payroll, array('route' => ['editor.payroll.update', $payroll->id], 'method' => 'PUT', 'class'=>'update', 'id' => 'form_payroll'))!!}
					@else
					{!! Form::open(array('route' => 'editor.payroll.store', 'class'=>'create', 'id' => 'form_payroll'))!!}
					@endif
					{{ csrf_field() }}
						<div class="x_content"> 
							<div class="col-md-12 col-sm-12 col-xs-12 form-group">
								{{ Form::label('year', 'Year') }}
								{{ Form::number('year', old('year'), ['class' => 'form-control', 'id' => 'year', 'placeholder' => 'Year of Payroll', 'min' => (date('Y') - 20), 'max' => (date('Y') + 20)]) }}
								<br>

								{{ Form::label('month', 'Month') }}
								{{ Form::select('month', $month_list, old('month'), ['class' => 'form-control', 'id' => 'month', 'placeholder' => 'Month of Payroll']) }}
								<br>

								{{ Form::label('comment', 'Comment') }}
								{{ Form::text('comment', old('comment'), ['class' => 'form-control', 'id' => 'comment', 'placeholder' => 'Comment']) }}
								<br>

								<button type="button" id="btn_confirm" data-toggle="modal" data-target="#modal_confirm" class="btn btn-success"><i class="fa fa-check"></i> Save</button>
							</div>
						</div>

					{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@stop

@section('modal')
<div class="modal fade" id="modal_confirm">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h4 class="modal-title">Payroll Summary</h4>
      		</div>
      		<div class="modal-body">
      			<table class="table">
      				<tr>
      					<th width="25%">Year</th>
      					<td width="75%" id="summary_payroll_year"></td>
      				</tr>
      				<tr>
      					<th>Month</th>
      					<td id="summary_payroll_month"></td>
      				</tr>
      				<tr>
      					<th>Comment</th>
      					<td id="summary_payroll_comment"></td>
      				</tr>
      			</table>
      		</div>
      		<div class="modal-footer">
      			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
      			<button type="button" id="btn_submit" class="btn btn-success"><i class="fa fa-check"></i> Submit</button>
      		</div>
    	</div>
  	</div>
</div>
@stop

@section('scripts')
<script>
$('#btn_submit').on('click', function()
{
	$('#form_payroll').submit();
});

$('#btn_confirm').on('click', function()
{
	$('#summary_payroll_year').text($('#year').val());
	$('#summary_payroll_month').text($('#month').val());
	$('#summary_payroll_comment').text($('#comment').val());
});
</script>
@stop

