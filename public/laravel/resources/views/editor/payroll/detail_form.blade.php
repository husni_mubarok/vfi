@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li><a href="{{ URL::route('editor.payroll.index') }}"><i class="fa fa-list-ul"></i> Payroll List</a></li>
		<li><a href="{{ URL::route('editor.payroll.detail', [$payroll->id]) }}"><i class="fa fa-search"></i> Detail</a></li>
		<li class="active">
			@if(isset($payroll_detail))
			<i class="fa fa-pencil"></i> Edit
			@else
			<i class="fa fa-plus"></i> Create
			@endif
		</li>
	</ol>
</section>
@actionStart('payroll', 'create|update')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12"> 
				<div class="col-md-12">
					<div class="x_panel">
						<h2>
							@if(isset($payroll_detail))
							<i class="fa fa-pencil"></i>
							@else
							<i class="fa fa-plus"></i> 
							@endif
							<i class="fa fa-list-ul"></i> <i class="fa fa-search"></i> Payroll Detail
						</h2>
					</div>
					<hr>
					{{-- Input Form --}}
					<div class="col-md-6">
						@include('errors.error')
						@if(isset($payroll_detail))
						{!! Form::model($payroll_detail, array('route' => ['editor.payroll.detail_update', $payroll_detail->id], 'method' => 'PUT', 'class'=>'update', 'id' => 'form_payroll_detail'))!!}
						@else
						{!! Form::open(array('route' => ['editor.payroll.detail_store', $payroll->id], 'class'=>'create', 'id' => 'form_payroll_detail'))!!}
						@endif
						{{ csrf_field() }}
						<div class="x_content"> 
							<div class="col-md-12 col-sm-12 col-xs-12 form-group">
								{{ Form::label('employee_id', 'Employee') }}
								{{ Form::select('employee_id', $employee_list, old('employee_id'), ['class' => 'form-control', 'id' => 'employee_id', 'placeholder' => 'Employee']) }}
								<br>

								{{ Form::label('workdays', 'Work Days') }}
								{{ Form::number('workdays', old('workdays'), ['class' => 'form-control', 'id' => 'workdays', 'placeholder' => 'Work Days', 'min' => '0']) }}
								<br>

								{{ Form::label('slr_basic', 'Basic Salary') }}
								{{ Form::number('slr_basic', old('slr_basic'), ['class' => 'form-control', 'id' => 'slr_basic', 'placeholder' => 'Basic Salary', 'min' => '0']) }}
								<br>

								{{ Form::label('slr_transport', 'Transport Cost') }}
								{{ Form::number('slr_transport', old('slr_transport'), ['class' => 'form-control', 'id' => 'slr_transport', 'placeholder' => 'Transport Cost', 'min' => '0']) }}
								<br>

								{{ Form::label('slr_tunjangan_makan', 'Consumption Cost') }}
								{{ Form::number('slr_tunjangan_makan', old('slr_tunjangan_makan'), ['class' => 'form-control', 'id' => 'slr_tunjangan_makan', 'placeholder' => 'Transport Cost', 'min' => '0']) }}
								<br>

								{{ Form::label('slr_cashbond', 'Cash Bond') }}
								{{ Form::number('slr_cashbond', old('slr_cashbond'), ['class' => 'form-control', 'id' => 'slr_cashbond', 'placeholder' => 'Transport Cost', 'min' => '0']) }}
								<br>

								{{ Form::label('slr_thr', 'THR') }}
								{{ Form::number('slr_thr', old('slr_thr'), ['class' => 'form-control', 'id' => 'slr_thr', 'placeholder' => 'Transport Cost', 'min' => '0']) }}
								<br>

								{{ Form::label('slr_pot_deposit', 'Pot Deposit') }}
								{{ Form::number('slr_pot_deposit', old('slr_pot_deposit'), ['class' => 'form-control', 'id' => 'slr_pot_deposit', 'placeholder' => 'Transport Cost', 'min' => '0']) }}
								<br>

								{{ Form::label('slr_total', 'Total Salary') }}
								{{ Form::number('slr_total', old('slr_total'), ['class' => 'form-control', 'id' => 'slr_total', 'placeholder' => 'Transport Cost', 'min' => '0']) }}
								<br>

								{{ Form::label('slr_thp', 'THP') }}
								{{ Form::number('slr_thp', old('slr_thp'), ['class' => 'form-control', 'id' => 'slr_thp', 'placeholder' => 'Transport Cost', 'min' => '0']) }}
								<br>

								<button type="button" id="btn_confirm" data-toggle="modal" data-target="#modal_confirm" class="btn btn-success"><i class="fa fa-check"></i> Save</button>
							</div>
						</div>
						{!! Form::close() !!}
					</div>
					{{-- Input Form --}}

					{{-- Employee Detail --}}
					<div class="col-md-6">
						<table class="table">
							
						</table>
					</div>
					{{-- Employee Detail --}}
				</div>
			</div>
		</div>
	</div>
</section>
@stop
@actionEnd

@section('modal')
<div class="modal fade" id="modal_confirm">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h4 class="modal-title">Payroll Detail Summary</h4>
      		</div>
      		<div class="modal-body">
      			<table class="table">
      				<tr>
      					<th width="25%">Employee</th>
      					<td width="75%" id="summary_payroll_detail_employee"></td>
      				</tr>
      				<tr>
      					<th>Work Days</th>
      					<td id="summary_payroll_detail_work_days"></td>
      				</tr>
      				<tr>
      					<th>Basic Salary</th>
      					<td id="summary_payroll_detail_basic_salary"></td>
      				</tr>
      				<tr>
      					<th>Transport Cost</th>
      					<td id="summary_payroll_detail_transport_cost"></td>
      				</tr>
      				<tr>
      					<th>Consumption Cost</th>
      					<td id="summary_payroll_detail_consumption_cost"></td>
      				</tr>
      				<tr>
      					<th>Cash Bond</th>
      					<td id="summary_payroll_detail_cash_bond"></td>
      				</tr>
      				<tr>
      					<th>THR</th>
      					<td id="summary_payroll_detail_thr"></td>
      				</tr>
      				<tr>
      					<th>Pot Deposit</th>
      					<td id="summary_payroll_detail_pot_deposit"></td>
      				</tr>
      				<tr>
      					<th>Total</th>
      					<td id="summary_payroll_detail_total"></td>
      				</tr>
      				<tr>
      					<th>THP</th>
      					<td id="summary_payroll_detail_thp"></td>
      				</tr>
      			</table>
      		</div>
      		<div class="modal-footer">
      			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
      			<button type="button" id="btn_submit" class="btn btn-success"><i class="fa fa-check"></i> Submit</button>
      		</div>
    	</div>
  	</div>
</div>
@stop

@section('scripts')
<script>
$('#btn_submit').on('click', function()
{
	$('#form_payroll_detail').submit();
});

$('#btn_confirm').on('click', function()
{
	$('#summary_payroll_detail_employee').text($('#employee_id option:selected').text());
	$('#summary_payroll_detail_work_days').text($('#workdays').val());
	$('#summary_payroll_detail_basic_salary').text($('#slr_basic').val());
	$('#summary_payroll_detail_transport_cost').text($('#slr_transport').val());
	$('#summary_payroll_detail_consumption_cost').text($('#slr_tunjangan_makan').val());
	$('#summary_payroll_detail_cash_bond').text($('#slr_cashbond').val());
	$('#summary_payroll_detail_thr').text($('#slr_thr').val());
	$('#summary_payroll_detail_pot_deposit').text($('#slr_pot_deposit').val());
	$('#summary_payroll_detail_total').text($('#slr_total').val());
	$('#summary_payroll_detail_thp').text($('#slr_thp').val());
});
</script>
@stop

