@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->

<style type="text/css">
	th { font-size: 11px; }
	td { font-size: 11px; }
</style>
<section class="content-header hidden-xs">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li class="active"><a href="#"><i class="fa fa-list-ul"></i> Payroll List</a></li>
  </ol>
</section>
@actionStart('payroll', 'read')
<section class="content">
	<section class="content box mobile box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-12">
			        <div class="x_panel">
		                <h2>
		                	<i class="fa fa-list-ul"></i> Payroll List 
	                		@actionStart('payroll', 'create')
		                	<a href="{{ URL::route('editor.payroll.createheader') }}" class="btn btn-success btn-lg pull-right"><i class="fa fa-plus"></i> Add</a> 
		                	@actionEnd
	                	</h2>
		                <hr>
			           <div class="x_content">
			                <table id="table_payrollx" class="table dataTable rwd-table">
							  	<thead>
							  	  	<tr>
								      	<th width="5%">#</th>
								      	<th>Year</th>
								      	<th>Month</th>
								      	<th>Status</th>
								      	<th>Total Salary</th>
								      	<th>Approval Date</th> 
								      	<th>Payment Date</th>
								      	<th>Comment</th>
								      	<th>Attachment Receipt</th>
								      	<th>Action Status</th>
								      	<th width="10%">Action</th>
							    	</tr>
							  	</thead>
							  	<tbody>
								@foreach($payrolls as $key => $payroll)
									<tr>
										<td data-th="#">{{$number++}}</td>
										<td id="year_{{$payroll->id}}" data-th="Year">{{$payroll->year}}</td>
										<td id="month_{{$payroll->id}}" data-th="Month">{{$payroll->month}}</td>
										<td data-th="Status">
											@if($payroll->status == 0)
											<span class="label label-danger"><i class="fa fa-unlock"></i> Open</span>
											@elseif($payroll->status == 1)
											<span class="label label-warning"><i class="fa fa-lock"></i> Waiting Finance Approval</span>
											@elseif($payroll->status == 2)
											<span class="label label-warning"><i class="fa fa-lock"></i> Waiting Owner Approval</span>
											@elseif($payroll->status == 3)
											<span class="label label-warning"><i class="fa fa-lock"></i> Waiting Finance Payment</span>
											@elseif($payroll->status == 4)
											<span class="label label-success"><i class="fa fa-check"></i> Paid</span>
											@endif
										</td>
										<td data-th="Total Salary">
											Total Salary
										</td>
										<td data-th="Approval Date">
											@if(!$payroll->approved_date)
											<span class="label label-danger"> UNAPPROVED </span>
											@elseif($payroll->approved_date)
											<span class="label label-success"> {{date('D, d-m-Y', strtotime($payroll->approved_date))}} </span>
											@endif
										</td> 
										<td data-th="Payment Date">
											@if(!$payroll->paid_date)
											<span class="label label-danger"> UNPAID </span>
											@elseif($payroll->paid_date)
											<span class="label label-success"> {{date('D, d-m-Y', strtotime($payroll->paid_date))}} </span>
											@endif
										</td>
										<td data-th="Comment">{{$payroll->comment}}</td>
										<td data-th="Attachment">
											@if($payroll->attachment_receipt == null)
											Tidak ada lampiran
											@else
											<a target="_blank" href="{{Config::get('constants.path.uploads')}}/payroll/attachment_receipt/{{$payroll->attachment_receipt}}"><i class="fa fa-download"></i>&nbsp;Download</a>
											
											@endif
										</td>  	
										<td>
											@if($payroll->status == 0)
						                		@actionStart('payroll', 'submit')
							                	{!! Form::open(['route' => ['editor.payroll.submit', $payroll->id], 'method' => 'PUT', 'id' => 'form_submit_'.$payroll->id]) !!}
							                	<button type="button" class="btn btn-sm btn-success btn_submit" value="{{$payroll->id}}">
							                		<i class="fa fa-check"></i> Submit
						                		</button>
							                	{!! Form::close() !!}
							                	@actionEnd

						                	@elseif($payroll->status == 1)
						                		@actionStart('payroll', 'paid')
							                	{!! Form::open(['route' => ['editor.payroll.finance_approve', $payroll->id], 'method' => 'PUT', 'id' => 'form_finance_'.$payroll->id]) !!}
							                	<input type="hidden" name="review" value="0" id="finance_review">
						                		<table>
					                				<tr>
					                					<td>
										                	<button type="button" class="btn btn-sm btn-danger btn_finance_reject" value="{{$payroll->id}}">
										                		<i class="fa fa-remove"></i> Reject
									                		</button>
									                	</td>
									                	<td>
										                	<button type="button" class="btn btn-sm btn-success btn_finance_approve" value="{{$payroll->id}}">
										                		<i class="fa fa-check"></i> Approve
									                		</button>
									                	</td>
								                	</tr>	
							                	</table>
							                	{!! Form::close() !!}
							                	@actionEnd

						                	@elseif($payroll->status == 2)
						                		@actionStart('payroll', 'issued')
							                	{!! Form::open(['route' => ['editor.payroll.owner_approve', $payroll->id], 'method' => 'PUT', 'id' => 'form_owner_'.$payroll->id]) !!}
							                	<button type="button" class="btn btn-sm btn-success btn_owner" value="{{$payroll->id}}">
							                		<i class="fa fa-check"></i> Issued
						                		</button>
							                	{!! Form::close() !!}
							                	@actionEnd

						                	@elseif($payroll->status == 3)
						                		@actionStart('payroll', 'paid')
							                	{!! Form::open(['route' => ['editor.payroll.finance_payment', $payroll->id], 'method' => 'PUT', 'id' => 'form_paid_'.$payroll->id]) !!}
							                	<button type="button" class="btn btn-sm btn-success btn_paid" value="{{$payroll->id}}">
							                		<i class="fa fa-check"></i> Paid
						                		</button>
							                	{!! Form::close() !!}
							                	@actionEnd

						                	@endif
										</td>
										<td align="center">
											<div class="act_tb">
												<div>
													<a href="{{ URL::route('editor.payroll.detail', [$payroll->id]) }}" class="btn btn-sm btn-default"><i class="fa fa-search"></i></a>
												</div>
												@actionStart('payroll', 'update')
												@if($payroll->status == 0)
												<div>
													<a href="{{ URL::route('editor.payroll.edit', [$payroll->id]) }}" class="btn btn-sm btn-success"><i class="fa fa-pencil"></i></a>
												</div>
												@endif
												@actionEnd
												@actionStart('payroll', 'delete')
												@if($payroll->status == 0)
												<div>
													{!! Form::open(['route' => ['editor.payroll.delete', $payroll->id], 'method' => 'delete']) !!}
													<button type="submit" onclick="return confirm('Delete?')" class="btn btn-sm btn-danger"><i class="fa fa-remove"></i></button>
													{!! Form::close() !!}
												</div>
												@endif
												@actionEnd
											</div>
										</td>
									</tr>
								@endforeach
								</tbody>
							</table>
			            </div> 
						{{$payrolls->links()}}
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@stop
@actionEnd

@section('modal')
<div class="modal fade" id="modal_summary">
  	<div class="modal-dialog" role="document">
	    <div class="modal-content">
	      	<div class="modal-header">
    		    <h5 class="modal-title"><i class="fa fa-warning"></i> Confirmation!</h5>
      		</div>
      		<div class="modal-body">
        		<div id="modal_summary_body"></div>
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-primary" id="btn_confirm">YES</button>
        		<button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
      		</div>
    	</div>
  	</div>
</div>
@stop

@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function () {
    $("#table_payroll").DataTable();
    });

$('.btn_submit').on('click', function()
{
	var id = $(this).val();
	$('#modal_summary_body').html('Submit Payroll '+$('#month_'+id).text()+' '+$('#year_'+id).text()+'?');
	$('#modal_summary').modal('show');
	$('#btn_confirm').on('click', function()
	{
		$(this).attr('disabled', true);
		$('#form_submit_'+id).submit();
	});
});

$('.btn_finance_reject').on('click', function()
{
	var id = $(this).val();
	$('#finance_review').val('0');
	$('#modal_summary_body').html('Reject Payroll '+$('#month_'+id).text()+' '+$('#year_'+id).text()+'?');
	$('#modal_summary').modal('show');
	$('#btn_confirm').on('click', function()
	{
		$(this).attr('disabled', true);
		$('#form_finance_'+id).submit();
	});
});

$('.btn_finance_approve').on('click', function()
{
	var id = $(this).val();
	$('#finance_review').val('1');
	$('#modal_summary_body').html('Approve Payroll '+$('#month_'+id).text()+' '+$('#year_'+id).text()+'?');
	$('#modal_summary').modal('show');
	$('#btn_confirm').on('click', function()
	{
		$(this).attr('disabled', true);
		$('#form_finance_'+id).submit();
	});
});

$('.btn_owner').on('click', function()
{
	var id = $(this).val();
	$('#modal_summary_body').html('Issue Payroll '+$('#month_'+id).text()+' '+$('#year_'+id).text()+'?');
	$('#modal_summary').modal('show');
	$('#btn_confirm').on('click', function()
	{
		$(this).attr('disabled', true);
		$('#form_owner_'+id).submit();
	});
});

$('.btn_paid').on('click', function()
{
	var id = $(this).val();
	$('#modal_summary_body').html('Pay Payroll '+$('#month_'+id).text()+' '+$('#year_'+id).text()+'?');
	$('#modal_summary').modal('show');
	$('#btn_confirm').on('click', function()
	{
		$(this).attr('disabled', true);
		$('#form_paid_'+id).submit();
	});
});
</script> 
@stop