@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li class="active"><a href="#"><i class="fa fa-dropbox"></i> Inventory</a></li>
  </ol>
</section>
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-10">
			        <div class="x_panel">
		                <h2>
		                	<i class="fa fa-dropbox"></i> Inventory List
	                	</h2>
		                <hr>
			            <div class="x_content">
			                <table id="inventoryTable" class="table table-striped dataTable">
							  	<thead>
							  	  	<tr>
								      	<th>#</th>
								      	<th>Name</th>
								      	<th>Size</th>
								      	<th>Block</th>
								      	<th>Start Date</th>
								      	<th>Finish Date</th>
								      	<th></th>
							    	</tr>
							  	</thead>
							  	<tbody>
							    @foreach($products as $key => $product)
							    	<tr>
							      		<td>{{$key+1}}</td>
								      	<td>{{$product['name']}}</td>
								      	<td>{{$product['size']}}</td>
								      	<td>{{$product['block']}}</td>
								      	<td>{{date("D, d M Y", strtotime($product['start_date']))}}</td>
								      	<td>{{date("D, d M Y", strtotime($product['finish_date']))}}</td>
								      	<td align="center">
								      		<a href="#" class="btn btn-sm btn-primary"> Export</a>
								      		<a href="#" class="btn btn-sm btn-primary"> Value Added</a>
								      		<a href="#" class="btn btn-sm btn-primary"> Sashimi</a>
							      		</td>
								    </tr>
							    @endforeach
								</tbody>
							</table>
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function () {
    $("#inventoryTable").DataTable(
    	{
    	
    	});
    });
</script>
@stop