@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="{{ URL::route('editor.variable.index') }}"><i class="fa fa-dollar"></i> Variable</a></li>
    @if(isset($variable))
    <li class="active"><i class="fa fa-pencil"></i> Edit</li>
    @else
    <li class="active"><i class="fa fa-plus"></i> Create</li>
    @endif
  </ol>
</section>
@actionStart('variable', 'create|update')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-5">
			        <div class="x_panel">
		                <h2>
		                	@if(isset($variable))
		                	<i class="fa fa-pencil"></i>
		                	@else
		                	<i class="fa fa-plus"></i> 
		                	@endif
		                	<i class="fa fa-dollar"></i> Variable
	                	</h2>
		                <hr>
			            <div class="x_content">
			                @include('errors.error')
	                        @if(isset($variable))
			                {!! Form::model($variable, array('route' => ['editor.variable.update', $variable->id], 'method' => 'PUT'))!!}
		                    @else
		                    {!! Form::open(array('route' => 'editor.variable.store'))!!}
		                    @endif
		                    {{ csrf_field() }}
		                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
		                    	{{ Form::label('name', 'Name') }}
		                        {{ Form::text('name', old('name'), array('class' => 'form-control', 'placeholder' => 'Name*', 'required' => 'true')) }}<br/>

		                        {{ Form::label('uom', 'Unit of Measurement') }}
		                        {{ Form::select('uom', ['kg' => 'Kilogram', 'pcs' => 'Pieces'], old('uom'), ['placeholder' => 'Select a Unit', 'class' => 'form-control']) }}<br/>

		                        {{ Form::label('type', 'Type') }}
		                        {{ Form::select('type', ['Reduction' => 'Reduction'], old('type'), ['placeholder' => 'Select a Type', 'class' => 'form-control']) }}<br/>

	                            <button type="submit" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
	                    	</div>
	                        {!! Form::close() !!}
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@actionEnd
@stop