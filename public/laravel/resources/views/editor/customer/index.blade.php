@extends('layouts.home.template')
@section('content')
<section class="content box box-solid">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<div class="x_panel">
					<h2>
						<i class="fa fa-truck"></i> Customer List
						@actionStart('customer', 'create')
						<a href="{{ URL::route('editor.customer.create') }}" class="btn btn-primary btn-lg pull-right"><i class="fa fa-plus"></i> Add</a>
						@actionEnd
					</h2>
					<hr>
					<div class="x_content">
						<table id="customerTable" class="table table-striped dataTable">
							<thead>
								<tr>
									<th>Name</th>
									<th>Address</th>
									<th>E-mail Address</th>
									<th>Phone Number</th>
									<th>Fax</th>
									<th>Website</th>
									<th>PIC</th>
									<th>Note</th>
									<th>Edit</th>
									<th>Delete</th>
								</tr>
							</thead>
							{{-- <tbody>
							@foreach($customers as $key => $customer)
							<tr>
								<td>{{$key+1}}</td>
								<td>{{$customer->name}}</td>
								<td>{{$customer->address}}</td>
								<td>{{$customer->email}}</td>
								<td>{{$customer->phone_number}}</td>
								<td>{{$customer->fax}}</td>
								<td>{{$customer->website}}</td>
								<td>{{$customer->pic}}</td>
								<td>{{$customer->note}}</td>
								<td align="center">
									@actionStart('customer', 'update')
									<div class="col-md-2 nopadding">
										<a href="{{ URL::route('editor.customer.edit', [$customer->id]) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
									</div>
									@actionEnd

									@actionStart('customer', 'delete')
									<div class="col-md-2 nopadding">
										{!! Form::open(array('route' => ['editor.customer.delete', $customer->id], 'method' => 'delete', 'class'=>'delete'))!!}
										{{ csrf_field() }}	                    				
										<button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-trash"></i></a></button>
										{!! Form::close() !!}
									</div>
									@actionEnd
								</td>
							</tr>
							@endforeach
						</tbody> --}}
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
</section>
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
	$(function() {
		$('#customerTable').DataTable({
			processing: true,
			serverSide: true,
			"pageLength": 25,
			"scrollY": "400px",
			ajax: "{{ url('editor/customer/data') }}",
			columns: [
			{ data: 'name', name: 'name' },
			{ data: 'address', name: 'address' },
			{ data: 'email', name: 'email' },
			{ data: 'phone_number', name: 'phone_number' },
			{ data: 'fax', name: 'fax' },
			{ data: 'website', name: 'website' },
			{ data: 'pic', name: 'pic' },
			{ data: 'note', name: 'note' },
			{ data: 'edit', name: 'edit' },
			{ data: 'delete', name: 'delete' }
			]
		});
	});
</script>

<script>
	$(".delete").on("submit", function(){
		return confirm("Do you want to delete this item?");
	});
</script>
@stop