@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="{{ URL::route('editor.role.index') }}"><i class="fa fa-vcard"></i> Role</a></li>
    @if(isset($roles))
    <li class="active"><a href="#"><i class="fa fa-pencil"></i> Edit</a></li>
    @else
    <li class="active"><a href="#"><i class="fa fa-plus"></i> Create</a></li>
    @endif
  </ol>
</section>
@actionStart('role', 'create|update')
<section class="content">
  <section class="content box box-solid">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-1"></div>
        <div class="col-md-5">
          <div class="x_panel">
            <h2>
              @if(isset($roles))
              <i class="fa fa-pencil"></i>
              @else
              <i class="fa fa-plus"></i> 
              @endif
              <i class="fa fa-vcard"></i> Role
            </h2>
            <hr>
            <div class="x_content">
              @include('errors.error')
              @if(isset($roles))
              {{ Form::model($roles, array('route' => array('editor.role.update'), 'method' => 'put')) }}
              @else
              {!! Form::open(array('route' => ['editor.role.store']))!!}
              @endif
              {{ csrf_field() }}
              <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                {{ Form::label('username', 'Username') }}
                @if(isset($username_array))
                {{ Form::select('user', $username_array, null, array('class' => 'form-control')) }}
                @else
                {{ Form::text('userDisplay', $username, array('disabled' => true, 'id' => 'userID', 'class' => 'form-control')) }}
                {{ Form::hidden('user', $id, array('id' => 'user'))}}
                @endif
                <br/>

                {{ Form::label('module', 'Module') }}
                @foreach($modules as $keymd => $valuemd)
                  <div class="form-group">
                    @php $cm = 0; @endphp
                    @if(isset($moduleUse))
                      @if(in_array($keymd, $moduleUse))
                        @php $cm = 1; @endphp
                        @php $iMC = $keymd; @endphp
                      @endif
                    @endif
                    {{ Form::checkbox('module['.$keymd.']', $keymd, $cm, ['id' => 'moduleid', 'class' => 'moduleC']) }} {{ $valuemd }}
                    <br>
                    @foreach($actions as $keyac => $valueac)
                      @php $ca = 0; @endphp
                      @if(isset($iMC))
                        @if(in_array($keyac, $actionUse[$iMC]))
                          @php $ca = 1; @endphp
                        @endif
                      @endif
                      {{ Form::checkbox('module['.$keymd.'][]', $keyac, $ca) }} {{ $valueac }}
                    @endforeach
                  </div>
                @endforeach

                <button type="submit" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>

              </div>
              {!! Form::close() !!} 
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</section>
@stop
@actionEnd

@section('scripts')
<script>
  function moduleIsChecked(){
    $(".moduleC").each(function () {
      var cThis =$(this).val();
      if($(this).prop("checked")){
        // console.log("enable");
        $("input:checkbox[name='module["+cThis+"][]']").removeAttr("disabled");
      }else {
        // console.log("disable");
        $("input:checkbox[name='module["+cThis+"][]']").prop("checked", false);
        $("input:checkbox[name='module["+cThis+"][]']").attr("disabled", true);
      }
    });
  }
  // make selected for selectbox
  function makeSelected(selector, value){
    var elementLength = $(selector+" option").length;
    for(var i = 0; i < elementLength; i++){
      // console.log(i);
      var Element = $(selector+" option:eq("+i+")");
      var valueElement = Element.val();
      if(valueElement == value){
        Element.attr("selected", true);
      }
    }
  }
  $(document).ready(function(){
    // # ROLES JS #
    moduleIsChecked();
    $(".moduleC").on("click", function () {
      var cThis =$(this).val();
      if($(this).prop("checked")){
        console.log("enable");
        $("input:checkbox[name='module["+cThis+"][]']").removeAttr("disabled");
      }else {
        console.log("disable");
        $("input:checkbox[name='module["+cThis+"][]']").prop("checked", false);
        $("input:checkbox[name='module["+cThis+"][]']").attr("disabled", true);
      }
    });
    $("#submitRoles").click(function(){
      var arrayVal = {};
      var arrayValModule = {};
      arrayVal['user'] = $("#user").val();
      // var tMRlength = $("#tableModule tbody tr").length;
      // for(var i= 0; i < tMRlength; i++) {
      //   var tMRV = $("#tableModule tbody tr:eq("+i+") td").eq(0).text();
      //   var getValFromtMRV = $("#module option:contains('"+tMRV+"')").val();
      //   var arrayValAction = [];
      //   $("#tableModule tbody tr:eq("+i+") td:eq(1) .labelAction").each(function(){
      //     arrayValAction.push($(this).text());
      //   });
      //   arrayValModule[getValFromtMRV] = arrayValAction;
      // };
      var keyModule;
      var arrayValAction = [];
      $("input:checkbox[name^='module']:checked").each(function(){
        if($(this).attr('id')=="moduleid"){
          keyModule = $(this).val();
          arrayValAction = [];
        }else {
          arrayValAction.push($(this).val());
          arrayValModule[keyModule] = arrayValAction;
        }
        // console.log(keyModule);
        console.log($(this).attr('name'));
        console.log($(this).val())
      });
      arrayVal['module'] = arrayValModule;
      console.log(arrayVal);
      var urlV = '{{ URL::route("editor.role.store") }}';
      var typeV = 'POST';
      if($("#userID").length > 0)
      {
        urlV = '{{ URL::route("editor.role.update") }}';
        typeV = 'PUT'
        console.log(urlV);
      }
      $.ajax({
          url : urlV,
          data : arrayVal,
          type : typeV,
          headers : {
            'X-CSRF-TOKEN' : $('meta[name="csrf_token"]').attr('content')
          },
          error : function(jqXHR, textStatus, errorThrown){
              console.log(jqXHR.responseText);
          },
          success : function(data, textStatus, jqXHR){
              console.log(jqXHR.responseText);
               window.location.replace('{{ URL::route("editor.role.index") }}');
          }
      });
    });
    $("#user").change(function(){
      $("input:checkbox").prop("checked", false);
      $.ajax({
        url : '{{ URL::route("get.defaultroles") }}',
        data : 'userID='+$(this).val(),
        type : 'GET',
        headers : {
          'X-CSRF-TOKEN' : $('meta[name="csrf_token"]').attr('content')
        },
        success : function(data, textStatus, jqXHR){
          // console.log(data);
          var key = Object.keys(data);
          for (var i = 0; i < key.length; i++) {
            $("input:checkbox[name='module["+key[i]+"]']").prop("checked", true);
            for (var x = 0; x < data[key[i]].length; x++) {
              $("input:checkbox[name='module["+key[i]+"][]'][value="+data[key[i]][x]+"]").prop("checked", true);
              // console.log(data[key[i]][x]);
            }
          }
          moduleIsChecked();
        }
      });
    });
    $("#module").change(function(){
        $("input:checkbox[name=actions]").prop("checked", false);
        var valModule = $("#module option:selected"). text();
        // console.log(valModule);
        $("#tableModule tbody tr").each(function(){
            if($(this).find('.labelModule').text() == valModule){
                console.log(valModule);
                var trTMIndex = $('tr:contains("'+valModule+'")').index();
                $('#tableModule tbody tr:eq('+trTMIndex+') td:eq(1) p').each(function(){
                    console.log($(this).text());
                    $("input:checkbox[value="+$(this).text()+"]").prop("checked", true);
                });
            }
        });
    });
    $("#addFormtoTable").click(function(){
      var tableValue = '';
      tableValue += '<tr>';
      var module = $("#module").val();
      var textModule = $("#module option:selected"). text();
      tableValue += '<td><p class="labelModule">'+textModule+'</p></td><td>';
      var actions = new Array();
      if(module == 0){
          $("input:checkbox[name=actions]").prop("checked", false);
          console.log("Must Select Module !");
      }
      else{
          $("input:checkbox[name=actions]:checked").each(function(){
              actions.push($(this).val());
              tableValue += '<p class="labelAction">'+$(this).val()+'</p>';
          });
          tableValue +='</td><td><a href="javascript:void(0)" id="deleteRow" class="btn btn-sm btn-danger" onClick="$(this).closest(\'tr\').remove();">Delete</a></td></tr>'
          $("#tableModule tbody tr").each(function(){
              if($(this).find('.labelModule').text() == textModule){
                  tableValue = '';
                  var trTMIndex = $('tr:contains("'+textModule+'")').index();
                  var tdTMIndex = $('td:contains("'+textModule+'")').index();
                  console.log(trTMIndex);
                  console.log(tdTMIndex);
                  $('#tableModule tbody tr:eq('+trTMIndex+') td').eq(tdTMIndex+1).html('');
                  $("input:checkbox[name=actions]:checked").each(function(){
                      console.log($(this).val());
                      $('#tableModule tbody tr:eq('+trTMIndex+') td').eq(tdTMIndex+1).append('<p class="labelAction">'+$(this).val()+'</p>');
                  });
              }
          });
          $("input:checkbox[name=actions]").prop("checked", false);
          $("#tableValue").append(tableValue);
          $("#module").val(0).select();
          actions = [];
      }
      // console.log(tableValue);
    });
  });
  // function stuff(){
  //   function foostuff(value, function(){
  //     $(document).ready(function(){
  //
  //     });
  //   });
  // }
</script>
@stop