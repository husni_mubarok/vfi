@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li><a href="{{ URL::route('editor.franchise_fee.index') }}"><i class="fa fa-industry"></i> Franchise Fee</a></li>
		<li class="active">
			@if(isset($franchise_fee))
			<i class="fa fa-pencil"></i> Edit
			@else
			<i class="fa fa-plus"></i> Create
			@endif
		</li>
	</ol>
</section>

<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12"> 
				<div class="col-md-12">
					<div class="x_panel">
						<h2>
							@if(isset($franchise_fee))
							<i class="fa fa-pencil"></i>
							@else
							<i class="fa fa-plus"></i> 
							@endif
							&nbsp;Franchise Fee
						</h2>
					</div>
					<hr>
					<div class="col-md-12">
					@include('errors.error')
					@if(isset($franchise_fee))
					{!! Form::model($franchise_fee, array('route' => ['editor.franchise_fee.update', $franchise_fee->id], 'files' => 'true', 'method' => 'PUT', 'class'=>'update', 'id' => 'form_payroll'))!!}
					@else
					{!! Form::open(array('route' => 'editor.franchise_fee.store', 'files' => 'true', 'class'=>'create', 'id' => 'form_payroll'))!!}
					@endif
					{{ csrf_field() }}
						<div class="x_content"> 
							
								<div class="col-md-6">
									{{ Form::label('month', 'Month') }}
									@if(isset($franchise_fee))
									{{ Form::select('month', $month_list, old('month'), ['class' => 'form-control', 'placeholder' => 'Select Month', 'id' => 'month', 'disabled' => 'true']) }}
									{{ Form::hidden('month', old('month')) }}
									@else
									{{ Form::select('month', $month_list, old('month'), ['class' => 'form-control', 'placeholder' => 'Select Month', 'id' => 'month']) }}
									@endif
									<br>
								</div>
								<div class="col-md-6">
									{{ Form::label('year', 'Year') }}
									@if(isset($franchise_fee))
									{{ Form::number('year', old('year'), ['class' => 'form-control', 'min' => date('Y') - 20, 'max' => date('Y') + 20, 'placeholder' => 'Input Year', 'id' => 'year', 'disabled' => 'true']) }}
									{{ Form::hidden('year', old('year')) }}
									@else
									{{ Form::number('year', old('year', date('Y')), ['class' => 'form-control', 'min' => date('Y') - 20, 'max' => date('Y') + 20, 'placeholder' => 'Input Year', 'id' => 'year']) }}
									@endif
									<br>
								</div>
								<div class="col-md-6">
								{{ Form::label('turnover', 'Turnover') }}
								{{ Form::number('turnover', old('turnover'), ['class' => 'form-control', 'id' => 'turnover', 'disabled' => 'true']) }}
								</div>
								
								<div class="col-md-6">
									{{ Form::label('royalty_percentage', 'Royalty Percentage') }}
									<div class="input-group">
										{{ Form::number('royalty_percentage', old('royalty_percentage', 5), ['class' => 'form-control', 'id' => 'royalty_percentage']) }}
										<span class="input-group-addon"><i class="fa fa-percent"></i></span>
									</div>
									<br>
								</div>
								<div class="col-md-12">
									{{ Form::label('royalty_value', 'Royalty Value') }}
									{{ Form::number('royalty_value', old('royalty_value'), ['class' => 'form-control', 'id' => 'royalty_value_text', 'disabled' => 'true']) }}
									{{ Form::hidden('royalty_value', old('royalty_value'), ['id' => 'royalty_value_value']) }}
									<br>
								</div>

								<div class="col-md-12">
									{{ Form::label('invoice_file', 'Invoice File (max 2 Mb)') }}
								</div>
								@if(isset($franchise_fee))
								<br>
								<a class="fancybox" rel="group" href="{{ Config::get('constants.path.uploads') }}/franchise_fee/{{ $franchise_fee->invoice_file }}">
									<img src="{{ Config::get('constants.path.uploads') }}/franchise_fee/thumbnail/{{ $franchise_fee->invoice_file }}" class="img-thumbnail img-responsive">
								</a>
								<br>
								@endif
								{{ Form::file('invoice_file') }}
								<br>

								<button type="button" id="btn_confirm" data-toggle="modal" data-target="#modal_confirm" class="btn btn-success"><i class="fa fa-check"></i> Save</button>
							
						</div>

					{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@stop

@section('modal')
<div class="modal fade" id="modal_confirm">
  	<div class="modal-dialog modal-md">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h4 class="modal-title">Franchise Fee Summary</h4>
      		</div>
      		<div class="modal-body">
      			<table class="table">
      				<tr>
      					<th>Period</th>
      					<td id="summary_period"></td>
      				</tr>
      				<tr>
      					<th>Turnover</th>
      					<td id="summary_turnover"></td>
      				</tr>
      				<tr>
      					<th>Royalty Percentage</th>
      					<td id="summary_royalty_percentage"></td>
      				</tr>
      				<tr>
      					<th>Royalty Value</th>
      					<td id="summary_royalty_value"></td>
      				</tr>
      			</table>
      		</div>
      		<div class="modal-footer">
      			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
      			<button type="button" id="btn_submit" class="btn btn-success"><i class="fa fa-check"></i> Submit</button>
      		</div>
    	</div>
  	</div>
</div>
@stop

@section('scripts')
<link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
<script>
$(document).ready(function()
{
	$(".fancybox").fancybox();
});

$('#btn_submit').on('click', function()
{
	$('#form_payroll').submit();
});

$('#btn_confirm').on('click', function()
{
	var period = $('#month option:selected').text()+' '+$('#year').val();
	var turnover = $('#turnover').val();
	var royalty_percentage = $('#royalty_percentage').val()+'%';
	var royalty_value = $('#royalty_value_value').val();

	$('#summary_period').text(period);
	$('#summary_turnover').text(turnover);
	$('#summary_royalty_percentage').text(royalty_percentage);
	$('#summary_royalty_value').text(royalty_value);
});

function get_turnover(month, year)
{
	$.ajax({
		url : '{{ URL::route('get.turnover') }}', 
		data : {'month' : month, 'year' : year}, 
		type : 'POST', 
		headers : {'X-CSRF-TOKEN' : $('meta[name = "csrf-token"]').attr('content')}, 
		success : function(data, textStatus, jqXHR)
		{
			$('#turnover').val(data).change();
		}, 
		error: function()
		{
			alert('Bulan ini tidak memiliki revenue!');
		}
	});
};

function calculate_royalty(turnover, royalty_percentage)
{
	turnover = parseFloat(turnover);
	royalty_percentage = parseFloat(royalty_percentage);
	//royalty_value = turnover / 100 * royalty_percentage;
	royalty_value = turnover / 100 * royalty_percentage;

	$('#royalty_value_text').val(royalty_value);
	$('#royalty_value_value').val(royalty_value);
};

$('#month, #year').on('change', function()
{
	var month = $('#month').val();
	var year = $('#year').val();
	if(month && year)
	{
		get_turnover(month, year);
	}
});

$('#turnover, #royalty_percentage').on('change', function()
{
	var turnover = $('#turnover').val();
	var royalty_percentage = $('#royalty_percentage').val();

	if(turnover && royalty_percentage)
	{
		calculate_royalty(turnover, royalty_percentage);
	}
});
</script>

@if(isset($franchise_fee))
<script>
var month = $('#month').val();
var year = $('#year').val();

get_turnover(month, year);
$.when($.ajax(get_turnover(month, year))).then(function()
{
	var turnover = $('#turnover').val();
	var royalty_percentage = $('#royalty_percentage').val();
	calculate_royalty(turnover, royalty_percentage);
});
</script>
@endif

@stop
