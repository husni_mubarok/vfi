@extends('layouts.administrator')

@section('content')
	@actionStart('users_privilege', 'read')
		<section class="content-header">
			<h1>
				Users Privilege
				 <small>Lorem Ipsum Dolor</small>
				 @actionStart('users_privilege', 'create')
				 	<a href="{{ URL::to('admin/users_privilege/create') }}" class="btn btn-primary pull-right">Create User Privilege</a>
				 @actionEnd
			</h1>
		</section>

		<section class="content">
			<div class="box box-default color-palette-box">
				<div class="box-body">
					<table id="privilege-table" class="table table-bordered table-hover table-responsive">
						<thead>
							<tr>
								<th>No</th>
								<th>Name</th>
								<th>Privilege</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							@foreach($users_privilege as $key => $up)
							<tr>
								<td>{{$key+1}}</td>
								<td>{{$up->email}}</td>
								<td>{{$up->privilege->name}}</td>
								<td style="text-align:center;">
									<a href="{{ URL::to('admin/users_privilege/' . $up->id . '/edit') }}" class="btn btn-sm btn-success" style="display:inline-block;">Update</a>
									{{ Form::open(array('url' => 'admin/users_privilege/' . $up->id, 'style' => 'display:inline-block;padding-left:15px;')) }}
			            {{ Form::hidden('_method', 'DELETE') }}
			            {{ Form::submit('Delete', array('class' => 'btn btn-sm btn-danger')) }}
			            {{ Form::close() }}
									<!-- <a href="javascript:void(0)" class="btn btn-sm btn-danger">Delete</a> -->
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</section>
	@actionEnd
@endsection
