@extends('layouts.administrator')

@section('content')
	@actionStart('actions', 'create|update')
	<section class="content-header">
		<h1>
			User Privilege
			 <small>Lorem Ipsum Dolor</small>
		</h1>
	</section>

	<section class="content">
		<div class="box box-default color-palette-box">
			<div class="box-header with-border">
				<h3 class="box-title"><i class="fa fa-tag"></i> Form</h3>
			</div>
			<div class="box-body">
				@if(isset($users_privilege))
		  		{{ Form::model($users_privilege, array('route' => array('users_privilege.update', $users_privilege->id), 'method' => 'put', 'class' => 'form-horizontal', 'role' => 'form')) }}
		  	@else
		  		{{ Form::open(array('url' => 'admin/users_privilege', 'class' => 'form-horizontal', 'role' => 'form')) }}
		  	@endif

				<div class="form-group">
					{{ Form::label('email', 'Email', array('class' => 'col-sm-2 control-label')) }}
					<div class="col-sm-10">
						@if(isset($emaillist))
      				{{ Form::select('email', $emaillist, null, array('class' => 'form-control')) }}
						@else
							{{ Form::text('email', Input::old('email'), array('disabled' => true, 'id' => 'email', 'class' => 'form-control')) }}
						@endif
					</div>
				</div>

				<div class="form-group">
					{{ Form::label('id_privilege', 'Privilege', array('class' => 'col-sm-2 control-label')) }}
					<div class="col-sm-10">
						{{ Form::select('id_privilege', $privilege, null, array('class' => 'form-control')) }}
					</div>
				</div>
				<hr>

				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
							{{ Form::submit('Submit', array('class' => 'btn btn-primary')) }}
					</div>
				</div>

				{{ Form::close() }}

			</div>
		</div>
	</section>

	@actionEnd
@endsection
