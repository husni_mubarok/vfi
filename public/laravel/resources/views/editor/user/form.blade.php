@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="{{ URL::route('editor.user.index') }}"><i class="fa fa-users"></i> User</a></li>
    @if(isset($user))
    <li class="active"><a href="#"><i class="fa fa-pencil"></i> Edit</a></li>
    @else
    <li class="active"><a href="#"><i class="fa fa-plus"></i> Create</a></li>
    @endif
  </ol>
</section>
@actionStart('user', 'create|update')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-5">
			        <div class="x_panel">
		                <h2>
		                	@if(isset($user))
		                	<i class="fa fa-pencil"></i>
		                	@else
		                	<i class="fa fa-plus"></i>
		                	@endif
		                	<i class="fa fa-users"></i>&nbsp;User
	                	</h2>
		                <hr>
			            <div class="x_content">
			                @include('errors.error')

			                @if(isset($user))
			                {!! Form::model($user, array('route' => ['editor.user.update', $user->id], 'method' => 'PUT', 'files' => 'true'))!!}
		                    @else
		                    {!! Form::open(array('route' => 'editor.user.store', 'files' => 'true'))!!}
		                    @endif
		                    {{ csrf_field() }}
		                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
		                    	{{ Form::label('username', 'Username') }}
		                    	@if(isset($user))
		                    	<p id="username">{{$user->username}}</p>
		                    	@else
		                        {{ Form::text('username', old('username'), ['class' => 'form-control', 'placeholder' => 'Username*', 'required' => 'true']) }}<br/>
		                        @endif
		                        

		                        {{ Form::label('email', 'E-mail Address') }}
		                        {{ Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => 'E-mail Address*', 'required' => 'true']) }}<br/>

		                        {{ Form::label('full_name', 'Name') }}
		                        <div id="full_name">
		                        	<div class="col-md-6 col-sm-6 col-xs-6">
		                        		{{ Form::text('first_name', old('first_name'), ['class' => 'form-control', 'placeholder' => 'First Name*', 'required' => 'true']) }}<br/>
		                        	</div>
		                        	<div class="col-md-6 col-sm-6 col-xs-6">
		                        		{{ Form::text('last_name', old('last_name'), ['class' => 'form-control', 'placeholder' => 'Last Name', 'required' => 'true']) }}<br/>
		                        	</div>
		                        </div>

		                        {{-- {{ Form::label('role_id', 'Role') }}
		                        @if(isset($user))
		                        <p id="role_id">{{$user->user_role->role->name}}</p>
		                        @else
		                        {{ Form::select('role_id', $roles, old('role_id'), ['class' => 'form-control', 'placeholder' => 'Select a role']) }}<br/>
		                        @endif --}}
		                        

		                        {{ Form::label('password', 'Password') }}
		                        @if(isset($user))
		                        <p><a id="password" href="{{ URL::route('editor.user.edit_password', [$user->id]) }}">Change password</a></p>
		                        @else
		                        {{ Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password*', 'required' => 'true']) }}<br/>

		                        {{ Form::label('password_confirmation', 'Confirm Password') }}
		                        {{ Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Repeat Password*', 'required' => 'true']) }}<br/>
		                        @endif

		                        {{ Form::label('image', 'Upload Profile Picture') }}
		                        {{ Form::file('image') }}<br/>

	                            <button type="submit" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
	                    	</div>
	                        {!! Form::close() !!}
			            </div>
			        </div>
		        </div>
		        @if(isset($user))
		        <div class="col-md-6 col-sm-6 col-xs-6">
		        <h2><i class="fa fa-image"></i>&nbsp;Profile Picture</h2>
		        <hr>
		        @if($user->filename == null)
					<br/><a class="fancybox" rel="group" href="{{Config::get('constants.path.uploads')}}/user/placeholder.png"><img src="{{Config::get('constants.path.uploads')}}/user/thumbnail/placeholder.png" class="img-thumbnail img-responsive"/></a><br/>
				@else
					<br/><a class="fancybox" rel="group" href="{{Config::get('constants.path.uploads')}}/user/{{$user->username}}/{{$user->filename}}"><img src="{{Config::get('constants.path.uploads')}}/user/{{$user->username}}/thumbnail/{{$user->filename}}" class="img-thumbnail img-responsive"/></a>
					<br/>
				@endif
		        </div>
				@endif
		    </div>
		</div>
	</section>
</section>
@actionEnd
@stop
@section('scripts')
<!-- Add fancyBox -->
<link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$(".fancybox").fancybox();
	});
</script>
@stop