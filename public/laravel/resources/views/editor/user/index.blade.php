@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active"><a href="#"><i class="fa fa-users"></i> User</a></li>
  </ol>
</section>
@actionStart('user', 'read')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-10">
			        <div class="x_panel">
		                <h2>
		                	<i class="fa fa-users"></i> User List
		                	@actionStart('user', 'create')
		                	<a href="{{ URL::route('editor.user.create') }}" class="btn btn-primary btn-lg pull-right"><i class="fa fa-plus"></i> Add</a>
		                	@actionEnd
	                	</h2>
		                <hr>
			            <div class="x_content">
			                <table id="roleTable" class="table table-hover dataTable">
							  	<thead>
							  	  	<tr>
								      	<th>#</th>
								      	<th>Username</th>
								      	<th>E-mail</th>
								      	<th>Name</th>
								      	{{-- <th>Role</th> --}}
								      	<th>Register Date</th>
								      	<th>Action</th>
							    	</tr>
							  	</thead>
							  	<tbody>
							    @foreach($users as $key => $user)
							    	<tr>
							      		<td>{{$key+1}}</td>
								      	<td>{{$user->username}}</td>
								      	<td>{{$user->email}}</td>
								      	<td>{{$user->first_name}} {{$user->last_name}}</td>
								      	{{-- <td>{{$}}</td> --}}
								      	<td>{{date("d-m-Y h:i:s", strtotime($user->created_at))}}</td>
								      	<td align="center">
								      		<div class="col-md-2 nopadding">
								      			<a href="{{ URL::route('editor.user.detail', [$user->id]) }}" class="btn btn-primary btn-sm"><i class="fa fa-search"></i></a>
								      		</div>
								      		@actionStart('user', 'update')
								      		<div class="col-md-2 nopadding">
								      			<a href="{{ URL::route('editor.user.edit', [$user->id]) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
							      			</div>
							      			@actionEnd
							      			
						      				@actionStart('user', 'delete')
							      			<div class="col-md-2 nopadding">
								      			{!! Form::open(array('route' => ['editor.user.delete', $user->id], 'method' => 'delete'))!!}
		                    					{{ csrf_field() }}	                    				
								      			<button type="submit" class="btn btn-primary btn-sm" onclick="return confirm('Delete this user?')"><i class="fa fa-trash"></i></button>
								      			{!! Form::close() !!}
							      			</div>
							      			@actionEnd
							      		</td>
								    </tr>
							    @endforeach
								</tbody>
							</table>
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@actionEnd
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function () {
    $("#roleTable").DataTable(
    	{
    		"scrollX": true
    	});
    });
</script>
@stop