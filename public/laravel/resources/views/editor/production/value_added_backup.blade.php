@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="{{ URL::route('editor.production.index') }}"><i class="fa fa-cubes"></i> Product</a></li>
    <li class="active"><i class="fa fa-book"></i> Recipe</li>
  </ol>
</section>
@actionStart('production', 'read')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-5">
			        <div class="x_panel">
			        	<div class="col-md-8">
			                <h2>
			                	<i class="fa fa-cubes"></i> 
			                	<i class="fa fa-book"></i> 
			                	{{$production->name}} Recipes
		                	</h2>
	                	</div>
	                	<div class="col-md-4">
	                		@if($production->recipe)
	                		<a href="{{ URL::route('editor.production.edit_recipe', [$production->recipe->id]) }}" class="btn btn-md btn-primary pull-right"><i class="fa fa-pencil"></i> Edit</a>
	                		@else
	                		<a href="{{ URL::route('editor.production.create_recipe', [$production->id]) }}" class="btn btn-md btn-primary pull-right"><i class="fa fa-pencil"></i> Write Recipe</a>
	                		@endif
	                	</div>
		                <hr>
		                @if($production->recipe)
			            <div class="x_content">
		                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
		                    	<table class="table">
		                    	<thead>
		                    		<tr>
		                    			<th>Recipe</th>
		                    			<th>Ingredient</th>
		                    		</tr>
	                    		</thead>
	                    		<tbody>
                    				<tr>
                    					<td>
                    						{{$production->recipe->name}}
                    						<br>
                    						(Qty: {{$production->recipe->qty}} kg)
                    						<br>
                    						{{$production->recipe->description}}
                						</td>
                    					<td>
                    						<table class="table">
                    							@foreach($production->recipe->recipe as $item)
                    							<tr>
                    								<td>{{$item->item->name}} [
                    									@foreach($item->item->item_custom_detail as $item_custom_detail)
                    									{{$item_custom_detail->custom_detail->value}}
                    									@endforeach
                    									]
                    								</td>
                    								<td>{{$item->qty}} {{$item->detail_uom->name}}</td>
                    								<td>{{$item->description}}</td>
                    							</tr>
                    							@endforeach
                    						</table>
                    					</td>
                    				</tr>
	                    		</tbody>
		                    	</table>
	                    	</div>
			            </div>
			            @endif
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@actionEnd
@stop

@section('scripts')

@stop