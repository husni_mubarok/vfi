@extends('layouts.home.template')
@section('content')
<div class="header_menu">
    <a href="{{ URL::route('editor.fish.index') }}">FISH</a>
    <a href="{{ URL::route('editor.production.index') }}">PRODUCT</a>
</div>
@actionStart('production', 'update')
<section class="content">
	<div class="row">
	    <div class="col-md-12 col-sm-12 col-xs-12">
	    	<div class="col-md-1"></div>
	    	<div class="col-md-10">
		        <div class="x_panel">
	                <h2>
	                	<i class="fa fa-cubes"></i> 
	                	<i class="fa fa-book"></i> 
	                	@if(isset($production_recipe))
	                	{{$production_recipe->product->name}} Ingredients
	                	@else
	                	{{$production->name}} Ingredients
	                	@endif
                	</h2>
	                <hr>
		            <div class="x_content">
						@if(isset($production_recipe))
						{!! Form::model($production_recipe, array('route' => ['editor.production.update_recipe', $production_recipe->id], 'method' => 'PUT'))!!}
						{{ csrf_field() }}
						<div class="col-md-6 col-sm-6 col-xs-6 form-group">
							{{ Form::hidden('name', $production_recipe->name) }}

							{{ Form::label('description', 'Description') }}
		                    {{ Form::text('description', old('description'), ['class' => 'form-control']) }}
		                    <br>

		                    {{ Form::label('qty', 'Min. Qty') }}
		                    <div class="input-group">
		                    	{{ Form::number('qty', old('qty'), ['class' => 'form-control']) }}
		                    	<div class="input-group-addon">
		                    		<span>kg</span>
		                    	</div>
		                    </div>
						</div>

						<div class="col-md-12 col-sm-12 col-xs-12">
		                    {{ Form::label('ingredients', 'Ingredients') }}
		                    <!-- Nav tabs -->
							<ul class="nav nav-tabs" role="tablist">
								@foreach($ingredients as $key => $ingredient)
							  	<li class="nav-item">
							    	<a class="nav-link" data-toggle="tab" href="#tab{{$key}}" role="tab">{{$ingredient['name']}}</a>
							  	</li>
							  	@endforeach
							</ul>

							<!-- Tab panes -->
							<div class="tab-content">
								@foreach($ingredients as $key => $ingredient)
							  	<div class="tab-pane" id="tab{{$key}}" role="tabpanel">
							  		<table class="table table-bordered">
							  		<thead>
							  		<tr>
							  			<th><i class="fa fa-checkbox"></i></th>
							  			<th>Name</th>
							  			<th>Criteria/Brand</th>
							  			<th>Qty</th>
							  			<th>UoM</th>
							  			<th>Description</th>
							  		</tr>
							  		</thead>
							  		<tbody>
						  			@foreach($ingredient['type'] as $type)
						  			<tr class="ingredient_row">
						  				<td width="2%"><input type="checkbox"  class="checkbox_type" id="{{$type['id']}}"></td>
						  				<td width="15%">{{$type['name']}}</td>
						  				<td width="20%">
						  					<select name="brand[{{$type['id']}}]" class="form-control brand" id="id_detail_{{$type['id']}}" disabled>
						  						<option selected disabled value=""> - Pilih brand - </option>
						  						@if($type['brand'])
						  						@foreach($type['brand'] as $brand_key => $brand)
						  						<option value="{{$brand_key}}">{{$brand['name']}} ({{$brand['desc']}}) </option>
						  						@endforeach
						  						@endif
						  					</select>
						  				</td>
						  				<td width="10%">{{ Form::number('quantity['.$type['id'].']', null, ['class' => 'form-control input_quantity', 'id' => 'quantity_'.$type['id'], 'disabled' => 'true']) }}</td>
						  				<td width="10%">
						  					<select name="uom[{{$type['id']}}]" class="form-control" id="id_uom_{{$type['id']}}" disabled>

						  					</select>
						  				</td>
						  				<td>
						  					<input type="text" name="item_desc[{{$type['id']}}]" class="form-control" id="item_desc_{{$type['id']}}" disabled>
						  				</td>
						  			</tr>
						  			@endforeach
						  			</tbody>
							  		</table>
							  	</div>
							  	@endforeach
							</div>

                            <button type="submit" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
                      	</div>

						@else

		                @include('errors.error')
	                    {!! Form::open(array('route' => ['editor.production.store_recipe', $production->id]))!!}
	                    {{ csrf_field() }}
	                    <div class="col-md-6 col-sm-6 col-xs-6 form-group">
	                    	{{ Form::hidden('name', $production->name) }}
	                    	{{-- {{ Form::label('name', 'Name') }} --}}
		                    {{-- {{ Form::text('name', old('name'), ['class' => 'form-control', 'required' => 'true']) }} --}}
		                    {{-- <br> --}}

		                    {{ Form::label('description', 'Description') }}
		                    {{ Form::text('description', old('description'), ['class' => 'form-control']) }}
		                    <br>

		                    {{ Form::label('qty', 'Min. Qty') }}
		                    <div class="input-group">
		                    	{{ Form::number('qty', old('qty'), ['class' => 'form-control']) }}
		                    	<div class="input-group-addon">
		                    		<span>kg</span>
		                    	</div>
		                    </div>
		                    <br>
	                    </div>

						<div class="col-md-12 col-sm-12 col-xs-12">
		                    {{ Form::label('ingredients', 'Ingredients') }}
		                    <!-- Nav tabs -->
							<ul class="nav nav-tabs" role="tablist">
								@foreach($ingredients as $key => $ingredient)
							  	<li class="nav-item">
							    	<a class="nav-link" data-toggle="tab" href="#tab{{$key}}" role="tab">{{$ingredient['name']}}</a>
							  	</li>
							  	@endforeach
							</ul>

							<!-- Tab panes -->
							<div class="tab-content">
								@foreach($ingredients as $key => $ingredient)
							  	<div class="tab-pane" id="tab{{$key}}" role="tabpanel">
							  		<table class="table table-bordered">
							  		<thead>
							  		<tr>
							  			<th><i class="fa fa-checkbox"></i></th>
							  			<th>Name</th>
							  			<th>Criteria/Brand</th>
							  			<th>Qty</th>
							  			<th>UoM</th>
							  			<th>Description</th>
							  		</tr>
							  		</thead>
							  		<tbody>
						  			@foreach($ingredient['type'] as $type)
						  			<tr class="ingredient_row">
						  				<td width="2%"><input type="checkbox"  class="checkbox_type" id="{{$type['id']}}"></td>
						  				<td width="15%">{{$type['name']}}</td>
						  				<td width="20%">
						  					<select name="brand[{{$type['id']}}]" class="form-control brand" id="id_detail_{{$type['id']}}" disabled>
						  						<option selected disabled value=""> - Pilih brand - </option>
						  						@if($type['brand'])
						  						@foreach($type['brand'] as $brand_key => $brand)
						  						<option value="{{$brand_key}}">{{$brand['name']}} ({{$brand['desc']}}) </option>
						  						@endforeach
						  						@endif
						  					</select>
						  				</td>
						  				<td width="10%">{{ Form::number('quantity['.$type['id'].']', null, ['class' => 'form-control input_quantity', 'id' => 'quantity_'.$type['id'], 'disabled' => 'true']) }}</td>
						  				<td width="10%">
						  					<select name="uom[{{$type['id']}}]" class="form-control" id="id_uom_{{$type['id']}}" disabled>

						  					</select>
						  				</td>
						  				<td>
						  					<input type="text" name="item_desc[{{$type['id']}}]" class="form-control" id="item_desc_{{$type['id']}}" disabled>
						  				</td>
						  			</tr>
						  			@endforeach
						  			</tbody>
							  		</table>
							  	</div>
							  	@endforeach
							</div>

                            <button type="submit" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
                    	</div>
                        @endif
                        {!! Form::close() !!}
		            </div>
		        </div>
	        </div>
	    </div>
	</div>
</section>
@actionEnd
@stop

@section('scripts')
<script>
$(document).ready(function(){
	$(".checkbox_type").on('change', function() {
		var id = $(this).prop('id');
		if($(this).prop('checked'))
		{
			$('#id_detail_'+id).attr('disabled', false);
		} else {
			$('#quantity_'+id).val('');
			$('#id_detail_'+id).val('');
			$('#item_desc_'+id).val('');
			$('#quantity_'+id).attr('disabled', true);
			$('#id_detail_'+id).attr('disabled', true);	
			$('#id_uom_'+id).attr('disabled', true);	
			$('#item_desc_'+id).attr('disabled', true);
		}
	});

	$(".brand").on('change', function() {
		var item_id = $(this).val();
		var type_id = $(this).closest('tr').find('td:eq(0)').find('input:checkbox:first').prop('id');
		$("#id_uom_"+type_id).empty();
		var select_option = '';
		$.ajax({
	        url : '{{ URL::route('get.item_uom') }}',
	        data : {'item_id':$(this).val()},
	        type : 'GET',
	        headers : {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
	        success : function(data, textStatus, jqXHR){
	        	if(data.length > 0)
				{        		
		            jQuery.each(data, function(i, val)
		            {
		            	select_option += '<option value="'+val['uom_id']+'">'+val['uom_name']+'</option>'
		            });
		            $('#quantity_'+type_id).attr('disabled', false);
					$('#id_uom_'+type_id).attr('disabled', false);
					$('#item_desc_'+type_id).attr('disabled', false);
	            } 
	            else 
	            {
	            	select_option += '<option selected disabled> Item belum memiliki UoM </option>';
	            }            
	            $("#id_uom_"+type_id).append(select_option);
	        },
	        error : function()
	        {
	        	select_option += '<option selected disabled> Gagal mendapatkan UoM </option>';
	        	$("#id_uom_"+type_id).append(select_option);
	        },
	    })
	});
});
</script>

@if(isset($production_recipe))
<script>
$(document).ready(function(){
	$.ajax({
        url : '{{ URL::route('get.recipe_items') }}',
        data : {'recipe_id' : {!! $production_recipe->id !!} },
        type : 'GET',
        headers : {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
        success : function(data, textStatus, jqXHR){
            jQuery.each(data, function(i, val)
            {
            	$("#"+i).attr('checked', true).change();
            	$("#id_detail_"+i).val(val['item_id']);
            	var select_option = '';
            	$.ajax({
			        url : '{{ URL::route('get.item_uom') }}',
			        data : {'item_id':val['item_id']},
			        type : 'GET',
			        headers : {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
			        success : function(data, textStatus, jqXHR){
			        	if(data.length > 0)
						{        		
				            jQuery.each(data, function(i, val)
				            {
				            	select_option += '<option value="'+val['uom_id']+'">'+val['uom_name']+'</option>'
				            });
				            $('#quantity_'+i).attr('disabled', false);
							$('#id_uom_'+i).attr('disabled', false);
							$('#item_desc_'+i).attr('disabled', false);
			            	$("#id_uom_"+i).append(select_option);
			            	$("#quantity_"+i).val(val['qty']);
			            	$("#id_uom_"+i).val(val['detail_uom_id']);
			            	$("#item_desc_"+i).val(val['description']);
			            } 
			            else 
			            {
			            	select_option += '<option selected disabled> Item belum memiliki UoM </option>';
			            	$("#id_uom_"+i).append(select_option);
			            }            
			        },
			        error : function()
			        {
			        	select_option += '<option selected disabled> Gagal mendapatkan UoM </option>';
			        	$("#id_uom_"+i).append(select_option);
			        },
			    })
            	
            })
        },
        error : function()
        {
            console.log('error');
        },
    })
});
</script>
@endif
@stop