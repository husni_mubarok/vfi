@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active"><i class="fa fa-cubes"></i> Product</li>
  </ol>
</section>
@actionStart('production', 'read')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-10">
			        <div class="x_panel">
		                <h2>
		                	<i class="fa fa-cubes"></i> Product List
		                	@actionStart('production', 'create')
		                	<a href="{{ URL::route('editor.production.create') }}" class="btn btn-primary btn-lg pull-right"><i class="fa fa-plus"></i> Add</a>
		                	@actionEnd
	                	</h2>
		                <hr>
			            <div class="x_content">
			                <table id="variableTable" class="table table-striped dataTable">
							  	<thead>
							  	  	<tr>
								      	<th width="5%">#</th>
								      	<th width="30%">Name</th>
								      	<th width="30%">Description</th>
								      	<th width="10%">Type</th>
								      	<th width="10%"></th>
							    	</tr>
							  	</thead>
							  	<tbody>
							    @foreach($productions as $key => $production)
							    	<tr>
							      		<td>{{$key+1}}</td>
								      	<td>[{{$production->fish->name}}] {{$production->name}}</td>
								      	<td>{{$production->description}}</td>
								      	<td>
								      		@actionStart('production', 'update')
								      		@if($production->type == 'regular')
								      		Regular
								      		@elseif($production->type == 'sashimi')
								      		{{-- <a href="{{ URL::route('editor.production.detail', [$production->id]) }}" class="btn btn-sm btn-primary"> --}}
								      		{{-- <i class="fa fa-search"></i>  --}}
								      		Sashimi
								      		{{-- </a> --}}
								      		@elseif($production->type == 'value_added')
								      		<a href="{{ URL::route('editor.production.detail', [$production->id]) }}" class="btn btn-sm btn-primary">
								      		<i class="fa fa-search"></i> 
								      		Value Added
								      		</a>
								      		@endif
								      		@actionEnd
								      	</td>
								      	<td align="center">
								      		<div class="col-md-2 nopadding">
								      			@actionStart('production', 'update')
								      			<a href="{{ URL::route('editor.production.edit', [$production->id]) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
								      			@actionEnd
								      		</div>
							      			<div class="col-md-2 nopadding">
							      				@actionStart('production', 'delete')
								      			{!! Form::open(array('route' => ['editor.production.delete', $production->id], 'method' => 'delete'))!!}
		                    					{{ csrf_field() }}	                    				
								      			<button type="submit" class="btn btn-primary btn-sm" onclick="return confirm('Confirm delete?')"><i class="fa fa-trash"></i></a></button>
								      			{!! Form::close() !!}
								      			@actionEnd
							      			</div>
							      		</td>
								    </tr>
							    @endforeach
								</tbody>
							</table>
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@actionEnd
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function () {
    $("#variableTable").DataTable(
    	{
    		"scrollX": true
    	});
    });
</script>
@stop