@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="{{ URL::route('editor.production.index') }}"><i class="fa fa-cubes"></i> Production</a></li>
    <li class="active"><i class="fa fa-dollar"></i> Variable</li>
  </ol>
</section>
@actionStart('production', 'update')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-5">
			        <div class="x_panel">
		                <h2>
		                	<i class="fa fa-cubes"></i>
		                	<i class="fa fa-dollar"></i> {{$production->name}} Variable
	                	</h2>
		                <hr>
			            <div class="x_content">
			                @include('errors.error')
		                    {!! Form::open(array('route' => ['editor.production.update_variable', $production->id]))!!}
		                    {{ csrf_field() }}
		                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
	                    	<table id="productTable" class="table table-sm table-hover">
	                    	<thead>
		                    	<tr>
		                    		<th><i class="fa fa-check-square-o"></i></th>
		                    		<th>Name.</th>
		                    		<th>UoM</th>
		                    		<th>Type</th>
		                    	</tr>
	                    	</thead>
	                    	<tbody>
		                    	@foreach($variables as $key => $variable)
		                    	<tr>
		                    		<td>
			                    		@if(in_array($variable->id, $production->production_variable->pluck('variable_id')->toArray()))
				                    	{{ Form::checkbox('variable[]', $variable->id, true) }}
				                    	@else
				                    	{{ Form::checkbox('variable[]', $variable->id) }}
				                    	@endif
			                    	</td>
			                    	<td>{{$variable->name}}</td>
			                    	<td>{{$variable->uom}}</td>
			                    	<td>{{$variable->type}}</td>
	                    		</tr>
		                    	@endforeach	
                    		</tbody>
							</table>	                    		
                            <button type="submit" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
	                    	</div>
	                        {!! Form::close() !!}
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@actionEnd
@stop