@extends('layouts.home.template')
@section('content')
<section class="content box box-solid">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-1"></div>
			<div class="col-md-5">
				<div class="x_panel">
					<h2>
						@if(isset($item))
						<i class="fa fa-pencil"></i>
						@else
						<i class="fa fa-plus"></i> 
						@endif
						<i class="fa fa-bar-chart"></i> Item
					</h2>
					<hr>
					<div class="x_content">
						@include('errors.error')
						@if(isset($item))
						{!! Form::model($item, array('route' => ['editor.item.update', $item->id], 'method' => 'PUT', 'class'=>'update'))!!}
						@else
						{!! Form::open(array('route' => 'editor.item.store', 'class'=>'create'))!!}
						@endif
						{{ csrf_field() }}
						<div class="col-md-12 col-sm-12 col-xs-12 form-group">
							{{ Form::label('name', 'Name') }}
							{{ Form::text('name', old('name'), array('class' => 'form-control', 'placeholder' => 'Name*', 'required' => 'true')) }}<br/>
							
							{{ Form::label('Master Item') }}
							{{ Form::select('id_master_item', $master_item_list, old('id_master_item'), array('class' => 'form-control', 'required' => 'true')) }}<br/>

							{{ Form::label('description', 'Description') }}
							{{ Form::text('description', old('description'), array('class' => 'form-control', 'placeholder' => 'Description*', 'required' => 'true')) }}<br/>

							{{ Form::label('price', 'Price') }}
							{{ Form::number('price', old('price'), array('class' => 'form-control', 'placeholder' => 'Price*', 'required' => 'true')) }}<br/>
							<button type="submit" class="btn btn-success pull-right"><i class="fa fa-check"></i> Save</button>
							<a href="{{ URL::route('editor.item.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Exit</a>
						</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@stop

@section('scripts')
<script>
	$(".update").on("submit", function(){
		return confirm("Do you want to update this item?");
	});

	$(".create").on("submit", function(){
		return confirm("Do you want to create this item?");
	});
</script>
@stop