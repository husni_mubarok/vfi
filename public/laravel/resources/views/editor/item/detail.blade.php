@extends('layouts.home.template')
@section('content')
<section class="content box box-solid">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-1"></div>
			<div class="col-md-5">
				<div class="x_panel">
					<h2>
						@if(isset($item))
						<i class="fa fa-pencil"></i>
						@else
						<i class="fa fa-plus"></i> 
						@endif
						<i class="fa fa-bar-chart"></i> Detail Item
					</h2>
					<hr>
					<div class="x_content">
						@include('errors.error')
						{!! Form::model($item, array('route' => ['editor.item.storedetail', $item->id, 'method' => 'PUT']))!!}
						{{ csrf_field() }}
						<div class="col-md-12 col-sm-12 col-xs-12 form-group">
							{{ Form::label('name', 'Name') }}
							{{ Form::text('name', old('name'), array('class' => 'form-control', 'placeholder' => 'Name*', 'required' => 'true', 'disabled' => 'disabled')) }}<br/>

							{{ Form::label('Master Item') }}
							{{ Form::select('id_master_item', $master_item_list, old('id_master_item'), array('class' => 'form-control', 'required' => 'true', 'disabled' => 'disabled')) }}<br/>

							{{ Form::label('description', 'Description') }}
							{{ Form::text('description', old('description'), array('class' => 'form-control', 'placeholder' => 'Description*', 'required' => 'true', 'disabled' => 'disabled')) }}<br/>

							{{ Form::label('price', 'Price') }}
							{{ Form::text('price', old('price'), array('class' => 'form-control', 'placeholder' => 'Price', 'required' => 'true', 'disabled' => 'disabled')) }}<br/>
							<hr>

							{{ Form::label('Detail') }}
							{{ Form::select('id_custom_detail', $cmb_item_custom_detail, old('id_custom_detail'), array('class' => 'form-control', 'required' => 'true')) }}<br/>
							<button type="submit" class="btn btn-success pull-right" style="margin-right: 10px" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
							<a href="{{ URL::route('editor.item.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>

							<a href="{{ URL::route('editor.item.itemuom', [$item->id]) }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-forward"></i> UOM</a>
						</div>
						{!! Form::close() !!}
					</div>

					<div class="col-md-12 col-sm-12 col-xs-12">          
						<table class="table table-bordered" id="detail_table">
							<thead>
								<tr>
									<th>#</th>
									<th>Name</th>
									<th>Value</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach($item_custom_detail as $key => $item_custom_details)
								<tr>
									<td>{{$key+1}}</td>
									<td>{{$item_custom_details->name}}</td>
									<td>{{$item_custom_details->value}}</td>
									<td align="center">
										<div class="col-md-2 nopadding">                            
											<a href="javascript:void(0)" onclick="editValue(this, {{ $item_custom_details->id }}); showeditmodal();" class="btn btn-default btn-sm">
												<i class="fa fa-edit"></i></a>
											</div>
											<div class="col-md-2 nopadding">
												{!! Form::open(array('route' => ['editor.item.deletedetail', $item_custom_details->id], 'method' => 'delete', 'class'=>'delete'))!!}
												{{ csrf_field() }}                              
												<button type="submit" class="btn btn-default btn-sm"><i class="fa fa-trash"></i></a></button>
												{!! Form::close() !!}
											</div>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table> 
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Modal -->
	<div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Edit Detail Item </h4>
				</div>
				<div class="modal-body">

					@if(isset($item))
					{!! Form::model($item, array('route' => ['editor.item.updatedetail', $item->id], 'method' => 'PUT', 'class'=>'update'))!!}
					@else

					@endif
					{{ csrf_field() }}

					{{ Form::hidden('iddetail', old('iddetail'), array('class' => 'form-control', 'placeholder' => 'iddetail *', 'required' => 'true', 'id' => 'iddetail')) }}

					{{ Form::label('Detail') }}
					{{ Form::select('id_custom_detail_edit', $cmb_item_custom_detail, old('id_custom_detail_edit'), array('class' => 'form-control', 'required' => 'true', 'id' => 'id_custom_detail_edit')) }}<br/>

					<br/>
					<button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Back</button>
					<button type="submit" id="btnadd" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-plus"></i> Edit</button>

					<br/>
				</div>
				{!! Form::close() !!} 
			</div>

		</div>      
	</div>
</div>
</div>
@stop

@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script>
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>

<script>

	function editValue(str, id){
  	// console.log("test");
  	var prd = id;
  	var id_custom_detail_edit = $(str).closest('tr').find('td:eq(1)').text();    
  	$("#id_custom_detail_edit").val(id_custom_detail_edit);

  	$('#id_custom_detail_edit option[value="' + id_custom_detail_edit +'"]').prop("selected", true);

  	$("#iddetail").val(prd);
    // console.log(id);
}

function showeditmodal(){
	$('#myModalEdit').modal();
}
</script>

<script>
	$(".update").on("submit", function(){
		return confirm("Do you want to update this item?");
	});

	$(".create").on("submit", function(){
		return confirm("Do you want to create this item?");
	});

	$(".delete").on("submit", function(){
		return confirm("Do you want to delete this item?");
	});

</script>
@stop