@extends('layouts.home.template')
@section('content')
<section class="content box box-solid">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-1"></div>
			<div class="col-md-5">
				<div class="x_panel">
					<h2>
						@if(isset($item))
						<i class="fa fa-pencil"></i>
						@else
						<i class="fa fa-plus"></i> 
						@endif
						<i class="fa fa-bar-chart"></i> Detail UOM
					</h2>
					<hr>
					<div class="x_content">
						<center>
							@if(Session::has('success'))
							<div class="alert alert-info">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<strong>Error, </strong> {{ Session::get('message', '') }}
							</div>
							@endif
							<br/>
						</center>

						@include('errors.error')
						{!! Form::model($item, array('route' => ['editor.item.storeitemuom', $item->id, 'class'=>'create', 'method' => 'PUT']))!!}
						{{ csrf_field() }}
						<div class="col-md-12 col-sm-12 col-xs-12 form-group">
							{{ Form::label('name', 'Name') }}
							{{ Form::text('name', old('name'), array('class' => 'form-control', 'placeholder' => 'Name*', 'required' => 'true', 'disabled' => 'disabled')) }}<br/>

							{{ Form::label('Master Item') }}
							{{ Form::select('id_master_item', $master_item_list, old('id_master_item'), array('class' => 'form-control', 'required' => 'true', 'disabled' => 'disabled')) }}<br/>

							{{ Form::label('description', 'Description') }}
							{{ Form::text('description', old('description'), array('class' => 'form-control', 'placeholder' => 'Description*', 'required' => 'true', 'disabled' => 'disabled')) }}<br/>

							{{ Form::label('price', 'Price') }}
							{{ Form::text('price', old('price'), array('class' => 'form-control', 'placeholder' => 'Price', 'required' => 'true', 'disabled' => 'disabled')) }}<br/>
							<hr>

							<hr>
							<h4>Detail UOM</h4>
							<hr>
							{{ Form::select('id_uom', $cmb_uom, old('id_uom'), array('class' => 'form-control', 'required' => 'true')) }}<br/>

							{{ Form::label('Name') }}
							{{ Form::select('nameuom', $cmb_unit, old('nameuom'), array('class' => 'form-control', 'required' => 'true')) }}<br/>

							{{ Form::number('value', old('value'), array('class' => 'form-control', 'placeholder' => 'Value*', 'required' => 'true')) }}<br/>

							{{ Form::text('initial', old('initial'), array('class' => 'form-control', 'placeholder' => 'Initial*', 'required' => 'true')) }}<br/>

							{{ Form::text('descriptionuom', old('descriptionuom'), array('class' => 'form-control', 'placeholder' => 'Description*', 'required' => 'true')) }}<br/>

							<button type="submit" class="btn btn-success pull-right"><i class="fa fa-check"></i> Save</button>
							<a href="{{ URL::route('editor.item.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
							<a href="{{ URL::route('editor.item.detail', [$item->id]) }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-forward"></i> Detail</a>
						</div>
						{!! Form::close() !!}
					</div>

					<div class="col-md-12 col-sm-12 col-xs-12">          
						<table class="table table-bordered" id="detail_table">
							<thead>
								<tr>
									<th>#</th>
									<th>Name</th>
									<th>Value</th>
									<th>Initial</th>
									<th>Description</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach($item_uom as $key => $item_uoms)
								<tr>
									<td>{{$key+1}}</td>
									<td>{{$item_uoms->name}}</td>
									<td>{{$item_uoms->value}}</td>
									<td>{{$item_uoms->initial}}</td>
									<td>{{$item_uoms->description}}</td>
									<td align="center">
										<div class="col-md-2 nopadding">                            
											<a href="javascript:void(0)" onclick="editValue(this, {{ $item_uoms->id }}); showeditmodal();" class="btn btn-default btn-sm">
												<i class="fa fa-edit"></i></a>
											</div>
											<div class="col-md-2 nopadding">
												{!! Form::open(array('route' => ['editor.item.deleteitemuom', $item_uoms->id], 'method' => 'delete', 'class'=>'delete'))!!}
												{{ csrf_field() }}                              
												<button type="submit" class="btn btn-default btn-sm"><i class="fa fa-trash"></i></a></button>
												{!! Form::close() !!}
											</div>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table> 
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Modal -->
	<div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Edit Item UOM </h4>
				</div>
				<div class="modal-body">

					@if(isset($item))
					{!! Form::model($item, array('route' => ['editor.item.updateitemuom', $item->id], 'method' => 'PUT', 'class'=>'update'))!!}
					@else

					@endif
					{{ csrf_field() }}

					{{ Form::hidden('iddetail', old('iddetail'), array('class' => 'form-control', 'placeholder' => 'iddetail *', 'required' => 'true', 'id' => 'iddetail')) }}

					{{ Form::select('id_uomedit', $cmb_uom, old('id_uomedit'), array('class' => 'form-control', 'required' => 'true', 'id' => 'id_uomedit')) }}<br/>

					{{ Form::text('nameuomedit', old('nameuomedit'), array('class' => 'form-control', 'placeholder' => 'Name*', 'required' => 'true', 'id' => 'nameuomedit')) }}<br/>

					{{ Form::text('valueedit', old('valueedit'), array('class' => 'form-control', 'placeholder' => 'Value*', 'required' => 'true', 'id' => 'valueedit')) }}<br/>

					{{ Form::text('initialedit', old('initialedit'), array('class' => 'form-control', 'placeholder' => 'Initial*', 'required' => 'true', 'id' => 'initialedit')) }}<br/>

					{{ Form::text('descriptionuomedit', old('descriptionuomedit'), array('class' => 'form-control', 'placeholder' => 'Description*', 'required' => 'true', 'id' => 'descriptionuomedit')) }}<br/>

					<br/>
					<button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Back</button>
					<button type="submit" class="btn btn-success pull-right"><i class="fa fa-check"></i> Edit</button>

					<br/>
				</div>
				{!! Form::close() !!} 
			</div>

		</div>      
	</div>
</div>
</div>
@stop

@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script>
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>

<script>

	function editValue(str, id){
		var prd = id;

     // var id_uomedit = $(str).closest('tr').find('td:eq(1)').text();

     var nameuomedit = $(str).closest('tr').find('td:eq(1)').text(); 

     // console.log(nameuomedit);

     var valueedit = $(str).closest('tr').find('td:eq(2)').text(); 
     var initialedit = $(str).closest('tr').find('td:eq(3)').text(); 
     var descriptionuomedit = $(str).closest('tr').find('td:eq(4)').text();  

     // $("#id_uomedit").val(id_uomedit);
     $("#nameuomedit").val(nameuomedit);
     $("#valueedit").val(valueedit);
     $("#initialedit").val(initialedit);
     $("#descriptionuomedit").val(descriptionuomedit);
     $("#iddetail").val(prd);

 }

 function showeditmodal(){
 	$('#myModalEdit').modal();
 }
</script>

<script>
	$(".update").on("submit", function(){
		return confirm("Do you want to update this item?");
	});

	$(".create").on("submit", function(){
		return confirm("Do you want to create this item?");
	});
</script>

<script>
	$(".delete").on("submit", function(){
		return confirm("Do you want to delete this item?");
	});

</script>
@stop