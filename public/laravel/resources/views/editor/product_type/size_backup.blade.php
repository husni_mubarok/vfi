@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="{{ URL::route('editor.product_type.index') }}"><i class="fa fa-cube"></i> Product Type</a></li>
    <li class="active"><a href="{{ URL::route('editor.product_type.edit_size', $product_type->id) }}"><i class="fa fa-bar-chart"></i> Size</a></li>
  </ol>
</section>
@actionStart('product_type', 'update')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-5">
			        <div class="x_panel">
		                <h2>
		                	<i class="fa fa-cube"></i>
		                	<i class="fa fa-bar-chart"></i> {{$product_type->name}} Size
	                	</h2>
		                <hr>
			            <div class="x_content">
			                @include('errors.error')
		                    {!! Form::open(array('route' => ['editor.product_type.update_size', $product_type->id]))!!}
		                    {{ csrf_field() }}
		                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
	                    	<table id="productTable" class="table table-sm table-hover">
	                    	<thead>
		                    	<tr>
		                    		<th><i class="fa fa-check-square-o"></i></th>
		                    		<th>Min.</th>
		                    		<th>Max.</th>
		                    		<th>Note</th>
		                    	</tr>
	                    	</thead>
	                    	<tbody>
		                    	@foreach($sizes as $key => $size)
		                    	<tr>
		                    		<td>
			                    		@if(in_array($size->id, $product_type->product_type_size->pluck('size_id')->toArray()))
				                    	{{ Form::checkbox('size[]', $size->id, true) }}
				                    	@else
				                    	{{ Form::checkbox('size[]', $size->id) }}
				                    	@endif
			                    	</td>
			                    	<td>{{$size->min_value}}</td>
			                    	<td>{{$size->max_value}}</td>
			                    	<td>{{$size->note}}</td>
	                    		</tr>
		                    	@endforeach	
                    		</tbody>
							</table>	                    		
                            <button type="submit" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
	                    	</div>
	                        {!! Form::close() !!}
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@actionEnd
@stop