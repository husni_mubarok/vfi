@extends('layouts.home.template')
@section('content')
<div class="header_menu">
    <a href="{{ URL::route('editor.size.index') }}">SIZE</a>
    <a href="{{ URL::route('editor.product_type.index') }}">PRODUCT TYPE</a>
    <a href="{{ URL::route('editor.product.index') }}">PRODUCT</a>
    <a href="{{ URL::route('editor.supplier.index') }}">SUPPLIER</a>
</div>
@actionStart('product_type', 'read')
<section class="content">
	<div class="row">
	    <div class="col-md-12 col-sm-12 col-xs-12">
	    	<div class="col-md-1"></div>
	    	<div class="col-md-10">
		        <div class="x_panel">
	                <h2>
	                	<i class="fa fa-cubes"></i> Product Type List
	                	@actionStart('product_type', 'create')
	                	<a href="{{ URL::route('editor.product_type.create') }}" class="btn btn-primary btn-lg pull-right"><i class="fa fa-plus"></i> Add</a>
	                	@actionEnd
                	</h2>
	                <hr>
		            <div class="x_content">
		                <table id="productTable" class="table table-striped dataTable">
						  	<thead>
						  	  	<tr>
							      	<th width="5%">#</th>
							      	<th>Name</th>
							      	<th>Description</th>
							      	<th>Product</th>
							      	<th>Sizes</th>
							      	<th width="10%"></th>
						    	</tr>
						  	</thead>
						  	<tbody>
						    @foreach($product_types as $key => $product_type)
						    	<tr>
						      		<td>{{$key+1}}</td>
							      	<td>{{$product_type->name}}</td>
							      	<td>{{$product_type->description}}</td>
							      	<td>
							      		<table class="table">
							      		<thead>
							      		<tr>
							      			<th>
							      				@actionStart('product_type', 'update')
							      				<a href="{{ URL::route('editor.product_type.detail', [$product_type->id]) }}" class="btn btn-primary btn-sm">
							      				<i class="fa fa-pencil"></i> Edit
							      				</a>
							      				@actionEnd
						      				</th>
							      		</tr>
							      		</thead>
							      		<tbody>
							      		@foreach($product_type->product as $product)
							      		<tr>
							      			<td>{{$product->name}}</td>
							      		</tr>
							      		@endforeach
							      		</tbody>
							      		</table>
						      		</td>
							      	<td>
							      		@actionStart('product_type', 'update')
							      		<a href="{{ URL::route('editor.product_type.edit_size', [$product_type->id]) }}" class="btn btn-primary btn-sm"><i class="fa fa-bar-chart"></i>&nbsp;{{$product_type->product_type_size->count()}}</a>
							      		@actionEnd
						      		</td>
							      	<td align="center">
							      		@actionStart('product_type', 'update')
							      		<div class="col-md-2 nopadding">
							      			<a href="{{ URL::route('editor.product_type.edit', [$product_type->id]) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
							      		</div>
							      		@actionEnd

							      		@actionStart('product_type', 'delete')
						      			<div class="col-md-2 nopadding">
							      			{!! Form::open(array('route' => ['editor.product_type.delete', $product_type->id], 'method' => 'delete'))!!}
	                    					{{ csrf_field() }}	                    				
							      			<button type="submit" class="btn btn-primary btn-sm" onclick="return confirm('Confirm delete?')"><i class="fa fa-trash"></i></a></button>
							      			{!! Form::close() !!}
						      			</div>
						      			@actionEnd
						      		</td>
							    </tr>
						    @endforeach
							</tbody>
						</table>
		            </div>
		        </div>
	        </div>
	    </div>
	</div>
</section>
@actionEnd
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function () {
    $("#productTable").DataTable(
    	{
    		"scrollX": true
    	});
    });
</script>
@stop