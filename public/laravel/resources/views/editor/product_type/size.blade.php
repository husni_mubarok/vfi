@extends('layouts.home.template')
@section('content')
<div class="header_menu">
    <a href="{{ URL::route('editor.size.index') }}">SIZE</a>
    <a href="{{ URL::route('editor.product_type.index') }}">PRODUCT TYPE</a>
    <a href="{{ URL::route('editor.product.index') }}">PRODUCT</a>
    <a href="{{ URL::route('editor.supplier.index') }}">SUPPLIER</a>
</div>
@actionStart('product_type', 'update')
<section class="content">
	<div class="row">
	    <div class="col-md-12 col-sm-12 col-xs-12">
	    	<div class="col-md-1"></div>
	    	<div class="col-md-5">
		        <div class="x_panel">
	                <h2>
	                	<i class="fa fa-cube"></i>
	                	<i class="fa fa-bar-chart"></i> {{$product_type->name}} Size
                	</h2>
	                <hr>
		            <div class="x_content">
		                @include('errors.error')
	                    {!! Form::open(array('route' => ['editor.product_type.update_size', $product_type->id]))!!}
	                    {{ csrf_field() }}
	                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                    	<table id="productTable" class="table table-sm table-hover">
                    	<thead>
	                    	<tr>
	                    		<th><i class="fa fa-check-square-o"></i></th>
	                    		<th>Min.</th>
	                    		<th>Max.</th>
	                    		<th>Note</th>
	                    	</tr>
                    	</thead>
                    	<tbody>
	                    	@foreach($sizes as $key => $size)
	                    	<tr>
	                    		<td>
		                    		@if(in_array($size->id, $product_type->product_type_size->pluck('size_id')->toArray()))
			                    	{{ Form::checkbox('size[]', $size->id, true) }}
			                    	@else
			                    	{{ Form::checkbox('size[]', $size->id) }}
			                    	@endif
		                    	</td>
		                    	<td>{{$size->min_value}}</td>
		                    	<td>{{$size->max_value}}</td>
		                    	<td>{{$size->note}}</td>
                    		</tr>
	                    	@endforeach	
                		</tbody>
						</table>	                    		
                        <button type="submit" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
                    	</div>
                        {!! Form::close() !!}
		            </div>
		        </div>
	        </div>
	    </div>
	</div>
</section>
@actionEnd
@stop