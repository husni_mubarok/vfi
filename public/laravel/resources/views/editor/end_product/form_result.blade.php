@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li><a href="{{ URL::route('editor.end_product.index') }}"><i class="fa fa-cube"></i> Sashimi</a></li>
    <li class="active"><a href="#"><i class="fa fa-dropbox"></i> Result</a></li>
  </ol>
</section>

<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<div class="x_panel">
						<h2>
							@if(isset($end_product_result))
		                	<i class="fa fa-pencil"></i> 
		                	@else
		                	<i class="fa fa-plus"></i> 
		                	@endif
		                	<i class="fa fa-dropbox"></i>
		                	@if($end_product->production_type == 'sashimi')
		                	Sashimi 
		                	@elseif($end_product->production_type == 'value_added') 
		                	Value Added
		                	@endif
		                	<small>Finish Product</small>
	                	</h2>
						<hr>
			    	  	<div class="x_content">
			    	  	@actionStart('end_product', 'create|update')
			    	  	@include('errors.error')
			    	  	@if(isset($end_product_result))
		                {!! Form::model($end_product, ['route' => ['editor.end_product.update_result', $end_product->id], 'method' => 'PUT']) !!}
			    	  	@else
			    	  	{!! Form::open(['route' => ['editor.end_product.store_result', $end_product->id]]) !!}
		                @endif
	                    {{ csrf_field() }}
	                    <div class="col-md-6 col-sm-6 col-xs-6 form-group">
	                    	<div class="col-md-8">
	                        {{ Form::label('finished_at', 'Finish Date') }}
	                        {{ Form::text('finished_at', old('finished_at'), array('class' => 'form-control', 'placeholder' => 'Date & Time*', 'required' => 'true', 'id' => 'finished_at')) }}
	                        </div>

	                        {{-- <div class="col-md-4">
		                        {{ Form::label('uom', 'Unit of Measurement') }}
		                        {{ Form::select('uom_id', $uom_list, old('uom_id'), array('class' => 'form-control')) }}
	                        </div> --}}
	                        <input type="hidden" name="uom_id" value="2">
	                        <br/>

	                        <table class="table table-bordeded">
	                        <thead>
	                        	<tr>
	                        		<th>Type</th>
	                        		<th colspan="3">Size</th>
	                        		<th>Tray</th>
	                        		<th></th>
	                        	</tr>
	                        </thead>
	                        <tbody id="cart">
	                        	<tr>
	                        	</tr>
	                        </tbody>
	                        <tfoot>
	                        	<tr>
	                        		<td>
	                        			<select name="type" class="form-control" id="input_type">
	                        				<option value="single">Single</option>
	                        				<option value="range">Range</option>
	                        			</select>
	                        		</td>
	                    			<td class="input_single" colspan="3">
	                    				<input type="number" class="form-control" id="size_single" min="0">
	                    			</td>
	                				<td class="input_range"  style="display:none"><input type="number" class="form-control" id="size_range_1" min="0"></td>
	                    			<td class="input_range"  style="display:none">~</td>
	                    			<td class="input_range"  style="display:none"><input type="number" class="form-control" id="size_range_2" min="0"></td>
	                        		<td><button type="button" class="btn btn-primary btn-md" id="btn_add" disabled><i class="fa fa-plus"></i></button></td>
	                        	</tr>
	                        </tfoot>
	                        </table>
	                        <button type="submit" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
	                    	{!! Form::close() !!}
			        	</div>
			        	@actionEnd

			        	@actionStart('end_product', 'read')
			        	<div class="col-md-6 col-sm-6 col-xs-6">
			        		<h4>Production Detail</h4>
					        <table class="table table-bordered">
					          <tr>
					            <th>Product</th>
					            <td>{{$end_product->production->name}}</td>
					          </tr>
					          <tr>
					            <th>Weight</th>
					            <td>{{number_format($end_product->weight, 2)}} kg</td>
					          </tr>
					          <tr>
					            <th>Qty / Tray</th>
					            <td>{{number_format($end_product->block_weight, 0)}}</td>
					          </tr>
					          <tr>
					            <th>Production Date</th>
					            <td>
					              {{date("D, d M Y", strtotime($end_product->started_at))}}
					              &nbsp;<i class="fa fa-long-arrow-right"></i>&nbsp;
					              @if($end_product->finished_at == null)
					              <i class="fa fa-question-circle-o"></i>
					              @else
					              {{date("D, d M Y", strtotime($end_product->finished_at))}} 
					              @endif
					            </td>
					          </tr>
					          <tr>
					            <th>Criteria</th>
					            <td>{{$end_product->criteria}}</td>
					          </tr>
					        </table>
			        	</div>
			        	@actionEnd
		    	  	</div>
	    	  	</div>
		  	</div>
	  	</div>
	</section>
</section>
@stop

@section('scripts')
<script>
var cart = JSON.parse("{}");
$(document).ready(function () {
	$('#finished_at').datetimepicker({
		format: 'YYYY-MM-DD',
	});

	var size = [];
});

$("#input_type").on('change', function() {
	if($("#input_type").val() == 'single') {
		$(".input_single").show();
		$(".input_range").hide();
		$("#size_range_1").val('');
		$("#size_range_2").val('');
	} else if($("#input_type").val() == 'range') {
		$(".input_range").show();
		$(".input_single").hide();
		$("#size_single").val('');
	}
});

$("#size_single").on('change blur', function() {
	if($("#size_single").val() == null)
	{
		$("#btn_add").attr('disabled', true);
	} else {
		$("#btn_add").attr('disabled', false);
	}
});

$("#size_range_1").on('change blur', function() {
	if($("#size_range_1").val() && $("#size_range_2").val())
	{
		$("#btn_add").attr('disabled', false);
	} else {
		$("#btn_add").attr('disabled', true);
	}
});

$("#size_range_2").on('change blur', function() {
	if($("#size_range_1").val() && $("#size_range_2").val())
	{
		$("#btn_add").attr('disabled', false);
	} else {
		$("#btn_add").attr('disabled', true);
	}
});
  
$("#btn_add").click(function() {
	$(".btn_reduce").on('click', function(){

		if(cart[size] > 1)
	    { 
	        cart[size] = cart[size]-1;
			$("#block_"+size).html(cart[size]);
		} else {
			cart[size]= null;
			$(this).parent().parent().remove();
		}
	});

    $(".btn_remove").click(function(){	   
		cart[size]= null;
    	$(this).parent().parent().remove();
    });
	if($("#input_type").val() == 'single') {
	    var outputString = '';
	    var size = $("#size_single").val();
	    if(cart[size]==undefined){
	    	cart[size]=1;
	    	outputString += '<tr>';
		    outputString += '<td></td>';
		    outputString += '<td colspan="3">';
		    outputString += size;
		    outputString += '<input type="hidden" id="size['+size+']" name="size['+size+']" value="'+size+'">';
		    outputString += '</td>';
		    outputString += '<td><div id="block_'+size+'">'+cart[size]+'</div><input type="hidden" id="block_value_'+size+'" name="block['+size+']" value="'+cart[size]+'" class="form-control"></td>';
		    outputString += '<td>';
		    outputString += '<button type="button" class="btn btn-primary btn-md" id="btn_add_'+size+'"><i class="fa fa-plus"></i></button>';
		    outputString += '<button type="button" class="btn btn-warning btn-md" id="btn_reduce_'+size+'"><i class="fa fa-minus"></i></button>';
		    outputString += '<button type="button" class="btn btn-danger btn-md" id="btn_remove_'+size+'"><i class="fa fa-remove"></i></button>';
		    outputString += '</td>';
		    outputString += '</tr>';

		    $("#cart").append(outputString);
		    $("#btn_reduce_"+size).on('click', function(){
		    	if(cart[size] > 1)
		        { 
			        cart[size] = cart[size]- 1;
					$("#block_"+size).html(cart[size]);
					$("#block_value_"+size).val(cart[size]);
					console.log(cart[size]);
				} else {
					cart[size]= null;
	        		$(this).parent().parent().remove();
				}
		    });

		    $("#btn_add_"+size).on('click', function(){
		        cart[size] =  cart[size]+1;
		    	$("#block_value_"+size).val(cart[size]);
				$("#block_"+size).html(cart[size]);
		    });

		    $("#btn_remove_"+size).on('click',function(){	   
				cart[size]= null;
	        	$(this).parent().parent().remove();
		    });

	    }else{
			cart[size]=cart[size]+1;	
			$("#block_value_"+size).val(cart[size]);
			$("#block_"+size).html(cart[size]);
	    }

	    $("#btn_submit").attr("disabled", false);
	    $("#size_single").val('');
	    $("#block").val('');
	   
	  
	} else if ($("#input_type").val() == 'range') {
		var outputString = '';
		var size = $("#size_range_1").val()+'_'+$("#size_range_2").val();
	    if(cart[size]==undefined){
	    	cart[size]=1;
	    	outputString += '<tr>';
		    outputString += '<td></td>';
		    outputString += '<td colspan="3">';
		    outputString += $("#size_range_1").val()+"~"+$("#size_range_2").val();
		    outputString += '<input type="hidden" id="size['+size+']" name="size['+size+']" value="'+size+'">';
		    outputString += '</td>';
		    outputString += '<td><div id="block_'+size+'">'+cart[size]+'</div><input type="hidden" id="block_value_'+size+'" name="block['+size+']" value="'+cart[size]+'" class="form-control"></td>';
		    outputString += '<td>';
		    outputString += '<button type="button" class="btn btn-primary btn-md" id="btn_add_'+size+'"><i class="fa fa-plus"></i></button>';
		    outputString += '<button type="button" class="btn btn-warning btn-md " id="btn_reduce_'+size+'"><i class="fa fa-minus"></i></button>';
		    outputString += '<button type="button" class="btn btn-danger btn-md" id="btn_remove_'+size+'"><i class="fa fa-remove"></i></button>';
		    outputString += '</td>';
		    outputString += '</tr>';

		    $("#cart").append(outputString);
		    $("#btn_reduce_"+size).on('click', function(){
		    	if(cart[size] > 1)
		        { 
			        cart[size] =  cart[size]-1;
					$("#block_"+size).html(cart[size]);
					$("#block_value_"+size).val(cart[size]);
					console.log(cart[size]);
				} else {
					cart[size]= null;
	        		$(this).parent().parent().remove();
				}
		    });
		    $("#btn_add_"+size).on('click', function(){
		        cart[size] =  cart[size]+1;
		        $("#block_value_"+size).val(cart[size]);
				$("#block_"+size).html(cart[size]);
		    });

		    $("#btn_remove_"+size).on('click',function(){	   
				cart[size]= null;
	        	$(this).parent().parent().remove();
		    });
	    }else{
			cart[size]=cart[size]+1;
			$("#block_value_"+size).val(cart[size]);
			$("#block_"+size).html(cart[size]);
	    }

	    
	    $("#btn_submit").attr("disabled", false);
	    $("#size_range_1").val('');
	    $("#size_range_2").val('');
	    $("#block").val('');

	    
	}

	$("#btn_add").attr('disabled', true);
});
</script>

@if(isset($end_product_result))
<script>
jQuery.each({!! $end_product_result !!}, function( i, val ) {
	var size = val['size'];
	var block = val['block'];
	cart[size] = block;
    var outputString = '';

   	outputString += '<tr>';
    outputString += '<td></td>';
    outputString += '<td colspan="3">';
    outputString += size;
    outputString += '<input type="hidden" id="size['+size+']" name="size['+size+']" value="'+size+'">';
    outputString += '</td>';
    outputString += '<td><div id="block_'+size+'">'+cart[size]+'</div><input type="hidden" id="block_value_'+size+'" name="block['+size+']" value="'+cart[size]+'" class="form-control"></td>';
    outputString += '<td>';
    outputString += '<button type="button" class="btn btn-primary btn-md" id="btn_add_'+size+'"><i class="fa fa-plus"></i></button>';
    outputString += '<button type="button" class="btn btn-warning btn-md" id="btn_reduce_'+size+'"><i class="fa fa-minus"></i></button>';
    outputString += '<button type="button" class="btn btn-danger btn-md" id="btn_remove_'+size+'"><i class="fa fa-remove"></i></button>';
    outputString += '</td>';
    outputString += '</tr>';

    $("#cart").append(outputString);
    $("#btn_reduce_"+size).on('click', function(){
    	if(cart[size] > 1)
        { 
	        cart[size] = cart[size]- 1;
			$("#block_"+size).html(cart[size]);
		} else {
			cart[size]= null;
    		$(this).parent().parent().remove();
		}
    });

    $("#btn_add_"+size).on('click', function(){
        cart[size] =  cart[size]+1;
    	$("#block_value_"+size).val(cart[size]);
		$("#block_"+size).html(cart[size]);
    });

    $("#btn_remove_"+size).on('click',function(){	   
		cart[size]= null;
    	$(this).parent().parent().remove();
    });
});
</script>
@endif
@stop