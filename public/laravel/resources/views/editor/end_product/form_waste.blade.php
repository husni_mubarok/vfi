@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li><a href="{{ URL::route('editor.end_product.index') }}"><i class="fa fa-cube"></i> End Product</a></li>
    <li class="active"><a href="#"><i class="fa fa-dollar"></i> Waste</a></li>
  </ol>
</section>

<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<div class="x_panel">
						<h2>
		                	@if(isset($end_product_waste))
		                	<i class="fa fa-pencil"></i>
		                	@else
		                	<i class="fa fa-plus"></i>
		                	@endif
		                	@if($end_product->production_type == 'sashimi')
		                	Sashimi 
		                	@elseif($end_product->production_type == 'value_added') 
		                	Value Added
		                	@endif
		                	<small>Waste Product</small>
	                	</h2>
		                <hr>
			    	  	<div class="x_content">
			    	  	@actionStart('end_product', 'create|update')
						{{-- Waste Product Form --}}
						  	<div class="col-md-6">
				        		<h4>Waste Products</h4>
				          		@include('errors.error')
				          		@if(isset($end_product_waste))
				          		{!! Form::model($end_product_waste, ['route' => ['editor.end_product.update_waste', $end_product->id], 'method' => 'PUT']) !!}
				          		@else
				          		{!! Form::open(array('route' => ['editor.end_product.store_waste', $end_product->id])) !!}		     
				          		@endif     		
				          		<table class="table table-bordered table-hover">
				          		<thead>
						            <tr>
							            <th>Name</th>
							            <th>UoM</th>
							            <th>Type</th>
							            <th>Value</th>
				          			</tr>
					          	</thead>
					          	<tbody>
			          			@if(isset($end_product_waste))
				           		 	@foreach($end_product_waste as $key => $production_result_variable)
				            		@if($production_result_variable->variable->type == 'Reduction')
					            	<tr>
						              	<td>{{$production_result_variable->variable->name}}</td>
						              	<td>{{$production_result_variable->variable->uom}}</td>
						              	<td>{{$production_result_variable->variable->type}}</td>
						              	<td>
						                	<div class="input-group">
						                  		{{ Form::number('value['.$production_result_variable->variable->id.']', old('value['.$production_result_variable->variable->id.']', $production_result_variable->value), array('required' => 'true', 'min' => '0', 'step' => '0.01', 'class' => 'form-control')) }}
						                  		<div class="input-group-addon"><span>kg</span></div>
						                	</div>
						              	</td>
				            		</tr>
					            	@endif
					            	@endforeach
				          		@else
				            		@foreach($end_product->production->production_variable as $key => $production_variable)
				            		@if($production_variable->variable->type == 'Reduction')
					              	<tr>
					                	<td>{{$production_variable->variable->name}}</td>
					                	<td>{{$production_variable->variable->uom}}</td>
					                	<td>{{$production_variable->variable->type}}</td>
					                	<td>
						                  	<div class="input-group">
					                    		{{ Form::number('value['.$production_variable->variable->id.']', old('value['.$production_variable->variable->id.']'), array('required' => 'true', 'min' => '0', 'step' => '0.01', 'class' => 'form-control')) }}
					                    		<div class="input-group-addon"><span>kg</span></div>
					                	  	</div>
					                	</td>
					              	</tr>
					            	@endif
					            	@endforeach
				          		@endif
				          		</tbody>
				        		</table>
				        		<button type="button" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
				        		{!! Form::close() !!}
					      	</div>
					      	{{-- Waste Product Form --}}
					      	@actionEnd

					      	@actionStart('end_product', 'read')
				      		{{-- Production Details --}}
				      		<div class="col-md-6">
				        		<h4>Material</h4>
				        		<table class="table table-bordered">
				          			<tr>
					            		<th>Product</th>
				            			<td>{{$end_product->production->name}}</td>
				          			</tr>
				          			<tr>
				            			<th>Weight</th>
				            			<td>{{number_format($end_product->weight, 2)}} kg</td>
				          			</tr>
				          			<tr>
				            			<th>Qty / Tray</th>
				            			<td>{{number_format($end_product->block_weight, 0)}}</td>
				          			</tr>
				          			<tr>
				            			<th>Production Date</th>
				            			<td>
				              				{{date("D, d M Y", strtotime($end_product->started_at))}}
				              				&nbsp;<i class="fa fa-long-arrow-right"></i>&nbsp;
				              				@if($end_product->finished_at == null)
				              				<i class="fa fa-question-circle-o"></i>
				              				@else
				              				{{date("D, d M Y", strtotime($end_product->finished_at))}} 
				              				@endif
				            			</td>
				          			</tr>
				          			<tr>
				            			<th>Criteria</th>
				            			<td>{{$end_product->criteria}}</td>
				          			</tr>
		        				</table>
				      		</div>
				      		{{-- Production Details --}}
				      		@actionEnd
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>
@stop

@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
var cart = JSON.parse("{}");

$("#size, #tray").on('change', function() {
	if($("#size").val() && $("#tray").val())
	{
		$("#btn_add").attr('disabled', false);
	} else {
		$("#btn_add").attr('disabled', true);
	}
});

$("#btn_add").on('click', function() {
	var size = parseInt($("#size").val());
	var tray = parseInt($("#tray").val());

	if(cart[size] == undefined)
	{
		cart[size] = tray;
		var new_row = '';
		new_row += '<tr>';
		new_row += '<td>';
		new_row += size;
		new_row += '</td>';
		new_row += '<td>';
		new_row += '<div id="text_'+size+'">'+cart[size]+'</div>';
		new_row += '<input type="hidden" name="tray['+size+']" id="tray_'+size+'" value="'+cart[size]+'">';
		new_row += '</td>';
		new_row += '<td>';
		new_row += '<button type="button" class="btn btn-danger" id="btn_del_'+size+'"><i class="fa fa-remove"></i></button>'
		new_row += '</td>';
		new_row += '</tr>';

		$("#cart").append(new_row);

		$("#btn_del_"+size).on('click', function() {
			cart[size] = null;
			$(this).parent().parent().remove();
		})
	} else {
		cart[size] += tray;
		$("#text_"+size).text(cart[size]);
		$("#tray_"+size).val(cart[size]);
	}

	$("#size").val('');
	$("#tray").val('');
});
</script>
@stop
