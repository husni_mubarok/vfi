@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li class="active"><a href="{{ URL::route('editor.end_product.index') }}"><i class="fa fa-cube"></i> End Product</a></li>
  </ol>
</section>

<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-10">
			        <div class="x_panel">
		                <h2>
		                	@if(isset($end_product))
		                	<i class="fa fa-pencil"></i>
		                	@else
		                	<i class="fa fa-plus"></i>
		                	@endif
		                	<i class="fa fa-cube"></i> End Product
	                	</h2>
		                <hr>
			            <div class="x_content">
                      <div class="col-md-6">
			                @include('errors.error')

	                    @if(isset($end_product))
			                   {!! Form::model($end_product, array('route' => ['editor.end_product.update', $end_product->id], 'method' => 'PUT'))!!}
		                  @else
		                    {!! Form::open(array('route' => 'editor.end_product.store'))!!}
		                  @endif

                      {{ csrf_field() }}

                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            {{ Form::label('cart_id', 'Material') }}
                            {{ Form::select('cart_id', $cart_list, old('cart_id'), ['class' => 'form-control', 'id' => 'cart', 'placeholder' => 'Select Material']) }}
                            <br>

                            {{ Form::label('production_id', 'Product') }}
                            {{ Form::select('production_id', $products, old('production_id'), ['class' => 'form-control', 'id' => 'cart', 'placeholder' => 'Select Product']) }}
                            <br>

                            {{ Form::label('started_at', 'Date') }}
						                {{ Form::text('started_at', old('started_at'), array('class' => 'form-control', 'placeholder' => 'Date & Time', 'required' => 'true', 'id' => 'date')) }}
                            <br>

                            {{ Form::hidden('amount', null, ['id' => 'material_amount']) }}
                            {{ Form::hidden('weight', null, ['id' => 'material_weight']) }}

                            {{ Form::label('block_weight', 'Quantity per Tray') }}
					                  {{ Form::number('block_weight', old('block_weight'), array('class' => 'form-control', 'placeholder' => 'Quantity per Tray', 'required' => 'true')) }}
                            <br>

                            {{ Form::label('miss_product', 'Defrost Weight') }}
                            {{ Form::number('miss_product', old('miss_product'), array('class' => 'form-control', 'placeholder' => 'Defrost Weight', 'required' => 'true')) }}
                            <br>

                            <button type="submit" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
	                    	</div>
	                        {!! Form::close() !!}
                        </div>

                        <div class="col-md-6">
                          <h4>Selected Material</h4>
                          <table class="table table-bordered" id="material_table">
                          <thead>
                          <tr>
                            <th>Doc Code</th>
                            <td id="doc_code"> - </td>
                            <th>Date</th>
                            <td id="mtrl_date"> - </td>
                          </tr>
                          <tr>
                            <th>Name</th>
                            <th>Size</th>
                            <th>Block Weight</th>
                            <th>Block</th>
                          </tr>
                          </thead>
                          <tbody id="body">
                          </tbody>
                          </table>
                        </div>
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@stop

@section('scripts')
<script>
$(document).ready(function(){
  $('#date').datetimepicker({
		format: 'YYYY-MM-DD',
	});
});

$("#cart").on('change', function() {
  $("#doc_code").empty();
  $("#mtrl_date").empty();
  $("#body").empty();

  $("#material_weight").val('');
  $("#material_amount").val('');

  $.ajax({
      url : '{{ URL::route('get.cart_detail') }}',
      data : 'cart_id='+$("#cart option:selected").val(),
      type : 'GET',
      headers : {
        'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
      },
      error : function(jqXHR, textStatus, errorThrown){
        $("#doc_code").append(' - ');
        $("#mtrl_date").append(' - ');
      },
      success : function(data, textStatus, jqXHR){
        $("#doc_code").append(data.doc_code);
        $("#mtrl_date").append(data.date);
        var detail_row = '';
        var total_weight = 0;
        var total_amount = 0;

        jQuery.each(data.detail, function(i, val)
        {
          detail_row += '<tr>';
          detail_row += '<td>'+val.name+'</td>';
          detail_row += '<td>'+val.size+'</td>';
          detail_row += '<td>'+val.block_weight+'</td>';
          detail_row += '<td>'+val.quantity+'</td>';
          detail_row += '</tr>';

          total_weight += (val.block_weight * val.quantity);
          total_amount += val.quantity;
        });

        $("#body").append(detail_row);
        $("#material_weight").val(total_weight);
        $("#material_amount").val(total_amount);
      },
  });
});

</script>
@stop
