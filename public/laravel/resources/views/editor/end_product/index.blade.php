@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li class="active"><a href="#"><i class="fa fa-cube"></i> End Product</a></li>
  </ol>
</section>
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-10">
			        <div class="x_panel">
		                <h2>
		                	<i class="fa fa-cube"></i> End Product List
		                	<a href="{{ URL::route('editor.end_product.create') }}" class="btn btn-primary btn-lg pull-right"><i class="fa fa-plus"></i> Add</a>
	                	</h2>
		                <hr>
			            <div class="x_content">
			                <table id="sashimiTable" class="table table-striped dataTable">
							  	<thead>
				                	<tr>
				                		<th>#</th>
				                		<th>Status</th>
				                		<th>Production</th>
				                		<th>Weight</th>
				                		<th>Waste <i class="fa fa-minus"></i></th>
				                		<th>Result <i class="fa fa-dropbox"></i></th>
				                		<th>Additional <i class="fa fa-plus-square-o"></i></th>
				                		<th>Action</th>
				                	</tr>
				                </thead>
				                <tbody>
				                	@foreach($end_products as $key => $end_product)
				                	@if($end_product->status == 'Pending')
				                	<tr class="danger">
				                	@elseif($end_product->status == 'On Going')
				                	<tr class="warning">
				                	@elseif($end_product->status == 'Done')
				                	<tr class="info">
				                	@endif
				                		<td>{{$key+1}}</td>
				                		<td>{{$end_product->status}}</td>
				                		<td>{{$end_product->production->name}}</td>
				                		<td>{{number_format($end_product->weight, 2)}}</td>
				                		<td>
				                			@if($end_product->production_result_variable->count() > 0)
				                			<ul>
				                			@foreach($end_product->production_result_variable as $variable)
				                			@if($variable->variable->type == 'Reduction')
				                			<li>{{$variable->variable->name}} : {{number_format($variable->value, 2)}} kg</li>
				                			@endif
				                			@endforeach
				                			</ul>
				                			@else
				                			<a href="{{ URL::route('editor.end_product.create_waste', [$end_product->id]) }}" class="btn btn-sm btn-primary">
				                			<i class="fa fa-minus"></i> <i class="fa fa-pencil"></i></a><br>
				                			@endif
			                			</td>
				                		<td>
				                			@if($end_product->production_result_detail->count() > 0)
				                			<ul>
				                			@foreach($end_product->production_result_detail as $detail)
				                			<li>{{$detail->size}} <i class="fa fa-arrow-right"></i> {{$detail->block}}</li>
				                			@endforeach
				                			</ul>
				                			@else
				                			<a href="{{ URL::route('editor.end_product.create_result', [$end_product->id]) }}" class="btn btn-sm btn-primary">
				                			<i class="fa fa-dropbox"></i> <i class="fa fa-pencil"></i></a>
				                			@endif
				                		</td>
				                		<td>
				                			@if($end_product->production_result_addition->count() > 0)
											<ul>
											@foreach($end_product->production_result_addition as $addition)
											<li>{{$addition->size}} <i class="fa fa-arrow-right"></i> {{$addition->block}}</li>
											@endforeach
											</ul>
											@else
											<a href="{{ URL::route('editor.end_product.create_addition', $end_product->id) }}" class="btn-sm btn-primary"><i class="fa fa-plus-square-o"></i> <i class="fa fa-pencil"></i></a>
											@endif
			                			</td>
				                		<td>
				                			@if($end_product->production_result_variable->count() > 0)
				                			<a href="{{ URL::route('editor.end_product.edit_waste', [$end_product->id]) }}" class="btn btn-sm btn-primary">
				                			<i class="fa fa-minus"></i> <i class="fa fa-pencil"></i></a><br>
				                			@endif

				                			@if($end_product->production_result_addition->count() > 0)
				                			<a href="{{ URL::route('editor.end_product.edit_addition', [$end_product->id]) }}" class="btn btn-sm btn-primary">
				                			<i class="fa fa-plus-square-o"></i> <i class="fa fa-pencil"></i></a><br>
				                			@endif

				                			@if($end_product->production_result_detail->count() > 0)
				                			<a href="{{ URL::route('editor.end_product.edit_result', [$end_product->id]) }}" class="btn btn-sm btn-primary">
				                			<i class="fa fa-dropbox"></i> <i class="fa fa-pencil"></i></a><br>
				                			@endif

				                			<a href="{{ URL::route('editor.end_product.summary', [$end_product->id]) }}" class="btn btn-md btn-primary">
				                			<i class="fa fa-file-text-o"></i></a>

				                			{!! Form::open(['route' => ['editor.end_product.delete', $end_product->id], 'method' => 'DELETE']) !!}
				                			<button type="submit" class="btn btn-danger" onclick="return confirm('Yakin hapus data?')"><i class="fa fa-trash"></i></button>
				                			{!! Form::close() !!}
				                		</td>
			                		</tr>
				                	@endforeach
				                </tbody>
							</table>
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function () {
    $("#sashimiTable").DataTable(
    	{
    	
    	});
    });
</script>
@stop