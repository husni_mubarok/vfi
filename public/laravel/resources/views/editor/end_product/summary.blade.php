@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li><a href="{{ URL::route('editor.end_product.index') }}"><i class="fa fa-cube"></i> End Product</a></li>
    <li class="active"><a href="#"><i class="fa fa-file-text-o"></i> Summary</a>
  </ol>
</section>

<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<div class="x_panel">
						<h2>
			          	<i class="fa fa-file-text-o"></i> Summary
			        	</h2>
	    	  	<hr>
	          @actionStart('end_product', 'read')
	    	  	<div class="col-md-2"></div>          
					  <div class="col-md-8">
	  				  @php
	            $total_material = 0;
	            $total_waste = 0;
	            $total_addition = 0;
	            $total_ready = 0;
	            $total_miss = 0;
	            foreach($end_product->cart->detail as $detail)
	            {
	              $total_material += ($detail->block * $detail->block_weight);
	            }
	            foreach($end_product->production_result_variable as $waste)
	            {
	              if($waste->variable->type == 'Reduction')
	              {
	                $total_waste += $waste->value;
	              }
	            }
	            foreach($end_product->production_result_addition as $addition)
	            {
	              $total_addition += ($addition->block * $end_product->block_weight);
	            }
	            foreach($end_product->production_result_detail as $detail)
	            {
	              $total_ready += (($detail->block * $end_product->block_weight) / $detail->size);
	            }
	            $total_miss = $end_product->weight - ($total_waste + $total_addition + $total_ready);
	            @endphp
	            <table class="table">
	              <tr>
	                <td colspan="3">
	                  <b>{{$end_product->production->fish->name}}</b> 
	                  <small>{{$end_product->status}}</small>
	                </td>
	                <td align="right">
	                  <b>{{number_format($end_product->weight, 2)}} kg</b>
	                </td>
	                <td width="5%">
	                  @if($end_product->status != 'Done')
	                  <a href="{{ URL::route('end_product.edit', [$end_product->id]) }}" class="btn btn-sm btn-success"><i class="fa fa-pencil"></i></a>
	                  @endif
	                </td>
	              </tr>
	              <tr>
	                <td colspan="5"><span>{{$end_product->production->name}}</span></td>
	              </tr>
	              <tr>
	                <td colspan="5">Qty / Tray = {{number_format($end_product->block_weight, 0)}}</td>
	              </tr>
	              <tr>
	                <td colspan="5">
	                  {{date("D, d M Y", strtotime($end_product->started_at))}} 
	                  <i class="fa fa-caret-right"></i> 
	                  {{date("D, d M Y", strtotime($end_product->finished_at))}}
	                </td>
	              </tr>
	              <tr>
	                <td colspan="5">Defrost Weight = {{number_format($end_product->miss_product, 2)}} kg</td>
	              </tr>

	              {{-- Materials --}}
	              <tr>
	                <td colspan="2"><b>Material</b></td>
	                <td></td>
	                <td align="right"><b>{{number_format($total_material, 2)}} kg</b></td>
	                <td>
	                  <button type="button" class="btn btn-default btn-sm" data-toggle="collapse" data-target=".material_rows">
	                  <i class="fa fa-chevron-down"></i></button>
	                </td>
	              </tr>
	              <tr class="collapse material_rows">
	                <th width="10%">Tgl. Masuk</th>
	                <th width="15%">Nama</th>
	                <th width="40%">Size</th>
	                <th align="right">Blok</th>
	                <th></th>
	              </tr>
	              @foreach($end_product->cart->detail as $detail)
	              <tr class="collapse material_rows">
	                <td>{{date("d/m/Y", strtotime($detail->created_at))}}</td>
	                <td>{{$detail->name}}</td>
	                <td>{{$detail->size}}</td>
	                <td align="right">{{$detail->block}}</td>
	                <td></td>
	              </tr>
	              @endforeach
	              {{-- Materials --}}

	              {{-- Waste Products --}}
	              <tr>
	                <td colspan="2"><b>Waste Product</b></td>
	                <td></td>
	                <td align="right"><b>{{number_format($total_waste, 2)}} kg</b></td>
	                <td>
	                  <button type="button" class="btn btn-default btn-sm" data-toggle="collapse" data-target=".waste_rows">
	                  <i class="fa fa-chevron-down"></i></button>
	                </td>
	              </tr>
	              <tr class="collapse waste_rows">
	                <th></th>
	                <th>Part</th>
	                <th></th>
	                <th align="right">Weight</th>
	                <th></th>
	              </tr>
	              @foreach($end_product->production_result_variable as $waste)
	              @if($waste->variable->type == 'Reduction')
	              <tr class="collapse waste_rows">
	                <td></td>
	                <td>{{$waste->variable->name}}</td>
	                <td></td>
	                <td align="right">{{number_format($waste->value, 2)}} kg</td>
	                <td></td>
	              </tr>
	              @endif
	              @endforeach
	              {{-- Waste Products --}}

	              {{-- Additional Products --}}
	              <tr>
	                <td colspan="2"><b>Additional Product</b></td>
	                <td></td>
	                <td align="right"><b>{{number_format($total_addition, 2)}} kg</b></td>
	                <td>
	                  <button type="button" class="btn btn-default btn-sm" data-toggle="collapse" data-target=".additional_rows">
	                  <i class="fa fa-chevron-down"></i></button>
	                </td>
	              </tr>
	              <tr class="collapse additional_rows">
	                <th></th>
	                <th>Size</th>
	                <th>Type, Reason</th>
	                <th align="right">Tray</th>
	                <th></th>
	              </tr>
	              @foreach($end_product->production_result_addition as $addition)
	              <tr class="collapse additional_rows">
	                <td></td>
	                <td>{{$addition->size}}</td>
	                <td>{{$addition->reject_type->name}}, {{$addition->reject_reason->name}}</td>
	                <td align="right">{{number_format($addition->block)}}</td>
	                <td></td>
	              </tr>
	              @endforeach
	              {{-- Additional Products --}}

	              {{-- Ready Products --}}
	              <tr>
	                <td colspan="2"><b>Ready Product</b></td>
	                <td></td>
	                <td align="right"><b>{{number_format($total_ready, 2)}} kg</b></td>
	                <td>
	                  <button type="button" class="btn btn-default btn-sm" data-toggle="collapse" data-target=".ready_rows">
	                  <i class="fa fa-chevron-down"></i></button>
	                </td>
	              </tr>
	              <tr class="collapse ready_rows">
	                <th></th>
	                <th>Size</th>
	                <th></th>
	                <th align="right">Tray</th>
	                <th></th>
	              </tr>
	              @foreach($end_product->production_result_detail as $detail)
	              <tr class="collapse ready_rows">
	                <td></td>
	                <td>{{$detail->size}}</td>
	                <td></td>
	                <td align="right">{{$detail->block}}</td>
	                <td></td>
	              </tr>
	              @endforeach
	              {{-- Ready Products --}}

	              {{-- Miss Products --}}
	              {{-- <tr>
	                <td colspan="2"><b>Miss Product</b></td>
	                <td></td>
	                <td align="right"><b>{{number_format($total_miss, 2)}} kg</b></td>
	                <td></td>
	              </tr> --}}
	              {{-- Miss Products --}}

	              {{-- Yield --}}
	              <tr>
	                <td colspan="2"><b>Yield</b></td>
	                <td></td>
	                <td></td>
	                <td>
	                  <button type="button" class="btn btn-default btn-sm" data-toggle="collapse" data-target=".yield_rows">
	                  <i class="fa fa-chevron-down"></i></button>
	                </td>
	              </tr>
	              <tr class="collapse yield_rows">
	                <td></td>
	                <td>Waste Product</td>
	                <td></td>
	                <td align="right">{{number_format($total_waste / $end_product->weight * 100, 2)}} %</td>
	                <td></td>
	              </tr>
	              <tr class="collapse yield_rows">
	                <td></td>
	                <td>Addition Product</td>
	                <td></td>
	                <td align="right">{{number_format($total_addition / $end_product->weight * 100, 2)}} %</td>
	                <td></td>
	              </tr>
	              <tr class="collapse yield_rows">
	                <td></td>
	                <td>Ready Product</td>
	                <td></td>
	                <td align="right">{{number_format($total_ready / $end_product->weight * 100, 2)}} %</td>
	                <td></td>
	              </tr>
	              <tr class="collapse yield_rows">
	                <td></td>
	                <td>Miss Product</td>
	                <td></td>
	                <td align="right">{{number_format($total_miss / $end_product->weight * 100, 2)}} %</td>
	                <td></td>
	              </tr>
	              {{-- Yield --}}   

	            </table>
	            <br>
	            <div class="pull-right" >
	              @if($end_product->status == 'Done')
	              <a href="{{ URL::route('end_product.pdf', $end_product->id) }}" class="btn btn-default btn-lg"><i class="fa fa-file-pdf-o"></i>&nbsp;PDF</a>
	              @else
	              {{ Form::open(['route' => ['end_product.close', $end_product->id], 'method' => 'PUT']) }}
	              <button type="submit" class="btn btn-default btn-lg"><i class="fa fa-lock"></i> Close</button>
	              {{ Form::close() }}
	              @endif
	              <a href="{{ URL::route('end_product') }}" class="btn btn-primary btn-lg" >
	              <i class="fa fa-list"></i> Go to List</a> 
	            </div>
	  				  <br>
					  </div> 
	          @actionEnd
			    </div>
			  </div>
		  </div>
	  </div>
	</section>
</section>
@stop
