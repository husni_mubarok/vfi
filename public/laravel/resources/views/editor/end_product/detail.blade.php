@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li class="active"><a href="{{ URL::route('editor.sashimi.index') }}"><i class="fa fa-cube"></i> Sashimi</a></li>
  </ol>
</section>

<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-10">
			        <div class="x_panel">
		                <h2>
		                	@if(isset($sashimi))
		                	<i class="fa fa-pencil"></i>
		                	@else
		                	<i class="fa fa-plus"></i> 
		                	@endif
		                	<i class="fa fa-cube"></i> Sashimi
	                	</h2>
		                <hr>
			            <div class="x_content">
			                @include('errors.error')
	                        @if(isset($sashimis))
			                {!! Form::model($sashimi, array('route' => ['editor.sashimi.update', $sashimi->id], 'method' => 'PUT'))!!}
		                    @else
		                    {!! Form::open(array('route' => ['editor.sashimi.store_detail', $sashimi->id]))!!}
		                    @endif
		                    {{ csrf_field() }}
		                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
		                    	<table class="table table-striped dataTable">
							  	<thead>
							  	  	<tr>
								      	<th>#</th>
								      	<th>Name</th>
								      	<th>Start Date</th>
								      	<th>Finish Date</th>
								      	<th>Size</th>
								      	<th>Block</th>
								      	<th></th>
							    	</tr>
							  	</thead>
							  	<tbody>
							    @foreach($sashimi->production_result->production_result_detail as $key => $product)
							    	<tr>
							      		<td>{{$key+1}}</td>
								      	<td>{{$sashimi->production_result->production->name}}</td>
								      	<td>{{date("D, d M Y", strtotime($sashimi->production_result->started_at))}}</td>
								      	<td>{{date("D, d M Y", strtotime($sashimi->production_result->finished_at))}}</td>
								      	<td>{{$product->size}}</td>
								      	<td>{{$product->block}}</td>
								      	<td align="center">
								      		<input type="number" name="item_{{$product->id}}" class="form-control" id="item_{{$product->id}}">
							      		</td>
								    </tr>
							    @endforeach
								</tbody>
								</table>

	                            <button type="submit" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
	                    	</div>
                        	{!! Form::close() !!}
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@stop

@section('scripts')
<script>
$(document).ready(function () {
	$('#date').datetimepicker({
		sideBySide: true,
		format: 'YYYY-MM-DD',
	});
});
</script>
@stop