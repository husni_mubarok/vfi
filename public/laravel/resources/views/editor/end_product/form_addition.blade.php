@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li><a href="{{ URL::route('editor.end_product.index') }}"><i class="fa fa-cube"></i> End Product</a></li>
    <li class="active"><a href="#"><i class="fa fa-plus-square-o"></i> Addition</a></li>
  </ol>
</section>

<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<div class="x_panel">
						<h2>
							@if(isset($end_product_addition))
		                	<i class="fa fa-pencil"></i> 
		                	@else
		                	<i class="fa fa-plus"></i> 
		                	@endif
		                	<i class="fa fa-plus-square-o"></i>
		                	@if($end_product->production_type == 'sashimi')
		                	Sashimi 
		                	@elseif($end_product->production_type == 'value_added') 
		                	Value Added
		                	@endif
		                	<small>Additional Product</small>
	                	</h2>
			    	  	<hr>
			    	  	<div class="x_content">
			    	  		@actionStart('end_product', 'create|update')
		    	  			{{-- Reject Products --}}
			            	<div class="col-md-6">
				                @include('errors.error')
				                @if(isset($end_product_addition))
				                {!! Form::model($end_product_addition, ['route' => ['editor.end_product.update_addition', $end_product->id], 'method' => 'PUT']) !!}
				                @else
			                    {!! Form::open(array('route' => ['editor.end_product.store_addition', $end_product->id]))!!}
			                    @endif
			                    {{ csrf_field() }}
			                    <div class="col-md-12">
				                    <div class="col-md-4">
				                    	{{ Form::label('Size type') }}
				                    	<select id="size_type" class="form-control">
				                    		<option value="single">Single</option>
				                    		<option value="range">Range</option>
				                    	</select>
				                    	<br>
				                    </div>

				                    <div class="col-md-4">
				                    	{{ Form::label('Reject type') }}
				                    	{{ Form::select('', $reject_type_list, null, ['class' => 'form-control', 'id' => 'reject_type']) }}
				                    	<br>
				                    </div>

				                    <div class="col-md-4">
				                    	{{ Form::label('Reason') }}
				                    	{{ Form::select('', $reject_reason_list, null, ['class' => 'form-control', 'id' => 'reject_reason']) }}
				                    </div>
				                    <br>
			                    </div>

			                    <div class="col-md-9 form-group" class="form-inline">
			                    	<div class="col-md-2"><h4>{{ Form::label('Size') }}</h4></div>
									<div class="col-md-10" id="input_single">
										<input type="number" id="size_single" class="form-control">
									</div>
									<div class="col-md-10" id="input_range" style="display:none">
										<div class="col-md-5">
											<input type="number" id="size_range_1" class="form-control">
										</div>
										<div class="col-md-2">
											<i class="fa fa-arrows-h"></i>
										</div>
										<div class="col-md-5">
											<input type="number" id="size_range_2" class="form-control">
										</div>
									</div>
			                    </div>

			                    <div class="col-md-3" align="center">
			                    	<button type="button" class="btn btn-primary" id="btn_add" disabled><i class="fa fa-plus"></i></button>
			                    </div>

			                    <h4>&nbsp;</h4>

			                    <table class="table table-bordered" width="100%">
			                    <thead>
			                    	<th>Type</th>
			                    	<th>Size</th>
			                    	<th>Tray</th>
			                    	<th>Reason</th>
			                    	<td></td>
			                    </thead>
		                    	<tbody id="cart">
		                    	</tbody>
			                    </table>
			                    <button type="button" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
	                    		{!! Form::close() !!}
	                		</div>
	                		{{-- Reject Products --}}
	                		@actionEnd

	                		@actionStart('end_product', 'read')
	                		{{-- Production Details --}}
	                		<div class="col-md-6">
				        		<h4>Material</h4>
				        		<table class="table table-bordered">
				          			<tr>
					            		<th>Product</th>
				            			<td>{{$end_product->production->name}}</td>
				          			</tr>
				          			<tr>
				            			<th>Weight</th>
				            			<td>{{number_format($end_product->weight, 2)}} kg</td>
				          			</tr>
				          			<tr>
				            			<th>Qty / Tray</th>
				            			<td>{{number_format($end_product->block_weight, 0)}}</td>
				          			</tr>
				          			<tr>
				            			<th>Production Date</th>
				            			<td>
				              				{{date("D, d M Y", strtotime($end_product->started_at))}}
				              				&nbsp;<i class="fa fa-long-arrow-right"></i>&nbsp;
				              				@if($end_product->finished_at == null)
				              				<i class="fa fa-question-circle-o"></i>
				              				@else
				              				{{date("D, d M Y", strtotime($end_product->finished_at))}} 
				              				@endif
				            			</td>
				          			</tr>
				          			<tr>
				            			<th>Criteria</th>
				            			<td>{{$end_product->criteria}}</td>
				          			</tr>
		        				</table>
				      		</div>
	                		{{-- Production Details --}}
	                		@actionEnd
				        </div>
		    	  	</div>
	    	  	</div>
		  	</div>
	  	</div>
	</section>
</section>
@stop

@section('scripts')
<script>
var cart = JSON.parse("{}");

// SWITCH SIZE FORM
$("#size_type").on('change', function() {
	if($("#size_type option:selected").val() == 'single')
	{
		$("#size_single").val('');
		$("#size_range_1").val('');
		$("#size_range_2").val('');
		$("#input_single").show();
		$("#input_range").hide();
	} 
	else if($("#size_type option:selected").val() == 'range') 
	{
		$("#size_single").val('');
		$("#size_range_1").val('');
		$("#size_range_2").val('');
		$("#input_single").hide();
		$("#input_range").show();
	}
});

// ADD BUTTON CHECK
$("#size_single, #size_range_1, #size_range_2").on('change', function() {
	var size_single = $("#size_single").val();
	var size_range_1 = $("#size_range_1").val();
	var size_range_2 = $("#size_range_2").val();

	if(size_single != '')
	{
		$("#btn_add").attr('disabled', false);
	} 
	else if (size_range_1 != '' && size_range_2 != '') 
	{
		$("#btn_add").attr('disabled', false);
	}
	else 
	{
		$("#btn_add").attr('disabled', true);
	}
});

// ADD BUTTON EVENT
$("#btn_add").on('click', function() {
	if($("#size_type option:selected").val() == 'single')
	{
		var reject_type = $("#reject_type option:selected").val();
		var size_val = $("#size_single").val();
		var reject_reason = $("#reject_reason option:selected").val();
		var key = reject_type+"_"+reject_reason+"_"+size_val;
		if(cart[key] == undefined)
		{
			cart[key] = 1;
			var new_row = '';
			new_row += '<tr>';
			new_row += '<td>';
			new_row += '<input type="hidden" name="type['+key+']" value="'+reject_type+'">';
			new_row += $("#reject_type option:selected").text();
			new_row += '</td>';
			new_row += '<td>';
			new_row += '<input type="hidden" name="size['+key+']" value="'+size_val+'">';
			new_row += size_val;
			new_row += '</td>';
			new_row += '<td>';
			new_row += '<input type="hidden" name="block['+key+']" value="'+cart[key]+'" id="block_'+key+'">';
			new_row += '<div id="text_'+key+'">'+cart[key]+'</div>';
			new_row += '</td>';
			new_row += '<td>';
			new_row += '<input type="hidden" name="reason['+key+']" value="'+reject_reason+'">';
			new_row += $("#reject_reason option:selected").text();
			new_row += '</td>';
			new_row += '<td>';
			new_row += '<button type="button" class="btn btn-primary btn-sm" id="btn_add_'+key+'">';
			new_row += '<i class="fa fa-plus"></i>';
			new_row += '</button>';
			new_row += '<button type="button" class="btn btn-warning btn-sm" id="btn_reduce_'+key+'">';
			new_row += '<i class="fa fa-minus"></i>';
			new_row += '</button>';
			new_row += '<button type="button" class="btn btn-danger btn-sm" id="btn_remove_'+key+'">';
			new_row += '<i class="fa fa-remove"></i>';
			new_row += '</button>';
			new_row += '</td>';
			new_row += '</tr>';

			$("#cart").append(new_row);
			$("#size_single").val('');
			$("#btn_add").attr('disabled', true);

			$("#btn_add_"+key).on('click', function() 
			{
				cart[key] += 1;
				$("#block_"+key).val(cart[key]);
				$("#text_"+key).text(cart[key]);
			});

			$("#btn_reduce_"+key).on('click', function() {
				if(cart[key] == 1)
				{
					cart[key] == null;
					$(this).parent().parent().remove();
				}
				else 
				{
					cart[key] -= 1;
					$("#block_"+key).val(cart[key]);
					$("#text_"+key).text(cart[key]);
				}
			});

			$("#btn_remove_"+key).on('click', function() 
			{
				cart[key] == null;
				$(this).parent().parent().remove();
			});
		} 
		else 
		{
			cart[key] += 1;
			$("#block_"+key).val(cart[key]);
			$("#text_"+key).text(cart[key]);
		}
	} 
	else if ($("#size_type option:selected").val() == 'range')
	{
		var reject_type = $("#reject_type option:selected").val();
		var size_val = $("#size_range_1").val()+"_"+$("#size_range_2").val();
		var reject_reason = $("#reject_reason option:selected").val();
		var key = reject_type+"_"+reject_reason+"_"+size_val;
		if(cart[key] == undefined)
		{
			cart[key] = 1;
			var new_row = '';
			new_row += '<tr>';
			new_row += '<td>';
			new_row += '<input type="hidden" name="type['+key+']" value="'+reject_type+'">';
			new_row += $("#reject_type option:selected").text();
			new_row += '</td>';
			new_row += '<td>';
			new_row += '<input type="hidden" name="size['+key+']" value="'+size_val+'">';
			new_row += size_val;
			new_row += '</td>';
			new_row += '<td>';
			new_row += '<input type="hidden" name="block['+key+']" value="'+cart[key]+'" id="block_'+key+'">';
			new_row += '<div id="text_'+key+'">'+cart[key]+'</div>';
			new_row += '</td>';
			new_row += '<td>';
			new_row += '<input type="hidden" name="reason['+key+']" value="'+reject_reason+'">';
			new_row += $("#reject_reason option:selected").text();
			new_row += '</td>';
			new_row += '<td>';
			new_row += '<button type="button" class="btn btn-primary btn-sm" id="btn_add_'+key+'">';
			new_row += '<i class="fa fa-plus"></i>';
			new_row += '</button>';
			new_row += '<button type="button" class="btn btn-warning btn-sm" id="btn_reduce_'+key+'">';
			new_row += '<i class="fa fa-minus"></i>';
			new_row += '</button>';
			new_row += '<button type="button" class="btn btn-danger btn-sm" id="btn_remove_'+key+'">';
			new_row += '<i class="fa fa-remove"></i>';
			new_row += '</button>';
			new_row += '</td>';
			new_row += '</tr>';

			$("#cart").append(new_row);
			$("#size_range_1").val('');
			$("#size_range_2").val('');
			$("#btn_add").attr('disabled', true);

			$("#btn_add_"+key).on('click', function() 
			{
				cart[key] += 1;
				$("#block_"+key).val(cart[key]);
				$("#text_"+key).text(cart[key]);
			});

			$("#btn_reduce_"+key).on('click', function() {
				if(cart[key] == 1)
				{
					cart[key] == null;
					$(this).parent().parent().remove();
				}
				else 
				{
					cart[key] -= 1;
					$("#block_"+key).val(cart[key]);
					$("#text_"+key).text(cart[key]);
				}
			});

			$("#btn_remove_"+key).on('click', function() 
			{
				cart[key] == null;
				$(this).parent().parent().remove();
			});
		} 
		else 
		{
			cart[key] += 1;
			$("#block_"+key).val(cart[key]);
			$("#text_"+key).text(cart[key]);
		}
	}
});
</script>

@if(isset($end_product_addition))
<script>
jQuery.each({!! $end_product_addition !!}, function( i, val ) {
	var type = val['reject_type_id'];
	var size = val['size'];
	var block = val['block'];
	var reason = val['reject_reason_id'];
	$("#reject_type").val(type);
	$("#reject_reason").val(reason);
	var key = type+"_"+reason+"_"+size;
	cart[key] = block;
   	var new_row = '';
	new_row += '<tr>';
	new_row += '<td>';
	new_row += '<input type="hidden" name="type['+key+']" value="'+type+'">';
	new_row += $("#reject_type option:selected").text();
	new_row += '</td>';
	new_row += '<td>';
	new_row += '<input type="hidden" name="size['+key+']" value="'+size+'">';
	new_row += size;
	new_row += '</td>';
	new_row += '<td>';
	new_row += '<input type="hidden" name="block['+key+']" value="'+cart[key]+'" id="block_'+key+'">';
	new_row += '<div id="text_'+key+'">'+cart[key]+'</div>';
	new_row += '</td>';
	new_row += '<td>';
	new_row += '<input type="hidden" name="reason['+key+']" value="'+reason+'">';
	new_row += $("#reject_reason option:selected").text();
	new_row += '</td>';
	new_row += '<td>';
	new_row += '<button type="button" class="btn btn-primary btn-sm" id="btn_add_'+key+'">';
	new_row += '<i class="fa fa-plus"></i>';
	new_row += '</button>';
	new_row += '<button type="button" class="btn btn-warning btn-sm" id="btn_reduce_'+key+'">';
	new_row += '<i class="fa fa-minus"></i>';
	new_row += '</button>';
	new_row += '<button type="button" class="btn btn-danger btn-sm" id="btn_remove_'+key+'">';
	new_row += '<i class="fa fa-remove"></i>';
	new_row += '</button>';
	new_row += '</td>';
	new_row += '</tr>';

	$("#cart").append(new_row);

	$("#btn_add_"+key).on('click', function() 
	{
		cart[key] += 1;
		$("#block_"+key).val(cart[key]);
		$("#text_"+key).text(cart[key]);
	});

	$("#btn_reduce_"+key).on('click', function() {
		if(cart[key] == 1)
		{
			cart[key] == null;
			$(this).parent().parent().remove();
		}
		else 
		{
			cart[key] -= 1;
			$("#block_"+key).val(cart[key]);
			$("#text_"+key).text(cart[key]);
		}
	});

	$("#btn_remove_"+key).on('click', function() 
	{
		cart[key] == null;
		$(this).parent().parent().remove();
	});
});
</script>
@endif
@stop