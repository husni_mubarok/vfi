@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="{{ URL::route('editor.placement_storage.index') }}"><i class="fa fa-dropbox"></i> Placement Storage</a></li>
    @if(isset($placement_storage))
    <li class="active"><a href="#"><i class="fa fa-pencil"></i> Edit</a></li>
    @else
    <li class="active"><a href="#"><i class="fa fa-plus"></i> Create</a></li>
    @endif
  </ol>
</section>
@actionStart('placement_storage', 'create|update')
<section class="content">
  <section class="content box box-solid">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-1"></div>
        <div class="col-md-5">
          <div class="x_panel">
            <h2>
              @if(isset($placement_storage))
              <i class="fa fa-pencil"></i>
              @else
              <i class="fa fa-plus"></i> 
              @endif
              <i class="fa fa-dropbox"></i> Placement Storage
            </h2>
            <hr>
            <div class="x_content">
              @include('errors.error')
              @if(isset($placement_storage))
              {{ Form::model($placement_storage, array('route' => array('editor.placement_storage.update', $placement_storage->id), 'method' => 'put')) }}
              @else
              {!! Form::open(array('route' => ['editor.placement_storage.store']))!!}
              @endif
              {{ csrf_field() }}
              <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                {{ Form::label('address', 'Address') }}
				        {{ Form::text('address', old('address'), array('class' => 'form-control', 'placeholder' => 'Address*')) }}<br/>

                {{ Form::label('notes', 'Notes') }}
				        {{ Form::text('notes', old('notes'), array('class' => 'form-control', 'placeholder' => 'Notes')) }}<br/>

                <button type="submit" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>

              </div>
              {!! Form::close() !!} 
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</section>
@stop
@actionEnd