@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="{{ URL::route('editor.purchase.index') }}"><i class="fa fa-shopping-cart"></i> Purchase</a></li>
    <li class="active"><a href="#"><i class="fa fa-file-text-o"></i> Summary</a></li>
  </ol>
</section>
@actionStart('purchase', 'read')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-10">
			        <div class="x_panel">
		                <h2>
		                	<i class="fa fa-file-text-o"></i> 
		                	{{$purchase->doc_code}}
		                	<i class="fa fa-caret-right"></i>
		                	{{$purchase->description}}
		                	@if($purchase->status == 'PO RM' || $purchase->status == 'Actual Delivery')
							<span class="label label-danger">{{$purchase->status}}</span>
							@elseif($purchase->status == 'Waiting COO' || $purchase->status == 'Waiting QC')
							<span class="label label-warning">{{$purchase->status}}</span>
							@elseif($purchase->status == 'Approved')
							<span class="label label-success">{{$purchase->status}}</span>
							@endif  
							<div class="pull-right">
							<button type="button" data-toggle="modal" data-target="#reject_history" class="btn btn-default btn-lg"><i class="fa fa-history"></i>&nbsp;Reject History</button>

		                	@if($purchase->status == 'Approved')
		                	{{-- <a href="{{ URL::route('editor.purchase.pdf', $purchase->id) }}" class="btn btn-default btn-lg"><i class="fa fa-file-pdf-o"></i>&nbsp;Download</a> --}}
		                	@elseif($purchase->status == 'PO RM' || $purchase->status == 'Actual Delivery')
		                	@actionStart('purchase', 'update')
		                	<a href="{{ URL::route('editor.purchase.edit', $purchase->id) }}" class="btn btn-default btn-lg"><i class="fa fa-pencil"></i>&nbsp;Edit</a>
		                	@actionEnd
		                	@endif
		                	</div>
	                	</h2>
		                <hr>
			            <div class="x_content">
			            	<div class="col-md-6">
			            		<h4><i class="fa fa-info-circle"></i>&nbsp;Information</h4>

	                			<h3><span class="label label-primary">Production Code: {{$purchase->trans_code}}</span></h3>
	                			<br/>
			            		<table class="table table-bordered">
			            			<tr>
			            				<th>Supplier</th>
			            				<td>
			            					<button type="button" data-toggle="modal" data-target="#supplier_detail" class="btn btn-default btn-sm"><i class="fa fa-search"></i>&nbsp;
			            					{{$purchase->purchase_supplier->supplier->name}}
			            					</button>
		            					</td>
			            				<th>Estimated Price</th>
			            				<td>{{$purchase->currency}}&nbsp;{{number_format($purchase->estimated_price)}}</td>
			            			</tr>
			            			<tr>
			            				<th>Purchase Time</th>
			            				<td>{{date("D, d M Y", strtotime($purchase->purchase_time))}}</td>
			            				<th>PPN</th>
			            				<td>@if($purchase->ppn == 'on') Yes @else No @endif</td>
			            			</tr>
			            			<tr>
			            				<th>Product</th>
			            				<td>{{$purchase->purchase_product->product->product_type->name}}&nbsp;{{$purchase->purchase_product->product->name}}</td>
			            				<th>DP</th>
			            				<td>{{$purchase->currency}}&nbsp;{{number_format($purchase->down_payment)}}</td>
			            			</tr>
			            			<tr>
			            				<th>Project</th>
			            				<td>{{$purchase->project}}</td>
			            				<th>Discount</th>
			            				<td>{{$purchase->currency}}&nbsp;{{number_format($purchase->discount)}}</td>
			            			</tr>  
			            			<tr>
			            				<th>Terms of Condition</th>
			            				<td>
			            					<button type="button" data-toggle="modal" data-target="#term_condition" class="btn btn-default btn-sm"><i class="fa fa-list-ol"></i>&nbsp;Show</button>
			            				</td>
			            				<th>Terms of Paymet</th>
			            				<td>
			            					<button type="button" data-toggle="modal" data-target="#term_payment" class="btn btn-default btn-sm"><i class="fa fa-money"></i>&nbsp;Show</button>
			            				</td>
			            			</tr>         			
			            			{{-- <tr>
			            				@php
			            				$reject = '-';
			            				foreach($purchase->purchase_reject as $purchase_reject)
			            				{
			            					$reject = $purchase_reject->reason;	
			            				}

			            				@endphp
			            				<th>Reject</th>
			            				<td>@if($purchase->purchase_reject->count() == 0) No @else Yes @endif</td>
			            				<th>Reason</th>
			            				<td>{{$reject}}</td>
			            			</tr> --}}
			            		</table>

								@actionStart('purchase_detail', 'read')
								@if($purchase->purchase_detail->count() > 0)
								<hr>
								<h4><i class="fa fa-shopping-cart"></i>&nbsp;Purchase Detail</h4>
				            	@php $total = 0; $total_weight = 0; @endphp
				                <table class="table table-bordered table-hover">
				                	<thead>
								  	  	<tr>
									      	<th>No.</th>
											<th>Size</th>
											<th>Amount (kg)</th>
											<th>Price ({{$purchase->currency}})</th>
											<th>Subtotal</th>
								    	</tr>
							    	</thead>
							    	<tbody>
				                	@foreach($purchase->purchase_detail as $key => $purchase_detail)
				                		@php
				                		$subtotal = $purchase_detail->amount * $purchase_detail->price;
				                		$total += $subtotal;
				                		$total_weight += $purchase_detail->amount;
				                		@endphp
							    		<tr>
							    			<th scope="row">{{$key+1}}</th>
							    			<td align="right">{{$purchase_detail->size}}</td>
							    			<td align="right">{{number_format($purchase_detail->amount)}}&nbsp;{{$purchase_detail->uom}}</td>
							    			<td align="right">{{number_format($purchase_detail->price)}}&nbsp;{{$purchase->currency}}</td>
							    			<td align="right">{{number_format($subtotal)}}&nbsp;{{$purchase->currency}}</td>
							    		</tr>
							    	@endforeach
							    	</tbody>
							    	<tfoot>
							    		<tr>
							    			<td><b>Total</b></td>
							    			<td><b>Weight</b></td>
							    			<td align="right">{{number_format($total_weight)}}</td>
							    			<td><b>Price</b></td>
							    			<td align="right">{{number_format($total)}}&nbsp;{{$purchase->currency}}</td>
							    		</tr>
							    	</tfoot>
								</table>
								@endif
								@actionEnd

								<div align="right">
									@if($purchase->status == 'PO RM')
										@if($purchase->price->count() == 0)
										@actionStart('price', 'create')
										<a href="{{ URL::route('editor.price.create', $purchase->id) }}" style="display:inline-block;" class="btn btn-primary"><i class="fa fa-dollar"></i>&nbsp;Price</a>
										@actionEnd
										@else
										@actionStart('price', 'update')
										<a href="{{ URL::route('editor.price.edit', $purchase->id) }}" style="display:inline-block;" class="btn btn-primary"><i class="fa fa-dollar"></i>&nbsp;Price</a>
										@actionEnd
										@actionStart('purchase', 'update')
										<a href="{{ URL::route('editor.purchase.confirm_po_rm', $purchase->id) }}" class="btn btn-primary" onclick="return confirm('Confirm Purchase?')"><i class="fa fa-lock"></i>&nbsp;Confirm</a>
										@actionEnd
										@endif

									@elseif($purchase->status == 'Waiting COO')
										@actionStart('price', 'approve')
										<button type="button" class="btn btn-success" data-toggle="modal" data-target="#approve_po_rm_form"><i class="fa fa-check"></i>&nbsp;Approve</button>
										@actionEnd

										@actionStart('price', 'reject')
										<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#reject_po_rm_form"><i class="fa fa-remove"></i>&nbsp;Reject</button>
										@actionEnd

									@elseif($purchase->status == 'Actual Delivery')
										@if($purchase->purchase_detail->count() == 0)
										@actionStart('purchase_detail', 'create')
										<a href="{{ URL::route('editor.purchase_detail.create', $purchase->id) }}" style="display:inline-block;" class="btn btn-primary"><i class="fa fa-shopping-cart"></i>&nbsp;Cart</a>
										@actionEnd
										@else
										@actionStart('purchase_detail', 'update')
										<a href="{{ URL::route('editor.purchase_detail.edit', $purchase->id) }}" style="display:inline-block;" class="btn btn-primary"><i class="fa fa-shopping-cart"></i>&nbsp;Cart</a>
										@actionEnd
										@actionStart('purchase', 'update')
										<a href="{{ URL::route('editor.purchase.confirm_actual_delivery', $purchase->id) }}" class="btn btn-primary" onclick="return confirm('Confirm Purchase?')"><i class="fa fa-lock"></i>&nbsp;Confirm</a>
										@actionEnd
										@endif

									@elseif($purchase->status == 'Waiting QC')
										@actionStart('purchase_detail', 'approve')
										<button type="button" class="btn btn-success" data-toggle="modal" data-target="#approve_actual_delivery_form"><i class="fa fa-check"></i>&nbsp;Approve</button>
										@actionEnd

										@actionStart('purchase_detail', 'reject')
										<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#reject_actual_delivery_form"><i class="fa fa-remove"></i>&nbsp;Reject</button>
										@actionEnd

									@endif
								</div>
			            	</div>
			            	<div class="col-md-6">
			            		@if($purchase->price->count() > 0)
			            		@actionStart('price', 'read')
			            		<h4><i class="fa fa-dollar"></i>&nbsp;Price List</h4>
			            		<table class="table table-bordered">
			            			<tr>
			            				<th>Min.</th>
			            				<th>Max.</th>
			            				<th>Note</th>
			            				<th>Price ({{$purchase->currency}})</th>
			            			</tr>
			            			@foreach($purchase->price as $price)
			            			<tr>
			            				<td>{{$price->size->min_value}}</td>
			            				<td>{{$price->size->max_value}}</td>
			            				<td>{{$price->size->note}}</td>
			            				<td>{{number_format($price->value)}}&nbsp;{{$purchase->currency}}</td>
			            			</tr>
			            			@endforeach
			            		</table>
			            		@actionEnd
			            		@endif
			            	</div>
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@actionEnd
@stop

@section('modal')
<div class="modal fade" id="reject_history" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="myModalLabel">Reject History</h4>
      		</div>
      		<table class="table">
      		<tr>
      			<th>Date</th>
      			<th>Rejected at</th>
      			<th>Reason</th>
      		</tr>
      		@foreach($purchase->purchase_reject as $purchase_reject)
      		<tr>
      			<th>{{date("D, d-m-Y", strtotime($purchase_reject->reject_date))}}</th>
      			<th>{{$purchase_reject->reject_step}}</th>
      			<th>{{$purchase_reject->reason}}</th>
      		</tr>
      		@endforeach
      		</table>
      		<div class="modal-footer">
      			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i>&nbsp;Close</button>
      		</div>
    	</div>
  	</div>
</div>
<div class="modal fade" id="supplier_detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="myModalLabel">Supplier Detail</h4>
      		</div>
      		<table class="table">
	      		<tr>
	      			<th>Name</th>
	      			<td>{{$purchase->purchase_supplier->supplier->name}}</td>
	      		</tr>
	      		<tr>
	      			<th>Address</th>
	      			<td>{{$purchase->purchase_supplier->supplier->address}}</td>
	      		</tr>
	      		<tr>
	      			<th>E-mail Address</th>
	      			<td>{{$purchase->purchase_supplier->supplier->email}}</td>
	      		</tr>
	      		<tr>
	      			<th>Phone Number</th>
	      			<td>{{$purchase->purchase_supplier->supplier->phone_number}}</td>
	      		</tr>
	      		<tr>
	      			<th>Fax</th>
	      			<td>{{$purchase->purchase_supplier->supplier->fax}}</td>
	      		</tr>
	      		<tr>
	      			<th>Website</th>
	      			<td>{{$purchase->purchase_supplier->supplier->website}}</td>
	      		</tr>
	      		<tr>
	      			<th>PIC</th>
	      			<td>{{$purchase->purchase_supplier->supplier->pic}}</td>
	      		</tr>
	      		<tr>
	      			<th>Bank Account</th>
	      			<td>{{$purchase->purchase_supplier->supplier->bank_account}}</td>
	      		</tr>
	      		<tr>
	      			<th>Note</th>
	      			<td>{{$purchase->purchase_supplier->supplier->note}}</td>
	      		</tr>
      		</table>
      		<div class="modal-footer">
      			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i>&nbsp;Close</button>
      		</div>
    	</div>
  	</div>
</div>
<div class="modal fade" id="term_condition" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="myModalLabel">Terms of Condition</h4>
      		</div>
      		<div class="modal-body">
      			{!! $purchase->term_condition !!}
      		</div>
      		<div class="modal-footer">
      			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i>&nbsp;Close</button>
      		</div>
    	</div>
  	</div>
</div>
<div class="modal fade" id="term_payment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="myModalLabel">Terms of Payment</h4>
      		</div>
      		<div class="modal-body">
      			{!! $purchase->term_payment !!}
      		</div>
      		<div class="modal-footer">
      			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i>&nbsp;Close</button>
      		</div>
    	</div>
  	</div>
</div>
<div class="modal fade" id="reject_po_rm_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="myModalLabel">Reject Form</h4>
      		</div>
      		{!! Form::open(array('route' => ['editor.purchase.reject_po_rm', $purchase->id]))!!}
      		<div class="modal-body">
      			{{ Form::label('reason', 'Reason') }}
                {{ Form::text('reason', old('reason'), array('class' => 'form-control', 'placeholder' => 'Reason', 'required' => 'true')) }}<br/>
      		</div>
      		<div class="modal-footer">
      			<button type="submit" class="btn btn-success" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
      		</div>
      		{!! Form::close() !!}
    	</div>
  	</div>
</div>
<div class="modal fade" id="approve_po_rm_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="myModalLabel">Approval Form</h4>
      		</div>
      		{!! Form::open(array('route' => ['editor.purchase.approve_po_rm', $purchase->id]))!!}
      		<div class="modal-body">
      			Approve PO RM?
      			<br/>
      		</div>
      		<div class="modal-footer">
      			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i>&nbsp;No</button>
      			<button type="submit" class="btn btn-primary" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> &nbsp;Yes</button>
      		</div>
      		{!! Form::close() !!}
    	</div>
  	</div>
</div>
<div class="modal fade" id="reject_actual_delivery_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="myModalLabel">Reject Form</h4>
      		</div>
      		{!! Form::open(array('route' => ['editor.purchase.reject_actual_delivery', $purchase->id]))!!}
      		<div class="modal-body">
      			{{ Form::label('reason', 'Reason') }}
                {{ Form::text('reason', old('reason'), array('class' => 'form-control', 'placeholder' => 'Reason', 'required' => 'true')) }}<br/>
      		</div>
      		<div class="modal-footer">
      			<button type="submit" class="btn btn-success" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
      		</div>
      		{!! Form::close() !!}
    	</div>
  	</div>
</div>
<div class="modal fade" id="approve_actual_delivery_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="myModalLabel">Approval Form</h4>
      		</div>
      		{!! Form::open(array('route' => ['editor.purchase.approve_actual_delivery', $purchase->id]))!!}
      		<div class="modal-body">
      			Approve Actual Delivery?
      			<br/>
      		</div>
      		<div class="modal-footer">
      			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i>&nbsp;No</button>
      			<button type="submit" class="btn btn-primary" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> &nbsp;Yes</button>
      		</div>
      		{!! Form::close() !!}
    	</div>
  	</div>
</div>
@stop