<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Purchase Summary</title>
<link href="style.css" rel="stylesheet" />
</head>

<body>
<div id='logo' style="width:300px; border:0px solid #000; text-align:center; float:left; margin-bottom:30px;">
	<img src="{{Config::get('constants.path.img')}}/logo_vfi.png" height="55px">
	<div id='comp_name'>
	Vessel Freshfish Indomakmur
	</div>
	<div id='comp_web'>
	www.vesselfreshfish.com
	</div>
</div>

<div style='font-size:10px; float:left; border:0px solid #000; width:400px'>
	<strong>???</strong><br>
	Jl. Industri Raya II Blok J No. 5<br>
	Jatake, Tangerang 15136<br>
	Banten<br>
	Phone : +6221 59304950, +6221 5901538, +6221 59319591<br>
	Fax : +6221 5901916<br>
	Email : marketing@vesselfreshfish.com<br>
	NPWP : 76.403.310.6-402.000
</div>

<div id='doc_name' style="clear:right; border:0px solid #000; float:left; width:200px; text-align:right; font-size:20px; font-weight:bold">
Purchase Order
<br />FLAG SOMETHING
<br />
<span style="text-align:center"><img src="{{Config::get('constants.path.img')}}/cod.png" /></span>
</div>

<div style="clear:both;">
<p>
<table border='0' width="900">
	<tr valign="top">
		<td width="33%">
			<table border='0' width="100%">
				<tr valign='top'>
					<td>Supplier:</td>
					<td>{{$purchase->purchase_supplier->supplier->name}}</td>
				</tr>

				<tr valign='top'>
					<td>Address:</td>
					<td>{{$purchase->purchase_supplier->supplier->address}}</td>
				</tr>

				<tr valign='top'>
					<td>Telephone:</td>
					<td>{{$purchase->purchase_supplier->supplier->phone_number}}</td>
				</tr>

				<tr valign='top'>
					<td>Email:</td>
					<td>{{$purchase->purchase_supplier->supplier->email}}</td>
				</tr>
			</table>
		</td>


		<td style="border-left:1px solid #000; border-right:1px solid #000; padding-left:5px; padding-right:5px;" width="33%">
			Deliver To:<br>
			PT VESSEL FRESHFISH INDOMAKMUR<br>
			JL INDUSTRI RAYA II BLOK J-5 JATAKE TANGERANG
		</td>


		<td width="33%">
			<table border="0" width="100%">
			  <tr>
				 <td width='90'>PO Number:</td>
				 <td>{{$purchase->id}}</td>
			  </tr>
			  <tr>
				 <td>Request Date:</td>
				 <td>{{$purchase->purchase_time}}</td>
			  </tr>
			 <tr>
				 <td>Key Number:</td>
				 <td>KEY NUMBER</td>
			 </tr>

			 <tr>
				 <td>Reference:</td>
				 <td>REFERENCE</td>
			 </tr>
			 <tr>
				 <td>Project:</td>
				 <td>PROJECT</td>
			 </tr>
				<tr>
				 <td>Currency:</td>
				 <td>IDR</td>
			  </tr>
			  <tr valign="top">
				 <td>Notes:</td>
				 <td>
					 {{$purchase->description}}
				 </td>
			  </tr>
			</table>
		</td>

	</tr>
</table>
</div>

<table class='mytable' width="900">
	<tr>
		<th width="30">No.</th>
		<th width="100">Item Code</th>
		<th>Item Name</th>
		<th width="50">Size</th>
		<th width="50">Qty</th>
		<th width="100">Unit Price</th>
		<th width="100">Subtotal</th>
	</tr>
	@php 
	$total = 0; 
	@endphp
	@foreach($purchase->purchase_detail as $key => $purchase_detail)
	@php
	$subtotal = $purchase_detail->price->value * $purchase_detail->amount;
	$total += $subtotal;
	@endphp
	<tr valign='top'>
		<td align='right'>{{$key+1}}</td>
		<td>{{$purchase_detail->purchase->purchase_product->product->id}}</td>
		<td>
			{{$purchase_detail->purchase->purchase_product->product->product_type->name}}&nbsp;{{$purchase_detail->purchase->purchase_product->product->name}}
		</td>
		<td>{{$purchase_detail->size}}</td>
		<td align='center'>{{$purchase_detail->amount}}</td>
		<td align='right'>{{$purchase_detail->price->value}}</td>
		<td align='right'>{{$subtotal}}</td>
	</tr>
	@endforeach
	<tr>
	  <td align='right'>&nbsp;</td>
	  <td colspan="3" valign="top"><em>Please attach, payment requirments:
        <ol>
          <li>Original Purchase Order that has been sign and stamped by The Company.</li>
          <li>Payment account number</li>
          <li>Report / Time sheet job &amp; tools usage</li>
          <li>Original Tax Invoice</li>
        </ol>
	  </em></td>
	  <td align='right'>&nbsp;</td>
	  <td align='right'>&nbsp;</td>
	  <td align='right'>&nbsp;</td>
  </tr>
	<tr>
		<td rowspan="6" align='right'>&nbsp;</td>
		<td colspan="3" rowspan="6" valign="top">A. Term Condition<br />
      TERMS<br />
      <br />
B. Term of Payment<br />
PAYMENT TERM </td>
		<td colspan="2" align='right'>Sub Total:</td>
		<td align='right'>{{$total}}</td>
	</tr>
	<tr>
		<td colspan="2" align='right'>Discount:</td>
		<td align='right'>PRICE DISCOUNT</td>
	</tr>
	<tr>
		<td colspan="2" align='right'>Net Include discount:</td>
		<td align='right'>PRICE AFTER DISCOUNT</td>
	</tr>
	<tr>
		<td colspan='2' align='right'>PPN 10%:</td>
		<td align='right'>
			TAX</td>
	</tr>
	<tr>
		<td colspan='2' align='right'>Down Payment:</td>
		<td align='right'>DOWN PAYMENT</td>
	</tr>
	<tr>
		<td colspan='2' align='right'>Total Price:</td>
		<td align='right'>GRAND TOTAL</td>
	</tr>
	<tr>
		<td colspan='7'>
			Terbilang:
			TERBILANG</td>
	</tr>
</table>

<br />
<br />
<br />


<table width="900" border="0">
  <tr>
    <td align="center">
		<strong>
		SUPPLIER
			<br />
      <br />
      <br />
      <br />
      <br />[Meterai 6000]
      <br />
      <br />
      <br />
      <br />
			<br />
			<br />
    <u>SUPPLIER PIC</u></strong>
		</td>
    <td width="33%">
    APPROVED IMAGE
	</td>
    <td width="33%" align="center">
    	<img src="{{Config::get('constants.path.img')}}/paidRU.png" />
	</td>
  </tr>
</table>
</body>
</html>