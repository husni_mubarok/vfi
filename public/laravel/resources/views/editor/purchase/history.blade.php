@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="{{ URL::route('editor.purchase.index') }}"><i class="fa fa-shopping-cart"></i> Purchase</a></li>
    <li><a href="#"><i class="fa fa-history"></i> Reject History</a></li>
  </ol>
</section>
@actionStart('purchase', 'read')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-10">
			        <div class="x_panel">
		                <h2>
		                	<i class="fa fa-history"></i> Reject History
	                	</h2>
		                <hr>
			            <div class="x_content">
			                <table id="rejectHistoryTable" class="table table-striped dataTable">
							  	<thead>
							  	  	<tr>
								      	<th>#</th>
										<th>Reason</th>
										<th>Rejected by</th>
										<th>Status</th>
										<th>Time</th>
							    	</tr>
							  	</thead>
							  	<tbody>
								    @foreach($purchase->purchase_reject as $key => $purchase_reject)
									<tr>
										<td>{{$key+1}}</td>
										<td>{{$purchase_reject->reason}}</td>
										<td>{{$purchase_reject->user->username}}</td>
										<td>{{$purchase_reject->reject_step}}</td>
										<td>{{$purchase_reject->reject_date}}</td>
									</tr>
									@endforeach
								</tbody>
							</table>
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@actionEnd
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function () {
    $("#rejectHistoryTable").DataTable();
    });
</script>
@stop