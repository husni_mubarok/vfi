@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active"><a href="#"><i class="fa fa-shopping-cart"></i> Purchase</a></li>
  </ol>
</section>
@actionStart('purchase', 'read')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-10">
			        <div class="x_panel">
		                <h2>
		                	<i class="fa fa-shopping-cart"></i>&nbsp;Purchase List
		                	@actionStart('purchase', 'create')
		                	<a href="{{ URL::route('editor.purchase.create') }}" class="btn btn-lg btn-primary pull-right"><i class="fa fa-plus"></i> Add</a>
		                	@actionEnd
	                	</h2>
		                <hr>
			            <div class="x_content">
			                <table id="purchaseTable" class="table table-striped dataTable">
							  	<thead>
							  	  	<tr>
							  	  		<th>No.</th>
								      	<th>Code</th>
								      	<th>Production Code</th>
								      	<th>Status</th>
										<th>Description</th>
										<th>Estimated Price</th>
										<th>Supplier</th>
										<th>Product</th>
										<th>Purchase Time</th>
										<th>Action</th>
							    	</tr>
							  	</thead>
							  	<tbody>
							    @foreach($purchases as $key => $purchase)
								<tr>
									<td>{{$key+1}}</td>
									<td><a href="{{ URL::route('editor.purchase.summary', $purchase->id) }}">{{$purchase->doc_code}}</a></td>
									<td><span class="label label-default">{{$purchase->trans_code}}</span></td>
									<td>
										@if($purchase->status == 'PO RM' || $purchase->status == 'Actual Delivery')
										<span class="label label-danger">{{$purchase->status}}</span>
										@elseif($purchase->status == 'Waiting COO' || $purchase->status == 'Waiting QC')
										<span class="label label-warning">{{$purchase->status}}</span>
										@elseif($purchase->status == 'Approved')
										<span class="label label-success">{{$purchase->status}}</span>
										@endif
									</td>
									<td>{{$purchase->description}}</td>
									<td>{{$purchase->currency}}&nbsp;{{number_format($purchase->estimated_price)}}</td>
									<td>{{$purchase->purchase_supplier->supplier->name}}</td>
									<td>{{$purchase->purchase_product->product->product_type->name}}&nbsp;{{$purchase->purchase_product->product->name}}</td>
									<td>{{date("d M Y", strtotime($purchase->purchase_time))}}</td>
									<td style="text-align:center;">
									@if($purchase->status == 'PO RM')
										@if($purchase->price->count() == 0)
										@actionStart('price', 'create')
										<a href="{{ URL::route('editor.price.create', $purchase->id) }}" style="display:inline-block;" class="btn-sm btn-primary"><i class="fa fa-dollar"></i>&nbsp;Price</a>
										@actionEnd
										@else
										@actionStart('price', 'update')
										<a href="{{ URL::route('editor.price.edit', $purchase->id) }}" style="display:inline-block;" class="btn-sm btn-primary"><i class="fa fa-dollar"></i>&nbsp;Price</a>
										@actionEnd
										@endif

									@elseif($purchase->status == 'Waiting COO')
										<p>Waiting</p>

									@elseif($purchase->status == 'Actual Delivery')
										@if($purchase->purchase_detail->count() == 0)
										@actionStart('purchase_detail', 'create')
										<a href="{{ URL::route('editor.purchase_detail.create', $purchase->id) }}" style="display:inline-block;" class="btn-sm btn-primary"><i class="fa fa-shopping-cart"></i>&nbsp;Cart</a>
										@actionEnd
										@else
										@actionStart('purchase_detail', 'update')
										<a href="{{ URL::route('editor.purchase_detail.edit', $purchase->id) }}" style="display:inline-block;" class="btn-sm btn-primary"><i class="fa fa-shopping-cart"></i>&nbsp;Cart</a>
										@actionEnd
										@endif

									@elseif($purchase->status == 'Waiting QC')
										<p>Waiting</p>

									@elseif($purchase->status == 'Approved')
										<p>Done!</p>

									@endif
										{{-- @actionStart('purchase', 'delete')
						      			{!! Form::open(array('route' => ['purchase.delete', $purchase->id], 'method' => 'delete', 'style' => 'display:inline-block;padding-left:15px;'))!!}
	                					{{ csrf_field() }}	                    				
						      			<button type="submit" class="btn btn-primary btn-sm" onclick="return confirm('Confirm delete?')"><i class="fa fa-trash"></i></a></button>
						      			{!! Form::close() !!}
										@actionEnd --}}
									</td>
								</tr>
								@endforeach
								</tbody>
							</table>
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@actionEnd
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function () {
    $("#purchaseTable").DataTable(
    	{
    		"scrollX": true
    	});
    });
</script>
@stop