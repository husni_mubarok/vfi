@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->

<style type="text/css">
	th { font-size: 11px; }
	td { font-size: 11px; }
</style>
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><a href="#"><i class="fa fa-file-text-o"></i> Invoice Consummable</a></li>
	</ol>
</section>

<section class="content">
	<section class="content box mobile box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-1"></div>
				<div class="col-md-12">
					<div class="x_panel">
						<h2>
							<i class="fa fa-file-text-o"></i> Invoice Consummable 
							@actionStart('invoicedirect', 'create')
							<a href="{{ URL::route('editor.invoicedirect.create') }}" class="btn btn-success btn-lg pull-right"><i class="fa fa-plus"></i> Add</a> 
							@actionEnd
						</h2>
						<hr>

						<ul class="nav nav-tabs"> 
							<li><a  href="{{ URL::route('editor.invoicedirect.index') }}">Invoice  Consummable</a></li>  
							<li class="active"><a href="{{ URL::route('editor.invoicedirect.bank') }}">Invoice Consummable Bank</a></li> 
						</ul> 
						
						<div class="x_content">
							<table id="invoicedirectTablex" class="table dataTable rwd-table">
								<thead>
									<tr>
										<th>#</th>
										<th>Invoice Type</th>
										<th>Invoice Date</th> 
										<th>Vendor</th> 
										<th>Bank</th>
										<th>Rek No</th>
										<th>Paid Date</th>
										<th>Ref No</th>
										<th>Add Cost</th>
										<th>Add Cost Notes</th>
										<th>Total</th> 
										<th>Grand Total</th>
										<th>Invoice Document</th>
										<th>Receipt Document</th>
										<th>Branch</th>
										<th>Status</th>  
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									@foreach($invoicedirects as $key => $invoicedirect)
									<tr>
										<td data-th="#">{{$number++}}</td>
										<td data-th="Invoice Type">{{$invoicedirect->invoice_type->inv_type_name}}</td>
										<td data-th="Invoice Date">{{date("d M Y", strtotime($invoicedirect->invoice_date))}}</td>  
										<td data-th="Vendor">{{$invoicedirect->vendor->vendor_name}}</td> 
										<td data-th="Bank">{{$invoicedirect->invoice_bank}}</td> 
										<td data-th="Rek No">{{$invoicedirect->invoice_rekening}}</td>
										<th data-th="Ref No">{{date("d M Y", strtotime($invoicedirect->paid_date))}}</th> 
										<th data-th="Ref No">{{$invoicedirect->reference_no}}</th>
										<td data-th="Add Cost">{{ number_format($invoicedirect->additional_cost,0) }}</td>
										<td data-th="Add Cost Desc">{{$invoicedirect->add_cost_desc}}</td> 
										<td data-th="Total">{{ number_format($invoicedirect->invoice_total,0) }}</td>
										<td data-th="Grand Total">{{ number_format($invoicedirect->additional_cost+$invoicedirect->invoice_total,0) }}</td>
										<!-- <td>
											@if($invoicedirect->invoice_attachment == null)
											<br/><a class="fancybox" rel="group" href="{{Config::get('constants.path.uploads')}}/invoicedirect/placeholder.png"><img src="{{Config::get('constants.path.uploads')}}/invoicedirect/thumbnail/placeholder.png" class="img-thumbnail img-responsive" /></a><br/>
											@else
											<br/><a target="_blank" href="{{Config::get('constants.path.uploads')}}/invoicedirect/{{$invoicedirect->invoice_attachment}}"><img src="{{Config::get('constants.path.uploads')}}/invoicedirect/thumbnail/{{$invoicedirect->invoice_attachment}}" class="img-thumbnail img-responsive" height="42" width="42"/></a>
											<br/>
											@endif
										</td>  -->
										<td data-th="Attachment">
											@if($invoicedirect->invoice_attachment == null)
											Tidak ada lampiran
											@else
											<a target="_blank" href="{{Config::get('constants.path.uploads')}}/invoicedirect/{{$invoicedirect->invoice_attachment}}"><i class="fa fa-download"></i>&nbsp;Download</a>
											@endif
										</td> 
										<td data-th="Attachment">
											@if($invoicedirect->invoice_receip == null)
											Tidak ada lampiran
											@else
											<a target="_blank" href="{{Config::get('constants.path.uploads')}}/invoicedirect/{{$invoicedirect->invoice_receip}}"><i class="fa fa-download"></i>&nbsp;Download</a>
											@endif
										</td> 
										<td data-th="Branch">{{ $invoicedirect->branch->branch_name }}</td> 
										<td data-th="Status"><span class="">
											@if($invoicedirect->approved==0)
											Request
											@elseif($invoicedirect->approved==1 & $invoicedirect->paid=='')
											Approved
											@elseif($invoicedirect->paid==1)
											<span class="label label-success"><i class="fa fa-money"></i>&nbsp;
											Paid
											</span>
											@endif
											<span>
											</td>  
										<td align="center">
											@if($invoicedirect->approved==0)
											
												<a href="{{ URL::route('editor.invoicedirect.edit', [$invoicedirect->id]) }}" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i></a>
											
											@else
											
												<a href="{{ URL::route('editor.invoicedirect.view', [$invoicedirect->id]) }}" class="btn btn-default btn-sm"><i class="fa fa-search"></i></a>
											
											@endif 
										</td>
									</tr>
									@endforeach
									</tbody>
								</table>
							</div>
							{{ $invoicedirects->links() }}
						</div>
					</div>
				</div>
			</div>
		</section>
	</section>
	@stop

	@section('scripts')
	<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
	<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
	<script>
		$(document).ready(function () {
			$("#invoicedirectTable").DataTable();
		});
	</script> 
	<script>
		$(".approved").on("submit", function(){
			return confirm("Do you want to issued this invoice?");
		});

		$(".paid").on("submit", function(){
			return confirm("Do you want to paid this invoice?");
		});
	</script>
	@stop