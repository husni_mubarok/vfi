<style type="text/css">
	.modal {
		text-align: center;
		padding: 0!important;
	}

	.modal:before {
		content: '';
		display: inline-block;
		height: 100%;
		vertical-align: middle;
		margin-right: -4px;
	}

	.modal-dialog {
		display: inline-block;
		text-align: left;
		vertical-align: middle;
	}
</style>

@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><a href="{{ URL::route('editor.invoicedirect.index') }}"><i class="fa fa-file-text-o"></i> Invoice Consummable</a></li>
	</ol>
</section>
<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-1"></div>
				<div class="col-md-5">
					<div class="x_panel">
						<h2>
							@if(isset($invoicedirect))
							<i class="fa fa-pencil"></i>
							@else
							<i class="fa fa-plus"></i> 
							@endif
							&nbsp;Invoice Consummable
						</h2>
						<hr>
						<div class="x_content">
							@include('errors.error')
							@if(isset($invoicedirect))
							{!! Form::model($invoicedirect, array('route' => ['editor.invoicedirect.update', $invoicedirect->id], 'method' => 'PUT', 'class'=>'update', 'files' => 'true', 'id'=>'form_invoicedirect'))!!}
							@else
							{!! Form::open(array('route' => 'editor.invoicedirect.store', 'class'=>'create', 'files' => 'true', 'id'=>'form_invoicedirect'))!!}
							@endif
							{{ csrf_field() }}
							<div class="col-md-12 col-sm-12 col-xs-12 form-group"> 
								{{ Form::label('Invoice Type') }}
								{{ Form::select('invoice_type_id', $invoice_type_list, old('invoice_type_id'), array('class' => 'form-control', 'required' => 'true', 'id' => 'invoice_type_id', 'placeholder' => 'Select Invoice Type')) }}<br/>

								{{ Form::label('invoice_date', 'Invoice Date') }}
								{{ Form::text('invoice_date', old('invoice_date'), array('class' => 'form-control', 'placeholder' => 'Invoice Date*', 'required' => 'true', 'id' => 'invoice_date')) }}<br/>

								{{ Form::label('Vendor') }}
								@if(isset($invoicedirect))
								{{ Form::select('vendor_id', $vendor_list, old('vendor_id'), array('class' => 'form-control', 'required' => 'true', 'placeholder' => 'Select Vendor', 'id' => 'vendor_id')) }}
								@else
								{{ Form::select('vendor_id', $vendor_list, old('vendor_id'), array('class' => 'form-control', 'required' => 'true', 'placeholder' => 'Select Vendor', 'id' => 'vendor_id', 'disabled' => 'disabled')) }}
								@endIf
								<br/>

								{{ Form::label('invoice_bank', 'Bank') }}
								{{ Form::text('invoice_bank', old('invoice_bank'), array('class' => 'form-control', 'placeholder' => 'Bank*', 'required' => 'true', 'id' => 'invoice_bank')) }}<br/>

								{{ Form::label('invoice_rekening', 'Rek No') }}
								{{ Form::text('invoice_rekening', old('invoice_rekening'), array('class' => 'form-control', 'placeholder' => 'Rek No*', 'required' => 'true', 'id' => 'invoice_rekening')) }}<br/>

								{{ Form::label('additional_cost', 'Additional Cost') }}  
								@if(isset($invoicedirect)) 
								{{ Form::text('invoice_total_show',number_format($invoicedirect->additional_cost,0), array('class' => 'form-control', 'placeholder' => 'Additional Cost*', 'required' => 'true', 'id' => 'additional_cost_show', 'oninput' => 'cal_sparator();')) }}<br/>
								@else
								{{ Form::text('additional_cost_show',old('additional_cost_show'), array('class' => 'form-control', 'placeholder' => 'Additional Cost*', 'required' => 'true', 'id' => 'additional_cost_show', 'oninput' => 'cal_sparator();')) }}<br/>
								@endif 
								{{ Form::hidden('additional_cost', old('additional_cost'), array('id' => 'additional_cost')) }}

								{{ Form::label('add_cost_desc', 'Additional Cost Notes') }}
								{{ Form::text('add_cost_desc', old('add_cost_desc'), array('class' => 'form-control', 'placeholder' => 'Additional Cost Notes*', 'required' => 'true', 'id' => 'invoice_rekening')) }}<br/>

								{{ Form::label('invoice_total', 'Total') }}
								{{ Form::number('invoice_total', old('invoice_total'), array('class' => 'form-control', 'placeholder' => 'Total*', 'disabled' => 'disabled')) }}<br/>


								{{ Form::label('comment', 'Comment') }}
								{{ Form::text('comment', old('comment'), array('class' => 'form-control', 'placeholder' => 'Comment*', 'required' => 'true')) }}<br/>

								{{ Form::label('image', 'Invoice Document') }}
								{{ Form::file('image'), array('id' => 'image') }}<br/>

								<hr>

								{{ Form::label('item_id', 'Item') }}
								{{ Form::select('item_id', $item_list, old('item_id'), array('class' => 'form-control', 'required' => 'true', 'id' => 'item_id', 'placeholder' => 'Select Item')) }}<br/>

								{{ Form::label('uom', 'UOM') }}
								{{ Form::text('uom', old('uom'), array('class' => 'form-control', 'placeholder' => 'UOM*', 'required' => 'uom', 'id' => 'uom')) }}<br/>

								{{ Form::label('quantity', 'Quantity') }}
								{{ Form::number('quantity', old('quantity'), array('class' => 'form-control', 'placeholder' => 'Quantity*', 'required' => 'quantity', 'id' => 'quantity', 'oninput' => 'caltotal();')) }}<br/> 

								{{ Form::label('price', 'Price') }} 
								{{ Form::text('price_show',old('price_show'), array('class' => 'form-control', 'placeholder' => 'Price*', 'required' => 'true', 'id' => 'price_show', 'oninput' => 'cal_sparator(); caltotal();')) }}<br/> 
								{{ Form::hidden('price', old('price'), array('id' => 'price')) }}

								{{ Form::label('total', 'Total') }} 
								{{ Form::text('total_show',old('total_show'), array('class' => 'form-control', 'placeholder' => 'Total*', 'required' => 'true', 'id' => 'total_show', 'oninput' => 'cal_sparator(); caltotal();')) }}<br/> 
								{{ Form::hidden('total', old('total'), array('id' => 'total')) }} 

								<button type="button" class="btn btn-sucess pull-right" id="btn_add_detail"><i class="fa fa-cart-plus"></i>&nbsp;Add</button> <br> 

								<hr>
								<div class="div_overflow">
									<table id="itemTable" class="table table-striped dataTable">
										<thead>
											<tr> 
												<th>Item</th>
												<th>UOM</th>
												<th>Qty</th>
												<th>Price</th>
												<th>Total</th> 
												<th>Action</th>
											</tr>
										</thead>
										<tbody id="cart_item">
										</tbody>
									</table>
								</div>
								<br>
								<button type="button" data-toggle="modal" class="btn btn-success pull-right" onclick="validate();" id="btn_save"><i class="fa fa-check"></i> Save</button>
								<a href="{{ URL::route('editor.invoicedirect.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
							</div>
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>
@stop

@section('modal')
<div class="modal fade" id="modal_invoicedirect">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Save this invoice?</h4>
			</div> 
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
				<button type="button" id="btn_submit" class="btn btn-success"><i class="fa fa-check"></i> OK</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_invoicedirect_warning">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header modal-danger">
				<h4 class="modal-title">Some data is required!</h4>
			</div> 
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Close</button> 
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')
<script>
	function numberWithCommas(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}

	function cal_sparator() {
		var budget_request_show = document.getElementById('budget_request_show').value;
		var result = document.getElementById('budget_request');
		var rsbudgetrequest = (budget_request_show);
		result.value = rsbudgetrequest.replace(/,/g, ""); 
	}

	window.onload= function(){ 
		
		n2= document.getElementById('budget_request_show');

		n2.onkeyup=n2.onchange= function(e){
			e=e|| window.event; 
			var who=e.target || e.srcElement,temp;
			if(who.id==='budget_request')  temp= validDigits(who.value,0); 
			else temp= validDigits(who.value);
			who.value= addCommas(temp);
		}   
		n2.onblur= function(){
			var 
			temp2=parseFloat(validDigits(n2.value));
			if(temp2)n2.value=addCommas(temp2.toFixed(0));
		}

	}

	
	function caltotal(){  
		var quantity = document.getElementById('quantity').value;
		var price = document.getElementById('price').value;
		document.getElementById('total').value = parseFloat(quantity) * parseFloat(price);  
		document.getElementById('total_show').value = numberWithCommas(parseFloat(quantity) * parseFloat(price));
	} 

	$("#item_id").on('click', function()
	{
		$.ajax({
			url : '{{ URL::route('get.item') }}',
			data : {'item_id':$("#item_id").val()},
			type : 'GET',
			headers : {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
			success : function(data, textStatus, jqXHR){
        	console.log(data);

        	$('#uom').empty();
        	$('#price').empty();
        	jQuery.each(data, function(i, val)
        	{
        		document.getElementById ("uom").value = data[0].uom;
        		document.getElementById ("price").value = data[0].price;
        		document.getElementById ("price_show").value = numberWithCommas(data[0].price);
        	}); 
        },
        error : function()
        {
        	$('#uom').empty(); 
        	$('#price').empty(); 
        },
    })
	});

	$("#item_id").on('change', function()
	{ 	 
		document.getElementById ("quantity").value = "";
		document.getElementById ("total").value = ""; 
	});
</script> 


<script>
	document.getElementById("btn_save").disabled = true;
	var cart_item= JSON.parse("{}"); 
</script>
@if(isset($invoice_detail))
<script>
	document.getElementById("btn_save").disabled = false;
	var cart_item = JSON.parse("{}");
	var cart_item_t = JSON.parse("{}");
// ADD BUTTON DETAIL
$("#btn_add_detail").on('click', function() { 

	
	var inpuom = $("#uom");
	var inpquantity = $("#quantity");
	var inpprice = $("#price");
	var inptotal = $("#total");

	if (inpuom.val().length < 1 || inpquantity.val().length < 1 || inpprice.val().length < 1 || inptotal.val().length < 1) {
		alert("Some data can't empty!");
	}else{

	document.getElementById("btn_save").disabled = false;

	var quantity = $("#quantity").val();
	var uom = $("#uom").val();
	var price = $("#price").val(); 
	var total = $("#total").val();

	var item_id = $("#item_id option:selected").val(); 

	console.log(item_id);


	var key = item_id+"_"+"_"+uom;

	if(cart_item[key] == undefined)
	{
		cart_item[key] = parseFloat(quantity);
		cart_item_t[key] = parseFloat(total);
		var new_row = '';
		new_row += '<tr>';
		new_row += '<td>';
		new_row += '<input type="hidden" name="item_id['+key+']" value="'+item_id+'">';
		new_row += $("#item_id option:selected").text();
		new_row += '</td>';

		new_row += '<td>';
		new_row += '<input type="hidden" name="uom['+key+']" value="'+uom+'">';
		new_row += uom;
		new_row += '</td>'; 

		new_row += '<td>'; 
		new_row += '<input type="hidden" name="quantity['+key+']" value="'+cart_item[key]+'" id="quantity_'+key+'">';
		new_row += '<div id="txtquantity_'+key+'">'+quantity+'</div>';
		
		new_row += '</td>'; 

		new_row += '<td>';
		new_row += '<input type="hidden" name="price['+key+']" value="'+price+'">';
		new_row += price;
		new_row += '</td>'; 

		new_row += '<td>'; 
		new_row += '<input type="hidden" name="total['+key+']" value="'+cart_item_t[key]+'" id="total_'+key+'">';
		new_row += '<div id="txttotal_'+key+'">'+total+'</div>';

		new_row += '<td>'; 
		new_row += '<button type="button" class="btn btn-danger btn-sm" id="btn_remove_'+key+'">';
		new_row += '<i class="fa fa-remove"></i>';
		new_row += '</button>';
		new_row += '</td>';
		new_row += '</tr>';

		$("#cart_item").append(new_row); 

		$("#btn_remove_"+key).on('click', function() 
		{
			cart_item[key] == null;
			$(this).parent().parent().remove();
		});
	} 
	else 
	{
		//alert("sadd");
		//console.log(cart_item[key]);
		cart_item[key] += parseInt(quantity);
		$("#quantity_"+key).val(cart_item[key]);
		$("#txtquantity_"+key).text(cart_item[key]);

		cart_item_t[key] += parseInt(total);
		$("#total_"+key).val(cart_item_t[key]);
		$("#txttotal_"+key).text(cart_item_t[key]);
	}
	document.getElementById ("uom").value = "";
	document.getElementById ("quantity").value = "";
	document.getElementById ("total").value = "";
	document.getElementById ("price").value = "";
	document.getElementById ("price_show").value = "";
	document.getElementById ("total_show").value = "";
}

});

jQuery.each({!! $invoice_detail !!}, function( i, val ) {  
	var quantity = val['quantity']; 
	var uom = val['uom']; 
	var price = val['price'];  
	var total = val['total']; 

	var item_id = val['item_id'];


	var key = item_id+"_"+quantity+"_"+uom;
	$("#item_id").val(item_id);
	// console.log(key);

	//cart_item[key] = item_id;
	var new_row = '';
	new_row += '<tr>';
	new_row += '<td>';
	new_row += '<input type="hidden" name="item_id['+key+']" value="'+item_id+'">';
	new_row += $("#item_id option:selected").text();
	new_row += '</td>';

	new_row += '<td>';
	new_row += '<input type="hidden" name="uom['+key+']" value="'+uom+'">';
	new_row += uom;
	new_row += '</td>';

	new_row += '<td>';
	new_row += '<input type="hidden" name="quantity['+key+']" value="'+quantity+'">';
	new_row += quantity;
	new_row += '</td>';

	new_row += '<td>';
	new_row += '<input type="hidden" name="price['+key+']" value="'+price+'">';
	new_row += price;
	new_row += '</td>'; 

	new_row += '<td>';
	new_row += '<input type="hidden" name="total['+key+']" value="'+total+'">';
	new_row += total;
	new_row += '</td>';

	new_row += '<td>'; 
	new_row += '<button type="button" class="btn btn-danger btn-sm" id="btn_remove_'+key+'">';
	new_row += '<i class="fa fa-remove"></i>';
	new_row += '</button>';
	new_row += '</td>';
	new_row += '</tr>';

	$("#cart_item").append(new_row);

	$("#btn_remove_"+key).on('click', function() 
	{
		cart_item[key] == null;
		$(this).parent().parent().remove();
	});
});

$('#btn_submit').on('click', function()
{
	console.log("sdsad");
	$('#form_invoicedirect').submit();
});

function validate(){
	//var inp = document.getElementById('image');
	var invoice_date = document.getElementById('invoice_date');
	var vendor_id = document.getElementById('vendor_id');
	var invoice_bank = document.getElementById('invoice_bank');
	var invoice_rekening = document.getElementById('invoice_rekening');
	var invoice_total = document.getElementById('invoice_total');

	if(invoice_date.value == '' || vendor_id.value == '' || invoice_bank.value == '' || invoice_rekening.value == ''){
		$('#modal_invoicedirect_warning').modal('toggle');
		$('#modal_invoicedirect_warning').modal('show');
		//inp.focus();
		return false;
	}else{
		$('#modal_invoicedirect').modal('toggle');
		$('#modal_invoicedirect').modal('show');
	}
}
</script>
@endif 


<script>
	$("#invoice_type_id").on('change', function()
	{
		//console.log("sada");
		$.ajax({
			url : '{{ URL::route('get.vendorinv') }}',
			data : {'invoice_type_id':$("#invoice_type_id").val()},
			type : 'GET',
			headers : {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
			success : function(data, textStatus, jqXHR){
				console.log(data);
				$('#vendor_id').empty();
				jQuery.each(data, function(i, val)
				{
					$('#vendor_id').append($('<option>', { 
						value: val.id,
						text : val.vendor_name 
					}));
				});
				$("#vendor_id").attr('disabled', false);
				$('#invoice_bank').empty(); 
				$('#invoice_rekening').empty();
			},
			error : function()
			{
				$('#vendor_id').empty();
				$('#vendor_id').attr('disabled', true);
			},
		})
	});


	$("#vendor_id").on('click', function()
	{
		$.ajax({
			url : '{{ URL::route('get.vendor') }}',
			data : {'vendor_id':$("#vendor_id").val()},
			type : 'GET',
			headers : {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
			success : function(data, textStatus, jqXHR){
				//console.log(data[0].vendor_bank);

				$('#invoice_bank').empty();
				$('#invoice_rekening').empty();
				jQuery.each(data, function(i, val)
				{
					document.getElementById ("invoice_bank").value = data[0].vendor_bank;
					document.getElementById ("invoice_rekening").value = data[0].vendor_rekening;
				}); 
			},
			error : function()
			{
				$('#invoice_bank').empty(); 
				$('#invoice_rekening').empty(); 
			},
		})
	});


	$("#vendor_id").on('click', function()
	{
		//console.log($("#vendor_id").val());
		$.ajax({
			url : '{{ URL::route('get.iteminv') }}',
			data : {'vendor_id':$("#vendor_id").val()},
			type : 'GET',
			headers : {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
			success : function(data, textStatus, jqXHR){
				//console.log(data);
				$('#item_id').empty();
				jQuery.each(data, function(i, val)

				{
					//console.log(id);
					$('#item_id').append($('<option>', { 
						value: val.id,
						text : val.item_name, 
					}));
				});
				$("#item_id").attr('disabled', false); 
			},
			error : function()
			{
				$('#item_id').empty();
				$('#item_id').attr('disabled', true);
			},
		})
	});

	//Sparatior
	function cal_sparator() {
		var price_show = document.getElementById('price_show').value;
		var result = document.getElementById('price');
		var rsprice = (price_show);
		result.value = rsprice.replace(/,/g, ""); 

		var additional_cost_show = document.getElementById('additional_cost_show').value;
		var result = document.getElementById('additional_cost');
		var rsadditional_cost = (additional_cost_show);
		result.value = rsadditional_cost.replace(/,/g, ""); 
	}

	window.onload= function(){ 
		
		n2= document.getElementById('price_show');

		n2.onkeyup=n2.onchange= function(e){
			e=e|| window.event; 
			var who=e.target || e.srcElement,temp;
			if(who.id==='price')  temp= validDigits(who.value,0); 
			else temp= validDigits(who.value);
			who.value= addCommas(temp);
		}   
		n2.onblur= function(){
			var 
			temp2=parseFloat(validDigits(n2.value));
			if(temp2)n2.value=addCommas(temp2.toFixed(0));
		}

		n3= document.getElementById('additional_cost_show');

		n3.onkeyup=n3.onchange= function(e){
			e=e|| window.event; 
			var who=e.target || e.srcElement,temp;
			if(who.id==='additional_cost')  temp= validDigits(who.value,0); 
			else temp= validDigits(who.value);
			who.value= addCommas(temp);
		}   
		n3.onblur= function(){
			var 
			temp3=parseFloat(validDigits(n3.value));
			if(temp3)n3.value=addCommas(temp3.toFixed(0));
		}


	}
</script>
@stop