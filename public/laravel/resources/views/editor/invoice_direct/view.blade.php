@extends('layouts.editor.template')
@section('content')

<style type="text/css">
	.modal {
		text-align: center;
		padding: 0!important;
	}

	.modal:before {
		content: '';
		display: inline-block;
		height: 100%;
		vertical-align: middle;
		margin-right: -4px;
	}

	.modal-dialog {
		display: inline-block;
		text-align: left;
		vertical-align: middle;
	}
</style>
<!-- Content Header (Page header) -->
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><a href="{{ URL::route('editor.invoicedirect.index') }}"><i class="fa fa-file-text-o"></i> Invoice Consummable</a></li>
	</ol>
</section>
<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-1"></div>
				<div class="col-md-5">
					<div class="x_panel">
						<h2>
							@if(isset($invoicedirect))
							<i class="fa fa-pencil"></i>
							@else
							<i class="fa fa-plus"></i> 
							@endif
							&nbsp;Invoice Consummable
						</h2>
						<hr>
						<div class="x_content">
							@include('errors.error') 
							{!! Form::model($invoicedirect, array('route' => ['editor.invoicedirect.updaterequset', $invoicedirect->id], 'method' => 'PUT', 'class'=>'update', 'files' => 'true'))!!}  
							{{ csrf_field() }}
							<div class="col-md-12 col-sm-12 col-xs-12 form-group"> 
								{{ Form::label('Invoice Type') }}
								{{ Form::select('invoice_type_id', $invoice_type_list, old('invoice_type_id'), array('class' => 'form-control', 'required' => 'true', 'disabled' => 'disabled')) }}<br/>

								{{ Form::label('invoice_date', 'Invoice Date') }}  
								<input type="text" class="form-control" name="invoice_date" value="{{date('d-M-Y', strtotime($invoicedirect->invoice_date))}}" disabled="disabled"><br/> 

								{{ Form::label('Vendor') }}
								{{ Form::select('vendor_id', $vendor_list, old('vendor_id'), array('class' => 'form-control', 'required' => 'true', 'id' => 'vendor_id', 'disabled' => 'disabled')) }}<br/>

								{{ Form::label('invoice_bank', 'Bank') }}
								{{ Form::text('invoice_bank', old('invoice_bank'), array('class' => 'form-control', 'placeholder' => 'Bank*', 'required' => 'true', 'id' => 'invoice_bank', 'disabled' => 'disabled')) }}<br/>

								{{ Form::label('invoice_rekening', 'Rek No') }}
								{{ Form::text('invoice_rekening', old('invoice_rekening'), array('class' => 'form-control', 'placeholder' => 'Rek No*', 'required' => 'true', 'id' => 'invoice_rekening', 'disabled' => 'disabled')) }}<br/>

								{{ Form::label('invoice_total', 'Total') }} 
								<input type="text" class="form-control" name="invoice_total" value="{{number_format($invoicedirect->invoice_total,0)}}" disabled="disabled"><br/>

								{{ Form::label('additional_cost', 'Additional Cost') }} 
								<input type="text" class="form-control" name="additional_cost" value="{{number_format($invoicedirect->additional_cost,0)}}" disabled="disabled"><br/> 

								{{ Form::label('add_cost_desc', 'Additional Cost Desc') }} 
								<input type="text" class="form-control" name="add_cost_desc" value="{{$invoicedirect->add_cost_desc,0}}" disabled="disabled"><br/>  

								{{ Form::label('paid_date', 'Payment Date') }}
								{{ Form::text('paid_date', old('paid_date'), array('class' => 'form-control', 'placeholder' => 'Payment Date*', 'required' => 'true', 'disabled' => 'disabled')) }}<br/>
								
								{{ Form::label('reference_no', 'Reference No') }}
								{{ Form::text('reference_no', old('reference_no'), array('class' => 'form-control', 'placeholder' => 'Reference No*', 'required' => 'true', 'id' => 'reference_no', 'disabled' => 'disabled')) }}<br/>

								{{ Form::label('comment', 'Comment') }}
								{{ Form::text('comment', old('comment'), array('class' => 'form-control', 'placeholder' => 'Comment*', 'required' => 'true', 'disabled' => 'disabled')) }}<br/>

								<hr>

								<table id="itemTable" class="table table-striped dataTable">
									<thead>
										<tr> 
											<th>Item</th>
											<th>UOM</th>
											<th>Qty</th>
											<th>Price</th>
											<th>Total</th>  
										</tr>
									</thead>
									<tbody>
										@foreach($invoice_detail as $key => $invoice_details)
										<tr>
											<td data-th="item_name">{{$invoice_details->item_name}}</td>
											<td data-th="uom">{{$invoice_details->uom}}</td>
											<td data-th="quantity">{{ number_format($invoice_details->quantity,0) }}</td> 
											<td data-th="price">{{ number_format($invoice_details->price,0) }}</td>  
											<td data-th="total">{{ number_format($invoice_details->total,0) }}</td>   
										</tr>
										@endforeach
									</tbody>
								</table>
								<hr>
								{!! Form::close() !!}
								@if($invoicedirect->approved==0)
								{!! Form::open(array('route' => ['editor.invoicedirect.updaterequset', $invoicedirect->id], 'method' => 'PUT', 'class'=>'approved', 'id'=>'form_invoicedirect'))!!} 
								{{ csrf_field() }}	                    				
								<button type="button" data-toggle="modal" data-target="#modal_invoicedirect" class="btn btn-success pull-right"><i class="fa fa-check"></i> Issued</button>
								{!! Form::close() !!}
								@elseif($invoicedirect->approved==1 & $invoicedirect->paid=='')

								{!! Form::open(array('route' => ['editor.invoicedirect.updatepaid', $invoicedirect->id], 'method' => 'PUT', 'class'=>'paid', 'id'=>'form_invoicedirect', 'files' => 'true'))!!} 
								{{ csrf_field() }}	 

								{{ Form::label('invoice_receip', 'Receipt Document') }}
								{{ Form::file('invoice_receip'), array('id' => 'invoice_receip') }}<br/>     	             				
								<button  type="button" data-toggle="modal" data-target="#modal_invoicedirect" class="btn btn-success pull-right"><i class="fa fa-check"></i> Paid</button>
								{!! Form::close() !!}

								@elseif($invoicedirect->paid==1)
								@endif
								<a href="{{ URL::route('editor.invoicedirect.bank') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>

							</div> 
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>
@stop
