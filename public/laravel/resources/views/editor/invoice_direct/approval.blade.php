@extends('layouts.editor.template')
@section('content')

<style type="text/css">
	.modal {
		text-align: center;
		padding: 0!important;
	}

	.modal:before {
		content: '';
		display: inline-block;
		height: 100%;
		vertical-align: middle;
		margin-right: -4px;
	}

	.modal-dialog {
		display: inline-block;
		text-align: left;
		vertical-align: middle;
	}
</style>
<!-- Content Header (Page header) -->
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><a href="{{ URL::route('editor.invoicedirect.index') }}"><i class="fa fa-file-text-o"></i> Invoice Consummable</a></li>
	</ol>
</section>
<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-1"></div>
				<div class="col-md-5">
					<div class="x_panel">
						<h2>
							@if(isset($invoicedirect))
							<i class="fa fa-pencil"></i>
							@else
							<i class="fa fa-plus"></i> 
							@endif
							&nbsp;Invoice Consummable
						</h2>
						<hr>
						<div class="x_content">
							@include('errors.error') 
							{!! Form::model($invoicedirect, array('route' => ['editor.invoicedirect.updaterequset', $invoicedirect->id], 'method' => 'PUT', 'class'=>'update', 'files' => 'true'))!!}  
							{{ csrf_field() }}
							<div class="col-md-12 col-sm-12 col-xs-12 form-group"> 
								{{ Form::label('Invoice Type') }}
								{{ Form::select('invoice_type_id', $invoice_type_list, old('invoice_type_id'), array('class' => 'form-control', 'required' => 'true', 'disabled' => 'disabled')) }}<br/>

								{{ Form::label('invoice_date', 'Invoice Date') }}  
								<input type="text" class="form-control" name="invoice_date" value="{{date('d-M-Y', strtotime($invoicedirect->invoice_date))}}" disabled="disabled"><br/> 

								{{ Form::label('Vendor') }}
								{{ Form::select('vendor_id', $vendor_list, old('vendor_id'), array('class' => 'form-control', 'required' => 'true', 'id' => 'vendor_id', 'disabled' => 'disabled')) }}<br/>

								{{ Form::label('invoice_bank', 'Bank') }}
								{{ Form::text('invoice_bank', old('invoice_bank'), array('class' => 'form-control', 'placeholder' => 'Bank*', 'required' => 'true', 'id' => 'invoice_bank', 'disabled' => 'disabled')) }}<br/>

								{{ Form::label('invoice_rekening', 'Rek No') }}
								{{ Form::text('invoice_rekening', old('invoice_rekening'), array('class' => 'form-control', 'placeholder' => 'Rek No*', 'required' => 'true', 'id' => 'invoice_rekening', 'disabled' => 'disabled')) }}<br/>

								{{ Form::label('invoice_total', 'Total') }}
								{{ Form::number('invoice_total', old('invoice_total'), array('class' => 'form-control', 'placeholder' => 'Total*', 'required' => 'true', 'disabled' => 'disabled')) }}<br/>  

								{{ Form::label('comment', 'Comment') }}
								{{ Form::text('comment', old('comment'), array('class' => 'form-control', 'placeholder' => 'Comment*', 'required' => 'true', 'disabled' => 'disabled')) }}<br/>

								<hr>

								<table id="itemTable" class="table table-striped dataTable">
									<thead>
										<tr> 
											<th>Item</th>
											<th>UOM</th>
											<th>Qty</th>
											<th>Price</th>
											<th>Total</th>  
										</tr>
									</thead>
									<tbody id="cart_item">
									</tbody>
								</table>
								<hr>
								{!! Form::close() !!}
								@if($invoicedirect->approved==0)
								{!! Form::open(array('route' => ['editor.invoicedirect.updaterequset', $invoicedirect->id], 'method' => 'PUT', 'class'=>'approved', 'id'=>'form_invoicedirect'))!!} 
								{{ csrf_field() }}	                    				
								<button type="button" data-toggle="modal" data-target="#modal_invoicedirect" class="btn btn-success pull-right"><i class="fa fa-check"></i> Issued</button>
								{!! Form::close() !!}
								@elseif($invoicedirect->approved==1 & $invoicedirect->paid=='')

								{!! Form::open(array('route' => ['editor.invoicedirect.updatepaid', $invoicedirect->id], 'method' => 'PUT', 'class'=>'paid', 'id'=>'form_invoicedirect', 'files' => 'true'))!!} 
								{{ csrf_field() }}	 

								@if($invoicedirect->approved==1)

								{{ Form::label('paid_date', 'Payment Date') }}
								{{ Form::text('paid_date', old('paid_date'), array('class' => 'form-control', 'placeholder' => 'Payment Date*', 'required' => 'true')) }}<br/>

								{{ Form::label('reference_no', 'Reference No') }}
								{{ Form::text('reference_no', old('reference_no'), array('class' => 'form-control', 'placeholder' => 'Reference No*', 'required' => 'true', 'id' => 'reference_no')) }}<br/>
								@endif

								 {{ Form::label('invoice_receip', 'Receipt Document') }}
								{{ Form::file('invoice_receip'), array('id' => 'invoice_receip') }}<br/>     	             				
								<button  type="button" data-toggle="modal" data-target="#modal_invoicedirect" class="btn btn-success pull-right"><i class="fa fa-check"></i> Paid</button>
								{!! Form::close() !!}

								@elseif($invoicedirect->paid==1)
								@endif
								<a href="{{ URL::route('editor.invoicedirect.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>

							</div> 
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>
@stop

@section('modal')
<div class="modal fade" id="modal_invoicedirect">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Submit this invoice?</h4>
			</div> 
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
				<button type="button" id="btn_submit" class="btn btn-success"><i class="fa fa-check"></i> OK</button>
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')
<script>
	$("#vendor_id").on('change', function()
	{
		$.ajax({
			url : '{{ URL::route('get.vendor') }}',
			data : {'vendor_id':$("#vendor_id").val()},
			type : 'GET',
			headers : {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
			success : function(data, textStatus, jqXHR){
				console.log(data[0].vendor_bank);

				$('#invoice_bank').empty();
				$('#invoice_rekening').empty();
				jQuery.each(data, function(i, val)
				{
					document.getElementById ("invoice_bank").value = data[0].vendor_bank;
					document.getElementById ("invoice_rekening").value = data[0].vendor_rekening;
				}); 
			},
			error : function()
			{
				$('#invoice_bank').empty(); 
				$('#invoice_rekening').empty(); 
			},
		})
	});

	function caltotal(){  
		var quantity = document.getElementById('quantity').value;
		var price = document.getElementById('price').value;
		document.getElementById('total').value = parseInt(quantity) * parseInt(price);  
	}

	$("#item_id").on('change', function()
	{
		$.ajax({
			url : '{{ URL::route('get.item') }}',
			data : {'item_id':$("#item_id").val()},
			type : 'GET',
			headers : {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
			success : function(data, textStatus, jqXHR){
        	// console.log(data[0].vendor_bank);

        	$('#uom').empty();
        	$('#price').empty();
        	jQuery.each(data, function(i, val)
        	{
        		document.getElementById ("uom").value = data[0].uom;
        		document.getElementById ("price").value = data[0].price;
        	}); 
        },
        error : function()
        {
        	$('#uom').empty(); 
        	$('#price').empty(); 
        },
    })
	});
</script> 


<script>
	var cart_item= JSON.parse("{}"); 
</script>
@if(isset($invoice_detail))
<script>

	var cart_item = JSON.parse("{}");
// ADD BUTTON DETAIL

jQuery.each({!! $invoice_detail !!}, function( i, val ) {  
	var quantity = val['quantity']; 
	var uom = val['uom']; 
	var price = val['price'];  
	var total = val['total']; 
	var item_name = val['item_name'];
	
	var key = quantity+"_"+uom+"_"+price+"_"+total;

	console.log(key);

	cart_item[key] = item_name;
	var new_row = '';
	new_row += '<tr>';

	new_row += '<td>';
	new_row += '<input type="hidden" name="item_name['+key+']" value="'+item_name+'">';
	new_row += item_name;
	new_row += '</td>';

	new_row += '<td>';
	new_row += '<input type="hidden" name="uom['+key+']" value="'+uom+'">';
	new_row += uom;
	new_row += '</td>';

	new_row += '<td>';
	new_row += '<input type="hidden" name="quantity['+key+']" value="'+quantity+'">';
	new_row += quantity;
	new_row += '</td>';

	new_row += '<td>';
	new_row += '<input type="hidden" name="price['+key+']" value="'+price+'">';
	new_row += price;
	new_row += '</td>'; 

	new_row += '<td>';
	new_row += '<input type="hidden" name="total['+key+']" value="'+total+'">';
	new_row += total;
	new_row += '</td>';
	new_row += '</tr>';

	$("#cart_item").append(new_row);

	$("#btn_remove_"+key).on('click', function() 
	{
		cart_item[key] == null;
		$(this).parent().parent().remove();
	});
});

$('#btn_submit').on('click', function()
{
	console.log("sdsad");
	$('#form_invoicedirect').submit();
});
</script>
@endif 
@stop