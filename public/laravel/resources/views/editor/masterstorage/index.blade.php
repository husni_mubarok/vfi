@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li class="active"><a href="#"><i class="fa fa-bar-chart"></i>  Item</a></li>
  </ol>
</section>
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-10">
			        <div class="x_panel">
		                <h2>
		                	<i class="fa fa-bar-chart"></i>  Item
		                	<a href="{{ URL::route('editor.item.create') }}" class="btn btn-default btn-lg pull-right"><i class="fa fa-plus"></i> Add</a>
	                	</h2>
		                <hr>
			            <div class="x_content">
			                <table id="itemTable" class="table table-striped dataTable">
							  	<thead>
							  	  	<tr>
								      	<th>Category</th>
								      	<th>Type</th>
								      	<th>Master Item</th>
								      	<th>Item</th>
								      	<th>Dsc</th>
								      	<th>Detail</th>
								      	<th>Edit</th>
								      	<th>Delete</th>
							    	</tr>
							  	</thead>
							</table>
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>

<script>

$(function() {
        $('#itemTable').DataTable({

      processing: true,
      serverSide: true,
      "pageLength": 25,
      "scrollY": "400px",
      ajax: "{{ url('editor/item/data') }}",
      columns: [
      { data: 'categoryitem', name: 'categoryitem' },
      { data: 'typeitem', name: 'typeitem' },
      { data: 'masteritem', name: 'masteritem' },
      { data: 'item', name: 'item' },
      { data: 'size', name: 'size' },
      { data: 'detail', name: 'detail' },
      { data: 'edit', name: 'edit' },
      { data: 'delete', name: 'delete' }
      ]
    });
  });
</script>
@stop
