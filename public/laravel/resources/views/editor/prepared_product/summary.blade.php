@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li><a href="{{ URL::route('editor.prepared_product.index') }}"><i class="fa fa-cube"></i> Prepared Product</a></li>
    <li class="active"><a href="#"><i class="fa fa-file-text-o"></i> Summary</a></li>
  </ol>
</section>
@actionStart('prepared_product', 'read')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-10">
			        <div class="x_panel">
		                <h2>
		                	<i class="fa fa-file-text-o"></i> 
		                	{{$prepared_product->m_prep_product->name}}
		                	</div>
	                	</h2>
		                <hr>
			            <div class="x_content">
			            	<div class="col-md-6">
			            		<table class="table table-bordered">
			            			<tr>
			            				<th>Name</th>
			            				<td>{{$prepared_product->m_prep_product->name}}</td>

			            				<th>Date</th>
			            				<td>
                            {{date("d M Y", strtotime($prepared_product->start_date))}}
                            &nbsp;<i class="fa fa-arrow-right"></i>&nbsp;
                            {{date("d M Y", strtotime($prepared_product->finish_date))}}
                          </td>
			            			</tr>
			            			<tr>
			            				<th>Material</th>
			            				<td>{{$prepared_product->production->name}}</td>

			            				<th>Work Days</th>
			            				<td>{{$prepared_product->work_days}}</td>
			            			</tr>
                        <tr>
                          <th>Average Size</th>
                          <td>{{$prepared_product->avg_size}}</td>

                          <th>Daily Target</th>
                          <td>{{$prepared_product->daily_target}}</td>
                        </tr>

                        <tr>
                          <th>Predust</th>
                          <td>{{$prepared_product->flavor}}</td>

                          <th></th>
                          <td></td>
                        </tr>
			            			
			            		</table>
			            		<hr>
			            		<h3>Production Estimation</h3>
			            		{{-- <table class="table table-bordered">
			            			<tr>
			            				<th colspan="2">{{$prepared_product->production_result_prepared_product->production_result->production->fish->name}}</th>
			            				<td>{{$prepared_product->production_result_prepared_product->production_result->status}}</td>
			            			</tr>
			            			<tr>
			            				<td>{{$prepared_product->production_result_prepared_product->production_result->production->name}}</td>
			            				<td>{{number_format($prepared_product->production_result_prepared_product->production_result->weight, 2)}} kg</td>
			            				<td>{{number_format($prepared_product->production_result_prepared_product->production_result->amount)}} pcs</td>
			            			</tr>
			            			<tr>
			            				<td align="right"><small>Result</small></td>
			            				<td>{{number_format($prepared_product->production_result_prepared_product->production_result->production_result_kg, 2)}} kg</td>
			            				<td>{{number_format($prepared_product->production_result_prepared_product->production_result->production_result_pack)}} pcs</td>
			            			</tr>
			            			<tr>
			            				<td></td>
			            				<td></td>
			            				<td>
			            					<ul>
			            						@foreach($prepared_product->production_result_prepared_product->production_result->production_result_size as $production_result_size)
			            						<li>{{$production_result_size->size->min_value}}~{{$production_result_size->size->max_value}}</li>
			            						@endforeach
			            					</ul>
			            				</td>
			            			</tr>
			            		</table> --}}
			            		<hr>
			            		<h3>Ingredient</h3>
			            		{{-- <table class="table table-bordered">
			            			@foreach($prepared_product->prepared_product_ingredient as $prepared_product_ingredient)
			            			<tr>
			            				<th>{{$prepared_product_ingredient->ingredient->name}} ({{$prepared_product_ingredient->ingredient->description}})</th>
			            				<td>{{$prepared_product_ingredient->value}} {{$prepared_product_ingredient->ingredient->uom}}</td>
			            			</tr>
			            			@endforeach
			            		</table> --}}
			            	</div>
			            	<div class="col-md-6">
			            		
			            	</div>
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@actionEnd
@stop
{{-- 
@section('modal')
<div class="modal fade" id="reject_history" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="myModalLabel">Reject History</h4>
      		</div>
      		<table class="table">
      		<tr>
      			<th>Date</th>
      			<th>Rejected at</th>
      			<th>Reason</th>
      		</tr>
      		@foreach($purchase->purchase_reject as $purchase_reject)
      		<tr>
      			<th>{{date("D, d-m-Y", strtotime($purchase_reject->reject_date))}}</th>
      			<th>{{$purchase_reject->reject_step}}</th>
      			<th>{{$purchase_reject->reason}}</th>
      		</tr>
      		@endforeach
      		</table>
      		<div class="modal-footer">
      			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i>&nbsp;Close</button>
      		</div>
    	</div>
  	</div>
</div>
<div class="modal fade" id="supplier_detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="myModalLabel">Supplier Detail</h4>
      		</div>
      		<table class="table">
	      		<tr>
	      			<th>Name</th>
	      			<td>{{$purchase->purchase_supplier->supplier->name}}</td>
	      		</tr>
	      		<tr>
	      			<th>Address</th>
	      			<td>{{$purchase->purchase_supplier->supplier->address}}</td>
	      		</tr>
	      		<tr>
	      			<th>E-mail Address</th>
	      			<td>{{$purchase->purchase_supplier->supplier->email}}</td>
	      		</tr>
	      		<tr>
	      			<th>Phone Number</th>
	      			<td>{{$purchase->purchase_supplier->supplier->phone_number}}</td>
	      		</tr>
	      		<tr>
	      			<th>Fax</th>
	      			<td>{{$purchase->purchase_supplier->supplier->fax}}</td>
	      		</tr>
	      		<tr>
	      			<th>Website</th>
	      			<td>{{$purchase->purchase_supplier->supplier->website}}</td>
	      		</tr>
	      		<tr>
	      			<th>PIC</th>
	      			<td>{{$purchase->purchase_supplier->supplier->pic}}</td>
	      		</tr>
	      		<tr>
	      			<th>Bank Account</th>
	      			<td>{{$purchase->purchase_supplier->supplier->bank_account}}</td>
	      		</tr>
	      		<tr>
	      			<th>Note</th>
	      			<td>{{$purchase->purchase_supplier->supplier->note}}</td>
	      		</tr>
      		</table>
      		<div class="modal-footer">
      			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i>&nbsp;Close</button>
      		</div>
    	</div>
  	</div>
</div>
<div class="modal fade" id="term_condition" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="myModalLabel">Terms of Condition</h4>
      		</div>
      		<div class="modal-body">
      			{!! $purchase->term_condition !!}
      		</div>
      		<div class="modal-footer">
      			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i>&nbsp;Close</button>
      		</div>
    	</div>
  	</div>
</div>
<div class="modal fade" id="term_payment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="myModalLabel">Terms of Payment</h4>
      		</div>
      		<div class="modal-body">
      			{!! $purchase->term_payment !!}
      		</div>
      		<div class="modal-footer">
      			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i>&nbsp;Close</button>
      		</div>
    	</div>
  	</div>
</div>
<div class="modal fade" id="reject_po_rm_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="myModalLabel">Reject Form</h4>
      		</div>
      		{!! Form::open(array('route' => ['editor.purchase.reject_po_rm', $purchase->id]))!!}
      		<div class="modal-body">
      			{{ Form::label('reason', 'Reason') }}
                {{ Form::text('reason', old('reason'), array('class' => 'form-control', 'placeholder' => 'Reason', 'required' => 'true')) }}<br/>
      		</div>
      		<div class="modal-footer">
      			<button type="submit" class="btn btn-success" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
      		</div>
      		{!! Form::close() !!}
    	</div>
  	</div>
</div>
<div class="modal fade" id="approve_po_rm_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="myModalLabel">Approval Form</h4>
      		</div>
      		{!! Form::open(array('route' => ['editor.purchase.approve_po_rm', $purchase->id]))!!}
      		<div class="modal-body">
      			Approve PO RM?
      			<br/>
      		</div>
      		<div class="modal-footer">
      			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i>&nbsp;No</button>
      			<button type="submit" class="btn btn-primary" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> &nbsp;Yes</button>
      		</div>
      		{!! Form::close() !!}
    	</div>
  	</div>
</div>
<div class="modal fade" id="reject_actual_delivery_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="myModalLabel">Reject Form</h4>
      		</div>
      		{!! Form::open(array('route' => ['editor.purchase.reject_actual_delivery', $purchase->id]))!!}
      		<div class="modal-body">
      			{{ Form::label('reason', 'Reason') }}
                {{ Form::text('reason', old('reason'), array('class' => 'form-control', 'placeholder' => 'Reason', 'required' => 'true')) }}<br/>
      		</div>
      		<div class="modal-footer">
      			<button type="submit" class="btn btn-success" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
      		</div>
      		{!! Form::close() !!}
    	</div>
  	</div>
</div>
<div class="modal fade" id="approve_actual_delivery_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="myModalLabel">Approval Form</h4>
      		</div>
      		{!! Form::open(array('route' => ['editor.purchase.approve_actual_delivery', $purchase->id]))!!}
      		<div class="modal-body">
      			Approve Actual Delivery?
      			<br/>
      		</div>
      		<div class="modal-footer">
      			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i>&nbsp;No</button>
      			<button type="submit" class="btn btn-primary" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> &nbsp;Yes</button>
      		</div>
      		{!! Form::close() !!}
    	</div>
  	</div>
</div>
@stop --}}