@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li><a href="#"><i class="fa fa-cube"></i> Prepared Product</a></li>
  </ol>
</section>
@actionStart('prepared_product', 'read')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-10">
			        <div class="x_panel">
		                <h2>
		                	<i class="fa fa-cube"></i> Prepared Product List 
		                	@actionStart('prepared_product', 'create')
		                	<a href="{{ URL::route('editor.prepared_product.create') }}" class="btn btn-primary btn-lg pull-right"><i class="fa fa-plus"></i> Add</a>
		                	@actionEnd
	                	</h2>
		                <hr>
			            <div class="x_content">
			                <table id="preparedProductTable" class="table table-striped dataTable">
							  	<thead>
							  	  	<tr>
								      	<th>#</th>
								      	<th>Product</th>
								      	<th>Material</th>
								      	<th>Average Size</th>
								      	<th>Predust</th>
								      	<th>Date</th>
								      	<th>Work Days</th>
								      	<th>Daily Target</th>
								      	<th></th>
							    	</tr>
							  	</thead>
							  	<tbody>
							    @foreach($prepared_products as $key => $prepared_product)
							    	<tr>
							      		<td>{{$key+1}}</td>
								      	<td>{{$prepared_product->m_prep_product->name}}</td>
								      	<td>{{$prepared_product->production->name}}</td>
								      	<td>{{$prepared_product->avg_size}}</td>
								      	<td>{{$prepared_product->flavor}}</td>
								      	<td>
								      		{{date("D, d M Y", strtotime($prepared_product->start_date))}}
								      		&nbsp;<i class="fa fa-arrow-right"></i>&nbsp;
								      		{{date("D, d M Y", strtotime($prepared_product->finish_date))}}
								      	</td>
								      	<td>{{$prepared_product->work_days}}</td>
								      	<td>{{$prepared_product->daily_target}}</td>
								      	<td align="center">
								      	<div class="col-md-2 nopadding">
								      			<a href="{{ URL::route('editor.prepared_product.create_daily', [$prepared_product->id]) }}" class="btn btn-primary btn-sm"><i class="fa fa-cogs"></i></a>
								      		</div>
								      		<div class="col-md-2 nopadding">
								      			<a href="{{ URL::route('editor.prepared_product.summary', [$prepared_product->id]) }}" class="btn btn-primary btn-sm"><i class="fa fa-file-text"></i></a>
								      		</div>

								      		{{-- 
								      		@actionStart('prepared_product', 'update')
								      		<div class="col-md-2 nopadding">
								      			<a href="{{ URL::route('editor.prepared_product.edit', [$prepared_product->id]) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
								      		</div>
								      		@actionEnd

								      		@actionStart('prepared_product', 'delete')
							      			<div class="col-md-2 nopadding">
								      			{!! Form::open(array('route' => ['editor.prepared_product.delete', $prepared_product->id], 'method' => 'delete'))!!}
		                    					{{ csrf_field() }}	                    				
								      			<button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-trash"></i></a></button>
								      			{!! Form::close() !!}
							      			</div>
							      			@actionEnd --}}
							      		</td>
								    </tr>
							    @endforeach
								</tbody>
							</table>
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@actionEnd
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function () {
    $("#preparedProductTable").DataTable();
    });
</script>
@stop