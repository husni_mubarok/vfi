@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li class="active"><a href="{{ URL::route('editor.prepared_product.index') }}"><i class="fa fa-cube"></i> Prepared Product</a></li>
  </ol>
</section>
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-5">
			        <div class="x_panel">
		                <h2>
		                	@if(isset($prepared_product))
		                	<i class="fa fa-pencil"></i>
		                	@else
		                	<i class="fa fa-plus"></i> 
		                	@endif
		                	<i class="fa fa-cube"></i> {{$prepared_product->m_prep_product->name}} Material
	                	</h2>
		                <hr>
			            <div class="x_content">

			            <table class="table table-bordered">
			            	<tr>
			            		<th>Product Name</th>
			            		<td>{{$prepared_product->m_prep_product->name}}</td>
			            	</tr>
			            	<tr>
			            		<th>Material (Size)</th>
			            		<td>
			            			{{$prepared_product->production->name}} 
			            			({{$prepared_product->size->min_value}}~{{$prepared_product->size->max_value}})
			            		</td>
			            	</tr>
			            	<tr>
			            		<th>Estimated Result</th>
			            		<td>{{$prepared_product->estimated_result}} {{$prepared_product->m_prep_product->uom}}</td>
			            	</tr>
			            </table>

			                @include('errors.error')
	                        {{-- @if(isset($prepared_product))
			                {!! Form::model($prepared_product, array('route' => ['editor.prepared_product.update', $prepared_product->id], 'method' => 'PUT'))!!}
		                    @else --}}
		                    {!! Form::open(array('route' => ['editor.prepared_product.update_material', $prepared_product->id]))!!}
		                    {{-- @endif --}}
		                    {{ csrf_field() }}
		                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
		                    	<table class="table">
		                    		<tr>
		                    			<th><i class="fa fa-check-square-o"></i></th>
		                    			<th>Type</th>
		                    			<th>Amount (pcs)</th>
		                    			<th>Weight (kg)</th>
		                    			<th>Availability</th>
		                    			<th>Date</th>
		                    		</tr>
			                    	@foreach($prepared_product->m_prep_product->production_m_prep_product as $production_m_prep_product)
			                    	@foreach($production_m_prep_product->production->production_result as $production_result)
			                    	<tr>
			                    		<td><input type="radio" name="material" value="{{$production_result->id}}" @if($production_result->available == 0) disabled @endif></td>
			                    		<td>{{$production_result->production->name}}</td>
			                    		<td align="right">{{number_format($production_result->amount)}}</td>
			                    		<td align="right">{{number_format($production_result->weight)}}</td>
			                    		<td>@if($production_result->available == 0) No @else Yes @endif</td>
			                    		<td>{{date("d M Y", strtotime($production_result->finished_at))}}</td>
			                    	</tr>
			                    	@endforeach
			                    	@endforeach
		                    	</table>
	                            <button type="submit" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
	                    	</div>
	                        {!! Form::close() !!}
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@stop