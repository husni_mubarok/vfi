@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li class="active"><a href="{{ URL::route('editor.prepared_product.index') }}"><i class="fa fa-cube"></i> Prepared Product</a></li>
  </ol>
</section>
@actionStart('prepared_product', 'create|update')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-5">
			        <div class="x_panel">
		                <h2>
		                	@if(isset($prepared_product))
		                	<i class="fa fa-pencil"></i>
		                	@else
		                	<i class="fa fa-plus"></i> 
		                	@endif
		                	<i class="fa fa-cube"></i> Prepared Product
	                	</h2>
		                <hr>
			            <div class="x_content">
			                @include('errors.error')
	                        @if(isset($prepared_product))
			                {!! Form::model($prepared_product, array('route' => ['editor.prepared_product.update', $prepared_product->id], 'method' => 'PUT'))!!}
		                    @else
		                    {!! Form::open(array('route' => 'editor.prepared_product.store'))!!}
		                    @endif
		                    {{ csrf_field() }}
		                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
		                    	{{ Form::label('m_prep_product_id', 'Product') }}
		                    	{{ Form::select('m_prep_product_id', $prepared_product_list, null, ['class' => 'form-control', 'placeholder' => 'Select a Product']) }}
		                    	<br/>

		                    	{{ Form::label('production_id', 'Material') }}
		                    	{{ Form::select('production_id', $production_list, null, ['class' => 'form-control', 'placeholder' => 'Select a Material']) }}
		                    	<br/>

		                    	{{ Form::label('avg_size', 'Average Size') }}
		                    	{{ Form::number('avg_size', old('avg_size'), ['class' => 'form-control', 'placeholder' => 'Average Size']) }}
		                    	<br/>

		                    	{{ Form::label('daily_target', 'Daily Target') }}
		                    	{{ Form::number('daily_target', old('daily_target'), ['class' => 'form-control', 'placeholder' => 'Daily Target']) }}
		                    	<br/>

		                    	{{ Form::label('work_days', 'Work Days') }}
		                    	{{ Form::number('work_days', old('work_days'), ['class' => 'form-control', 'placeholder' => 'Work Days']) }}
		                    	<br/>

		                    	{{ Form::label('start_date', 'Start Date') }}
		                    	{{ Form::text('start_date', old('start_date'), ['class' => 'form-control', 'placeholder' => 'Start Date']) }}
		                    	<br/>

		                    	{{ Form::label('finish_date', 'Finish Date') }}
		                    	{{ Form::text('finish_date', old('finish_date'), ['class' => 'form-control', 'placeholder' => 'Finish Date']) }}
		                    	<br/>

		                    	{{ Form::label('flavor', 'Flavor') }}
		                    	{{ Form::select('flavor', ['Italian' => 'Italian', 'Spicy' => 'Spicy', 'Salty' => 'Salty'], null, ['class' => 'form-control', 'placeholder' => 'Select a flavor']) }}
		                    	<br/>


		                    	{{-- {{ Form::label('m_prep_product_id', 'Prepared Product') }} --}}
		                    	{{-- {{ Form::select('m_prep_product_id', $m_prep_products, null, ['class' => 'form-control', 'placeholder' => 'Select a Prepared Product']) }}<br/> --}}

		                    	{{-- <div class="col-md-8 col-sm-8 col-xs-8">
		                    	{{ Form::label('production_id', 'Material') }}
		                    	{{ Form::select('production_id', [], null, ['class' => 'form-control', 'placeholder' => 'Select a Material', 'disabled' => 'true']) }}<br/>
		                    	</div>

		                    	<div class="col-md-4 col-sm-4 col-xs-4">
		                    	{{ Form::label('size_id', 'Size') }}
		                    	{{ Form::select('size_id', [], null, ['class' => 'form-control', 'placeholder' => 'Select a Size', 'disabled' => 'true']) }}<br/>
		                    	</div>

		                        {{ Form::label('estimated_result', 'Estimated Result') }}
		                        <div class="input-group">
		                        	{{ Form::number('estimated_result', old('estimated_result'), array('class' => 'form-control', 'placeholder' => 'Estimated Result')) }}
			                        <div class="input-group-addon">
			                        	<span id="m_prep_product_uom"> - </span>
			                        </div>
		                        </div><br/> --}}

		                        {{-- {{ Form::label('create_date', 'Create Date') }}
		                        {{ Form::text('create_date', old('create_date'), ['class' => 'form-control', 'id' => 'create_date']) }}<br/>

		                        {{ Form::label('expire_date', 'Expire Date') }}
		                        {{ Form::text('expire_date', old('expire_date'), ['class' => 'form-control', 'id' => 'expire_date']) }}<br/> --}}

	                            <button type="submit" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
	                    	</div>
	                        {!! Form::close() !!}
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@actionEnd
@stop

@section('scripts')
<script>
$(document).ready(function () {
	$('#start_date').datetimepicker({
		format: 'YYYY-MM-DD',
	});

	$('#finish_date').datetimepicker({
		format: 'YYYY-MM-DD',
	});
});

// $("#m_prep_product_id").on('change blur', function()
// {
//     $.ajax({
//         url : '{{ URL::route('get.m_prep_product_uom') }}',
//         data : {'m_prep_product_id':$("#m_prep_product_id").val()},
//         type : 'GET',
//         headers : {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
//         success : function(data, textStatus, jqXHR){
//         	$('#m_prep_product_uom').replaceWith('<span id="m_prep_product_uom">'+data+'</span>')
//         },
//         error : function(){
//         	$('#m_prep_product_uom').replaceWith('<span id="m_prep_product_uom"> - </span>')
//         },
//     })

//     $.ajax({
//         url : '{{ URL::route('get.production') }}',
//         data : {'m_prep_product_id':$("#m_prep_product_id").val()},
//         type : 'GET',
//         headers : {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
//         success : function(data, textStatus, jqXHR){
//         	$('#production_id').empty();
//         	jQuery.each(data, function(i, val)
//             {
//             	$('#production_id').append($('<option>', { 
// 			        value: i,
// 			        text : val
// 			    }));
//             });
//             $("#production_id").attr('disabled', false);
//         },
//         error : function(){
//         	$("#production_id").val('');
//         	$("#production_id").attr('disabled', true);
//         	$("#size_id").val('');
//         	$("#size_id").attr('disabled', true);
//         },
//     })
// });

// $("#production_id").on('change blur', function()
// {
// 	$.ajax({
//         url : '{{ URL::route('get.production_size') }}',
//         data : {'production_id':$("#production_id").val()},
//         type : 'GET',
//         headers : {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
//         success : function(data, textStatus, jqXHR){
//         	$('#size_id').empty();
//         	jQuery.each(data, function(i, val)
//             {
//             	$('#size_id').append($('<option>', { 
// 			        value: i,
// 			        text : val
// 			    }));
//             });
//             $("#size_id").attr('disabled', false);
//         },
//         error : function(){
//         	$("#size_id").val('');
//         	$("#size_id").attr('disabled', true);
//         },
//     })
// });


</script>
@stop