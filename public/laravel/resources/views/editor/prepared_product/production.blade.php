@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li class="active"><a href="{{ URL::route('editor.prepared_product.index') }}"><i class="fa fa-cube"></i> Prepared Product</a></li>
  </ol>
</section>
@actionStart('prepared_product', 'create|update')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-5">
			        <div class="x_panel">
		                <h2>
		                	@if(isset($prepared_product_detail))
		                	<i class="fa fa-pencil"></i>
		                	@else
		                	<i class="fa fa-plus"></i> 
		                	@endif
		                	<i class="fa fa-cube"></i> Prepared Product
	                	</h2>
		                <hr>
			            <div class="x_content">
			                @include('errors.error')
	                        @if(isset($prepared_product_detail))
			                {!! Form::model($prepared_product_detail, array('route' => ['editor.prepared_product.update_daily', $prepared_product_detail->id], 'method' => 'PUT'))!!}
		                    @else
		                    {!! Form::open(array('route' => ['editor.prepared_product.store_daily', $prepared_product->id]))!!}
		                    @endif
		                    {{ csrf_field() }}
		                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
		                    	{{ Form::label('product', 'Product') }}
		                    	<p>{{$prepared_product->m_prep_product->name}}</p>

		                    	{{ Form::label('date', 'Current Date') }}
		                    	{{ Form::hidden('date', date("Y-m-d"))}}
		                    	<p>{{date("l, d M Y")}}</p>

		                    	{{ Form::label('material_used', 'Material Used') }}
		                    	<p>{{$prepared_product->production->name}}</p>
		                    	
		                    	{{ Form::label('processed_amount', 'Produced Amount') }}
		                    	<div class="input-group">
		                    	{{ Form::number('processed_amount', old('processed_amount'), ['class' => 'form-control']) }}
	                    		<div class="input-group-addon">{{$prepared_product->m_prep_product->uom}}</div>	
		                    	</div>

		                    	<hr>
		                    	<h3>Ingredient Used</h3>
		                    	<table class="table">
		                    	@foreach($prepared_product->m_prep_product->m_prep_product_ingredient as $ingredient)
		                    	<tr>
		                    		<th>{{$ingredient->ingredient->name}}</th>
	                    			<td>
	                    				<div class="input-group">
		                    			{{ Form::number('ingredient['.$ingredient->ingredient->id.']', old('ingredient['.$ingredient->ingredient->id.']'), ['class' => 'form-control']) }}
		                    			<div class="input-group-addon">{{$ingredient->ingredient->uom}}</div>
		                    			</div>
	                    			</td>
		                    	</tr>
		                    	@endforeach
		                    	</table>

	                            <button type="submit" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
	                    	</div>
	                        {!! Form::close() !!}
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@actionEnd
@stop

@section('scripts')
<script>
$(document).ready(function () {
	$('#start_date').datetimepicker({
		format: 'YYYY-MM-DD',
	});

	$('#finish_date').datetimepicker({
		format: 'YYYY-MM-DD',
	});
});
</script>
@stop