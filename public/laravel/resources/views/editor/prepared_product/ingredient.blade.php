@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li><a href="{{ URL::route('editor.prepared_product.index') }}"><i class="fa fa-cube"></i> Prepared Product</a></li>
    <li class="active"><a href="#"><i class="fa fa-dollar"></i> Ingredient</a></li>
  </ol>
</section>
@actionStart('price', 'create|update')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-10">
			        <div class="x_panel">
		                <h2>
		                	<i class="fa fa-dollar"></i>&nbsp;Ingredient List
	                	</h2>
		                <hr>
			            <div class="x_content">
			            	<div class="col-md-6">
			            		<table class="table table-bordered">
			            			<tr>
			            				<th>Name</th>
			            				<td>{{$prepared_product->m_prep_product->name}}</td>

			            				<th>Create Date</th>
			            				<td>{{date("d M Y", strtotime($prepared_product->created_at))}}</td>
			            			</tr>
			            			<tr>
			            				<th>Material</th>
			            				<td>{{$prepared_product->production->name}}</td>

			            				<th>Size</th>
			            				<td>{{$prepared_product->size->min_value}}~{{$prepared_product->size->max_value}}</td>
			            			</tr>
			            			<tr>
			            				<th>Estimated Result</th>
			            				<td>{{number_format($prepared_product->estimated_result)}} {{$prepared_product->m_prep_product->uom}}</td>

			            				<th>Size</th>
			            				<td>{{$prepared_product->size->min_value}}~{{$prepared_product->size->max_value}}</td>
			            			</tr>
			            		</table>

			            		<hr>

			            		@include('errors.error')
			            		{{-- @if(isset($prices))
				            	{!! Form::open(array('route' => ['editor.price.update', $purchase->id], 'method' => 'PUT'))!!}
				            	@else --}}
				            	{!! Form::open(array('route' => ['editor.prepared_product.update_ingredient', $prepared_product->id])) !!}
				            	{{-- @endif --}}
				                <table class="table table-bordered">
								  	<thead>
								  	  	<tr>
											<th>Name</th>
											<th>Description</th>
											<th>Quantity</th>
										</tr>
								  	</thead>
								  	<tbody>
							  			@foreach($prepared_product->m_prep_product->m_prep_product_ingredient as $key => $m_prep_product_ingredient)
							  			<tr>
							  				<td>{{$m_prep_product_ingredient->ingredient->name}}</td>
							  				<td>{{$m_prep_product_ingredient->ingredient->description}}</td>
							  				<td>
							  					<div class="input-group">
							  					{{ Form::number('ingredient['.$m_prep_product_ingredient->ingredient->id.']', old('ingredient['.$m_prep_product_ingredient->ingredient->id.']'), array('class' => 'form-control')) }}
							  					{{-- {{ Form::number('ingredient['.$m_prep_product_ingredient->ingredient->id.']', old('ingredient['.$m_prep_product_ingredient->ingredient->id.']', $prepared_product->prepared_product_ingredient[$key]->value), array('class' => 'form-control')) }} --}}
							  					<div class="input-group-addon">
							  						<span>{{$m_prep_product_ingredient->ingredient->uom}}</span>
							  					</div>
							  					</div>
							  				</td>
							  			</tr>
								  		@endforeach
									</tbody>
								</table>
								<button type="button" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
								{!! Form::close() !!}
							</div>

							<div class="col-md-6">
							@php
							$ready_stock = 0;
							foreach($stock_items as $stock_item)
							{
								$ready_stock += $stock_item->value;
							}
							foreach($consumed_items as $consumed_item)
							{
								$ready_stock -= $consumed_item->estimated_result;
							}
							@endphp
							{{$ready_stock}}
							</div>
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@actionEnd
@stop