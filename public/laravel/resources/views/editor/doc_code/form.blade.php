@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="{{ URL::route('editor.doc_code.index') }}"><i class="fa fa-file-code-o"></i> Doc Code</a></li>
    @if(isset($doc_code))
    <li class="active"><a href="#"><i class="fa fa-pencil"></i> Edit</a></li>
    @else
    <li class="active"><a href="#"><i class="fa fa-plus"></i> Create</a></li>
    @endif
  </ol>
</section>
@actionStart('doc_code', 'create|update')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-5">
			        <div class="x_panel">
		                <h2>
		                	@if(isset($doc_code))
		                	<i class="fa fa-pencil"></i>
		                	@else
		                	<i class="fa fa-plus"></i> 
		                	@endif
		                	<i class="fa fa-file-code-o"></i> Doc Code
	                	</h2>
		                <hr>
			            <div class="x_content">
			                @include('errors.error')
	                        @if(isset($doc_code))
			                {!! Form::model($doc_code, array('route' => ['editor.doc_code.update', $doc_code->id], 'method' => 'PUT'))!!}
		                    @else
		                    {!! Form::open(array('route' => 'editor.doc_code.store'))!!}
		                    @endif
		                    {{ csrf_field() }}
		                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
		                    	{{ Form::label('name', 'Name') }}
		                        {{ Form::text('name', old('name'), array('class' => 'form-control', 'placeholder' => 'Name*', 'required' => 'true')) }}<br/>

	                            <button type="submit" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
	                    	</div>
	                        {!! Form::close() !!}
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@actionEnd
@stop