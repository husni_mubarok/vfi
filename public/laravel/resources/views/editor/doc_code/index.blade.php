@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active"><a href="#"><i class="fa fa-file-code-o"></i> Doc Code</a></li>
  </ol>
</section>
@actionStart('doc_code', 'read')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-10">
			        <div class="x_panel">
		                <h2>
		                	<i class="fa fa-file-code-o"></i> Doc Code List
		                	<a href="{{ URL::route('editor.doc_code.create') }}" class="btn btn-primary btn-lg pull-right"><i class="fa fa-plus"></i> Add</a>
	                	</h2>
		                <hr>
			            <div class="x_content">
			                <table id="docCodeTable" class="table table-striped dataTable">
							  	<thead>
							  	  	<tr>
								      	<th>#</th>
								      	<th>Name</th>
								      	<th>Last Number</th>
								      	<th></th>
							    	</tr>
							  	</thead>
							  	<tbody>
							    @foreach($doc_codes as $key => $doc_code)
							    	<tr>
							      		<td>{{$key+1}}</td>
								      	<td>{{$doc_code->name}}</td>
								      	<td>{{$doc_code->last_num}}</td>
								      	<td align="center">
								      		@actionStart('doc_code', 'update')
								      		<div class="col-md-2 nopadding">
								      			<a href="{{ URL::route('editor.doc_code.edit', [$doc_code->id]) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
								      		</div>
								      		@actionEnd

								      		@actionStart('doc_code', 'delete')
								      		<div class="col-md-2 nopadding">
								      			{!! Form::open(array('route' => ['editor.doc_code.reset', $doc_code->id], 'method' => 'put'))!!}
		                    					{{ csrf_field() }}	                    				
								      			<button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-history"></i></a></button>
								      			{!! Form::close() !!}
								      		</div>
								      		@actionEnd

								      		@actionStart('doc_code', 'delete')
							      			<div class="col-md-2 nopadding">
								      			{!! Form::open(array('route' => ['editor.doc_code.delete', $doc_code->id], 'method' => 'delete'))!!}
		                    					{{ csrf_field() }}	                    				
								      			<button type="submit" class="btn btn-primary btn-sm" onclick="return confirm('Confirm delete?')"><i class="fa fa-trash"></i></a></button>
								      			{!! Form::close() !!}
							      			</div>
							      			@actionEnd
							      		</td>
								    </tr>
							    @endforeach
								</tbody>
							</table>
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@actionEnd
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function () {
    $("#docCodeTable").DataTable(
    	{
    		"scrollX": true
    	});
    });
</script>
@stop