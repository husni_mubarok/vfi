@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active"><a href="#"><i class="fa fa-gears"></i> Module</a></li>
  </ol>
</section>
@actionStart('module', 'read')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-1"></div>
					<div class="col-md-10">
						<div class="x_panel">
						<h2>
		                	<i class="fa fa-gears"></i> Module List
		                	@actionStart('module', 'create')
		                	<a href="{{ URL::route('editor.module.create') }}" class="btn btn-primary btn-lg pull-right"><i class="fa fa-plus"></i> Add</a>
		                	@actionEnd
	                	</h2>
		                <hr>
			            <div class="x_content">
			            	<table id="moduleTable" class="table table-hover table-striped">
			            		<thead>
			            			<tr>
			            				<th>#</th>
			            				<th>Name</th>
			            				<th>Description</th>
			            				<th>Action</th>
			            			</tr>
			            		</thead>
			            		<tbody>
			            			@foreach($modules as $key => $module)
									<tr>
										<td>{{$key+1}}</td>
										<td>{{$module->name}}</td>
										<td>{{$module->description}}</td>
										<td style="text-align:center;">
											@actionStart('module', 'update')
											<a href="{{ URL::route('editor.module.edit', $module->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil" style="display:inline-block"></i></a>
											@actionEnd

											@actionStart('module', 'delete')
											{!! Form::open(array('route' => ['editor.module.delete', $module->id], 'method' => 'delete', 'style' => 'display:inline-block;padding-left:15px;'))!!}
	                    					{{ csrf_field() }}	                    				
							      			<button type="submit" class="btn btn-primary btn-sm" onclick="return confirm('Confirm delete?')"><i class="fa fa-trash"></i></a></button>
							      			{!! Form::close() !!}
							      			@actionEnd
										</td>
									</tr>
									@endforeach
			            		</tbody>
			            	</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>
@actionEnd
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function () {
    $("#moduleTable").DataTable(
    	{
    		"scrollX": true
    	});
    });
</script>
@stop