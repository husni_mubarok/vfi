@extends('layouts.home.template')
@section('content') 
<section class="content box box-solid">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-1"></div>
			<div class="col-md-5">
				<div class="x_panel">
					<h2>
						@if(isset($marketing))
						<i class="fa fa-pencil"></i>
						@else
						<i class="fa fa-plus"></i> 
						@endif
						<i class="fa fa-address-card"></i> Marketing
					</h2>
					<hr>
					<div class="x_content">
						@include('errors.error')
						@if(isset($marketing))
						{!! Form::model($marketing, array('route' => ['editor.marketing.update', $marketing->id], 'method' => 'PUT', 'class'=>'update'))!!}
						@else
						{!! Form::open(array('route' => 'editor.marketing.store', 'class'=>'create'))!!}
						@endif
						{{ csrf_field() }}
						<div class="col-md-12 col-sm-12 col-xs-12 form-group">
							{{ Form::label('name', 'Name') }}
							{{ Form::text('name', old('name'), array('class' => 'form-control', 'placeholder' => 'Name*', 'required' => 'true')) }}<br/>

							{{ Form::label('address', 'Address') }}
							{{ Form::text('address', old('address'), array('class' => 'form-control', 'placeholder' => 'Address*', 'required' => 'true')) }}<br/>

							{{ Form::label('email', 'E-mail Address') }}
							{{ Form::text('email', old('email'), array('class' => 'form-control', 'placeholder' => 'E-mail Address*', 'required' => 'true')) }}<br/>

							{{ Form::label('phone_number', 'Phone Number') }}
							{{ Form::text('phone_number', old('phone_number'), array('class' => 'form-control', 'placeholder' => 'Phone Number*', 'required' => 'true')) }}<br/> 

							{{ Form::label('note', 'Note') }}
							{{ Form::text('note', old('note'), array('class' => 'form-control', 'placeholder' => 'Note', 'required' => 'true')) }}<br/>

							<button type="submit" class="btn btn-success pull-right"><i class="fa fa-check"></i> Save</button>
						</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@stop

@section('scripts')
<script>
	$(".update").on("submit", function(){
		return confirm("Do you want to update this marketing?");
	});

	$(".create").on("submit", function(){
		return confirm("Do you want to create this marketing?");
	});
</script>
@stop