@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active"><i class="fa fa-ban"></i> Reject Type</li>
  </ol>
</section>
@actionStart('reject_type', 'read')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-10">
			        <div class="x_panel">
		                <h2>
		                	<i class="fa fa-ban"></i> Reject Type List
		                	@actionStart('reject_type', 'create')
		                	<a href="{{ URL::route('editor.reject_type.create') }}" class="btn btn-default btn-lg pull-right"><i class="fa fa-plus"></i> Add</a>
		                	@actionEnd
	                	</h2>
		                <hr>
			            <div class="x_content">
			                <table id="reject_typeTable" class="table table-striped dataTable">
							  	<thead>
							  	  	<tr>
								      	<th>#</th>
								      	<th>Name</th>
								      	<th>Description</th>
								      	<th></th>
							    	</tr>
							  	</thead>
							  	<tbody>
							    @foreach($reject_types as $key => $reject_type)
							    	<tr>
							      		<td>{{$key+1}}</td>
								      	<td>{{$reject_type->name}}</td>
								      	<td>{{$reject_type->description}}</td>
								      	<td align="center">
								      		<div class="col-md-2 nopadding">
								      			@actionStart('reject_type', 'update')
								      			<a href="{{ URL::route('editor.reject_type.edit', [$reject_type->id]) }}" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i></a>
								      			@actionEnd
								      		</div>
							      			<div class="col-md-2 nopadding">
							      				@actionStart('reject_type', 'delete')
								      			{!! Form::open(array('route' => ['editor.reject_type.delete', $reject_type->id], 'method' => 'delete'))!!}
		                    					{{ csrf_field() }}	                    				
								      			<button type="submit" class="btn btn-default btn-sm" onclick="return confirm('Confirm delete?')"><i class="fa fa-trash"></i></a></button>
								      			{!! Form::close() !!}
								      			@actionEnd
							      			</div>
							      		</td>
								    </tr>
							    @endforeach
								</tbody>
							</table>
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@actionEnd
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function () {
    $("#reject_typeTable").DataTable(
    	{
    		"scrollX": true
    	});
    });
</script>
@stop