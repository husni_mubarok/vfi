@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="{{ URL::route('editor.purchase.index') }}"><i class="fa fa-shopping-cart"></i> Purchase</a></li>
    <li class="active"><a href="#"><i class="fa fa-dropbox"></i> Purchase Detail</a></li>
  </ol>
</section>
@actionStart('purchase_detail', 'create|update')
<section class="content">
    <section class="content box box-solid">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div class="x_panel">
                        <h2>
                            <i class="fa fa-dropbox"></i>&nbsp;Purchase Detail
                            <small>{{$purchase->description}}</small>
                        </h2>
                        <hr>
                        <div class="x_content">
                            <div class="col-md-6">
                                {{ Form::label('product', 'Product') }}&nbsp;<i class="fa fa-caret-right"></i>&nbsp;{{$purchase->purchase_product->product->product_type->name}}&nbsp;{{$purchase->purchase_product->product->name}}<br/>

                                {{ Form::label('size', 'Size') }}
                                {{ Form::number('size', old('size'), ['id' => 'size', 'min' => '0', 'class' => 'form-control', 'step' => '1']) }}<br/>

                                {{ Form::label('amount', 'Weight') }}
                                {{ Form::number('amount', old('amount'), ['id' => 'amount', 'min' => '0', 'class' => 'form-control']) }}<br/>

                                {{ Form::label('price', 'Price') }}
                                {{ Form::number('price', old('price'), ['id' => 'price', 'class' => 'form-control', 'disabled' => 'true']) }}<br/>
                                {{-- <div id="price_text">0</div><br/> --}}

                                <button type="button" id="btn_add_product" class="btn btn-default" disabled><i class="fa fa-cart-plus"></i>&nbsp;Add</button>
                                <br/>
                                @include('errors.error')
                                {!! Form::open(array('route' => ['editor.purchase_detail.store', $purchase->id]))!!}  
                                <table class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>Size</th>
                                            <th>Price @ kg</th>
                                            <th>Amount (kg)</th>
                                            <th>Total</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="cart">
                                    </tbody>
                                </table>
                                <button type="button" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
                                {!! Form::close() !!}
                            </div>
                            <div class="col-md-6">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Min.</th>
                                            <th>Max.</th>
                                            <th>Note</th>
                                            <th>Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($purchase->price as $price)
                                        <tr>
                                            <td>{{$price->size->min_value}}</td>
                                            <td>{{$price->size->max_value}}</td>
                                            <td>{{$price->size->note}}</td>
                                            <td>{{number_format($price->value)}}&nbsp;{{$purchase->currency}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
@actionEnd
@stop

@section('scripts')
@if(isset($purchase_details))
<script>
$(document).ready(function(){
    var cart_content = {!! $purchase_details !!}
    jQuery.each( cart_content, function( i, val ) {
        var prependTable = '';
        
        prependTable += '<tr>';
        prependTable += '<td>'+val['size']+'<input type="hidden" name="size[]" value="'+val['size']+'"></td>';
        prependTable += '<td align="left">'+val['price']+'<input type="hidden" name="price[]" value="'+val['price']+'"></td>';
        prependTable += '<td align="left">'+val['amount']+'<input type="hidden" name="amount[]" value="'+val['amount']+'"></td>';
        prependTable += '<td align="left">'+val['subtotal']+'<input type="hidden" class="subtotal" value="'+val['subtotal']+'"></td>';
        prependTable += '<td><button type="button" class="btn_remove_product btn"><i class="fa fa-remove"></i></button></td>';
        prependTable += '</tr>';
        $("#cart").append(prependTable);
        
        $(".btn_remove_product").click(function(){
            $(this).parent().parent().remove(); 
        });

        $("#submit_button").replaceWith('<button type="button" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>');
    });
})
</script>
@endif
<script>
    $(document).ready(function(){
        $("#amount, #size").on('change blur keyup', function() {
            if(!$("#size").val() || !$("#amount").val()) 
            {
                $("#btn_add_product").attr("disabled", true);
            } else {
                $("#btn_add_product").attr("disabled", false);
            }
        });

        $("#size").on('blur', function()
        {
            $.ajax({
                url : '{{ URL::route('get.price') }}',
                data : {'size':$("#size").val(), 'purchase_id':{!! $purchase->id !!}, 'product_type_id':{!! $purchase->purchase_product->product->product_type->id !!}},
                type : 'GET',
                headers : {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
                success : function(data, textStatus, jqXHR){
                    // console.log(data);
                    $("#price").val(data);
                },
                error : function()
                {
                    $("#price").val('0');
                    $("#btn_add_product").attr("disabled", true);
                },
            })
        });
            
        $("#btn_add_product").click(function(){
            var outputString = '';
            outputString += '<tr>';
            outputString += '<td>'+$("#size").val()+'<input type="hidden" name="size[]" value="'+$("#size").val()+'"></td>';
            outputString += '<td align="left">'+$("#price").val()+'<input type="hidden" name="price[]" value="'+$("#price").val()+'"></td>';
            outputString += '<td align="left">'+$("#amount").val()+'<input type="hidden" name="amount[]" value="'+$("#amount").val()+'"></td>';
            outputString += '<td align="left">'+$("#amount").val() * $("#price").val()+'<input type="hidden" class="subtotal" value="'+$("#amount").val() * $("#price").val()+'"></td>';
            outputString += '<td><button type="button" class="btn_remove_product btn"><i class="fa fa-remove"></i></button></td>';
            outputString += '</tr>';

            $("#cart").append(outputString);
            $("#btn_submit").attr("disabled", false);
            $("#size").val('');
            $("#amount").val('');
            $("#price").val('');
            $("#btn_add_product").attr("disabled", true);

            $(".btn_remove_product").click(function(){
                $(this).parent().parent().remove(); 
            });
        });
    })
</script>
@stop