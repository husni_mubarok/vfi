@extends('layouts.home.template')
@section('content') 
<div class="header_menu mobile-hide">
  <a href="{{ URL::route('editor.type_item.index') }}">TYPE ITEM</a> |
  <a href="{{ URL::route('editor.category_item.index') }}">CATEGORY ITEM</a> |
  <a href="{{ URL::route('editor.master_item.index') }}">MASTER ITEM</a> |
  <a href="{{ URL::route('editor.unit.index') }}">SATUAN</a> |
  <a href="{{ URL::route('editor.custom_detail.index') }}">CUSTOM DETAIL</a>
</div> 
<section class="content box box-solid">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-1"></div>
			<div class="col-md-5">
				<div class="x_panel">
					<h2>
						@if(isset($unit))
						<i class="fa fa-pencil"></i>
						@else
						<i class="fa fa-plus"></i> 
						@endif
						<i class="fa fa-bar-chart"></i> Unit
					</h2>
					<hr>
					<div class="x_content">
						@include('errors.error')
						@if(isset($unit))
						{!! Form::model($unit, array('route' => ['editor.unit.update', $unit->id], 'method' => 'PUT', 'class'=>'update'))!!}
						@else
						{!! Form::open(array('route' => 'editor.unit.store', 'class'=>'create'))!!}
						@endif
						{{ csrf_field() }}
						<div class="col-md-12 col-sm-12 col-xs-12 form-group">
							
							{{-- {{ Form::label('Unit') }}
							{{ Form::select('parent', $unit_list, old('parent'), array('class' => 'form-control', 'required' => 'true')) }}<br/> --}}

							{{ Form::label('name', 'Name') }}
							{{ Form::text('name', old('name'), array('class' => 'form-control', 'placeholder' => 'Name*', 'required' => 'true')) }}<br/>

							{{ Form::label('description', 'Description') }}
							{{ Form::text('description', old('description'), array('class' => 'form-control', 'placeholder' => 'Description*', 'required' => 'true')) }}<br/>

							<button type="submit" class="btn btn-success pull-right"><i class="fa fa-check"></i> Save</button>

							<a href="{{ URL::route('editor.unit.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
						</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@stop

@section('scripts')
<script>
	$(".update").on("submit", function(){
		return confirm("Do you want to update this item type?");
	});

	$(".create").on("submit", function(){
		return confirm("Do you want to create this item type?");
	});
</script>
@stop