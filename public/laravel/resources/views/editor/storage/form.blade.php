@extends('layouts.home.template')
@section('content')
<!-- Content Header (Page header) -->
<div class="header_menu mobile-hide">
    <a href="{{ URL::to('/editor/storage') }}">Storage</a> |
</div>

@actionStart('storage', 'create|update')
<section class="content">
	<!-- <section class="content box box-solid"> -->
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-5">
			        <div class="x_panel">
		                <h2>
		                	@if(isset($storage))
		                	<i class="fa fa-pencil"></i>
		                	@else
		                	<i class="fa fa-plus"></i>
		                	@endif
		                	<i class="fa fa-truck"></i> Storage
	                	</h2>
		                <hr>
			            <div class="x_content">
			                @include('errors.error')

	                    @if(isset($storage))
			                   {!! Form::model($storage, array('route' => ['editor.storage.update', $storage->id], 'method' => 'PUT'))!!}
		                  @else
		                    {!! Form::open(array('route' => 'editor.storage.store'))!!}
		                  @endif

                      {{ csrf_field() }}

                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                          <input type="hidden" name="valArySend" id="valArySend">

		                    	{{ Form::label('production', 'Production*') }}
		                      {{ Form::text('production', old('production'), array('class' => 'form-control', 'placeholder' => 'Production', 'required' => 'true')) }}
                            <br/>
                            <a href="#" class="btn btn-primary" id="lookup"><span class="fa fa-search"></span>&nbsp;Lookup</a>
                            <br/>

                            <br/>
                            {{ Form::label('placement_storage', 'Placement Storage') }}
                            {{ Form::select('placement_storage', $ds, old('placement_storage'), ['placeholder' => 'Select a Type', 'class' => 'form-control', 'required' => 'true']) }}
                            <!-- <br/>
                            <a href="#" class="btn btn-primary" id="add_placement"><span class="fa fa-search"></span>&nbsp;Add Placement</a>
                            <br/> -->
                            <br>

                            {{ Form::label('datetime', 'Date & Time*') }}
          							    {{ Form::text('datetime', old('datetime'), array('class' => 'form-control', 'placeholder' => 'Date & Time', 'required' => 'true', 'id' => 'datetime')) }}<br/>

                            {{ Form::label('unit', 'Unit') }}
		                        {{ Form::select('unit', ['Block' => 'Block'], old('unit'), ['placeholder' => 'Select a Type', 'class' => 'form-control', 'required' => 'true', 'id'=>'unitSelect']) }}<br/>

                            <br/>

                            <button type="submit" class="btn btn-success pull-right" ><i class="fa fa-check"></i> Save</button>
	                    	</div>
	                        {!! Form::close() !!}
			            </div>
			        </div>
		        </div>
            <div class="col-md-6">
              <h3>Item To Store</h3>
              <table class="table">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Type</th>
                  <th>Size</th>
                  <th>Qty</th>
                </tr>
                </thead>
                <tbody id="cart">

                </tbody>
              </table>
            </div>
		    </div>
		</div>
	<!-- </section> -->
</section>
@actionEnd
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Production</h4>
      </div>
      <div class="modal-body">
        <!-- <h3>Advance Search</h3>
        <hr>
        <table>
          <tr id="filter_col1" data-column="2">
            <td>Name : </td>
            <td><input type="text" name="nameST" class="column_filter" id="col2_filter"></td>
          </tr>
          <tr id="filter_col2" data-column="3">
            <td>Size : </td>
            <td><input type="text" name="sizeST" class="column_filter" id="col3_filter"></td>
          </tr>
          <tr id="filter_col3" data-column="4">
            <td>Block : </td>
            <td><input type="text" name="blockST" class="column_filter" id="col4_filter"></td>
          </tr>
          <tr id="filter_col4" data-column="5">
            <td>Condition : </td>
            <td><input type="text" name="conditionST" class="column_filter" id="col5_filter"></td>
          </tr>
          <tr>
            <td>Storage Date Time : </td>
            <td><input type="text" name="date_startST" id="date_startST"> | <input type="text" name="date_finishST" id="date_finishST"></td>
          </tr>
          <tr>
            <td>Quantity : </td>
            <td><input type="text" name="quantityST" id="quantityST"></td>
          </tr>
        </table>
        <a href="javascript:void(0)" class="btn btn-success" id="searchTable">Search</a>
        <hr> -->
        <table class="table table-striped table-responsive" id="productionResult">
          <thead>
          <tr>
            <th>No</th>
            <th>Name</th>
            <th>Start Date</th>
            <th>Finish Date</th>
            <th>Wieght</th>
            <th>Block Wieght</th>
            <th>Total Block</th>
            <th>Master Carton</th>
            <th>Total Master Carton</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody id="elmTableFirst">

        </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="myModalStorage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Production</h4>
      </div>
      <div class="modal-body">
        <p> Name Storage : <input type="text" id="name_storage"></p>
        <p> Description : <textarea id="description_storage"></textarea></p>
        <p> <a href="#" id="submit_storage" class="btn btn-primary">submit</a></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document" style="width:990px;">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Production</h4>
      </div>
      <div class="modal-body">
        <div>
          <table class="table table-striped table-responsive">
            <thead>
            <tr>
              <th>No</th>
              <th>Size</th>
              <th>Type</th>
              <th>Qty Production [Block]</th>
              <th>Qty Storage</th>
              <th>Qty Remaining</th>
              <th>Item</th>
              <th>Store</th>
            </tr>
          </thead>
          <tbody id="elmTable">
          </tbody>
        </table>
        <a href="javascript:void(0)" class="btn btn-success" id="storeSubmit">Store</a>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Back</button>
      </div>
    </div>
  </div>
</div>
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script>
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>

<script>
function validval(this_elm, idelm){
  console.log(idelm);
  var elmVal = $(this_elm).closest('tr').find('td:eq(5)').text();
  var curVal = $("#storeval"+idelm).val();
  // console.log(elmVal);
  // console.log(curVal);
  if(parseInt(curVal) > parseInt(elmVal)){
    alert('stock tidak tersedia');
    $("#storeval"+idelm).val("");
  }
  // console.log(elmVal);
  // console.log(curVal);
}

function get_stock(this_elm, j){
  // console.log("testing");
  // console.log(this_elm.value);
  // var table_position = $(this_elm).closest('tr').find('td:eq(4)').text();
  // console.log(table_position);
  // $(this_elm).closest('tr').find('td:eq(4)').text("");

  
  var elm_source_type = $(this_elm).closest('tr').find('td:eq(2)').text();
  if (elm_source_type == "Good"){
    source_type = 1;
  }else{
    source_type = 2;
  }
  console.log(source_type);
  // var elm_id = $(this_elm).closest('tr').find('data-id');
  // console.log(elm_id);
  console.log($('#hide_id'+j).val());
  var elm_id = $('#hide_id'+j).val();
  var elm_item = this_elm.value;
  console.log(elm_item);

  var urlV = '{{ URL::to("/getstockitem") }}';
  // console.log(urlV);
  var typeV = 'GET';

  $.ajax({
      url : urlV,
      data : 'elm_id='+elm_id+'&elm_item='+elm_item+'&source_type='+source_type,
      type : typeV,
      headers : {
        'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
      },
      error : function(jqXHR, textStatus, errorThrown){
          console.log(jqXHR.responseText);
      },
      success : function(data, textStatus, jqXHR){
        console.log(data);
         $(this_elm).closest('tr').find('td:eq(4)').text(data);

         var stock_production = $(this_elm).closest('tr').find('td:eq(3)').text();
         var stock_storage = $(this_elm).closest('tr').find('td:eq(4)').text();
         var stock_current = stock_production - stock_storage;
         $(this_elm).closest('tr').find('td:eq(5)').text(stock_current);

      }
    });

  //send data to controller [elm_id, elm_item, source_type]
}

function addValue(str, id){
  var prd = id;
  var q_item = $(str).closest('tr').find('td:eq(1)').text();
  var weight = $(str).closest('tr').find('td:eq(4)').text();

  $("#production").val(id);
  // $("#qty").val(qty);
  $("#weight").val(weight);
  $('#myModal').modal("hide");
  $('#myModal2').modal('show');
  console.log(id);
  console.log(q_item);

  // var fooURL = '/getcartondetail/'+id;

  // console.log(fooURL);

  var urlV = '{{ URL::to("/getcartondetail") }}';
  // console.log(urlV);
  var typeV = 'GET';

  $.ajax({
      url : urlV,
      data : 'id='+prd+'&q_item='+q_item,
      type : typeV,
      headers : {
        'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
      },
      error : function(jqXHR, textStatus, errorThrown){
          console.log(jqXHR.responseText);
      },
      success : function(data, textStatus, jqXHR){
          //console.log(jqXHR.responseText);
          console.log(data);
          var returnedData = JSON.parse(data);
          // console.log(returnedData[0]['data_detail']);
          var data_detail = returnedData[0][0]['data_detail'];

          //save name in hidden element
          var name_product = returnedData[0][0]['product'];

          var option_value = returnedData[1];
          console.log(option_value);

          var elm_option = "";


          for (var n = option_value.length - 1; n >= 0; n--) {
            elm_option += '<option value="'+option_value[n]['id']+'">'+option_value[n]['item_name']+'</option>'
          }
          console.log(elm_option);

          var elm = "";
          elm +="<input type='hidden' name='name_product' id='name_product' value='"+name_product+"'>";
          for (var j = 0; j < data_detail.length; j++) {
            console.log(data_detail[j]['carton']);
            console.log(data_detail[j]['size']);
            // var countCarton = data_detail[j]['carton'];
            var countCarton = data_detail[j]['block'];
            var elmDisabeled = "";
            if(countCarton < 1){
              elmDisabeled = "disabled='true'";
            }

            var srctype_val = 0;
            if(data_detail[j]['condition'] == "Good"){
              srctype_val = 1;
            }else{
              srctype_val = 2;
            }

            var no = j + 1;
            elm += "<tr>";
            elm += '<td><input type="hidden" name="srctype'+j+'" id="srctype'+j+'" value="'+srctype_val+'"><input type="hidden" name="hide_id'+j+'" id="hide_id'+j+'" value="'+data_detail[j]['id']+'">'+no+'</td>';
            elm += "<td id='detail_size"+j+"'>"+data_detail[j]['size']+"</td>";
            elm += "<td id='detail_condition"+j+"'>"+data_detail[j]['condition']+"</td>";
            // elm += '<td>'+data_detail[j]['carton']+"</td>";
            elm += '<td>'+data_detail[j]['block']+"</td>";
            elm += '<td>-</td>';
            elm += '<td>-</td>';
            elm += '<td><select id="elm_select'+j+'" name="item" onchange="get_stock(this, '+j+')" '+elmDisabeled+'><option>'+elm_option+'</option></select></td>';
            elm += '<td><input type="number" name="storeval'+j+'" id="storeval'+j+'" onkeyup="validval(this, '+j+')" '+elmDisabeled+'></td>';
            elm += "</tr>"
          }

          // var production_id = returnedData[0][0]['production_id'];
          // var prd = returnedData[0][0]['production_result_detail'];
          // var pra = returnedData[0][0]['production_result_addition'];
          // var temps = returnedData[1];
          // console.log(temps);
          // var elm = "";
          // var counter_id=0;
          // var numberElm = 0;
          // for (var i = 0; i < prd.length; i++) {

          //   var no = i + 1;
          //   elm += "<tr>";
          //   elm += '<td>'+no+'<input type="hidden" name="srctype'+i+'" id="srctype'+i+'" value="1"><input type="hidden" name="pidval'+i+'" id="pidval'+i+'" value="'+production_id+'"><input type="hidden" name="idval'+i+'" id="idval'+i+'" value="'+prd[i]['id']+'"></td>';
          //   elm += "<td id='production_size_"+i+"' >"+prd[i]['size']+"</td>";
          //   elm += '<td>'+prd[i]['block']+" lorem</td>";
          //   var testv = 0;
          //   var stok = 0;

          //   for (var z = 0; z < temps.length; z++) {
          //     if(temps[z]['source_type'] == 1){
          //       // console.log(temps[z]['source_id']);
          //       // console.log("++");
          //       // console.log(prd[i]['id']);
          //       if(temps[z]['source_id'] == prd[i]['id']){
          //         console.log("masuk");
          //         testv = testv + temps[z]['quantity'];
          //         // console.log(prd[i]['block']);
          //         // console.log(temps[z]['quantity']);
          //       }
          //     }
          //   }
          //   stok = prd[i]['block'] - testv;
          //   console.log("---");
          //   console.log(testv);
          //   console.log("---");
          //   elm += '<td>'+testv+'<input type="hidden" name="blockval'+i+'" id="blockval'+i+'" value="'+stok+'"></td>';
          //   elm += '<td>'+stok+'</td>';
          //   elm += '<td><input type="number" name="storeval'+i+'" id="storeval'+i+'" onkeyup="validval('+i+')"></td>';
          //   elm += "</tr>";
          //   numberElm = i;
          // }
          // console.log('test');
          // console.log(numberElm);
          // for (var i = 0; i < pra.length; i++) {
          //   var nov = i +1;
          //   no = nov + no;
          //   var noel = numberElm + nov;
          //   elm += "<tr>";
          //   elm += '<td>'+no+'<input type="hidden" name="srctype'+noel+'" id="srctype'+noel+'" value="2"><input type="hidden" name="pidval'+noel+'" id="pidval'+noel+'" value="'+production_id+'"><input type="hidden" name="idval'+noel+'" id="idval'+noel+'" value="'+pra[i]['id']+'"></td>';
          //   elm += "<td id='production_size_"+noel+"' >"+pra[i]['size']+"</td>";
          //   elm += '<td>'+pra[i]['block']+"</td>";
          //   var testv = 0;
          //   var stok = 0;
          //   for (var z = 0; z < temps.length; z++) {
          //     if(temps[z]['source_type'] == 2){
          //       // console.log(temps[z]['source_id']);
          //       // console.log("++");
          //       // console.log(prd[i]['id']);
          //       if(temps[z]['source_id'] == pra[i]['id']){
          //         console.log("masuk");
          //         testv = testv + temps[z]['quantity'];
          //         // console.log(prd[i]['block']);
          //         // console.log(temps[z]['quantity']);
          //       }
          //     }
          //   }
          //   stok = pra[i]['block'] - testv;
          //   console.log("---");
          //   console.log(testv);
          //   console.log("---");
          //   elm += '<td>'+testv+'<input type="hidden" name="blockval'+noel+'" id="blockval'+noel+'" value="'+stok+'"></td>';
          //   elm += '<td>'+stok+'</td>';
          //   elm += '<td><input type="number" name="storeval'+noel+'" id="storeval'+noel+'" onkeyup="validval('+noel+')"></td>';
          //   elm += "</tr>";
          // }
          console.log(elm);
          $("#elmTable").html(elm);
           //window.location.replace('{{ URL::to("/getSizeBlock") }}');
      }
  });
}

$(document).ready(function(){





  $("#storeSubmit").click(function(){
    var cRow = $('#elmTable').find('tr').length;
    console.log("row");
    console.log(cRow);

    var sendVals = [];
    var cartHTML="";
    for (var x = 0; x < cRow; x++) {


      console.log($("#storeval"+x).val());
      if($("#storeval"+x).val() != ""){
        console.log("===");
        console.log(x);
        var sendVal = {};
        var product_name = $("#name_product").val();

        var size = $("#detail_size"+x).text();
        var condition_view = $("#detail_condition"+x).text();
        // console.log(size);
        var store_value = $("#storeval"+x).val();

        // sendVal['srctype'] = $("#srctype"+x).val();
        // sendVal['pidval'] = $("#pidval"+x).val();
        // sendVal['idval'] = $("#idval"+x).val();
        // sendVal['storeval'] = store_value;

        //new store array to transaction storage
        sendVal['production_id'] = $("#production").val();
        sendVal['source_type'] = $("#srctype"+x).val();
        sendVal['source_id'] = $("#hide_id"+x).val();
        sendVal['id_item'] = $("#elm_select"+x).val();;
        sendVal['quantity'] = store_value;

        // console.log("MASUK");
        console.log(sendVal);
        sendVals.push(sendVal);


        cartHTML+="<tr>";
        cartHTML+="<td>"+product_name+"</td>";
        cartHTML+="<td>"+condition_view+"</td>"
        cartHTML+="<td>"+size+"</td>";
        cartHTML+="<td>"+store_value+"</td>";
        cartHTML+="</tr>";

        // console.log("=====");
        // console.log($("#srctype"+i).val())
        // console.log($("#pidval"+i).val());
        // console.log($("#idval"+i).val());
        // console.log($("#storeval"+i).val());
        // console.log(sendVal);
        // sendVal="";
      }

    }
    $("#cart").html(cartHTML);
    console.log(sendVals);
    $("#valArySend").val(JSON.stringify(sendVals));
    $('#unitSelect option[value=Block]').attr('selected','selected');

    $("#myModal2").modal('hide');
  });

  $("#lookup").click(function(){
    // var fRow = $('#elmTableFirst').find('tr').length;
    // // console.log(fRow);
    // for (var i = 0; i < fRow; i++) {
    //   var qtyjs = parseInt($("#elmTableFirst tr").eq(i).find('td:eq(3)').text());
    //   console.log(qtyjs);
    //   if(qtyjs < 1){
    //     $("#elmTableFirst tr").eq(i).hide();
    //   }
    //   // console.log("test");
    // }

    $('#myModal').modal();
    // $("#productionResult").DataTable({
    //   "pageLength": 5
    // });
  });


  $('#add_placement').click(function() {
    $('#myModalStorage').modal();
  });

  $('#datetime').datetimepicker({
		sideBySide: true,
		format: 'YYYY-MM-DD HH:mm',
	});
});
</script>


<script>
  $(function() {
    var table = $("#productionResult").DataTable({
      processing: true,
      serverSide: true,
      "pageLength": 5,
      "order": [[ 5, "desc" ]],
      // ajax: "{{ url('editor/storage/data') }}",
       ajax: "{{ url('/getcarton') }}",
      columns: [
      { data: 'id', name: 'id' },
      { data: 'product', name: 'product' },
      { data: 'start_date', name: 'start_date' },
      { data: 'finish_date', name: 'finish_date' },
      { data: 'weight', name: 'weight' },
      { data: 'block_weight', name: 'block_weight' },
      { data: 'total_block', name: 'total_block' },
      { data: 'mst_carton', name: 'mst_carton' },
      { data: 'total_carton', name: 'total_carton' },
      { data: 'action', 'searchable': false, 'orderable':false }
      ]
    });
  });
</script>
@stop
