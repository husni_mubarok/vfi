@extends('layouts.home.template')
@section('content')
<!-- Content Header (Page header) -->
<div class="header_menu mobile-hide">
  @foreach($ds as $unpack_key => $unpack_ds)
    <a href="{{ URL::to('editor/storage/show/'.$unpack_key) }}">{{ $unpack_ds }}</a> |
  @endforeach
</div>
@actionStart('storage', 'read')
<section class="content">
	<!-- <section class="content box box-solid"> -->
		<div class="row">
		    <!-- <div class="col-md-12"> -->
		    	<div class="col-md-12">
			        <div class="x_panel">
		                <h2>
		                	<i class="fa fa-dropbox"></i> Storage List
	                	</h2>
		                <hr>
			            <div class="x_content">

                    <a href="{{ URL::route('editor.storage.create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add</a>
<br><br>

              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li><a href="#tab_1" data-toggle="tab" id="firstTab"><b>Storage</b></a></li>
                  <li class="active"><a href="#tab_2" data-toggle="tab" id="secondTab"><b>Storage History</b></a></li>
                  <li><a href="#tab_3" data-toggle="tab" id="thirdTab"><b>Auto Store To Storage</b></a></li>
                </ul>
                <div class="tab-content">
                  <div class="tab-pane" id="tab_1">
                    <!-- <h3>Advance Search</h3>
                    <hr>
                    <table>
                      <tr id="filter_col1" data-column="2">
                        <td>Name : </td>
                        <td><input type="text" name="nameST" class="column_filter" id="col2_filter"></td>
                      </tr>
                      <tr id="filter_col2" data-column="3">
                        <td>Size : </td>
                        <td><input type="text" name="sizeST" class="column_filter" id="col3_filter"></td>
                      </tr>
                      <tr id="filter_col3" data-column="4">
                        <td>Block : </td>
                        <td><input type="text" name="blockST" class="column_filter" id="col4_filter"></td>
                      </tr>
                      <tr id="filter_col4" data-column="5">
                        <td>Condition : </td>
                        <td><input type="text" name="conditionST" class="column_filter" id="col5_filter"></td>
                      </tr>
                      <tr>
                        <td>Storage Date Time : </td>
                        <td><input type="text" name="date_startSTv" id="date_startSTv"> s/d <input type="text" name="date_finishSTv" id="date_finishSTv"></td>
                      </tr>
                      <tr>
                        <td>Start Production : </td>
                        <td><input type="text" name="date_productionSv" id="date_productionSv"></td>
                      </tr>
                      <tr>
                        <td>Finish Production : </td>
                        <td><input type="text" name="date_productionFv" id="date_productionFv"></td>
                      </tr>
                    </table>
                    <a href="javascript:void(0)" class="btn btn-success" id="searchTablev">Search</a>
                    <hr> -->
                    <table id="storageTable" class="table table-striped dataTable" style="width: 100%" width="100%">
                <thead>
                    <tr>
                      <th>#</th>
                      <th>Address</th>
                      <th>Item</th>
                      <th>Entity</th>
                      <th>Entity Value</th>
                      <th>Total</th>
                      <th>Action</th>
                  </tr>
                </thead>

              </table>

                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane active" id="tab_2">
                    <!-- <h3>Advance Search</h3>
                    <hr>
                    <table>
                      <tr id="filter_col1" data-column="2">
                        <td>Name : </td>
                        <td><input type="text" name="nameSTval" class="column_filter" id="col2_filter"></td>
                      </tr>
                      <tr id="filter_col2" data-column="3">
                        <td>Size : </td>
                        <td><input type="text" name="sizeSTval" class="column_filter" id="col3_filter"></td>
                      </tr>
                      <tr id="filter_col3" data-column="4">
                        <td>Block : </td>
                        <td><input type="text" name="blockSTval" class="column_filter" id="col4_filter"></td>
                      </tr>
                      <tr id="filter_col4" data-column="5">
                        <td>Condition : </td>
                        <td><input type="text" name="conditionSTval" class="column_filter" id="col5_filter"></td>
                      </tr>
                      <tr>
                        <td>Storage Date Time : </td>
                        <td><input type="text" name="date_startST" id="date_startST"> s/d <input type="text" name="date_finishST" id="date_finishST"></td>
                      </tr>
                      <tr>
                        <td>Start Production : </td>
                        <td><input type="text" name="date_productionS" id="date_productionS"></td>
                      </tr>
                      <tr>
                        <td>Finish Production : </td>
                        <td><input type="text" name="date_productionF" id="date_productionF"></td>
                      </tr>
                    </table>
                    <br>
                    <a href="#" class="btn btn-success" id="searchTableDetail">Search</a>
                    <hr> -->
                    <table id="storageDetailTable" class="table table-striped dataTable" style="width: 100%" width="100%">
                <thead>
                    <tr>
                      <th>#</th>
                      <th>DOC CODE</th>
                      <th>Address</th>
                      <th>Date Time Store</th>
                      <th>Status</th>
                      <th>Item</th>
                      <th>Weight Production (Kg)</th>
                      <th>Entity</th>
                      <th>Entity Value</th>
                      <th>Quantity</th>
                      <th>Unit</th>
                      <th>#</th>
                      <th>diff</th>
                      <!-- <th>Action</th> -->
                  </tr>
                </thead>

            </table>
          </div>


          <div class="tab-pane" id="tab_3">
          <table id="autoStorageTable" class="table table-striped dataTable" style="width: 100%" width="100%">
                <thead>
                    <tr>
                      <th>#</th>
                      <th>DOC CODE</th>
                      <th>Address</th>
                      <th>Date Store</th>
                      <th>Status</th>
                      <th>Item</th>
                      <th>Weight Production (Kg)</th>
                      <!-- <th>Entity</th>
                      <th>Entity Value</th> -->
                      <th>Quantity</th>
                      <th>Unit</th>
                      <th>#</th>
                      <th>Approve</th>
                  </tr>
                </thead>

            </table>
          </div>



                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div>
			            </div>
			        </div>
		        </div>
		    <!-- </div> -->
		</div>
	<!-- </section> -->
</section>
<div class="modal fade" id="myModalMoveRev" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Move Storage</h4>
      </div>
      <div class="modal-body">
        <div class="panel panel-primary">
          <div class="panel-heading">Detail</div>
          <div class="panel-body">
            <table class="table table-bordered">
              <tr>
                <td>Address Storege</td>
                <td>:</td>
                <td><p id="text_address"></p></td>
              </tr>
              <tr>
                <td>Item</td>
                <td>:</td>
                <td><p id="text_item"></p></td>
              </tr>
              <tr>
                <td>Total</td>
                <td>:</td>
                <td><p id="text_total"></p></td>
              </tr>
            </table>
          </div>
        </div>
        <h4>Form Move Item</h4>
        <hr>
        <form class="form-horizontal">
          <input type="hidden" name="id_transaction_storage" id="id_transaction_storage">
          <input type="hidden" name="id_item" id="id_item">
          <input type="hidden" name="id_placement_storage" id="id_placement_storage">
          
          <div class="form-group">
            {{ Form::label('placement_storage', 'Placement Storage', ['class'=>'col-sm-3 control-label']) }}
            <div class="col-sm-4">
              {{ Form::select('placement_storage', $ds, old('placement_storage'), ['placeholder' => 'Select a Type', 'class' => 'form-control', 'required' => 'true', 'id'=>'inputPlacement']) }}
            </div>
          </div>
          <div class="form-group">
            <label for="inputPassword3" class="col-sm-3 control-label">Quantity</label>
            <div class="col-sm-4">
              <input type="text" class="form-control" id="inputQuantityCarton" placeholder="Quantity" value="0">
            </div>
            <div class="col-sm-3">
              <select class="form-control" id="inputUnit">
                <option value="Carton">Carton</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="inputPassword3" class="col-sm-3 control-label">&nbsp;</label>
            <div class="col-sm-4">
              <input type="text" class="form-control" id="inputQuantity" placeholder="Quantity" value="0">
            </div>
            <div class="col-sm-3">
              <select class="form-control" id="inputUnit">
                <option value="Block">Block</option>
              </select>
            </div>
          </div>
        </form>
       <!--  <p> Name Storage : <input type="text" id="name_storage"></p>
        <p> QTY : <textarea id="description_storage"></textarea></p>
        <p> <a href="#" id="submit_storage" class="btn btn-primary">submit</a></p> -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="actionMoveRev">SUBMIT</button>
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="myModalAdded" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Update Stock</h4>
      </div>
      <div class="modal-body">
        <div class="panel panel-primary">
          <div class="panel-heading">Detail</div>
          <div class="panel-body">
            <table class="table table-bordered">
              <tr>
                <td>Address Storege</td>
                <td>:</td>
                <td><p id="text_address_x"></p></td>
              </tr>
              <tr>
                <td>Item</td>
                <td>:</td>
                <td><p id="text_item_x"></p></td>
              </tr>
              <tr>
                <td>Production Quantity</td>
                <td>:</td>
                <td><p id="text_production_qty"></p></td>
              </tr>
              <tr>
                <td>Storage Quantity</td>
                <td>:</td>
                <td><p id="text_storage_qty"></p></td>
              </tr>
              <tr>
                <td>Missing Quantity</td>
                <td>:</td>
                <td><p id="text_missing_qty"></p></td>
              </tr>
            </table>
          </div>
        </div>
        <h4>Form Update Quantity Item</h4>
        <hr>
        <form class="form-horizontal">
          <input type="hidden" name="id_transaction_storagex" id="id_transaction_storagex">
          
          <div class="form-group">
            <label for="inputPassword3" class="col-sm-3 control-label">Quantity Storage</label>
            <div class="col-sm-6">
              <input type="text" class="form-control" id="inputQuantityx" placeholder="Quantity" >
            </div>
          </div>
          
        </form>
       <!--  <p> Name Storage : <input type="text" id="name_storage"></p>
        <p> QTY : <textarea id="description_storage"></textarea></p>
        <p> <a href="#" id="submit_storage" class="btn btn-primary">submit</a></p> -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="actionUpdateStock">SUBMIT</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="myModalApprove" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Store Data to Storage Approvel</h4>
      </div>
      <div class="modal-body">
        <div class="panel panel-primary">
          <div class="panel-heading">Detail</div>
          <div class="panel-body">
            <table class="table table-bordered">
              <tr>
                <td>Address Storege</td>
                <td>:</td>
                <td><p id="text_address_y"></p></td>
              </tr>
              <tr>
                <td>Item</td>
                <td>:</td>
                <td><p id="text_item_y"></p></td>
              </tr>
              <tr>
                <td>Production Quantity</td>
                <td>:</td>
                <td><p id="text_production_qty_y"></p></td>
              </tr>
              <tr>
                <td>Store Quantity</td>
                <td>:</td>
                <td><p id="text_store_qty_y"></p></td>
              </tr>
            </table>
            <input type="hidden" name="qtyApprv" id="qtyApprv">
            <input type="hidden" name="id_transaction_storageApprv" id="id_transaction_storageApprv">
          </div>
        </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="actionApprove">SUBMIT</button>
      </div>
    </div>
  </div>
</div>

<!-- <div class="modal fade" id="myModalMove" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Move Storage</h4>
      </div>
      <div class="modal-body">
        <div class="panel panel-primary">
          <div class="panel-heading">Detail</div>
          <div class="panel-body">
            <table class="table table-bordered">
              <tr>
                <td>Address Storege</td>
                <td>:</td>
                <td><p id="text_asddress"></p></td>
              </tr>
              <tr>
                <td>Item</td>
                <td>:</td>
                <td><p id="text_item"></p></td>
              </tr>
              <tr>
                <td>Total</td>
                <td>:</td>
                <td><p id="text_total"></p></td>
              </tr>
            </table>
          </div>
        </div>
        <h4>Form Move Item</h4>
        <hr>
        <form class="form-horizontal">
          <input type="hidden" name="id_trans_storage" id="id_trans_storage">
          <input type="hidden" name="id_item" id="id_item">
          <input type="hidden" name="source_type" id="source_type">
          <input type="hidden" name="source_id" id="source_id">
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Placement Storege</label>
            <div class="col-sm-4">
              <select class="form-control" id="inputPlacement">
                <option value="99">MIGRATION STORAGE</option>
                <option value="100">STORAGE A</option>
                <option value="101">STORAGE B</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="inputPassword3" class="col-sm-3 control-label">Quantity</label>
            <div class="col-sm-4">
              <input type="text" class="form-control" id="inputQuantity" placeholder="Quantity">
            </div>
            <div class="col-sm-3">
              <select class="form-control" id="inputUnit">
                <option value="Block">Block</option>
              </select>
            </div>
          </div>
        </form>
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="actionMove">SUBMIT</button>
      </div>
    </div>
  </div>
</div> -->
@actionEnd
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script>
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
function addedItem(element, id_transaction_storage, production_qty){
  console.log(element);
  var text_address = $(element).closest('tr').find('td:eq(2)').text();
  var text_item = $(element).closest('tr').find('td:eq(5)').text();
  var text_production_qty = production_qty;
  var text_storage_qty = $(element).closest('tr').find('td:eq(9)').text();
  var text_missing_qty = parseInt(text_production_qty) - parseInt(text_storage_qty);
  $("#text_address_x").html(text_address);
  $("#text_item_x").html(text_item);
  $("#text_production_qty").html(text_production_qty);
  $("#text_storage_qty").html(text_storage_qty);
  $("#text_missing_qty").html(text_missing_qty);
  $("#inputQuantityx").val(text_storage_qty);
  $("#id_transaction_storagex").val(id_transaction_storage);
  $('#myModalAdded').modal('show');
}


// function move storege
function moveStorege(id, id_item, source_type, source_id, element){
  console.log("move");

  $("#id_trans_storage").val(id);
  $("#id_item").val(id_item);
  $("#source_type").val(source_type);
  $("#source_id").val(source_id);
  
  var text_asddress = $(element).closest('tr').find('td:eq(2)').text();
  var text_item = $(element).closest('tr').find('td:eq(5)').text();
  var text_quantity = $(element).closest('tr').find('td:eq(8)').text();
  var text_unit = $(element).closest('tr').find('td:eq(9)').text();
  $("#text_asddress").html(text_asddress);
  $("#text_item").html(text_item);
  $("#text_total").html(text_quantity+" "+text_unit);
  console.log(text_item);
  $('#myModalMove').modal('show');
}

// Revision 
function moveStorageRev(id_item, id_placement_storage, id_transaction_storage, element){
  console.log(id_item);
  console.log(id_placement_storage);
  console.log(id_transaction_storage);

  var text_address = $(element).closest('tr').find('td:eq(1)').text();
  var text_item = $(element).closest('tr').find('td:eq(2)').text();
  var text_total = $(element).closest('tr').find('td:eq(5)').text();


  $("#id_transaction_storage").val(id_transaction_storage);
  $("#id_item").val(id_item);
  $("#id_placement_storage").val(id_placement_storage);

  $("#text_address").html(text_address);
  $("#text_item").html(text_item);
  $("#text_total").html(text_total);

  $('#myModalMoveRev').modal('show');

}



function format(d){
    // return 'Date Start Production : '+d.started_at+'<br>'+
    //     'Date Finish Production : '+d.finished_at+'<br>';

    return 'Data Detail ...';
}

function filterColumn(i) {
    $('#storageDetailTable').DataTable().column(i).search(
        $('#col'+i+'_filter').val()
    ).draw();
  }

function btnApprove(element, id_transaction_storage, production_qty){
  var qty = $(element).closest('tr').find('td:eq(9)').find('input').val();

  var text_address = $(element).closest('tr').find('td:eq(2)').text();
  var text_item = $(element).closest('tr').find('td:eq(5)').text();
  var text_production_qty = production_qty;

  $("#qtyApprv").val(qty);
  $("#id_transaction_storageApprv").val(id_transaction_storage);

  $("#text_address_y").html(text_address);
  $("#text_item_y").html(text_item);
  $("#text_production_qty_y").html(text_production_qty);
  $("#text_store_qty_y").html(qty);

  console.log(qty);
  console.log(id_transaction_storage);

  
  $("#myModalApprove").modal('show');

      
}
//   $.fn.dataTable.ext.search.push(
//     function( settings, data, dataIndex ) {
//         var min = $('#date_startST').val();
//         var max = $('#date_finishST').val();
//         var dtv = data[6]; // use data for the age column
//
//         if ( ( isNaN( min ) && isNaN( max ) ) ||
//              ( isNaN( min ) && dtv <= max ) ||
//              ( min <= dtv   && isNaN( max ) ) ||
//              ( min <= dtv   && dtv <= max ) )
//         {
//             return true;
//         }
//         return false;
//     }
// )




  $(document).ready(function () {
    $("#actionApprove").click(function(){
      
      console.log("approved");
     
      var qty = $("#qtyApprv").val();
      var id_transaction_storage = $("#id_transaction_storageApprv").val(); 


      console.log(qty);
      console.log(id_transaction_storage);

      var urlV = '{{ URL::to("/editor/storage/approve") }}';
      var typeV = 'POST';

      
      $.ajax({
          url : urlV,
          data : 'qty='+qty+'&id_transaction_storage='+id_transaction_storage,
          type : typeV,
          // headers : {
          //   'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
          // },
          error : function(jqXHR, textStatus, errorThrown){
              console.log(jqXHR.responseText);

          },
          success : function(data, textStatus, jqXHR){
            console.log(data);
            if(data == "TRUE"){
              location.reload();
            }else{
              alert("Approve Fialed !");
              location.reload();
            }

          }
      });
    });

    $("#actionUpdateStock").click(function(){
      console.log("updateStock");

      var id_transaction_storage = $("#id_transaction_storagex").val();
      var uQty = $("#inputQuantityx").val();
      var urlV = '{{ URL::to("/updatestorage") }}';
      var typeV = 'POST';

      $.ajax({
          url : urlV,
          data : 'id_transaction_storage='+id_transaction_storage+'&uQty='+uQty,
          type : typeV,
          // headers : {
          //   'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
          // },
          error : function(jqXHR, textStatus, errorThrown){
              console.log(jqXHR.responseText);

          },
          success : function(data, textStatus, jqXHR){
            console.log(data);
            if(data == "TRUE"){
              alert("Data Storage Updated")
              location.reload();
            }else{
              alert("Update Fialed !");
              location.reload();
            }

          }
      });
    });

    $("#actionMove").click(function(){
      var oldStorageID = $("#id_trans_storage").val();
      var inputPlacement = $("#inputPlacement").val();
      var inputQuantity = $("#inputQuantity").val();
      var inputUnit = $("#inputUnit").val();
      var id_item = $("#id_item").val();
      var source_type = $("#source_type").val();
      var source_id = $("#source_id").val();

      var urlV = '{{ URL::to("/movestorage") }}';
      var typeV = 'POST';

      $.ajax({
          url : urlV,
          data : 'oldStorageID='+oldStorageID+'&inputPlacement='+inputPlacement+'&inputQuantity='+inputQuantity+'&inputUnit='+inputUnit+'&id_item='+id_item+'&source_type='+source_type+'&source_id='+source_id,
          type : typeV,
          // headers : {
          //   'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
          // },
          error : function(jqXHR, textStatus, errorThrown){
              console.log(jqXHR.responseText);

          },
          success : function(data, textStatus, jqXHR){
            console.log(data);
            if(data == "TRUE"){
              location.reload();
            }else{
              alert("Move Fialed !");
              location.reload();
            }

          }
      });


      // console.log(oldStorageID);
    });

    $("#actionMoveRev").click(function(){
      var inputQuantityRaw = $("#inputQuantity").val();
      if (inputQuantityRaw == "") {
        inputQuantityRaw = 0;
      }

      var inputQuantityCartonRaw = $("#inputQuantityCarton").val();
      if (inputQuantityCartonRaw == "") {
        inputQuantityCartonRaw = 0;
      }

      var calcCartonBlock = inputQuantityCartonRaw * 6;
      
      var realQuantity = calcCartonBlock + parseInt(inputQuantityRaw);
      console.log(realQuantity);

      var inputPlacement = $("#inputPlacement").val();
      var inputQuantity = realQuantity;
      var inputUnit = "Block";
      var id_item = $("#id_item").val();
      var id_placement_storage = $("#id_placement_storage").val();
      var id_transaction_storage = $("#id_transaction_storage").val();

      var urlV = '{{ URL::to("/movestoragerev") }}';
      var typeV = 'POST';

      $.ajax({
          url : urlV,
          data : 'inputPlacement='+inputPlacement+'&inputQuantity='+inputQuantity+'&inputUnit='+inputUnit+'&id_item='+id_item+'&id_placement_storage='+id_placement_storage+'&id_transaction_storage='+id_transaction_storage,
          type : typeV,
          // headers : {
          //   'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
          // },
          error : function(jqXHR, textStatus, errorThrown){
              console.log(jqXHR.responseText);

          },
          success : function(data, textStatus, jqXHR){
            console.log(data);
            if(data == "TRUE"){
              location.reload();
            }else{
              alert("Move Fialed !");
              location.reload();
            }

          }
      });
    });


    var autoStorageTable = $('#autoStorageTable').DataTable( {
       processing: true,
       serverSide: true,
       ajax: {
             url: '{{ url('editor/storage/dataautostore') }}',
             data: function (d) {
                 // d.name = $('input[name=nameSTval]').val();
                 // d.size = $('input[name=sizeSTval]').val();
                 // d.block = $('input[name=blockSTval]').val();
                 // d.reson = $('input[name=conditionSTval]').val();
                 // d.datess = $('input[name=date_startST]').val();
                 // d.datefs = $('input[name=date_finishST]').val();
                 // d.datesp = $('input[name=date_productionS]').val();
                 // d.datefp = $('input[name=date_productionF]').val();
                 d.placement_storage = '@if(isset($params)){{ $params }}@endif';
             }
         },
         rowCallback: function(row, data, index) {

          if (data['status_production']<='3') {
            $(row).attr('style', 'background-color:#f2dede');
          }
        },
         "columns": [
           {
               "class":          "details-control",
               "orderable":      false,
               "data":           null,
               "defaultContent": ""
           },
           { "data": "doc_code" },
           { "data": "address" },
           { "data": "DATETIME" },
           { "data": "status" },
           { "data": "name" },
           { "data": "weight_production" },
           // { "data": "entity" },
           // { "data": "entity_value" },
           { "data": "quantity"},
           { "data": "unit"},
           { "data": "action" },
           { "data": "approve" },
       ],
       "order": [[1, 'asc']],
       "pageLength" : 10,
       "scrollY" : "300px",
       // "sScrollX": "100%",
      "scrollX": true,
      "responsive": true,
"bAutoWidth": true
    } );

    // $("#autoStorageTable").click(function(){
    //   console.log("tes");
    //   autoStorageTable.draw();
    //    // e.preventDefault();
    // });

    var detailAutoStorageTable = [];

  $('#autoStorageTable tbody').on( 'click', 'tr td.details-control', function () {
      var tr = $(this).closest('tr');
      var row = detailAutoStorageTable.row( tr );
      var idx = $.inArray( tr.attr('id'), detailAutoStorageTable);

      if ( row.child.isShown() ) {
          tr.removeClass( 'details' );
          row.child.hide();

          // Remove from the 'open' array
          detailAutoStorageTable.splice( idx, 1 );
      }
      else {
          tr.addClass( 'details' );
          row.child( format( row.data() ) ).show();

          // Add to the 'open' array
          if ( idx === -1 ) {
              detailAutoStorageTable.push( tr.attr('id') );
          }
      }
  } );

  // On each draw, loop over the `detailRows` array and show any child rows
  autoStorageTable.on('draw', function(){
    $.each(detailAutoStorageTable, function ( i, id ) {
        $('#'+id+' td.details-control').trigger( 'click' );
    });
 });


    var dtcSearch = $('#storageDetailTable').DataTable( {
       processing: true,
       serverSide: true,
       ajax: {
             url: '{{ url('editor/storage/datasearch') }}',
             data: function (d) {
                 d.name = $('input[name=nameSTval]').val();
                 d.size = $('input[name=sizeSTval]').val();
                 d.block = $('input[name=blockSTval]').val();
                 d.reson = $('input[name=conditionSTval]').val();
                 d.datess = $('input[name=date_startST]').val();
                 d.datefs = $('input[name=date_finishST]').val();
                 d.datesp = $('input[name=date_productionS]').val();
                 d.datefp = $('input[name=date_productionF]').val();
                 d.placement_storage = '@if(isset($params)){{ $params }}@endif';
             }
         },
         rowCallback: function(row, data, index) {
          // console.log(data);
          var doc_code = data['doc_code'];
          var u_doc_code = doc_code.split("_"); 


          // Todo Now add Flag from not same qty
          if (data['quantity']<data['production_qty'] && u_doc_code[0] != "MOVE") {
            $(row).attr('style', 'background-color:#fdffad');
            $(row).find('td:eq(10)').html('<a href="javascript:void(0)" class="btn btn-primary" onclick="addedItem(this,'+data['id_transaction_storage']+', '+data['production_qty']+')">Change</a>');
            // console.log(data['diff_qty']);
            
            // console.log(u_doc_code[0]);   
          }  
        },
         "columns": [
           {
               "class":          "details-control",
               "orderable":      false,
               "data":           null,
               "defaultContent": ""
           },
           { "data": "doc_code" },
           { "data": "address" },
           { "data": "DATETIME" },
           { "data": "status" },
           { "data": "name" },
           { "data": "weight_production" },
           { "data": "entity" },
           { "data": "entity_value" },
           { "data": "quantity"},
           { "data": "unit"},
           {
               "defaultContent": "",
           },
           { "data": "diff_qty", "visible" : false },
       ],
       "order": [[12, 'desc']],
       "pageLength" : 10,
       "scrollY" : "300px",
       // "sScrollX": "100%",
      "scrollX": true,
      "responsive": true,
"bAutoWidth": true
    } );

    // $("#searchTableDetail").click(function(){
    //   console.log("tes");
    //   dtcSearch.draw();
    //    // e.preventDefault();
    // });

    // $('input.column_filter').on( 'keyup', function () {
    //     console.log($(this).parents('tr').attr('data-column'));
    //     filterColumn($(this).parents('tr').attr('data-column'));
    // });

    var dt = $('#storageTable').DataTable( {
       processing: true,
       serverSide: true,
       ajax: {
             url: '{{ url('editor/storage/datasearch_history') }}',
             data: function (d) {
                 d.name = $('input[name=nameST]').val();
                 d.size = $('input[name=sizeST]').val();
                 d.block = $('input[name=blockST]').val();
                 d.reson = $('input[name=conditionSTv]').val();
                 d.datess = $('input[name=date_startSTv]').val();
                 d.datefs = $('input[name=date_finishSTv]').val();
                 d.datesp = $('input[name=date_productionSv]').val();
                 d.datefp = $('input[name=date_productionFv]').val();
                 d.placement_storage = '@if(isset($params)){{ $params }}@endif';
             }
         },
         "columns": [
           {
               "class":          "details-control",
               "orderable":      false,
               "data":           null,
               "defaultContent": ""
           },
           { "data": "address" },
           { "data": "name" },
           { "data": "entity" },
           { "data": "entity_value" },
           { "data": "rs_concat" },
           { "data": "action" },
       ],
       "order": [[1, 'asc']],
       "pageLength" : 10,
       "scrollY" : "300px",
       // "sScrollX": "100%",
    "scrollX": true,
    "responsive": true,
"bAutoWidth": true
    } );

    // $("#searchTablev").click(function(){
    //   console.log("tes");
    //   dt.draw();
    //    // e.preventDefault();
    // });

  //   var dt = $('#storageTable').DataTable( {
  //      "processing": true,
  //      "serverSide": true,
  //      "ajax": "{{ url('editor/storage/dataindex/1') }}",
  //      "columns": [
  //          {
  //              "class":          "details-control",
  //              "orderable":      false,
  //              "data":           null,
  //              "defaultContent": ""
  //          },
  //          { "data": "nameProduct" },
  //          { "data": "type" },
  //          { "data": "size" },
  //          { "data": "stokin" },
  //          { "data": "stokout_export" },
  //          { "data": "stokout_valueadded" },
  //          { "data": "stokout_sashimi" },
  //          { "data": "stokcur" }
  //      ],
  //      "order": [[1, 'asc']],
  //      "pageLength" : 10,
  //      "scrollY" : "300px"
  //  } );

   // Array to track the ids of the details displayed rows
   var detailRows = [];

   $('#storageTable tbody').on( 'click', 'tr td.details-control', function () {
       var tr = $(this).closest('tr');
       var row = dt.row( tr );
       var idx = $.inArray( tr.attr('id'), detailRows);

       if ( row.child.isShown() ) {
           tr.removeClass( 'details' );
           row.child.hide();

           // Remove from the 'open' array
           detailRows.splice( idx, 1 );
       }
       else {
           tr.addClass( 'details' );
           row.child( format( row.data() ) ).show();

           // Add to the 'open' array
           if ( idx === -1 ) {
               detailRows.push( tr.attr('id') );
           }
       }
   } );

   // On each draw, loop over the `detailRows` array and show any child rows
   dt.on('draw', function(){
     $.each( detailRows, function ( i, id ) {
         $('#'+id+' td.details-control').trigger( 'click' );
     });
  });


  // var dtc = $('#storageDetailTable').DataTable( {
  //    "processing": true,
  //    "serverSide": true,
  //    "ajax": "{{ url('editor/storage/datasearch') }}",
  //    "columns": [
  //        {
  //            "class":          "details-control",
  //            "orderable":      false,
  //            "data":           null,
  //            "defaultContent": ""
  //        },
  //        { "data": "production_id" },
  //        { "data": "name" },
  //        { "data": "size" },
  //        { "data": "block_weight" },
  //        { "data": "reason" },
  //        { "data": "datetime" },
  //        { "data": "stock_in" },
  //        { "data": "started_at", "visible" : false },
  //        { "data": "finished_at", "visible" : false }
  //    ],
  //    "order": [[1, 'asc']],
  //    "pageLength" : 10,
  //    "scrollY" : "300px"
  // } );

  // Array to track the ids of the details displayed rows
  var detailRowsc = [];

  $('#storageDetailTable tbody').on( 'click', 'tr td.details-control', function () {
      var tr = $(this).closest('tr');
      var row = dtcSearch.row( tr );
      var idx = $.inArray( tr.attr('id'), detailRowsc);

      if ( row.child.isShown() ) {
          tr.removeClass( 'details' );
          row.child.hide();

          // Remove from the 'open' array
          detailRowsc.splice( idx, 1 );
      }
      else {
          tr.addClass( 'details' );
          row.child( format( row.data() ) ).show();

          // Add to the 'open' array
          if ( idx === -1 ) {
              detailRowsc.push( tr.attr('id') );
          }
      }
  } );

  // On each draw, loop over the `detailRows` array and show any child rows
  dtcSearch.on('draw', function(){
    $.each( detailRowsc, function ( i, id ) {
        $('#'+id+' td.details-control').trigger( 'click' );
    });
 });

// var val_name = $('input[name=nameSTval]').val();

 // $("#searchTableDetail").click(function(){
 //   console.log("tes");
 //   dtcSearch.draw();
 //    // e.preventDefault();
 // });


    // $("#storageTable").DataTable(
    // 	{
    //     "pageLength" : 10,
    //     "scrollY" : "400px",
    //     "order": [[ 1, "asc" ]]
    // 	});
    // $("#storageDetailTable").DataTable(
    // 	{
    //     "pageLength" : 10,
    //     "scrollY" : "400px",
    //     "order": [[ 5, "desc" ]]
    // 	});

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      console.log("oke");
      jQuery( jQuery.fn.dataTable.tables( true ) ).DataTable().columns.adjust();
    });

      // $("#firstTab").click(function(){
      //   console.log("testing");
      //   $('#storageTable').DataTable().columns.adjust();
      //   // $('#storageTable').DataTable().responsive.recalc();
      // });

      // $("#secondtTab").click(function(){
      //   console.log("testing");
      //   $('#storageTable').DataTable().columns.adjust().draw();
      // });

      // $("#thirdTab").click(function(){
      //   $('#autoStorageTable').DataTable().columns.adjust().draw();
      // });
    
    });
</script>
@stop
