@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->

<style type="text/css">
	th { font-size: 11px; }
	td { font-size: 11px; }
</style>
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li class="active"><a href="#"><i class="fa fa-bar-chart"></i> Marketing Cost</a></li>
  </ol>
</section>

<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-12">
			        <div class="x_panel">
		                <h2>
		                	<i class="fa fa-bar-chart"></i> Marketing Cost
		                	@actionStart('marketing_cost', 'create')
		                	<a href="{{ URL::route('editor.marketing_cost.create') }}" class="btn btn-default btn-lg pull-right"><i class="fa fa-plus"></i> Add</a>
		                	@actionEnd
	                	</h2>
		                <hr>
			           <div class="x_content">
			                <table id="marketing_costTable" class="table table-striped dataTable">
							  	<thead>
							  	  	<tr>
								      	<th>Doc Code</th>
								      	<th>Date</th>
								      	<th>Marketing</th>
								      	<th>Customer</th> 
								      	<th>Name</th>
								      	<th>Harga Jual</th>
								      	<th>PHL</th>
								      	<th>Handling Fee</th>
								      	<th>Margin</th>
								      	<th>Price</th>
								      	<th>Est. Produksi</th>
								      	<th>Detail</th>
								      	<th>Consumable</th>
								      	<th>Eq</th>
								      	<th>Eq Fry</th>
								      	<th>Action</th>
							    	</tr>
							  	</thead>
							</table>
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
$(function() {
        $('#marketing_costTable').DataTable({

      processing: true,
      serverSide: true,
      "pageLength": 25,
      "scrollY": "400px",
      ajax: "{{ url('editor/marketing_cost/data') }}",
      columns: [
      { data: 'doc_code', name: 'doc_code' },
      { data: 'datetime', name: 'datetime' },
      { data: 'marketing', name: 'marketing' },
      { data: 'customer', name: 'customer' }, 
      { data: 'name', name: 'name' },
      { data: 'sales_price', name: 'sales_price' },
      { data: 'phl', name: 'phl' },
      { data: 'handling_fee', name: 'handling_fee' },
      { data: 'margin', name: 'margin' },
      { data: 'price', name: 'price' },
      { data: 'estimated_qty', name: 'estimated_qty' },
      { data: 'detail', name: 'detail' },
      { data: 'consumable', name: 'consumable' },
      { data: 'equipment', name: 'equipment' },
      { data: 'equipment_fry', name: 'equipment_fry' },
      { data: 'edit', name: 'edit' }
      ]
    });
  });
</script>
@stop