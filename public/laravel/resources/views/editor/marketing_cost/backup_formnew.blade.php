@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><a href="{{ URL::route('editor.marketing_cost.index') }}"><i class="fa fa-bar-chart"></i> Marketing Cost</a></li>
	</ol>
</section>

<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<div class="x_panel">
						<h2>
							@if(isset($marketing_cost))
							<i class="fa fa-pencil"></i>
							@else
							<i class="fa fa-plus"></i> 
							@endif
							<i class="fa fa-money"></i> Marketing Cost
						</h2>
					</div>
					<hr>
					@include('errors.error')
					@if(isset($marketing_cost))
					{!! Form::model($marketing_cost, array('route' => ['editor.marketing_cost.update', $marketing_cost->id], 'method' => 'PUT', 'class'=>'update'))!!}
					@else
					{!! Form::open(array('route' => 'editor.marketing_cost.store', 'class'=>'editnew'))!!}
					@endif
					{{ csrf_field() }}
					<div class="col-md-6">
						<div class="x_content">							
							<div class="col-md-12 col-sm-12 col-xs-12 form-group">

								<div class="dropdown-plans">
									<label>Product</label>
									<select id="basic_plan" name="id_productions" class="form-control"> 
										@foreach($production_list_ed as $key => $production_list_ed)
										<option value="{{ $production_list_ed->id }}">{{ $production_list_ed->name }}</option>
										@endforeach
									</select>
								</div>
								  
								<hr>

								{{ Form::label('doc_code', 'Doc Code (Auto)') }}
								{{ Form::text('doc_code', old('doc_code'), array('class' => 'form-control', 'placeholder' => 'Doc Code*', 'required' => 'true', 'disabled')) }}<br/>

								{{ Form::label('datetime', 'Date Time') }}
								{{ Form::text('datetime', old('datetime'), array('class' => 'form-control', 'placeholder' => 'Date Time*', 'required' => 'true', 'id' => 'datetime')) }}<br/>

								{{ Form::label('Marketing') }}
								{{ Form::select('id_marketing', $marketing_list, old('id_marketing'), array('class' => 'form-control', 'required' => 'true', 'id' => 'id_marketing')) }}
		                    	<br/>

		                    	{{ Form::label('Customer') }}
								{{ Form::select('id_customer', $customer_list, old('id_customer'), array('class' => 'form-control', 'required' => 'true', 'id' => 'id_customer')) }}
		                    	<br/>

								{{ Form::label('description', 'Description') }}
								{{ Form::text('description', old('description'), array('class' => 'form-control', 'placeholder' => 'Description*', 'required' => 'true')) }}

								<hr>
								{{ Form::label('sales_price', 'Harga Jual') }}
								{{ Form::number('sales_price', old('sales_price'), array('class' => 'form-control', 'placeholder' => 'Harga Jual*', 'required' => 'true')) }}<br/>

								{{ Form::label('estimated_qty', 'Est. hari produksi / year') }}
								{{ Form::number('estimated_qty', old('estimated_qty'), array('class' => 'form-control', 'placeholder' => 'Est. hari produksi / year*', 'required' => 'true')) }}
								<br/>

								{{ Form::label('phl', 'PHL') }}
								{{ Form::number('phl', old('phl'), array('class' => 'form-control', 'placeholder' => 'PHL*', 'required' => 'true')) }}

								<br/>

								{{ Form::label('handling_fee', 'Handling Fee') }}
								{{ Form::number('handling_fee', old('handling_fee'), array('class' => 'form-control', 'placeholder' => 'Handling Fee*', 'required' => 'true')) }}

							</div>
						</div>
					</div>

					<div class="col-md-6">
						<div class="x_content">
							<div class="col-md-12 col-sm-12 col-xs-12 form-group"> 
								{{ Form::hidden('handling_fee', old('handling_fee'), array('class' => 'form-control', 'placeholder' => 'Harga Per PCS*', 'required' => 'true')) }}<br/> 
								{{ Form::hidden('handling_fee', old('handling_fee'), array('class' => 'form-control', 'placeholder' => 'Margin*', 'required' => 'true')) }}<br/> 
								{{ Form::hidden('handling_fee', old('handling_fee'), array('class' => 'form-control', 'placeholder' => 'Harga*', 'required' => 'true')) }}
							</div>
						</div>
					</div>

					<div class="col-md-12">
						<hr>
						<ul class="nav nav-tabs">
							<li class="active"><a data-toggle="tab" href="#home">Detail</a></li>
							<li><a data-toggle="tab" href="#menu1">Equipment</a></li>
							<li><a data-toggle="tab" href="#menu2">Equipment Fry</a></li>
							<li><a data-toggle="tab" href="#menu3">Consumable</a></li>
							<li><a data-toggle="tab" href="#menu4">Packaging</a></li>
						</ul>
						<div class="tab-content">
							<div id="home" class="tab-pane fade in active">
								<div class="box-body">
									<div class="row">
										<div class="col-xs-4">
											{{ Form::label('id_marketing_cost_item', 'Cost Item') }}
		                    				{{ Form::select('id_marketing_cost_item', $marketing_cost_item_list, old('id_marketing_cost_item'), array('class' => 'form-control', 'required' => 'true', 'id' => 'id_marketing_cost_item')) }}
										</div>
										<div class="col-xs-3">
											{{ Form::label('quantitydetail_val', 'Quantity') }}
											{{ Form::number('quantitydetail_val', old('quantitydetail_val'), ['id' => 'quantitydetail_val', 'min' => '0', 'class' => 'form-control']) }}
										</div>
										<div class="col-xs-3">
											{{ Form::label('Unit') }}
		                    				{{ Form::select('id_unit_detail', $unit_list, old('id_unit_detail'), array('class' => 'form-control', 'required' => 'true', 'id' => 'id_unit_detail')) }}
										</div>
										<div class="col-xs-2">
											<br/>
											<button type="button" class="btn btn-default" id="btn_add_detail"><i class="fa fa-cart-plus"></i>&nbsp;Add</button>
										</div>
										<div class="col-xs-12">
											<hr>
											<table class="table table-striped table-hover">
												<thead>
													<tr>
														<th>Name</th>
														<th>Quantity</th>
														<th>UoM</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody id="cart">
		                    					</tbody>
											</table>
										</div> 
									</div>
								</div> 
								<!-- /.box-body -->
							</div> 
 
							<div id="menu1" class="tab-pane fade">
								<div class="box-body">
									<div class="row">
										<div class="col-xs-4"> 
											{{ Form::label('id_item_equipment', 'Name') }}
		                    				{{ Form::select('id_item_equipment', $equipment_list, old('id_item_equipment'), array('class' => 'form-control', 'required' => 'true', 'id' => 'id_item_equipment')) }}
										</div>
										<div class="col-xs-3">
											{{ Form::label('quantity_equipment', 'Quantity') }}
											{{ Form::number('quantity_equipment', old('quantity_equipment'), ['id' => 'quantity_equipment', 'min' => '0', 'class' => 'form-control']) }}
										</div>
										<div class="col-xs-3">
											{{ Form::label('Unit') }}
		                    				{{ Form::select('id_unit_equipment', $unit_list, old('id_unit_equipment'), array('class' => 'form-control', 'required' => 'true', 'id' => 'id_unit_equipment')) }}
										</div>
										<div class="col-xs-2">
											<br/>
											<button type="button" class="btn btn-default" id="btn_add_equipment"><i class="fa fa-cart-plus"></i>&nbsp;Add</button>

										</div>
										<div class="col-xs-12">
											<hr>
											<table class="table table-striped table-hover">
												<thead>
													<tr>
														<th>Name</th>
														<th>Quantity</th>
														<th>UoM</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody id="cart_equipment">
		                    					</tbody>
											</table>
										</div> 
									</div>
								</div> 
								<!-- /.box-body -->
							</div> 

							<div id="menu2" class="tab-pane fade">
								<div class="box-body">
									<div class="row">
										<div class="col-xs-4">
											{{ Form::label('id_item_equipment_fry', 'Name') }}
		                    				{{ Form::select('id_item_equipment_fry', $equipment_list, old('id_item_equipment_fry'), array('class' => 'form-control', 'required' => 'true', 'id' => 'id_item_equipment_fry')) }}
										</div>
										<div class="col-xs-3">
											{{ Form::label('quantity_equipment_fry', 'Quantity') }}
											{{ Form::number('quantity_equipment_fry', old('quantity_equipment_fry'), ['id' => 'quantity_equipment_fry', 'min' => '0', 'class' => 'form-control']) }}
										</div>
										<div class="col-xs-3">
											{{ Form::label('Unit') }}
		                    				{{ Form::select('id_unit_equipment_fry', $unit_list, old('id_unit_equipment_fry'), array('class' => 'form-control', 'required' => 'true', 'id' => 'id_unit_equipment_fry')) }}
										</div>
										<div class="col-xs-2">
											<br/>
											<button type="button" class="btn btn-default" id="btn_add_equipment_fry"><i class="fa fa-cart-plus"></i>&nbsp;Add</button>

										</div>
										<div class="col-xs-12">
											<hr>
											<table class="table table-striped table-hover">
												<thead>
													<tr>
														<th>Name</th>
														<th>Quantity</th>
														<th>UoM</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody id="cart_equipment_fry">
		                    					</tbody>
											</table>
										</div> 
									</div>
								</div> 
								<!-- /.box-body -->
							</div> 


							<div id="menu3" class="tab-pane fade">
								<div class="box-body">
									<div class="row">
										<div class="col-xs-4"> 
											{{ Form::label('id_consumable', 'Name') }}
		                    				{{ Form::select('id_consumable', $consummable_list, old('id_consumable'), array('class' => 'form-control', 'required' => 'true', 'id' => 'id_consumable')) }}
										</div>
										<div class="col-xs-3">
											{{ Form::label('quantity_consumable', 'Quantity') }}
											{{ Form::number('quantity_consumable', old('quantity_consumable'), ['id' => 'quantity_consumable', 'min' => '0', 'class' => 'form-control']) }}
										</div>
										<div class="col-xs-3">
											{{ Form::label('Unit') }}
		                    				{{ Form::select('id_unit_consumable', $unit_list, old('id_unit_consumable'), array('class' => 'form-control', 'required' => 'true', 'id' => 'id_unit_consumable')) }}
										</div>
										<div class="col-xs-2">
											<br/>
											<button type="button" class="btn btn-default" id="btn_add_consumable"><i class="fa fa-cart-plus"></i>&nbsp;Add</button>

										</div>
										<div class="col-xs-12">
											<hr>
											<table class="table table-striped table-hover">
												<thead>
													<tr>
														<th>Name</th>
														<th>Quantity</th>
														<th>UoM</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody id="cart_consumable">
		                    					</tbody>
											</table>
										</div> 
									</div>
								</div> 
								<!-- /.box-body -->
							</div> 


							<div id="menu4" class="tab-pane fade">
								<div class="box-body">
									<div class="row">
										<div class="col-xs-4"> 
											{{ Form::label('id_packaging', 'Name') }}
		                    				{{ Form::select('id_packaging', $packaging_list, old('id_packaging'), array('class' => 'form-control', 'required' => 'true', 'id' => 'id_packaging')) }}
										</div>
										<div class="col-xs-3">
											{{ Form::label('quantity_packaging', 'Quantity') }}
											{{ Form::number('quantity_packaging', old('quantity_packaging'), ['id' => 'quantity_packaging', 'min' => '0', 'class' => 'form-control']) }}
										</div>
										<div class="col-xs-3">
											{{ Form::label('Unit') }}
		                    				{{ Form::select('id_unit_packaging', $unit_list, old('id_unit_packaging'), array('class' => 'form-control', 'required' => 'true', 'id' => 'id_unit_packaging')) }}
										</div>
										<div class="col-xs-2">
											<br/>
											<button type="button" class="btn btn-default" id="btn_add_packaging"><i class="fa fa-cart-plus"></i>&nbsp;Add</button>

										</div>
										<div class="col-xs-12">
											<hr>
											<table class="table table-striped table-hover">
												<thead>
													<tr>
														<th>Name</th>
														<th>Quantity</th>
														<th>UoM</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody id="cart_packaging">
		                    					</tbody>
											</table>
										</div> 
									</div>
								</div> 
								<!-- /.box-body -->
							</div> 
						</div>

						<hr>
						<button type="button" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
						<a href="{{ URL::route('editor.marketing_cost.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
					</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</section>

@stop

@section('scripts')
<script>

var cart_equipment = JSON.parse("{}");
var cart_equipment_fry = JSON.parse("{}");
var cart_consumable = JSON.parse("{}");
var cart_packaging = JSON.parse("{}");
</script>
@if(isset($details))
<script>

var cart = JSON.parse("{}");
// ADD BUTTON DETAIL
$("#btn_add_detail").on('click', function() {
	var quantitydetail_val = $("#quantitydetail_val").val();

	var id_marketing_cost_item = $("#id_marketing_cost_item option:selected").val();
	var id_unit_detail = $("#id_unit_detail option:selected").val(); 
	var key = id_marketing_cost_item+"_"+quantitydetail_val+"_"+id_unit_detail;

		if(cart[key] == undefined)
		{
			cart[key] = 1;
			var new_row = '';
			new_row += '<tr>';
			new_row += '<td>';
			new_row += '<input type="hidden" name="id_marketing_cost_item['+key+']" value="'+id_marketing_cost_item+'">';
			new_row += $("#id_marketing_cost_item option:selected").text();
			new_row += '</td>';
			new_row += '<td>';
			new_row += '<input type="hidden" name="quantitydetail['+key+']" value="'+quantitydetail_val+'">';
			new_row += quantitydetail_val;
			new_row += '</td>';
			new_row += '<td>';
			new_row += '<input type="hidden" name="id_unit_detail['+key+']" value="'+id_unit_detail+'">';
			new_row += $("#id_unit_detail option:selected").text();
			new_row += '</td>';
			new_row += '<td>'; 
			new_row += '<button type="button" class="btn btn-danger btn-sm" id="btn_remove_'+key+'">';
			new_row += '<i class="fa fa-remove"></i>';
			new_row += '</button>';
			new_row += '</td>';
			new_row += '</tr>';

			$("#cart").append(new_row); 
			 
			$("#btn_remove_"+key).on('click', function() 
			{
				cart[key] == null;
				$(this).parent().parent().remove();
			});
		} 
		
});

jQuery.each({!! $details !!}, function( i, val ) {  
	var quantitydetail = val['quantity']; 

	var id_marketing_cost_item = val['id_marketing_cost_item'];
	var id_unit_detail = val['id_unit_detail'];
	var key = id_marketing_cost_item+"_"+quantitydetail+"_"+id_unit_detail;
	console.log(id_unit_detail);
	$("#id_marketing_cost_item").val(id_marketing_cost_item);
	$("#id_unit_detail").val(id_unit_detail);
	cart[key] = id_marketing_cost_item;
   	var new_row = '';
	new_row += '<tr>';
	new_row += '<td>';
	new_row += '<input type="hidden" name="id_marketing_cost_item['+key+']" value="'+id_marketing_cost_item+'">';
	new_row += $("#id_marketing_cost_item option:selected").text();
	new_row += '</td>';
	new_row += '<td>';
	new_row += '<input type="hidden" name="quantitydetail['+key+']" value="'+quantitydetail+'">';
	new_row += quantitydetail;
	new_row += '</td>';
	new_row += '<td>';
	new_row += '<input type="hidden" name="id_unit_detail['+key+']" value="'+id_unit_detail+'">';
	new_row += $("#id_unit_detail option:selected").text();
	new_row += '</td>';
	new_row += '<td>'; 
	new_row += '<button type="button" class="btn btn-danger btn-sm" id="btn_remove_'+key+'">';
	new_row += '<i class="fa fa-remove"></i>';
	new_row += '</button>';
	new_row += '</td>';
	new_row += '</tr>';

	$("#cart").append(new_row);
 
	$("#btn_remove_"+key).on('click', function() 
	{
		cart[key] == null;
		$(this).parent().parent().remove();
	});
});
</script>
@endif


@if(isset($equipments))
<script>
// ADD BUTTON EQUIPMENT
$("#btn_add_equipment").on('click', function() { 
	var id_item_equipment = $("#id_item_equipment option:selected").val();
	var quantity_equipment = $("#quantity_equipment").val();
	var id_unit_equipment = $("#id_unit_equipment option:selected").val(); 

	var key = id_item_equipment+"_"+quantity_equipment+"_"+id_unit_equipment;

		if(cart_equipment[key] == undefined)
		{
			cart_equipment[key] = 1;
			var new_row = '';
			new_row += '<tr>';
			new_row += '<td>'; 
			new_row += '<input type="hidden" name="id_item_equipment['+key+']" value="'+id_item_equipment+'">';
			new_row += $("#id_item_equipment option:selected").text();
			new_row += '</td>';
			new_row += '<td>';
			new_row += '<input type="hidden" name="quantity_equipment['+key+']" value="'+quantity_equipment+'">';
			new_row += quantity_equipment;
			new_row += '</td>';
			new_row += '<td>';
			new_row += '<input type="hidden" name="id_unit_equipment['+key+']" value="'+id_unit_equipment+'">';
			new_row += $("#id_unit_equipment option:selected").text();
			new_row += '</td>';
			new_row += '<td>'; 
			new_row += '<button type="button" class="btn btn-danger btn-sm" id="btn_remove_equipment_'+key+'">';
			new_row += '<i class="fa fa-remove"></i>';
			new_row += '</button>';
			new_row += '</td>';
			new_row += '</tr>';

			$("#cart_equipment").append(new_row); 
			 
			$("#btn_remove_equipment_"+key).on('click', function() 
			{
				cart_equipment[key] == null;
				$(this).parent().parent().remove();
			});
		} 
		
});

jQuery.each({!! $equipments !!}, function( i, val ) {   
	var id_item_equipment = val['id_item_equipment'];  
	var quantity_equipment = val['quantity_equipment'];
	var id_unit_equipment = val['id_unit_equipment'];
	var key = id_item_equipment+"_"+quantity_equipment+"_"+id_unit_equipment;

	$("#id_item_equipment").val(id_item_equipment);
	$("#id_unit_equipment").val(id_unit_equipment);

	//cart_equipment[key] = id_marketing_cost_item;
   	var new_row = '';
	new_row += '<tr>';
	new_row += '<td>'; 
	new_row += '<input type="hidden" name="id_item_equipment['+key+']" value="'+id_item_equipment+'">';
	new_row += $("#id_item_equipment option:selected").text();
	new_row += '</td>';
	new_row += '<td>';
	new_row += '<input type="hidden" name="quantity_equipment['+key+']" value="'+quantity_equipment+'">';
	new_row += quantity_equipment;
	new_row += '</td>';
	new_row += '<td>';
	new_row += '<input type="hidden" name="id_unit_equipment['+key+']" value="'+id_unit_equipment+'">';
	new_row += $("#id_unit_equipment option:selected").text();
	new_row += '</td>';
	new_row += '<td>'; 
	new_row += '<button type="button" class="btn btn-danger btn-sm" id="btn_remove_equipment_'+key+'">';
	new_row += '<i class="fa fa-remove"></i>';
	new_row += '</button>';
	new_row += '</td>';
	new_row += '</tr>';

	$("#cart_equipment").append(new_row);
 
	$("#btn_remove_equipment_"+key).on('click', function() 
	{
		cart_equipment[key] == null;
		$(this).parent().parent().remove();
	});
});
</script>
@endif


@if(isset($equipments_fry))
<script>
// ADD BUTTON EQUIPMENT FRY
$("#btn_add_equipment_fry").on('click', function() { 
	var id_item_equipment_fry = $("#id_item_equipment_fry option:selected").val(); 
	var quantity_equipment_fry = $("#quantity_equipment_fry").val();
 
	var id_unit_equipment_fry = $("#id_unit_equipment_fry option:selected").val(); 
	var key = id_item_equipment_fry+"_"+quantity_equipment_fry+"_"+id_unit_equipment_fry;

		if(cart_equipment_fry[key] == undefined)
		{
			cart_equipment_fry[key] = 1;
			var new_row = '';
			new_row += '<tr>';
			new_row += '<td>';
			new_row += '<input type="hidden" name="id_item_equipment_fry['+key+']" value="'+id_item_equipment_fry+'">';
			new_row += $("#id_item_equipment_fry option:selected").text();
			new_row += '</td>';
			new_row += '<td>';
			new_row += '<input type="hidden" name="quantity_equipment_fry['+key+']" value="'+quantity_equipment_fry+'">';
			new_row += quantity_equipment_fry;
			new_row += '</td>';
			new_row += '<td>';
			new_row += '<input type="hidden" name="id_unit_equipment_fry['+key+']" value="'+id_unit_equipment_fry+'">';
			new_row += $("#id_unit_equipment_fry option:selected").text();
			new_row += '</td>';
			new_row += '<td>'; 
			new_row += '<button type="button" class="btn btn-danger btn-sm" id="btn_remove_equipment_fry_'+key+'">';
			new_row += '<i class="fa fa-remove"></i>';
			new_row += '</button>';
			new_row += '</td>';
			new_row += '</tr>';

			$("#cart_equipment_fry").append(new_row); 
			 
			$("#btn_remove_equipment_fry_"+key).on('click', function() 
			{
				cart_equipment[key] == null;
				$(this).parent().parent().remove();
			});
		} 
		
});

jQuery.each({!! $equipments_fry !!}, function( i, val ) {    
	var quantity_equipment_fry = val['quantity_equipment_fry']; 
	var id_item_equipment_fry = val['id_item_equipment_fry'];  
	var id_unit_equipment_fry = val['id_unit_equipment_fry'];
	var key = id_item_equipment_fry+"_"+quantity_equipment_fry+"_"+id_unit_equipment_fry;

	$("#id_item_equipment_fry").val(id_item_equipment_fry);
	$("#id_unit_equipment_fry").val(id_unit_equipment_fry);

	//cart_equipment_fry[key] = id_marketing_cost_item_fry;
   	var new_row = '';
	new_row += '<tr>';
	new_row += '<td>';
	new_row += '<input type="hidden" name="id_item_equipment_fry['+key+']" value="'+id_item_equipment_fry+'">';
	new_row += $("#id_item_equipment_fry option:selected").text();
	new_row += '</td>';
	new_row += '<td>';
	new_row += '<input type="hidden" name="quantity_equipment_fry['+key+']" value="'+quantity_equipment_fry+'">';
	new_row += quantity_equipment_fry;
	new_row += '</td>';
	new_row += '<td>';
	new_row += '<input type="hidden" name="id_unit_equipment_fry['+key+']" value="'+id_unit_equipment_fry+'">';
	new_row += $("#id_unit_equipment_fry option:selected").text();
	new_row += '</td>';
	new_row += '<td>'; 
	new_row += '<button type="button" class="btn btn-danger btn-sm" id="btn_remove_equipment_fry_'+key+'">';
	new_row += '<i class="fa fa-remove"></i>';
	new_row += '</button>';
	new_row += '</td>';
	new_row += '</tr>';

	$("#cart_equipment_fry").append(new_row);
 
	$("#btn_remove_equipment_fry_"+key).on('click', function() 
	{
		cart_equipment_fry[key] == null;
		$(this).parent().parent().remove();
	});
});
</script>
@endif


@if(isset($consumables))
<script>
// ADD BUTTON CONSUMABLE
$("#btn_add_consumable").on('click', function() {
	var id_consumable = $("#id_consumable option:selected").val(); 
	var quantity_consumable = $("#quantity_consumable").val();
 
	var id_unit_consumable = $("#id_unit_consumable option:selected").val(); 
	var key = id_consumable+"_"+quantity_consumable+"_"+id_unit_consumable;

		if(cart_consumable[key] == undefined)
		{
			cart_consumable[key] = 1;
			var new_row = '';
			new_row += '<tr>';
			new_row += '<td>';
			new_row += '<input type="hidden" name="id_consumable['+key+']" value="'+id_consumable+'">';
			new_row += $("#id_consumable option:selected").text();
			new_row += '</td>';
			new_row += '<td>';
			new_row += '<input type="hidden" name="quantity_consumable['+key+']" value="'+quantity_consumable+'">';
			new_row += quantity_consumable;
			new_row += '</td>';
			new_row += '<td>';
			new_row += '<input type="hidden" name="id_unit_consumable['+key+']" value="'+id_unit_consumable+'">';
			new_row += $("#id_unit_consumable option:selected").text();
			new_row += '</td>';
			new_row += '<td>'; 
			new_row += '<button type="button" class="btn btn-danger btn-sm" id="btn_remove_consumable_'+key+'">';
			new_row += '<i class="fa fa-remove"></i>';
			new_row += '</button>';
			new_row += '</td>';
			new_row += '</tr>';

			$("#cart_consumable").append(new_row); 
			 
			$("#btn_remove_consumable_"+key).on('click', function() 
			{
				cart_consumable[key] == null;
				$(this).parent().parent().remove();
			});
		} 
		
});

jQuery.each({!! $consumables !!}, function( i, val ) {  
	var quantity_consumable = val['quantity_consumable']; 
	var id_consumable = val['id_consumable'];  
	var id_unit_consumable = val['id_unit_consumable'];
	var key = id_consumable+"_"+quantity_consumable+"_"+id_unit_consumable;

	$("#id_consumable").val(id_consumable);
	$("#id_unit_consumable").val(id_unit_consumable);

	//cart_consumable[key] = id_consumable;
   	var new_row = '';
	new_row += '<tr>';
	new_row += '<td>';
	new_row += '<input type="hidden" name="id_consumable['+key+']" value="'+id_consumable+'">';
	new_row += $("#id_consumable option:selected").text();
	new_row += '</td>';
	new_row += '<td>';
	new_row += '<input type="hidden" name="quantity_consumable['+key+']" value="'+quantity_consumable+'">';
	new_row += quantity_consumable;
	new_row += '</td>';
	new_row += '<td>';
	new_row += '<input type="hidden" name="id_unit_consumable['+key+']" value="'+id_unit_consumable+'">';
	new_row += $("#id_unit_consumable option:selected").text();
	new_row += '</td>';
	new_row += '<td>'; 
	new_row += '<button type="button" class="btn btn-danger btn-sm" id="btn_remove_consumable_'+key+'">';
	new_row += '<i class="fa fa-remove"></i>';
	new_row += '</button>';
	new_row += '</td>';
	new_row += '</tr>';

	$("#cart_consumable").append(new_row);
 
	$("#btn_remove_consumable_"+key).on('click', function() 
	{
		cart_consumable[key] == null;
		$(this).parent().parent().remove();
	});
});

</script>
@endif

@if(isset($packagings))
<script>
	// ADD BUTTON PACKAGING
$("#btn_add_packaging").on('click', function() {
	var id_packaging = $("#id_packaging option:selected").val(); 
	var quantity_packaging = $("#quantity_packaging").val();
 
	var id_unit_packaging = $("#id_unit_packaging option:selected").val(); 
	var key = id_packaging+"_"+quantity_packaging+"_"+id_unit_packaging;

		if(cart_packaging[key] == undefined)
		{
			cart_packaging[key] = 1;
			var new_row = '';
			new_row += '<tr>';
			new_row += '<td>';
			new_row += '<input type="hidden" name="id_packaging['+key+']" value="'+id_packaging+'">';
			new_row += $("#id_packaging option:selected").text();
			new_row += '</td>';
			new_row += '<td>';
			new_row += '<input type="hidden" name="quantity_packaging['+key+']" value="'+quantity_packaging+'">';
			new_row += quantity_packaging;
			new_row += '</td>';
			new_row += '<td>';
			new_row += '<input type="hidden" name="id_unit_packaging['+key+']" value="'+id_unit_packaging+'">';
			new_row += $("#id_unit_packaging option:selected").text();
			new_row += '</td>';
			new_row += '<td>'; 
			new_row += '<button type="button" class="btn btn-danger btn-sm" id="btn_remove_packaging_'+key+'">';
			new_row += '<i class="fa fa-remove"></i>';
			new_row += '</button>';
			new_row += '</td>';
			new_row += '</tr>';

			$("#cart_packaging").append(new_row); 
			 
			$("#btn_remove_packaging_"+key).on('click', function() 
			{
				cart_packaging[key] == null;
				$(this).parent().parent().remove();
			});
		} 
		
});

jQuery.each({!! $packagings !!}, function( i, val ) {   

	var quantity_packaging = val['quantity_packaging']; 
	var id_unit_packaging = val['id_unit_packaging'];  
	var id_packaging = val['id_packaging'];
	var key = id_packaging+"_"+quantity_packaging+"_"+id_unit_packaging;

	$("#id_unit_packaging").val(id_unit_packaging);
	$("#id_packaging").val(id_packaging);

	//cart_packaging[key] = id_packaging
   	var new_row = '';
	new_row += '<tr>';
	new_row += '<td>';
	new_row += '<input type="hidden" name="id_packaging['+key+']" value="'+id_packaging+'">';
	new_row += $("#id_packaging option:selected").text();
	new_row += '</td>';
	new_row += '<td>';
	new_row += '<input type="hidden" name="quantity_packaging['+key+']" value="'+quantity_packaging+'">';
	new_row += quantity_packaging;
	new_row += '</td>';
	new_row += '<td>';
	new_row += '<input type="hidden" name="id_unit_packaging['+key+']" value="'+id_unit_packaging+'">';
	new_row += $("#id_unit_packaging option:selected").text();
	new_row += '</td>';
	new_row += '<td>'; 
	new_row += '<button type="button" class="btn btn-danger btn-sm" id="btn_remove_packaging_'+key+'">';
	new_row += '<i class="fa fa-remove"></i>';
	new_row += '</button>';
	new_row += '</td>';
	new_row += '</tr>';

	$("#cart_packaging").append(new_row);
 
	$("#btn_remove_packaging_"+key).on('click', function() 
	{
		cart_packaging[key] == null;
		$(this).parent().parent().remove();
	});
});
</script>
@endif
<script>
	
 $(document).ready(function(){
    $('#datetime').datetimepicker({
      sideBySide: true,
      format: 'YYYY-MM-DD HH:mm',
    });
  });
</script>
@stop

