<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html><head>
<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type"><title>Ingredient</title>

</head>

<style type="text/css">
	body {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
        background-color: #FAFAFA;
        font: 8pt arial;
	    }
	    * {
	        box-sizing: border-box;
	        -moz-box-sizing: border-box;
	    }

	    .logo{position: absolute;margin-top:-2mm;}
	    .logo img{height: 15mm;}
	    .tb_ingr{ border: 1px solid #bdbdbd; }
	    .tb_ingr tr th{
	    	background-color: #eaeaea;
	    	padding: 5px 10px;
	    	text-align: left;
	    }
	    .tb_ingr tr td{
	    	padding: 2px 10px;
	    }
	    .page {
	        width: 210mm;
	        height: 325mm;
	        padding: 10mm;
	        margin: 10mm auto;
	        border: 1px #D3D3D3 solid;
	        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
	        background-size: 210mm auto;
	        background: #fff;
	    }

	    .summary{border-collapse: collapse;}
	    .summary tr td{
	    	padding: 3px 10px;
	    }
	    .summary tr th{
	    	padding: 4px 10px;
	    	background-color: #eaeaea;
	    	text-align: left;
	    	font-size: 12px;
	    	font-weight: bold;

	    }

	     @page {
	        /*size: A4;*/
	        margin: 0;
	    }
	    @media print {
	        html, body {
	            width: 210mm;
	            height: 325mm;   
	            background: #fff;     
	        }
	        .page {
	            margin: 0;
	            border: initial;
	            border-radius: initial;
	            width: initial;
	            min-height: initial;
	            box-shadow: initial;
	            background: #fff;
	            page-break-after: always;
	        	background-size: 100% auto;
	        }
	    }
</style>

<body>
<div class="page">
		
	<div class="logo">
		<img src="{{Config::get('constants.path.img')}}/vfi-small.png">
	</div>
	<center>
	<h1>{{ ucwords($marketing_cost->production_name) }}</h1>
	<hr>
	</center>
	<br>
	<table style="float: left;" border="0" cellpadding="2" cellspacing="2">
		<tbody>
		<tr>
		<td>Harga Jual (pcs)</td>
		<td style="text-align: right;">{{ number_format($marketing_cost->sales_price,0) }}</td>
		<td></td>
		</tr>
		@foreach($details as $key => $detail)
		<tr>
		<td>{{ $detail->name_marketing_cost }}</td>
		<td style="text-align: right;">{{ number_format($detail->quantity,0) }}</td>
		<td>per {{$detail->description}}</td>
		</tr>
		@endforeach 
		<tr>
		<td>Type</td>
		<td style="text-align: right;">Vannamei</td>
		<td></td>
		</tr>
		</tbody>
		</table>
		<table style="float: right;" border="0" cellpadding="2" cellspacing="2">
		<tbody>
		<tr>
		<td>Est. Hari Produksi/Year</td>
		<td style="text-align: right;">{{ number_format($marketing_cost->estimated_qty,0) }}</td>
		<td></td>
		</tr>
		<tr>
		<td>Estimate Produksi.</td>
		<td style="text-align: right;">{{ $marketing_cost->estimate_produksi }}</td>
		<td>Pcs/Day</td>
		</tr>
		<tr>
		<td>Estimate Produksi</td>
		<td style="text-align: right;">{{ $marketing_cost->estimate_produksi_sum }}</td>
		<td>Pcs/Year</td>
		</tr>
		<tr>
		<td>Estimate End Product</td>
		<td style="text-align: right;">{{ $marketing_cost->estimate_end_product }}</td>
		<td>Kg/Year</td>
		</tr>
		</tbody>
	</table>
	<br>
	<div style="clear: both;"></div>
	<br>
	<br>
	<table style="border-collapse: collapse;" border="0" cellpadding="5" cellspacing="5" width="100%" class="tb_ingr">
		<tbody>
		<tr>
		<th></th>
		<th style="text-align: center; font-weight: bold;width: 30mm">VALUE
		ADDED</th>
		<th style="text-align: center; font-weight: bold;">MATERIAL<br>PROD / DAY</th>
		<th style="text-align: center; font-weight: bold;">MATERIAL<br>
		PROD / YEAR</th>
		<th style="text-align: center; font-weight: bold;">SATUAN</th>
		<th style="text-align: center; font-weight: bold;">COST
		/ DAY</th>
		<th style="text-align: center; font-weight: bold;">COST
		/ YEAR</th>
		</tr>
		<tr>
		<td colspan="7"></td>
		</tr>
		<tr>
		<th style="font-weight: bold;">INGREDIENT</th>
		<th></th>
		<th style="font-weight: bold;"></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		</tr>
		<tr>
		 @foreach($ingredients as $key => $ingredient)
		<td></td>
		<td style="text-align: right;">{{$ingredient->ingredient_name}}</td>
		<td style="text-align: right;">{{number_format($ingredient->qty_day, 0)}}</td>
		<td style="text-align: right;">{{number_format(($ingredient->qty_year),2)}}</td>
		<td style="text-align: center;">{{$ingredient->uom_name}} / Year</td>
		<td style="text-align: right;">{{number_format($ingredient->cost_day)}}</td>
		<td style="text-align: right;">{{ number_format($ingredient->cost_year,0) }}</td>
		</tr>
		@endforeach 
		<tr>
		<th style="font-weight: bold;"  colspan="7" rowspan="1">Equipment</th>
		</tr>
		@foreach($equipments as $key => $equipment)
		<tr>
		<td style="text-align: right;" colspan="2" rowspan="1">{{$equipment->item_name}}</td>
		<td style="text-align: right;">{{$equipment->quantity_equipment}}</td>
		<td style="text-align: right;">{{number_format($equipment->material_prod_year)}}</td>
		<td style="text-align: center;">{{$equipment->description}} / Year</td>
		<td style="text-align: right;">{{number_format($equipment->cost_day,0)}}</td>
		<td style="text-align: right;">{{number_format($equipment->cost_year,0)}}</td>
		</tr>
		@endforeach

		<tr>
		<th style="font-weight: bold;"  colspan="7" rowspan="1">Equipment Fry</th>
		</tr>
		@foreach($equipments_fry as $key => $equipments_fry)
		<tr>
		<td style="text-align: right;" colspan="2" rowspan="1">{{$equipments_fry->item_name}}</td>
		<td style="text-align: right;">{{$equipments_fry->quantity_equipment}}</td>
		<td style="text-align: right;">{{number_format($equipments_fry->material_prod_year)}}</td>
		<td style="text-align: center;">{{$equipments_fry->description}} / Year</td>
		<td style="text-align: right;">{{number_format($equipments_fry->cost_day,0)}}</td>
		<td style="text-align: right;">{{number_format($equipments_fry->cost_year,0)}}</td>
		</tr>
		@endforeach

		<tr>
		<td colspan="7">&nbsp;</td>
		</tr>
		<tr>
		<th style="font-weight: bold;" colspan="2" rowspan="1">PHL</th>
		<th style="text-align: right;">{{$marketing_cost->phl}}</th>
		<th style="text-align: right;">{{$marketing_cost->phl * $marketing_cost->estimated_qty}}</th>
		<th style="text-align: center;">Person</th>
		<th style="text-align: right;">{{ number_format($marketing_cost->phl_price * $marketing_cost->phl,0) }}</th>
		<th style="text-align: right;">{{ number_format($marketing_cost->phl_price * ($marketing_cost->phl * $marketing_cost->estimated_qty),0) }}</th>
		</tr>
		<tr>
		<td colspan="7">&nbsp;</td>
		</tr>
		<tr>
		<th style="font-weight: bold;"  colspan="7" rowspan="1">Consummable</th>
		</tr>
		@foreach($consumables as $key => $consumables)
		<tr>
		<td style="text-align: right;" colspan="2" rowspan="1">{{$consumables->item_name}}</td>
		<td style="text-align: right;">{{$consumables->quantity_consumable}}</td>
		<td style="text-align: right;">{{number_format($consumables->material_prod_year) }}</td>
		<td style="text-align: center;">Kg / Year</td>
		<td style="text-align: right;">{{ number_format($consumables->cost_day,0) }}</td>
		<td style="text-align: right;">{{number_format($consumables->cost_year,0) }}</td>
		</tr>
		@endforeach 
		<tr>
		<td colspan="7">&nbsp;</td>
		</tr>
		<tr>
		<th style="font-weight: bold;" colspan="2" rowspan="1">TOTAL COST UNTUK PRODUCT</th>
		<th></th>
		<th></th>
		<th></th>
		<th style="text-align: right;">Per Day</th>
		<th style="text-align: right;">Per Year</th>
		</tr>
		<tr>
		<td  colspan="2"></td>
		<td></td>
		<td></td>
		<td></td>
		<td style="text-align: right;">{{number_format($total_cost->cost_day,0) }}</td>
		<td style="text-align: right;">{{number_format($total_cost->cost_year,0) }}</td>
		</tr>
		<tr>
		<th style="font-weight: bold;"  colspan="7" rowspan="1">Packaging</th>
		</tr>
		@foreach($packagings as $key => $packaging)
		<tr>
		<td style="text-align: right;" colspan="2" rowspan="1">{{$packaging->item_name}}</td>
		<td style="text-align: right;">{{$packaging->quantity_packaging}}</td>
		<td style="text-align: right;">{{number_format($packaging->material_prod_year,0)}}</td>
		<td style="text-align: center;">Pcs</td>
		<td style="text-align: right;">{{number_format($packaging->cost_day,0)}}</td>
		<td style="text-align: right;">{{ number_format($packaging->cost_year,0) }}</td>
		</tr>
		@endforeach 
		<tr>
		<td colspan="7">&nbsp;</td>
		</tr>
		<tr>
		<th style="font-weight: bold;"  colspan="7" rowspan="1">Harga Jual / Pack Final Product</th>
		</tr>
		@foreach($total_cost_f as $key => $total_cost_f)
		<tr>
		<td style="text-align: right;" colspan="2" rowspan="1">{{$total_cost_f->item_name}}</td>
		<td style="text-align: right;">{{$total_cost_f->quantity_packaging}}</td>
		<td style="text-align: right;">{{number_format($total_cost_f->material_prod_year,0)}}</td>
		<td style="text-align: center;">Pcs</td>
		<td style="text-align: right;">{{number_format($total_cost_f->cost_day,0)}}</td>
		<td style="text-align: right;">{{ number_format($total_cost_f->cost_year,0) }}</td>
		</tr>
		@endforeach 
		<tr>
		<td colspan="7">&nbsp;</td>
		</tr>
		<tr>
		<th style="font-weight: bold;"  colspan="7" rowspan="1">Harga Jual Product / Pack</th>
		</tr>
		@foreach($packagings as $key => $packaging)
		<tr>
		<td style="text-align: right;" colspan="2" rowspan="1">{{$packaging->item_name}}</td>
		<td style="text-align: right;">{{ number_format(($total_cost_f->t_cost_day + $marketing_cost->sales_price_per_day) / $total_cost_f->quantity_packaging,0) }}</td>
		<td style="text-align: right;"></td>
		<td style="text-align: center;"></td>
		<td style="text-align: right;"></td>
		<td style="text-align: right;"></td>
		</tr>
		@endforeach 
		<tr>
			<td colspan="7">&nbsp;</td>
		</tr>
		</tbody>
	</table>
	<br>
	<br>
	<table border="0" cellpadding="3" cellspacing="3" class="summary">
		<tbody> 
		<tr>
		<td style="width:40mm"><span style="font-weight: bold;">Harga
		Jual Per Day</span></td>
		<td></td> 
		<td style="text-align: right;">{{number_format($marketing_cost->sales_price_per_day,0) }}</td> 
		</tr>
		 
		<tr>
		<td><span style="font-weight: bold;">Harga
		Jual Per Year</span></td>
		<td></td>
		<td style="text-align: right;">{{number_format($marketing_cost->sales_price_per_year,0) }}</td>
		</tr>
		<tr>
		<td><span style="font-weight: bold;">Handling
		Fee</span></td>
		<td></td>
		<td style="text-align: right;">{{$marketing_cost->handling_fee}}%</td>
		</tr>
		@foreach($packagings as $key => $packaging)
		<tr>
		<td></td>
		<td style="text-align: right;">{{ number_format($packaging->total_cost_day,0) }}</td>
		<td style="text-align: right;">{{ number_format($packaging->total_cost_year,0) }}</td>
		</tr>
		 @endforeach 
		<tr>
		<th>PROFIT (%)</th>
		<th style="text-align: right;">{{ number_format($total_cost_f->t_cost_day + $marketing_cost->sales_price_per_day,0) }}</th>
		<th style="text-align: right;">{{ number_format($total_cost_f->t_cost_year + $marketing_cost->sales_price_per_year,0) }}</th>
		</tr>
		<tr>
		<td></td>
		<td style="text-align: right;">{{ number_format(((($total_cost_f->t_cost_day + $marketing_cost->sales_price_per_day) - ($marketing_cost->sales_price_per_day))) / ($marketing_cost->sales_price_per_day) * 100,2)  }}</td>
		<td style="text-align: right;">{{ number_format(((($total_cost_f->t_cost_year + $marketing_cost->sales_price_per_year) - $marketing_cost->sales_price_per_year)) / ($marketing_cost->sales_price_per_year) * 100,2)  }}</td>
		</tr>
		</tbody>
	</table>
</div>
</body>
</html>