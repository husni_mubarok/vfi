@extends('layouts.home.template')
@section('content') 
<div class="header_menu mobile-hide">
	<a href="{{ URL::route('editor.type_item.index') }}">TYPE ITEM</a> |
	<a href="{{ URL::route('editor.category_item.index') }}">CATEGORY ITEM</a> |
	<a href="{{ URL::route('editor.master_item.index') }}">MASTER ITEM</a> |
	<a href="{{ URL::route('editor.unit.index') }}">SATUAN</a> |
	<a href="{{ URL::route('editor.custom_detail.index') }}">CUSTOM DETAIL</a>
</div> 
<section class="content box box-solid">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<div class="x_panel">
					<h2>
						<i class="fa fa-bar-chart"></i> Custom Detail
						<a href="{{ URL::route('editor.custom_detail.create') }}" class="btn btn-default btn-lg pull-right"><i class="fa fa-plus"></i> Add</a>
					</h2>
					<hr>
					<div class="x_content">
						<table id="CustomDetailTable" class="table table-striped dataTable">
							<thead>
								<tr>
									<th>#</th>
									<th>Name</th>
									<th>Value</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach($custom_details as $key => $custom_detail)
								<tr>
									<td>{{$key+1}}</td>
									<td>{{$custom_detail->name}}</td>
									<td>{{$custom_detail->value}}</td>
									<td align="center">
										<div class="col-md-2 nopadding">
											<a href="{{ URL::route('editor.custom_detail.edit', [$custom_detail->id]) }}" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i></a>
										</div>
										<div class="col-md-2 nopadding">
											{!! Form::open(array('route' => ['editor.custom_detail.delete', $custom_detail->id], 'method' => 'delete', 'class'=>'delete'))!!}
											{{ csrf_field() }}	                    				
											<button type="submit" class="btn btn-default btn-sm"><i class="fa fa-trash"></i></a></button>
											{!! Form::close() !!}
										</div>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
	$(document).ready(function () {
		$("#CustomDetailTable").DataTable(
		{
			"scrollX": true
		});
	});
</script>
<script>
	$(".delete").on("submit", function(){
		return confirm("Do you want to delete this custom detail?");
	});
</script>
@stop