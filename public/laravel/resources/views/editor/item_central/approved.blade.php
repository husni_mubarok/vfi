@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><a href="{{ URL::route('editor.item_central.index') }}"><i class="fa fa-dot-circle-o"></i> Item Central</a></li>
	</ol>
</section>

<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12"> 
				<div class="col-md-12">
					<div class="x_panel">
						<h2>
							@if(isset($item_central))
							<i class="fa fa-pencil"></i>
							@else
							<i class="fa fa-plus"></i> 
							@endif
							&nbsp;ItemCentral
						</h2>
					</div>
					<hr>
					@include('errors.error')
					@if(isset($item_central))
					{!! Form::model($item_central, array('route' => ['editor.item_central.updaterequset', $item_central->id], 'method' => 'PUT', 'class'=>'update', 'id' => 'form_approval'))!!}
					@else
					{!! Form::open(array('route' => 'editor.item_central.store', 'class'=>'create'))!!}
					@endif
					{{ csrf_field() }}
					<div class="col-md-6">
						<div class="x_content"> 
							<div class="col-md-12 col-sm-12 col-xs-12 form-group">

								{{ Form::label('start_date', 'Start Date') }}
								{{ Form::text('start_date', old('start_date'), array('class' => 'form-control', 'placeholder' => 'Start Date*', 'required' => 'true', 'id' => 'start_date', 'disabled' => 'disabled')) }}<br/>

								{{ Form::label('end_date', 'End Date') }}
								{{ Form::text('end_date', old('end_date'), array('class' => 'form-control', 'placeholder' => 'End Date*', 'required' => 'true', 'id' => 'end_date', 'disabled' => 'disabled')) }}<br/> 

								{{ Form::label('Consummable') }} 
								<select class="form-control" required="true" name="consumable" disabled="disabled">
									<option value=0>No</option>
									<option value=1>Yes</option> 
								</select><br/>  
							</div>
						</div>
					</div>

					<div class="col-md-6">
						<div class="x_content">
							<div class="col-md-12 col-sm-12 col-xs-12 form-group">
								
								{{ Form::label('total_item_central', 'Total ItemCentral') }}
								{{ Form::number('total_item_central', old('total_item_central'), array('class' => 'form-control', 'placeholder' => 'Estimated Total*', 'required' => 'true', 'disabled' => 'disabled')) }}<br/>

								{{ Form::label('total_cashin', 'Total Cashin') }}
								{{ Form::number('total_cashin', old('total_cashin'), array('class' => 'form-control', 'placeholder' => 'Total Cashin*', 'required' => 'true')) }}<br/>

								{{ Form::label('total_cashout', 'Total Cashout') }}
								{{ Form::number('total_cashout', old('total_cashout'), array('class' => 'form-control', 'placeholder' => 'Total Cashout*', 'required' => 'true', 'disabled' => 'disabled')) }} <br/>

								{{ Form::label('comment', 'Comment') }}
								{{ Form::text('comment', old('comment'), array('class' => 'form-control', 'placeholder' => 'Comment*', 'required' => 'true')) }}  
							</div>
						</div>
					</div> 

					<div class="col-md-12">   
						<div class="box-body">
							<div class="row"> 
								<table class="table table-striped table-hover" id="item_centralTable">
									<thead>
										<tr>
											<th width="20%">Item Item Central</th>
											<th width="10%">Quantity</th> 
											<th width="15%">Unit Price</th>
											<th width="10%">UOM</th>
											<th width="15%">Nota Number</th>
											<th width="10%">Date</th> 
											<th width="15%">Total</th> 
										</tr>
									</thead>
									<tbody> 
										@foreach($item_central_detail as $key => $item_central_details)
										<tr>
											<td>
												{{$item_central_details->item_name}} 
											</td>
											<td>
												{{$item_central_details->quantity}} 
											</td> 
											<td>  
												{{$item_central_details->price}} 
											</td> 
											<td>
												{{$item_central_details->uom}} 
											</td> 
											<td>
												{{$item_central_details->nota_number}} 
											</td> 
											<td>
												{{$item_central_details->date}} 
											</td> 
											<td>
												<p id="total{{$key}}">{{$item_central_details->total}} </p>
											</td> 
										</tr> 
										@endforeach
									</tbody>
								</table>
							</div> 
							<!-- /.box-body -->
						</div>
						<hr> 
						<button type="button" class="btn btn-success pull-right" id="btn_confirm" data-toggle="modal" data-target="#modal_confirm">Approve</button>
						<a href="{{ URL::route('editor.item_central.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
					</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</section>
@stop

@section('modal')
<div class="modal fade" id="modal_confirm">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h4 class="modal-title">Approve?</h4>
      		</div> 
      		<div class="modal-footer">
      			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
      			<button type="button" id="btn_submit" class="btn btn-success"><i class="fa fa-check"></i> Yes</button>
      		</div>
    	</div>
  	</div>
</div>
@stop

@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
	$(document).ready(function () {
		$("#item_centralTable").DataTable(
		{
			"language": {
				"emptyTable": "-"
			}
		} 
		);
	});
</script>  
<script type="text/javascript">
	$('#btn_submit').on('click', function()
	{
		$('#form_approval').submit();
	});
</script>
@stop

