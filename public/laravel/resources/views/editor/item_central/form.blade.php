<style type="text/css">
	.modal {
		text-align: center;
		padding: 0!important;
	}

	.modal:before {
		content: '';
		display: inline-block;
		height: 100%;
		vertical-align: middle;
		margin-right: -4px;
	}

	.modal-dialog {
		display: inline-block;
		text-align: left;
		vertical-align: middle;
	}
</style>

@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><a href="{{ URL::route('editor.item_central.index') }}"><i class="fa fa-dot-circle-o"></i> Item Central</a></li>
	</ol>
</section>

<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12"> 
				<div class="col-md-12">
					<div class="x_panel">
						<h2>
							@if(isset($item_central))
							<i class="fa fa-pencil"></i>
							@else
							<i class="fa fa-plus"></i> 
							@endif
							&nbsp;Item Central
						</h2>
					</div>
					<hr>
					<a href="{{ url('editor/item_central/storeexport/xls') }}"><button class="btn btn-success"><i class="fa fa-download"></i> Download Template .xls</button></a>

					<hr>

					{!! Form::open(array('route' => 'editor.item_central.storeimport', 'class'=>'create', 'files' => 'true'))!!}
					{{ csrf_field() }}	   
					{{ Form::label('import_file', 'Import Item from Excel File (.ods)') }}<br>
					Max 5MB
					{{ Form::file('import_file') }} <br/>                 				
					<button type="submit" class="btn btn-success"><i class="fa fa-file-excel-o"></i> Import</button>
					{!! Form::close() !!}  

					<hr>
					
					@include('errors.error')
					@if(isset($item_central))
					{!! Form::model($item_central, array('route' => ['editor.item_central.update', $item_central->id], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_itemcentral'))!!}
					@else
					{!! Form::open(array('route' => 'editor.item_central.store', 'class'=>'create', 'id'=>'form_itemcentral'))!!}
					@endif
					{{ csrf_field() }}
					<div class="col-md-6">
						<div class="x_content"> 
							
							{{ Form::label('date', 'Date') }}
							{{ Form::text('date', old('date'), array('class' => 'form-control', 'placeholder' => 'Date*', 'required' => 'true', 'id' => 'date')) }}<br/>

							{{ Form::label('total_item_central', 'Estimated Total') }}
							{{ Form::number('total_item_central', old('total_item_central'), array('class' => 'form-control', 'placeholder' => 'Estimated Total*', 'required' => 'true', 'disabled' => 'disabled')) }}<br/>
							
						</div>
					</div>

					<div class="col-md-6">
						<div class="x_content">
							

							{{ Form::label('total_item_central', 'Actual Total') }}
							{{ Form::number('total_invoice', old('total_invoice'), array('class' => 'form-control', 'placeholder' => 'Actual Total*', 'required' => 'true', 'disabled' => 'disabled')) }}<br/>

							{{ Form::label('comment', 'Comment') }}
							{{ Form::text('comment', old('comment'), array('class' => 'form-control', 'placeholder' => 'Comment*', 'required' => 'true')) }} <br/>

							
						</div>
					</div> 


					<div class="col-md-12">  
						<ul class="nav nav-tabs">
							<li class="active"><a data-toggle="tab" href="#consummable">Consummable</a></li>
							<li><a data-toggle="tab" href="#supply">Supply</a></li>   
						</ul>
						<div class="tab-content">
							<div id="consummable" class="tab-pane fade in active"> 
								<div class="box-body">
									<div class="div_overflow"> 

										<table class="table table-striped table-hover" id="item_centralTablex" style="min-width: 800px">
											<thead>
												<tr>
													<th width="20%">Item Name</th>
													<th width="10%">UOM</th> 
													<th width="10%">Stock</th>
													<th width="10%">Qty to Buy</th>
													<th width="10%">Unit Price</th>
													<th width="15%">Total</th> 
												</tr>
											</thead>
											<tbody>  
												@foreach($item_central_detail as $key => $item_central_details)
												<tr>
													<td>
														{{$item_central_details->item_name}} 
													</td>
													<td>
														{{ Form::text('detail['.$item_central_details->id.'][uom]', old($item_central_details->uom.'[uom]', $item_central_details->uom), ['id' => 'uom', 'min' => '0', 'class' => 'form-control', 'placeholder' => 'UOM*']) }}
													</td>  
													<td> 
														@if($item_central_details->quantity_stock=='')
														{{ Form::number('detail['.$item_central_details->id.'][quantity_stock]', old($item_central_details->quantity_stock.'[quantity_stock]',0), ['id' => 'quantity_stock'.$item_central_details->id, 'min' => '0', 'class' => 'form-control', 'placeholder' => 'Quantity*', 'oninput' => 'total()']) }}
														@else
														{{ Form::number('detail['.$item_central_details->id.'][quantity_stock]', old($item_central_details->quantity_stock.'[quantity_stock]', $item_central_details->quantity_stock), ['id' => 'quantity_stock'.$item_central_details->id, 'min' => '0', 'class' => 'form-control', 'placeholder' => 'Quantity*', 'oninput' => 'total()']) }}
														@endif 
													</td> 
													<td> 
														@if($item_central_details->quantity=='')
														{{ Form::number('detail['.$item_central_details->id.'][quantity]', old($item_central_details->quantity.'[quantity]',0), ['id' => 'quantity'.$item_central_details->id, 'min' => '0', 'class' => 'form-control', 'placeholder' => 'Quantity*', 'oninput' => 'total()']) }}
														@else
														{{ Form::number('detail['.$item_central_details->id.'][quantity]', old($item_central_details->quantity.'[quantity]', $item_central_details->quantity), ['id' => 'quantity'.$item_central_details->id, 'min' => '0', 'class' => 'form-control', 'placeholder' => 'Quantity*', 'oninput' => 'total()']) }}
														@endif 
													</td> 
													<td> 
														@if($item_central_details->price=='')

														{{ Form::text('detail['.$item_central_details->id.'][price_show]', old($item_central_details->id.'[price_show]', $item_central_details->price_show), ['class' => 'form-control', 'id' => 'price_show_'.$item_central_details->id, 'min' => '0', 'oninput' => 'cal_sparator'.$item_central_details->id.'(); total();']) }}

														{{ Form::hidden('detail['.$item_central_details->id.'][price]', old($item_central_details->price.'[price]', 0), ['id' => 'price'.$item_central_details->id, 'min' => '0', 'class' => 'form-control', 'placeholder' => 'Unit Price*', 'oninput' => 'total()']) }}
														@else

														{{ Form::text('detail['.$item_central_details->id.'][price_show]', old($item_central_details->id.'[price_show]', $item_central_details->price_show), ['class' => 'form-control', 'id' => 'price_show_'.$item_central_details->id, 'min' => '0', 'oninput' => 'cal_sparator'.$item_central_details->id.'(); total();']) }}

														{{ Form::hidden('detail['.$item_central_details->id.'][price]', old($item_central_details->price.'[price]', $item_central_details->price), ['id' => 'price'.$item_central_details->id, 'min' => '0', 'class' => 'form-control', 'placeholder' => 'Unit Price*', 'oninput' => 'total()']) }}
														@endif 
													</td> 
												</td> 
												<td> 
													<p id="total{{$item_central_details->id}}">{{$item_central_details->total}} </p>
												</td> 
											</tr> 


											<script type="text/javascript">
					//item central 
					function cal_sparator{{$item_central_details->id}}() {
						var price_show{{$item_central_details->id}} = document.getElementById('price_show_{{$item_central_details->id}}').value;
						var result{{$item_central_details->id}} = document.getElementById('price{{$item_central_details->id}}');
						var rsprice{{$item_central_details->id}} = (price_show{{$item_central_details->id}});
						result{{$item_central_details->id}}.value = rsprice{{$item_central_details->id}}.replace(/,/g, ""); 
						
						n{{$item_central_details->id}}= document.getElementById('price_show_{{$item_central_details->id}}');

						n{{$item_central_details->id}}.onkeyup=n{{$item_central_details->id}}.onchange= function(e){
							e=e|| window.event; 
							var who=e.target || e.srcElement,temp{{$item_central_details->id}};
							if(who.id==='price{{$item_central_details->id}}')  temp{{$item_central_details->id}}= validDigits(who.value,0); 
							else temp{{$item_central_details->id}}= validDigits(who.value);
							who.value= addCommas(temp{{$item_central_details->id}});
						}   
						n{{$item_central_details->id}}.onblur= function(){
							var 
							temp{{$item_central_details->id}}=parseFloat(validDigits(n{{$item_central_details->id}}.value));
							if(temp{{$item_central_details->id}})n{{$item_central_details->id}}.value=addCommas(temp{{$item_central_details->id}}.toFixed(0));
						}
					}

				</script>
				@endforeach
			</tbody>
		</table>
	</div> 
	<!-- /.box-body -->
</div>
</div>
<div id="supply" class="tab-pane"> 
	<div class="box-body">
		<div class="div_overflow"> 
			<table class="table table-striped table-hover" id="item_centralTablex"  style="min-width: 800px">
				<thead>
					<tr>
						<th width="20%">Item Name</th>
						<th width="5%">UOM</th> 
						<th width="5%">Stock</th>
						<th width="5%">Qty to Buy</th>
						<th width="10%">Unit Price</th>
						<th width="15%">Total</th> 
					</tr>
				</thead>
				<tbody>  
					@foreach($item_central_detail_supply as $key => $item_central_details)
					<tr>
						<td>
							{{$item_central_details->item_name}} 
						</td>
						<td>
							{{ Form::text('detail['.$item_central_details->id.'][uom]', old($item_central_details->uom.'[uom]', $item_central_details->uom), ['id' => 'uom', 'min' => '0', 'class' => 'form-control', 'placeholder' => 'UOM*']) }}
						</td>  
						<td> 
							@if($item_central_details->quantity_stock=='')
							{{ Form::number('detail['.$item_central_details->id.'][quantity_stock]', old($item_central_details->quantity_stock.'[quantity_stock]',0), ['id' => 'quantity_stock'.$key, 'min' => '0', 'class' => 'form-control', 'placeholder' => 'Quantity*', 'oninput' => 'total()']) }}
							@else
							{{ Form::number('detail['.$item_central_details->id.'][quantity_stock]', old($item_central_details->quantity_stock.'[quantity_stock]', $item_central_details->quantity_stock), ['id' => 'quantity_stock'.$key, 'min' => '0', 'class' => 'form-control', 'placeholder' => 'Quantity*', 'oninput' => 'total()']) }}
							@endif 
						</td> 
						<td> 
							@if($item_central_details->quantity=='')
							{{ Form::number('detail['.$item_central_details->id.'][quantity]', old($item_central_details->quantity.'[quantity]',0), ['id' => 'quantity'.$item_central_details->id, 'min' => '0', 'class' => 'form-control', 'placeholder' => 'Quantity*', 'oninput' => 'total()']) }}
							@else
							{{ Form::number('detail['.$item_central_details->id.'][quantity]', old($item_central_details->quantity.'[quantity]', $item_central_details->quantity), ['id' => 'quantity'.$item_central_details->id, 'min' => '0', 'class' => 'form-control', 'placeholder' => 'Quantity*', 'oninput' => 'total()']) }}
							@endif 
						</td> 
						<td> 
							@if($item_central_details->price=='')

							{{ Form::text('detail['.$item_central_details->id.'][price_show]', old($item_central_details->id.'[price_show]', $item_central_details->price_show), ['class' => 'form-control', 'id' => 'price_show_'.$item_central_details->id, 'min' => '0', 'oninput' => 'cal_sparator'.$item_central_details->id.'(); total();']) }}

							{{ Form::hidden('detail['.$item_central_details->id.'][price]', old($item_central_details->price.'[price]', 0), ['id' => 'price'.$item_central_details->id, 'min' => '0', 'class' => 'form-control', 'placeholder' => 'Unit Price*', 'oninput' => 'total()']) }}
							@else

							{{ Form::text('detail['.$item_central_details->id.'][price_show]', old($item_central_details->id.'[price_show]', $item_central_details->price_show), ['class' => 'form-control', 'id' => 'price_show_'.$item_central_details->id, 'min' => '0', 'oninput' => 'cal_sparator'.$item_central_details->id.'(); total();']) }}

							{{ Form::hidden('detail['.$item_central_details->id.'][price]', old($item_central_details->price.'[price]', $item_central_details->price), ['id' => 'price'.$item_central_details->id, 'min' => '0', 'class' => 'form-control', 'placeholder' => 'Unit Price*', 'oninput' => 'total()']) }}
							@endif 
						</td> 
					</td> 
					<td> 
						<p id="total{{$item_central_details->id}}">{{$item_central_details->total}} </p>
					</td> 
				</tr> 

				<script type="text/javascript">
					//item central 
					function cal_sparator{{$item_central_details->id}}() {
						var price_show{{$item_central_details->id}} = document.getElementById('price_show_{{$item_central_details->id}}').value;
						var result{{$item_central_details->id}} = document.getElementById('price{{$item_central_details->id}}');
						var rsprice{{$item_central_details->id}} = (price_show{{$item_central_details->id}});
						result{{$item_central_details->id}}.value = rsprice{{$item_central_details->id}}.replace(/,/g, ""); 
						
						n{{$item_central_details->id}}= document.getElementById('price_show_{{$item_central_details->id}}');

						n{{$item_central_details->id}}.onkeyup=n{{$item_central_details->id}}.onchange= function(e){
							e=e|| window.event; 
							var who=e.target || e.srcElement,temp{{$item_central_details->id}};
							if(who.id==='price{{$item_central_details->id}}')  temp{{$item_central_details->id}}= validDigits(who.value,0); 
							else temp{{$item_central_details->id}}= validDigits(who.value);
							who.value= addCommas(temp{{$item_central_details->id}});
						}   
						n{{$item_central_details->id}}.onblur= function(){
							var 
							temp{{$item_central_details->id}}=parseFloat(validDigits(n{{$item_central_details->id}}.value));
							if(temp{{$item_central_details->id}})n{{$item_central_details->id}}.value=addCommas(temp{{$item_central_details->id}}.toFixed(0));
						}
					}

				</script>
				@endforeach
			</tbody>
		</table> 
	</div> 
</div>
</div> 
<button type="button" data-toggle="modal" data-target="#modal_itemcentral" class="btn btn-success pull-right"><i class="fa fa-check"></i> Submit</button>
<a href="{{ URL::route('editor.item_central.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
</div>
{!! Form::close() !!} 
</div>
</div>
</div>
</div>
</section>

@stop


@section('modal')
<div class="modal fade" id="modal_itemcentral">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Save this item central?</h4>
			</div> 
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
				<button type="button" id="btn_submit" class="btn btn-success"><i class="fa fa-check"></i> OK</button>
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')

<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
	$(document).ready(function () {
		$("#item_centralTable").DataTable(
		{
			"language": {
				"emptyTable": "-"
			}
		} 
		);
	});
	function numberWithCommas(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
</script>  
<script type="text/javascript">
	$(document).ready(function(){
		@foreach($item_central_detail as $key => $item_central_details)
		$('#date{{$item_central_details->id}}').datepicker({
			sideBySide: true,
			format: 'yyyy-mm-dd',
		}); 
		@endforeach
	});

	function total(){
		@foreach($item_central_detail as $key => $item_central_details)
		var quantity{{$item_central_details->id}} = document.getElementById('quantity{{$item_central_details->id}}').value;
		var price{{$item_central_details->id}} = document.getElementById('price{{$item_central_details->id}}').value;
		document.getElementById('total{{$item_central_details->id}}').innerHTML = numberWithCommas(parseFloat(quantity{{$item_central_details->id}}) * parseFloat(price{{$item_central_details->id}}));
		@endforeach

		@foreach($item_central_detail_supply as $key => $item_central_details)
		var quantity{{$item_central_details->id}} = document.getElementById('quantity{{$item_central_details->id}}').value;
		var price{{$item_central_details->id}} = document.getElementById('price{{$item_central_details->id}}').value;
		document.getElementById('total{{$item_central_details->id}}').innerHTML = numberWithCommas(parseFloat(quantity{{$item_central_details->id}}) * parseFloat(price{{$item_central_details->id}}));
		@endforeach
	}

	$('#btn_submit').on('click', function()
	{
		$('#form_itemcentral').submit();
	});
</script>
@stop

