@extends('layouts.home.template')
@section('content')
<section class="content box box-solid">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<div class="x_panel">
					<h2>
						<i class="fa fa-cubes"></i> Production Export List
						
						<a href="{{ URL::route('editor.production_export.create') }}" class="btn btn-primary btn-lg pull-right"><i class="fa fa-plus"></i> Add</a>
						
					</h2>
					<hr>
					<div class="x_content">
						<table id="productTable" class="table table-striped dataTable">
							<thead>
								<tr>
									<th>#</th>
									<th>Doc Code</th>
									<th>Date</th>
									<th>Qty</th>
								</tr>
							</thead>
							<tbody>
								@foreach($production_exports as $key => $production_export)
								<tr>
									<td>{{$key+1}}</td>
									<td>{{$production_export->doc_code}}</td>
									<td>{{$production_export->date}}</td>
									<td>{{$production_export->quantity}}</td>
									<td align="center">
										<div class="col-md-2 nopadding">
											<a href="{{ URL::route('editor.production_export.edit', [$production_export->id]) }}" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i></a>
										</div>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
	$(function() {
		var table = $("#productTable").DataTable({
			processing: true,
			serverSide: true,
			"pageLength": 25,
			"scrollY": "100px"
		});
	});
</script>
@stop