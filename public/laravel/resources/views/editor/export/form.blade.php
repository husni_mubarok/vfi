  @extends('layouts.editor.template')
  @section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      CMS
      <small>Content Management System</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
      <li class="active"><a href="{{ URL::route('editor.production_export.index') }}"><i class="fa fa-cube"></i> Export</a></li>
    </ol>
  </section>

  <section class="content">
   <section class="content box box-solid">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
       <div class="col-md-1"></div>
       <div class="col-md-5">
         <div class="x_panel">
          <h2>
           @if(isset($production_export))
           <i class="fa fa-pencil"></i>
           @else
           <i class="fa fa-plus"></i> 
           @endif
           <i class="fa fa-cube"></i> Export
         </h2>
         <hr>
         <div class="x_content">
           @include('errors.error')
           @if(isset($production_export))
           {!! Form::model($production_export, array('route' => ['editor.production_export.update', $production_export->id], 'method' => 'PUT'))!!}
           @else
           {!! Form::open(array('route' => 'editor.production_export.store'))!!}
           @endif
           {{ csrf_field() }}
           <div class="col-md-12 col-sm-12 col-xs-12 form-group">
            {{ Form::label('Store') }}
            {{ Form::hidden('store_id', old('store_id'), array('class' => 'form-control', 'placeholder' => 'Doc Code*', 'required' => 'true', 'id' => 'store_id')) }}<br/>
            {{ Form::text('name', old('name'), array('class' => 'form-control', 'placeholder' => 'Name*', 'required' => 'true', 'id' => 'name')) }}<br/>
            {{ Form::text('criteria', old('criteria'), array('class' => 'form-control', 'placeholder' => 'Criteria*', 'required' => 'true', 'id' => 'criteria')) }}<br/>
            <a href="#" class="btn btn-primary" id="lookup"> Lookup</a><br/>
            {{ Form::label('doc_code', 'Doc Code') }}
            {{ Form::text('doc_code', old('doc_code'), array('class' => 'form-control', 'placeholder' => 'Doc Code*', 'required' => 'true')) }}<br/>
            {{ Form::label('date', 'Date') }}
            {{ Form::text('date', old('date'), array('class' => 'form-control', 'placeholder' => 'Date & Time*', 'required' => 'true', 'id' => 'dateprod')) }}<br/>
            {{ Form::label('quantity', 'Qty') }}
            {{ Form::text('quantity', old('quantity'), array('class' => 'form-control', 'placeholder' => 'Qty', 'id' => 'quantity', 'onchange' => 'stockval()')) }}<br/>
            {{ Form::hidden('quantity1', old('quantity1'), array('class' => 'form-control', 'placeholder' => 'Qty', 'id' => 'quantity1')) }}<br/>
            <button type="submit" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
          </div>
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
</div>
</section>
</section>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Production</h4>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-responsive" id="productionResult">
          <thead>
            <tr>
              <th>No</th>
              <th>Name</th>
              <th>Criteria</th>
              <th>Block Weight</th>
              <th>Start At</th>
              <th>Finish At</th>
              <th>Prod Qty</th>
              <th>Export Qty</th>
              <th>Remain Qty</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($tempstorage as $tempstorages)
            <tr>
              <td>{{$tempstorages->id}}</td>
              <td>{{$tempstorages->name}}</td>
              <td>{{$tempstorages->criteria}}</td>
              <td>{{$tempstorages->block_weight}}</td>
              <td>{{$tempstorages->started_at}}</td>
              <td>{{$tempstorages->finished_at}}</td>
              <td>{{$tempstorages->prodqty}}</td>
              <td>{{$tempstorages->exportqty}}</td>
              <td>{{$tempstorages->remainqty}}</td>
              <td><a href="javascript:void(0)" class="btn btn-success" id="btnstore" onclick="addValue(this, {{ $tempstorages->id }})">Store</a></td>
            </tr>
            @endforeach

          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  @stop
  @section('scripts')
  <script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script>
  <script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>

  <script>
    $(document).ready(function () {
     $('#dateprod').datetimepicker({
      sideBySide: true,
      format: 'YYYY-MM-DD HH:mm',
    });
   });

    function addValue(str, id){
      var prd = id;
      var store_id = $(str).closest('tr').find('td:eq(1)').text();
      var name = $(str).closest('tr').find('td:eq(1)').text();
      var criteria = $(str).closest('tr').find('td:eq(2)').text(); 
      var remainqty = $(str).closest('tr').find('td:eq(8)').text();

      $("#store_id").val(id);
      $("#quantity").val(remainqty);
      $("#name").val(name);
      $("#criteria").val(criteria);
      $("#quantity1").val(remainqty);
      console.log(id);
      $('#myModal').modal("hide");
    }
    $(document).ready(function(){
      $("#productionResult").DataTable({
        "pageLength": 3
      });

      $("#lookup").click(function(){
        console.log('testing');
        $('#myModal').modal();
      });

      $('#datetime').datetimepicker({
        sideBySide: true,
        format: 'YYYY-MM-DD HH:mm',
      });
    });

    function stockval() {    
      var quantity = $('#quantity').val();
      var quantity1 = $('#quantity1').val();
      if ( parseInt(quantity) > parseInt(quantity1) ) {
        alert("over stock"); 
      }
    };
  </script>
  @stop