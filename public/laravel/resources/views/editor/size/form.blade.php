@extends('layouts.home.template')
@section('content')
<div class="header_menu">
    <a href="{{ URL::route('editor.size.index') }}">SIZE</a>
    <a href="{{ URL::route('editor.product_type.index') }}">PRODUCT TYPE</a>
    <a href="{{ URL::route('editor.product.index') }}">PRODUCT</a>
    <a href="{{ URL::route('editor.supplier.index') }}">SUPPLIER</a>
</div>
@actionStart('size', 'create|update')
<section class="content">
	<div class="row">
	    <div class="col-md-12 col-sm-12 col-xs-12">
	    	<div class="col-md-1"></div>
	    	<div class="col-md-5">
		        <div class="x_panel">
	                <h2>
	                	@if(isset($size))
	                	<i class="fa fa-pencil"></i>
	                	@else
	                	<i class="fa fa-plus"></i> 
	                	@endif
	                	<i class="fa fa-bar-chart"></i> Size
                	</h2>
	                <hr>
		            <div class="x_content">
		                @include('errors.error')
                        @if(isset($size))
		                {!! Form::model($size, array('route' => ['editor.size.update', $size->id], 'method' => 'PUT'))!!}
	                    @else
	                    {!! Form::open(array('route' => 'editor.size.store'))!!}
	                    @endif
	                    {{ csrf_field() }}
	                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
	                    	{{ Form::label('min_value', 'Minimum Value') }}
	                        {{ Form::number('min_value', old('min_value'), array('class' => 'form-control', 'placeholder' => 'Minimum Value*', 'required' => 'true')) }}<br/>

	                        {{ Form::label('max_value', 'Maximum Value') }}
	                        {{ Form::number('max_value', old('max_value'), array('class' => 'form-control', 'placeholder' => 'Maximum Value*', 'required' => 'true')) }}<br/>

	                        {{ Form::label('note', 'Note') }}
	                        {{ Form::text('note', old('note'), array('class' => 'form-control', 'placeholder' => 'Note', 'required' => 'true')) }}<br/>

                            <button type="submit" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
                    	</div>
                        {!! Form::close() !!}
		            </div>
		        </div>
	        </div>
	    </div>
	</div>
</section>
@actionEnd
@stop