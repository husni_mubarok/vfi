@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active"><a href="#"><i class="fa fa-bar-chart"></i> Size</a></li>
  </ol>
</section>
@actionStart('size', 'read')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-10">
			        <div class="x_panel">
		                <h2>
		                	<i class="fa fa-bar-chart"></i> Size List
		                	<a href="{{ URL::route('editor.size.create') }}" class="btn btn-primary btn-lg pull-right"><i class="fa fa-plus"></i> Add</a>
	                	</h2>
		                <hr>
			            <div class="x_content">
			                <table id="supplierTable" class="table table-striped dataTable">
							  	<thead>
							  	  	<tr>
								      	<th>#</th>
								      	<th>Min. value</th>
								      	<th>Max. value</th>
								      	<th>Note</th>
								      	<th></th>
							    	</tr>
							  	</thead>
							  	<tbody>
							    @foreach($sizes as $key => $size)
							    	<tr>
							      		<td>{{$key+1}}</td>
								      	<td>{{$size->min_value}}</td>
								      	<td>{{$size->max_value}}</td>
								      	<td>{{$size->note}}</td>
								      	<td align="center">
								      		@actionStart('size', 'update')
								      		<div class="col-md-2 nopadding">
								      			<a href="{{ URL::route('editor.size.edit', [$size->id]) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
								      		</div>
								      		@actionEnd

								      		@actionStart('size', 'delete')
							      			<div class="col-md-2 nopadding">
								      			{!! Form::open(array('route' => ['editor.size.delete', $size->id], 'method' => 'delete'))!!}
		                    					{{ csrf_field() }}	                    				
								      			<button type="submit" class="btn btn-primary btn-sm" onclick="return confirm('Confirm delete?')"><i class="fa fa-trash"></i></a></button>
								      			{!! Form::close() !!}
							      			</div>
							      			@actionEnd
							      		</td>
								    </tr>
							    @endforeach
								</tbody>
							</table>
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@actionEnd
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function () {
    $("#supplierTable").DataTable(
    	{
    		"scrollX": true
    	});
    });
</script>
@stop