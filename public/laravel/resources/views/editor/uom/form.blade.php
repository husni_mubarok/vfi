@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li class="active"><a href="{{ URL::route('editor.uom.index') }}"><i class="fa fa-bar-chart"></i> Unit of Measure</a></li>
  </ol>
</section>

<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-5">
			        <div class="x_panel">
		                <h2>
		                	@if(isset($uom))
		                	<i class="fa fa-pencil"></i>
		                	@else
		                	<i class="fa fa-plus"></i> 
		                	@endif
		                	<i class="fa fa-bar-chart"></i> Unit of Measure
	                	</h2>
		                <hr>
			            <div class="x_content">
			                @include('errors.error')
	                        @if(isset($uom))
			                {!! Form::model($uom, array('route' => ['editor.uom.update', $uom->id], 'method' => 'PUT'))!!}
		                    @else
		                    {!! Form::open(array('route' => 'editor.uom.store'))!!}
		                    @endif
		                    {{ csrf_field() }}
		                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
		                    	{{ Form::label('Category') }}
		                    	{{ Form::select('uom_category_id', $uom_category_list, old('uom_category_id'), array('class' => 'form-control', 'required' => 'true')) }}<br/>

		                    	{{ Form::label('Parent') }}
		                    	{{ Form::select('parent_id', $uom_list, old('parent_id'), array('class' => 'form-control', 'required' => 'true')) }}<br/>

		                    	{{ Form::label('name', 'Name') }}
		                        {{ Form::text('name', old('name'), array('class' => 'form-control', 'placeholder' => 'Name*', 'required' => 'true')) }}<br/>

		                        {{ Form::label('value', 'Value') }}
		                        {{ Form::number('value', old('value'), array('class' => 'form-control', 'placeholder' => 'Value*', 'required' => 'true', 'step' => 0.01)) }}<br/>

	                            <button type="submit" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
	                    	</div>
	                        {!! Form::close() !!}
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@stop