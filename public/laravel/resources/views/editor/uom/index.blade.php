@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li class="active"><a href="#"><i class="fa fa-bar-chart"></i> Unit of Measure</a></li>
  </ol>
</section>
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-10">
			        <div class="x_panel">
		                <h2>
		                	<i class="fa fa-bar-chart"></i> Unit of Measure List
		                	<a href="{{ URL::route('editor.uom.create') }}" class="btn btn-default btn-lg pull-right"><i class="fa fa-plus"></i> Add</a>
	                	</h2>
		                <hr>
			            <div class="x_content">
			                <table id="uomCategoryTable" class="table table-striped dataTable">
							  	<thead>
							  	  	<tr>
								      	<th>#</th>
								      	<th>Name</th>
								      	<th>Parent</th>
								      	<th>Value</th>
								      	<th>Category</th>
								      	<th></th>
							    	</tr>
							  	</thead>
							  	<tbody>
							    @foreach($uoms as $key => $uom)
							    	<tr>
							      		<td>{{$key+1}}</td>
								      	<td>{{$uom->name}}</td>
								      	<td>
								      		@if($uom->parent_uom == null)
								      		-
								      		@else
								      		{{$uom->parent_uom->name}}
								      		@endif
							      		</td>
								      	<td>{{$uom->value}} @if($uom->parent_uom) * {{$uom->parent_uom->name}} @endif</td>
								      	<td>{{$uom->uom_category->name}}</td>
								      	<td align="center">
								      		<div class="col-md-2 nopadding">
								      			<a href="{{ URL::route('editor.uom.edit', [$uom->id]) }}" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i></a>
								      		</div>
							      			<div class="col-md-2 nopadding">
								      			{!! Form::open(array('route' => ['editor.uom.delete', $uom->id], 'method' => 'delete'))!!}
		                    					{{ csrf_field() }}	                    				
								      			<button type="submit" class="btn btn-default btn-sm"><i class="fa fa-trash"></i></a></button>
								      			{!! Form::close() !!}
							      			</div>
							      		</td>
								    </tr>
							    @endforeach
								</tbody>
							</table>
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function () {
    $("#uomCategoryTable").DataTable(
    	{
    		"scrollX": true
    	});
    });
</script>
@stop