@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header hidden-xs">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li class="active"><a href="{{ URL::route('editor.religion.index') }}"><i class="fa fa-bar-chart"></i> Religion</a></li>
  </ol>
</section>
<section class="content">
<section class="content box box-solid">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-1"></div>
			<div class="col-md-5">
				<div class="x_panel">
					<h2>
					
						@if(isset($religion))
						<i class="fa fa-pencil"></i>
						@else
						<i class="fa fa-plus"></i> 
						@endif
						<i class="fa fa-bar-chart"></i> Religion
					</h2>
					<hr>
					<div class="x_content">
						@include('errors.error')
						@if(isset($religion))
						{!! Form::model($religion, array('route' => ['editor.religion.update', $religion->id], 'method' => 'PUT', 'class'=>'update'))!!}
						@else
						{!! Form::open(array('route' => 'editor.religion.store', 'class'=>'create'))!!}
						@endif
						{{ csrf_field() }}
						<div class="col-md-12 col-sm-12 col-xs-12 form-group"> 
							{{ Form::label('religion_name', 'Name') }}
							{{ Form::text('religion_name', old('religion_name'), array('class' => 'form-control', 'placeholder' => 'Name*', 'required' => 'true')) }}<br/>

							{{ Form::label('religion_desc', 'Description') }}
							{{ Form::text('religion_desc', old('religion_desc'), array('class' => 'form-control', 'placeholder' => 'Description*', 'required' => 'true')) }}<br/>
							<button type="submit" class="btn btn-success pull-right"><i class="fa fa-check"></i> Save</button>
							<a href="{{ URL::route('editor.religion.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
						</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
</section>
@stop

