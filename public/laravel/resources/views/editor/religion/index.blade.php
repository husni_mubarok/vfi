@extends('layouts.editor.template')
@section('content')
<section class="content-header hidden-xs">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li class="active"><a href="#"><i class="fa fa-bar-chart"></i> Religion</a></li>
  </ol>
</section>
<section class="content">
	<section class="content box box-solid">
		<div class="row">
	    <div class="col-md-12 col-sm-12 col-xs-12">
	    	<div class="col-md-1"></div>
	    	<div class="col-md-12">
		        <div class="x_panel">
	                <h2>
	                	<i class="fa fa-tags"></i> Religion List
	                	@actionStart('religion', 'create')
	                	<a href="{{ URL::route('editor.religion.create') }}" class="btn btn-default btn-lg pull-right"><i class="fa fa-plus"></i> Add</a>
	                	@actionEnd
                	</h2>
	                <hr>
		            <div class="x_content">
		                <table id="drinkTable" class="table table-striped dataTable">
						  	<thead>
								<tr>
									<th>#</th>
									<th>Name</th>
									<th>Description</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach($religions as $key => $religion)
								<tr>
									<td>{{$key+1}}</td>
									<td>{{$religion->religion_name}}</td>
									<td>{{$religion->religion_desc}}</td>
									<td align="center">
										<div class="col-md-2 nopadding">
											@actionStart('religion', 'update')
											<a href="{{ URL::route('editor.religion.edit', [$religion->id]) }}" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i></a>
											@actionEnd
										</div>
										<div class="col-md-2 nopadding">
											{!! Form::open(array('route' => ['editor.religion.delete', $religion->id], 'method' => 'delete', 'class'=>'delete'))!!}
											{{ csrf_field() }}	
											@actionStart('religion', 'delete')                    				
											<button type="submit" class="btn btn-default btn-sm"><i class="fa fa-trash"></i></a></button>
											@actionEnd
											{!! Form::close() !!}
										</div>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
</section>
@stop

@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function () {
    $("#drinkTable").DataTable();
    });
</script>

<!-- Add fancyBox -->
<link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$(".fancybox").fancybox();
	});
</script>
@stop