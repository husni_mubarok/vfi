@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active"><a href="#"><i class="fa fa-truck"></i> Supplier</a></li>
  </ol>
</section>
@actionStart('supplier', 'read')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-10">
			        <div class="x_panel">
		                <h2>
		                	<i class="fa fa-truck"></i> Supplier List
		                	@actionStart('supplier', 'create')
		                	<a href="{{ URL::route('editor.supplier.create') }}" class="btn btn-primary btn-lg pull-right"><i class="fa fa-plus"></i> Add</a>
		                	@actionEnd
	                	</h2>
		                <hr>
			            <div class="x_content">
			                <table id="supplierTable" class="table table-striped dataTable">
							  	<thead>
							  	  	<tr>
								      	<th>#</th>
								      	<th>Name</th>
								      	<th>Address</th>
								      	<th>E-mail Address</th>
								      	<th>Phone Number</th>
								      	<th>Fax</th>
								      	<th>Website</th>
								      	<th>PIC</th>
								      	<th>Bank Account</th>
								      	<th>Note</th>
								      	<th>Products</th>
								      	<th></th>
							    	</tr>
							  	</thead>
							  	<tbody>
							    @foreach($suppliers as $key => $supplier)
							    	<tr>
							      		<td>{{$key+1}}</td>
								      	<td>{{$supplier->name}}</td>
								      	<td>{{$supplier->address}}</td>
								      	<td>{{$supplier->email}}</td>
								      	<td>{{$supplier->phone_number}}</td>
								      	<td>{{$supplier->fax}}</td>
								      	<td>{{$supplier->website}}</td>
								      	<td>{{$supplier->pic}}</td>
								      	<td>{{$supplier->bank_account}}</td>
								      	<td>{{$supplier->note}}</td>
								      	<td>
								      		@actionStart('supplier', 'update')
								      		<a href="{{ URL::route('editor.supplier.edit_product', [$supplier->id]) }}" class="btn btn-primary btn-sm"><i class="fa fa-cubes">&nbsp;{{$supplier->supplier_product->count()}}</i></a>
								      		@actionEnd
							      		</td>
								      	<td align="center">
								      		@actionStart('supplier', 'update')
								      		<div class="col-md-2 nopadding">
								      			<a href="{{ URL::route('editor.supplier.edit', [$supplier->id]) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
								      		</div>
								      		@actionEnd

								      		@actionStart('supplier', 'delete')
							      			<div class="col-md-2 nopadding">
								      			{!! Form::open(array('route' => ['editor.supplier.delete', $supplier->id], 'method' => 'delete'))!!}
		                    					{{ csrf_field() }}	                    				
								      			<button type="submit" class="btn btn-primary btn-sm" onclick="return confirm('Confirm delete?')"><i class="fa fa-trash"></i></a></button>
								      			{!! Form::close() !!}
							      			</div>
							      			@actionEnd
							      		</td>
								    </tr>
							    @endforeach
								</tbody>
							</table>
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@actionEnd
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function () {
    $("#supplierTable").DataTable(
    	{
    		"scrollX": true
    	});
    });
</script>
@stop