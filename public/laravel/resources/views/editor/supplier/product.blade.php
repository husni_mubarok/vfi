@extends('layouts.home.template')
@section('content')
<div class="header_menu">
    <a href="{{ URL::route('editor.size.index') }}">SIZE</a>
    <a href="{{ URL::route('editor.product_type.index') }}">PRODUCT TYPE</a>
    <a href="{{ URL::route('editor.product.index') }}">PRODUCT</a>
    <a href="{{ URL::route('editor.supplier.index') }}">SUPPLIER</a>
</div>
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-5">
			        <div class="x_panel">
		                <h2>
		                	<i class="fa fa-truck"></i>
		                	<i class="fa fa-cubes"></i> {{$supplier->name}} Product
	                	</h2>
		                <hr>
			            <div class="x_content">
			                @include('errors.error')
		                    {!! Form::open(array('route' => ['editor.supplier.update_product', $supplier->id]))!!}
		                    {{ csrf_field() }}
		                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
	                    	<table class="table table-sm table-hover">
	                    	<thead>
		                    	<tr>
		                    		<th><i class="fa fa-check-square-o"></i></th>
		                    		<th>Product</th>
		                    		<th>Type</th>
		                    	</tr>
	                    	</thead>
	                    	<tbody>
		                    	@foreach($products as $key => $product)
		                    	<tr>
		                    		<td>
			                    	@if(in_array($product->id, $supplier->supplier_product->pluck('product_id')->toArray()))
			                    	{{ Form::checkbox('product[]', $product->id, true) }}
			                    	@else
			                    	{{ Form::checkbox('product[]', $product->id) }}
			                    	@endif
		                    		</td>
		                    		<td>{{ $product->product_type->name }}</td>
		                    		<td>{{ $product->name }}</td>
		                    	</tr>
		                    	@endforeach
	                    	</tbody>
                            </table>
                            <button type="submit" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
	                    	</div>
	                        {!! Form::close() !!}
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@stop