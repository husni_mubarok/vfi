@extends('layouts.home.template')
@section('content')
<div class="header_menu">
    <a href="{{ URL::route('editor.size.index') }}">SIZE</a>
    <a href="{{ URL::route('editor.product_type.index') }}">PRODUCT TYPE</a>
    <a href="{{ URL::route('editor.product.index') }}">PRODUCT</a>
    <a href="{{ URL::route('editor.supplier.index') }}">SUPPLIER</a>
</div>
@actionStart('supplier', 'create|update')
	<section class="content">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-5">
			        <div class="x_panel">
		                <h2>
		                	@if(isset($supplier))
		                	<i class="fa fa-pencil"></i>
		                	@else
		                	<i class="fa fa-plus"></i> 
		                	@endif
		                	<i class="fa fa-truck"></i> Supplier
	                	</h2>
		                <hr>
			            <div class="x_content">
			                @include('errors.error')
	                        @if(isset($supplier))
			                {!! Form::model($supplier, array('route' => ['editor.supplier.update', $supplier->id], 'method' => 'PUT'))!!}
		                    @else
		                    {!! Form::open(array('route' => 'editor.supplier.store'))!!}
		                    @endif
		                    {{ csrf_field() }}
		                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
		                    	{{ Form::label('code', 'Code') }}
		                        {{ Form::text('code', old('code'), array('class' => 'form-control', 'placeholder' => 'Code*', 'required' => 'true')) }}<br/>

		                    	{{ Form::label('name', 'Name') }}
		                        {{ Form::text('name', old('name'), array('class' => 'form-control', 'placeholder' => 'Name*', 'required' => 'true')) }}<br/>

		                        {{ Form::label('address', 'Address') }}
		                        {{ Form::text('address', old('address'), array('class' => 'form-control', 'placeholder' => 'Address*', 'required' => 'true')) }}<br/>

		                        {{ Form::label('email', 'E-mail Address') }}
		                        {{ Form::text('email', old('email'), array('class' => 'form-control', 'placeholder' => 'E-mail Address*', 'required' => 'true')) }}<br/>

		                        {{ Form::label('phone_number', 'Phone Number') }}
		                        {{ Form::text('phone_number', old('phone_number'), array('class' => 'form-control', 'placeholder' => 'Phone Number*', 'required' => 'true')) }}<br/>

		                        {{ Form::label('fax', 'Fax') }}
		                        {{ Form::text('fax', old('fax'), array('class' => 'form-control', 'placeholder' => 'Fax*', 'required' => 'true')) }}<br/>

		                        {{ Form::label('website', 'Website') }}
		                        {{ Form::text('website', old('website'), array('class' => 'form-control', 'placeholder' => 'Website*', 'required' => 'true')) }}<br/>

		                        {{ Form::label('pic', 'Person In Charge') }}
		                        {{ Form::text('pic', old('pic'), array('class' => 'form-control', 'placeholder' => 'Person In Charge*', 'required' => 'true')) }}<br/>

		                        {{ Form::label('bank_account', 'Bank Account') }}
		                        {{ Form::text('bank_account', old('bank_account'), array('class' => 'form-control', 'placeholder' => 'Bank Account*', 'required' => 'true')) }}<br/>

		                        {{ Form::label('note', 'Note') }}
		                        {{ Form::text('note', old('note'), array('class' => 'form-control', 'placeholder' => 'Note', 'required' => 'true')) }}<br/>

	                            <button type="submit" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
	                    	</div>
	                        {!! Form::close() !!}
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
@actionEnd
@stop