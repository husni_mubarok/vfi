
@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<style type="text/css">
	.modal {
		text-align: center;
		padding: 0!important;
	}

	.modal:before {
		content: '' ;
		display: inline-block;
		height: 100%;
		vertical-align: middle;
		margin-right: -4px;
	}

	.modal-dialog {
		display: inline-block;
		text-align: left;
		vertical-align: middle;
	}
</style>
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><a href="{{ URL::route('editor.invoice_type.index') }}"><i class="fa fa-file-archive-o"></i> Invoice Type</a></li>
	</ol>
</section>
<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-1"></div>
				<div class="col-md-5">
					<div class="x_panel">
						<h2>
							
							@if(isset($invoice_type))
							<i class="fa fa-pencil"></i>
							@else
							<i class="fa fa-plus"></i> 
							@endif
							&nbsp;Invoice Type
						</h2>
						<hr>
						<div class="x_content">
							@include('errors.error')
							@if(isset($invoice_type))
							{!! Form::model($invoice_type, array('route' => ['editor.invoice_type.update', $invoice_type->id], 'method' => 'PUT', 'class'=>'update', 'id' => 'form_invoicetype'))!!}
							@else
							{!! Form::open(array('route' => 'editor.invoice_type.store', 'class'=>'create', 'id' => 'form_invoicetype'))!!}
							@endif
							{{ csrf_field() }}
							<div class="col-md-12 col-sm-12 col-xs-12 form-group"> 
								{{ Form::label('Category Item') }}
								{{ Form::select('item_category_id', $item_cat_list, old('item_category_id'), array('class' => 'form-control', 'required' => 'true')) }}<br/>
								
								{{ Form::label('inv_type_name', 'Name') }}
								{{ Form::text('inv_type_name', old('inv_type_name'), array('class' => 'form-control', 'placeholder' => 'Name*', 'required' => 'true')) }}<br/>

								{{ Form::label('inv_type_desc', 'Description') }}
								{{ Form::text('inv_type_desc', old('inv_type_desc'), array('class' => 'form-control', 'placeholder' => 'Description*', 'required' => 'true')) }}<br/>
								<button type="button" data-toggle="modal" data-target="#modal_invoicetype" class="btn btn-success pull-right"><i class="fa fa-check"></i> Save</button>
								<a href="{{ URL::route('editor.invoice_type.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
							</div>
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>
@stop

@section('modal')
<div class="modal fade" id="modal_invoicetype">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Save this invoice type?</h4>
			</div> 
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
				<button type="button" id="btn_submit" class="btn btn-success"><i class="fa fa-check"></i> OK</button>
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')
<script>
	$('#btn_submit').on('click', function()
	{ 
		$('#form_invoicetype').submit();
	});
</script>
@stop