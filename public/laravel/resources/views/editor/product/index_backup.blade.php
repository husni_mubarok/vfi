@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active"><a href="#"><i class="fa fa-cube"></i> Product</a></li>
  </ol>
</section>
@actionStart('product', 'read')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-10">
			        <div class="x_panel">
		                <h2>
		                	<i class="fa fa-cube"></i> @if(isset($product_type)) {{$product_type->name}} Products @else Product List @endif 
		                	@actionStart('product', 'create')
		                	<a href="{{ URL::route('editor.product.create') }}" class="btn btn-primary btn-lg pull-right"><i class="fa fa-plus"></i> Add</a>
		                	@actionEnd
	                	</h2>
		                <hr>
			            <div class="x_content">
			                <table id="productTable" class="table table-striped dataTable">
							  	<thead>
							  	  	<tr>
								      	<th width="5%">#</th>
								      	<th>Name</th>
								      	<th>Description</th>
								      	<th>Sea Product</th>
								      	<th width="10%"></th>
							    	</tr>
							  	</thead>
							  	<tbody>
							    @foreach($products as $key => $product)
							    	<tr>
							      		<td>{{$key+1}}</td>
								      	<td>[{{$product->product_type->name}}] {{$product->name}}</td>
								      	<td>{{$product->description}}</td>
								      	<td>
								      		<table class="table">
					      					@foreach($product->product_fish as $fish)
					      					<tr>
					      						<td>{{$fish->fish->name}}</td>
					      					</tr>
					      					@endforeach
								      		</table>
							      		</td>
								      	<td align="center">
								      		@actionStart('product', 'update')
								      		<div class="col-md-2 nopadding">
								      			<a href="{{ URL::route('editor.product.edit', [$product->id]) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
								      		</div>
								      		@actionEnd

								      		@actionStart('product', 'delete')
							      			<div class="col-md-2 nopadding">
								      			{!! Form::open(array('route' => ['editor.product.delete', $product->id], 'method' => 'delete'))!!}
		                    					{{ csrf_field() }}	                    				
								      			<button type="submit" class="btn btn-primary btn-sm" onclick="return confirm('Confirm delete?')"><i class="fa fa-trash"></i></a></button>
								      			{!! Form::close() !!}
							      			</div>
							      			@actionEnd
							      		</td>
								    </tr>
							    @endforeach
								</tbody>
							</table>
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@actionEnd
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function () {
    $("#productTable").DataTable(
    	{
    		"scrollX": true
    	});
    });
</script>
@stop