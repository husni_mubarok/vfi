@extends('layouts.home.template')
@section('content')
<div class="header_menu">
    <a href="{{ URL::route('editor.size.index') }}">SIZE</a>
    <a href="{{ URL::route('editor.product_type.index') }}">PRODUCT TYPE</a>
    <a href="{{ URL::route('editor.product.index') }}">PRODUCT</a>
    <a href="{{ URL::route('editor.supplier.index') }}">SUPPLIER</a>
</div>
<section class="content">
	<div class="row">
	    <div class="col-md-12 col-sm-12 col-xs-12">
	    	<div class="col-md-1"></div>
	    	<div class="col-md-10">
		        <div class="x_panel">
	                <h2>
	                	<i class="fa fa-cube"></i> Product {{$product->name}}
	                	<a href="{{ URL::route('editor.product_type.create', $product->id) }}" class="btn btn-default btn-lg pull-right"><i class="fa fa-plus"></i> Add</a>
                	</h2>
	                <hr>
		            <div class="x_content">
		                <table id="productTable" class="table table-striped dataTable">
						  	<thead>
						  	  	<tr>
							      	<th>#</th>
							      	<th>Type</th>
							      	<th>Description</th>
							      	<th></th>
						    	</tr>
						  	</thead>
						  	<tbody>
						    @foreach($product->product_type as $key => $product_type)
						    	<tr>
						      		<td>{{$key+1}}</td>
							      	<td>{{$product_type->name}}</td>
							      	<td>{{$product_type->description}}</td>
							      	<td align="center">
							      		<div class="col-md-2 nopadding">
							      			<a href="{{ URL::route('editor.product_type.edit', [$product_type->id]) }}" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i></a>
							      		</div>
						      			<div class="col-md-2 nopadding">
							      			{!! Form::open(array('route' => ['editor.product_type.delete', $product_type->id], 'method' => 'delete'))!!}
	                    					{{ csrf_field() }}	                    				
							      			<button type="submit" class="btn btn-default btn-sm"><i class="fa fa-trash"></i></a></button>
							      			{!! Form::close() !!}
						      			</div>
						      		</td>
							    </tr>
						    @endforeach
							</tbody>
						</table>
		            </div>
		        </div>
	        </div>
	    </div>
	</div>
</section>
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function () {
    $("#productTable").DataTable(
    	{
    		"scrollX": true
    	});
    });
</script>
@stop