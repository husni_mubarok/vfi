@extends('layouts.home.template')
@section('content')
<div class="header_menu">
    <a href="{{ URL::route('editor.size.index') }}">SIZE</a>
    <a href="{{ URL::route('editor.product_type.index') }}">PRODUCT TYPE</a>
    <a href="{{ URL::route('editor.product.index') }}">PRODUCT</a>
    <a href="{{ URL::route('editor.supplier.index') }}">SUPPLIER</a>
</div>
@actionStart('product', 'create|update')
<section class="content">
	<div class="row">
	    <div class="col-md-12 col-sm-12 col-xs-12">
	    	<div class="col-md-1"></div>
	    	<div class="col-md-5">
		        <div class="x_panel">
	                <h2>
	                	@if(isset($product))
	                	<i class="fa fa-pencil"></i>
	                	@else
	                	<i class="fa fa-plus"></i> 
	                	@endif
	                	<i class="fa fa-cube"></i> Product
                	</h2>
	                <hr>
		            <div class="x_content">
		                @include('errors.error')
                        @if(isset($product))
		                {!! Form::model($product, array('route' => ['editor.product.update', $product->id], 'method' => 'PUT'))!!}
	                    @else
	                    {!! Form::open(array('route' => ['editor.product.store']))!!}
	                    @endif
	                    {{ csrf_field() }}
	                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
	                        {{ Form::label('product_id', 'Product Type') }}
	                        @if(isset($product))
	                        <select name="product_type_id" class="form-control" disabled>
	                        	<option value="" selected disabled>Select a product type</option>
	                        	@foreach($product_types as $key => $product_type)
	                        	<option value="{{$key}}" @if($key == $product->product_type->id) selected  @endif>{{$product_type}}</option>
	                        	@endforeach
	                        </select><br/>
	                        @else
	                        <select name="product_type_id" class="form-control">
	                        	<option value="" selected disabled>Select a product type</option>
	                        	@foreach($product_types as $key => $product_type)
	                        	<option value="{{$key}}" @if($key == $product_type_id) selected  @endif>{{$product_type}}</option>
	                        	@endforeach
	                        </select><br/>
	                        @endif

	                        {{ Form::label('code', 'Code') }}
	                        {{ Form::text('code', old('code'), array('class' => 'form-control', 'placeholder' => 'Code*', 'required' => 'true')) }}<br/>

	                        {{ Form::label('name', 'Name') }}
	                        {{ Form::text('name', old('name'), array('class' => 'form-control', 'placeholder' => 'Name*', 'required' => 'true')) }}<br/>

	                        {{ Form::label('description', 'Description') }}
	                        {{ Form::text('description', old('description'), array('class' => 'form-control', 'placeholder' => 'Description')) }}<br/>

                            <button type="submit" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
                    	</div>
                        {!! Form::close() !!}
		            </div>
		        </div>
	        </div>
	    </div>
	</div>
</section>
@actionEnd
@stop