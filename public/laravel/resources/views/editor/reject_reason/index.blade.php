@extends('layouts.home.template')
@section('content')
<div class="header_menu">
    <a href="{{ URL::route('editor.fish.index') }}">FISH</a>
    <a href="{{ URL::route('editor.production.index') }}">PRODUCT</a>
    <a href="{{ URL::route('editor.reject_type.index') }}">REJECT TYPE</a>
    <a href="{{ URL::route('editor.reject_reason.index') }}">REJECT REASON</a>
</div>
@actionStart('reject_reason', 'read')
<section class="content">
	<div class="row">
	    <div class="col-md-12 col-sm-12 col-xs-12">
	    	<div class="col-md-1"></div>
	    	<div class="col-md-10">
		        <div class="x_panel">
	                <h2>
	                	<i class="fa fa-ban"></i> Reject Reason List
	                	@actionStart('reject_reason', 'create')
	                	<a href="{{ URL::route('editor.reject_reason.create') }}" class="btn btn-default btn-lg pull-right"><i class="fa fa-plus"></i> Add</a>
	                	@actionEnd
                	</h2>
	                <hr>
		            <div class="x_content">
		                <table id="reject_reasonTable" class="table table-striped dataTable">
						  	<thead>
						  	  	<tr>
							      	<th>#</th>
							      	<th>Name</th>
							      	<th>Description</th>
							      	<th></th>
						    	</tr>
						  	</thead>
						  	<tbody>
						    @foreach($reject_reasons as $key => $reject_reason)
						    	<tr>
						      		<td>{{$key+1}}</td>
							      	<td>{{$reject_reason->name}}</td>
							      	<td>{{$reject_reason->description}}</td>
							      	<td align="center">
							      		<div class="col-md-2 nopadding">
							      			@actionStart('reject_reason', 'update')
							      			<a href="{{ URL::route('editor.reject_reason.edit', [$reject_reason->id]) }}" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i></a>
							      			@actionEnd
							      		</div>
						      			<div class="col-md-2 nopadding">
						      				@actionStart('reject_reason', 'delete')
							      			{!! Form::open(array('route' => ['editor.reject_reason.delete', $reject_reason->id], 'method' => 'delete'))!!}
	                    					{{ csrf_field() }}	                    				
							      			<button type="submit" class="btn btn-default btn-sm" onclick="return confirm('Confirm delete?')"><i class="fa fa-trash"></i></a></button>
							      			{!! Form::close() !!}
							      			@actionEnd
						      			</div>
						      		</td>
							    </tr>
						    @endforeach
							</tbody>
						</table>
		            </div>
		        </div>
	        </div>
	    </div>
	</div>
</section>
@actionEnd
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function () {
    $("#reject_reasonTable").DataTable(
    	{
    		"scrollX": true
    	});
    });
</script>
@stop