@extends('layouts.home.template')
@section('content')
<style type="text/css">
	.modal {
		text-align: center;
		padding: 0!important;
	}

	.modal:before {
		content: '';
		display: inline-block;
		height: 100%;
		vertical-align: middle;
		margin-right: -4px;
	}

	.modal-dialog {
		display: inline-block;
		text-align: left;
		vertical-align: middle;
	}
</style>
<div class="header_menu mobile-hide">
	<a href="{{ URL::route('tally.index') }}">WIP</a>
</div> 
<section class="content">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			{!! Form::model($codesizes,array('route' => ['codesize.storecodesize'], 'method' => 'PUT', 'id' => 'form_codesize'))!!} 
			<div class="col-md-4">
				<div class="x_panel"> 
					<div class="x_content">
						<table style="width:100%">
						<tr><td><br>{{ Form::label('name', 'Code Size') }}</td></tr>
						<tr><td><input type="text" class="form-control" name="name" id="name"></td></tr>
						<tr><td><br>{{ Form::label('grade', 'Grade') }}</td></tr>
						<tr>
							<td>
								<select name="grade_id" id="grade_id" class="form-control">
									<option value="" selected disabled>-Choose-</option>
									@foreach($grades as $key => $grade)
										<option value="{{$grade->id}}">{{$grade->name}}</option>
									@endforeach
								</select>
							</td>
						</tr>
						<tr><td><br>{{ Form::label('size', 'Size') }}</td></tr>
						<tr>
							<td>
								<select name="size_id" id="size_id" class="form-control">
									<option value="" selected disabled>-Choose-</option>
									@foreach($sizes as $key => $size)
										<option value="{{$size->id}}">{{$size->note}}</option>
									@endforeach
								</select>
							</td>
						</tr>
						<tr><td><br>{{ Form::label('block_weight', 'Block Weight') }}</td></tr>	
						<tr>
							<td>
								<div class="input-group">
									<input type="number" class="form-control" placeholder="0" name="block_weight" id="block_weight" value="0">
									<div class="input-group-addon"><b>Kg</b></div>
								</div>
							</td>
							{{-- <td align="center">
						<button type="button" class="btn btn-primary btn-sm" onclick="block_weight_add();"><i class="fa fa-plus"></i></button>
						<button type="button" class="btn btn-warning btn-sm" onclick="block_weight_reduce();"><i class="fa fa-minus"></i></button>
							</td> --}}
						</tr>
									{{-- <tr><td><br>{{ Form::label('freezing_type', 'Freezing Type') }}</td></tr> --}}
									{{-- <tr>
										<td>
								<select name="freezing_type_id" id="freezing_type_id" class="form-control">
												<option value="" selected disabled>-Choose-</option>
												@foreach($freezing_types as $key => $freezing_type)
										<option value="{{$freezing_type->id}}">{{$freezing_type->name}}</option>
									@endforeach
											</select>
										</td>
									</tr> --}}
						<tr>
							<td>
								<br>
								<button type="button" id="btn_confirm" data-toggle="modal" data-target="#modal_confirm" class="btn btn-success pull-right"><i class="fa fa-check"></i> Save</button>
							</td>
						</tr>
						</table>
					</div>  
				</div>
			</div> 
			{!!Form::close()!!}

	<div class="col-md-8">
		<div class="x_panel"> 
			<div class="x_content">  
				<h2>Code Size</h2>
				<hr>
				<table id="codesize_table" class="table table-hover table-striped">
				<thead>
				<tr>
					<th>#</th>
					<th>Grade</th>
					<th>Code Size</th>
					<th>Size</th>
					{{-- <th>Freezing Type</th> --}}
					<th>Block Weight (Kg)</th>
					<th>Action</th>
				</tr>
				</thead>
				<tbody>
				@foreach($codesizes as $key => $codesize_result)
					<tr>
						<td>{{$key+1}}</td>
						<td>{{$codesize_result->grade_name}}</td>
						<td>{{$codesize_result->name}}</td>
						<td>{{$codesize_result->size_name}}</td>
						{{-- <td>{{$codesize_result->freezing_name}}</td> --}}
						<td>{{$codesize_result->block_weight}}</td>
						<td>
								{{ Form::open(['id' => 'delete_codesize', 'method' => 'DELETE', 'route' => ['codesize.deletecodesize', $codesize_result->id_code_size]]) }}
									<button type="button" id="btn_confirm" data-toggle="modal" data-target="#modal_delete" class="btn btn-danger"><i class="fa fa-trash"></i></button>
							{{ Form::close() }}
						</td>
					</tr>
				@endforeach
				</tbody>
				</table>
			</div>
		</div>
	</div>
</section>
@stop

@section('modal')
<div class="modal fade" id="modal_confirm">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="modal_grading"> Are you sure want to save?</h4>
			</div> 
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> No</button>
				<button type="button" id="btn_submit" onclick="submit_form();" class="btn btn-success"><i class="fa fa-check"></i> Yes</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_delete">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="modal_grading"> Are you sure want to delete?</h4>
			</div> 
			<div class="modal-footer">
				<button type="button" class="btn btn-success" data-dismiss="modal"><i class="fa fa-remove"></i> No</button>
				<button type="button" id="btn_submit" onclick="delete_form();" class="btn btn-danger"><i class="fa fa-check"></i> Yes</button>
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')
{{-- DATATABLES --}}
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
$(document).ready(function() {
	// $("#codesize_table").DataTable();
	$('#codesize_table').DataTable({
		"pageLength": 50
	});
});
</script>

<script>
// function block_weight_add() {$('#block_weight').val(parseInt($('#block_weight').val())+parseInt(1));};
// function block_weight_reduce() {$('#block_weight').val(parseInt($('#block_weight').val())-parseInt(1));};

function submit_form()
{
	var name = document.getElementById("name").value;
	var grade_id = document.getElementById("grade_id").value;
	var size_id = document.getElementById("size_id").value;
	// var freezing_type_id = document.getElementById("freezing_type_id").value;

	if(name == '') {alert("Code Size is required!");}
	else if(grade_id == '') {alert("Grade is required!");}
	else if(size_id == '') {alert("Size is required!");}
	// else if(freezing_type_id == '') {alert("Freezing Type is required!");}
	else {$('#form_codesize').submit();}
};

function delete_form()
{
	$('#delete_codesize').submit();
}
</script>
@stop