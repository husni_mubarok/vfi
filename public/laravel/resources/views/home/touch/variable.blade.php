@extends('layouts.touch.template')
@section('content')
<div class="col-md-7 vficenter">
  <div id="content_full">
  	<h1>Fish Production</h1>
    <div class="row bs-wizard small" style="border-bottom:0;">
      <!-- class name =  1. complete 2.active 3.disabled-->
      
      <div class="col-xs-2 bs-wizard-step complete">
        {{-- <div class="text-center bs-wizard-stepnum">Start</div> --}}
        <div class="progress sm"><div class="progress-bar"></div></div>
        <a href="#" class="bs-wizard-dot"></a>
        <div class="bs-wizard-info text-center">Start<br><i>Project</i></div>
      </div>

      <div class="col-xs-2 bs-wizard-step active"><!-- active -->
        {{-- <div class="text-center bs-wizard-stepnum">Step 2</div> --}}
        <div class="progress sm"><div class="progress-bar"></div></div>
        <a href="#" class="bs-wizard-dot"></a>
        <div class="bs-wizard-info text-center">Step 1<br><i>Waste Product</i></div>
      </div>

      <div class="col-xs-2 bs-wizard-step disabled"><!-- active -->
        {{-- <div class="text-center bs-wizard-stepnum">Step 3</div> --}}
        <div class="progress sm"><div class="progress-bar"></div></div>
        <a href="#" class="bs-wizard-dot"></a>
        <div class="bs-wizard-info text-center">Step 2<br><i>Additional Product</i></div>
      </div>

      <div class="col-xs-2 bs-wizard-step disabled"><!-- active -->
        {{-- <div class="text-center bs-wizard-stepnum">Step 4</div> --}}
        <div class="progress sm"><div class="progress-bar"></div></div>
        <a href="#" class="bs-wizard-dot"></a>
        <div class="bs-wizard-info text-center">Step 3<br><i>Finish Product</i></div>
      </div>
      <div class="col-xs-2 bs-wizard-step disabled"><!-- active -->
        {{-- <div class="text-center bs-wizard-stepnum">Finish</div> --}}
        <div class="progress sm"><div class="progress-bar"></div></div>
        <a href="#" class="bs-wizard-dot"></a>
        <div class="bs-wizard-info text-center">Finish<br><i>Summary</i></div>
      </div>
    </div>
    <hr>
		<div class="row info" >
    	<div class="col-md-12" style="padding:0px 25px">
    		<h2 style="margin-top:0">Waste </h2>
  		</div>
		</div>

		<hr>
 		<div class="row">

      {{-- Waste Product Form --}}
		  <div class="col-md-6">
        <h4>Waste Products</h4>
          @include('errors.error')
          {!! Form::open(array('route' => ['editor.production_result.store_waste', $production_result->id])) !!}
          <table class="table table-bordered table-hover">
          <thead>
            <tr>
            <th>Name</th>
            <th>UoM</th>
            <th>Type</th>
            <th>Value</th>
          </tr>
          </thead>
          <tbody>
          @if(isset($production_result_variables))
            @foreach($production_result_variables as $key => $production_result_variable)
            @if($production_result_variable->variable->type == 'Reduction')
            <tr>
              <td>{{$production_result_variable->variable->name}}</td>
              <td>{{$production_result_variable->variable->uom}}</td>
              <td>{{$production_result_variable->variable->type}}</td>
              <td>
                <div class="input-group">
                  {{ Form::number('value['.$production_result_variable->variable->id.']', old('value['.$production_result_variable->variable->id.']', $production_result_variable->value), array('required' => 'true', 'min' => '0', 'step' => '0.01', 'class' => 'form-control')) }}
                  <div class="input-group-addon"><span>kg</span></div>
                </div>
              </td>
            </tr>
            @endif
            @endforeach
          @else
            @foreach($production_result->production->production_variable as $key => $production_variable)
            @if($production_variable->variable->type == 'Reduction')
              <tr>
                <td>{{$production_variable->variable->name}}</td>
                <td>{{$production_variable->variable->uom}}</td>
                <td>{{$production_variable->variable->type}}</td>
                <td>
                  <div class="input-group">
                    {{ Form::number('value['.$production_variable->variable->id.']', old('value['.$production_variable->variable->id.']'), array('required' => 'true', 'min' => '0', 'step' => '0.01', 'class' => 'form-control')) }}
                    <div class="input-group-addon"><span>kg</span></div>
                  </div>
                </td>
              </tr>
            @endif
            @endforeach
          @endif
          </tbody>
        </table>
        <button type="button" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
        {!! Form::close() !!}
      </div>
      {{-- Waste Product Form --}}

      {{-- Production Details --}}
      <div class="col-md-6">
        <h4>Production Detail</h4>
        <table class="table table-bordered">
          <tr>
            <th>Product</th>
            <td>{{$production_result->production->name}}</td>
          </tr>
          <tr>
            <th>Weight</th>
            <td>{{number_format($production_result->weight, 2)}} kg</td>
          </tr>
          <tr>
            <th>Block Weight</th>
            <td>{{number_format($production_result->block_weight, 2)}} kg</td>
          </tr>
          <tr>
            <th>Production Date</th>
            <td>
              {{date("D, d M Y", strtotime($production_result->started_at))}}
              &nbsp;<i class="fa fa-long-arrow-right"></i>&nbsp;
              @if($production_result->finished_at == null)
              <i class="fa fa-question-circle-o"></i>
              @else
              {{date("D, d M Y", strtotime($production_result->finished_at))}} 
              @endif
            </td>
          </tr>
          <tr>
            <th>Criteria</th>
            <td>{{$production_result->criteria}}</td>
          </tr>
        </table>
      </div>
      {{-- Production Details --}}

    </div>
  </div>
</div>
@stop