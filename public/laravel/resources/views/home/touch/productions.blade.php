@extends('layouts.touch.template')
@section('content')
<div class="col-md-7 vficenter">
    <div id="content_full">
      <h1>Fish Production</h1>
            {{-- <hr> --}}

            <div class="row bs-wizard small" style="border-bottom:0;">
                <!-- class name =  1. complete 2.active 3.disabled-->
                
                <div class="col-xs-2 bs-wizard-step active">
                  {{-- <div class="text-center bs-wizard-stepnum">Start</div> --}}
                  <div class="progress sm"><div class="progress-bar"></div></div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">Start<br><i>Project</i></div>
                </div>

                <div class="col-xs-2 bs-wizard-step disabled"><!-- active -->
                  {{-- <div class="text-center bs-wizard-stepnum">Step 2</div> --}}
                  <div class="progress sm"><div class="progress-bar"></div></div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">Step 1<br><i>Waste Product</i></div>
                </div>

                <div class="col-xs-2 bs-wizard-step disabled"><!-- active -->
                  {{-- <div class="text-center bs-wizard-stepnum">Step 3</div> --}}
                  <div class="progress sm"><div class="progress-bar"></div></div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">Step 2<br><i>Additional Product</i></div>
                </div>

                <div class="col-xs-2 bs-wizard-step disabled"><!-- active -->
                  {{-- <div class="text-center bs-wizard-stepnum">Step 4</div> --}}
                  <div class="progress sm"><div class="progress-bar"></div></div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">Step 3<br><i>Finish Product</i></div>
                </div>
                <div class="col-xs-2 bs-wizard-step disabled"><!-- active -->
                  {{-- <div class="text-center bs-wizard-stepnum">Finish</div> --}}
                  <div class="progress sm"><div class="progress-bar"></div></div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">Finish<br><i>Summary</i></div>
                </div>
            </div>
            <hr>
      <div class="row info" >
        <div class="col-md-12" style="padding:0px 25px">
          <h2 style="margin-top:0">
              All Projects 
              {{-- <a href="{{ URL::route('production_result.create') }}" class="btn btn-primary  btn-lg pull-right"><i class="fa fa-plus"></i> Add Production</a> --}}
          </h2>
          <h4>{{date("l, d-m-Y")}}</h4>
        </div>
      </div>
      <hr>
       <div class="row">
                <div class="col-md-12">
                @foreach($production_results as $production_result)
                <div class="col-md-6">
                  @if($production_result->status == 'Pending')
                    <div class="box box-danger box-solid">
                  @elseif($production_result->status == 'On Going')
                    <div class="box box-warning box-solid">
                  @else 
                    <div class="box box-success box-solid">
                  @endif
                      <div class="box-header with-border">
                        <h3 class="box-title">{{$production_result->production->fish->name}}</h3>
                        <div class="box-tools pull-right">
                          <span>{{number_format(($production_result->amount), 0)}} pcs | {{number_format($production_result->weight, 2)}} kg</span>
                          <span class="label">{{$production_result->status}}</span>
                          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                      </div>
                      <div class="box-body">
                        <p><b></b></p>
                        <table class="table" border="0">
                          <tr>
                              <td><a href="{{ URL::route('production_result.create_waste', $production_result->id) }}">
                                <i class="fa fa-search"></i>&nbsp;&nbsp;&nbsp;{{$production_result->production->name}}</a></td>
                              <td align="right" colspan="2"></td>
                              <td align="center">{{$production_result->criteria}}</td>
                          </tr>
                          <tr>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td><a href="{{ URL::route('production_result.summary', $production_result->id) }}" class="pull-right btn btn-sm btn-primary"><i class="fa fa-file-text-o"></i>&nbsp;Summary</a></td>
                          </tr>
                        </table>
                      </div>
                    </div>
                </div>
                @endforeach
              </div>
            </div>
          </div>
        </div>
@stop