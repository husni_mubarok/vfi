@extends('layouts.touch.template')
@section('content')
<div class="col-md-7 vficenter">
    <div id="content_full">
      	<h1>Fish Production</h1>
        <div class="row bs-wizard small" style="border-bottom:0;">
            <!-- class name =  1. complete 2.active 3.disabled-->
            
            <div class="col-xs-2 bs-wizard-step active">
              {{-- <div class="text-center bs-wizard-stepnum">Start</div> --}}
              <div class="progress sm"><div class="progress-bar"></div></div>
              <a href="#" class="bs-wizard-dot"></a>
              <div class="bs-wizard-info text-center">Start<br><i>Project</i></div>
            </div>

            <div class="col-xs-2 bs-wizard-step disabled"><!-- active -->
              {{-- <div class="text-center bs-wizard-stepnum">Step 2</div> --}}
              <div class="progress sm"><div class="progress-bar"></div></div>
              <a href="#" class="bs-wizard-dot"></a>
              <div class="bs-wizard-info text-center">Step 1<br><i>Waste Product</i></div>
            </div>

            <div class="col-xs-2 bs-wizard-step disabled"><!-- active -->
              {{-- <div class="text-center bs-wizard-stepnum">Step 3</div> --}}
              <div class="progress sm"><div class="progress-bar"></div></div>
              <a href="#" class="bs-wizard-dot"></a>
              <div class="bs-wizard-info text-center">Step 2<br><i>Reject Product</i></div>
            </div>

            <div class="col-xs-2 bs-wizard-step disabled"><!-- active -->
              {{-- <div class="text-center bs-wizard-stepnum">Step 4</div> --}}
              <div class="progress sm"><div class="progress-bar"></div></div>
              <a href="#" class="bs-wizard-dot"></a>
              <div class="bs-wizard-info text-center">Step 3<br><i>Finish Product</i></div>
            </div>
            <div class="col-xs-2 bs-wizard-step disabled"><!-- active -->
              {{-- <div class="text-center bs-wizard-stepnum">Finish</div> --}}
              <div class="progress sm"><div class="progress-bar"></div></div>
              <a href="#" class="bs-wizard-dot"></a>
              <div class="bs-wizard-info text-center">Finish<br><i>Summary</i></div>
            </div>
        </div>
        <hr>
  		<div class="row info" >
        	<div class="col-md-12" style="padding:0px 25px">
          		<h2 style="margin-top:0">
              		Add Production 
          		</h2>
    		</div>
  		</div>
      		<hr>
   		<div class="row">
   			@include('errors.error')
        @if(isset($production_result))
        {{ Form::model($production_result, array('route' => array('production_result.update', $production_result->id), 'method' => 'put')) }}
        @else    
        {!! Form::open(array('route' => 'production_result.store'))!!}
        @endif
        {{ csrf_field() }}
   			<div class="col-md-6">
 				        {{ Form::label('fish_id', 'Fish') }}
                @if(isset($production_result))
                <p>{{$production_result->production->fish->name}}</p>
                @else
                {{ Form::select('fish_id', $fishes, old('fish_id'), ['class' => 'form-control', 'placeholder' => 'Select a Fish']) }}
                @endif
                <br/>

                {{ Form::label('production_id', 'Production') }}
                @if(isset($production_result))
                <p>{{$production_result->production->name}}</p>
                @else
                <select name="production_id" id="production_id" class="form-control" disabled>
                </select>
                @endif
                <br/>

				        {{ Form::label('weight', 'Weight') }}
                <div class="input-group">
                  {{ Form::number('weight', old('weight'), array('class' => 'form-control default_input_number', 'placeholder' => 'Weight*', 'required' => 'true', 'min' => '0', 'step' => '0.01')) }}
                  <div class="input-group-addon">kg</div>
                </div><br/>     

                {{ Form::label('block_weight', 'Block Weight') }}
                <div class="input-group">
                  {{ Form::number('block_weight', old('block_weight'), array('class' => 'form-control default_input_number', 'placeholder' => 'Block Weight*', 'required' => 'true', 'min' => '0', 'step' => '0.01')) }}
                  <div class="input-group-addon">kg</div>
                </div><br/>                 

                {{ Form::label('started_at', 'Start Date') }}
                {{ Form::text('started_at', old('started_at'), array('class' => 'form-control', 'placeholder' => 'Date & Time*', 'required' => 'true', 'id' => 'started_at')) }}<br/>

                {{ Form::label('criteria', 'Criteria') }}
                {{ Form::text('criteria', old('criteria'), array('class' => 'form-control default_input_keyboard', 'placeholder' => 'Criteria',)) }}<br/>

                <button type="submit" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
   			</div>
   			{!! Form::close() !!}
        </div>
    </div>
</div>
@stop
@section('scripts')
<script>
$(document).ready(function () {
	$('#started_at').datetimepicker({
		sideBySide: true,
		format: 'YYYY-MM-DD HH:mm',
	});
});
</script>
<script>
$("#fish_id").on('change keyup blur', function()
{
    $.ajax({
        url : '{{ URL::route('get.production') }}',
        data : {'fish_id':$("#fish_id").val()},
        type : 'GET',
        headers : {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
        success : function(data, textStatus, jqXHR){
        	console.log(data);
            $('#production_id').empty();
            jQuery.each(data, function(i, val)
            {
            	console.log(val.id, val.name);
            	$('#production_id').append($('<option>', { 
			        value: val.id,
			        text : val.name 
			    }));
            });
            $("#production_id").attr('disabled', false);
        },
        error : function()
        {
        	$('#production_id').empty();
        	$('#production_id').attr('disabled', true);
        },
    })
});
</script>
@stop