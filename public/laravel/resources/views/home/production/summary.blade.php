@extends('layouts.home.template')
@section('content')
<div class="header_menu mobile-hide">
    <a href="{{ URL::route('production') }}">WORKING</a>
    <a href="{{ URL::route('production.unstored') }}">UNSTORED</a>
    <a href="{{ URL::route('production.bank') }}">BANK</a>
</div>
<section class="content">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<h2>
        	<i class="fa fa-file-text-o"></i> Summary
          @if($result['status'] < 4)
          <a href="{{ URL::route('production.edit_results', $result['id']) }}" class="btn btn-success btn-sm pull-right"> <i class="fa fa-pencil"></i> Edit</a>
          @endif
      	</h2>
  	  	<hr>
  	  	<div class="col-md-2"></div>          
			  <div class="col-md-8">
          <table class="table">
            <tr>
              <td colspan="3">
                <b>{{$result['material']}}</b> 
              </td>
              <td align="right">
                <b>{{number_format($result['weight'], 2)}} kg</b>
              </td>
              <td>
                @if($result['status'] != 4)
                @endif
              </td>
            </tr>
            <tr>
              <td colspan="3"><span>{{$result['product']}}</span></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td colspan="5">Block weight = {{number_format($result['block_weight'], 2)}} kg</td>
            </tr>
            <tr>
              <td colspan="5">Master carton = {{$result['mst_carton']}}</td>
            </tr>
            <tr>
              <td colspan="5">
                {{date("D, d M Y", strtotime($result['start_date']))}} 
                <i class="fa fa-caret-right"></i> 
                {{date("D, d M Y", strtotime($result['finish_date']))}}
              </td>
            </tr>

            {{-- Waste Products --}}
            <tr>
              <td colspan="2">
                <b>Waste Product</b> 
                @if($result['status'] < 4)
                @endif
              </td>
              <td></td>
              <td align="right"><b>{{number_format($result['total_waste'], 2)}} kg</b></td>
              <td>
                <button type="button" class="btn btn-default btn-sm" data-toggle="collapse" data-target=".waste_rows">
                <i class="fa fa-chevron-down"></i></button>
              </td>
            </tr>
            <tr class="collapse waste_rows">
              <th></th>
              <th>Part</th>
              <th></th>
              <th align="right">Weight</th>
              <th></th>
            </tr>
            @if(isset($result['waste']))
            @foreach($result['waste'] as $waste_key => $waste)
            <tr class="collapse waste_rows">
              <td colspan="5">Input #{{$waste_key+1}}</td>
            </tr>
            @foreach($waste as $waste_detail)
            <tr class="collapse waste_rows">
              <td></td>
              <td>{{str_replace('_', ' ', $waste_detail['name'])}}</td>
              <td></td>
              <td align="right">{{number_format($waste_detail['value'], 2)}} kg</td>
              <td></td>
            </tr>
            @endforeach
            @endforeach
            @endif
            {{-- Waste Products --}}

            {{-- Ready Products --}}
            <tr>
              <td colspan="2">
                <b>Ready Product</b>
                @if($result['status'] < 4)
                @endif
              </td>
              <td></td>
              <td align="right"><b>{{number_format($result['total_detail'], 2)}} kg</b></td>
              <td>
                <button type="button" class="btn btn-default btn-sm" data-toggle="collapse" data-target=".ready_rows">
                <i class="fa fa-chevron-down"></i></button>
              </td>
            </tr>
            <tr class="collapse ready_rows">
              <th></th>
              <th>Size</th>
              <td align="right"><b>MC</b></td>
              <td align="right"><b>Unpack</b></td>
              <th></th>
            </tr>
            @if(isset($result['detail']))
            @foreach($result['detail'] as $detail)
            <tr class="collapse ready_rows">
              <td></td>
              <td>{{str_replace('_', '~', $detail['size'])}}</td>
              <td align="right">{{$detail['mc']}}</td>
              <td align="right">{{$detail['unpack']}}</td>
              <td></td>
            </tr>
            @endforeach
            @endif
            {{-- Ready Products --}}

            {{-- Additional Products --}}
            <tr>
              <td colspan="2">
                <b>Additional Product</b>
                @if($result['status'] < 4)
                @endif
              </td>
              <td></td>
              <td align="right"><b>{{number_format($result['total_addition'], 2)}} kg</b></td>
              <td>
                <button type="button" class="btn btn-default btn-sm" data-toggle="collapse" data-target=".additional_rows">
                <i class="fa fa-chevron-down"></i></button>
              </td>
            </tr>
            <tr class="collapse additional_rows">
              <th></th>
              <th>
                <table class="table">
                <tr>
                  <th width="20%">Size</th>
                  <th width="20%">Block</th>
                  <th width="20%">Block Weight</th>
                  <th width="20%">Type</th>
                  <th width="20%">Reason</th>
                </tr>
                </table>
              </th>
              <td align="right"><b>MC</b></td>
              <td align="right"><b>Unpack</b></td>
              <th></th>
            </tr>
            @if(isset($result['addition']))
            @foreach($result['addition'] as $addition)
            <tr class="collapse additional_rows">
              <td></td>
              <td>
                <table class="table">
                <tr>
                  <td width="20%">{{str_replace('_', '~', $addition['size'])}}</td>
                  <td width="20%">{{$addition['block']}}</td>
                  <td width="20%">{{number_format($addition['block_weight'], 2)}}</td>
                  <td width="20%">{{$addition['reject_type']}}</td>
                  <td width="20%">{{$addition['reject_reason']}}</td>
                </tr>
                </table>
              </td>
              <td align="right">{{$addition['mc']}}</td>
              <td align="right">{{$addition['unpack']}}</td>
              <td></td>
            </tr>
            @endforeach
            @endif
            {{-- Additional Products --}}

            {{-- Miss Products --}}
            <tr>
              <td colspan="2"><b>Miss Product</b></td>
              <td></td>
              <td align="right"><b>{{number_format($result['total_miss'], 2)}} kg</b></td>
              <td></td>
            </tr>
            {{-- Miss Products --}}

            {{-- Yield --}}
            <tr>
              <td colspan="2"><b>Yield</b></td>
              <td></td>
              <td></td>
              <td>
                <button type="button" class="btn btn-default btn-sm" data-toggle="collapse" data-target=".yield_rows">
                <i class="fa fa-chevron-down"></i></button>
              </td>
            </tr>
            <tr class="collapse yield_rows">
              <td></td>
              <td>Waste Product</td>
              <td></td>
              <td align="right">{{number_format($result['total_waste'] / $result['weight'] * 100, 2)}} %</td>
              <td></td>
            </tr>
            <tr class="collapse yield_rows">
              <td></td>
              <td>Ready Product</td>
              <td></td>
              <td align="right">
                @if(($result['total_detail'] / $result['weight'] * 100) < 40)
                  <span class="label label-danger">
                  {{number_format($result['total_detail'] / $result['weight'] * 100, 2)}} %
                  </span>
                  @else
                  {{number_format($result['total_detail'] / $result['weight'] * 100, 2)}} %
                  @endif
              </td>
              <td></td>
            </tr>
            <tr class="collapse yield_rows">
              <td></td>
              <td>Addition Product</td>
              <td></td>
              <td align="right">{{number_format($result['total_addition'] / $result['weight'] * 100, 2)}} %</td>
              <td></td>
            </tr>
            <tr class="collapse yield_rows">
              <td></td>
              <td>Miss Product</td>
              <td></td>
              <td align="right">{{number_format($result['total_miss'] / $result['weight'] * 100, 2)}} %</td>
              <td></td>
            </tr>
            {{-- Yield --}}   

          </table>
          
          <div class="panel panel-primary">
            <div class="panel-heading">
              <h3 class="panel-title">STORE TO STORAGE</h3>
            </div>
            <div class="panel-body">
              <!-- <div class="alert alert-danger" role="alert"> -->
                <!-- <strong>PRODUCTION NOT INSERTED IN STORAGE</strong>
                <br><br> -->
              <!-- </div> -->
              <form class="form-horizontal">

                <input type="hidden" name="dataStorage" id="dataStorage" value="{{$avr}}">
                <div class="form-group">
                  <label for="inputPlacement" class="col-sm-3 control-label">Placement Storage</label>
          <div class="col-sm-5">
            {{ Form::select('placement_storage', $ds, old('placement_storage'), ['placeholder' => 'Select a Type', 'class' => 'form-control', 'required' => 'true', 'id'=>'inputPlacement', 'disabled'=>$disable ]) }}
          </div>
                  
                  <div class="col-sm-3">
                    <button type="button" class="form-control btn btn-success" id="btnStore" {{$disable}}>STORE</button>
                  </div>
                </div>
              </form>

            </div>
          </div>

          <br><br>

          <div class="pull-right">
            @if($result['status'] == 4)
            {{-- <a href="{{ URL::route('production.pdf', $result['id']) }}" class="btn btn-default btn-lg"><i class="fa fa-file-pdf-o"></i>&nbsp;PDF</a> --}}
            @else
            <a href="{{ URL::route('production.close', $result['id'])
            }}" class="btn btn-default btn-lg" onclick="return confirm('Close Production?')"><i class="fa fa-lock"></i>&nbsp;Close</a>
            @endif
            <a href="{{ URL::route('production') }}" class="btn btn-primary btn-lg" >
            <i class="fa fa-list"></i> Go to List</a> 
          </div>
				  <br>
			  </div> 
	    </div>
	  </div>
  </div>
</section>
@stop

@section('scripts')
<script type="text/javascript">
  $(document).ready(function(){
    $("#btnStore").click(function(){
      var dataStorage = $("#dataStorage").val();
      var inputPlacement = $("#inputPlacement").val();

      var urlV = '{{ URL::to("/poststorage") }}';
      var typeV = 'POST';

      $.ajax({
          url : urlV,
          data : 'inputPlacement='+inputPlacement+'&dataStorage='+dataStorage,
          type : typeV,
          // headers : {
          //   'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
          // },
          error : function(jqXHR, textStatus, errorThrown){
              console.log(jqXHR.responseText);

          },
          success : function(data, textStatus, jqXHR){
            console.log(data);
            alert('Data Stored to Storege Successfull');
            // if(data == "TRUE"){
            //   location.reload();
            // }else{
            //   alert("Move Fialed !");
            //   location.reload();
            // }

          }
      });

      console.log(dataStorage);
      console.log(inputPlacement);
    });
  });
</script>
@stop