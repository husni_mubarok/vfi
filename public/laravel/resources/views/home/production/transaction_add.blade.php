@extends('layouts.home.template')
@section('content')
<section class="content box box-solid">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<div class="x_panel">
					<h2>
						<i class="fa fa-sign-out"></i>&nbsp;Relokasi Produksi
					</h2>
					<hr>
					<div class="row bs-wizard small" style="border-bottom:0;">
		                <!-- class name =  1. complete 2.active 3.disabled-->
		                
		                <div class="col-xs-3 bs-wizard-step complete">
		                  {{-- <div class="text-center bs-wizard-stepnum">Start</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="{{ URL::route('production') }}" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Start<br><i>Project</i></div>
		                </div>

		                <div class="col-xs-3 bs-wizard-step complete"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Step 3</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="{{ URL::route('production.transaction', [$addition->production_result->id]) }}" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Step 1<br><i>Pilih Ukuran</i></div>
		                </div>

		                <div class="col-xs-3 bs-wizard-step active"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Step 3</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Step 2<br><i>Tentukan Jumlah<br>Dan Tujuan</i></div>
		                </div>

		                <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Finish</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Finish<br><i>Report</i></div>
		                </div>
		            </div>
		            <hr>
		            {!! Form::open(['route' => ['production.transaction_add_store', $addition->id]]) !!}
		            <div class="col-md-6">
		            	@php
		            	$used_addition = 0;
		            	$added_addition = 0;
		            	foreach($addition->from as $from)
		            	{
		            		$used_addition += $from->amount;
		            	}
		            	foreach($addition->to as $to)
		            	{
		            		$added_addition += $to->amount;
		            	}
		            	@endphp
		            	<h4><i class="fa fa-upload"></i> Sumber</h4>
	            		<table class="table table-border">
	            			<tr>
	            				<th>{{$addition->production_result->production->name}}</th>
	            				<th>{{$addition->size}}</th>
	            			</tr>
	            			<tr>
	            				<th>
	            					{{date("D, d M Y", strtotime($addition->production_result->started_at))}}
	            					<i class="fa fa-arrow-right"></i> 
	            					{{date("D, d M Y", strtotime($addition->production_result->finished_at))}}
	            				</th>
	            				<th>
	            					{{($addition->block - $used_addition + $added_addition) % $addition->production_result->mst_carton}}
	            				</th>
	            			</tr>
	            			<tr>
	            				<th>Jumlah di alokasi</th>
	            				<td>{{ Form::number('amount', old('amount'), ['class' => 'form-control', 'required' => 'true', 'id' => 'amount']) }}</td>
	            			</tr>
	            		</table>
            		</div>
            		<br>
            		<div class="col-md-12">
	            		<h4><i class="fa fa-download"></i> Menuju</h4>
		            	<table id="listTable" class="table table-hover">
		            	<thead>
		            		<tr>
		            			<th>#</th>
		            			<th>Produksi</th>
		            			<th>Tgl. Produksi</th>
		            			<th>Ukuran</th>
		            			<th>MC</th>
		            			<th>Carton</th>
		            			<th>Unpack</th>
		            		</tr>
		            	</thead>
		            	<tbody>
		            	@if(isset($addition_list))
		            	@foreach($addition_list as $item)
            				<tr>
            					<td>{{ Form::radio('addition_id', $item['id']) }}</td>
            					<td>{{$item['product']}}</td>
            					<td>
            						{{$item['start_date']}}
            						<i class="fa fa-arrow-right"></i>
            						{{$item['finish_date']}}
        						</td>
            					<td>{{$item['size']}}</td>
            					<td>{{$item['mst_carton']}}</td>
            					<td>{{$item['carton']}}</td>
            					<td>{{$item['unpack']}}</td>
            				</tr>
		            	@endforeach
		            	@else
		            		<tr>
		            			<td colspan="7">Tidak ada produk dengan ukuran yang sama</td>
		            		</tr>
		            	@endif
		            	</tbody>
		            	</table>

		            	@if(isset($addition_list))
		            	<button type="submit" class="btn btn-lg btn-success"><i class="fa fa-check"></i> Save</button>
		            	@else
		            	<a href="{{ URL::route('production') }}" class="btn btn-warning btn-md"><i class="fa fa-caret-square-o-left"></i> Back to List</a>
		            	@endif
		            </div>
		            {!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</section>

@stop

@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
@if(isset($addition_list))
<script>
$(document).ready(function () {
	$("#listTable").DataTable(
	{
		"pageLength": 5,
		"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "All"] ]
	});
});
</script>
@endif
<script>
$("#amount").on('change', function() {
	if($("#amount").val() > {{($addition->block - $used_addition + $added_addition) % $addition->production_result->mst_carton}})
	{
		alert('Jumlah yang dimasukkan melebihi stok tersisa!');
		$("#amount").val('');
	}
});
</script>
@stop