@extends('layouts.home.template')
@section('content')
<div class="header_menu mobile-hide">
    <a href="{{ URL::route('production') }}">WORKING</a>
    <a href="{{ URL::route('production.unstored') }}">UNSTORED</a>
    <a href="{{ URL::route('production.bank') }}">BANK</a>
</div>
<section class="content">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<div class="x_panel">
					<h2>
	                	<i class="fa fa-pencil"></i> 
	                	<i class="fa fa-dropbox"></i> Result
                	</h2>
					<hr>
					<div class="row bs-wizard small" style="border-bottom:0;">
		                <!-- class name =  1. complete 2.active 3.disabled-->

		                <div class="col-xs-3 bs-wizard-step complete"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Step 2</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Step 1<br><i>Waste Product</i></div>
		                </div>

		                <div class="col-xs-3 bs-wizard-step active"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Step 3</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Step 2<br><i>Finish Product</i></div>
		                </div>

		                <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Step 4</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Step 3<br><i>Additional Product</i></div>
		                </div>
		                <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Finish</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Finish<br><i>Summary</i></div>
		                </div>
		            </div>
		    	  	<hr>
		    	  	<div class="row">
		    	  	@include('errors.error')
		    	  	@if(isset($details))
		    	  	{!! Form::model($production_result, ['route' => ['production.update_result', $production_result->id], 'method' => 'PUT', 'id' => 'form_result']) !!}
		    	  	@else
	                {!! Form::model($production_result, array('route' => ['production.store_result', $production_result->id], 'id' => 'form_result'))!!}
	                @endif
                    {{ csrf_field() }}
                    <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                    	<div class="col-md-8">
                        {{ Form::label('finished_at', 'Finish Date') }}
                        {{ Form::text('finished_at', old('finished_at'), array('class' => 'form-control', 'placeholder' => 'Date & Time*', 'required' => 'true', 'id' => 'finished_at')) }}
                        </div>

                        {{-- <div class="col-md-4">
	                        {{ Form::label('uom', 'Unit of Measurement') }}
	                        {{ Form::select('uom_id', $uom_list, old('uom_id'), array('class' => 'form-control')) }}
                        </div> --}}
                        <input type="hidden" name="uom_id" value="2">
                        <br/>

                        <table class="table table-bordeded">
                        <thead>
                        	<tr>
                        		<th width="17%">Type</th>
                        		<th colspan="3">Size</th>
                        		<th width="13%">Block</th>
                        		<th width="20%"></th>
                        	</tr>
                        </thead>
                        <tbody id="cart">
                        	<tr>
                        	</tr>
                        </tbody>
                        <tfoot>
                        	<tr>
                        		<td>
                        			<select name="type" class="form-control" id="input_type">
                        				<option value="single">Single</option>
                        				<option value="range">Range</option>
                        			</select>
                        		</td>
                    			<td class="input_single" colspan="3">
                    				<input type="number" class="form-control" id="size_single" min="0">
                    			</td>
                				<td class="input_range"  style="display:none"><input type="number" class="form-control" id="size_range_1" min="0"></td>
                    			<td class="input_range"  style="display:none">~</td>
                    			<td class="input_range"  style="display:none"><input type="number" class="form-control" id="size_range_2" min="0"></td>
                        		<td><button type="button" class="btn btn-primary btn-md" id="btn_add"><i class="fa fa-plus"></i></button></td>
                        	</tr>
                        </tfoot>
                        </table>
                        <button type="button" id="btn_submit" class="btn btn-success pull-right" data-toggle="modal" data-target="#confirmation_modal"><i class="fa fa-check"></i> Save</button>
                    	{!! Form::close() !!}
		        	</div>

		        	{{-- <div class="col-md-6 col-sm-6 col-xs-6">
		        		<h4>Production Detail</h4>
				        <table class="table table-bordered">
				          <tr>
				            <th>Product</th>
				            <td>{{$production_result->production->name}}</td>
				          </tr>
				          <tr>
				            <th>Weight</th>
				            <td>{{number_format($production_result->weight, 2)}} kg</td>
				          </tr>
				          <tr>
				            <th>Block Weight</th>
				            <td>{{number_format($production_result->block_weight, 2)}} kg</td>
				          </tr>
				          <tr>
				            <th>Production Date</th>
				            <td>
				              {{date("D, d M Y", strtotime($production_result->started_at))}}
				              &nbsp;<i class="fa fa-long-arrow-right"></i>&nbsp;
				              @if($production_result->finished_at == null)
				              <i class="fa fa-question-circle-o"></i>
				              @else
				              {{date("D, d M Y", strtotime($production_result->finished_at))}} 
				              @endif
				            </td>
				          </tr>
				          <tr>
				            <th>Criteria</th>
				            <td>{{$production_result->criteria}}</td>
				          </tr>
				        </table>
		        	</div> --}}
	    	  	</div>
    	  	</div>
	  	</div>
  	</div>
</section>
@stop

@section('modal')
<div class="modal fade" id="confirmation_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="myModalLabel">Result Summary</h4>
      		</div>
      		<div class="modal-body">
      			<table class="table">
      				<thead>
      					<th width="50%">Size</th>
      					<th width="50%">Block</th>
      				</thead>
      				<tbody id="result_list">
      				</tbody>
      			</table>
      		</div>
      		<div class="modal-footer">
      			<button type="button" id="btn_confirm" class="btn btn-success"><i class="fa fa-check"></i> YES</button>
      			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> NO</button>
      		</div>
    	</div>
  	</div>
</div>
@stop

@section('scripts')
<script>
var cart = JSON.parse("{}");
$(document).ready(function () {
	$('#finished_at').datetimepicker({
		format: 'YYYY-MM-DD',
	});

	var size = [];
});

$("#input_type").on('change', function() {
	if($("#input_type").val() == 'single') {
		$(".input_single").show();
		$(".input_range").hide();
		$("#size_range_1").val('');
		$("#size_range_2").val('');
	} else if($("#input_type").val() == 'range') {
		$(".input_range").show();
		$(".input_single").hide();
		$("#size_single").val('');
	}
});

$("#btn_add").click(function() {
	if($("#input_type").val() == 'single') {
	    var outputString = '';
	    var size = $("#size_single").val();
	    if(size)
	    {
		    if(cart[size]==undefined){
		    	cart[size]=1;
		    	outputString += '<tr>';
			    outputString += '<td></td>';
			    outputString += '<td colspan="3">';
			    outputString += size;
			    outputString += '<input type="hidden" id="size['+size+']" name="size['+size+']" value="'+size+'">';
			    outputString += '</td>';
			    outputString += '<td><div id="block_'+size+'">'+cart[size]+'</div><input type="hidden" id="block_value_'+size+'" name="block['+size+']" value="'+cart[size]+'" class="form-control"></td>';
			    outputString += '<td>';
			    outputString += '<button type="button" class="btn btn-primary btn-md" id="btn_add_'+size+'"><i class="fa fa-plus"></i></button>';
			    outputString += '<button type="button" class="btn btn-warning btn-md" id="btn_reduce_'+size+'"><i class="fa fa-minus"></i></button>';
			    outputString += '<button type="button" class="btn btn-danger btn-md" id="btn_remove_'+size+'"><i class="fa fa-remove"></i></button>';
			    outputString += '</td>';
			    outputString += '</tr>';

			    $("#cart").append(outputString);
			    $("#btn_reduce_"+size).on('click', function(){
			    	if(cart[size] > 1)
			        { 
				        cart[size] = cart[size]- 1;
						$("#block_"+size).html(cart[size]);
						$("#block_value_"+size).val(cart[size]);
					} else {
						cart[size]= null;
		        		$(this).parent().parent().remove();
					}
			    });

			    $("#btn_add_"+size).on('click', function(){
			        cart[size] =  cart[size]+1;
			    	$("#block_value_"+size).val(cart[size]);
					$("#block_"+size).html(cart[size]);
			    });

			    $("#btn_remove_"+size).on('click',function(){	   
					cart[size]= null;
		        	$(this).parent().parent().remove();
			    });

		    }else{
				cart[size]=cart[size]+1;	
				$("#block_value_"+size).val(cart[size]);
				$("#block_"+size).html(cart[size]);
		    }
		    $("#size_single").val('');
		    $("#block").val('');
	    }
	   
	  
	} else if ($("#input_type").val() == 'range') {
		var outputString = '';
		var size = $("#size_range_1").val()+'_'+$("#size_range_2").val();
		if(size)
		{
		    if(cart[size]==undefined){
		    	cart[size]=1;
		    	outputString += '<tr>';
			    outputString += '<td></td>';
			    outputString += '<td colspan="3">';
			    outputString += $("#size_range_1").val()+"~"+$("#size_range_2").val();
			    outputString += '<input type="hidden" id="size['+size+']" name="size['+size+']" value="'+size+'">';
			    outputString += '</td>';
			    outputString += '<td><div id="block_'+size+'">'+cart[size]+'</div><input type="hidden" id="block_value_'+size+'" name="block['+size+']" value="'+cart[size]+'" class="form-control"></td>';
			    outputString += '<td>';
			    outputString += '<button type="button" class="btn btn-primary btn-md" id="btn_add_'+size+'"><i class="fa fa-plus"></i></button>';
			    outputString += '<button type="button" class="btn btn-warning btn-md " id="btn_reduce_'+size+'"><i class="fa fa-minus"></i></button>';
			    outputString += '<button type="button" class="btn btn-danger btn-md" id="btn_remove_'+size+'"><i class="fa fa-remove"></i></button>';
			    outputString += '</td>';
			    outputString += '</tr>';

			    $("#cart").append(outputString);
			    $("#btn_reduce_"+size).on('click', function(){
			    	if(cart[size] > 1)
			        { 
				        cart[size] =  cart[size]-1;
						$("#block_"+size).html(cart[size]);
						$("#block_value_"+size).val(cart[size]);
					} else {
						cart[size]= null;
		        		$(this).parent().parent().remove();
					}
			    });
			    $("#btn_add_"+size).on('click', function(){
			        cart[size] =  cart[size]+1;
			        $("#block_value_"+size).val(cart[size]);
					$("#block_"+size).html(cart[size]);
			    });

			    $("#btn_remove_"+size).on('click',function(){	   
					cart[size]= null;
		        	$(this).parent().parent().remove();
			    });
		    }else{
				cart[size]=cart[size]+1;
				$("#block_value_"+size).val(cart[size]);
				$("#block_"+size).html(cart[size]);
		    }

			$("#size_range_1").val('');
		    $("#size_range_2").val('');
		    $("#block").val('');
		}

	    
	}
});

$("#btn_submit").on('click', function() 
{
	$("#result_list").empty();
	var new_row = '';
	jQuery.each(cart, function(i, val)
	{
		if(val != null)
		{
			new_row += '<tr>';
			new_row += '<td>';
			new_row += i.replace(/_/g,"~");
			new_row += '</td>';
			new_row += '<td>';
			new_row += val;
			new_row += '</td>';
			new_row += '</tr>';
		}
	});
	$("#result_list").append(new_row);
});

$("#btn_confirm").on('click', function() {
	$("#form_result").submit();
});
</script>

@if(isset($details))
<script>
jQuery.each({!! $details !!}, function( i, val ) {
	var size = val['size'];
	var block = val['block'];
	cart[size] = block;
    var outputString = '';

   	outputString += '<tr>';
    outputString += '<td></td>';
    outputString += '<td colspan="3">';
    outputString += size;
    outputString += '<input type="hidden" id="size['+size+']" name="size['+size+']" value="'+size+'">';
    outputString += '</td>';
    outputString += '<td><div id="block_'+size+'">'+cart[size]+'</div><input type="hidden" id="block_value_'+size+'" name="block['+size+']" value="'+cart[size]+'" class="form-control"></td>';
    outputString += '<td>';
    outputString += '<button type="button" class="btn btn-primary btn-md" id="btn_add_'+size+'"><i class="fa fa-plus"></i></button>';
    outputString += '<button type="button" class="btn btn-warning btn-md" id="btn_reduce_'+size+'"><i class="fa fa-minus"></i></button>';
    outputString += '<button type="button" class="btn btn-danger btn-md" id="btn_remove_'+size+'"><i class="fa fa-remove"></i></button>';
    outputString += '</td>';
    outputString += '</tr>';

    $("#cart").append(outputString);
    $("#btn_reduce_"+size).on('click', function(){
    	if(cart[size] > 1)
        { 
	        cart[size] = cart[size] - 1;
        	$("#block_value_"+size).val(cart[size]);
			$("#block_"+size).html(cart[size]);
		} else {
			cart[size]= null;
    		$(this).parent().parent().remove();
		}
    });

    $("#btn_add_"+size).on('click', function(){
        cart[size] =  cart[size] + 1;
    	$("#block_value_"+size).val(cart[size]);
		$("#block_"+size).html(cart[size]);
    });

    $("#btn_remove_"+size).on('click',function(){	   
		cart[size]= null;
    	$(this).parent().parent().remove();
    });
});
</script>
@endif
@stop