@extends('layouts.home.template')
@section('content')
<div class="header_menu mobile-hide">
    <a href="{{ URL::route('production') }}">WORKING</a>
    <a href="{{ URL::route('production.unstored') }}">UNSTORED</a>
    <a href="{{ URL::route('production.bank') }}">BANK</a>
</div>
<section class="content">
	
			<div class="x_panel">
				<h2>
                	<i class="fa fa-pencil"></i> 
                	<i class="fa fa-plus-square-o"></i> Results
            	</h2>
	    	  	<hr>
	    	  	<div class="row">
    	  			{{-- Reject Products --}}
	            	<div class="col-md-12 col-xs-12 col-sm-12">
	            		@if(isset($results))
	            		{!! Form::model($results, ['route' => ['production.update_results', $production_result->id], 'id' => 'form_results', 'method' => 'PUT']) !!}
	            		@else
	            		{!! Form::open(['route' => ['production.store_results', $production_result->id], 'id' => 'form_results']) !!}
	            		@endif
		                <div class="panel-group" id="accordion">
						  	<div class="panel panel-default">
						    	<div class="panel-heading">
						      		<h4 class="panel-title">
						        		<a data-toggle="collapse" data-parent="#accordion" href="#main_product">Main Products</a>
						      		</h4>
						    	</div>
						    	<div id="main_product" class="panel-collapse collapse in">
						      		<div class="panel-body">
						      			<div class="row">
						      				<div class="col-md-12">
						      					<div class="form-group">
						      						{{ Form::label('finish_date', 'Finish Date') }}
						      						{{ Form::text('finish_date', old('finish_date'), ['class' => 'form-control', 'id' => 'finish_date']) }}
						      					</div>
						      				</div>
						      			</div>
						      			<div class="row">
						      				<div class="col-md-5 col-sm-12">	
						      					<div class="form-group">
												    <label for="">Size</label>
												   {{ Form::text('main_product_size', null, ['class' => 'form-control', 'id' => 'main_product_size','placeholder' => 'Contoh: 1, 5, 2-3, 4-7']) }}
												</div>
						      				</div>
						      				<div class="col-md-5 col-sm-12">
						      					<div class="form-group">
												    <label for="">Block</label>
												    <input type="text" class="form-control" id="" placeholder="" disabled="disabled">
												</div>
						      				</div>
						      				<div class="col-md-2 col-sm-12">
						      					<div class="form-group">
												    <label for="">&nbsp;</label>
												   	<button type="button" id="main_product_add" class="form-control btn btn-md btn-primary"><i class="fa fa-plus"></i>&nbsp;&nbsp;Add</button>
												</div>
						      					
						      				</div>
						      			</div>

						      			<table class="table">
						      			<thead>
						      				<tr>
						      					<th width="40%"> </th>
						      					<th width="40%"></th>
						      					<td width="20%"></td>
						      				</tr>
						      			</thead>
						      			<tbody id="main_product_cart">
						      				{{-- Main Product Cart --}}
						      			</tbody>
						      			<tfoot>
						      				<tr>
						      					<td>
						      						
						      					</td>
						      					<td></td>
						      					<td>
						      						
						      					</td>
						      				</tr>
						      			</tfoot>
						      			</table>
					      			</div>
						    	</div>
						  	</div>

						  	<div class="panel panel-default">
						    	<div class="panel-heading">
						      		<h4 class="panel-title">
						        		<a data-toggle="collapse" data-parent="#accordion" href="#additional_product">Additional Products</a>
						      		</h4>
						    	</div>
						    	<div id="additional_product" class="panel-collapse collapse">
						      		<div class="panel-body">
						      			<div class="row">
						      				<div class="col-md-2 col-xs-12">
						      					<div class="form-group">
												    <label for="">Additional Type</label>
												   	{{ Form::text('additional_product_reject_type', null, ['class' => 'form-control', 'id' => 'additional_product_reject_type']) }}
												</div>
						      				</div>
						      				<div class="col-md-2 col-xs-12">
						      					<div class="form-group">
												    <label for="">Reason</label>
												   	{{ Form::text('additional_product_reject_reason', null, ['class' => 'form-control', 'id' => 'additional_product_reject_reason']) }}
												</div>
						      				</div>
						      				<div class="col-md-2 col-xs-12">
						      					<div class="form-group">
												    <label for="">Block Weight</label>
												   	{{ Form::number('additional_product_block_weight', null, ['class' => 'form-control', 'id' => 'additional_product_block_weight', 'min' => '0', 'step' => '0.01']) }}
												</div>
						      				</div>
						      				<div class="col-md-3 col-xs-12">
						      					<div class="form-group">
												    <label for="">Size</label>
												   {{ Form::text('additional_product_size', null, ['class' => 'form-control', 'id' => 'additional_product_size','placeholder' => 'Contoh: 1, 5, 2-3, 4-7']) }}
												</div>
						      				</div>
						      				<div class="col-md-2 col-xs-12">
						      					<div class="form-group">
												    <label for="">Block</label>
												   	<input type="text" class="form-control" disabled="disabled">
												</div>
						      				</div>
						      				<div class="col-md-1 col-xs-12">
							      				<div class="form-group">
							      					<label for="">&nbsp;</label>
							      					<button type="button" id="additional_product_add" class="btn btn-md btn-primary form-control btn-block"><i class="fa fa-plus"></i>&nbsp;&nbsp;Add</button>
							      				</div>
						      				</div>
						      			</div>
				      					{{-- <table class="table">
						      			<thead>
						      				<tr>
						      					<th width="25%">Additional Type </th>
						      					<th width="25%">Reason </th>
						      					<th width="10%">Block Weight </th>
						      					<th width="10%">Size (untuk size range gunakan tanda -. Contoh: 1-3, 4-5) </th>
						      					<th width="15%">Block </th>
						      					<td width="15%"></td>
						      				</tr>
						      			</thead>
						      			<tbody id="additional_product_cart"> --}}
						      				{{-- Additional Product Cart --}}
						      			{{-- </tbody>
						      			<tfoot>
						      				<tr>
						      					<td>
						      						
						      					</td>
						      					<td>
						      						
						      					</td>
						      					<td>
						      						
						      					</td>
						      					<td>
						      						
						      					</td>
						      					<td></td>
						      					<td>
						      						
						      					</td>
						      				</tr>
						      			</tfoot>
						      			</table> --}}
						      		</div>
						    	</div>
						  	</div>

						  	<div class="panel panel-default">
						    	<div class="panel-heading">
						      		<h4 class="panel-title">
						        		<a data-toggle="collapse" data-parent="#accordion" href="#waste_product">Waste Products</a>
						      		</h4>
						    	</div>
						    	<div id="waste_product" class="panel-collapse collapse">
						      		<div class="panel-body">
						      			<div class="row">
						      				<div class="col-md-5 col-xs-12">
						      					<div class="form-group">
												    <label for="">Nama</label>
												   	{{ Form::text('waste_product_name', null, ['class' => 'form-control', 'id' => 'waste_product_name']) }}
												</div>
						      				</div>
						      				<div class="col-md-5 col-xs-12">
						      					<div class="form-group">
												    <label for="">Berat</label>
												   	{{ Form::number('waste_product_weight', null, ['class' => 'form-control', 'id' => 'waste_product_weight', 'step' => '0.01', 'min' => '0']) }}
												</div>
						      				</div>
						      				<div class="col-md-2 col-xs-12">
						      					<div class="form-group">
												    <label for="">&nbsp;</label>
												   	<button type="button" id="waste_product_add" class="btn btn-md btn-primary btn-block"><i class="fa fa-plus"></i>&nbsp;&nbsp;Add</button>
												</div>
						      				</div>

						      			</div>
						      			{{-- <table class="table">
						      			<thead>
						      				<tr>
						      					<th width="40%">Nama </th>
						      					<th width="40%">Berat </th>
						      					<td width="40%"></td>
						      				</tr>
						      			</thead>
						      			<tbody id="waste_product_cart"> --}}
						      				{{-- Waste Product Cart --}}
						      			{{-- </tbody>
						      			<tfoot>
						      				<tr>
						      					<td>
						      						
						      					</td>
						      					<td>
						      						
						      					</td>
						      					<td>
						      						
						      					</td>
						      					<td></td>
						      				</tr>
						      			</tfoot>
						      			</table> --}}
						      		</div>
						    	</div>
						  	</div>
						</div>
						<center>
							<button class="btn btn-lg btn-default" id="btn_summarize" type="button" data-toggle="modal" data-target="#confirm_modal"><i class="fa fa-paper-plane"></i> Submit</button>
						</center>

						{!! Form::close() !!}
            		</div>
            		{{-- Reject Products --}}

            		{{-- Production Details --}}
            		{{-- <div class="col-md-6">
		        		<h4>Production Detail</h4>
		        		<table class="table table-bordered">
		          			<tr>
			            		<th>Product</th>
		            			<td>{{$production_result->production->name}}</td>
		          			</tr>
		          			<tr>
		            			<th>Weight</th>
		            			<td>{{number_format($production_result->weight, 2)}} kg</td>
		          			</tr>
		          			<tr>
		            			<th>Block Weight</th>
		            			<td>{{number_format($production_result->block_weight, 2)}} kg</td>
		          			</tr>
		          			<tr>
		            			<th>Production Date</th>
		            			<td>
		              				{{date("D, d M Y", strtotime($production_result->started_at))}}
		              				&nbsp;<i class="fa fa-long-arrow-right"></i>&nbsp;
		              				@if($production_result->finished_at == null)
		              				<i class="fa fa-question-circle-o"></i>
		              				@else
		              				{{date("D, d M Y", strtotime($production_result->finished_at))}} 
		              				@endif
		            			</td>
		          			</tr>
		          			<tr>
		            			<th>Criteria</th>
		            			<td>{{$production_result->criteria}}</td>
		          			</tr>
        				</table>
		      		</div> --}}
            		{{-- Production Details --}}
		        </div>
    	  	</div>
	  	
</section>
@stop

@section('modal')
<div class="modal fade" id="confirm_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="myModalLabel">Production Summary</h4>
      		</div>
      		<div class="modal-body">
      			<h3>Main Product</h3>
      			<table class="table">
      			<thead>
      				<tr>
      					<th>Size</th>
      					<th>Block</th>
      				</tr>
      			</thead>
      			<tbody id="main_product_summary">
      			</tbody>
      			</table>
      			<hr>
      			<h3>Additional Product</h3>
      			<table class="table">
      			<thead>
      				<tr>
      					<th>Additional Type</th>
      					<th>Reason</th>
      					<th>Block Weight</th>
      					<th>Size</th>
      					<th>Block</th>
      				</tr>
      			</thead>
      			<tbody id="additional_product_summary">
      			</tbody>
      			</table>
      			<hr>
      			<h3>Waste Product</h3>
      			<table class="table">
      			<thead>
      				<tr>
      					<th>Name</th>
      					<th>Weight</th>
      				</tr>
      			</thead>
      			<tbody id="waste_product_summary">
      			</tbody>
      			</table>
      		</div>
      		<div class="modal-footer">
      			<button type="button" id="confirm_submit" class="btn btn-success"><i class="fa fa-check"></i> YES</button>
      			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> NO</button>
      		</div>
    	</div>
  	</div>
</div>
@stop

@section('scripts')
<script>
$('#confirm_submit').on('click', function()
{
	$('#form_results').submit();
});

//initialize objects
var main_product_cart = JSON.parse("{}");
var additional_product_cart = JSON.parse("{}");
var waste_product_cart = JSON.parse("{}");

$(document).ready(function () {
	$('#finish_date').datetimepicker({
		format: 'DD-MM-YYYY',
	});
});

//Main Product Cart
$('#main_product_add').on('click', function() 
{
	var size = $('#main_product_size').val().replace(/[^0-9-]/g, "").replace(/[^0-9-]/g, "").replace(/\-/g,'~');
	var key = size.replace(/~/g, '_').replace(/\./g, 'x');
	if(key)
	{
		var key_length = key.split('_').length;
		if(key_length == 1 || key_length == 2)
		{
			if(main_product_cart[key] == undefined)
			{
				main_product_cart[key] = 1;
				var new_row = '';
				new_row += '<tr>';
				new_row += '<input type="hidden" name="main_product_cart['+key+'][size]" value="'+size+'">';
				new_row += '<td>';
				new_row += size;
				new_row += '</td>';
				new_row += '<input type="hidden" name="main_product_cart['+key+'][block]" id="main_product_'+key+'_value" value="'+main_product_cart[key]+'">';
				new_row += '<td id="main_product_'+key+'_text">';
				new_row += main_product_cart[key];
				new_row += '</td>';
				new_row += '<td>';
				new_row += '<button type="button" id="main_product_'+key+'_add" class="btn btn-lg btn-primary"><i class="fa fa-plus"></i></button>';
				new_row += '<button type="button" id="main_product_'+key+'_subtract" class="btn btn-lg btn-warning"><i class="fa fa-minus"></i></button>';
				new_row += '<button type="button" id="main_product_'+key+'_delete" class="btn btn-lg btn-danger"><i class="fa fa-remove"></i></button>';
				new_row += '</td>';
				new_row += '</tr>';

				$('#main_product_cart').append(new_row);

				//listener for add button
				$('#main_product_'+key+'_add').on('click', function()
				{
					main_product_cart[key] = parseFloat(main_product_cart[key]) + 1;
					$('#main_product_'+key+'_text').text(main_product_cart[key]);
					$('#main_product_'+key+'_value').val(main_product_cart[key]);
				});

				//listener for subtract button
				$('#main_product_'+key+'_subtract').on('click', function()
				{
					if(main_product_cart[key] > 1)
					{
						main_product_cart[key] = parseFloat(main_product_cart[key]) - 1;
						$('#main_product_'+key+'_text').text(main_product_cart[key]);
						$('#main_product_'+key+'_value').val(main_product_cart[key]);
					} else {
						main_product_cart[key] = null;
						$(this).parent().parent().remove();
					}
				});

				//listener for delete button
				$('#main_product_'+key+'_delete').on('click', function()
				{
					main_product_cart[key] = null;
					$(this).parent().parent().remove();
				});
			}
			else
			{
				main_product_cart[key] = parseFloat(main_product_cart[key]) + 1;
				$('#main_product_'+key+'_text').text(main_product_cart[key]);
				$('#main_product_'+key+'_value').val(main_product_cart[key]);
			}

			$('#main_product_size').val('');
		}
	}
});


//Additional Product Cart
$('#additional_product_add').on('click', function()
{
	var reason = $('#additional_product_reject_reason').val().trim().toLowerCase().replace(/[^a-zA-Z0-9 ]/g, "");
	var type = $('#additional_product_reject_type').val().trim().toLowerCase().replace(/[^a-zA-Z0-9 ]/g, "");
	var block_weight = $('#additional_product_block_weight').val();
	var size = $('#additional_product_size').val().replace(/[^0-9-]/g, "").replace(/\-/g,'~');
	var key = reason.replace(/ /g, "xyz")+"_"+type.replace(/ /g, "xyz")+"_"+block_weight.replace(/\./g, 'x')+size.replace(/~/g, '_');
	console.log(size);
	if(reason && type && block_weight && size && key)
	{
		if(additional_product_cart[key] == undefined)
		{
			additional_product_cart[key] = [];
			additional_product_cart[key]['value'] = 1;
			additional_product_cart[key]['reason'] = reason;
			additional_product_cart[key]['type'] = type;
			additional_product_cart[key]['block_weight'] = block_weight;
			additional_product_cart[key]['size'] = size;
			
			var new_row = '';
			new_row += '<tr>';
			new_row += '<input type="hidden" name="additional_product_cart['+key+'][reject_type]" value="'+type+'">';
			new_row += '<td>';
			new_row += type;
			new_row += '</td>';
			new_row += '<input type="hidden" name="additional_product_cart['+key+'][reject_reason]" value="'+reason+'">';
			new_row += '<td>';
			new_row += reason;
			new_row += '</td>';
			new_row += '<input type="hidden" name="additional_product_cart['+key+'][block_weight]" value="'+block_weight+'">';
			new_row += '<td>';
			new_row += block_weight;
			new_row += '</td>';
			new_row += '<input type="hidden" name="additional_product_cart['+key+'][size]" value="'+size+'">';
			new_row += '<td>';
			new_row += size;
			new_row += '</td>';
			new_row += '<input type="hidden" name="additional_product_cart['+key+'][block]" id="additional_product_'+key+'_value" value="'+additional_product_cart[key]['value']+'">';
			new_row += '<td id="additional_product_'+key+'_text">';
			new_row += additional_product_cart[key]['value'];
			new_row += '</td>';
			new_row += '<td>';
			new_row += '<button type="button" id="additional_product_'+key+'_add" class="btn btn-lg btn-primary"><i class="fa fa-plus"></i></button>';
			new_row += '<button type="button" id="additional_product_'+key+'_subtract" class="btn btn-lg btn-warning"><i class="fa fa-minus"></i></button>';
			new_row += '<button type="button" id="additional_product_'+key+'_delete" class="btn btn-lg btn-danger"><i class="fa fa-remove"></i></button>';
			new_row += '</td>';
			new_row += '</tr>';

			$('#additional_product_cart').append(new_row);

			//listener for add button
			$('#additional_product_'+key+'_add').on('click', function()
			{
				additional_product_cart[key]['value'] = parseFloat(additional_product_cart[key]['value']) + 1;
				$('#additional_product_'+key+'_text').text(additional_product_cart[key]['value']);
				$('#additional_product_'+key+'_value').val(additional_product_cart[key]['value']);
			});

			//listener for subtract button
			$('#additional_product_'+key+'_subtract').on('click', function()
			{
				if(additional_product_cart[key]['value'] > 1)
				{
					additional_product_cart[key]['value'] = parseFloat(additional_product_cart[key]['value']) - 1;
					$('#additional_product_'+key+'_text').text(additional_product_cart[key]['value']);
					$('#additional_product_'+key+'_value').val(additional_product_cart[key]['value']);
				} else {
					additional_product_cart[key] = null;
					$(this).parent().parent().remove();
				}
			});

			//listener for delete button
			$('#additional_product_'+key+'_delete').on('click', function()
			{
				additional_product_cart[key] = null;
				$(this).parent().parent().remove();
			});
		}
		else 
		{
			additional_product_cart[key]['value'] += 1;
			$('#additional_product_'+key+'_text').text(additional_product_cart[key]['value']);
			$('#additional_product_'+key+'_value').val(additional_product_cart[key]['value']);
		}

		$('#additional_product_reject_reason').val('');
		$('#additional_product_reject_type').val('');
		$('#additional_product_block_weight').val('');
		$('#additional_product_size').val('');
	}
});

//Waste Product Cart
$('#waste_product_add').on('click', function()
{
	var name = $('#waste_product_name').val().trim().toLowerCase().replace(/[^a-zA-Z0-9 ]/g, "");
	var weight = parseFloat($('#waste_product_weight').val());
	var key = name.replace(/ /g, "_");
	if(key)
	{
		if(waste_product_cart[key] == undefined)
		{
			waste_product_cart[key] = weight;
			var new_row = '';
			new_row += '<tr>';
			new_row += '<input type="hidden" name="waste_product_cart['+key+'][name]" value="'+name+'">';
			new_row += '<td>';
			new_row += name;
			new_row += '</td>';
			new_row += '<input type="hidden" name="waste_product_cart['+key+'][weight]" id="waste_product_'+key+'_value" value="'+weight+'">';
			new_row += '<td id="waste_product_'+key+'_text">';
			new_row += weight;
			new_row += '</td>';
			new_row += '<td>';
			new_row += '<button type="button" id="waste_product_'+key+'_delete" class="btn btn-lg btn-danger"><i class="fa fa-remove"></i></button>';
			new_row += '</td>';
			new_row += '</tr>';

			$('#waste_product_cart').append(new_row);

			//listener for delete button
			$('#waste_product_'+key+'_delete').on('click', function()
			{
				waste_product_cart[key] = null;
				$(this).parent().parent().remove();
			});
		} 
		else 
		{
			waste_product_cart[key] += weight;
			$('#waste_product_'+key+'_text').text(waste_product_cart[key]);
			$('#waste_product_'+key+'_value').val(waste_product_cart[key]);
		}

		$('#waste_product_name').val('');
		$('#waste_product_weight').val('');
	} 
});

$('#btn_summarize').on('click', function() {
	$('#main_product_summary').empty();
	$('#additional_product_summary').empty();
	$('#waste_product_summary').empty();

	jQuery.each(main_product_cart, function (key, value)
	{
		if(value)
		{
			var new_row = '';
			new_row += '<tr>';
			new_row += '<td>';
			new_row += key.replace(/_/g, '~');
			new_row += '</td>';
			new_row += '<td>';
			new_row += value;
			new_row += '</td>';
			new_row += '</tr>';
			$('#main_product_summary').append(new_row);
		}
	});

	jQuery.each(additional_product_cart, function (key, value)
	{
		if(value)
		{
			var new_row = '';
			new_row += '<tr>';
			new_row += '<td>';
			new_row += value['type'];
			new_row += '</td>';
			new_row += '<td>';
			new_row += value['reason'];
			new_row += '</td>';
			new_row += '<td>';
			new_row += value['block_weight'] + ' kg';
			new_row += '</td>';
			new_row += '<td>';
			new_row += value['size'];
			new_row += '</td>';
			new_row += '<td>';
			new_row += value['value'];
			new_row += '</td>';
			new_row += '</tr>';
			$('#additional_product_summary').append(new_row);
		}
	});

	jQuery.each(waste_product_cart, function (key, value)
	{
		if(value)
		{
			var new_row = '';
			new_row += '<tr>';
			new_row += '<td>';
			new_row += key.replace(/_/g, ' ');
			new_row += '</td>';
			new_row += '<td>';
			new_row += value;
			new_row += '</td>';
			new_row += '</tr>';
			$('#waste_product_summary').append(new_row);
		}
	})
});

</script>

@if(isset($results))
<script>
jQuery.each({!! $production_result->production_result_detail !!}, function (key, value)
{
	var size = value['size'];
	var block = parseFloat(value['block']);
	var key = size.replace(/~/g, '_');
	main_product_cart[key] = block;
	var new_row = '';
	new_row += '<tr>';
	new_row += '<input type="hidden" name="main_product_cart['+key+'][size]" value="'+size+'">';
	new_row += '<td>';
	new_row += size;
	new_row += '</td>';
	new_row += '<input type="hidden" name="main_product_cart['+key+'][block]" id="main_product_'+key+'_value" value="'+main_product_cart[key]+'">';
	new_row += '<td id="main_product_'+key+'_text">';
	new_row += main_product_cart[key];
	new_row += '</td>';
	new_row += '<td>';
	new_row += '<button type="button" id="main_product_'+key+'_add" class="btn btn-lg btn-primary"><i class="fa fa-plus"></i></button>';
	new_row += '<button type="button" id="main_product_'+key+'_subtract" class="btn btn-lg btn-warning"><i class="fa fa-minus"></i></button>';
	new_row += '<button type="button" id="main_product_'+key+'_delete" class="btn btn-lg btn-danger"><i class="fa fa-remove"></i></button>';
	new_row += '</td>';
	new_row += '</tr>';

	$('#main_product_cart').append(new_row);

	//listener for add button
	$('#main_product_'+key+'_add').on('click', function()
	{
		main_product_cart[key] = parseFloat(main_product_cart[key]) + 1;
		$('#main_product_'+key+'_text').text(main_product_cart[key]);
		$('#main_product_'+key+'_value').val(main_product_cart[key]);
	});

	//listener for subtract button
	$('#main_product_'+key+'_subtract').on('click', function()
	{
		if(main_product_cart[key] > 1)
		{
			main_product_cart[key] = parseFloat(main_product_cart[key]) - 1;
			$('#main_product_'+key+'_text').text(main_product_cart[key]);
			$('#main_product_'+key+'_value').val(main_product_cart[key]);
		} else {
			main_product_cart[key] = null;
			$(this).parent().parent().remove();
		}
	});

	//listener for delete button
	$('#main_product_'+key+'_delete').on('click', function()
	{
		main_product_cart[key] = null;
		$(this).parent().parent().remove();
	});
});

jQuery.each({!! $production_result->production_result_addition !!}, function (key, value)
{
	var reason = value['reject_reason'];
	var type = value['reject_type'];
	var block_weight = value['block_weight'].toString();
	var size = value['size'];
	var key = reason.replace(/ /g, '_')+"_"+type.replace(/ /g, '_')+"_"+block_weight.replace(/\./g, 'x')+size.replace(/~/g, '_');
	additional_product_cart[key] = [];
	additional_product_cart[key]['value'] = parseFloat(value['block']);
	additional_product_cart[key]['reason'] = reason;
	additional_product_cart[key]['type'] = type;
	additional_product_cart[key]['block_weight'] = block_weight;
	additional_product_cart[key]['size'] = size;
	
	var new_row = '';
	new_row += '<tr>';
	new_row += '<input type="hidden" name="additional_product_cart['+key+'][reject_type]" value="'+type+'">';
	new_row += '<td>';
	new_row += type;
	new_row += '</td>';
	new_row += '<input type="hidden" name="additional_product_cart['+key+'][reject_reason]" value="'+reason+'">';
	new_row += '<td>';
	new_row += reason;
	new_row += '</td>';
	new_row += '<input type="hidden" name="additional_product_cart['+key+'][block_weight]" value="'+block_weight+'">';
	new_row += '<td>';
	new_row += block_weight;
	new_row += '</td>';
	new_row += '<input type="hidden" name="additional_product_cart['+key+'][size]" value="'+size+'">';
	new_row += '<td>';
	new_row += size;
	new_row += '</td>';
	new_row += '<input type="hidden" name="additional_product_cart['+key+'][block]" id="additional_product_'+key+'_value" value="'+additional_product_cart[key]['value']+'">';
	new_row += '<td id="additional_product_'+key+'_text">';
	new_row += additional_product_cart[key]['value'];
	new_row += '</td>';
	new_row += '<td>';
	new_row += '<button type="button" id="additional_product_'+key+'_add" class="btn btn-lg btn-primary"><i class="fa fa-plus"></i></button>';
	new_row += '<button type="button" id="additional_product_'+key+'_subtract" class="btn btn-lg btn-warning"><i class="fa fa-minus"></i></button>';
	new_row += '<button type="button" id="additional_product_'+key+'_delete" class="btn btn-lg btn-danger"><i class="fa fa-remove"></i></button>';
	new_row += '</td>';
	new_row += '</tr>';

	$('#additional_product_cart').append(new_row);

	//listener for add button
	$('#additional_product_'+key+'_add').on('click', function()
	{
		additional_product_cart[key]['value'] = parseFloat(additional_product_cart[key]['value']) + 1;
		$('#additional_product_'+key+'_text').text(additional_product_cart[key]['value']);
		$('#additional_product_'+key+'_value').val(additional_product_cart[key]['value']);
	});

	//listener for subtract button
	$('#additional_product_'+key+'_subtract').on('click', function()
	{
		if(additional_product_cart[key]['value'] > 1)
		{
			additional_product_cart[key]['value'] = parseFloat(additional_product_cart[key]['value']) - 1;
			$('#additional_product_'+key+'_text').text(additional_product_cart[key]['value']);
			$('#additional_product_'+key+'_value').val(additional_product_cart[key]['value']);
		} else {
			additional_product_cart[key] = null;
			$(this).parent().parent().remove();
		}
	});

	//listener for delete button
	$('#additional_product_'+key+'_delete').on('click', function()
	{
		additional_product_cart[key] = null;
		$(this).parent().parent().remove();
	});
});

jQuery.each({!! $production_result->waste !!}, function (waste_key, waste_value)
{
	var header_row = '';
	header_row += '<tr>';
	header_row += '<th colspan="3">Input #'+parseInt(waste_key+1)+'</th>';
	header_row += '</tr>';
	$('#waste_product_cart').append(header_row);

	jQuery.each(waste_value['waste_detail'], function(waste_detail_key, waste_detail_value)
	{
		var name = waste_detail_value['name'];
		var weight = parseFloat(waste_detail_value['value']);
		var new_row = '';
		new_row += '<tr>';
		new_row += '<td>';
		new_row += name;
		new_row += '</td>';
		new_row += '<td>';
		new_row += weight;
		new_row += '</td>';
		new_row += '<td>';

		$('#waste_product_cart').append(new_row);
	});
});
var footer_row = '';
footer_row += '<tr>';
footer_row += '<th colspan="3">Input #'+parseInt({!! $production_result->waste !!}.length+1)+'</th>';
footer_row += '</tr>';
$('#waste_product_cart').append(footer_row);
</script>
@endif

@stop