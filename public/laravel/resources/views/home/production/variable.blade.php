@extends('layouts.home.template')
@section('content')
<div class="header_menu mobile-hide">
    <a href="{{ URL::route('production') }}">WORKING</a>
    <a href="{{ URL::route('production.unstored') }}">UNSTORED</a>
    <a href="{{ URL::route('production.bank') }}">BANK</a>
</div>
<section class="content">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<div class="x_panel">
					<h2>
	                	@if(isset($production_result_variables))
	                	<i class="fa fa-pencil"></i>
	                	@else 
	                	<i class="fa fa-plus"></i>
	                	@endif
	                	<i class="fa fa-dollar"></i>&nbsp;Waste Products
                	</h2>
					<hr>
					<div class="row bs-wizard small" style="border-bottom:0;">
		                <!-- class name =  1. complete 2.active 3.disabled-->

		                <div class="col-xs-3 bs-wizard-step active"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Step 2</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Step 1<br><i>Waste Product</i></div>
		                </div>

		                <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Step 3</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Step 2<br><i>Finish Product</i></div>
		                </div>

		                <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Step 4</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Step 3<br><i>Additional Product</i></div>
		                </div>
		                <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Finish</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Finish<br><i>Summary</i></div>
		                </div>
		            </div>

		    	  	<hr>

		    	  	<div class="row">
					{{-- Waste Product Form --}}
					  	<div class="col-xs-12 col-sm-12 col-md-6">
			        		<h4>Waste Products</h4>
			          		@include('errors.error')
			          		@if(isset($production_result_variables))
			          		{!! Form::open(['route' => ['production.update_waste', $production_result->id], 'method' => 'PUT', 'id' => 'form_waste']) !!}
			          		@else
			          		{!! Form::open(array('route' => ['production.store_waste', $production_result->id], 'id' => 'form_waste')) !!}
			          		@endif
			          		<table class="table table-bordered">
			          		<thead>
					            <tr>
						            <th width="48%">Name</th>
						            <th width="47%">Weight (kg)</th>
						            <td width="5%"></td>
			          			</tr>
				          	</thead>
				          	<tbody id="waste_body">
		          				{{-- Table Content --}}
			          		</tbody>
			          		<tfoot>
			          			<tr>
			          				<td><input type="text" id="waste_name" class="form-control" placeholder="Nama"></td>
			          				<td><input type="number" id="waste_value" class="form-control" min="0" step="0.01" placeholder="Berat"></td>
			          				<td><button type="button" id="waste_add" class="btn btn-primary"><i class="fa fa-plus"></i></button></td>
			          			</tr>
			          		</tfoot>
			        		</table>
			        		<button type="button" id="btn_submit" data-toggle="modal" data-target="#confirmation_modal" class="btn btn-success pull-right"><i class="fa fa-check"></i> Save</button>
			        		{!! Form::close() !!}
				      	</div>
				      	{{-- Waste Product Form --}}

			      		{{-- Production Details --}}
			      		{{-- <div class="col-md-6">
			        		<h4>Production Detail</h4>
			        		<table class="table table-bordered">
			          			<tr>
				            		<th>Product</th>
			            			<td>{{$production_result->production->name}}</td>
			          			</tr>
			          			<tr>
			            			<th>Weight</th>
			            			<td>{{number_format($production_result->weight, 2)}} kg</td>
			          			</tr>
			          			<tr>
			            			<th>Block Weight</th>
			            			<td>{{number_format($production_result->block_weight, 2)}} kg</td>
			          			</tr>
			          			<tr>
			            			<th>Production Date</th>
			            			<td>
			              				{{date("D, d M Y", strtotime($production_result->started_at))}}
			              				&nbsp;<i class="fa fa-long-arrow-right"></i>&nbsp;
			              				@if($production_result->finished_at == null)
			              				<i class="fa fa-question-circle-o"></i>
			              				@else
			              				{{date("D, d M Y", strtotime($production_result->finished_at))}} 
			              				@endif
			            			</td>
			          			</tr>
			          			<tr>
			            			<th>Criteria</th>
			            			<td>{{$production_result->criteria}}</td>
			          			</tr>
	        				</table>
			      		</div> --}}
			      		{{-- Production Details --}}
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@stop

@section('modal')
<div class="modal fade" id="confirmation_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="myModalLabel">Waste Summary</h4>
      		</div>
      		<div class="modal-body">
      			<table class="table">
      				<thead>
      					<th width="50%">Nama</th>
      					<th width="50%">Berat (kg)</th>
      				</thead>
      				<tbody id="waste_list">
      				</tbody>
      			</table>
      		</div>
      		<div class="modal-footer">
      			<button type="button" id="btn_confirm" class="btn btn-success"><i class="fa fa-check"></i> YES</button>
      			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> NO</button>
      		</div>
    	</div>
  	</div>
</div>
@stop

@section('scripts')
<script>
var waste = JSON.parse("{}");

$("#waste_add").on('click', function() {
	var name = $("#waste_name").val().toLowerCase().trim().replace(/ /g,"_");
	var value = $("#waste_value").val();
	if(name && value)
	{
		if(waste[name] == undefined)
		{
			waste[name] = parseFloat(value);
			var new_row = '';
			new_row += '<tr>';
			new_row += '<td>';
			new_row += $("#waste_name").val();
			new_row += '</td>';
			new_row += '<td id="waste_text_'+name+'">';
			new_row += waste[name];
			new_row += '</td>';
			new_row += '<td>';
			new_row += '<input type="hidden" name="waste['+name+']" value="'+waste[name]+'" id="waste_'+name+'">';
			new_row += '<button type="button" id="waste_remove_'+name+'" class="btn btn-danger"><i class="fa fa-remove"></i></button>';
			new_row += '</td>';
			new_row += '</tr>';
			$("tbody#waste_body").append(new_row);

			$("#waste_remove_"+name).on('click', function() {
				waste[name] = null;
				$(this).parent().parent().remove();
			});
		} else {
			waste[name] = waste[name] + parseFloat(value);
			$("#waste_"+name).val(waste[name]);
			$("#waste_text_"+name).text(waste[name]);
		}

		$("#waste_name").val('');
		$("#waste_value").val('');
	}
});

$("#btn_submit").on('click', function() 
{
	$("#waste_list").empty();
	var new_row = '';
	jQuery.each(waste, function(i, val)
	{
		if(val != undefined)
		{
			new_row += '<tr>';
			new_row += '<td>';
			new_row += i;
			new_row += '</td>';
			new_row += '<td>';
			new_row += val+' kg';
			new_row += '</td>';
			new_row += '</tr>';
		}
	});
	$("#waste_list").append(new_row);
});

$("#btn_confirm").on('click', function() {
	$("#form_waste").submit();
});
</script>

@if(isset($production_result_variables))
<script>
$(document).ready(function(){
	jQuery.each({!! $production_result_variables !!}, function( i, val ) {
		var name = val['name'].toLowerCase();
		var value = val['value'];
		waste[name] = parseFloat(value);
		var new_row = '';
		new_row += '<tr>';
		new_row += '<td>';
		new_row += name.replace(/_/g," ");
		new_row += '</td>';
		new_row += '<td id="waste_text_'+name+'">';
		new_row += waste[name];
		new_row += '</td>';
		new_row += '<td>';
		new_row += '<input type="hidden" name="waste['+name+']" value="'+waste[name]+'" id="waste_'+name+'">';
		new_row += '<button type="button" id="waste_remove_'+name+'" class="btn btn-danger"><i class="fa fa-remove"></i></button>';
		new_row += '</td>';
		new_row += '</tr>';
		$("tbody#waste_body").append(new_row);

		$("#waste_remove_"+name).on('click', function() {
			waste[name] = null;
			$(this).parent().parent().remove();
		});
	});
});
</script>
@endif
@stop