@extends('layouts.home.template')
@section('content')
<div class="header_menu">
    <a href="{{ URL::route('production') }}">WORKING</a>
    <a href="{{ URL::route('production.unstored') }}">UNSTORED</a>
    <a href="{{ URL::route('production.bank') }}">BANK</a>
</div>
<section class="content">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<h2>
					<i class="fa fa-cogs"></i>  &nbsp;Production Working
                	<a href="{{ URL::route('production.create') }}" class="btn btn-primary btn-lg pull-right"><i class="fa fa-plus"></i> Add</a>
				</h2>
				<hr>
				@if($today_data->count() == 0)
				<center>
					<h1>NO PRODUCTION WORKING</h1>
					<a href="{{ URL::route('production.create') }}" class="btn btn-primary btn-lg"><i class="fa fa-plus"></i> Add</a>
				</center>
				@else
	            {{-- TODAY PROJECTS --}}
	            <table class="table table-bordered table-responsive" id="production_result_table">
	            <thead>
	            	<tr>
	            		<th width="3%">#</th>
	            		<th width="5%">Status</th>
	            		<th width="17%">Product</th>
	            		<th width="8%">Weight (kg)</th>
	            		<th width="17%">Date</th>
	            		<th>Criteria</th>
	            		<th>Waste</th>
	            		<th>Result</th>
	            		<th>Additional</th>
	            	</tr>
            	</thead>
            	<tbody>
					@foreach($today_data as $key => $result)
					<tr>
						<td>{{$key+1}}</td>
						<td>
							@if($result->status == 0 || $result->status == 1 || $result->status == 2)
							<span class="label label-danger">On Going</span>
							{{-- @elseif($result->status == 1) --}}
							{{-- <span class="label label-primary">Result</span>	 --}}
							{{-- @elseif($result->status == 2) --}}
							{{-- <span class="label label-default">Additional</span> --}}
							@elseif($result->status == 3)
							<span class="label label-warning">Done</span>
							@elseif($result->status == 4)
							<span class="label label-success">Closed</span>
							@endif
						</td>
						<td>
							<a href="{{ URL::route('production.summary', [$result->id]) }}" class="btn btn-default">
								{{$result->production->name}}
							</a>
						</td>
						<td>{{number_format($result->weight, 2)}}</td>
						<td>
							{{date("D, d M Y", strtotime($result->started_at))}} 
							<i class="fa fa-arrow-right"></i>
							{{date("D, d M Y", strtotime($result->finished_at))}}
						</td>
						<td>@if($result->criteria) {{$result->criteria}} @else - @endif</td>
						<td>
							@if($result->waste->count() == 0)
							- 
							@else
							<table class="table">
								@foreach($result->waste as $waste)
								<tr>
									<td>
										<table class="table">
										@foreach($waste->waste_detail as $waste_detail)
										<tr>
											<td width="50%">{{$waste_detail->name}}</td>
											<td width="50%">{{number_format($waste_detail->value, 2)}} kg</td>
										</tr>
										@endforeach
										</table>
									</td>
								@endforeach
								</tr>
							</table>
							@endif
						</td>
						<td>
							@if($result->production_result_detail->count() == 0)
							-
							@else
							<table class="table">
								@foreach($result->production_result_detail as $ready)
								<tr>
									<td width="25%">{{str_replace('_', '~', $ready->size)}}</td>
									<td width="25%">{{number_format(($ready->block * $result->block_weight), 2)}} kg</td>
									<td width="25%">{{$ready->block}} block</td>
									<td width="25%">{{floor($ready->block / $result->mst_carton)}} MC</td>
								</tr>
								@endforeach
							</table>
							@endif
						</td>
						<td>
							@if($result->production_result_addition->count() == 0)
							-
							@else
							<table class="table">
								@foreach($result->production_result_addition as $addition)
								<tr>
									<td width="25%">{{str_replace('_', '~', $addition->size)}}</td>
									<td width="25%">{{number_format(($addition->block * $addition->block_weight), 2)}} kg</td>
									<td width="25%">{{$addition->block}} block</td>
									<td width="25%">{{floor($addition->block / $result->mst_carton)}} MC</td>
								</tr>
								@endforeach
							</table>
							@endif
						</td>
					</tr>
					@endforeach	            	
            	</tbody>
	            </table>

	            {{-- <div class="row" >
		        	<div class="col-md-12" style="padding:0px 25px">
		          		<h2 style="margin-top:0">Current Projects </h2>
		          		<h4>{{date("l, d-m-Y")}}</h4>
		        	</div>
	    	  	</div>
	    	  	<div class="row">
	    	  		<div class="col-md-12">
	    	  			@foreach($today_data as $production_result)
		                <div class="col-md-6">
		                  	@if($production_result->status == '0')
                    			<div class="box box-danger box-solid collapsed-box">
		                  	@elseif($production_result->status == '1')
			                    <div class="box box-primary box-solid collapsed-box">
		                    @elseif($production_result->status == '2')
			                    <div class="box box-info box-solid collapsed-box">
		                    @elseif($production_result->status == '3')
			                    <div class="box box-warning box-solid collapsed-box">
		                    @elseif($production_result->status == '4')
			                    <div class="box box-success box-solid collapsed-box">
		                  	@endif
		                      	<div class="box-header with-border">
			                        <h3 class="box-title">{{$production_result->production->name}}</h3>
			                        <div class="box-tools pull-right">
		                          		<small>{{date("d-m-Y", strtotime($production_result->started_at))}}</small>
		                          		<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
			                        </div>
		                      	</div>
		                      	<div class="box-body">
			                        <table class="table" border="0">
			                          	<tr>
			                              	<td width="65%">{{$production_result->production->name}}</td>
			                              	<td width="15%" align="right">{{date("d-m-Y", strtotime($production_result->started_at))}}</td>
			                              	<td width="5%"><i class="fa fa-long-arrow-right"></i></td>
			                              	<td width="15%" align="left">
			                              	@if($production_result->finished_at == null)
			                              	<i class="fa fa-question-circle-o"></i>
			                              	@else
			                              	{{date("d-m-Y", strtotime($production_result->finished_at))}}
			                              	@endif
		                              	  	</td>
			                          	</tr>
			                          	<tr>
			                              	<td></td>
			                              	<td></td>
			                              	<td></td>
			                              	<td>
			                              	@if($production_result->status == 0)
			                              	<a href="{{ URL::route('production.create_waste', $production_result->id) }}" class="pull-right btn btn-sm btn-primary"><i class="fa fa-trash"></i>&nbsp;Trash</a>
			                              	@elseif($production_result->status == 1)
			                              	<a href="{{ URL::route('production.create_reject', $production_result->id) }}" class="pull-right btn btn-sm btn-primary"><i class="fa fa-ban"></i>&nbsp;Reject</a>
			                              	@elseif($production_result->status == 2)
			                              	<a href="{{ URL::route('production.create_result', $production_result->id) }}" class="pull-right btn btn-sm btn-primary"><i class="fa fa-dropbox"></i>&nbsp;Result</a>
			                              	@elseif($production_result->status == 3)
			                              	<a href="{{ URL::route('production.summary', $production_result->id) }}" class="pull-right btn btn-sm btn-primary"><i class="fa fa-file-text-o"></i>&nbsp;Summary</a>
			                              	@endif
		                              	  	</td>
			                          	</tr>
			                        </table>
			                      </div>
			                    </div>
			                </div>
		                @endforeach
	    	  		</div>
				</div> --}}
				{{-- TODAY PROJECTS --}}
	    	  	@endif
			</div>
		</div>
	</div>
</section>
@stop

@section('modal')
@if($waiting_closure->count() > 0)
<div class="modal fade" id="notification_modal" tabindex="-1" role="dialog" aria-labelledby="modal_label" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h4 class="modal-title" id="modal_label">Purchase Summary</h4>
      		</div>
      		<div class="modal-body">
				<table class="table">
				<thead>
					<tr>
						<th>Product</th>
						<th>Start Date</th>
						<th>Weight</th>
					</tr>
				</thead>
				<tbody>
					@foreach($waiting_closure as $product_waiting)
					<tr>
						<td>
							<a href="{{ URL::route('purchase.summary', $product_waiting->id) }}" class="btn btn-primary btn-md">{{$product_waiting->production->name}}</a>
						</td>
						<td>{{date('d F Y', strtotime($product_waiting->started_at))}}</td>
						<td>{{number_format($product_waiting->weight)}}</td>
					</tr>
					@endforeach
				</tbody>
				</table>
      		</div>
      		<div class="modal-footer">
      			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i>&nbsp;Close</button>
      		</div>
    	</div>
  	</div>
</div>
@endif
@stop

@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script src="//cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.js"></script>
<script type="text/javascript">
$(document).ready( function () {
    $('#production_result_table').DataTable(
    	{
		    responsive: true,
		    scrollX: true
		} 
	);
});
</script>

@if($waiting_closure->count() > 0)
<script>
$(document).ready(function()
{
	$("#notification_modal").modal('show');
});
</script>
@endif
@stop
