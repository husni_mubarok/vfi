@extends('layouts.home.template')
@section('content')
<div class="header_menu">
    <a href="{{ URL::route('production') }}">WORKING</a>
    <a href="{{ URL::route('production.unstored') }}">UNSTORED</a>
    <a href="{{ URL::route('production.bank') }}">BANK</a>
</div>
<section class="content">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<h2>
					<i class="fa fa-cubes"></i>  &nbsp;Production Unstored
				</h2>
				<hr>

				<table class="table table-bordered" id="production_result_table">
				<thead>
					<tr>
						<th width="3%">#</th>
						<th>Product</th>
						<th>Result</th>
						<th>Unstored</th>
						<th>Stored</th>
						<th>Addition</th>
						<th>Unstored</th>
						<th>Stored</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
				@foreach($production_result_list as $key => $result)
					<tr>
						<td>{{$key+1}}</td>
						<td>
							<a href="{{ URL::route('production.summary', [$result['id']]) }}" class="btn btn-default">
							{{$result['product']}}
							</a>
						</td>
						<td>
							@if(isset($result['detail']))
								<table class="table">
								@foreach($result['detail'] as $detail)
									<tr>
										<td>{{str_replace('_', '~', $detail['size'])}}</td>
									</tr>
								@endforeach
								</table>
							@endif
						</td>
						<td>
							@if(isset($result['detail']))
								<table class="table">
								@foreach($result['detail'] as $detail)
									<tr>
										<td>{{$detail['block'] - $detail['stored']}}</td>
									</tr>
								@endforeach
								</table>
							@endif
						</td>
						<td>
							@if(isset($result['detail']))
								<table class="table">
								@foreach($result['detail'] as $detail)
									<tr>
										<td>{{$detail['stored']}}</td>
									</tr>
								@endforeach
								</table>
							@endif
						</td>
						<td>
							@if(isset($result['addition']))
								<table class="table">
								@foreach($result['addition'] as $addition)
									<tr>
										<td>{{str_replace('_', '~', $addition['size'])}}</td>
									</tr>
								@endforeach
								</table>
							@endif
						</td>
						<td>
							@if(isset($result['addition']))
								<table class="table">
								@foreach($result['addition'] as $addition)
									<tr>
										<td>{{$addition['block'] - $addition['stored']}}</td>
									</tr>
								@endforeach
								</table>
							@endif
						</td>
						<td>
							@if(isset($result['addition']))
								<table class="table">
								@foreach($result['addition'] as $addition)
									<tr>
										<td>{{$addition['stored']}}</td>
									</tr>
								@endforeach
								</table>
							@endif
						</td>
						<td>
							<a href="{{ URL::route('production.transaction', [$result['id']]) }}" class="btn btn-primary"><i class="fa fa-exchange"></i></a>
						</td>
					</tr>
				@endforeach
				</tbody>
				</table>		    	  	
			</div>
		</div>
	</div>
</section>

@stop

@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function () {
    $("#production_result_table").DataTable(
    	{
    		//
    	});
    });
</script>
@stop