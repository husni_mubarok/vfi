@extends('layouts.home.template')
@section('content')
<section class="content box box-solid">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<div class="x_panel">
					<h2>
						<i class="fa fa-sign-out"></i>&nbsp;Relokasi Produksi
					</h2>
					<hr>
					<div class="row bs-wizard small" style="border-bottom:0;">
		                <!-- class name =  1. complete 2.active 3.disabled-->
		                
		                <div class="col-xs-3 bs-wizard-step complete">
		                  {{-- <div class="text-center bs-wizard-stepnum">Start</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="{{ URL::route('production') }}" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Start<br><i>Project</i></div>
		                </div>

		                <div class="col-xs-3 bs-wizard-step complete"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Step 3</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="{{ URL::route('production.transaction', [$add_trs_history->from->production_result->id]) }}" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Step 1<br><i>Pilih Ukuran</i></div>
		                </div>

		                <div class="col-xs-3 bs-wizard-step complete"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Step 3</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="{{ URL::route('production.transaction_add', [$add_trs_history->from->id]) }}" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Step 2<br><i>Tentukan Jumlah<br>Dan Tujuan</i></div>
		                </div>

		                <div class="col-xs-3 bs-wizard-step active"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Finish</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Finish<br><i>Report</i></div>
		                </div>
		            </div>
		            <hr>
	            	<div class="row">
			            <h3><i class="fa fa-upload"></i> Sumber</h3>
			            <div class="col-md-5">
			            	<table class="table">
		            		<tr>
		            			<th colspan="3">{{$add_trs_history->from->production_result->production->name}}</th>
		            			<th>{{$add_trs_history->from->size}}</th>
		            		</tr>
		            		<tr>
		            			<td colspan="4">
		            				{{date("D, d M Y", strtotime($add_trs_history->from->production_result->started_at))}} 
		            				<i class="fa fa-arrow-circle-o-right"></i> 
		            				{{date("D, d M Y", strtotime($add_trs_history->from->production_result->finished_at))}}
	            				</td>
		            		</tr>
		            		<tr>
		            			<td>MC</td>
		            			<td>{{floor(($before + $add_trs_history->amount) / $add_trs_history->from->production_result->mst_carton)}}</td>
		            			<td>Unpack</td>
		            			<td>{{($before + $add_trs_history->amount) % $add_trs_history->from->production_result->mst_carton}}</td>
		            		</tr>
			            	</table>
			            </div>
			            <div class="col-md-2">
			            	<center><i class="fa fa-long-arrow-right"></i></center>
			            </div>
			            <div class="col-md-5">
			            	<table class="table">
		            		<tr>
		            			<th colspan="3">{{$add_trs_history->from->production_result->production->name}}</th>
		            			<th>{{$add_trs_history->from->size}}</th>
		            		</tr>
		            		<tr>
		            			<td colspan="4">
		            				{{date("D, d M Y", strtotime($add_trs_history->from->production_result->started_at))}} 
		            				<i class="fa fa-arrow-circle-o-right"></i> 
		            				{{date("D, d M Y", strtotime($add_trs_history->from->production_result->finished_at))}}
	            				</td>
		            		</tr>
		            		<tr>
		            			<td>MC</td>
		            			<td>{{floor($before / $add_trs_history->from->production_result->mst_carton)}}</td>
		            			<td>Unpack</td>
		            			<td>{{$before % $add_trs_history->from->production_result->mst_carton}}</td>
		            		</tr>
			            	</table>
			            </div>
		            </div>
		            <br>
		            <hr>
		            <br>
		            <div class="row">
			            <h3><i class="fa fa-download"></i> Menuju</h3>
			            <div class="col-md-5">
			            	<table class="table">
		            		<tr>
		            			<th colspan="3">{{$add_trs_history->to->production_result->production->name}}</th>
		            			<th>{{$add_trs_history->to->size}}</th>
		            		</tr>
		            		<tr>
		            			<td colspan="4">
		            				{{date("D, d M Y", strtotime($add_trs_history->to->production_result->started_at))}} 
		            				<i class="fa fa-arrow-circle-o-right"></i> 
		            				{{date("D, d M Y", strtotime($add_trs_history->to->production_result->finished_at))}}
	            				</td>
		            		</tr>
		            		<tr>
		            			<td>MC</td>
		            			<td>{{floor(($after - $add_trs_history->amount) / $add_trs_history->to->production_result->mst_carton)}}</td>
		            			<td>Unpack</td>
		            			<td>{{($after - $add_trs_history->amount) % $add_trs_history->from->production_result->mst_carton}}</td>
		            		</tr>
			            	</table>
			            </div>
			            <div class="col-md-2">
			            	<center><i class="fa fa-long-arrow-right"></i></center>
			            </div>
			            <div class="col-md-5">
			            	<table class="table">
		            		<tr>
		            			<th colspan="3">{{$add_trs_history->to->production_result->production->name}}</th>
		            			<th>{{$add_trs_history->to->size}}</th>
		            		</tr>
		            		<tr>
		            			<td colspan="4">
		            				{{date("D, d M Y", strtotime($add_trs_history->to->production_result->started_at))}} 
		            				<i class="fa fa-arrow-circle-o-right"></i> 
		            				{{date("D, d M Y", strtotime($add_trs_history->to->production_result->finished_at))}}
	            				</td>
		            		</tr>
		            		<tr>
		            			<td>MC</td>
		            			<td>{{floor($after / $add_trs_history->to->production_result->mst_carton)}}</td>
		            			<td>Unpack</td>
		            			<td>{{$after % $add_trs_history->from->production_result->mst_carton}}</td>
		            		</tr>
			            	</table>
			            </div>
		            </div>
		            <br>
		            <br>
		            <div class="row">
		            	<a href="{{ URL::route('production.transaction', [$add_trs_history->from->production_result->id]) }}" class="btn btn-lg btn-default pull-right"><i class="fa fa-caret-left"></i> BACK</a>
		            </div>
				</div>
			</div>
		</div>
	</div>
</section>

@stop
