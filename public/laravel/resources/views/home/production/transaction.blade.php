@extends('layouts.home.template')
@section('content')
<section class="content box box-solid">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<div class="x_panel">
					<h2>
						<i class="fa fa-sign-out"></i>&nbsp;Relokasi Produksi
					</h2>
					<hr>
					<div class="row bs-wizard small" style="border-bottom:0;">
		                <!-- class name =  1. complete 2.active 3.disabled-->
		                
		                <div class="col-xs-3 bs-wizard-step complete">
		                  {{-- <div class="text-center bs-wizard-stepnum">Start</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="{{ URL::route('production') }}" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Start<br><i>Project</i></div>
		                </div>

		                <div class="col-xs-3 bs-wizard-step active"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Step 3</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Step 1<br><i>Pilih Ukuran</i></div>
		                </div>

		                <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Step 3</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Step 2<br><i>Tentukan Jumlah<br>Dan Tujuan</i></div>
		                </div>

		                <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Finish</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Finish<br><i>Report</i></div>
		                </div>
		            </div>
		            <hr>
		            <div class="col-md-6">
		            	<h3>{{$result->production->name}}</h3>
		            	@foreach($result->production_result_detail as $detail_key => $detail)
		            	@php
		            	$used_detail = 0;
		            	$added_detail = 0;
		            	foreach($detail->from as $from)
		            	{
		            		$used_detail += $from->amount;
		            	}
		            	foreach($detail->to as $to)
		            	{
		            		$added_detail += $to->amount;
		            	}
		            	@endphp
		            	<h4>Result size {{$detail->size}}</h4>
		            	<table class="table table-bordered">
	            		<thead>
	            			<tr>
	            				<th>{{$detail->size}}</th>
	            				<th>{{($detail->block - $used_detail + $added_detail) % $result->mst_carton}}</th>
	            				<th>
	            					@if((($detail->block - $used_detail + $added_detail) % $result->mst_carton) > 0)
	            					<a href="{{ URL::route('production.transaction_det', [$detail->id]) }}" class="btn btn-sm btn-primary"><i class="fa fa-sign-out"></i></a>
		            				@else
		            				- 
		            				@endif
	            				</th>
	            			</tr>
	            		</thead>
	            		<tbody>
	            			<tr>
	            				<th>Relokasi menuju</th>
	            				<th>Stok dialokasi</th>
	            				<th></th>
	            			</tr>
	            			@if($detail->from->count() > 0)
	            			@foreach($detail->from as $from)
	            			<tr>
	            				<td>
	            					<a href="{{ URL::route('production.summary', [$from->to->production_result->id]) }}">[{{date("D, d M Y", strtotime($from->to->production_result->created_at))}}] {{$from->to->production_result->production->name}} {{$from->to->size}}</a>
            					</td>
	            				<td>-{{$from->amount}}</td>
	            				<td></td>
	            			</tr>
	            			@endforeach
	            			@else
	            			<tr>
	            				<td colspan="3"> - </td>
	            			</tr>
	            			@endif
	            		</tbody>
			            </table>
			            <br>
			            @endforeach
			            <hr>
			            @foreach($result->production_result_addition as $addition_key => $addition)
		            	@php
		            	$used_addition = 0;
		            	$added_addition = 0;
		            	foreach($addition->from as $from)
		            	{
		            		$used_addition += $from->amount;
		            	}
		            	foreach($addition->to as $to)
		            	{
		            		$added_addition += $to->amount;
		            	}
		            	@endphp
		            	<h4>Addition size {{$addition->size}}</h4>
		            	<table class="table table-bordered">
	            		<thead>
	            			<tr>
	            				<th>{{$addition->size}}</th>
	            				<th>{{($addition->block - $used_addition + $added_addition) % $result->mst_carton}}</th>
	            				<th>
	            					@if((($addition->block - $used_addition + $added_addition) % $result->mst_carton) > 0)
	            					<a href="{{ URL::route('production.transaction_add', [$addition->id]) }}" class="btn btn-sm btn-primary"><i class="fa fa-sign-out"></i></a>
		            				@else
		            				- 
		            				@endif
	            				</th>
	            			</tr>
	            		</thead>
	            		<tbody>
	            			<tr>
	            				<th>Relokasi menuju</th>
	            				<th>Stok dialokasi</th>
	            				<th></th>
	            			</tr>
	            			@if($addition->from->count() > 0)
	            			@foreach($addition->from as $from)
	            			<tr>
	            				<td>
	            					<a href="{{ URL::route('production.summary', [$from->to->production_result->id]) }}">[{{date("D, d M Y", strtotime($from->to->production_result->created_at))}}] {{$from->to->production_result->production->name}} {{$from->to->size}}</a>
            					</td>
	            				<td>-{{$from->amount}}</td>
	            				<td></td>
	            			</tr>
	            			@endforeach
	            			@else
	            			<tr>
	            				<td colspan="3"> - </td>
	            			</tr>
	            			@endif
	            		</tbody>
			            </table>
			            <br>
			            @endforeach
		            </div>
				</div>
			</div>
		</div>
	</div>
</section>

@stop
