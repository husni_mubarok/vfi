@extends('layouts.home.template')
@section('content')
<div class="header_menu mobile-hide">
    <a href="{{ URL::route('production') }}">WORKING</a>
    <a href="{{ URL::route('production.unstored') }}">UNSTORED</a>
    <a href="{{ URL::route('production.bank') }}">BANK</a>
</div>
<section class="content">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<div class="x_panel">
					<h2>
	                	<i class="fa fa-plus"></i> 
	                	<i class="fa fa-gears"></i> Production
                	</h2>
					<hr>
		    	  	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		    	  		@include('errors.error')
		    	  		@if(isset($production_result))
		    	  		{{ Form::model($production_result, array('route' => array('production.update', $production_result->id), 'method' => 'put', 'id' => 'form_create')) }}
		    	  		@else
			            {!! Form::open(array('route' => 'production.store', 'id' => 'form_create'))!!}
			            @endif
			            {{ csrf_field() }}
			   			<div class="col-md-6">
			   				{{ Form::label('purchase_id', 'Purchase') }}
			   				@if(isset($production_result))
			   					@if($production_result->purchase)
			   					<p>{{$production_result->purchase->description}}</p>
			   					@else
			   					<p> NO PURCHASE </p>
			   					@endif
			   				@else
			   				{{ Form::select('purchase_id', $purchase_list, old('purchase_id'), ['class' => 'form-control', 'id' => 'purchase_id']) }}
			   				@endif
			   				<br>

			   				{{-- {{ Form::label('fish_id', 'Sea Product') }}
			   				@if(isset($production_result))
			   				<p>{{$production_result->production->fish->name}}</p>
			   				@else
			                {{ Form::select('fish_id', $fishes, old('fish_id'), ['class' => 'form-control', 'placeholder' => 'Select a Sea Product']) }}
			                @endif
			                <br/> --}}

			                {{ Form::label('production_id', 'Production') }}
			                @if(isset($production_result))
			                <p>{{$production_result->production->name}}</p>
			                @else
			                <select name="production_id" id="production_id" class="form-control" disabled>
			                </select>
			                @endif
			                <br/>

							{{ Form::label('weight', 'Weight') }}
							<div class="input-group">
			                	{{ Form::number('weight', old('weight'), array('class' => 'form-control', 'placeholder' => 'Weight*', 'required' => 'true', 'min' => '0', 'step' => '0.01', 'id' => 'input_weight')) }}
			                	<div class="input-group-addon">kg</div>
			                </div><br/>

			                {{ Form::label('block_weight', 'Block Weight') }}
							<div class="input-group">
			                	{{ Form::number('block_weight', old('block_weight'), array('class' => 'form-control', 'placeholder' => 'Block Weight*', 'required' => 'true', 'min' => '0', 'step' => '0.01', 'id' => 'input_block_weight')) }}
			                	<div class="input-group-addon">kg</div>
			                </div><br/>

			                {{ Form::label('mst_carton', 'Master Carton') }}
							<div class="input-group">
	                        	{{ Form::number('mst_carton', old('mst_carton'), array('class' => 'form-control', 'placeholder' => 'Master Carton*', 'required' => 'true', 'min' => '1', 'step' => '1', 'id' => 'input_master_carton')) }}
		                        <div class="input-group-addon">
		                        	<span>packs</span>
		                        </div>
	                        </div><br/>

			                {{ Form::label('started_at', 'Start Date') }}
			                {{ Form::text('started_at', old('started_at'), array('class' => 'form-control', 'placeholder' => 'Date & Time*', 'required' => 'true', 'id' => 'started_at')) }}<br/>

			                {{ Form::label('criteria', 'Criteria') }}
			                {{ Form::text('criteria', old('criteria'), array('class' => 'form-control default_input_keyboard', 'placeholder' => 'Criteria', 'id' => 'input_criteria')) }}<br/>

			                <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#confirm_box" id="btn_submit"><i class="fa fa-check"></i> Save</button>
			   			</div>
			   			{!! Form::close() !!}
		    	  	</div>
				</div>
			</div>
		</div>
	</div>
</section>
@stop

@section('modal')
<div class="modal fade" id="confirm_box" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="myModalLabel">Production Summary</h4>
      		</div>
      		<div class="modal-body">
      			<table class="table">
      				<tr>
      					<th width="25%">Purchase</th>
      					<td width="75%" id="purchase"></td>
      				</tr>
      				<tr>
      					<th>Product</th>
      					<td id="product"></td>
      				</tr>
      				<tr>
      					<th>Weight</th>
      					<td id="weight"></td>
      				</tr>
      				<tr>
      					<th>Block Weight</th>
      					<td id="block_weight"></td>
      				</tr>
      				<tr>
      					<th>Master Carton</th>
      					<td id="master_carton"></td>
      				</tr>
      				<tr>
      					<th>Start Date</th>
      					<td id="start_date"></td>
      				</tr>
      				<tr>
      					<th>Criteria</th>
      					<td id="criteria"></td>
      				</tr>
      			</table>
      		</div>
      		<div class="modal-footer">
      			<button type="button" id="yes" class="btn btn-success"><i class="fa fa-check"></i> YES</button>
      			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i>&nbsp;NO</button>
      		</div>
    	</div>
  	</div>
</div>
@stop

@section('scripts')
<script>
$(document).ready(function () {
	$('#started_at').datetimepicker({
		sideBySide: true,
		format: 'YYYY-MM-DD',
	});
});

$("#btn_submit").on('click', function() {
	$("#purchase").text($("#purchase_id option:selected").text());
	$("#product").text($("#production_id option:selected").text());
	$("#weight").text($("#input_weight").val());
	$("#block_weight").text($("#input_block_weight").val());
	$("#master_carton").text($("#input_master_carton").val());
	$("#start_date").text($("#started_at").val());
	$("#criteria").text($("#input_criteria").val());
});

$("#yes").on('click', function() 
{
	$("#form_create").submit();
});
</script>
<script>
$("#fish_id").on('change', function()
{
    $.ajax({
        url : '{{ URL::route('get.production') }}',
        data : {'fish_id':$("#fish_id").val()},
        type : 'GET',
        headers : {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
        success : function(data, textStatus, jqXHR){
        	$('#production_id').empty();
        	if(data.length == 0)
        	{
        		$("#production_id").append($('<option>', { 
			        value: null,
			        text : 'Tidak ada produk reguler terdaftar' 
			    }));
    			$('#production_id').attr('disabled', true);		
        	} else {
	            jQuery.each(data, function(i, val)
	            {
	            	$('#production_id').append($('<option>', { 
				        value: val.id,
				        text : val.name 
				    }));
	            });
	            $("#production_id").attr('disabled', false);
        	}
        },
        error : function()
        {
        	$('#production_id').empty();
        	$('#production_id').attr('disabled', true);
        },
    })
});

$("#purchase_id").on('change', function() {
	$.ajax({
        url : '{{ URL::route('get.product_by_purchase') }}',
        data : {'purchase_id':$("#purchase_id").val()},
        type : 'GET',
        headers : {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
        success : function(data, textStatus, jqXHR){
        	$("#input_weight").val('');
            $('#production_id').empty();
        	$("#input_weight").val(data['weight']);
            jQuery.each(data['productions'], function(i, val)
            {
            	$('#production_id').append($('<option>', { 
			        value: val.id,
			        text : val.name 
			    }));
            });
            $("#production_id").attr('disabled', false);
        },
        error : function()
        {
        	$('#production_id').empty();
        	$('#production_id').append($('<option>', { 
		        value: null,
		        text : 'No Product available' 
		    }));
        	$('#production_id').attr('disabled', true);
        },
    })
});
</script>
@stop