    @extends('layouts.home.template')
    @section('content')
    <style type="text/css">
      #myModal .modal-dialog
      {
        width: 90%;
      }

      #myModalEdit .modal-dialog
      {
        width: 50%;
      }
    </style>
    @actionStart('export', 'create|update')
    	<section class="content box box-solid">
    		<div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
           <div class="col-md-1"></div>
           <div class="col-md-5">
             <div class="x_panel">
              <h2>
               @if(isset($cart))
               <i class="fa fa-pencil"></i>
               @else
               <i class="fa fa-plus"></i> 
               @endif
               <i class="fa fa-share-square-o"></i> Cart
             </h2>
            <MARQUEE DIRECTION=LEFT><p style="color: red;">Kegunaan sama dalam satu tanggal disatuin, beda tanggal beda input data.</p></MARQUEE>
             <hr>
             <div class="x_content">
               @include('errors.error')
               @if(isset($cart))
               {!! Form::model($cart, array('route' => ['cart.update', $cart->id], 'method' => 'PUT'))!!}
               @else
               {!! Form::open(array('route' => 'cart.store'))!!}
               @endif
               {{ csrf_field() }}
             </div>
             
             {{ Form::label('doc_code', 'Doc Code (auto)') }}
             {{ Form::text('doc_code', old('doc_code'), array('class' => 'form-control', 'placeholder' => 'Doc Code*', 'required' => 'true', 'disabled')) }}
             
             {{ Form::label('date', 'Date') }}
             {{ Form::text('date', old('date'), array('class' => 'form-control', 'placeholder' => 'Date & Time*', 'required' => 'true', 'id' => 'dateprod')) }}
             
             <hr/>

             <a href="#myModal" class="btn btn-primary" id="lookup" onclick="refreshdttable();"><i class="fa fa-folder-open"></i>  Lookup</a><br/>

             {{ Form::hidden('source_id', old('source_id'), array('class' => 'form-control', 'placeholder' => 'source_id *', 'required' => 'true', 'id' => 'source_id')) }}
             {{ Form::hidden('production_id', old('production_id'), array('class' => 'form-control', 'placeholder' => 'production_id *', 'required' => 'true', 'id' => 'production_id')) }}
             {{ Form::hidden('unitedit', old('unitedit'), array('class' => 'form-control', 'placeholder' => 'unitedit *', 'required' => 'true', 'id' => 'unitedit')) }}

             {{ Form::label('name', 'Name') }}
             <div class="well well-sm" id="lbl_name">&nbsp;</div>
             {{ Form::hidden('name', old('name'), array('class' => 'form-control', 'placeholder' => 'Name*', 'required' => 'true', 'id' => 'name')) }}
             {{ Form::label('criteria', 'Criteria') }}
             <div class="well well-sm" id="lbl_criteria">&nbsp;</div>
             {{ Form::hidden('criteria', old('criteria'), array('class' => 'form-control', 'placeholder' => 'Criteria*', 'required' => 'true', 'id' => 'criteria')) }}

             {{ Form::label('size', 'Size') }}
             <div class="well well-sm" id="lbl_size">&nbsp;</div>
             {{ Form::hidden('size', old('size'), array('class' => 'form-control', 'placeholder' => 'Size*', 'required' => 'true', 'id' => 'size')) }}

             {{ Form::label('block', 'Block') }}
             <div class="well well-sm" id="lbl_block">&nbsp;</div>
             {{ Form::hidden('block', old('block'), array('class' => 'form-control', 'placeholder'=> 'Block*', 'id'=>'block')) }}      

             {{ Form::label('weight', 'Weight') }}
             <div class="well well-sm" id="lbl_weight">&nbsp;</div>
             {{ Form::hidden('weight', old('weight'), array('class' => 'form-control', 'placeholder' => 'Weight*', 'required' => 'true', 'id' => 'weight')) }}

             {{ Form::label('block_weight', 'Block Weight') }}
             <div class="well well-sm" id="lbl_block_weight">&nbsp;</div>
             {{ Form::hidden('block_weight', old('block_weight'), array('class' => 'form-control', 'placeholder' => 'Block Weight*', 'required' => 'true', 'id' => 'block_weight')) }}
             
             {{ Form::label('quantity', 'Qty') }}
             {{ Form::number('quantity', old('quantity'), array('class' => 'form-control', 'placeholder' => 'Qty', 'id' => 'quantity', 'onchange' => 'stockval()')) }}

             {{ Form::hidden ('quantity1', old('quantity1'), array('class' => 'form-control', 'placeholder' => 'Qty', 'id' => 'quantity1')) }}
             <br/>
             <button type="submit" id="btnadd" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-plus"></i> Add</button>
             <br/>
             <br/>
           </div>
           {!! Form::close() !!}
         </div>
         
         <div class="col-md-12 col-sm-12 col-xs-12">          
          <table class="table table-bordered" id="Cart_table">
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Criteria</th>
                <th>Size</th>
                <th>Weight</th>
                <th>Block Weight</th>
                <th>Block</th>
                <th>Qty</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($cartdets as $key => $cartdet)
              <tr>
                <td>{{$key+1}}</td>
                <td>{{$cartdet->name}}</td>
                <td>{{$cartdet->criteria}}</td>
                <td>{{$cartdet->size}}</td>
                <td>{{$cartdet->block_weight}}</td>
                <td>{{$cartdet->weight}}</td>
                <td>{{$cartdet->block}}</td>
                <td>{{$cartdet->quantity}}</td>
                <td align="center">
                  <div class="col-md-2 nopadding">                            
                    <a href="javascript:void(0)" onclick="editValue(this, {{ $cartdet->id }}); showeditmodal();" class="btn btn-default btn-sm">
                      <i class="fa fa-edit"></i></a>
                    </div>
                    <div class="col-md-2 nopadding">
                      {!! Form::open(array('route' => ['cart.deletedet', $cartdet->id], 'method' => 'delete', 'class'=>'delete'))!!}
                      {{ csrf_field() }}                              
                      <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-trash"></i></a></button>
                      {!! Form::close() !!}
                    </div>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>  
            @if(isset($cart))
            {!! Form::model($cart, array('route' => ['cart.storeconfirm', $cart->id], 'method' => 'PUT'))!!}
            <button type="submit" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Concirm</button> 
            @else
            {!! Form::open(array('route' => 'cart.store'))!!}
            <button type="submit" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();" disabled="disabled"><i class="fa fa-check"></i> Concirm</button> 
            @endif
            {{ csrf_field() }} 
            {!! Form::close() !!} 
            <a href="{{ URL::route('cart.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
            <br/><br/>
            <p class="pull-right"><i>*Data yang diconfirm didtak bisa diedit/dihapus</i></p>
            </td>
          </div> 
        </div>   
        
      </div>

    </div>
  </section>
@actionEnd

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Production</h4>
      </div>
      <div class="modal-body">
        <table id="tempstorage" class="table table-striped">
          <thead>
            <tr>
              <th>S_id</th>
              <th>P_id</th>
              <th>Name</th>
              <th>Criteria</th>
              <th>Block Weight</th>
              <th>Size</th>
              <th>Block</th>
              <th>Start At</th>
              <th>Finish At</th>
              <th>Unit</th>
              <th>Weight</th>
              <th>Condition</th>
              <th>Prod Qty</th>
              <th>Cart Qty</th>
              <th>Remain Qty</th>
              <th>Action</th>
            </tr>
          </thead>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Back</button>
        <button onClick="refreshdttable()" type="button" class="btn btn-success btn-flat"> <i class="fa fa-refresh"></i> Refresh</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Edit Item </h4>
      </div>
      <div class="modal-body">

        @if(isset($cart))
        {!! Form::model($cart, array('route' => ['cart.updatedetail', $cart->id], 'method' => 'PUT'))!!}
        @else
        
        @endif
        {{ csrf_field() }}
        
        {{ Form::hidden('iddetail', old('iddetail'), array('class' => 'form-control', 'placeholder' => 'iddetail *', 'required' => 'true', 'id' => 'iddetail')) }}
        
        {{ Form::label('nameedit', 'Name') }}
        {{ Form::text('nameedit', old('nameedit'), array('class' => 'form-control', 'placeholder' => 'Name*', 'required' => 'true', 'id' => 'nameedit')) }}
        
        {{ Form::label('criteriaedit', 'Criteria') }}
        {{ Form::text('criteriaedit', old('criteriaedit'), array('class' => 'form-control', 'placeholder' => 'Criteria*', 'required' => 'true', 'id' => 'criteriaedit')) }}

        {{ Form::label('sizeedit', 'Size') }}
        {{ Form::text('sizeedit', old('sizeedit'), array('class' => 'form-control', 'placeholder' => 'Size*', 'required' => 'true', 'id' => 'sizeedit')) }}

        {{ Form::label('blockedit', 'Block') }}
        {{ Form::text('blockedit', old('blockedit'), array('class' => 'form-control', 'placeholder' => 'Block*', 'required' => 'true', 'id' => 'blockedit')) }}

        {{ Form::label('weightedit', 'Weight') }}
        {{ Form::text('weightedit', old('weightedit'), array('class' => 'form-control', 'placeholder' => 'Weight*', 'required' => 'true', 'id' => 'weightedit')) }}

        {{ Form::label('blockweightedit', 'Block Weight') }}
        {{ Form::text('blockweightedit', old('blockweightedit'), array('class' => 'form-control', 'placeholder' => 'Block Weight*', 'required' => 'true', 'id' => 'blockweightedit')) }}
        
        {{ Form::label('quantityedit', 'Quantity') }}
        {{ Form::text('quantityedit', old('quantityedit'), array('class' => 'form-control', 'placeholder' => 'Block*', 'required' => 'true', 'id' => 'quantityedit')) }}
        
        <br/>
        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Back</button>
        <button type="submit" id="btnadd" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-plus"></i> Confirm</button>

        <br/>
      </div>
      {!! Form::close() !!} 
    </div>

  </div>      
</div>
</div>
</div>


@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script>
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>

<script>
  $(document).ready(function () {
   $('#dateprod').datetimepicker({
    sideBySide: true,
    format: 'YYYY-MM-DD HH:mm',
  });
 });

  function addValue(str, id){
    var prd = id;
    var store_id = $(str).closest('tr').find('td:eq(0)').text();
    var source_id = $(str).closest('tr').find('td:eq(1)').text();
    var production_id = $(str).closest('tr').find('td:eq(0)').text();
    var name = $(str).closest('tr').find('td:eq(2)').text();
    var criteria = $(str).closest('tr').find('td:eq(3)').text(); 
    var block_weight = $(str).closest('tr').find('td:eq(4)').text(); 
    var size = $(str).closest('tr').find('td:eq(5)').text(); 
    var block = $(str).closest('tr').find('td:eq(6)').text(); 
    var unitedit = $(str).closest('tr').find('td:eq(9)').text(); 
    var weight = $(str).closest('tr').find('td:eq(10)').text(); 
    var remainqty = $(str).closest('tr').find('td:eq(14)').text();

    $("#production_id").val(production_id);
    $("#source_id").val(source_id);
    $("#quantity").val(remainqty);
    $("#name").val(name);
    $("#criteria").val(criteria);
    $("#size").val(size);
    $("#block").val(block);
    $("#block_weight").val(block_weight);
    $("#quantity1").val(remainqty);
    $("#unitedit").val(unitedit);
    $("#weight").val(weight);
    $("#iddetail").val('');
    $("#lbl_name").text(name);
    $("#lbl_weight").text(weight);
    $("#lbl_criteria").text(criteria);
    $("#lbl_size").text(size);
    $("#lbl_block").text(block);
    $("#lbl_block_weight").text(block_weight);
    
    console.log(id);
    $('#myModal').modal("hide");
  }

  function editValue(str, id){
    var prd = id;

    var nameedit = $(str).closest('tr').find('td:eq(1)').text();
    var criteriaedit = $(str).closest('tr').find('td:eq(2)').text(); 
    var sizeedit = $(str).closest('tr').find('td:eq(3)').text(); 
    var weight = $(str).closest('tr').find('td:eq(4)').text(); 
    var blockweightedit = $(str).closest('tr').find('td:eq(5)').text();
    var blockedit = $(str).closest('tr').find('td:eq(6)').text(); 
    var quantityedit = $(str).closest('tr').find('td:eq(7)').text();
    
    $("#iddetail").val(prd)
    $("#quantityedit").val(quantityedit);
    $("#nameedit").val(nameedit);
    $("#criteriaedit").val(criteriaedit);
    $("#sizeedit").val(sizeedit);
    $("#blockedit").val(blockedit);
    $("#weightedit").val(weight);
    $("#blockweightedit").val(blockweightedit);
    console.log(id);
  }

  function showeditmodal(){
    $('#myModalEdit').modal();
  }

  $(document).ready(function(){
    $("#productionResult").DataTable({
      "pageLength": 3
    });

    $("#lookup").click(function(){
      console.log('testing');
      $('#myModal').modal();
    });

    $('#datetime').datetimepicker({
      sideBySide: true,
      format: 'YYYY-MM-DD HH:mm',
    });

  });

  function stockval() {   
    
    var quantity = $('#quantity').val();
    var quantity1 = $('#quantity1').val();
    if ( parseInt(quantity) > parseInt(quantity1) ) {
      $('#quantity').val(quantity1);
      alert("over stock"); 
    }
  };

  function refreshdttable() {
    table.ajax.reload();
  }
</script>
<script>
  $(".delete").on("submit", function(){
    return confirm("Do you want to delete this item?");
  });
</script>

<script>
var table;
$(document).ready(function() {
  table = $('#tempstorage').DataTable( {

    //var table = $("#tempstorage").DataTable({
      processing: true,
      serverSide: true,
      "pageLength": 25,
      "scrollY": "400px",
      ajax: "{{ url('editor/lookupcart') }}",
      columns: [
      { data: 'production_id', name: 'production_id' },
      { data: 'resultdetailid', name: 'resultdetailid' },
      { data: 'name', name: 'name' },
      { data: 'criteria', name: 'criteria' },
      { data: 'block_weight', name: 'block_weight' },
      { data: 'size', name: 'size' },
      { data: 'block', name: 'block' },
      { data: 'started_at', name: 'started_at' },
      { data: 'finished_at', name: 'finished_at' },
      { data: 'unit', name: 'unit' },
      { data: 'weight', name: 'weight' },
      { data: 'sourcetype', name: 'sourcetype' },
      { data: 'prodqty', name: 'prodqty' },
      { data: 'cartqty', name: 'cartqty' },
      { data: 'remainqty', name: 'remainqty' },
      { data: 'action', 'searchable': false, 'orderable':false }
      ]
    });
  });
</script>
@stop