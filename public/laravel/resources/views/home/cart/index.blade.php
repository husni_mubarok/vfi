@extends('layouts.home.template')
@section('content')
@actionStart('export', 'read')
  <section class="content box box-solid">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="col-md-1"></div>
          <div class="col-md-10">
              <div class="x_panel">
                    <h2>
                      <i class="fa fa-share-square-o"></i> Export List    
                      @actionStart('export', 'create')              
                      <a href="{{ URL::route('cart.create') }}" class="btn btn-primary btn-lg pull-right"><i class="fa fa-plus"></i> Add</a>
                      @actionEnd
                    </h2>
                    <hr>
                    {{-- <form method="POST" id="search-form" class="form-inline" role="form">
                        <div class="form-group">
                            <input type="text" class="form-control" name="datefrom" id="datefrom" placeholder="Date From">
                        </div>
                        <div class="form-group">
                            <label> &nbsp-&nbsp</label>
                            <input type="text" class="form-control" name="dateto" id="dateto" placeholder="Date To">
                        </div>
                        <button type="submit" class="btn btn-primary">Search</button>
                    </form> --}}
                    <div class="x_content">
                        <table id="cart" class="table table-striped">
                              <thead>
                                      <tr>
                                      <th>Doc No</th>
                                      <th>Date</th>
                                      <th>Process</th>
                                      <th>Status</th>
                                      <th>Delete</th>
                                      <th>Action</th>
                                      <th>Process</th>
                              </tr>
                              </thead>
                      </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
@actionEnd
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function () {
    $("#cart_table").DataTable(
      {
        "scrollX": true
      });
  });
</script>
<script>
  $(document).ready(function () {
   $('#datefrom').datetimepicker({
    sideBySide: true,
    format: 'YYYY-MM-DD HH:mm',
  });
  $('#dateto').datetimepicker({
    sideBySide: true,
    format: 'YYYY-MM-DD HH:mm',
  });
 });
 </script>
<script>
    $(".delete").on("submit", function(){
        return confirm("Do you want to delete this item?");
    });
   
</script>
<script>
  $(function() {
    var table = $("#cart").DataTable({
      processing: true,
      serverSide: true,
      "pageLength": 25,
      "scrollY": "400px",
      ajax: {
            url: '{{ url('editor/data') }}',
            data: function (d) {
                d.datefrom = $('input[name=datefrom]').val();
            }
        },
      columns: [
      // {
      //     "className":      'details-control',
      //     "orderable":      false,
      //     "searchable":      false,
      //     "data":           null,
      //     "defaultContent": ''
      // },
      { data: 'doc_code', name: 'doc_code' },
      { data: 'date', name: 'date' },
      { data: 'action', 'searchable': false, 'orderable':false },
      { data: 'dnstatus', 'searchable': false, 'orderable':false },
      { data: 'actiondl', 'searchable': false, 'orderable':false },
      { data: 'actioned', 'searchable': false, 'orderable':false },
      { data: 'process', 'searchable': false, 'orderable':false }
      ]
    });
  });

    $('#search-form').on('submit', function(e) {
        oTable.draw();
        e.preventDefault();
    });
</script>
@stop