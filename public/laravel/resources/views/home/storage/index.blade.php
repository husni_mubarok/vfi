@extends('layouts.home.template')
@section('content')
@actionStart('storage', 'read')
  <section class="content box box-solid">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="col-md-1"></div>
          <div class="col-md-10">
              <div class="x_panel">
                    <h2>
                      <i class="fa fa-dropbox"></i> Storage List
                    </h2>
                    <hr>
                  <div class="x_content">

                    <a href="{{ URL::route('storage.create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add</a>
<br><br>

              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab"><b>Storage History</b></a></li>
                  <li><a href="#tab_2" data-toggle="tab" id="secondTab"><b>Storage</b></a></li>
                </ul>
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
                    <br>
                    <table id="storageTable" class="table table-striped dataTable">
                <thead>
                    <tr>
                      <th>#</th>
                      <th>Name</th>
                      <th>Type</th>
                      <th>Size</th>
                      <th>Stock IN</th>
                      <th>Export</th>
                      <th>Value Added</th>
                      <th>R&D</th>
                      <th>Stock Current</th>
                  </tr>
                </thead>

              </table>

                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_2">
                    <br>
                    <table id="storageDetailTable" class="table table-striped dataTable" width="100%">
                <thead>
                    <tr>
                      <th>#</th>
                      <th>Id</th>
                      <th>Name</th>
                      <th>Size Block</th>
                      <th>Block</th>
                      <th>Condition</th>
                      <th>Stored DateTime</th>
                      <th>Quantity</th>
                  </tr>
                </thead>

            </table>
          </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div>
                  </div>
              </div>
            </div>
        </div>
    </div>
  </section>
@actionEnd
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script>
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
function format(d){
    return 'Date Start Production : '+d.dateProduksiStart+'<br>'+
        'Date Finish Production : '+d.dateProduksiFinish+'<br>';
}

  $(document).ready(function () {
    var dt = $('#storageTable').DataTable( {
       "processing": true,
       "serverSide": true,
       "ajax": "{{ url('storage/dataindex/1') }}",
       "columns": [
           {
               "class":          "details-control",
               "orderable":      false,
               "data":           null,
               "defaultContent": ""
           },
           { "data": "nameProduct" },
           { "data": "type" },
           { "data": "size" },
           { "data": "stokin" },
           { "data": "stokout_export" },
           { "data": "stokout_valueadded" },
           { "data": "stokout_sashimi" },
           { "data": "stokcur" }
       ],
       "order": [[1, 'asc']],
       "pageLength" : 10,
       "scrollY" : "300px"
   } );

   // Array to track the ids of the details displayed rows
   var detailRows = [];

   $('#storageTable tbody').on( 'click', 'tr td.details-control', function () {
       var tr = $(this).closest('tr');
       var row = dt.row( tr );
       var idx = $.inArray( tr.attr('id'), detailRows);

       if ( row.child.isShown() ) {
           tr.removeClass( 'details' );
           row.child.hide();

           // Remove from the 'open' array
           detailRows.splice( idx, 1 );
       }
       else {
           tr.addClass( 'details' );
           row.child( format( row.data() ) ).show();

           // Add to the 'open' array
           if ( idx === -1 ) {
               detailRows.push( tr.attr('id') );
           }
       }
   } );

   // On each draw, loop over the `detailRows` array and show any child rows
   dt.on('draw', function(){
     $.each( detailRows, function ( i, id ) {
         $('#'+id+' td.details-control').trigger( 'click' );
     });
  });


  var dtc = $('#storageDetailTable').DataTable( {
     "processing": true,
     "serverSide": true,
     "ajax": "{{ url('storage/dataindex/2') }}",
     "columns": [
         {
             "class":          "details-control",
             "orderable":      false,
             "data":           null,
             "defaultContent": ""
         },
         { "data": "id" },
         { "data": "name" },
         { "data": "sizeblock" },
         { "data": "block" },
         { "data": "condition" },
         { "data": "datetime" },
         { "data": "quantity" }
     ],
     "order": [[1, 'asc']],
     "pageLength" : 10,
     "scrollY" : "300px"
  } );

  // Array to track the ids of the details displayed rows
  var detailRowsc = [];

  $('#storageDetailTable tbody').on( 'click', 'tr td.details-control', function () {
      var tr = $(this).closest('tr');
      var row = dtc.row( tr );
      var idx = $.inArray( tr.attr('id'), detailRowsc);

      if ( row.child.isShown() ) {
          tr.removeClass( 'details' );
          row.child.hide();

          // Remove from the 'open' array
          detailRowsc.splice( idx, 1 );
      }
      else {
          tr.addClass( 'details' );
          row.child( format( row.data() ) ).show();

          // Add to the 'open' array
          if ( idx === -1 ) {
              detailRowsc.push( tr.attr('id') );
          }
      }
  } );

  // On each draw, loop over the `detailRows` array and show any child rows
  dtc.on('draw', function(){
    $.each( detailRowsc, function ( i, id ) {
        $('#'+id+' td.details-control').trigger( 'click' );
    });
 });



    // $("#storageTable").DataTable(
    //  {
    //     "pageLength" : 10,
    //     "scrollY" : "400px",
    //     "order": [[ 1, "asc" ]]
    //  });
    // $("#storageDetailTable").DataTable(
    //  {
    //     "pageLength" : 10,
    //     "scrollY" : "400px",
    //     "order": [[ 5, "desc" ]]
    //  });
    });
</script>
@stop
