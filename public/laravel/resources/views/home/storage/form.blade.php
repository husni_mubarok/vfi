@extends('layouts.home.template')
@section('content')
@actionStart('storage', 'create|update')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-5">
			        <div class="x_panel">
		                <h2>
		                	@if(isset($storage))
		                	<i class="fa fa-pencil"></i>
		                	@else
		                	<i class="fa fa-plus"></i>
		                	@endif
		                	<i class="fa fa-truck"></i> Storage
	                	</h2>
		                <hr>
			            <div class="x_content">
			                @include('errors.error')

	                    @if(isset($storage))
			                   {!! Form::model($storage, array('route' => ['storage.update', $storage->id], 'method' => 'PUT'))!!}
		                  @else
		                    {!! Form::open(array('route' => 'storage.store'))!!}
		                  @endif

                      {{ csrf_field() }}

                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                          <input type="hidden" name="valArySend" id="valArySend">

		                    	{{ Form::label('production', 'Production*') }}
		                      {{ Form::text('production', old('production'), array('class' => 'form-control', 'placeholder' => 'Production', 'required' => 'true')) }}
                            <br/>
                            <a href="#" class="btn btn-primary" id="lookup"><span class="fa fa-search"></span>&nbsp;Lookup</a>
                            <br/>

                            <br/>
                            {{ Form::label('datetime', 'Date & Time*') }}
          							    {{ Form::text('datetime', old('datetime'), array('class' => 'form-control', 'placeholder' => 'Date & Time', 'required' => 'true', 'id' => 'datetime')) }}<br/>

                            {{ Form::label('unit', 'Unit') }}
		                        {{ Form::select('unit', ['Block' => 'Block', 'Tray' => 'Tray'], old('unit'), ['placeholder' => 'Select a Type', 'class' => 'form-control', 'required' => 'true']) }}<br/>

                            {{ Form::label('weight', 'Wieght*') }}
          							    {{ Form::text('weight', old('weight'), array('class' => 'form-control', 'placeholder' => 'Weight', 'required' => 'true', 'id' => 'weight')) }}<br/>

                            <button type="submit" class="btn btn-success pull-right" ><i class="fa fa-check"></i> Save</button>
	                    	</div>
	                        {!! Form::close() !!}
			            </div>
			        </div>
		        </div>
            <div class="col-md-6">
              <h3>Item To Store</h3>
              <table class="table">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Size</th>
                  <th>Qty</th>
                </tr>
                </thead>
                <tbody id="cart">

                </tbody>
              </table>
            </div>
		    </div>
		</div>
	</section>
</section>
@actionEnd
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Production</h4>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-responsive" id="productionResult">
          <thead>
          <tr>
            <th>No</th>
            <th>Name</th>
            <th>Quantity Detail</th>
            <th>Quantity</th>
            <th>Wieght</th>
            <th>Date</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody id="elmTableFirst">

        </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Production</h4>
      </div>
      <div class="modal-body">
        <div>
          <table class="table table-striped table-responsive">
            <thead>
            <tr>
              <th>No</th>
              <th>Size</th>
              <th>Qty Production</th>
              <th>Qty Storage</th>
              <th>Qty Remaining</th>
              <th>Store</th>
            </tr>
          </thead>
          <tbody id="elmTable">
          </tbody>
        </table>
        <a href="javascript:void(0)" class="btn btn-success" id="storeSubmit">Store</a>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Back</button>
      </div>
    </div>
  </div>
</div>
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script>
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>

<script>
function validval(idelm){
  console.log(idelm);
  var elmVal = $("#blockval"+idelm).val();
  var curVal = $("#storeval"+idelm).val();
  console.log(elmVal);
  console.log(curVal);
  if(parseInt(curVal) > parseInt(elmVal)){
    alert('stock tidak tersedia');
    $("#storeval"+idelm).val("");
  }
  console.log(elmVal);
  console.log(curVal);
}

function addValue(str, id){
  var prd = id;
  var qty = $(str).closest('tr').find('td:eq(3)').text();
  var weight = $(str).closest('tr').find('td:eq(4)').text();

  $("#production").val(id);
  $("#qty").val(qty);
  $("#weight").val(weight);
  $('#myModal').modal("hide");
  $('#myModal2').modal('show');

  var urlV = '{{ URL::to("/getsizeblock") }}';
  var typeV = 'GET';

  $.ajax({
      url : urlV,
      data : 'id_pick='+prd,
      type : typeV,
      headers : {
        'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
      },
      error : function(jqXHR, textStatus, errorThrown){
          console.log(jqXHR.responseText);
      },
      success : function(data, textStatus, jqXHR){
          //console.log(jqXHR.responseText);
          console.log(data);
          var returnedData = JSON.parse(data);
          var production_id = returnedData[0][0]['production_id'];
          var prd = returnedData[0][0]['production_result_detail'];
          var pra = returnedData[0][0]['production_result_addition'];
          var temps = returnedData[1];
          console.log(temps);
          var elm = "";
          var counter_id=0;
          var numberElm = 0;
          for (var i = 0; i < prd.length; i++) {

            var no = i + 1;
            elm += "<tr>";
            elm += '<td>'+no+'<input type="hidden" name="srctype'+i+'" id="srctype'+i+'" value="1"><input type="hidden" name="pidval'+i+'" id="pidval'+i+'" value="'+production_id+'"><input type="hidden" name="idval'+i+'" id="idval'+i+'" value="'+prd[i]['id']+'"></td>';
            elm += "<td id='production_size_"+i+"' >"+prd[i]['size']+"</td>";
            elm += '<td>'+prd[i]['block']+"</td>";
            var testv = 0;
            var stok = 0;

            for (var z = 0; z < temps.length; z++) {
              if(temps[z]['source_type'] == 1){
                // console.log(temps[z]['source_id']);
                // console.log("++");
                // console.log(prd[i]['id']);
                if(temps[z]['source_id'] == prd[i]['id']){
                  console.log("masuk");
                  testv = testv + temps[z]['quantity'];
                  // console.log(prd[i]['block']);
                  // console.log(temps[z]['quantity']);
                }
              }
            }
            stok = prd[i]['block'] - testv;
            console.log("---");
            console.log(testv);
            console.log("---");
            elm += '<td>'+testv+'<input type="hidden" name="blockval'+i+'" id="blockval'+i+'" value="'+stok+'"></td>';
            elm += '<td>'+stok+'</td>';
            elm += '<td><input type="number" name="storeval'+i+'" id="storeval'+i+'" onkeyup="validval('+i+')"></td>';
            elm += "</tr>";
            numberElm = i;
          }
          console.log('test');
          console.log(numberElm);
          for (var i = 0; i < pra.length; i++) {
            var nov = i +1;
            no = nov + no;
            var noel = numberElm + nov;
            elm += "<tr>";
            elm += '<td>'+no+'<input type="hidden" name="srctype'+noel+'" id="srctype'+noel+'" value="2"><input type="hidden" name="pidval'+noel+'" id="pidval'+noel+'" value="'+production_id+'"><input type="hidden" name="idval'+noel+'" id="idval'+noel+'" value="'+pra[i]['id']+'"></td>';
            elm += "<td id='production_size_"+noel+"' >"+pra[i]['size']+"</td>";
            elm += '<td>'+pra[i]['block']+"</td>";
            var testv = 0;
            var stok = 0;
            for (var z = 0; z < temps.length; z++) {
              if(temps[z]['source_type'] == 2){
                // console.log(temps[z]['source_id']);
                // console.log("++");
                // console.log(prd[i]['id']);
                if(temps[z]['source_id'] == pra[i]['id']){
                  console.log("masuk");
                  testv = testv + temps[z]['quantity'];
                  // console.log(prd[i]['block']);
                  // console.log(temps[z]['quantity']);
                }
              }
            }
            stok = pra[i]['block'] - testv;
            console.log("---");
            console.log(testv);
            console.log("---");
            elm += '<td>'+testv+'<input type="hidden" name="blockval'+noel+'" id="blockval'+noel+'" value="'+stok+'"></td>';
            elm += '<td>'+stok+'</td>';
            elm += '<td><input type="number" name="storeval'+noel+'" id="storeval'+noel+'" onkeyup="validval('+noel+')"></td>';
            elm += "</tr>";
          }
          console.log(elm);
          $("#elmTable").html(elm);
           //window.location.replace('{{ URL::to("/getSizeBlock") }}');
      }
  });
}
$(document).ready(function(){





  $("#storeSubmit").click(function(){
    var cRow = $('#elmTable').find('tr').length;
    console.log("row");
    console.log(cRow);

    var sendVals = [];
    var cartHTML="";
    for (var x = 0; x < cRow; x++) {


      console.log($("#storeval"+x).val());
      if($("#storeval"+x).val() != ""){
        console.log("===");
        console.log(x);
        var sendVal = {};
        var product_name = $("#product_name_"+$("#pidval"+x).val()).text();

        var size = $("#production_size_"+x).text();
        var store_value = $("#storeval"+x).val();
        sendVal['srctype'] = $("#srctype"+x).val();
        sendVal['pidval'] = $("#pidval"+x).val();
        sendVal['idval'] = $("#idval"+x).val();
        sendVal['storeval'] = store_value;
        // console.log("MASUK");
        console.log(sendVal);
        sendVals.push(sendVal);


        cartHTML+="<tr>";
        cartHTML+="<td>"+product_name+"</td>";
        cartHTML+="<td>"+size+"</td>";
        cartHTML+="<td>"+store_value+"</td>";
        cartHTML+="</tr>";

        // console.log("=====");
        // console.log($("#srctype"+i).val())
        // console.log($("#pidval"+i).val());
        // console.log($("#idval"+i).val());
        // console.log($("#storeval"+i).val());
        // console.log(sendVal);
        // sendVal="";
      }

    }
    $("#cart").html(cartHTML);
    console.log(sendVals);
    $("#valArySend").val(JSON.stringify(sendVals));
    $("#myModal2").modal('hide');
  });

  $("#lookup").click(function(){
    // var fRow = $('#elmTableFirst').find('tr').length;
    // // console.log(fRow);
    // for (var i = 0; i < fRow; i++) {
    //   var qtyjs = parseInt($("#elmTableFirst tr").eq(i).find('td:eq(3)').text());
    //   console.log(qtyjs);
    //   if(qtyjs < 1){
    //     $("#elmTableFirst tr").eq(i).hide();
    //   }
    //   // console.log("test");
    // }

    $('#myModal').modal();
    // $("#productionResult").DataTable({
    //   "pageLength": 5
    // });
  });

  $('#datetime').datetimepicker({
		sideBySide: true,
		format: 'YYYY-MM-DD HH:mm',
	});
});
</script>
<script>
  $(function() {
    var table = $("#productionResult").DataTable({
      processing: true,
      serverSide: true,
      "pageLength": 5,
      "order": [[ 5, "desc" ]],
      ajax: "{{ url('storage/data') }}",
      columns: [
      { data: 'id', name: 'id' },
      { data: 'production.name', name: 'production.name' },
      { data: 'detail_html', name: 'detail_html' },
      { data: 'total_block', name: 'total_block' },
      { data: 'block_weight', name: 'block_weight' },
      { data: 'date', name: 'date' },
      { data: 'action', 'searchable': false, 'orderable':false }
      ]
    });
  });
</script>
@stop
