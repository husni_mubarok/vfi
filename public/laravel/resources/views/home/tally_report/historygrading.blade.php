@extends('layouts.tablet.template')
<meta name="_token" content="{{ csrf_token() }}"/>    
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12"> 
      <div class="x_panel"> 
        <div class="x_content">   
          <div class="col-md-4 col-sm-4 col-xs-4">
           <center><h3><b>GRADING HISTORY</b></h3></center> 
         </div> 
         <div class="col-md-4 col-sm-4 col-xs-4">
           <center><h1>{{$temp_tally->trans_code}}</h1></center>   
         </div>   

        <div class="col-md-1 col-sm-1 col-xs-1" style="padding: 5px">  
          <a href="{{ URL::route('tally_report.index') }}" type="button" class="btn btn-primary btn-lg btn-block nomargin"  style="padding: 10px 0;"><i class="fa fa-home faa-pulse animated"></i></a>
        </div>  
       
   </div>
 </div>
</div> 

 <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: -30px">
   <hr>
   <p id="breadcrumb"></p>
 </div> 
</div>  


<div class="row">

  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="col-md-12">
      <div class="x_panel"> 
        <div class="x_content">  
          <table id="itemTable" class="table table-striped dataTable">
            <thead>
              <tr>
                <th>Qty Type</th>
                <th>Grade</th>
                <th>Size</th>
                <th>Freezing</th>  
                <th>Code Size</th>
                <th>Block Weight (Kg)</th>
                <th>Total Block</th>
                <th>Rest</th>
              </tr>
            </thead>
            <tbody>
            @if(isset($grading_history)) 
              @foreach($grading_history as $key => $history_grading)
              <tr>
                <td>{{$history_grading->qty_name}}</td>
                <td>{{$history_grading->grade}}</td>
                <td>{{$history_grading->size}}</td>
                <td>{{$history_grading->freezing}}</td>
                <td>{{$history_grading->code_size}}</td>
                <td>{{$history_grading->block}}</td>
                <td>{{$history_grading->total_block}}</td>
                <td>{{$history_grading->rest}}</td>
              </tr>
              @endforeach
              @endIf
            </tbody>
          </table>
        </div> 
      </div>
    </div>  
  </div>   

