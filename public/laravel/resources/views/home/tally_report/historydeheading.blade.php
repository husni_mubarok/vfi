@extends('layouts.tablet.template')
<meta name="_token" content="{{ csrf_token() }}"/>  
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12"> 
      <div class="x_panel"> 
        <div class="x_content">   
          <div class="col-md-4 col-sm-4 col-xs-4">
           <center><h3><b>HEADSPLIT HISTORY</b></h3></center> 
         </div> 
         <div class="col-md-4 col-sm-4 col-xs-4">
           <center><h1>{{$temp_tally->trans_code}}</h1></center>   
         </div>    

        <div class="col-md-1 col-sm-1 col-xs-1" style="padding: 5px">  
          <a href="{{ URL::route('tally_report.index') }}" type="button" class="btn btn-primary btn-lg btn-block nomargin"  style="padding: 10px 0;"><i class="fa fa-home faa-pulse animated"></i></a>
        </div>  
      </div> 
       
   </div>
 </div>
</div> 

 <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: -30px">
   <hr>
   <p id="breadcrumb"></p>
 </div> 
  </div>   


  <div class="row"> 
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-12">
          <div class="x_panel"> 
            <div class="x_content"> 
              <table id="itemTable" class="table table-bordered dataTable">
                <thead>
                  <tr>   
                    <th>Name</th>
                    <th><center>Weight</center></th> 
                    <th><center>Waste</center></th>   

                  </tr> 
                </thead>
                <tbody>  
                @if(isset($array_all))
                  @foreach($array_all AS $all)  
                   <tr>
                    <td colspan="3">{{$all["name"]}}</td> 
                    </tr>
                    @foreach($all["detail"] AS $detail)
                    <tr> 
                   <td></td>
                    <td>{{$detail["weight"]}}</td>    
                    <td>{{$detail["waste"]}}</td>  
                    </tr> 
@endforeach 
                  <tr>
                    <td><b>TOTAL</b></td>  
                    <td><b>{{$all["total_weight"]}}</b></td>    
                    <td><b>{{$all["total_waste"]}}</b></td>  
</tr>
                      @endforeach 
                      @endIf
                    </tbody> 
                  </table>   
                </div> 
              </div>
            </div>  
          </div>  
