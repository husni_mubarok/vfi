@extends('layouts.home.template')
@section('content')
<div class="header_menu mobile-hide">
	<a href="{{ URL::route('purchase.index') }}">PURCHASE</a>
</div>
<section class="content box box-solid">
 <div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<div class="x_panel">
					 <div class="x_content">
					{{ csrf_field() }}
					 <div role="tabpanel" class="tab-pane active" id="wait">
    						<br><br> 
    						<table id="index_table" class="table table-striped dataTable">
								<thead>
									<tr>
										<th width="5%">No.</th>
										<th>Code</th>
										<th>Date</th>  
										<th>Quantity</th>
										<th>Status</th> 
										<th>action</th>
									</tr>
								</thead>
								<tbody id="table_body">

									@foreach($temp_tally as $key => $tt)
									<tr>
										<td>{{ $key+1 }}</td>
										<td>{{ $tt->trans_code}}</td>
										<td>{{ $tt->date_time}}</td>
										<td>{{ $tt->quantity }}</td>
										<td>{{ $tt->form_status }}</td>
										<td>
										<a href="{{ URL::route('tally_report.historyreceiving', [$tt->id]) }}" class="btn btn-primary btn-sm"><i class="fa fa-file-text-o"></i>&nbsp;Receiving</a>
										<a href="{{ URL::route('tally_report.historydeheading', [$tt->id]) }}" class="btn btn-primary btn-sm"><i class="fa fa-file-text-o"></i>&nbsp;Headsplit</a>
										<a href="{{ URL::route('tally_report.historypeeling', [$tt->id]) }}" class="btn btn-primary btn-sm"><i class="fa fa-file-text-o"></i>&nbsp;Peeling</a>
										<a href="{{ URL::route('tally_report.historygrading', [$tt->id]) }}" class="btn btn-primary btn-sm"><i class="fa fa-file-text-o"></i>&nbsp;Grading</a>
										<a href="{{ URL::route('tally_report.viewpacking', [$tt->id]) }}" class="btn btn-primary btn-sm"><i class="fa fa-file-text-o"></i>&nbsp;Packing</a>
										<a href="{{ URL::route('tally_report.finalreport', [$tt->id]) }}" class="btn btn-primary btn-sm"><i class="fa fa-file-text-o"></i>&nbsp;Final Report</a>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>  
					</div>
				</div>
			</div>
		</div>
</section>
 @stop



@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function () {
    $("#index_table").DataTable(
    	{
    	});
    });
</script>
@stop