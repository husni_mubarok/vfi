@extends('layouts.tablet.template')
<meta name="_token" content="{{ csrf_token() }}"/>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
         <div class="x_panel"> 
        <div class="x_content">   
          <div class="col-md-4 col-sm-4 col-xs-4">
           <center><h3><b>PACKING</b></h3></center> 
         </div> 
         <div class="col-md-4 col-sm-4 col-xs-4">
           <center><h1>{{$temp_tally->trans_code}}</h1></center>   
         </div>  
          
        <div class="col-md-1 col-sm-1 col-xs-1" style="padding: 5px">  
          <a href="{{ URL::route('tally_report.index') }}" type="button" class="btn btn-primary btn-lg btn-block nomargin"  style="padding: 10px 0;"><i class="fa fa-home faa-pulse animated"></i></a>
        </div>  
   </div>
   <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: -30px">
   <hr>
   <p id="breadcrumb"></p>
 </div>
   </div> 

   

<div class="row">  
  <div class="col-md-12 col-sm-12 col-xs-12"> 
    <div class="x_panel"> 
      <div class="x_content">   
        <table id="index_table" class="table table-striped dataTable">
          <thead>
            <tr> 
              <th rowspan="2">#</th>
              <th rowspan="2">GRADE</th>
              <th rowspan="2">SIZE</th>  
              <th rowspan="2">FREEZING</th> 
              <th rowspan="2">PRODUCTION CODE</th>
              <th rowspan="2">BLOCK</th>
              <th rowspan="2">TOTAL</th>
              <th rowspan="2">TOTAL BLOCK</th>
              <th rowspan="2">IC</th>
              <th colspan="4"><center>TO BE SET</center></th>
            </tr> 
            <tr> 
              <th>MC</th>
              <th>MIX</th>
              <th>REST</th>  
              <th>ICR</th>  
            </tr> 
          </thead> 
          <tbody>
           @foreach($tally_detail as $key => $tally_details)
           <tr>
            <td>{{$key+1}}</td>  
            <td>{{$tally_details->grade}}</td>  
            <td>{{$tally_details->size}}</td>
            <td>{{$tally_details->freezing}}</td>
            <td>{{$tally_details->purchase_code}} | {{$tally_details->code_size}}</td>
            <td>{{$tally_details->block_weight}}</td>
            <td>{{$tally_details->total}}</td>
            <td>{{$tally_details->total_block}}</td>
            <td>{{$tally_details->ic}}
            </td>
            <td>
             {{$tally_details->mc}}
           </td>
           <td>
             {{$tally_details->mix}}
           </td>
           <td>
            {{$tally_details->rest}}
          </td>
          <td>
           {{$tally_details->icr}}
         </td>
       </tr> 
       @endForeach
     </tbody> 
   </table>  
 </div>
</div>
</div>   
 

 
<script>

function storeclose ()
  { 
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this packing!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#BDBDBD',
      confirmButtonText: 'Yes, close it!'
    }).then(function () {

      $('#form_close').submit();
    })
  };  
</script>


@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function () {
    $("#index_table").DataTable(
      {
         "scrollX": true
      });
    });
</script>
@stop