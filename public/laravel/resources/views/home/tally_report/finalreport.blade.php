@extends('layouts.tablet.template')
<meta name="_token" content="{{ csrf_token() }}"/>



<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="col-md-1"></div> 
    <div class="x_panel">
      <h2>
        <i class="fa fa-file-text"></i>&nbsp;FINAL REPORT
      </h2> 
      <h4>&nbsp;CODE : {{$temp_tally->trans_code}}</h4>
      <hr> 
      <div class="x_content">
       <div class="col-md-3 col-sm-3 col-xs-3">

        {{ Form::label('name', 'TOTAL MC DEFROST') }}
        <br/>
        MC 
        <br/>

        {{ Form::label('name', 'TOTAL MC DEFROST') }}
        <div class="input-group"> 
          {{ Form::text('name',old('name'), array('class' => 'form-control', 'placeholder' => 'TOTAL MC DEFROST*', 'required' => 'true', 'id' => 'name')) }} 
          <span class="input-group-addon">KG</span> 
        </div>
        <br/>

        {{ Form::label('name', 'TOTAL CUMULATIVE') }} 
        <br/>
        MC 
        <br/>

        {{ Form::label('name', 'RAW MATERIAL') }} 
        <br/>
        MC 
        <br/>

        {{ Form::label('name', 'RM FRESH') }} 
        <br/>
        MC 
        <br/>

        {{ Form::label('name', 'RM BEKU') }} 
        <br/>
        MC 
        <br/> 
      </div>

      <div class="col-md-3 col-sm-3 col-xs-3">

        {{ Form::label('name', 'TOTAL MC DEFROST') }}
        <br/>
        MC 
        <br/>

        {{ Form::label('name', 'TOTAL MC DEFROST') }}
        <div class="input-group"> 
          {{ Form::text('name',old('name'), array('class' => 'form-control', 'placeholder' => 'TOTAL MC DEFROST*', 'required' => 'true', 'id' => 'name')) }} 
          <span class="input-group-addon">MC</span> 
        </div>
        <br/>

        {{ Form::label('name', 'TOTAL CUMULATIVE') }} 
        <br/>
        MC 
        <br/>  
      </div>

      <div class="col-md-6 col-sm-6 col-xs-6">
       <h4><b>END PRODUCT</b></h4><hr/>
     </div>

     <div class="col-md-3 col-sm-3 col-xs-3">

      {{ Form::label('name', 'TOTAL MC TODAY') }}
      <br/>
      MC 
      <br/>

      {{ Form::label('name', 'TOTAL MC CS A') }}
      <div class="input-group"> 
        {{ Form::text('name',old('name'), array('class' => 'form-control', 'placeholder' => 'TOTAL MC DEFROST*', 'required' => 'true', 'id' => 'name')) }} 
        <span class="input-group-addon">KG</span> 
      </div>
      <br/>

      {{ Form::label('name', 'CUMULATIVE') }} 
      <br/>
      MC 
      <br/> 
    </div>

    <div class="col-md-3 col-sm-3 col-xs-3"> 
      {{ Form::label('name', 'TOTAL MC TODAY') }} 
      <br/>
      MC 
      <br/>  
      {{ Form::label('name', 'TOTAL MC CS A') }} 
      <br/>
      MC 
      <br/> 
      {{ Form::label('name', 'CUMULATIVE') }} 
      <br/>
      MC 
      <br/> 
    </div>

  </div>
</div>
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
  <hr/>
</div>
</div>

<div class="row">  
  <div class="col-md-12 col-sm-12 col-xs-12"> 
    <div class="x_panel"> 
      <div class="x_content">
        <h3>PROCESS REPORT</h3>   
        <table id="itemTable" class="table table-striped dataTable">
          <thead> 
            <tr> 
              <th>PROCESS TREATMENT</th>
              {{-- <th>SALINITY</th>
              <th>DURATION</th>
              <th>SALINITY RM</th>   --}}
              <th>TOT RM</th> 
              <th>RESULT</th> 
              <th>PERCENTAGE</th>  
            </tr> 
          </thead> 
          <tbody> 
            @if(isset($process_report_ft))
              <tr>
                <td> <b>FIRST TREATMENT</b> </td>
                {{-- <td> {{$process_report_ft->salinity}} </td> --}}
                {{-- <td> {{$process_report_ft->duration}} </td> --}}
                {{-- <td> {{$process_report_ft->salinity_rm}} </td> --}}
                <td> {{$process_report_ft->tot_rm}} </td>
                <td> {{$process_report_ft->result}} </td>
                <td> {{NUMBER_FORMAT((($process_report_ft->result-$process_report_ft->tot_rm)/$process_report_ft->result)*100,2)}} </td>
              </tr>
            @endif

            <tr>
              <td colspan="7"><b>2 ND TREATMENT</b></td> 
            </tr>

            @if(isset($process_report_ft_tube)) 
              <tr>
                <td>TUBE</td>
                {{-- <td> {{$process_report_ft_tube->salinity}} </td> --}}
                {{-- <td> {{$process_report_ft_tube->duration}} </td> --}}
                {{-- <td> {{$process_report_ft_tube->salinity_rm}} </td> --}}
                <td> {{$process_report_ft_tube->tot_rm}} </td>
                <td> {{$process_report_ft_tube->result}} </td>
                <td> {{NUMBER_FORMAT((($process_report_ft_tube->result-$process_report_ft_tube->tot_rm)/$process_report_ft_tube->result)*100,2)}} </td>
              </tr>
            @endif

            @if(isset($process_report_ft_tentacle)) 
              <tr>
                <td>TENTACLE </td>
                {{-- <td>{{$process_report_ft_tentacle->salinity}} </td> --}}
                {{-- <td>{{$process_report_ft_tentacle->duration}} </td> --}}
                {{-- <td>{{$process_report_ft_tentacle->salinity_rm}} </td> --}}
                <td> {{$process_report_ft_tentacle->tot_rm}} </td>
                <td> {{$process_report_ft_tentacle->result}} </td>
                <td> {{NUMBER_FORMAT((($process_report_ft_tentacle->result-$process_report_ft_tentacle->tot_rm)/$process_report_ft_tentacle->result)*100,2)}} </td>
              </tr>  
            @endif
          </tbody> 
        </table>  
      </div>
    </div>



<div class="row">  
  <div class="col-md-12 col-sm-12 col-xs-12"> 
    <div class="x_panel"> 
      <div class="x_content">
        <h3>WORK RESULT</h3>   
        <table id="itemTable" class="table table-striped dataTable">
          <thead> 
            <tr> 
              <th colspan="2"><center>WORK RESULT</center></th>
              <th>YIELD</th> 
            </tr> 
          </thead> 
          <tbody> 
            @if(isset($process_report_ft_tube))
              <tr>
                <td>TOT WEIGHT TUBE AFTER TREATMENT</td>
                <td>{{$process_report_ft_tube->result}}</td>  
                <td></td> 
              </tr> 
            @endif

  <!-- @if(isset($process_wr))
          <tr>
             <td>TOT WEIGHT TUBE BEFORE ABF</td>
             <td>{{$process_wr->tube}}</td>
             <td>{{NUMBER_FORMAT((($process_wr->tube-$process_report_ft_tube->result)/$process_report_ft_tube->result)*100,2)}}</td>    
          </tr> 
@endif

@if(isset($process_report_ft_tentacle))
          <tr>
             <td>TOT WEIGHT TENTACLES AFTER TREATMENT</td>
             <td>{{$process_report_ft_tentacle->result}}</td> 
             <td></td>   
          </tr> 
          @endif

@if(isset($process_wr))
          <tr>
             <td>TOT WEIGHT TENTACLE BEFORE ABF</td>
             <td>{{$process_wr->tentacle}}</td>  
             <td>{{NUMBER_FORMAT((($process_wr->tentacle-$process_report_ft_tentacle->result)/$process_report_ft_tentacle->result)*100,2)}}</td>    
          </tr>  
@endif
            -->
    </tbody> 
  </table>  
</div>
</div>
</div>   
<div class="row">  
  <div class="col-md-12 col-sm-12 col-xs-12"> 
    <div class="x_panel"> 
      <div class="x_content">
        <h3>TO BE ADDITIONAL OR WASTE</h3>   
        <table id="itemTable" class="table table-striped dataTable">
          <thead> 
            <tr> 
              <th><center>NAME</center></th>
              <th>WEIGHT</th> 
            </tr> 
          </thead> 
          <tbody>
          @if(isset($process_rec_dhd))
          @foreach($process_rec_dhd as $key => $prdhd) 
           <tr>
             <td>{{$prdhd->name}}</td>
             <td>{{$prdhd->weight}}</td>  
            <td></td> 
          </tr>  
          @endforeach
          @endif
    </tbody> 
  </table>  
</div>
</div>
</div>   
<div class="row">  
  <div class="col-md-12 col-sm-12 col-xs-12"> 
    <div class="x_panel"> 
      <div class="x_content">
        <h3>ADDITIONAL VALUE</h3>   
        <table id="itemTable" class="table table-striped dataTable">
          <thead> 
            <tr> 
              <th><center>NAME</center></th>
              <th>WEIGHT</th> 
            </tr> 
          </thead> 
          <tbody>
          @if(isset($process_rec_add))
          @foreach($process_rec_add as $key => $pra) 
           <tr>
             <td>{{$pra->name}}</td>
             <td>{{$pra->weight}}</td>  
            <td></td> 
          </tr>  
          @endforeach
          @endif
    </tbody> 
  </table>  
</div>
</div>
</div>   

<script>

  function storeclose ()
  { 
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this packing!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#BDBDBD',
      confirmButtonText: 'Yes, close it!'
    }).then(function () {

      $('#form_close').submit();
    })
  };  
</script>


