@extends('layouts.tablet.template')
<meta name="_token" content="{{ csrf_token() }}"/> 

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12"> 
      <div class="x_panel"> 
        <div class="x_content">   
          <div class="col-md-4 col-sm-4 col-xs-4">
           <center><h3><b>RECEIVING HISTORY</b></h3></center> 
         </div> 
         <div class="col-md-4 col-sm-4 col-xs-4">
           <center><h1>{{$temp_tally->trans_code}}</h1></center>   
         </div>   
        <div class="col-md-1 col-sm-1 col-xs-1" style="padding: 5px">  
          <a href="{{ URL::route('tally_report.index') }}" type="button" class="btn btn-primary btn-lg btn-block nomargin"  style="padding: 10px 0;"><i class="fa fa-home faa-pulse animated"></i></a>
        </div>  
       
   </div>
 </div>
</div> 

 <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: -30px">
   <hr>
   <p id="breadcrumb"></p>
 </div> 


<div class="row"> 

  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="col-md-6">
      <div class="x_panel"> 
        <div class="x_content">  
         @if(isset($array_all_receiving))
         <b>RECEIVING</b> 
         <table id="itemTable" class="table table-bordered dataTable">
          <thead>
            <tr>   
              <th width="20%">Size</th>
              <th width="80%">Weight</th>  
            </tr> 
          </thead>
          <tbody> 
            @foreach($array_all_receiving AS $all_receiving) 
            <tr>  
              <td width="20%" colspan="3"><b>{{$all_receiving["note"]}}</b></td>  
              <tr>   
               @foreach($all_receiving["detail"] AS $detail_item_receiving) 
               <tr>  
                <td width="20%"></td>  
                <td width="80%">{{$detail_item_receiving["weight"]}}</td> 
              </tr> 
              @endforeach  
              <td width="20%"><b>TOTAL</b></td>  
              <td width="80%"><b>{{$all_receiving["total_weight"]}}</b></td> 
              @endforeach 
            </tbody> 
          </table>
        </div>
      </div>
    </div> 

    @endif 


    @if(isset($array_all_treatment)) 
    <div class="col-md-6">
      <div class="x_panel"> 
        <div class="x_content">  
          <b>TREATMENT</b> 
          <table id="itemTable" class="table table-bordered dataTable">
            <thead>
              <tr>   
                <th width="20%">Size</th>
                <th width="80%"><center>Weight</center></th>   
              </tr> 
            </thead>
            <tbody> 
              @foreach($array_all_treatment AS $all_treatment) 
              <tr>  
                <td width="80%" colspan="2">{{$all_treatment["note"]}}</td>  
                <tr>  
                  @foreach($all_treatment["detail"] AS $detail_item_treatment) 
                  <tr>  
                    <td width="20%"></td> 
                    <td width="80%">{{$detail_item_treatment["weight"]}}</td>
                    <tr>  
                      @endforeach
                      <td width="20%"><b>TOTAL</b></td>  
                      <td width="80%"><b>{{$all_treatment["total_weight"]}}</b></td>
                      @endforeach
                    </tbody>
                  </table>   
                </div> 
              </div>
            </div> 
            @endif 
          </div>
        </div>  
      </div>   
      <br/>


      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-12">
          <div class="x_panel"> 
            <div class="x_content">  
              <br/>
              <br/>
              <b>ADDITIONAL</b> 
              <table id="itemTable" class="table table-bordered dataTable">
                <thead>
                  <tr>   
                    <th>Name</th>
                    <th><center>Weight</center></th>   
                    <th><center>Pan</center></th>   
                    <th><center>Rest</center></th>   
                  </tr> 
                </thead>
                <tbody>  
                  @if(isset($array_all_add))
                  @foreach($array_all_add AS $all_add)
                  <tr>   
                    <td colspan="4">{{$all_add["name"]}}</td>  
                    <td></td>      
                    <td></td>   
                    <tr>  
                      @foreach($all_add["detail"] AS $detail_add) 
                      <tr>   
                        <td></td>  
                        <td>{{$detail_add["weight"]}}</td>     
                        <td>{{$detail_add["pan"]}}</td>   
                        <td>{{$detail_add["rest"]}}</td>   
                        <tr> 
                         @endforeach  
                         <td><b>TOTAL</b></td>  
                         <td><b>{{$all_add["total_weight"]}}</b></td>      
                         <td><b>{{$all_add["total_pan"]}}</b></td> 
                         <td><b>{{$all_add["total_rest"]}}</b></td> 
                         @endforeach  
                         @endIf
                       </tbody> 
                     </table>   
                   </div> 
                 </div>
               </div>  
             </div>   
           </div>  <br>


           <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-12">
          <div class="x_panel"> 
            <div class="x_content">  
              <br/>
              <br/>
              <b>TREATMENT OPTION</b> 
               <table id="itemTable" class="table table-bordered dataTable">
            <thead>
              <tr>   
                <th>Salinity</th>
                <th>Duration</th> 
              </tr> 
            </thead>
            <tbody> 
            @if(isset($treatment_option)) 
              @foreach($treatment_option AS $treatment_options)  
              <tr>  
                  <td>{{$treatment_options->salinity}}</td>  
                  <td>{{$treatment_options->duration}}</td> 
                  <tr> 
                    @endforeach  
                    @endIf
                  </tbody>
                </table>     
                   </div> 
                 </div>
               </div>  
             </div>   
           </div> 
           <hr/>

