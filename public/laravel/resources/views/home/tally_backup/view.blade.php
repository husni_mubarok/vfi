@extends('layouts.home.template')
@section('content')
<style type="text/css">
  .modal {
    text-align: center;
    padding: 0!important;
  }

  .modal:before {
    content: '';
    display: inline-block;
    height: 100%;
    vertical-align: middle;
    margin-right: -4px;
  }

  .modal-dialog {
    display: inline-block;
    text-align: left;
    vertical-align: middle;
  }
</style>
<div class="header_menu mobile-hide">
  <a href="{{ URL::route('tally.index') }}">MASTER TALLY</a>
</div> 
<section class="content">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      {!! Form::model($purchase, array('route' => ['tally.storedetail', $purchase->id], 'method' => 'PUT', 'class'=>'update', 'id' => 'form_tally'))!!} 
      <div class="col-md-4">
        <div class="x_panel"> 
          <div class="x_content">   

            {{ Form::label('trans_code', 'Trans Code') }} 
            {{ Form::text('trans_code_sw',old('trans_code', $purchase->trans_code), array('class' => 'form-control', 'placeholder' => 'Trans Code*', 'required' => 'true', 'id' => 'trans_code', 'disabled' => 'disabled')) }}
            {{ Form::hidden('trans_code',old('trans_code'), array('required' => 'true', 'id' => 'trans_code')) }}<br/>

            {{ Form::label('name', 'Product') }} 
            {{ Form::text('name',old('name'), array('class' => 'form-control', 'placeholder' => 'Product*', 'required' => 'true', 'id' => 'name', 'disabled' => 'disabled')) }}
            {{ Form::hidden('product_id',old('product_id'), array('required' => 'true', 'id' => 'product_id')) }}<br/> 
          </div>
        </div>
      </div> 
      <div class="col-md-12 col-sm-12 col-xs-12">
        <hr> 
      </div>
      {!! Form::close() !!} 
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3">
          <div class="x_panel"> 
            <div class="x_content"> 
              <b>TREATMENT</b>
              <hr> 
              <table id="itemTable" class="table table-striped dataTable">
                <thead>
                  <tr> 
                    <th>#</th>
                    <th>Size</th>
                    <th>Weight</th> 
                  </tr>
                </thead>
                <tbody>
                  @foreach($tally_detail_view_treatment as $key => $tally_detail_view_treatments)
                  <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$tally_detail_view_treatments->note}}</td>
                    <td>{{$tally_detail_view_treatments->weight}}</td>
                  </tr>
                  @endForeach
                </tbody>
              </table>   
            </div> 
          </div>
        </div> 
        <div class="col-md-3">
          <div class="x_panel"> 
            <div class="x_content">  
              <b>RECEIVING</b>
              <hr> 
              <table id="itemTable" class="table table-striped dataTable">
                <thead>
                  <tr> 
                    <th>#</th>
                    <th>Size</th>
                    <th>Weight</th> 
                  </tr>
                </thead>
                <tbody>
                  @foreach($tally_detail_view_receiving as $key => $tally_detail_view_receivings)
                  <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$tally_detail_view_receivings->note}}</td>
                    <td>{{$tally_detail_view_receivings->weight}}</td>
                  </tr>
                  @endForeach
                </tbody>
              </table>  
            </div> 
          </div>  
        </div>  

        <div class="col-md-6">
          <div class="x_panel"> 
            <div class="x_content"> 
              <b>ADDITIONAL</b>
              <hr>
              <div class="x_panel"> 
                <div class="x_content">  
                  <div class="div_overflow">
                    <table id="itemTable" class="table table-striped dataTable">
                      <thead>
                        <tr> 
                          <th>#</th>
                          <th>Name</th>
                          <th>Weight</th> 
                          <th>Pan</th> 
                          <th>Rest</th> 
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($tally_additional_view as $key => $tally_additional_views)
                        <tr>
                          <td>{{$key+1}}</td>
                          <td>{{$tally_additional_views->name}}</td>
                          <td>{{$tally_additional_views->weight}}</td>
                          <td>{{$tally_additional_views->pan}}</td>
                          <td>{{$tally_additional_views->rest}}</td>
                        </tr>
                        @endForeach
                      </tbody>
                    </table>   
                  </div> 
                </div>  
              </div>  
            </div>
          </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
          <hr>
          {!! Form::open(array('route' => ['tally.close', $purchase->id], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_close'))!!}
          {{ csrf_field() }}  
          <a href="{{ URL::route('tally.history', $purchase->id) }}" class="btn btn-default btn-lg pull-right" style="margin-right: 10px"><i class="fa fa-note"></i> History</a>
           <a href="{{ URL::route('tally.index') }}" class="btn btn-default btn-lg pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Exit</a> 
          {!! Form::close() !!}
        </div>
      </div> 
    </div> 
  </section>
  @stop

