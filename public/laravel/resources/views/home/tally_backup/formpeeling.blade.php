@extends('layouts.home.template')
@section('content')
<style type="text/css">
  .modal {
    text-align: center;
    padding: 0!important;
  }

  .modal:before {
    content: '';
    display: inline-block;
    height: 100%;
    vertical-align: middle;
    margin-right: -4px;
  }

  .modal-dialog {
    display: inline-block;
    text-align: left;
    vertical-align: middle;
  }
</style>
<div class="header_menu mobile-hide">
  <a href="{{ URL::route('tally.index') }}">WIP</a>
</div> 
<section class="content">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      {!! Form::model($purchase, array('route' => ['tally.storepeeling', $purchase->id], 'method' => 'PUT', 'class'=>'update', 'id' => 'form_tally'))!!} 
      <div class="col-md-4">
        <div class="x_panel"> 
          <div class="x_content">   

            {{ Form::label('trans_code', 'Trans Code') }} 
            {{ Form::text('trans_code_sw',old('trans_code', $purchase->trans_code), array('class' => 'form-control', 'placeholder' => 'Trans Code*', 'required' => 'true', 'id' => 'trans_code', 'disabled' => 'disabled')) }}
            {{ Form::hidden('trans_code',old('trans_code'), array('required' => 'true', 'id' => 'trans_code')) }}<br/>

            {{ Form::label('name', 'Product') }} 
            {{ Form::text('name',old('name'), array('class' => 'form-control', 'placeholder' => 'Product*', 'required' => 'true', 'id' => 'name', 'disabled' => 'disabled')) }}
            {{ Form::hidden('product_id',old('product_id'), array('required' => 'true', 'id' => 'product_id')) }}<br/>

            {{ Form::label('project', 'Project') }} 
            {{ Form::text('project',old('project'), array('class' => 'form-control', 'placeholder' => 'Project*', 'required' => 'true', 'id' => 'project', 'disabled' => 'disabled')) }}<br>
 
            <br/>
            <table id="itemTable" class="table table-striped dataTable">
              <thead>
                <tr> 
                  <th>#</th>
                  <th>Name</th>
                  <th>Weight</th>  
                  <th>Action</th> 
                </tr>
              </thead>
              <tbody>
               @foreach($tally_detail as $key => $tally_details)
               <tr>
                <td>
                  {{$key+1}}</td>  
                  <td> 
                    {{$tally_details->name}}
                  </td>  
                  <td> 
                    {{ Form::number('tally['.$tally_details->id.'][weight]', old($tally_details->id.'[weight]', 0), ['class' => 'form-control', 'id' => 'weight_'.$tally_details->id, 'min' => '0']) }} 
                  </td> 
                  <td> 
                      <button type="button" class="btn btn-primary btn-sm" id="btn_add_{{$tally_details->id}}" onclick="add_{{$tally_details->id}}();">
                       <i class="fa fa-plus"></i>
                     </button> 
                  <button type="button"  class="btn btn-warning btn-sm" id="btn_reduce_{{$tally_details->id}}" onclick="reduce_{{$tally_details->id}}();"><i class="fa fa-minus"></i>
                     </button>
                  </td> 
                </tr>

                 <script type="text/javascript">  
                function reduce_{{$tally_details->id}}() { 
                  var weight_{{$tally_details->id}} = document.getElementById('weight_{{$tally_details->id}}').value; 
                  document.getElementById('weight_{{$tally_details->id}}').value = parseInt(weight_{{$tally_details->id}})-parseInt(1); 
                };

                function add_{{$tally_details->id}}() {  
                  var weight_{{$tally_details->id}} = document.getElementById('weight_{{$tally_details->id}}').value; 
                  document.getElementById('weight_{{$tally_details->id}}').value = parseInt(weight_{{$tally_details->id}})+parseInt(1); 
                }; 

                </script>
                @endForeach
              </tbody>
            </table> 
            <button type="button" id="btn_confirm" data-toggle="modal" data-target="#modal_confirm" class="btn btn-success pull-right"><i class="fa fa-check"></i> Add</button>
            <a href="{{ URL::route('tally.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>  
            <br>  
          </div>  
        </div>
      </div> 
      {!! Form::close() !!} 
  </section>
  @stop

  @section('modal')
  <div class="modal fade" id="modal_confirm">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="summary_cashbond_payroll_date"> Submit this form?</h4>
        </div> 
       <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
        <button type="button" id="btn_submit" onclick="submit_form();" class="btn btn-success"><i class="fa fa-check"></i> Submit</button>
      </div>
    </div>
  </div>
</div>
@stop

<script>
  function submit_form ()
  { 
       
       $('#form_tally').submit();
     
  }; 

  function set_btn_receiving_active()
  { 
    $("#btn_receiving").removeClass("btn btn-default").addClass("btn btn-primary");
    $("#btn_treatment").removeClass("btn btn-primary").addClass("btn btn-default");

    document.getElementById ("tally_type").value = "RECEIVING";

  };
  function set_btn_treatment_active()
  { 
    $("#btn_treatment").removeClass("btn btn-default").addClass("btn btn-primary");
    $("#btn_receiving").removeClass("btn btn-primary").addClass("btn btn-default");

    document.getElementById ("tally_type").value = "TREATMENT";

  };

$(function() {
    console.log( "ready!" );
});

</script>


@if(isset($tally))
<script>

  var cart_receiving = JSON.parse("{}"); 

// ADD BUTTON DETAIL
$("#btn_add_detail").on('click', function() { 

  //console.log("asdas");
  
  var inpweight = $("#weight"); 

  if (inpweight.val().length < 1) {
    alert("Some data can't empty!");
  }else{

    var weight = $("#weight").val();  

    var size_id = $("#size_id option:selected").val(); 
    var tally_type = $("#tally_type option:selected").val(); 
    var key = size_id+"_"+weight+"_"+tally_type;

    if(cart_receiving[key] == undefined)
    {
      cart_receiving[key] = parseFloat(weight); 
      var new_row = '';
      new_row += '<tr>';
      new_row += '<td>';
      new_row += '<input type="hidden" name="size_id['+key+']" value="'+size_id+'">';
      new_row += $("#size_id option:selected").text();
      new_row += '</td>';

      new_row += '<td>';
      new_row += '<input type="hidden" name="tally_type['+key+']" value="'+tally_type+'">';
      new_row += tally_type;
      new_row += '</td>'; 

      new_row += '<td>'; 
      new_row += '<input type="hidden" name="weight['+key+']" value="'+cart_receiving[key]+'" id="weight_'+key+'">';
      new_row += '<div id="txtweight_'+key+'">'+weight+'</div>';
      new_row += '</td>';  

      new_row += '<td>'; 
      new_row += '<button type="button" class="btn btn-danger btn-sm" id="btn_remove_'+key+'">';
      new_row += '<i class="fa fa-remove"></i>';
      new_row += '</button>';
      new_row += '</td>';
      new_row += '</tr>';

      $("#cart_receiving").append(new_row); 

      $("#btn_remove_"+key).on('click', function() 
      {
        cart_receiving[key] == null;
        $(this).parent().parent().remove();
      });
    }
    else 
    {
    //alert("sadd");
    //console.log(cart_item[key]);

    cart_receiving[key] += parseFloat(weight);
    $("#weight_"+key).val(cart_receiving[key]);
    $("#txtweight_"+key).text(cart_receiving[key]); 

  } 

  document.getElementById ("weight").value = ""; 
}

});

jQuery.each({!! $tally !!}, function( i, val ) {  
  var size_id = val['size_id']; 
  var tally_type = val['tally_type'];  
  var weight = val['weight'];

  //var item_id = $("#item_id option:selected").val();  

  var key = size_id+"_"+"_"+tally_type+"_"+weight;

  $("#size_id").val(size_id);
  $("#tally_type").val(tally_type);
  //console.log(key);

  var new_row = '';
  new_row += '<tr>';
  new_row += '<td>';
  new_row += '<input type="hidden" name="size_id['+key+']" value="'+size_id+'">';
  new_row += $("#size_id option:selected").text();
  new_row += '</td>';

  new_row += '<td>';
  new_row += '<input type="hidden" name="tally_type['+key+']" value="'+tally_type+'">';
  new_row += $("#tally_type option:selected").text();
  new_row += '</td>';

  new_row += '<td>';
  new_row += '<input type="hidden" name="weight['+key+']" value="'+weight+'">';
  new_row += weight;
  new_row += '</td>'; 

  new_row += '<td>'; 
  new_row += '<button type="button" class="btn btn-danger btn-sm" id="btn_remove_'+key+'">';
  new_row += '<i class="fa fa-remove"></i>';
  new_row += '</button>';
  new_row += '</td>';
  new_row += '</tr>';

  $("#cart_receiving").append(new_row);

  $("#btn_remove_"+key).on('click', function() 
  {
    cart_receiving[key] == null;
    $(this).parent().parent().remove();
  });
  document.getElementById ("weight").value = "";
});
</script>


@endif 

