@extends('layouts.home.template')
@section('content')

<div class="header_menu mobile-hide">
  <a href="{{ URL::route('tally.index') }}">MASTER TALLY</a>
</div> 
<section class="content">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      {!! Form::model($purchase, array('route' => ['tally.storedetailpeeling', $purchase->id], 'method' => 'PUT', 'class'=>'update', 'id' => 'form_tally'))!!} 
      <div class="col-md-4">
        <div class="x_panel"> 
          <div class="x_content">   

            {{ Form::label('trans_code', 'Trans Code') }} 
            {{ Form::text('trans_code_sw',old('trans_code', $purchase->trans_code), array('class' => 'form-control', 'placeholder' => 'Trans Code*', 'required' => 'true', 'id' => 'trans_code', 'disabled' => 'disabled')) }}
            {{ Form::hidden('trans_code',old('trans_code'), array('required' => 'true', 'id' => 'trans_code')) }}<br/>

            {{ Form::label('name', 'Product') }} 
            {{ Form::text('name',old('name'), array('class' => 'form-control', 'placeholder' => 'Product*', 'required' => 'true', 'id' => 'name', 'disabled' => 'disabled')) }}
            {{ Form::hidden('product_id',old('product_id'), array('required' => 'true', 'id' => 'product_id')) }}<br/>

      
              <div class="x_panel"> 
                <div class="x_content">  
                  <div class="div_overflow">
                    <table id="itemTable" class="table table-striped dataTable">
                      <thead>
                        <tr> 
                          <th>#</th>
                          <th>Size</th>
                          <th>Weight</th> 
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($tally_detail_view as $key => $tally_detail_views)
                        <tr>
                          <td>{{$key+1}}</td>
                          <td>{{$tally_detail_views->name}}</td>
                          <td>{{$tally_detail_views->weight}}</td>
                        </tr>
                        @endForeach
                      </tbody>
                    </table>  
                    <hr> 
                     <a href="{{ URL::route('tally.historypeeling', $purchase->id) }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-note"></i> History</a>
                    <a href="{{ URL::route('tally.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Exit</a>  
                  </div> 
                </div>  
              </div> 
            </div>
          </div>   
        </div>  
      </div>
    </div> 
    {!! Form::close() !!} 
  </section>
  @stop

  