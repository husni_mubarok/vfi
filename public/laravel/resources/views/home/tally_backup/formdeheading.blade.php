@extends('layouts.home.template')
@section('content')
<style type="text/css">
  .modal {
    text-align: center;
    padding: 0!important;
  }

  .modal:before {
    content: '';
    display: inline-block;
    height: 100%;
    vertical-align: middle;
    margin-right: -4px;
  }

  .modal-dialog {
    display: inline-block;
    text-align: left;
    vertical-align: middle;
  }
</style>
<div class="header_menu mobile-hide">
  <a href="{{ URL::route('tally.index') }}">DEHEADING</a>
</div> 
<section class="content">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      {!! Form::model($purchase, array('route' => ['tally.storedeheading', $purchase->id], 'method' => 'PUT', 'class'=>'update', 'id' => 'form_tally'))!!} 
      <div class="col-md-6">
        <div class="x_panel"> 
          <div class="x_content">   

            {{ Form::label('trans_code', 'Trans Code') }} 
            {{ Form::text('trans_code_sw',old('trans_code', $purchase->trans_code), array('class' => 'form-control', 'placeholder' => 'Trans Code*', 'required' => 'true', 'id' => 'trans_code', 'disabled' => 'disabled')) }}
            {{ Form::hidden('trans_code',old('trans_code'), array('required' => 'true', 'id' => 'trans_code')) }}<br/>

            {{ Form::label('name', 'Product') }} 
            {{ Form::text('name',old('name'), array('class' => 'form-control', 'placeholder' => 'Product*', 'required' => 'true', 'id' => 'name', 'disabled' => 'disabled')) }}
            {{ Form::hidden('product_id',old('product_id'), array('required' => 'true', 'id' => 'product_id')) }}<br/>

            {{ Form::label('project', 'Project') }} 
            {{ Form::text('project',old('project'), array('class' => 'form-control', 'placeholder' => 'Project*', 'required' => 'true', 'id' => 'project', 'disabled' => 'disabled')) }}<br>
 
            <br/>
            <table id="itemTable" class="table table-striped dataTable">
              <thead>
                <tr> 
                  <th>#</th>
                  <th>Name</th>
                  <th>Use</th>  
                  <th></th> 
                  <th>Waste</th> 
                  <th></th> 
                </tr>
              </thead>
              <tbody>
               @foreach($tally_detail as $key => $tally_details)
               <tr>
                <td>
                  {{$key+1}}</td>  
                  <td> 
                    {{$tally_details->name}}
                  </td>  
                  <td> 
                    {{ Form::number('tally['.$tally_details->id.'][weight]', old($tally_details->id.'[weight]', 0), ['class' => 'form-control', 'id' => 'weight_'.$tally_details->id, 'min' => '0']) }} 
                  </td> 
                  <td> 
                      <button type="button" class="btn btn-primary btn-sm" id="btn_add_{{$tally_details->id}}" onclick="add_{{$tally_details->id}}();">
                       <i class="fa fa-plus"></i>
                     </button> 
                  <button type="button"  class="btn btn-warning btn-sm" id="btn_reduce_{{$tally_details->id}}" onclick="reduce_{{$tally_details->id}}();"><i class="fa fa-minus"></i>
                     </button>
                  </td> 
                  <td> 
                    {{ Form::number('tally['.$tally_details->id.'][waste]', old($tally_details->id.'[waste]', 0), ['class' => 'form-control', 'id' => 'waste_'.$tally_details->id, 'min' => '0']) }} 
                  </td> 
                  <td> 
                      <button type="button" class="btn btn-primary btn-sm" id="btn_add_{{$tally_details->id}}" onclick="add_waste_{{$tally_details->id}}();">
                       <i class="fa fa-plus"></i>
                     </button> 
                  <button type="button"  class="btn btn-warning btn-sm" id="btn_reduce_{{$tally_details->id}}" onclick="reduce_waste_{{$tally_details->id}}();"><i class="fa fa-minus"></i>
                     </button>
                  </td> 
                </tr>

                 <script type="text/javascript">  
                function reduce_{{$tally_details->id}}() { 
                  var weight_{{$tally_details->id}} = document.getElementById('weight_{{$tally_details->id}}').value; 
                  document.getElementById('weight_{{$tally_details->id}}').value = parseInt(weight_{{$tally_details->id}})-parseInt(1); 
                };

                function add_{{$tally_details->id}}() {  
                  var weight_{{$tally_details->id}} = document.getElementById('weight_{{$tally_details->id}}').value; 
                  document.getElementById('weight_{{$tally_details->id}}').value = parseInt(weight_{{$tally_details->id}})+parseInt(1); 
                }; 

                function reduce_waste_{{$tally_details->id}}() { 
                  var waste_{{$tally_details->id}} = document.getElementById('waste_{{$tally_details->id}}').value; 
                  document.getElementById('waste_{{$tally_details->id}}').value = parseInt(waste_{{$tally_details->id}})-parseInt(1); 
                };

                function add_waste_{{$tally_details->id}}() {  
                  var waste_{{$tally_details->id}} = document.getElementById('waste_{{$tally_details->id}}').value; 
                  document.getElementById('waste_{{$tally_details->id}}').value = parseInt(waste_{{$tally_details->id}})+parseInt(1); 
                }; 

                </script>
                @endForeach
              </tbody>
            </table> 
            <hr>
            <button type="button" id="btn_confirm" data-toggle="modal" data-target="#modal_confirm" class="btn btn-success pull-right"><i class="fa fa-check"></i> Save</button>
            <a href="{{ URL::route('tally.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>  
            <br>  
          </div>  
        </div>
      </div> 
      {!! Form::close() !!} 
  </section>
  @stop

  @section('modal')
  <div class="modal fade" id="modal_confirm">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="summary_cashbond_payroll_date"> Submit this form?</h4>
        </div> 
       <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
        <button type="button" id="btn_submit" onclick="submit_form();" class="btn btn-success"><i class="fa fa-check"></i> Submit</button>
      </div>
    </div>
  </div>
</div>
@stop

<script>
  function submit_form ()
  {  
       $('#form_tally').submit(); 
  }; 
 
$(function() {
    console.log( "ready!" );
});

</script>


 

