@extends('layouts.home.template')
@section('content')
<div class="header_menu mobile-hide">
  <a href="{{ URL::route('tally.index') }}">MASTER TALLY</a>
</div> 
<section class="content">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
    <h4><b>DEHEADING</b></h4>
    <hr>
        
      <div class="col-md-6">
        <div class="x_panel"> 
          <div class="x_content">  

           {{ Form::label('trans_code', 'Trans Code') }} 
           {{ Form::text('trans_code_sw',old('trans_code', $purchase->trans_code), array('class' => 'form-control', 'placeholder' => 'Trans Code*', 'required' => 'true', 'id' => 'trans_code', 'disabled' => 'disabled')) }}
           {{ Form::hidden('trans_code',old('trans_code'), array('required' => 'true', 'id' => 'trans_code')) }}<br/>

           {{ Form::label('name', 'Product') }} 
           {{ Form::text('name',old('name'), array('class' => 'form-control', 'placeholder' => 'Product*', 'required' => 'true', 'id' => 'name', 'disabled' => 'disabled')) }}
           {{ Form::hidden('product_id',old('product_id'), array('required' => 'true', 'id' => 'product_id')) }}<br/>

             <table id="itemTable" class="table table-striped dataTable">
          <thead>
            <tr> 
              <th>#</th>
              <th>Name</th>
              <th>Use</th> 
              <th>Waste</th>
            </tr>
          </thead>
          <tbody>
            @foreach($tally_detail_view as $key => $tally_detail_views)
            <tr>
              <td>{{$key+1}}</td>
              <td>{{$tally_detail_views->name}}</td>
              <td>{{$tally_detail_views->weight}}</td>
              <td>{{$tally_detail_views->waste}}</td>
            </tr>
            @endForeach
          </tbody>
        </table>  
        <hr>
         <a href="{{ URL::route('tally.historydeheading', $purchase->id) }}" class="btn btn-default btn-lg pull-right" style="margin-right: 10px"><i class="fa fa-note"></i> History</a>
        <a href="{{ URL::route('tally.index') }}" class="btn btn-default pull-right btn-lg" style="margin-right: 10px"><i class="fa fa-close"></i> Exit</a> 
      <br>  
    </div>  
  </div>
</div>  
</div> 
</section>
@stop
 
