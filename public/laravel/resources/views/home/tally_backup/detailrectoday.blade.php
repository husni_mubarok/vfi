@extends('layouts.home.template')
@section('content')
<style type="text/css">
  .modal {
    text-align: center;
    padding: 0!important;
  }

  .modal:before {
    content: '';
    display: inline-block;
    height: 100%;
    vertical-align: middle;
    margin-right: -4px;
  }

  .modal-dialog {
    display: inline-block;
    text-align: left;
    vertical-align: middle;
  }
</style>
<div class="header_menu mobile-hide">
  <a href="{{ URL::route('tally.index') }}">RECEIVING TODAY</a>
</div> 
<section class="content">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      {!! Form::model($purchase, array('route' => ['tally.closerectoday', $purchase->id], 'method' => 'PUT', 'class'=>'update', 'id' => 'form_tally'))!!} 
      <div class="col-md-4">
        <div class="x_panel"> 
          <div class="x_content">   

            {{ Form::label('trans_code', 'Trans Code') }} 
            {{ Form::text('trans_code_sw',old('trans_code', $purchase->trans_code), array('class' => 'form-control', 'placeholder' => 'Trans Code*', 'required' => 'true', 'id' => 'trans_code', 'disabled' => 'disabled')) }}
            {{ Form::hidden('trans_code',old('trans_code'), array('required' => 'true', 'id' => 'trans_code')) }}<br/>

            {{ Form::label('name', 'Product') }} 
            {{ Form::text('name',old('name'), array('class' => 'form-control', 'placeholder' => 'Product*', 'required' => 'true', 'id' => 'name', 'disabled' => 'disabled')) }}
            {{ Form::hidden('product_id',old('product_id'), array('required' => 'true', 'id' => 'product_id')) }}<br/>

            @if($purchase->tally_type=='RECEIVING') 
            <button type="button" class="btn btn-primary" id="btn_receiving" onclick="set_btn_receiving_active();" style="margin-right: 10px">&nbsp;RECEIVING</button>
            @else
            <button type="button" class="btn btn-default" id="btn_receiving" onclick="set_btn_receiving_active();" style="margin-right: 10px">&nbsp;RECEIVING</button>
            @endif

            @if($purchase->tally_type=='TREATMENT') 
            <button type="button" class="btn btn-primary" id="btn_treatment" onclick="set_btn_treatment_active();">&nbsp;TREATMENT</button>
            @else
            <button type="button" class="btn btn-default" id="btn_treatment" onclick="set_btn_treatment_active();">&nbsp;TREATMENT</button>
            @endif
            <br/><br/>
            {{ Form::hidden('tally_type',old('tally_type'), array('required' => 'true', 'id' => 'tally_type')) }}
            <table id="itemTable" class="table table-striped dataTable">
              <thead>
                <tr> 
                  <th>#</th>
                  <th>Size</th>
                  <th>Weight</th> 
                  <th>Action</th> 
                </tr>
              </thead>
              <tbody>
               @foreach($tally_detail as $key => $tally_details)
               <tr>
                <td>
                  {{$key+1}}</td>  
                  <td> 
                    {{$tally_details->note}}
                  </td>  
                  <td> 
                    {{ Form::number('tally['.$tally_details->id.'][weight]', old($tally_details->id.'[weight]', $tally_details->weight), ['class' => 'form-control', 'id' => 'weight_'.$tally_details->id, 'min' => '0']) }} 
                  </td> 
                  <td> 
                      <button type="button" class="btn btn-primary btn-sm" id="btn_add_{{$tally_details->id}}" onclick="add_{{$tally_details->id}}();">
                       <i class="fa fa-plus"></i>
                     </button> 
                  <button type="button"  class="btn btn-warning btn-sm" id="btn_reduce_{{$tally_details->id}}" onclick="reduce_{{$tally_details->id}}();"><i class="fa fa-minus"></i>
                     </button>
                  </td> 
              </tr>

              <script type="text/javascript">  
                function reduce_{{$tally_details->id}}() { 
                  var weight_{{$tally_details->id}} = document.getElementById('weight_{{$tally_details->id}}').value; 
                  document.getElementById('weight_{{$tally_details->id}}').value = parseInt(weight_{{$tally_details->id}})-parseInt(1); 
                };

                function add_{{$tally_details->id}}() {  
                  var weight_{{$tally_details->id}} = document.getElementById('weight_{{$tally_details->id}}').value; 
                  document.getElementById('weight_{{$tally_details->id}}').value = parseInt(weight_{{$tally_details->id}})+parseInt(1); 
                };

                // document.getElementById("weight_{{$tally_details->id}}").disabled = true;
                // document.getElementById("btn_reduce_{{$tally_details->id}}").style.display = "none";
                // document.getElementById("btn_add_{{$tally_details->id}}").style.display = "none";

                // function checkbox_{{$tally_details->id}}() {   

                // var reduce = document.getElementById('btn_reduce_{{$tally_details->id}}');
                // reduce.style.display='block';

                // var add = document.getElementById('btn_add_{{$tally_details->id}}');
                // add.style.display='block';

                //     document.getElementById("weight_{{$tally_details->id}}").disabled = false; 
                //   }; 

                </script>
                @endForeach
              </tbody>
            </table>  
            <button type="button" id="btn_confirm" data-toggle="modal" data-target="#modal_confirm" class="btn btn-success pull-right"><i class="fa fa-check"></i> Close</button>
            <a href="{{ URL::route('tally.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Exit</a>  
            <br>  
          </div>  
        </div>
      </div> 
      {!! Form::close() !!}

      <div class="col-md-3">
        <h4>{{$purchase->tally_type}}</h4>
        <hr>
        <div class="x_panel"> 
          <div class="x_content">  
            <div class="div_overflow">
              <table id="itemTable" class="table table-striped dataTable">
                <thead>
                  <tr> 
                    <th>#</th>
                    <th>Size</th>
                    <th>Weight</th> 
                  </tr>
                </thead>
                <tbody>
                  @foreach($tally_detail as $key => $tally_details)
                  <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$tally_details->note}}</td>
                    <td>{{$tally_details->weight}}</td>
                  </tr>
                  @endForeach
                </tbody>
              </table>
            </div> 
          </div>  
        </div> 
      </div>
    </div> 
  </section>
  @stop

  @section('modal')
  <div class="modal fade" id="modal_confirm">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="summary_cashbond_payroll_date">Close this form? </h4>
        </div> 
         
       <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
        <button type="button" id="btn_submit" onclick="submit_form();" class="btn btn-success"><i class="fa fa-check"></i> Close</button>
      </div>
    </div>
  </div>
</div>
@stop

<script>

  function checkbox_toggle(id)
  {
    if($('#checkbox_'+id).is(":checked"))
    {
      $('#weight_'+id).attr('disabled', false); 
    } else {
      $('#weight_'+id).val('');
      $('#weight_'+id).attr('disabled', true); 
    }
  };



  function submit_form ()
  {
    console.log("asd");
    $('#form_tally').submit();
  };

  function set_btn_receiving_active()
  { 
    $("#btn_receiving").removeClass("btn btn-default").addClass("btn btn-primary");
    $("#btn_treatment").removeClass("btn btn-primary").addClass("btn btn-default");

    document.getElementById ("tally_type").value = "RECEIVING";

  };
  function set_btn_treatment_active()
  { 
    $("#btn_treatment").removeClass("btn btn-default").addClass("btn btn-primary");
    $("#btn_receiving").removeClass("btn btn-primary").addClass("btn btn-default");

    document.getElementById ("tally_type").value = "TREATMENT";

  };
</script> 

<script> 
  jQuery.each({!! $tally_detail !!}, function(key, value)
  {
    $('#checkbox_'+value['size_id']).attr('checked', true);
    checkbox_toggle(value['size_id']);
    $('#weight_'+value['size_id']).val(value['weight']);
  });
</script>
