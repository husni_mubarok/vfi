@extends('layouts.home.template')
@section('content')
<div class="header_menu mobile-hide">
	<a href="{{ URL::route('purchase.index') }}">PURCHASE</a>
</div>
<section class="content">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<div class="x_panel">
					<h2>
						<i class="fa fa-shopping-cart"></i>&nbsp;MASTER TALLY
					</h2>
					<hr>
					<a href="{{ URL::route('tally.index') }}" class="btn btn-warning btn-md">OUTSTANDING</a>&nbsp;<a href="{{ URL::route('tally.bank') }}" class="btn btn-success btn-md">BANK</a>
					<hr>
					<div class="x_content">
						@foreach($project_list as $project)
						<button type="button" class="btn btn-default btn_project" value="{{$project}}">{{$project}}</button>
						@endforeach

						<table class="table table-hover table-striped" id="index_table">
							<thead>
								<tr>
									<th width="5%">No.</th>
									<th>Code</th>
									<th>Production Code</th>  
									<th>Supplier</th>
									<th>Product</th> 
									@actionStart('purchase', 'delete')
									<th></th>
									@actionEnd
									<th>Form</th>
								</tr>
							</thead>
							<tbody id="table_body">
								{{-- Content by Ajax --}}
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@stop



@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script> 

 $(document).ready(function () {
    $("#index_table").DataTable(
    	{
    		"scrollX": true
    	});
    });

	$('.btn_project').on('click', function()
	{
		var button = $(this);
		$('.btn_project').removeClass('active').attr('disabled', true);
		$('#index_table').DataTable().destroy();
		$('#table_body').empty().append('<tr><td colspan="9" align="center">Loading Data...</td></tr>');
		$.ajax({
			url: '{{URL::route('get.purchase_by_project')}}', 
			data: {'project': $(this).val()}, 
			type: 'GET', 
			headers : {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
			success: function(data, textStatus, jqXHR)
			{
				var new_row = '';
				jQuery.each(data, function(i, val)
				{
					var url_summary = '{{ URL::route('purchase.summary', 'id') }}';
					url_summary = url_summary.replace('id', val['id']);
					var url_delete = '{{ URL::route('purchase.delete', 'id') }}';
					url_delete = url_delete.replace('id', val['id']);

					//RECEIVING
					var url_create = '{{ URL::route('tally.create', 'id') }}';
					url_create = url_create.replace('id', val['id']);

					var url_detail = '{{ URL::route('tally.detail', 'id') }}';
					url_detail = url_detail.replace('id', val['id']);

					var url_view = '{{ URL::route('tally.view', 'id') }}';
					url_view = url_view.replace('id', val['id']);

					//DEHEADING
					var url_create_deheading = '{{ URL::route('tally.createdeheading', 'id') }}';
					url_create_deheading = url_create_deheading.replace('id', val['id']);

					var url_detail_deheading = '{{ URL::route('tally.detaildeheading', 'id') }}';
					url_detail_deheading = url_detail_deheading.replace('id', val['id']);

					var url_view_deheading = '{{ URL::route('tally.viewdeheading', 'id') }}';
					url_view_deheading = url_view_deheading.replace('id', val['id']);

					//PEELING
					var url_create_peeling = '{{ URL::route('tally.createpeeling', 'id') }}';
					url_create_peeling = url_create_peeling.replace('id', val['id']);

					var url_detail_peeling = '{{ URL::route('tally.detailpeeling', 'id') }}';
					url_detail_peeling = url_detail_peeling.replace('id', val['id']);

					var url_view_peeling = '{{ URL::route('tally.viewpeeling', 'id') }}';
					url_view_peeling = url_view_peeling.replace('id', val['id']);

					//GRADING
					var url_create_grading = '{{ URL::route('tally.creategrading', 'id') }}';
					url_create_grading = url_create_grading.replace('id', val['id']);

					//PACKING
					var url_create_packing = '{{ URL::route('tally.createpacking', 'id') }}';
					url_create_packing = url_create_packing.replace('id', val['id']); 

					var url_view_packing = '{{ URL::route('tally.viewpacking', 'id') }}';
					url_view_packing = url_view_packing.replace('id', val['id']);

					new_row += '<tr>';
					new_row += '<td>';
					new_row += i+1;
					new_row += '</td>';
					new_row += '<td>';
					new_row += '<a href="#" class="btn btn-default btn-sm">';
					new_row += val['doc_code'];
					new_row += '</a>';
					new_row += '</td>'; 
					new_row += '<td>';
					new_row += val['trans_code'];
					new_row += '</td>'; 
					new_row += '<td>';
					new_row += val['purchase_supplier']['supplier']['name'];
					new_row += '</td>';
					new_row += '<td>';
					new_row += val['purchase_product']['product']['name'];
					new_row += '</td>'; 
					new_row += '<td>';
					new_row += '</td>'; 
					new_row += '<td>';
 
					if(val['form_status']>0){ 	
					new_row += '<a href="'+url_view+'" type="button" class="btn btn-primary btn-sm">';
					new_row += '<i class="fa fa-search"></i> Receiving</a>&nbsp;';
					}else if(val['form_status']==0 && val['action_form_status']=='Edit'){ 
					new_row += '<a href="'+url_detail+'" type="button" class="btn btn-primary btn-sm">';
					new_row += '<i class="fa fa-pencil"></i> Receiving</a>&nbsp;';
					}else if(val['form_status']==0 || val['form_status']==null){ 
					new_row += '<a href="'+url_create+'" type="button" class="btn btn-primary btn-sm">';
					new_row += '<i class="fa fa-plus"></i> Receiving</a>&nbsp;';
					};

					if(val['form_status']>1){ 	
					new_row += '<a href="'+url_view_deheading+'" type="button" class="btn btn-primary btn-sm">';
					new_row += '<i class="fa fa-search"></i> Heading</a>&nbsp;';
					}else if(val['form_status']==1 && val['action_form_status']=='Edit'){ 
					new_row += '<a href="'+url_detail_deheading+'" type="button" class="btn btn-primary btn-sm">';
					new_row += '<i class="fa fa-pencil"></i> Heading</a>&nbsp;';
					}else if(val['form_status']==1 && val['action_form_status']=='Done'){ 
					new_row += '<a href="'+url_create_deheading+'" type="button" class="btn btn-primary btn-sm">';
					new_row += '<i class="fa fa-plus"></i> Heading</a>&nbsp;';
					};

				 	if(val['form_status']>2){ 	
					new_row += '<a href="'+url_view_peeling+'" type="button" class="btn btn-primary btn-sm">';
					new_row += '<i class="fa fa-search"></i> Peeling</a>&nbsp;';
					}else if(val['form_status']==2 && val['action_form_status']=='Edit'){ 
					new_row += '<a href="'+url_detail_peeling+'" type="button" class="btn btn-primary btn-sm">';
					new_row += '<i class="fa fa-pencil"></i> Peeling</a>&nbsp;';
					}else if(val['form_status']==2 && val['action_form_status']=='Done'){ 
					new_row += '<a href="'+url_create_peeling+'" type="button" class="btn btn-primary btn-sm">';
					new_row += '<i class="fa fa-plus"></i> Pelling</a>&nbsp;';
					};

					if(val['form_status']>3){ 	
					new_row += '<a href="'+url_create_grading+'" type="button" class="btn btn-primary btn-sm">';
					new_row += '<i class="fa fa-search"></i> Grading</a>&nbsp;';
					}else if(val['form_status']==3 && val['action_form_status']=='Edit'){ 
					new_row += '<a href="'+url_create_grading+'" type="button" class="btn btn-primary btn-sm">';
					new_row += '<i class="fa fa-pencil"></i> Grading</a>&nbsp;';
					}else if(val['form_status']==3 && val['action_form_status']=='Done'){ 
					new_row += '<a href="'+url_create_grading+'" type="button" class="btn btn-primary btn-sm">';
					new_row += '<i class="fa fa-plus"></i> Grading</a>&nbsp;';
					};


					if(val['form_status']>4){ 	
					new_row += '<a href="'+url_view_packing+'" type="button" class="btn btn-primary btn-sm">';
					new_row += '<i class="fa fa-search"></i> Packing</a>&nbsp;'; 
					}else if(val['form_status']==4 && val['action_form_status']=='Done'){ 
					new_row += '<a href="'+url_create_packing+'" type="button" class="btn btn-primary btn-sm">';
					new_row += '<i class="fa fa-plus"></i> Packing</a>&nbsp;';
					};

					 

					new_row += '</td>';
					new_row += '</tr>';
				});

				$('#table_body').empty().append(new_row);
				$("#index_table").DataTable();
				$('.btn_project').removeClass('active').attr('disabled', false);
				button.addClass('active');
			}, 
			error: function()
			{
				//$('.btn_project').removeClass('active').attr('disabled', false);
				alert('Gagal menarik data!');
			}
		});



	});
</script>

<script>
  $(document).ready(function () {
    $("#index_table").DataTable(
    	{
    		"scrollX": true
    	});
    });
</script>

@stop