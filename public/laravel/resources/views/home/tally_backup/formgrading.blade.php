@extends('layouts.home.template')
@section('content')
<style type="text/css">
  .modal {
    text-align: center;
    padding: 0!important;
  }

  .modal:before {
    content: '';
    display: inline-block;
    height: 100%;
    vertical-align: middle;
    margin-right: -4px;
  }

  .modal-dialog {
    display: inline-block;
    text-align: left;
    vertical-align: middle;
  }
</style>
<div class="header_menu mobile-hide">
  <a href="{{ URL::route('tally.index') }}">WIP</a>
</div> 
<section class="content">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      {!! Form::model($purchase, array('route' => ['tally.storegrading', $purchase->id], 'method' => 'PUT', 'class'=>'update', 'id' => 'form_grading'))!!} 
      <div class="col-md-4">
        <div class="x_panel"> 
          <div class="x_content">
      
      <table style="width:100%">
      <tr><td>{{ Form::label('trans_code', 'Production Code') }}</td></tr>
      <tr><td><input type="text" class="form-control" value="{{$purchase->trans_code}}" disabled="disabled"></td></tr>
      <tr><td><br>{{ Form::label('name', 'Product') }}</td></tr>
            <tr><td><input type="text" class="form-control" value="{{$purchase->name}}" disabled="disabled"></td></tr>
            <tr><td><br>{{ Form::label('project', 'Project') }} </td></tr>
            <tr><td><input type="text" class="form-control" value="{{$purchase->project}}" disabled="disabled"></td></tr>
            <tr><td><br>{{ Form::label('qty_type', 'Qty Type') }}</td></tr>
            <tr>
              <td>
          @foreach($qty_types as $key => $qty_type)
            <button type="button" class="btn btn-default qty_type" id="btn_qty_type{{$key}}" value="{{$qty_type->id}}" onclick="set_btn_qty_type_active{{$key}}();">{{$qty_type->name}}</button>
          @endforeach
          <input type="hidden" id="qty_type" name="qty_type">
              </td>
            </tr>
            <tr><td><br>{{ Form::label('grade', 'Grade') }}</td></tr>
            <tr>
              <td>
          @foreach($grades as $key => $grade)
            <button type="button" class="btn btn-default grade" id="btn_grade{{$key}}" value="{{$grade->id}}" onclick="set_btn_grade_active{{$key}}();">{{$grade->name}}</button>
          @endforeach
          <input type="hidden" id="grade_id" name="grade_id">
              </td>
            </tr>
            <tr><td><br>{{ Form::label('code_size', 'Code Size') }}</td></tr>
            <tr>
              <td>
          <select name="code_size_id" class="form-control" id="code_size_id">
            <option value="" selected disabled>-Choose-</option>
            @foreach($code_sizes as $key => $code_size)
              <option value="{{$code_size->id}}" >{{$code_size->name}} / {{$code_size->note}} / {{$code_size->freezing_name}} / {{$code_size->block_weight}} Kg</option>
            @endforeach
          </select>
              </td>
            </tr>
          <tr><td><br>{{ Form::label('total_block', 'Total Block') }}</td></tr>
          <tr>
            <td><input type="number" placeholder="0" name="total_block" id="total_block" class="form-control" value="0"></td>
            <td align="center">
          <button type="button" class="btn btn-primary btn-sm" onclick="block_add();"><i class="fa fa-plus"></i></button>
          <button type="button" class="btn btn-warning btn-sm" onclick="block_reduce();"><i class="fa fa-minus"></i></button>
            </td>
          </tr>
          <tr><td><br>{{ Form::label('rest', 'Rest') }}</td></tr>
          <tr>
            <td><input type="number" placeholder="0" name="rest" id="rest" class="form-control" value="0"></td>
            <td align="center">
          <button type="button" class="btn btn-primary btn-sm" onclick="rest_add();"><i class="fa fa-plus"></i></button>
          <button type="button" class="btn btn-warning btn-sm" onclick="rest_reduce();"><i class="fa fa-minus"></i></button>
            </td>
          </tr>
          <tr>
            <td><br>
          @if(($purchase->form_status == 3 && $purchase->action_form_status == 'Done') || ($purchase->form_status == 4 && $purchase->action_form_status == 'Edit'))
            <button type="button" id="btn_confirm" data-toggle="modal" data-target="#modal_confirm" class="btn btn-success pull-right"><i class="fa fa-check"></i> Add</button>
          @endif
            </td>
          </tr>
      </table>

          </div>  
        </div>
      </div> 
    {!! Form::close() !!} 

  <div class="col-md-8">
    <div class="x_panel"> 
      <div class="x_content">  
        <h2>Grading & Sizing</h2>
        <hr>
        <h3>Summary</h3>
        <table id="itemTable" class="table table-striped dataTable">
        <thead>
        <tr>
          <th>Qty Type</th>
          <th>Grade</th>
          <th>Size</th>
          <th>Freezing</th>  
          <th>Code Size</th>
          <th>Block Weight (Kg)</th>
          <th>Total Block</th>
          <th>Rest</th>
        </tr>
        </thead>
        <tbody>
          @foreach($grading_view as $key => $view_grading)
            <tr>
              <td>{{$view_grading->qty_name}}</td>
              <td>{{$view_grading->grade}}</td>
              <td>{{$view_grading->size}}</td>
              <td>{{$view_grading->freezing}}</td>
              <td>{{$view_grading->code_size}}</td>
              <td>{{$view_grading->block}}</td>
              <td>{{$view_grading->total_block}}</td>
              <td>{{$view_grading->rest}}</td>
            </tr>
          @endforeach
        </tbody>
        </table>
        <hr>
        <h3>History</h3>
        <table id="itemTable" class="table table-striped dataTable">
        <thead>
        <tr>
          <th>Qty Type</th>
          <th>Grade</th>
          <th>Size</th>
          <th>Freezing</th>  
          <th>Code Size</th>
          <th>Block Weight (Kg)</th>
          <th>Total Block</th>
          <th>Rest</th>
        </tr>
        </thead>
        <tbody>
          @foreach($grading_history as $key => $history_grading)
            <tr>
              <td>{{$history_grading->qty_name}}</td>
              <td>{{$history_grading->grade}}</td>
              <td>{{$history_grading->size}}</td>
              <td>{{$history_grading->freezing}}</td>
              <td>{{$history_grading->code_size}}</td>
              <td>{{$history_grading->block}}</td>
              <td>{{$history_grading->total_block}}</td>
              <td>{{$history_grading->rest}}</td>
            </tr>
          @endforeach
        </tbody>
        </table>
      </div>
    </div>
    @if(($purchase->form_status == 3 && $purchase->action_form_status == 'Done') || ($purchase->form_status == 4 && $purchase->action_form_status == 'Edit'))
      <br>
      {!! Form::model($purchase, array('route' => ['tally.closegrading', $purchase->id], 'method' => 'PUT', 'class'=>'update', 'id' => 'form_grading_close'))!!}
      <button type="button" id="btn_confirm" data-toggle="modal" data-target="#modal_confirm_close" class="btn btn-danger pull-left"><i class="fa fa-lock"></i> Close</button>
      {!! Form::close() !!}
    @endif
  </div>
</section>
@stop

@section('modal')
<div class="modal fade" id="modal_confirm">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="modal_grading"> Are you sure want to add?</h4>
      </div> 
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> No</button>
        <button type="button" id="btn_submit" onclick="submit_form();" class="btn btn-success"><i class="fa fa-check"></i> Yes</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_confirm_close">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="modal_grading"> Are you sure want to close?</h4>
      </div> 
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal"><i class="fa fa-remove"></i> No</button>
        <button type="button" id="btn_submit" onclick="submit_form_close();" class="btn btn-danger"><i class="fa fa-check"></i> Yes</button>
      </div>
    </div>
  </div>
</div>
@stop

<script>
// function block_weight_add() {$('#block_weight').val(parseInt($('#block_weight').val())+parseInt(1));};
// function block_weight_reduce() {$('#block_weight').val(parseInt($('#block_weight').val())-parseInt(1));};

function block_add() {$('#total_block').val(parseInt($('#total_block').val())+parseInt(1));};
function block_reduce() {$('#total_block').val(parseInt($('#total_block').val())-parseInt(1));};

function rest_add() {$('#rest').val(parseInt($('#rest').val())+parseInt(1));};
function rest_reduce() {$('#rest').val(parseInt($('#rest').val())-parseInt(1));};

@foreach($qty_types as $key => $qty_type)
  function set_btn_qty_type_active{{$key}}()
  { 
    $(".qty_type").removeClass("btn btn-primary qty_type").addClass("btn btn-default qty_type");

    $("#qty_type").val(document.getElementById ("btn_qty_type{{$key}}").value);

    // console.log($("#qty_type").val());

    $("#btn_qty_type{{$key}}").removeClass("btn btn-default qty_type").addClass("btn btn-primary qty_type");
  }
@endforeach

@foreach($grades as $key => $grade)
  function set_btn_grade_active{{$key}}()
  { 
    $(".grade").removeClass("btn btn-primary grade").addClass("btn btn-default grade");

    $("#grade_id").val(document.getElementById ("btn_grade{{$key}}").value);

    // console.log($("#grade_id").val());

    $("#btn_grade{{$key}}").removeClass("btn btn-default grade").addClass("btn btn-primary grade");
  }
@endforeach

function submit_form()
{
  var grade_id = document.getElementById("grade_id").value;
  var code_size_id = document.getElementById("code_size_id").value;
  // var block_weight = document.getElementById("block_weight").value;
  var qty_type_id = document.getElementById("qty_type").value;

  if(qty_type_id == '') {alert("Qty Type is required!");}
  else if(grade_id == '') {alert("Grade is required!");}
  else if(code_size_id == '') {alert("Code Size is required!");}
  // else if(block_weight == '') {alert("Block Weight is required!");}
  else {$('#form_grading').submit();}
};

function submit_form_close()
{ 
  $('#form_grading_close').submit();
}; 
</script>