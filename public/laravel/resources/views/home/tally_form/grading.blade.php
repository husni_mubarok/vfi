@extends('layouts.tabletform.template')
<style type="text/css">
  .btn-big{
    padding: 0px;
    height: 120px;
    vertical-align: middle;
    font-size: 3vw !important;
    white-space: initial !important;
  }
</style>

<meta name="_token" content="{{ csrf_token() }}"/>
<div id="all_element">
    <section class="myheader pink">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-4 heading">
                <h1>GRADING</h1>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
                <section class="info">
                       <a href=""><span class="no_label active">{{$temp_tally->trans_code}}<p><i>34 : 13 Minutes</i></p></span></a>
                       <a href=""><span class="no_label">{{$temp_tally->trans_code}}<p><i>34 : 13 Minutes</i></p></span></a>
                       <a href=""><span class="no_label disabled"><i class="fa fa-lock"></i>&nbsp;&nbsp;{{$temp_tally->trans_code}}<p><i>34 : 13 Minutes</i></p></span></a>
            </section>
            </div>
        </div>
    </section>
    <section class="myheader pink float">
        <div class="row">
            <div class="col-md-8 col-sm-8">
                <ul>
                   <!--  <li>
                       {!! Form::open(array('route' => ['tally_form.closegrading', $temp_tally->id], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_close'))!!}
                        {{ csrf_field() }}   
                            <a class="black_red" id="btn_close" onclick="storeclose();" title="CLOSE">
                                <i class="fa fa-unlock"></i>
                                <p>CLOSE</p>
                            </a>
                        {!! Form::close() !!}   
                    </li> -->
                    <li>
                        <a class="HOME" href="{{ URL::route('tally_form.index') }}">
                            <i class="fa fa-home"></i>
                            <p>HOME</p>
                        </a>
                    </li>
                    <li>
                        <a class="" title="HISTORY" href="{{ URL::route('tally_form.historygrading', $temp_tally->id) }}">
                            <i class="fa fa-file-text-o"></i>
                             <p>HSTRY</p>
                        </a>
                    </li>
                    <li>
                       
                        <a class="back_div_qtytype"  id="back_div_qtytype"  style="display: none">
                            <img src="{{Config::get('constants.path.img')}}/icon/back.svg" style="height: 35px;margin-bottom:0px;">
                            <p style="margin-top:5px;">BACK</p>
                        </a>
                        <a class="back_text_grade"  id="back_text_grade" style="display: none">
                            <img src="{{Config::get('constants.path.img')}}/icon/back.svg" style="height: 35px;margin-bottom:0px;">
                            <p style="margin-top:5px;">BACK</p>
                        </a>
                        <a class="back_text_code_size"  id="back_text_code_size" style="display: none">
                            <img src="{{Config::get('constants.path.img')}}/icon/back.svg" style="height: 35px;margin-bottom:0px;">
                            <p style="margin-top:5px;">BACK</p>
                        </a>
                        <a class="back_text_rest"  id="back_text_rest" style="display: none">
                            <img src="{{Config::get('constants.path.img')}}/icon/back.svg" style="height: 35px;margin-bottom:0px;">
                            <p style="margin-top:5px;">BACK</p>
                        </a>
                        <a class="back_text_total_block"  id="back_text_total_block" style="display: none">
                            <img src="{{Config::get('constants.path.img')}}/icon/back.svg" style="height: 35px;margin-bottom:0px;">
                            <p style="margin-top:5px;">BACK</p>
                        </a>
                        <a class="back_collect"  id="back_collect" style="display: none">
                            <img src="{{Config::get('constants.path.img')}}/icon/back.svg" style="height: 35px;margin-bottom:0px;">
                            <p style="margin-top:5px;">BACK</p>
                        </a>

                       
                        

                    </li>
                </ul>
            </div>
        </div>
    </section>

  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12"> 
      <div class="x_panel"> 
        <div class="x_content">   
          <!-- <div class="col-md-4 col-sm-4 col-xs-4">
           <center><h1><b>GRADING</b></h1></center>
         </div>  -->
         <!-- <div class="col-md-4 col-sm-4 col-xs-4">
           <center><h1>{{$temp_tally->trans_code}}</h1></center>   
         </div>   -->
         <!-- {!! Form::open(array('route' => ['tally_form.closegrading', $temp_tally->id], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_close'))!!}
         {{ csrf_field() }}  
         <div class="col-md-1 col-sm-1 col-xs-1" style="padding: 5px">  
          <button type="button" id="btn_close" onclick="storeclose();" class="btn btn-danger btn-lg btn-block nomargin" style="padding: 10px 0;"><i class="fa fa-close faa-pulse animated"></i></button> 
        </div>
        {!! Form::close() !!}   

        <div class="col-md-1 col-sm-1 col-xs-1" style="padding: 5px">  
          <a href="{{ URL::route('tally_form.index') }}" type="button" class="btn btn-primary btn-lg btn-block nomargin"  style="padding: 10px 0;"><i class="fa fa-home faa-pulse animated"></i></a>
        </div> 
       
       <div class="col-md-1 col-sm-1 col-xs-1" style="padding: 5px">  
        <a href="{{ URL::route('tally_form.historygrading', $temp_tally->id) }}" type="button" class="btn btn-primary btn-lg pull-right btn-block nomargin" style="padding: 10px;"><i class="fa fa-file-text-o faa-pulse animated"></i></a>
      </div> 
       <div class="col-md-1 col-sm-1 col-xs-1" style="padding: 5px"> -->  

        <!-- <a id="back_div_qtytype"  style="display: none" href="#" type="button" class="btn btn-warning btn-lg back_div_qtytype" style="padding: 10px 0"><i class="fa fa-arrow-circle-left faa-pulse animated"></i></a>

        <a id="back_text_grade"  style="display: none" href="#" type="button" class="btn btn-warning btn-lg back_text_grade" style="padding: 10px 0;"><i class="fa fa-arrow-circle-left faa-pulse animated"></i></a>

        <a id="back_text_code_size"  style="display: none" href="#" type="button" class="btn btn-warning btn-lg back_text_code_size" style="padding: 10px 0;"><i class="fa fa-arrow-circle-left faa-pulse animated"></i></a>

        <a id="back_text_rest"  style="display: none" href="#" type="button" class="btn btn-warning btn-lg back_text_rest" style="padding: 10px 0;"><i class="fa fa-arrow-circle-left faa-pulse animated"></i><br/></a>

        <a id="back_text_total_block"  style="display: none" href="#" type="button" class="btn btn-warning btn-lg back_text_total_block" style="padding: 10px 0;"><i class="fa fa-arrow-circle-left faa-pulse animated"></i></a> 

        <a id="back_collect"  style="display: none" href="#" type="button" class="btn btn-warning btn-lg back_collect" style="padding: 10px 0;"><i class="fa fa-arrow-circle-left faa-pulse animated"></i></a> -->  
      </div>
    </div>
  </div>
</div> 


<form class="form-horizontal" role="form">
  {{ csrf_field() }}
  <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: -30px;"> 
    <div class="col-md-12">
      <div class="x_panel"> 
        <div class="x_content">   
          <br/> 

          <div id="div_qtytype" class="div_qtytype"> 

            <h3 class="float_breadcrumb">QTY TYPE</h3>
            @foreach($qty_types as $key => $qty_type)
            <div class="col-md-4 col-sm-4 col-xs-4 button ">
                <a class="btn btn-black qty_type" id="btn_qty_type{{$key}}" value="{{$qty_type->id}}" onclick="set_btn_qty_type_active{{$key}}();">
                    <img src="{{Config::get('constants.path.img')}}/icon/kg.svg">
                    <span id="{{$qty_type->name}}">{{$qty_type->name}}</span>
                </a>
                <a class="btn btn-block btn-close btn_collect this_collect"><i class="fa fa-unlock"></i>&nbsp;&nbsp;Close</a>
            </div>
            @endforeach
            <input type="hidden" id="qty_type" name="qty_type"> 

          </div>

           
          <div class="col-md-4 col-sm-4 col-xs-4 button btn_collect">
                <a class="btn btn-black btn_collect"  id="btn_collect">
                    <img src="{{Config::get('constants.path.img')}}/icon/collect.svg">
                    <span id="">COLLECT</span>
                    <a class="btn btn-block btn-close btn_collect this_collect"><i class="fa fa-unlock"></i>&nbsp;&nbsp;Close</a>
                </a>
            </div>

          <div id="text_grade_option" class="text_grade_option" style="display: none">
            <h3 class="float_breadcrumb">GRADE OPTION</h3>
            <div class="col-md-4 col-sm-4 col-xs-4" style="padding: 5px">
            <button type="button" class="btn btn-black btn-block grade" id="grade_option_none" value="grade_option_none" onclick="grade_option_none();" style="padding: 40px 0; font-size: 40px;">NONE</button>
            </div>   
            <div class="col-md-4 col-sm-4 col-xs-4" style="padding: 5px">
            <button type="button" class="btn btn-black btn-block grade" id="grade_option_bs" value="grade_option_bs" onclick="grade_option_bs();" style="padding: 40px; font-size: 40px;">BS</button>
            </div>  
          </div>

          <div id="text_grade" class="text_grade" style="display: none">
            <h3 class="float_breadcrumb">GRADE</h3> 
            @foreach($grades as $key => $grade)
            <div class="col-md-4 col-sm-4 col-xs-4" style="padding: 5px">
            <button type="button" class="btn btn-black btn-block grade" id="btn_grade{{$key}}" value="{{$grade->id}}" onclick="set_btn_grade_active{{$key}}();" style="padding: 40px; font-size: 40px;">{{$grade->name}}</button>
            </div>
            @endforeach
            <input type="hidden" id="grade_id" name="grade_id"> 
          </div>

          <div id="text_code_size" class="text_code_size" style="display: none">
            <h3 class="float_breadcrumb">CODE SIZE</h3>
            @foreach($code_sizes as $key => $code_size)
            <div class="col-md-4 col-sm-4 col-xs-4" style="padding: 5px">
            <button type="button" class="btn btn-black btn-block grade" id="btn_code_size{{$key}}" value="{{$code_size->id}}" onclick="set_btn_code_size_active{{$key}}();" style="padding: 40px; font-size: 25px;">{{$code_size->name}}</button>
            </div>
            @endforeach
            <input type="hidden" id="code_size_id" name="code_size_id"> 
            {{ Form::hidden('purchase_id',old('purchase_id', $temp_tally->id_purchase), array('required' => 'true', 'id' => 'purchase_id')) }} 
            {{ Form::hidden('temp_tally_id',old('temp_tally_id', $temp_tally->id), array('required' => 'true', 'id' => 'temp_tally_id')) }} 
            <br/> 
          </div> 
          <div id="text_total_block" class="text_total_block" style="display: none">
            <h3 class="float_breadcrumb">TOTAL BLOCK</h3>
            {{ Form::number('total_block',old('total_block'), array('class' => 'form-control', 'placeholder' => 'TOTAL BLOCK*', 'required' => 'true', 'id' => 'total_block', 'style' => 'height:70px; font-size:36px;')) }}<br>  
          </div>

          <a id="next_total_block" href="#" type="button" class="btn btn-primary btn-lg pull-right next_total_block" style="padding: 35px !important; font-size: 20px; display: none"><i class="fa fa-arrow-circle-right faa-pulse"></i><br/><p>NEXT</p></a>

          <div id="text_rest" class="text_rest" style="display: none">
            <h3 class="float_breadcrumb">REMAIN</h3>
            {{ Form::number('rest',old('rest'), array('class' => 'form-control', 'placeholder' => 'Remain*', 'required' => 'true', 'id' => 'rest', 'style' => 'height:70px; font-size:36px;')) }}<br>   
            <a href="#" class="btn btn-success pull-right btn-lg pull-right" onclick="store();" style="padding: 35px; font-size: 20px"> <i class="fa fa-check"></i> SAVE</a> 
          </div>

           <div id="text_collect" class="text_collect" style="display: none">
          <h3 class="float_breadcrumb">COLLECT</h3>
          {{ Form::number('collect',old('collect',0), array('class' => 'form-control', 'placeholder' => 'Collect*', 'required' => 'true', 'id' => 'collect', 'style' => 'height:70px; font-size:36px;')) }}<br>  
          

            <div class="row">
                <div class="col-md-8 col-sm-8 col-md-offset-2 col-sm-offset-2">
                    <br>
                    <a href="#" class="btn btn-success btn-lg btn-block" onclick="storecollect();"> <i class="fa fa-check"></i>&nbsp;&nbsp;SAVE</a>
                </div>
            </div> 
        </div>  
        </div>
      </div> 
    </div>
  </div>
</form>  
<div id="loader"></div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script type="text/javascript"> 
  function store()
  {  
   var qty_type = document.getElementById ("qty_type").value;
   var grade_id = document.getElementById ("grade_id").value;
   var code_size_id = document.getElementById ("code_size_id").value;
   var rest = document.getElementById ("rest").value;
   var total_block = document.getElementById ("total_block").value;
   if(qty_type=='' || grade_id=='' || code_size_id=='' || rest=='' || total_block=='')
   {
    swal("Error!", "All data is required!", "error")
  }
  else{ 
    showLoader();
    $.ajax({
      type: 'POST',
      url: "{{ URL::route('storegrading', $temp_tally->id) }}",
      data: {
        '_token': $('input[name=_token]').val(), 
        'qty_type': $('#qty_type').val(),
        'grade_id': $('#grade_id').val(),
        'code_size_id': $('#code_size_id').val(),
        'rest': $('#rest').val(),
        'total_block': $('#total_block').val(), 
        'purchase_id': $('#purchase_id').val(), 
        'temp_tally_id': $('#temp_tally_id').val()  
      },
      success: function(data) { 
        if ((data.errors)) {
         swal("Error!", "Gat data failed!", "error")
       } else {

        swal("Success!", "Data has been saved!", "success")
        hideLoader();
        document.getElementById ("qty_type").value = ""; 
        document.getElementById ("grade_id").value = ""; 
        document.getElementById ("code_size_id").value = ""; 
        document.getElementById ("rest").value = ""; 
        document.getElementById ("total_block").value = ""; 
        document.getElementById ("purchase_id").value = "";  

        $('.btn_collect').show(100);
        $('.text_code_size').hide(100);
        $('.text_grade').hide(100);
        $('.text_total_block').hide(100);
        $('.next_total_block').hide(100);
        $('.back_text_grade').hide(100);
        $('.back_text_rest').hide(100);
        $('.text_rest').hide(100);
        $('.back_text_total_block').hide(100);
        $('.back_text_code_size').hide(100);
        $('.div_qtytype').show(100);


        @foreach($qty_types as $key => $qty_type) 
        $(".qty_type").removeClass("btn btn-black qty_type").addClass("btn btn-black qty_type"); 
        $("#qty_type").val(document.getElementById ("btn_qty_type{{$key}}").value); 
        @endforeach

        @foreach($grades as $key => $grade) 
        $(".grade").removeClass("btn btn-black grade").addClass("btn btn-black grade"); 
        $("#grade_id").val(document.getElementById ("btn_grade{{$key}}").value); 
        @endforeach

        @foreach($code_sizes as $key => $code_size) 
        $(".code_size").removeClass("btn btn-black code_size").addClass("btn btn-black code_size"); 
        $("#code_size_id").val(document.getElementById ("btn_code_size{{$key}}").value); 
        @endforeach
      } 
    },

  })
  }
};
</script>

<script> 

  function block_add() {$('#total_block').val(parseInt($('#total_block').val())+parseInt(1));};
  function block_reduce() {$('#total_block').val(parseInt($('#total_block').val())-parseInt(1));};

  function rest_add() {$('#rest').val(parseInt($('#rest').val())+parseInt(1));};
  function rest_reduce() {$('#rest').val(parseInt($('#rest').val())-parseInt(1));};

  @foreach($qty_types as $key => $qty_type)
  function set_btn_qty_type_active{{$key}}()
  { 
    $(".qty_type").removeClass("btn btn-black qty_type").addClass("btn btn-black qty_type");

    $("#qty_type").val(document.getElementById ("btn_qty_type{{$key}}").value);

    // console.log($("#qty_type").val());

    $("#btn_qty_type{{$key}}").removeClass("btn btn-black qty_type").addClass("btn btn-black qty_type");
  }
  @endforeach

  @foreach($grades as $key => $grade)
  function set_btn_grade_active{{$key}}()
  { 
    $(".grade").removeClass("btn btn-black grade").addClass("btn btn-black grade");

    $("#grade_id").val(document.getElementById ("btn_grade{{$key}}").value);

    // console.log($("#grade_id").val());

    $("#btn_grade{{$key}}").removeClass("btn btn-black grade").addClass("btn btn-black grade");
  }
  @endforeach

  @foreach($code_sizes as $key => $code_size)
  function set_btn_code_size_active{{$key}}()
  { 
    $(".code_size").removeClass("btn btn-black code_size").addClass("btn btn-black code_size");

    $("#code_size_id").val(document.getElementById ("btn_code_size{{$key}}").value);

    // console.log($("#grade_id").val());

    $("#btn_code_size{{$key}}").removeClass("btn btn-black code size").addClass("btn btn-black code_size");
  }
  @endforeach

  function submit_form()
  {
    var grade_id = document.getElementById("grade_id").value;
    var code_size_id = document.getElementById("code_size_id").value;
  // var block_weight = document.getElementById("block_weight").value;
  var qty_type_id = document.getElementById("qty_type").value;

  if(qty_type_id == '') {alert("Qty Type is required!");}
  else if(grade_id == '') {alert("Grade is required!");}
  else if(code_size_id == '') {alert("Code Size is required!");}
  // else if(block_weight == '') {alert("Block Weight is required!");}
  else {$('#form_grading').submit();}
};

function submit_form_close()
{ 
  $('#form_grading_close').submit();
}; 

function storeclose ()
{ 
  swal({
    title: 'Are you sure?',
    text: "You won't be able to revert this grading!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#d33',
    cancelButtonColor: '#BDBDBD',
    confirmButtonText: 'Yes, close it!'
  }).then(function () {

    $('#form_close').submit();
  })
};  

function storecollect()
  { 
   
   var collect = document.getElementById ("collect").value; 
   if(collect=='')
   {
    swal("Error!", "All data is required!", "error")
  }
  else{  
    $.ajax({
      type: 'POST',
      url: "{{ URL::route('collectgrading', $temp_tally->id) }}",
      data: {
        '_token': $('input[name=_token]').val(), 
        'collect': $('#collect').val(),    
        'temp_tally_id': $('#temp_tally_id').val()   
      },
      success: function(data) { 
        if ((data.errors)) {
         swal("Error!", "Gat data failed!", "error")
       } else {

         swal("Success!", "Data has been saved!", "success") 
         $('.text_collect').hide(100);
         $('.qty_type').show(100);
         $('.btn_collect').show(100); 

         $('#text_collect').val(''); 

      
       } 
     },

   })
  }
};


$(document).ready(function() {
  $('#div_qtytype').click(function() {  

    $('.div_qtytype').hide(100);
    $('.text_grade').hide(100);
    $('.text_grade_option').show(100);
    $('.text_code_size').hide(100); 
    $('.back_div_qtytype').show(100);
    $('.back_text_grade').hide(100);
     $('.btn_collect').hide(100);
  });

  $('#text_grade').click(function() {  

    $('.div_qtytype').hide(100);
    $('.text_grade').hide(100);
    $('.text_code_size').show(100);
    $('.back_text_grade').show(100);
    $('.back_div_qtytype').hide(100);

  });

  $('#back_div_qtytype').click(function() {  
   $('.div_qtytype').show(100);
   $('.back_div_qtytype').hide(100);
 $('.text_grade_option').hide(100);

 $('.btn_collect').show(100);


 });

  $('#grade_option_none').click(function() {  
   $('.text_grade_option').hide(100);
    $('.text_grade').show(100);
    $('.text_code_size').hide(100);
    $('.back_text_grade').show(100);
    $('.back_div_qtytype').hide(100);


 });

  $('#grade_option_bs').click(function() {  
   
    $('.text_grade_option').hide(100);
    $('.text_total_block').show(100);
    $('.next_total_block').show(100); 
    $('.back_text_code_size').hide(100);
    document.getElementById ("code_size_id").value = "00";  
    document.getElementById ("grade_id").value = "4";  


 });

  $('#back_text_grade').click(function() {  
   $('.div_qtytype').hide(100);
   $('.text_grade').show(100);
   $('.text_code_size').hide(100);
   $('.back_text_grade').hide(100);
   $('.back_div_qtytype').show(100); 
   $('.back_text_code_size').hide(100);
   $('.text_total_block').hide(100);
   $('.next_total_block').hide(100);

 }); 

  $('#text_code_size').click(function() {  

    $('.text_code_size').hide(100);
    $('.text_grade').hide(100);
    $('.text_total_block').show(100);
    $('.next_total_block').show(100);
    $('.back_text_grade').hide(100);
    $('.back_text_rest').hide(100);
    $('.back_text_code_size').show(100);

    $('.back_div_qtytype').hide(100);

  });

  $('#back_text_code_size').click(function() {  

    $('.back_text_code_size').hide(100);
    $('.text_grade').hide(100);
    $('.text_total_block').hide(100);
    $('.next_total_block').hide(100);
    $('.back_text_grade').show(100);
    $('.text_code_size').show(100);
    $('.back_text_rest').hide(100);
    $('.back_text_total_block').hide(100);
    $('.div_qtytype').hide(100);

    $('.back_div_qtytype').hide(100);

  });

  $('#next_total_block').click(function() {  

   $('.next_total_block').hide(100);
   $('.text_code_size').hide(100);
   $('.text_grade').hide(100);
   $('.text_total_block').hide(100);
   $('.back_text_total_block').hide(100);
    $('.back_div_qtytype').hide(100);
   $('.text_rest').show(100);

   $('.back_text_code_size').hide(100);

   $('.back_text_rest').show(100);

 });

  $('#back_text_rest').click(function() {  

   $('.next_total_block').show(100);
   $('.text_code_size').hide(100);
   $('.text_grade').hide(100);
   $('.text_total_block').show(100);
   $('.next_total_block').show(100);
   $('.back_text_grade').hide(100);
   $('.back_text_rest').hide(100);
   $('.text_rest').hide(100);
   $('.back_text_total_block').hide(100);
   $('.back_text_code_size').show(100);

   

 });

    $('#btn_collect').click(function() {  
    $('.qty_type').hide(100);
    $('.div_qtytype').hide(100);
     $('.btn_collect').hide(100);
    $('.back_collect').show(100);
    $('.text_collect').show(100); 

  }); 

   $('#back_collect').click(function() { 
    $('.div_qtytype').show(100);
    $('.qty_type').show(100);
    $('.back_collect').hide(100);
    $('.text_collect').hide(100); 
    $('.btn_collect').show(100);

  }); 

  

  $('#all_element').click(function() {   

   var type_name = document.getElementById ("type_name").value;  

   document.getElementById ("breadcrumb").innerHTML = type_name;
 });


}); 

document.getElementById("loader").style.display = "none";

function showLoader() {
  document.getElementById("loader").style.display = "block";
}

function hideLoader() {
  document.getElementById("loader").style.display = "none";
}
</script>