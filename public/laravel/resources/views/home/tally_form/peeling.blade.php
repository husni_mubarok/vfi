@extends('layouts.tabletform.template')
<!-- <style type="text/css">
  .btn-big{
    padding: 0px;
    height: 120px;
    vertical-align: middle;
    font-size: 3vw !important;
    white-space: initial !important;
  }
</style> -->
<meta name="_token" content="{{ csrf_token() }}"/> 

<div id="all_element">
    <section class="myheader lime">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-4 heading">
                <h1>PEELING</h1>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
                <section class="info">
                       <a href=""><span class="no_label active">{{$temp_tally->trans_code}}<p><i>34 : 13 Minutes</i></p></span></a>
                       <a href=""><span class="no_label">{{$temp_tally->trans_code}}<p><i>34 : 13 Minutes</i></p></span></a>
                       <a href=""><span class="no_label disabled"><i class="fa fa-lock"></i>&nbsp;&nbsp;{{$temp_tally->trans_code}}<p><i>34 : 13 Minutes</i></p></span></a>
            </section>
            </div>
        </div>
    </section>
    <section class="myheader lime float">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <ul>
                    <!-- <li>
                       {!! Form::open(array('route' => ['tally_form.closepeeling', $temp_tally->id], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_close'))!!}
                        {{ csrf_field() }}  
                            <a class="red" id="btn_close" onclick="storeclose();" title="CLOSE">
                                <i class="fa fa-unlock"></i>
                                <p>CLOSE</p>
                            </a>
                        {!! Form::close() !!}   
                    </li> -->
                    <li>
                        <a class="HOME" href="{{ URL::route('tally_form.index') }}">
                            <i class="fa fa-home"></i>
                            <p>HOME</p>
                        </a>
                    </li>
                    <li>
                        <a class="" title="HISTORY" href="{{ URL::route('tally_form.historypeeling', $temp_tally->id) }}">
                            <i class="fa fa-file-text-o"></i>
                             <p>HSTRY</p>
                        </a>
                    </li>
                    <li>
                       
                        <a class="back"  id="back"  style="display: none">
                            <img src="{{Config::get('constants.path.img')}}/icon/back.svg" style="height: 35px;margin-bottom:0px;">
                            <p style="margin-top:5px;">BACK</p>
                        </a>
                        <a class="back_salinity"  id="back_salinity" style="display: none">
                            <img src="{{Config::get('constants.path.img')}}/icon/back.svg" style="height: 35px;margin-bottom:0px;">
                            <p style="margin-top:5px;">BACK</p>
                        </a>
                        <a class="back_duration"  id="back_duration" style="display: none">
                            <img src="{{Config::get('constants.path.img')}}/icon/back.svg" style="height: 35px;margin-bottom:0px;">
                            <p style="margin-top:5px;">BACK</p>
                        </a>
                        <a class="back_salinity_rm"  id="back_salinity_rm" style="display: none">
                            <img src="{{Config::get('constants.path.img')}}/icon/back.svg" style="height: 35px;margin-bottom:0px;">
                            <p style="margin-top:5px;">BACK</p>
                        </a>
                        <a class="back_collect"  id="back_collect" style="display: none">
                            <img src="{{Config::get('constants.path.img')}}/icon/back.svg" style="height: 35px;margin-bottom:0px;">
                            <p style="margin-top:5px;">BACK</p>
                        </a>

                    </li>
                </ul>
            </div>
        </div>
    </section>
    

  <div class="row">

    <!-- <div class="col-md-12 col-sm-12 col-xs-12"> 
      <div class="x_panel"> 
        <div class="x_content">   
          <div class="col-md-4 col-sm-4 col-xs-4">
           <center><h1 id="form_title">PEELING</h1></center>
         </div>
         <div class="col-md-4 col-sm-4 col-xs-4">
           <center><h1>{{$temp_tally->trans_code}}</h1></center>
         </div>
         {!! Form::open(array('route' => ['tally_form.closepeeling', $temp_tally->id], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_close'))!!}
         {{ csrf_field() }}
         <div class="col-md-1 col-sm-1 col-xs-1" style="padding: 5px">
          <button type="button" id="btn_close" onclick="storeclose();" class="btn btn-danger btn-lg btn-block nomargin" style="padding: 10px 0;"><i class="fa fa-close faa-pulse animated"></i></button>
        </div>
        {!! Form::close() !!}

        <div class="col-md-1 col-sm-1 col-xs-1" style="padding: 5px">
          <a href="{{ URL::route('tally_form.index') }}" type="button" class="btn btn-primary btn-lg btn-block nomargin"  style="padding: 10px 0;"><i class="fa fa-home faa-pulse animated"></i></a>
        </div>

       <div class="col-md-1 col-sm-1 col-xs-1" style="padding: 5px">
        <a href="{{ URL::route('tally_form.historypeeling', $temp_tally->id) }}" type="button" class="btn btn-primary btn-lg pull-right btn-block nomargin" style="padding: 10px 0;"><i class="fa fa-file-text-o faa-pulse animated"></i></a>
      </div>
      <div class="col-md-1 col-sm-1 col-xs-1" style="padding: 5px">
       <a id="back"  style="display: none" href="#" type="button" class="btn btn-warning btn-lg back" style="padding: 10px 0;"><i class="fa fa-arrow-circle-left faa-pulse animated"></i></a>

       <a id="back_salinity"  style="display: none" href="#" type="button" class="btn btn-warning btn-lg back_salinity" style="padding: 10px 0;"><i class="fa fa-arrow-circle-left faa-pulse animated"></i></a>

       <a id="back_duration"  style="display: none" href="#" type="button" class="btn btn-warning btn-lg back_duration" style="padding: 10px 0;"><i class="fa fa-arrow-circle-left faa-pulse animated"></i></a>

       <a id="back_salinity_rm"  style="display: none" href="#" type="button" class="btn btn-warning btn-lg back_salinity_rm" style="padding: 10px 0;"><i class="fa fa-arrow-circle-left faa-pulse animated"></i></a>

       <a id="back_collect"  style="display: none" href="#" type="button" class="btn btn-warning btn-lg back_collect" style="padding: 10px 0;"><i class="fa fa-arrow-circle-left faa-pulse animated"></i></a>
     </div>
   </div>
 </div>
</div>
<div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: -30px">
 <hr> 
</div>   -->

<form class="form-horizontal" role="form">
  {{ csrf_field() }}
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="col-md-12">
      <div class="x_panel"> 
        <div class="x_content">    
          <div id="peeling_type" class="peeling_type"> 
            <h3 class="float_breadcrumb">TYPE</h3>
            @foreach($tally_detail as $key => $tally_details)
            <div class="col-md-4 col-sm-4 col-xs-4 button">
                <a  class="btn btn-black" id="{{$tally_details->id}}" onclick="set_btn_{{$tally_details->id}}_active();" value="{{$tally_details->id}}">
                    <img src="{{Config::get('constants.path.img')}}/icon/kg.svg">
                    <span id="{{$tally_details->name}}">{{$tally_details->name}}</span>
                </a> 
                <a href="asd" class="btn btn-block btn-close"><i class="fa fa-unlock"></i>&nbsp;&nbsp;Close</a>
            </div>
            <script type="text/javascript">
             function set_btn_{{$tally_details->id}}_active()
             {
              @foreach($tally_detail as $key => $tally_detail_btn)
              $("#{{$tally_detail_btn->id}}").removeClass("btn btn-black").addClass("btn btn-black"); 
              @endforeach
              $("#{{$tally_details->id}}").removeClass("btn btn-black").addClass("btn btn-black"); 

              var type_name = document.getElementById ("{{$tally_details->name}}").innerHTML;
              document.getElementById ("type_name").value = type_name;
            };
          </script>

          @endforeach

          {{ Form::hidden('type_name',old('type_name'), array('required' => 'true', 'id' => 'type_name')) }}
          {{ Form::hidden('tally_peeling_master_id',old('tally_peeling_master_id'), array('required' => 'true', 'id' => 'tally_peeling_master_id')) }}
          {{ Form::hidden('purchase_id',old('purchase_id', $temp_tally->id_purchase), array('required' => 'true', 'id' => 'purchase_id')) }}
          {{ Form::hidden('temp_tally_id',old('temp_tally_id', $temp_tally->id), array('required' => 'true', 'id' => 'temp_tally_id')) }}
        </div>

        <div class="col-md-4 col-sm-4 col-xs-4 button btn_collect"> 
            <a id="btn_collect" class="btn btn-block btn-black btn_collect this_collect">
                <img src="{{Config::get('constants.path.img')}}/icon/collect.svg">
                <span>COLLECT</span>
            </a>
            <a class="btn btn-block btn-close btn_collect this_collect"><i class="fa fa-unlock"></i>&nbsp;&nbsp;Close</a>
        </div>

        <div id="text_weight" class="text_weight" style="display: none">
          <h3 class="float_breadcrumb">WEIGHT</h3>
          {{ Form::number('weight',old('weight'), array('class' => 'form-control', 'placeholder' => 'Weight*', 'required' => 'true', 'id' => 'weight', 'style' => 'height:70px; font-size:36px;', 'autofocus')) }}<br> 

           <div class="row">
                <div class="col-md-8 col-sm-8 col-md-offset-2 col-sm-offset-2">
                    <br>
                    <a href="#" class="btn btn-success btn-lg btn-block" onclick="store();"> <i class="fa fa-check"></i>&nbsp;&nbsp;SAVE</a>
                </div>
            </div> 
        </div>

        <div id="text_salinity" class="text_salinity" style="display: none">
          <h3>SALINITY</h3>
          {{ Form::number('salinity',old('salinity'), array('class' => 'form-control', 'placeholder' => 'Salinity*', 'required' => 'true', 'id' => 'salinity', 'style' => 'height:70px; font-size:36px;', 'autofocus')) }}<br>
        </div>

        <a id="next_total_salinity" href="#" type="button" class="btn btn-primary btn-lg pull-right next_total_salinity" style="padding: 35px !important; font-size: 20px; display: none"><i class="fa fa-arrow-circle-right faa-pulse animated"></i><br/><p>NEXT</p></a>

        <div id="text_duration" class="text_duration" style="display: none">
          <h3>DURATION</h3>
          {{ Form::number('duration',old('duration'), array('class' => 'form-control', 'placeholder' => 'Duration*', 'required' => 'true', 'id' => 'duration', 'style' => 'height:70px; font-size:36px;')) }}<br>
        </div>

        <a id="next_duration" href="#" type="button" class="btn btn-primary btn-lg pull-right next_duration" style="padding: 35px !important; font-size: 20px; display: none"><i class="fa fa-arrow-circle-right faa-pulse animated"></i><br/><p>NEXT</p></a>

        <div id="text_salinity_rm" class="text_salinity_rm" style="display: none">
          <h3>SALINITY RM</h3>
          {{ Form::number('salinity_rm',old('salinity_rm'), array('class' => 'form-control', 'placeholder' => 'Salinity RM*', 'required' => 'true', 'id' => 'salinity_rm', 'style' => 'height:70px; font-size:36px;')) }}<br>
          <a href="#" class="btn btn-success btn-lg pull-right" onclick="storetreatmentoption();" style="padding: 35px !important; font-size: 20px;"> <i class="fa fa-check"></i> SAVE</a>
        </div>

         <div id="text_collect" class="text_collect" style="display: none">
          <h3 class="float_breadcrumb">COLLECT</h3>
          <h4>Peeling</h4>
          {{ Form::number('peeling',old('peeling',0), array('class' => 'form-control', 'placeholder' => 'Collect*', 'required' => 'true', 'id' => 'peeling', 'style' => 'height:70px; font-size:36px;', 'autofocus')) }}<br>
          <h4>Trimming</h4>
          {{ Form::number('trimming',old('trimming',0), array('class' => 'form-control', 'placeholder' => 'Collect*', 'required' => 'true', 'id' => 'trimming', 'style' => 'height:70px; font-size:36px;')) }}<br>
          <!-- <a href="#" class="btn btn-success btn-lg pull-right" onclick="storecollect();" style="padding: 35px; font-size: 20px"> <i class="fa fa-check"></i><br/> SAVE</a> -->
          <div class="row">
                <div class="col-md-8 col-sm-8 col-md-offset-2 col-sm-offset-2">
                    <br>
                    <a href="#" class="btn btn-success btn-lg btn-block" onclick="storecollect();"> <i class="fa fa-check"></i>&nbsp;&nbsp;SAVE</a>
                </div>
            </div> 

        </div>
      </div>
    </div>
  </div>
</div>
</form>
<div id="loader"></div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script type="text/javascript">
  $("button").click(function() {
    var fired_button = $(this).val();
    document.getElementById ("tally_peeling_master_id").value = fired_button;
    document.getElementById("weight").focus();
    document.getElementById("waste").focus();
  });

  function store()
  {
   var weight = document.getElementById("weight").value;
   var tally_peeling_master_id = document.getElementById ("tally_peeling_master_id").value;
   if(weight=='' || tally_peeling_master_id=='')
   {
    swal("Error!", "All data is required!", "error")
  }
  else{
    showLoader();
    $.ajax({
      type: 'POST',
      url: "{{ URL::route('storepeeling', $temp_tally->id) }}",
      data: {
        '_token': $('input[name=_token]').val(),
        'tally_peeling_master_id': $('#tally_peeling_master_id').val(),
        'weight': $('#weight').val(),
        'purchase_id': $('#purchase_id').val(),
        'temp_tally_id': $('#temp_tally_id').val()
      },
      success: function(data) {
        if ((data.errors)) {
         swal("Error!", "Gat data failed!", "error")
       } else {

         swal("Success!", "Data has been saved!", "success")
         hideLoader();

         $('.peeling_type').show(100);
         $('#btn_collect').show(100);
         $('.text_weight').hide(100);
         $('.back').hide(100);

         $('.treatment_option_active').show(100);

         document.getElementById ("weight").value = "";
         document.getElementById ("id").value = "";


         @foreach($tally_detail as $key => $tally_detail_btn)
         $("#{{$tally_detail_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default");
         @endforeach
       }
     },

   })
  }
};


function storetreatmentoption()
{
 var salinity = document.getElementById ("salinity").value;
 var duration = document.getElementById ("duration").value;
 if(salinity=='' || duration=='')
 {
  swal("Error!", "All data is required!", "error")
}
else{
  showLoader();
  $.ajax({
    type: 'POST',
    url: "{{ URL::route('storepeelingtreatment', $temp_tally->id) }}",
    data: {
      '_token': $('input[name=_token]').val(),
      'salinity': $('#salinity').val(),
      'duration': $('#duration').val(),
      'salinity_rm': $('#salinity_rm').val(),
      'temp_tally_id': $('#temp_tally_id').val()
    },
    success: function(data) {
      if ((data.errors)) {
       swal("Error!", "Gat data failed!", "error")
     } else {

       swal("Success!", "Data has been saved!", "success")
       hideLoader();

       $('.peeling_type').show(100);
       $('.text_duration').hide(100);
       $('.back_duration').hide(100);


    $('.back_salinity_rm').hide(100);
    $('.text_salinity_rm').hide(100);

       $('.treatment_option_active').show(100);

       document.getElementById ("salinity").value = "";
       document.getElementById ("duration").value = "";
       document.getElementById ("salinity_rm").value = "";
       document.getElementById ("id").value = "";


       @foreach($tally_detail as $key => $tally_detail_btn)
       $("#{{$tally_detail_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default");
       @endforeach
     }
   },

 })
}
};

function storecollect()
  {
   var peeling = document.getElementById("peeling").value;
   var trimming = document.getElementById("trimming").value;
   if(peeling=='' && trimming == '')
   {
    swal("Error!", "All data is required!", "error")
  }
  else{
    $.ajax({
      type: 'POST',
      url: "{{ URL::route('collectpeeling', $tally_id) }}",
      data: {
        '_token': $('input[name=_token]').val(),
        'peeling': $('#peeling').val(),
        'trimming': $('#trimming').val(),
        'temp_tally_id': $('#temp_tally_id').val()
      },
      success: function(data) {
        if ((data.errors)) {
         swal("Error!", "Gat data failed!", "error")
       } else {

         swal("Success!", "Data has been saved!", "success")
         $('.text_collect').hide(100);
         $('.peeling_type').show(100);
         $('.btn_collect').show(100);

         $('#text_collect').val('');

         @foreach($tally_detail as $key => $tally_detail_btn)
         $("#{{$tally_detail_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default");
         @endforeach
       }
     },

   })
  }
};

function storeclose ()
{
  swal({
    title: 'Are you sure?',
    text: "You won't be able to revert this peeling!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#d33',
    cancelButtonColor: '#BDBDBD',
    confirmButtonText: 'Yes, close it!'
  }).then(function () {

    $('#form_close').submit();
  })
};

$(document).ready(function() {
  $('#peeling_type').click(function() {

    $('.peeling_type').hide(100);
    $('.text_weight').show(100);
    $('.treatment_option_active').hide(100);
    $('.btn_collect').hide(100);
    $('.back').show(100);

  });

  $('#back').click(function() {
    $('.peeling_type').show(100);
    $('.text_weight').hide(100);
    $('.back').hide(100);
    $('.treatment_option_active').show(100);
     $('.btn_collect').show(100);
  });

  $('#treatment_option_active').click(function() {
    $('.peeling_type').hide(100);
    $('.treatment_option_active').hide(100);
    $('.back').hide(100);
    $('.text_salinity').show(100);
    $('.next_total_salinity').show(100);
    $('.back_salinity').show(100);
  });

  $('#next_total_salinity').click(function() {
    $('.next_total_salinity').hide(100);
    $('.text_duration').show(100);
    $('.text_salinity').hide(100);
    $('.back_salinity').hide(100);
    $('.back_duration').show(100);
    $('.next_duration').show(100);
  });

  $('#next_duration').click(function() {
    $('.next_total_salinity').hide(100);
    $('.text_duration').hide(100);
    $('.text_salinity').hide(100);
    $('.back_salinity').hide(100);
    $('.back_duration').hide(100);
    $('.next_duration').hide(100);
    $('.text_salinity_rm').show(100);
    $('.back_salinity_rm').show(100);
  });

  $('#back_salinity').click(function() {
    $('.receiving_treatment').hide(100);
    $('.back_salinity').hide(100);
    $('.peeling_type').show(100);
    $('.additional').hide(100);
    $('.back_width').hide(100);
    $('.text_salinity').hide(100);
    $('.next_total_salinity').hide(100);
    $('.treatment_option_active').show(100);
  });

  $('#back_duration').click(function() {
    $('.receiving_treatment').hide(100);
    $('.back_duration').hide(100);
    $('.back_salinity').show(100);
    $('.text_salinity').show(100);
    $('.text_duration').hide(100);
    $('.next_duration').hide(100);
    $('.next_total_salinity').show(100);
  });

  $('#back_salinity_rm').click(function() {
    $('.receiving_treatment').hide(100);
    $('.back_duration').show(100);
    $('.back_salinity_rm').hide(100);
    $('.text_salinity_rm').hide(100);
    $('.text_duration').show(100);
    $('.next_duration').show(100);
  });

  $('#btn_collect').click(function() {
    $('.peeling_type').hide(100);
     $('.btn_collect').hide(100);
    $('.back_collect').show(100);
    $('.text_collect').show(100);

  });

   $('#back_collect').click(function() {
    $('.peeling_type').show(100);
    $('#btn_collect').show(100);
    $('.btn_collect').show(100);
    $('.back_collect').hide(100);
    $('.text_collect').hide(100);
  });

  $('#all_element').click(function() {

   var type_name = document.getElementById ("type_name").value;

   document.getElementById ("form_title").innerHTML = type_name;
 });


});

document.getElementById("loader").style.display = "none";

function showLoader() {
  document.getElementById("loader").style.display = "block";
}

function hideLoader() {
  document.getElementById("loader").style.display = "none";
}
</script>

