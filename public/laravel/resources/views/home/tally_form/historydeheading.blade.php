@extends('layouts.tablet.template')
<meta name="_token" content="{{ csrf_token() }}"/>
    <section class="myheader green">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-4 heading">
                <h1>HEADSPLIT</h1>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
                <section class="info">
                       <a href=""><span class="no_label active">{{$temp_tally->trans_code}}<p><i>34 : 13 Minutes</i></p></span></a>
                       <a href=""><span class="no_label">{{$temp_tally->trans_code}}<p><i>34 : 13 Minutes</i></p></span></a>
                       <a href=""><span class="no_label disabled"><i class="fa fa-lock"></i>&nbsp;&nbsp;{{$temp_tally->trans_code}}<p><i>34 : 13 Minutes</i></p></span></a>
            </section>
            </div>
        </div>
    </section>
    <section class="myheader green float">
        <div class="row">
            <div class="col-md-8 col-sm-8">
                <ul>
                    <li>
                        <a class="HOME" href="{{ URL::route('tally_form.index') }}">
                            <i class="fa fa-home"></i>
                            <p>HOME</p>
                        </a>
                    </li>
                    <li>
                        <a class="" title="RECEIVING HISTORY" href="{{ URL::route('tally_form.deheading', $temp_tally->id) }}">
                            <img src="{{Config::get('constants.path.img')}}/icon/back.svg" style="height: 35px;margin-bottom:0px;">
                            <p style="margin-top:5px;">BACK</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </section>
   <section class="info dark">
       <div class="row">
           <div class="col-md-8 col-sm-8">
               <h3 class="mybreadcrumb"><b>HEADSPLIT HISTORY</b></h3>
           </div>
       </div>
    </section>
   
    <br>

<div class="row">
  <!-- <div class="col-md-12 col-sm-12 col-xs-12"> 
    <div class="x_panel"> 
      <div class="x_content">   
        <div class="col-md-4 col-sm-4 col-xs-4">
         <center><h3><b>HEADSPLIT HISTORY</b></h3></center> 
       </div> 
       <div class="col-md-4 col-sm-4 col-xs-4">
         <center><h1>{{$temp_tally->trans_code}}</h1></center>   
       </div>  
       {!! Form::open(array('route' => ['tally_form.closedeheading', $temp_tally->id], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_close'))!!}
       {{ csrf_field() }}  
       <div class="col-md-1 col-sm-1 col-xs-1" style="padding: 5px">  
        <button type="button" id="btn_close" onclick="storeclose();" class="btn btn-danger btn-lg btn-block nomargin" style="padding: 10px 0;"><i class="fa fa-close faa-pulse animated"></i></button> 
      </div>
      {!! Form::close() !!}   

      <div class="col-md-1 col-sm-1 col-xs-1" style="padding: 5px">  
        <a href="{{ URL::route('tally_form.index') }}" type="button" class="btn btn-primary btn-lg btn-block nomargin"  style="padding: 10px 0;"><i class="fa fa-home faa-pulse animated"></i></a>
      </div> 

      <div class="col-md-1 col-sm-1 col-xs-1" style="padding: 5px">  
        <a href="{{ URL::route('tally_form.deheading', $temp_tally->id) }}" type="button" class="btn btn-primary btn-lg pull-right btn-block nomargin" style="padding: 10px 0;"><i class="fa fa-wpforms faa-pulse animated"></i></a>
      </div>  
    </div>
  </div>
</div>  -->

<div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: -30px">
 <hr>
 <p id="breadcrumb"></p>
</div> 

<div class="row"> 
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="col-md-12">
      <div class="x_panel"> 
        <div class="x_content"> 
          <table id="itemTable" class="table table-bordered dataTable">
            <thead>
              <tr>   
                <th>Name</th>
                <th><center>Weight</center></th> 
                <th><center>Waste</center></th>   

              </tr> 
            </thead>
            <tbody>  
              @if(isset($array_all))
              @foreach($array_all AS $all)  
              <tr>
                <td colspan="3">{{$all["name"]}}</td> 
              </tr>
              @foreach($all["detail"] AS $detail)
              <tr> 
               <td></td>
               <td>{{$detail["weight"]}}</td>    
               <td>{{$detail["waste"]}}</td>  
             </tr> 
             @endforeach 
             <tr>
              <td><b>TOTAL</b></td>  
              <td><b>{{$all["total_weight"]}}</b></td>    
              <td><b>{{$all["total_waste"]}}</b></td>  
            </tr>
            @endforeach 
            @endIf
          </tbody> 
        </table>   
      </div> 
    </div>
  </div>  
</div> 
<script type="text/javascript">
    function storeclose ()
  { 
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this headsplit!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#BDBDBD',
      confirmButtonText: 'Yes, close it!'
    }).then(function () {

      $('#form_close').submit();
    })
  };  
</script>