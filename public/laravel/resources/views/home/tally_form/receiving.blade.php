@extends('layouts.tabletform.template')

<style type="text/css">
    .btn.btn-default{background: #333333!important}
</style>

<meta name="_token" content="{{ csrf_token() }}"/>
<div id="all_element">
    <section class="myheader blue">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-4 heading">
                <h1>RECEIVING</h1>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
                <section class="info">
                       <span class="no_label active">{{$temp_tally->trans_code}}<p><i>34 : 13 Minutes</i></p></span>
                       <span class="no_label">{{$temp_tally->trans_code}}<p><i>34 : 13 Minutes</i></p></span>
                       <span class="no_label disabled"><i class="fa fa-lock"></i>&nbsp;&nbsp;{{$temp_tally->trans_code}}<p><i>34 : 13 Minutes</i></p></span>
            </section>
            </div>
        </div>
    </section>
    <section class="myheader blue float">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <ul>
                    <li>
                        <a class="HOME" href="{{ URL::route('tally_form.index') }}">
                            <i class="fa fa-home"></i>
                            <p>HOME</p>
                        </a>
                    </li>
                    <li>
                        <a class="" title="RECEIVING HISTORY" href="{{ URL::route('tally_form.historyreceiving', $temp_tally->id) }}">
                            <i class="fa fa-file-text-o"></i>
                             <p>HSTRY</p>
                        </a>
                    </li>
                    <li>
                       
                        <a class="back_receiving"  id="back_receiving"  style="display: none">
                            <img src="{{Config::get('constants.path.img')}}/icon/back.svg" style="height: 35px;margin-bottom:0px;">
                            <p style="margin-top:5px;">BACK</p>
                        </a>
                        <a class="back_size"  id="back_size" style="display: none">
                            <img src="{{Config::get('constants.path.img')}}/icon/back.svg" style="height: 35px;margin-bottom:0px;">
                            <p style="margin-top:5px;">BACK</p>
                        </a>
                        <a class="back_width"  id="back_width" style="display: none">
                            <img src="{{Config::get('constants.path.img')}}/icon/back.svg" style="height: 35px;margin-bottom:0px;">
                            <p style="margin-top:5px;">BACK</p>
                        </a>
                        <a class="back_additional"  id="back_additional" style="display: none">
                            <img src="{{Config::get('constants.path.img')}}/icon/back.svg" style="height: 35px;margin-bottom:0px;">
                            <p style="margin-top:5px;">BACK</p>
                        </a>
                        <a class="back_weight"  id="back_weight" style="display: none">
                            <img src="{{Config::get('constants.path.img')}}/icon/back.svg" style="height: 35px;margin-bottom:0px;">
                            <p style="margin-top:5px;">BACK</p>
                        </a>
                        

                    </li>
                </ul>
            </div>
        </div>
    </section>

   

    



 <form class="form-horizontal" role="form">
  {{ csrf_field() }}

    <!-- BUTTON -->
    <div id="rec_type" class="rec_type">
        <section class="button">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-4" style="padding: 5px">
                    <a class="btn btn-black" id="btn_receiving" onclick="set_btn_receiving_active();" >
                        <img src="{{Config::get('constants.path.img')}}/icon/kg.svg">
                        <span>Receiving</span>
                    </a>
                    <a class="btn btn-block btn-close btn_collect this_collect"><i class="fa fa-unlock"></i>&nbsp;&nbsp;Close</a>
                </div>
                 <div class="col-md-4 col-sm-4 col-xs-4" style="padding: 5px">
                    <a class="btn btn-black" id="btn_treatment" onclick="set_btn_treatment_active();">
                        <img src="{{Config::get('constants.path.img')}}/icon/treatment.svg">
                        <span>Treatment</span>
                    </a>
                    <a class="btn btn-block btn-close btn_collect this_collect"><i class="fa fa-unlock"></i>&nbsp;&nbsp;Close</a>
                </div>
                 <div class="col-md-4 col-sm-4 col-xs-4" style="padding: 5px">
                    <a class="btn btn-black" id="btn_additional" onclick="set_btn_additional_active();">
                        <img src="{{Config::get('constants.path.img')}}/icon/additional.svg">
                        <span>Additional</span>
                    </a>
                    <a class="btn btn-block btn-close btn_collect this_collect"><i class="fa fa-unlock"></i>&nbsp;&nbsp;Close</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-4" style="padding: 5px">
                    <a class="btn btn-black" id="btn_additional" onclick="set_btn_additional_active();">
                        <img src="{{Config::get('constants.path.img')}}/icon/collect.svg">
                        <span>Collect</span>
                    </a>
                    <a class="btn btn-block btn-close btn_collect this_collect"><i class="fa fa-unlock"></i>&nbsp;&nbsp;Close</a>
                </div>
            </div>
        </section>
    </div>

  <div class="row"> 
    <div class="col-md-12 col-sm-12 col-xs-12">  
      <div class="x_panel"> 
        <div class="x_content">

            
            

           
            {{ Form::hidden('tally_type',old('tally_type'), array('required' => 'true', 'id' => 'tally_type')) }}  
         

          <div id="receiving_treatment" class="receiving_treatment" style="display: none">
            <div class="row"> 
              <div class="col-md-12 col-sm-12 col-xs-12">  
                <div class="x_panel"> 
                  <div class="x_content">
                    <h3>SIZE</h3> 
                    <div id="size_fresh" class="size_fresh">
                      @foreach($tally_detail as $key => $tally_details)
                      <div class="col-md-4 col-sm-4 col-xs-4" style="padding: 5px"> 
                        <button type="button" class="btn btn-black btn-block" style="padding: 50px 0; font-size: 50px;" id="{{$tally_details->id}}" onclick="set_btn_{{$tally_details->id}}_active();" value="{{$tally_details->id}}"><p style="font-size: 40px" id="{{$tally_details->note}}"> {{$tally_details->note}}</p></button>  
                      </div>
                      <script type="text/javascript">
                       function set_btn_{{$tally_details->id}}_active()
                       { 
                        @foreach($tally_detail as $key => $tally_detail_btn)
                        $("#{{$tally_detail_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-black"); 
                        @endforeach
                        @foreach($tally_additional as $key => $tally_add_btn)
                        $("#add_{{$tally_add_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-black"); 
                        @endforeach
                        $("#{{$tally_details->id}}").removeClass("btn btn-black").addClass("btn btn-black");
                        var size_name = document.getElementById ("{{$tally_details->note}}").innerHTML; 
                        document.getElementById ("size_name").value = size_name;
                      };  
                    </script> 
                    @endforeach 
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div id="size_beku_kapal" class="size_beku_kapal" style="display: none">
            @foreach($size_beku_kapal as $key => $size_beku_kapals) 
            <button type="button" class="btn btn-default btn-lg" style="margin-top: 5px; padding: 30px;" id="{{$size_beku_kapals->id}}" onclick="set_btn_{{$size_beku_kapals->id}}_active();" value="{{$size_beku_kapals->id}}">{{$size_beku_kapals->note}}</button> 

            <script type="text/javascript">
             function set_btn_{{$size_beku_kapals->id}}_active()
             { 
              @foreach($size_beku_kapal as $key => $tally_beku_kapal_btn)
              $("#{{$tally_beku_kapal_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default"); 
              @endforeach
              @foreach($tally_additional as $key => $tally_add_btn)
              $("#add_{{$tally_add_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default"); 
              @endforeach
              $("#{{$size_beku_kapals->id}}").removeClass("btn btn-default").addClass("btn btn-primary"); 
            };  
          </script> 
          @endforeach  
        </div> 
      </div> 


      {{ Form::hidden('size_name',old('size_name'), array('required' => 'true', 'id' => 'size_name')) }} 
      {{ Form::hidden('size_id',old('size_id'), array('required' => 'true', 'id' => 'size_id')) }} 
      {{ Form::hidden('purchase_id',old('purchase_id', $temp_tally->id_purchase), array('required' => 'true', 'id' => 'purchase_id')) }}
      {{ Form::hidden('temp_tally_id',old('temp_tally_id', $temp_tally->id), array('required' => 'true', 'id' => 'temp_tally_id')) }} 
      <div id="text_weight" class="text_weight" style="display: none">
        <br/> 
        <h3>WEIGHT</h3>
        {{ Form::number('weight',old('weight'), array('class' => 'form-control', 'placeholder' => 'WEIGHT*', 'required' => 'true', 'id' => 'weight', 'style' => 'height:70px; font-size:36px;')) }}<br>  
        <div class="row">
            <div class="col-md-8 col-sm-8 col-md-offset-2 col-sm-offset-2">
                <br>
                <a href="#" class="btn btn-success btn-lg btn-block" onclick="store();"> <i class="fa fa-check"></i>&nbsp;&nbsp;SAVE</a>
            </div>
        </div>
      </div>

      <div id="additional" class="additional" style="display: none">
        <div class="row"> 
          <div class="col-md-12 col-sm-12 col-xs-12">  
            <div class="x_panel"> 
              <div class="x_content">
                <h3>ADDITIONAL VALUE</h3>
                @foreach($tally_additional as $key => $tally_additionals)
                <div class="col-md-4 col-sm-4 col-xs-4" style="padding: 5px"> 
                  <button type="button" class="btn btn-default btn-lg btn-block" style="padding: 30px 0; font-size: 30px;" id="add_{{$tally_additionals->id}}" onclick="set_btn_add_{{$tally_additionals->id}}_active();" value="{{$tally_additionals->id}}"><p id="{{$tally_additionals->name}}"> {!! nl2br(e($tally_additionals->name)) !!}</p></button> 
                </div> 
                <script type="text/javascript">
                 function set_btn_add_{{$tally_additionals->id}}_active()
                 { 
                  @foreach($tally_additional as $key => $tally_add_btn)
                  $("#add_{{$tally_add_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-black"); 
                  @endforeach
                  @foreach($tally_detail as $key => $tally_detail_btn)
                  $("#{{$tally_detail_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-black"); 
                  @endforeach
                  $("#add_{{$tally_additionals->id}}").removeClass("btn btn-black").addClass("btn btn-black"); 

                  var additional_name = document.getElementById ("{{$tally_additionals->name}}").innerHTML; 
                  document.getElementById ("additional_name").value = additional_name;
                };  
              </script> 
              @endforeach 
              {{ Form::hidden('additional_name',old('additional_name'), array('required' => 'true', 'id' => 'additional_name')) }} 
            </div>
          </div>
        </div>
      </div>
    </div>  
    <div class="row"> 
      <div class="col-md-12 col-sm-12 col-xs-12">   
        <div id="weight_additional" class="weight_additional" style="display: none">
          {{ Form::hidden('tally_additional_master_id',old('tally_additional_master_id'), array('required' => 'true', 'id' => 'tally_additional_master_id')) }}  
          <br/> 
          <h3>WEIGHT</h3>
          {{ Form::number('weight_add',old('weight_add'), array('class' => 'form-control', 'placeholder' => 'WEIGHT*', 'required' => 'true', 'id' => 'weight_add', 'style' => 'height:70px; font-size:36px;')) }}<br>
            <div class="row">
                <div class="col-md-8 col-sm-8 col-md-offset-2 col-sm-offset-2">
                    <br>
                    <a href="#" class="btn btn-success btn-lg btn-block" onclick="storeadd();"> <i class="fa fa-check"></i> SAVE</a>
                </div>
            </div>
        </div>  
      </div>
    </div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12"> 
        <div class="x_panel"> 
          <div class="x_content">
            <div id="text_treatment" class="text_treatment" style="display: none">
                <div class="row">
                  <div class="col-md-4 col-sm-4 col-xs-4"> 
                    <h3>BEFORE</h3>
                    {{ Form::number('before_weight',old('before_weight'), array('class' => 'form-control', 'placeholder' => 'WEIGHT*', 'required' => 'true', 'id' => 'before_weight', 'style' => 'height:70px; font-size:30px;', 'oninput' => 'calc_treatment();')) }}<br> 

                    {{ Form::number('before_salinity_rm',old('before_salinity_rm'), array('class' => 'form-control', 'placeholder' => 'SALINITY RM*', 'required' => 'true', 'id' => 'before_salinity_rm', 'style' => 'height:70px; font-size:30px;')) }}<br>
                  </div> 

                  <div class="col-md-4 col-sm-4 col-xs-4"> 
                   <h3>INGREDIENT</h3>
                   <div class="input-group"> 
                    {{ Form::number('before_salinity_water',old('before_salinity_water'), array('class' => 'form-control', 'placeholder' => 'SALINITY WATER*', 'required' => 'true', 'id' => 'before_salinity_water', 'style' => 'height:70px; font-size:30px;')) }} <span class="input-group-addon" style=" font-size:30px;">L</span> 
                  </div><br>  

                  <div class="input-group"> 
                    {{ Form::number('ingredient_salinity_rm',old('ingredient_salinity_rm'), array('class' => 'form-control', 'placeholder' => 'SALINITY RM*', 'required' => 'true', 'id' => 'ingredient_salinity_rm', 'style' => 'height:70px; font-size:30px;')) }}<span class="input-group-addon" style=" font-size:30px;">%</span> 
                  </div><br>

                  <div class="input-group"> 

                    {{ Form::number('before_duration',old('before_duration'), array('class' => 'form-control', 'placeholder' => 'DURATION*', 'required' => 'true', 'id' => 'before_duration', 'style' => 'height:70px; font-size:30px;')) }}<span class="input-group-addon" style=" font-size:30px;">M</span> 
                  </div><br>

                  <div class="input-group"> 

                    {{ Form::number('ice',old('ice'), array('class' => 'form-control', 'placeholder' => 'ES*', 'required' => 'true', 'id' => 'ice', 'style' => 'height:70px; font-size:30px;')) }}<span class="input-group-addon" style=" font-size:30px;">%</span> 
                  </div><br>

                  <div class="input-group"> 

                    {{ Form::number('',old('salt'), array('class' => 'form-control', 'placeholder' => 'SALT*', 'required' => 'true', 'id' => 'salt', 'style' => 'height:70px; font-size:30px;')) }}<span class="input-group-addon" style=" font-size:30px;">%</span> 
                  </div><br>


                  <div class="input-group"> 

                    {{ Form::number('',old('temperature'), array('class' => 'form-control', 'placeholder' => 'TEMPERATURE*', 'required' => 'true', 'id' => 'temperature', 'style' => 'height:70px; font-size:30px;')) }}<span class="input-group-addon" style=" font-size:30px;">C</span> 
                  </div><br>

                  <div class="input-group"> 

                    {{ Form::number('',old('other'), array('class' => 'form-control', 'placeholder' => 'ETC*', 'required' => 'true', 'id' => 'other', 'style' => 'height:70px; font-size:30px;')) }}<span class="input-group-addon" style=" font-size:30px;">%</span> 
                  </div><br>

                </div> 
                <div class="col-md-4 col-sm-4 col-xs-4"> 
                  <h3>AFTER</h3>
                  {{ Form::number('after_weight',old('after_weight'), array('class' => 'form-control', 'placeholder' => 'WEIGHT*', 'required' => 'true', 'id' => 'after_weight', 'style' => 'height:70px; font-size:30px;', 'oninput' => 'calc_treatment();')) }}<br> 

                  <div class="input-group"> 

                    {{ Form::number('after_salinity_rm',old('after_salinity_rm'), array('class' => 'form-control', 'placeholder' => 'SALINITY*', 'required' => 'true', 'id' => 'after_salinity_rm', 'style' => 'height:70px; font-size:30px;')) }}<span class="input-group-addon" style=" font-size:30px;">%</span> 
                  </div><br/> 
                  {{ Form::number('calculate_treatment',old('calculate_treatment'), array('class' => 'form-control', 'placeholder' => 'CALCULATE*', 'required' => 'true', 'id' => 'calculate_treatment', 'style' => 'height:70px; font-size:30px;')) }} 
                  </div>
              </div>

              <div class="row">
                    <div class="col-md-2 col-sm-2 col-xs-12"></div>
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        <a href="#" class="btn btn-danger btn-lg btn-block" onclick="cleartext();"> <i class="fa fa-refresh"></i> CLEAR</a> 
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        <a href="#" class="btn btn-success btn-lg  btn-block" onclick="storetreatmentoption();"> <i class="fa fa-check"></i> SAVE</a>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12"></div>
              </div>
              <br>
            </div> 
          </div>  
        </div> 
      </div> 
    </form> 

   <!--  <section class="button">
        <div class="row">
            <div class="col-md-8 col-sm-8 col-md-offset-2 col-sm-offset-2">
                <a class="btn btn-back btn-block back_receiving" id="back_receiving"  style="display: none">
                    <img src="{{Config::get('constants.path.img')}}/icon/back.svg">
                    <span>BACK</span>
                </a>
                <a class="btn btn-back btn-block back_size" id="back_size"  style="display: none">
                    <img src="{{Config::get('constants.path.img')}}/icon/back.svg">
                    <span>BACK</span>
                </a>
                <a class="btn btn-back btn-block back_width" id="back_width"  style="display: none">
                    <img src="{{Config::get('constants.path.img')}}/icon/back.svg">
                    <span>BACK</span>
                </a>
                <a class="btn btn-back btn-block back_additional" id="back_additional"  style="display: none">
                    <img src="{{Config::get('constants.path.img')}}/icon/back.svg">
                    <span>BACK</span>
                </a>
                <a class="btn btn-back btn-block back_weight" id="back_weight"  style="display: none">
                    <img src="{{Config::get('constants.path.img')}}/icon/back.svg">
                    <span>BACK</span>
                </a>

            </div>
        </div>
    </section> -->

   <!--  <a id="back_receiving"  style="display: none" href="#" type="button" class="btn btn-warning btn-lg btn-block back_receiving nomargin" style="padding: 10px 0;"><i class="fa fa-arrow-circle-left faa-pulse animated"></i></a> 

    <a id="back_size"  style="display: none" href="#" type="button" class="btn btn-warning btn-lg back_size" style="padding: 10px 0;"><i class="fa fa-arrow-circle-left faa-pulse animated"></i></a> 

    <a id="back_width"  style="display: none" href="#" type="button" class="btn btn-warning btn-lg back_width" style="padding: 10px 0;"><i class="fa fa-arrow-circle-left faa-pulse animated"></i></a>  

    <a id="back_additional"  style="display: none" href="#" type="button" class="btn btn-warning btn-lg back_additional" style="padding: 10px 0;"><i class="fa fa-arrow-circle-left faa-pulse animated"></i></a> 

    <a id="back_weight"  style="display: none" href="#" type="button" class="btn btn-warning btn-lg back_weight" style="padding: 10px 0;"><i class="fa fa-arrow-circle-left faa-pulse animated"></i></a>  -->


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

    <script type="text/javascript">
      $("button").click(function() {
        document.getElementById ("size_id").value = "";
        document.getElementById ("tally_additional_master_id").value = "";
        var fired_button = $(this).val();
        document.getElementById ("size_id").value = fired_button;
        document.getElementById ("tally_additional_master_id").value = fired_button;
        document.getElementById ("weight").focus();
      });

      function set_btn_fresh_active()
      { 
        $("#btn_fresh").removeClass("btn btn-default").addClass("btn btn-primary");
        $("#btn_bekukapal").removeClass("btn btn-primary").addClass("btn btn-default");  
        $('.treatment_option_active').hide(100); 
      };

      function set_btn_bekukapal_active()
      { 
        $("#btn_fresh").removeClass("btn btn-primary").addClass("btn btn-default");
        $("#btn_bekukapal").removeClass("btn btn-default").addClass("btn btn-primary"); 
        $('.treatment_option_active').hide(100);  
      };

      function set_btn_receiving_active()
      { 
        $("#btn_receiving").removeClass("btn btn-default").addClass("btn btn-primary");
        $("#btn_treatment").removeClass("btn btn-primary").addClass("btn btn-default");
        $("#btn_additional").removeClass("btn btn-primary").addClass("btn btn-default");
        $('.treatment_option_active').hide(100);
        document.getElementById ("tally_type").value = "RECEIVING";
      };

      function set_btn_treatment_active()
      { 
        $("#btn_receiving").removeClass("btn btn-primary").addClass("btn btn-default");
        $("#btn_treatment").removeClass("btn btn-default").addClass("btn btn-primary");
        $("#btn_additional").removeClass("btn btn-primary").addClass("btn btn-default");
        $('.treatment_option_active').hide(100);
        document.getElementById ("tally_type").value = "TREATMENT";
      };

      function set_btn_additional_active()
      { 
        $("#btn_receiving").removeClass("btn btn-primary").addClass("btn btn-default");
        $("#btn_treatment").removeClass("btn btn-primary").addClass("btn btn-default");
        $("#btn_additional").removeClass("btn btn-default").addClass("btn btn-primary");
        $('.treatment_option_active').hide(100);
        document.getElementById ("tally_type").value = "ADDITIONAL";
      };

      function store()
      {   
       var tally_type = document.getElementById ("tally_type").value;
       var weight = document.getElementById ("weight").value;
       var size_id = document.getElementById ("size_id").value; 
       if(tally_type=='' || weight=='' || size_id=='')
       {
        swal("Error!", "All data is required!", "error")
      }
      else{ 
       $.ajax({
        type: 'POST',
        url: "{{ URL::route('store', $tally_details->id) }}",
        data: {
          '_token': $('input[name=_token]').val(),
          'tally_type': $('#tally_type').val(),
          'size_id': $('#size_id').val(),
          'weight': $('#weight').val(),
          'purchase_id': $('#purchase_id').val(),
          'temp_tally_id': $('#temp_tally_id').val() 
        },
        success: function(data) { 
          if ((data.errors)) {
           swal("Error!", "Gat data failed!", "error")
         } else {

          swal("Success!", "Receiving/treatment data has been save!", "success")

          $('.receiving_treatment').hide(100);
          $('.back_receiving').hide(100);
          $('.rec_type').show(100);
          $('.additional').hide(100);
          $('.back_width').hide(100);
          $('.text_weight').hide(100);
          $('.weight_additional').hide(100); 
          $('.rest_additional').hide(100);  
          $('.treatment_option_active').show(100); 

          cleartext();
          $("#btn_additional").removeClass("btn btn-primary").addClass("btn btn-default");
          $("#btn_receiving").removeClass("btn btn-primary").addClass("btn btn-default");
          $("#btn_treatment").removeClass("btn btn-primary").addClass("btn btn-default");

          @foreach($size_beku_kapal as $key => $tally_beku_kapal_btn)
          $("#{{$tally_beku_kapal_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default"); 
          @endforeach

          @foreach($tally_detail as $key => $tally_detail_btn)
          $("#{{$tally_detail_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default"); 
          @endforeach
        } 
      },
    })
     }
   };

   function storeadd()
   { 
     var tally_additional_master_id = $('#tally_additional_master_id').val();
     var weight_add = $('#weight_add').val();  
     if(tally_additional_master_id=='' || weight_add=='')
     {
      swal("Error!", "All data is required!", "error")
    }
    else{ 
     $.ajax({
      type: 'POST',
      url: "{{ URL::route('storeadditional', $tally_details->id) }}",
      data: {
        '_token': $('input[name=_token]').val(),
        'tally_additional_master_id': $('#tally_additional_master_id').val(),
        'weight_add': $('#weight_add').val(), 
        'purchase_id': $('#purchase_id').val(),
        'temp_tally_id': $('#temp_tally_id').val() 
      },
      success: function(data) { 
        if ((data.errors)) {
         swal("Error!", "Gat data failed!", "error")
       } else {


        swal("Success!", "Additional data has been save!", "success")
        cleartext();

        $('.treatment_option_active').show(100);

        $('.receiving_treatment').hide(100);
        $('.back_receiving').hide(100);
        $('.rec_type').show(100);
        $('.additional').hide(100);
        $('.back_width').hide(100);
        $('.text_weight').hide(100);
        $('.weight_additional').hide(100);  
        $('.back_size').hide(100);
        $('.back_additional').hide(100);

        $("#btn_additional").removeClass("btn btn-primary").addClass("btn btn-default");
        $("#btn_receiving").removeClass("btn btn-primary").addClass("btn btn-default");
        $("#btn_treatment").removeClass("btn btn-primary").addClass("btn btn-default");

        @foreach($size_beku_kapal as $key => $tally_beku_kapal_btn)
        $("#{{$tally_beku_kapal_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default"); 
        @endforeach

        @foreach($tally_additional as $key => $tally_additional_btn)
        $("#add_{{$tally_additional_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default"); 
        @endforeach
      } 
    },

  })
   }
 };

 function calc_treatment(){   
  var after_weight = $('#after_weight').val();
  var before_weight = $('#before_weight').val(); 
  $('#calculate_treatment').val() = (parseFloat(after_weight) / parseFloat(before_weight))*100;  
}

function storetreatmentoption()
{   
  var before_weight = $('#before_weight').val();
  var before_salinity_water = $('#before_salinity_water').val();
  var before_salinity_rm = $('#before_salinity_rm').val();
  var after_weight = $('#after_weight').val();
  var after_salinity_rm = $('#after_salinity_rm').val();  
  var ice = $('#ice').val(); 
  var salt = $('#salt').val(); 
  var temperature = $('#temperature').val(); 
  var other = $('#other').val(); 
  var ingredient_salinity_rm = $('#ingredient_salinity_rm').val(); 

  if(before_weight=='' || before_salinity_water=='' || before_salinity_rm=='' || after_weight=='' || after_salinity_rm=='' || ice=='' || salt=='' || temperature=='' || other=='' || ingredient_salinity_rm=='')
  {
    swal("Error!", "All data is required!", "error")
  }
  else{  
    $.ajax({
      type: 'POST',
      url: "{{ URL::route('storereceivingtreatment', $temp_tally->id) }}",
      data: {
        '_token': $('input[name=_token]').val(),  
        'before_weight': $('#before_weight').val(),  
        'before_salinity_water': $('#before_salinity_water').val(), 
        'before_salinity_rm': $('#before_salinity_rm').val(), 
        'after_weight': $('#after_weight').val(), 
        'after_salinity_rm': $('#after_salinity_rm').val(),   
        'temp_tally_id': $('#temp_tally_id').val(),
        'before_duration': $('#before_duration').val(), 
        'ingredient_salinity_rm': $('#ingredient_salinity_rm').val(), 
        'ice': $('#ice').val() ,
        'salt': $('#salt').val() ,
        'temperature': $('#temperature').val() ,
        'other': $('#other').val()    
      },
      success: function(data) { 
        if ((data.errors)) {
         swal("Error!", "Gat data failed!", "error")
       } else {
         swal("Success!", "Peeling treatment has been save!", "success")
         $('.back_receiving').hide(100);
         $('.text_treatment').hide(100);
         $('.rec_type').show(100);
         cleartext();

         @foreach($tally_detail as $key => $tally_detail_btn)
         $("#{{$tally_detail_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default"); 
         @endforeach
       } 
     },

   })
  }
};


function cleartext()
{ 
  $('#before_weight').val(''); 
  $('#before_salinity_water').val(''); 
  $('#before_salinity_rm').val(''); 
  $('#before_duration').val(''); 
  $('#after_weight').val(''); 
  $('#after_salinity_rm').val('');
  $('#ice').val(''); 
  $('#salt').val(''); 
  $('#temperature').val('');  
  $('#ingredient_salinity_rm').val(''); 
  $('#other').val('');   
  $('#tally_additional_master_id').val('');
  $('#weight_add').val(''); 
  $('#size_name').val('');
  $('#additional_name').val('');  
  $('#calculate_treatment').val('');  
  $('#breadcrumb').innerHTML = "";
};

function storeclose ()
{ 
  swal({
    title: 'Are you sure?',
    text: "You won't be able to revert this receiving!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#d33',
    cancelButtonColor: '#BDBDBD',
    confirmButtonText: 'Yes, close it!'
  }).then(function () {
    $('#form_close').submit();
  })
};  

$(document).ready(function() {
  $('#btn_additional').click(function() { 
    $('.receiving_treatment').hide(100);
    $('.back_receiving').show(100);
    $('.rec_type').hide(100);
    $('.back_size').hide(100);
    $('.back_width').hide(100);
    $('.text_weight').hide(100);
    $('.additional').show(100);
    $('.weight_additional').hide(100); 
  });

  $('#btn_receiving').click(function() { 
    $('.receiving_treatment').show(100);
    $('.back_receiving').show(100);
    $('.back_size').hide(100);
    $('.rec_type').hide(100);
    $('.additional').hide(100);
    $('.back_width').hide(100);
    $('.text_weight').hide(100);
    $('.additional').hide(100); 
    $('.weight_additional').hide(100); 
  });

  $('#btn_treatment').click(function() {  
    $('.back_receiving').show(100);
    $('.back_size').hide(100);
    $('.rec_type').hide(100);
    $('.additional').hide(100);
    $('.back_width').hide(100);
    $('.text_weight').hide(100);
    $('.additional').hide(100); 
    $('.weight_additional').hide(100); 
    $('.text_treatment').show(100);
  });

  $('#back_receiving').click(function() { 
    $('.receiving_treatment').hide(100);
    $('.back_receiving').hide(100);
    $('.rec_type').show(100);
    $('.additional').hide(100);
    $('.back_width').hide(100);
    $('.text_weight').hide(100);
    $('.weight_additional').hide(100);  
    $('.text_treatment').hide(100);  
    $('.treatment_option_active').show(100);
  });
  
  $('#back_salinity_rm').click(function() { 
    $('.receiving_treatment').hide(100);
    $('.back_duration').hide(100);
    $('.back_salinity').hide(100); 
    $('.text_salinity').hide(100);
    $('.text_duration').show(100);
    $('.next_duration').show(100);
    $('.back_duration').show(100);
    $('.next_total_salinity').hide(100); 
    $('.text_salinity_rm').hide(100);
    $('.back_salinity_rm').hide(100); 
  });

  $('#treatment_option_active').click(function() { 
    $('.peeling_type').hide(100);
    $('.treatment_option_active').hide(100);
    $('.back_receiving').hide(100); 
    $('.text_salinity').show(100);
    $('.next_total_salinity').show(100); 
    $('.back_salinity').show(100); 
    $('.rec_type').hide(100); 
  }); 

  $('#next_total_salinity').click(function() {  
    $('.next_total_salinity').hide(100); 
    $('.text_duration').show(100); 
    $('.text_salinity').hide(100);
    $('.back_salinity').hide(100);
    $('.back_duration').show(100);
    $('.next_duration').show(100);
  });

  $('#next_duration').click(function() {  
    $('.next_total_salinity').hide(100); 
    $('.text_duration').hide(100); 
    $('.text_salinity').hide(100);
    $('.back_salinity').hide(100);
    $('.back_duration').hide(100);
    $('.next_duration').hide(100);
    $('.back_salinity_rm').show(100);
    $('.text_salinity_rm').show(100);
  });

  $('#receiving_treatment').click(function() { 
    $('.receiving_treatment').hide(100);
    $('.text_weight').show(100);
    $('.back_width').show(100); 
    $('.back_receiving').hide(100);
    $('.additional').hide(100);
    $('.weight_additional').hide(100); 
  });

  $('#back_width').click(function() { 
   $('.receiving_treatment').show(100);
   $('.text_weight').hide(100);
   $('.back_width').hide(100);
   $('.back_receiving').show(100);
   $('.back_size').hide(100); 
   $('.additional').hide(100); 
   $('.weight_additional').hide(100); 
 });


  $('#additional').click(function() { 
    $('.additional').hide(100); 
    $('.weight_additional').show(100);
    $('.back_size').hide(100);
    $('.back_width').hide(100);
    $('.back_additional').show(100);
    $('.back_receiving').hide(100);
    $('.next_weight').show(100); 
  });

  $('#back_additional').click(function() { 
    $('.additional').show(100); 
    $('.weight_additional').hide(100); 
    $('.back_size').hide(100);
    $('.back_width').hide(100);
    $('.back_additional').hide(100);
    $('.back_receiving').show(100);
    $('.next_weight').hide(100); 
  });
  
  $('#back_weight').click(function() { 
    $('.next_weight').show(100); 
    $('.pan_additional').hide(100); 
    $('.weight_additional').show(100);  
    $('.back_additional').show(100);
    $('.back_weight').hide(100);
  });

  $('#all_element').click(function() {   
   var tally_type =$('#tally_type').val(); 
   var size_name =$('#size_name').val();
   var additional_name =$('#additional_name').val();

   document.getElementById ("breadcrumb").innerHTML = tally_type + "/" + size_name + "/" + additional_name;
 });
});  
</script>