@extends('layouts.tablet.template')
<meta name="_token" content="{{ csrf_token() }}"/> 
<div class="row">
 <div class="col-md-12 col-sm-12 col-xs-12"> 
      <div class="x_panel"> 
        <div class="x_content">   
          <div class="col-md-4 col-sm-4 col-xs-4">
           <center><h3><b>PEELING HISTORY</b></h3></center> 
         </div> 
         <div class="col-md-4 col-sm-4 col-xs-4">
           <center><h1>{{$temp_tally->trans_code}}</h1></center>   
         </div>  
         {!! Form::open(array('route' => ['tally_form.closepeeling', $temp_tally->id], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_close'))!!}
         {{ csrf_field() }}  
         <div class="col-md-1 col-sm-1 col-xs-1" style="padding: 5px">  
          <button type="button" id="btn_close" onclick="storeclose();" class="btn btn-danger btn-lg btn-block nomargin" style="padding: 10px 0;"><i class="fa fa-close faa-pulse animated"></i></button> 
        </div>
        {!! Form::close() !!}   

        <div class="col-md-1 col-sm-1 col-xs-1" style="padding: 5px">  
          <a href="{{ URL::route('tally_form.index') }}" type="button" class="btn btn-primary btn-lg btn-block nomargin"  style="padding: 10px 0;"><i class="fa fa-home faa-pulse animated"></i></a>
        </div> 
       
       <div class="col-md-1 col-sm-1 col-xs-1" style="padding: 5px">  
        <a href="{{ URL::route('tally_form.peeling', $temp_tally->id) }}" type="button" class="btn btn-primary btn-lg pull-right btn-block nomargin" style="padding: 10px 0;"><i class="fa fa-wpforms faa-pulse animated"></i></a>
      </div> 
       
   </div>
 </div>
</div> 

 <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: -30px">
   <hr>
   <p id="breadcrumb"></p>
 </div> 
</div>  
<div class="row"> 
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="col-md-12">
      <div class="x_panel"> 
        <div class="x_content">  
          <table id="itemTable" class="table table-bordered dataTable">
            <thead>
              <tr>   
                <th>Name</th>
                <th><center>Weight</center></th> 
              </tr> 
            </thead>
            <tbody> 
            @if(isset($array_all)) 
              @foreach($array_all AS $all)  
              <tr>  
              <td colspan="2">{{$all["name"]}}</td> 
                <tr>  
                 @foreach($all["detail"] AS $detail_item) 
                 <tr>  
                  <td></td>  
                  <td>{{$detail_item["weight"]}}</td> 
                  <tr> 
                    @endforeach
                    <tr>  
                  <td><b>TOTAL</b></td>  
                  <td><b>{{$all["total_weight"]}}</b></td> 
                  <tr> 
                    @endforeach 
                    @endIf
                  </tbody>
                </table> 

                <h3>TREATMENT OPTION</h3>
                <table id="itemTable" class="table table-bordered dataTable">
            <thead>
              <tr>   
                <th>Salinity</th>
                <th>Duration</th> 
              </tr> 
            </thead>
            <tbody> 
            @if(isset($treatment_option)) 
              @foreach($treatment_option AS $treatment_options)  
              <tr>  
                  <td>{{$treatment_options->salinity}}</td>  
                  <td>{{$treatment_options->duration}}</td> 
                  <tr> 
                    @endforeach  
                    @endIf
                  </tbody>
                </table>   
              </div> 
            </div>
          </div>  
        </div>  

