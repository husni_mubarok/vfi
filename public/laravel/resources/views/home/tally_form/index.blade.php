@extends('layouts.tabletform.template') 
    <section class="myheader">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 heading">
                <h1>TALLY MASTER</h1>
            </div>
        </div>
    </section>
    <div class="row">
        <div class="col-md-4 col-sm-4 col-xs-4 button "> 
            <a id="btn_collect" class="btn btn-block btn-black blue">
                <img src="{{Config::get('constants.path.img')}}/icon/receiving.svg">
                <span>RECEIVING</span>
            </a>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4 button "> 
            <a id="btn_collect" class="btn btn-block btn-black green">
                <img src="{{Config::get('constants.path.img')}}/icon/head_split.svg">
                <span>HEAD SPLIT</span>
            </a>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4 button "> 
            <a id="btn_collect" class="btn btn-block btn-black lime">
                <img src="{{Config::get('constants.path.img')}}/icon/peeling.svg">
                <span>PEELING</span>
            </a>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4 button "> 
            <a id="btn_collect" class="btn btn-block btn-black orange">
                <img src="{{Config::get('constants.path.img')}}/icon/treatment.svg">
                <span>TREATMENT</span>
            </a>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4 button "> 
            <a id="btn_collect" class="btn btn-block btn-black pink">
                <img src="{{Config::get('constants.path.img')}}/icon/grading.svg">
                <span>GRADING</span>
            </a>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4 button "> 
            <a id="btn_collect" class="btn btn-block btn-black brown">
                <img src="{{Config::get('constants.path.img')}}/icon/pack.svg">
                <span>PACKING</span>
            </a>
        </div>

    </div>


<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12"> 
    <div class="col-md-12">  
     <center>
      @actionStart('tally_receiving', 'create')
      @if(isset($last_temp_tally_rec))
      <div class="col-md-3 col-sm-3 col-xs-3" style="padding: 5px">
       <a href="{{ URL::route('tally_form.receiving', $last_temp_tally_rec->id) }}" class="btn btn-default btn-lg btn-block" style="padding: 30px 0;"> <i class="fa fa-level-down"></i> RECEIVING <br/>[ 
         {{$last_temp_tally_rec->trans_code}} 
         ]</a> 
       </div> 
       @else
       <div class="col-md-3 col-sm-3 col-xs-3" style="padding: 5px">
         <a href="#" class="btn btn-default btn-lg btn-block" style="padding: 30px 0;"> <i class="fa fa-level-down"></i> RECEIVING <br/>[ 
           KOSONG 
           ]</a> 
         </div>
         @endif
         @actionEnd

         @actionStart('tally_headsplit', 'create')
         @if(isset($last_temp_tally_headsplit))
         <div class="col-md-3 col-sm-3 col-xs-3" style="padding: 5px">
           <a href="{{ URL::route('tally_form.deheading', $last_temp_tally_headsplit->id) }}" class="btn btn-default btn-lg btn-block" style="padding: 30px 0;"> <i class="fa fa-hand-scissors-o"></i> HEAD SPLIT <br/>[ 
             {{$last_temp_tally_headsplit->trans_code}} 
             ]</a>  
           </div>
           @else
           <div class="col-md-3 col-sm-3 col-xs-3" style="padding: 5px">
             <a href="#" class="btn btn-default btn-lg btn-block" style="padding: 30px 0;"> <i class="fa fa-hand-scissors-o"></i> HEAD SPLIT <br/>[ 
               KOSONG 
               ]</a> 
             </div>
             @endif
             @actionEnd

             @actionStart('tally_peeling', 'create')
             @if(isset($last_temp_tally_peeling))
             <div class="col-md-3 col-sm-3 col-xs-3" style="padding: 5px">
               <a href="{{ URL::route('tally_form.peeling', $last_temp_tally_peeling->id) }}" class="btn btn-default btn-lg btn-block" style="padding: 30px 0;"> <i class="fa fa-paint-brush"></i> PEELING <br/>[ 
                 {{$last_temp_tally_peeling->trans_code}} 
                 ]</a>  
               </div>
               @else
               <div class="col-md-3 col-sm-3 col-xs-3" style="padding: 5px">
                 <a href="#" class="btn btn-default btn-lg btn-block" style="padding: 30px 0;"> <i class="fa fa-paint-brush"></i> PEELING <br/>[ 
                   KOSONG 
                   ]</a>
                 </div> 
                 @endif
                 @actionEnd

                 @actionStart('tally_treatment', 'create')
             @if(isset($last_temp_tally_treatment))
             <div class="col-md-3 col-sm-3 col-xs-3" style="padding: 5px">
               <a href="{{ URL::route('tally_form.treatment', $last_temp_tally_treatment->id) }}" class="btn btn-default btn-lg btn-block" style="padding: 30px 0;"> <i class="fa fa-paint-brush"></i> TREATMENT <br/>[ 
                 {{$last_temp_tally_treatment->trans_code}} 
                 ]</a>  
               </div>
               @else
               <div class="col-md-3 col-sm-3 col-xs-3" style="padding: 5px">
                 <a href="#" class="btn btn-default btn-lg btn-block" style="padding: 30px 0;"> <i class="fa fa-paint-brush"></i> TREATMENT <br/>[ 
                   KOSONG 
                   ]</a>
                 </div> 
                 @endif
                 @actionEnd

                 @actionStart('tally_grading', 'create')
                 @if(isset($last_temp_tally_grading))
                 <div class="col-md-3 col-sm-3 col-xs-3" style="padding: 5px">
                   <a href="{{ URL::route('tally_form.grading', $last_temp_tally_grading->id) }}" class="btn btn-default btn-lg btn-block" style="padding: 30px 0;"> <i class="fa fa-sliders"></i> GRADING <br/>[ 
                     {{$last_temp_tally_grading->trans_code}} 
                     ]</a> 
                   </div> 
                   @else
                   <div class="col-md-3 col-sm-3 col-xs-3" style="padding: 5px">
                     <a href="#" class="btn btn-default btn-lg btn-block" style="padding: 30px 0;"> <i class="fa fa-sliders"></i> GRADING <br/>[ 
                       KOSONG 
                       ]</a> 
                     </div>
                     @endif
                     @actionEnd

                     @actionStart('tally_packing', 'create')
                     @if(isset($last_temp_tally_packing))

                     @if(isset($tally_packing))
                     <div class="col-md-3 col-sm-3 col-xs-3" style="padding: 5px">
                       <a href="{{ URL::route('tally_form.editpacking', $last_temp_tally_packing->id) }}" class="btn btn-default btn-lg btn-block" style="padding: 30px 0;"> <i class="fa fa-suitcase"></i> EDIT PACKING  <br/>[ 
                         {{$last_temp_tally_packing->trans_code}} 
                         ]</a>  
                       </div>
                       @else
                        <div class="col-md-3 col-sm-3 col-xs-3" style="padding: 5px">
                       <a href="{{ URL::route('tally_form.createpacking', $last_temp_tally_packing->id) }}" class="btn btn-default btn-lg btn-block" style="padding: 30px 0;"> <i class="fa fa-suitcase"></i> CREATE PACKING <br/>[ 
                         {{$last_temp_tally_packing->trans_code}} 
                         ]</a>  
                       </div>
                       @endif


                       @else
                       <div class="col-md-3 col-sm-3 col-xs-3" style="padding: 5px">
                         <a href="#" class="btn btn-default btn-lg btn-block" style="padding: 30px 0;"> <i class="fa fa-suitcase"></i> PACKING <br/>[ 
                           KOSONG 
                           ]</a> 
                         </div>
                         @endif
                         @actionEnd
                       
                     </div> 
                   </center> 
                     <div class="row"> 
                       <div class="col-md-12 col-sm-12 col-xs-12"> 
                        <br>
                         <div class="col-md-12">
                          <div class="x_panel"> 
                            <div class="x_content">  
                             <center>
                               <a href="" type="button" class="btn btn-success btn-circle btn-xl" style="margin-left: 10px"><i class="fa fa-refresh faa-pulse animated"></i><br/><p style="font-size: 14px;">Refresh</p></a>
                             </center>
                           </div>
                         </div>
                       </div>
                     </div> 
                     </div> 
                 </div> 





