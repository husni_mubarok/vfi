@extends('layouts.tablet.template')
<meta name="_token" content="{{ csrf_token() }}"/> 
<section class="myheader pink">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-4 heading">
                <h1>GRADING</h1>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
                <section class="info">
                       <a href=""><span class="no_label active">{{$temp_tally->trans_code}}<p><i>34 : 13 Minutes</i></p></span></a>
                       <a href=""><span class="no_label">{{$temp_tally->trans_code}}<p><i>34 : 13 Minutes</i></p></span></a>
                       <a href=""><span class="no_label disabled"><i class="fa fa-lock"></i>&nbsp;&nbsp;{{$temp_tally->trans_code}}<p><i>34 : 13 Minutes</i></p></span></a>
            </section>
            </div>
        </div>
    </section>
    <section class="myheader pink float">
        <div class="row">
            <div class="col-md-8 col-sm-8">
                <ul>
                    <li>
                        <a class="HOME" href="{{ URL::route('tally_form.index') }}">
                            <i class="fa fa-home"></i>
                            <p>HOME</p>
                        </a>
                    </li>
                    <li>
                        <a class="" title="BACK" href="{{ URL::route('tally_form.grading', $temp_tally->id) }}">
                            <img src="{{Config::get('constants.path.img')}}/icon/back.svg" style="height: 35px;margin-bottom:0px;">
                            <p style="margin-top:5px;">BACK</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </section>
   <section class="info dark">
       <div class="row">
           <div class="col-md-8 col-sm-8">
               <h3 class="mybreadcrumb"><b>GRADING HISTORY</b></h3>
           </div>
       </div>
    </section>

    <br>



<div class="row">

  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="col-md-12">
      <div class="x_panel"> 
        <div class="x_content" style="overflow-y: auto;">  
          <table id="itemTable" class="table dataTable ">
            <thead>
              <tr>
                <th>Qty Type</th>
                <th>Grade</th>
                <th>Size</th>
                <th>Freezing</th>  
                <th>Code Size</th>
                <th>Block Weight (Kg)</th>
                <th>Total Block</th>
                <th>Rest</th>
              </tr>
            </thead>
            <tbody>
            @if(isset($grading_history)) 
              @foreach($grading_history as $key => $history_grading)
              <tr>
                <td>{{$history_grading->qty_name}}</td>
                <td>{{$history_grading->grade}}</td>
                <td>{{$history_grading->size}}</td>
                <td>{{$history_grading->freezing}}</td>
                <td>{{$history_grading->code_size}}</td>
                <td>{{$history_grading->block}}</td>
                <td>{{$history_grading->total_block}}</td>
                <td>{{$history_grading->rest}}</td>
              </tr>
              @endforeach
              @endIf
            </tbody>
          </table>
        </div> 
      </div>
    </div>  
  </div>   

<script type="text/javascript">
  function storeclose ()
{ 
  swal({
    title: 'Are you sure?',
    text: "You won't be able to revert this grading!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#d33',
    cancelButtonColor: '#BDBDBD',
    confirmButtonText: 'Yes, close it!'
  }).then(function () {

    $('#form_close').submit();
  })
};  
</script>