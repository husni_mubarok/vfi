@extends('layouts.tablet.template')
<meta name="_token" content="{{ csrf_token() }}"/> 
<section class="myheader blue">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-4 heading">
                <h1>RECEIVING</h1>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
                <section class="info">
                       <a href=""><span class="no_label active">{{$temp_tally->trans_code}}<p><i>34 : 13 Minutes</i></p></span></a>
                       <a href=""><span class="no_label">{{$temp_tally->trans_code}}<p><i>34 : 13 Minutes</i></p></span></a>
                       <a href=""><span class="no_label disabled"><i class="fa fa-lock"></i>&nbsp;&nbsp;{{$temp_tally->trans_code}}<p><i>34 : 13 Minutes</i></p></span></a>
            </section>
            </div>
        </div>
    </section>
    <section class="myheader blue float">
        <div class="row">
            <div class="col-md-8 col-sm-8">
                <ul>
                    <li>
                        <a class="HOME" href="{{ URL::route('tally_form.index') }}">
                            <i class="fa fa-home"></i>
                            <p>HOME</p>
                        </a>
                    </li>
                     <li>
                        <a class="" title="RECEIVING HISTORY" href="{{ URL::route('tally_form.receiving', $temp_tally->id) }}">
                            <img src="{{Config::get('constants.path.img')}}/icon/back.svg" style="height: 35px;margin-bottom:0px;">
                            <p style="margin-top:5px;">BACK</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </section>
   <section class="info dark">
       <div class="row">
           <div class="col-md-8 col-sm-8">
               <h3 class="mybreadcrumb"><b>RECEIVING HISTORY</b></h3>
           </div>
       </div>
    </section>

    <br>



<!-- <div class="row">
  <div class="x_panel"> 
    <div class="x_content">
      <div class="col-md-12 col-sm-12 col-xs-12">  
        <div class="col-md-4 col-sm-4 col-xs-4">
         <center><h3><b>RECEIVING HISTORY</b></h3></center> 
       </div> 
       <div class="col-md-4 col-sm-4 col-xs-4">
         <center><h1>{{$temp_tally->trans_code}}</h1></center>   
       </div>  
       <div class="col-md-1 col-sm-1 col-xs-1" style="padding: 5px"> 
         {!! Form::open(array('route' => ['tally_form.closereceiving', $temp_tally->id], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_close'))!!}
         {{ csrf_field() }}   
         <button type="button" id="btn_close" onclick="storeclose();" class="btn btn-danger btn-lg btn-block" style="padding: 10px 0;"><i class="fa fa-close faa-pulse animated"></i></button>  
         {!! Form::close() !!}   
       </div>
       <div class="col-md-1 col-sm-1 col-xs-1" style="padding: 5px">  
        <a href="{{ URL::route('tally_form.index') }}" type="button" class="btn btn-primary btn-lg btn-block"  style="padding: 10px 0;"><i class="fa fa-home faa-pulse animated"></i></a>
      </div> 
      <div class="col-md-1 col-sm-1 col-xs-1" style="padding: 5px">  
        <a href="{{ URL::route('tally_form.receiving', $temp_tally->id) }}" type="button" class="btn btn-primary btn-lg pull-right btn-block" style="padding: 10px 0;"><i class="fa fa-wpforms faa-pulse animated"></i></a> 
      </div>  
    </div>
  </div>
</div>
</div> -->


   @if(isset($array_all_receiving))
   <div class="row"> 
  <div class="col-md-12 col-sm-12 col-xs-12"> 
     <h4><b>RECEIVING</b> </h4>
     <table id="itemTable" class="table table-bordered dataTable">
      <thead>
        <tr>   
          <th width="20%">Size</th>
          <th width="80%">Weight</th>  
        </tr> 
      </thead>
      <tbody> 
        @foreach($array_all_receiving AS $all_receiving) 
        <tr>  
          <td width="20%" colspan="3"><b>{{$all_receiving["note"]}}</b></td>  
          <tr>   
           @foreach($all_receiving["detail"] AS $detail_item_receiving) 
           <tr>  
            <td width="20%"></td>  
            <td width="80%">{{$detail_item_receiving["weight"]}}</td> 
          </tr> 
          @endforeach  
          <td width="20%"><b>TOTAL</b></td>  
          <td width="80%"><b>{{$all_receiving["total_weight"]}}</b></td> 
        </tr>
      </tr>
      @endforeach 
    </tbody> 
  </table> 
</div> 
</div>   
@endif 

@if(isset($array_all_treatment))  

   <div class="row"> 
  <div class="col-md-12 col-sm-12 col-xs-12"> 
  <h4><b>TREATMENT</b></h4> 
  <table id="itemTable" class="table table-bordered dataTable">
    <thead>
      <tr>   
        <th>Before Weight</th>
        <th>Before Salinity Water</th> 
        <th>Before Salinity RM</th>
        <th>After Weight</th>
        <th>Duration</th>
        <th>After Salinity RM</th>
        <th>Water Temperature</th>
        <th>Salt</th>
        <th>Ice</th>
        <th>Ect</th>
      </tr> 
    </thead>
    <tbody> 
      @if(isset($array_all_treatment)) 
      @foreach($array_all_treatment AS $all)   
      <tr>  
       @foreach($all["detail"] AS $detail_item) 
       <tr>   
        <td>{{$detail_item["before_weight"]}}</td>
        <td>{{$detail_item["before_salinity_water"]}}</td>
        <td>{{$detail_item["before_salinity_rm"]}}</td>
        <td>{{$detail_item["after_weight"]}}</td>
        <td>{{$detail_item["before_duration"]}}</td> 
        <td>{{$detail_item["after_salinity_rm"]}}</td>
        <td>{{$detail_item["temperature"]}}</td> 
        <td>{{$detail_item["salt"]}}</td> 
        <td>{{$detail_item["ice"]}}</td> 
        <td>{{$detail_item["other"]}}</td> 
      </tr> 
      @endforeach
      <tr>    
        <td><b>{{$all["total_before_weight"]}}</b></td> 
        <td><b>{{$all["total_before_salinity_water"]}}</b></td> 
        <td><b>{{$all["total_before_salinity_rm"]}}</b></td> 
        <td><b>{{$all["total_after_weight"]}}</b></td> 
        <td><b>{{$all["total_after_salinity_rm"]}}</b></td> 
        <td><b>{{$all["total_before_duration"]}}</b></td> 
        <th><b>{{$all["total_temperature"]}}</b></th>
        <th><b>{{$all["total_salt"]}}</b></th>
        <th><b>{{$all["total_ice"]}}</b></th>
        <th><b>{{$all["total_other"]}}</b></th>
      </tr> 
      @endforeach 
      </tr>
      @endIf
    </tbody>
  </table> 
</div> 
</div>
@endif 

@if(isset($array_all_add))
<div class="row"> 
  <div class="col-md-12 col-sm-12 col-xs-12">  
    <h4><b>ADDITIONAL</b></h4>
    <table id="itemTable" class="table table-bordered dataTable">
      <thead>
        <tr>   
          <th>Name</th>
          <th><center>Weight</center></th>    
        </tr> 
      </thead>
      <tbody>  
        @foreach($array_all_add AS $all_add)
        <tr>   
          <td colspan="4">{{$all_add["name"]}}</td>  
          <tr>  
            @foreach($all_add["detail"] AS $detail_add) 
            <tr>   
              <td></td>  
              <td>{{$detail_add["weight"]}}</td>    
              <tr> 
               @endforeach  
               <td><b>TOTAL</b></td>  
               <td><b>{{$all_add["total_weight"]}}</b></td>   
               @endforeach  
             </tr>
           </tr>
         </tr>
       </tr>
     </tbody> 
   </table> 
 </div>
</div> 
@endIf  

<script type="text/javascript">
  
function storeclose ()
{ 
  swal({
    title: 'Are you sure?',
    text: "You won't be able to revert this receiving!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#d33',
    cancelButtonColor: '#BDBDBD',
    confirmButtonText: 'Yes, close it!'
  }).then(function () {
    $('#form_close').submit();
  })
};  
</script>