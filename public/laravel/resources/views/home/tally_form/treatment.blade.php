@extends('layouts.tabletform.template')
<!-- <style type="text/css">
  .btn-big{
    padding: 0px;
    height: 120px;
    vertical-align: middle;
    font-size: 3vw !important;
    white-space: initial !important;
  }

</style> -->
<meta name="_token" content="{{ csrf_token() }}"/>
    <section class="myheader orange">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-4 heading">
                <h1>TREATMENT</h1>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
                <section class="info">
                       <a href=""><span class="no_label active">{{$temp_tally->trans_code}}<p><i>34 : 13 Minutes</i></p></span></a>
                       <a href=""><span class="no_label">{{$temp_tally->trans_code}}<p><i>34 : 13 Minutes</i></p></span></a>
                       <a href=""><span class="no_label disabled"><i class="fa fa-lock"></i>&nbsp;&nbsp;{{$temp_tally->trans_code}}<p><i>34 : 13 Minutes</i></p></span></a>
            </section>
            </div>
        </div>
    </section>
    <section class="myheader orange float">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <ul>
                    <!-- <li>
                       {!! Form::open(array('route' => ['tally_form.closepeeling', $temp_tally->id], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_close'))!!}
                        {{ csrf_field() }}
                            <a class="red" id="btn_close" onclick="storeclose();" title="CLOSE">
                                <i class="fa fa-unlock"></i>
                                <p>CLOSE</p>
                            </a>
                        {!! Form::close() !!}
                    </li> -->
                    <li>
                        <a class="HOME" href="{{ URL::route('tally_form.index') }}">
                            <i class="fa fa-home"></i>
                            <p>HOME</p>
                        </a>
                    </li>
                    <li>
                        <a class="" title="HISTORY" href="{{ URL::route('tally_form.historytreatment', $temp_tally->id) }}">
                            <i class="fa fa-file-text-o"></i>
                             <p>HSTRY</p>
                        </a>
                    </li>
                    <li>

                        <a class="back"  id="back"  style="display: none">
                            <img src="{{Config::get('constants.path.img')}}/icon/back.svg" style="height: 35px;margin-bottom:0px;">
                            <p style="margin-top:5px;">BACK</p>
                        </a>
                        <a class="back_salinity"  id="back_salinity" style="display: none">
                            <img src="{{Config::get('constants.path.img')}}/icon/back.svg" style="height: 35px;margin-bottom:0px;">
                            <p style="margin-top:5px;">BACK</p>
                        </a>
                        <a class="back_duration"  id="back_duration" style="display: none">
                            <img src="{{Config::get('constants.path.img')}}/icon/back.svg" style="height: 35px;margin-bottom:0px;">
                            <p style="margin-top:5px;">BACK</p>
                        </a>
                        <a class="back_salinity_rm"  id="back_salinity_rm" style="display: none">
                            <img src="{{Config::get('constants.path.img')}}/icon/back.svg" style="height: 35px;margin-bottom:0px;">
                            <p style="margin-top:5px;">BACK</p>
                        </a>

                    </li>
                </ul>
            </div>
        </div>
    </section>
    


<div id="all_element">
  <div class="row">
   <!--  <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_content">

          <div class="col-md-4 col-sm-4 col-xs-4">
           <center><h1 id="title_form">TREATMENT</h1></center>
         </div>
         <div class="col-md-4 col-sm-4 col-xs-4">
           <center><h1>{{$temp_tally->trans_code}}</h1></center>
         </div>
         {!! Form::open(array('route' => ['tally_form.closepeelingtreatment', $temp_tally->id], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_close'))!!}
         {{ csrf_field() }}
         <div class="col-md-1 col-sm-1 col-xs-1" style="padding: 5px">
          <button type="button" id="btn_close" onclick="storeclose();" class="btn btn-danger btn-lg btn-block nomargin" style="padding: 10px 0;"><i class="fa fa-close faa-pulse animated"></i></button>
        </div>
        {!! Form::close() !!}

        <div class="col-md-1 col-sm-1 col-xs-1" style="padding: 5px">
          <a href="{{ URL::route('tally_form.index') }}" type="button" class="btn btn-primary btn-lg btn-block nomargin"  style="padding: 10px 0;"><i class="fa fa-home faa-pulse animated"></i></a>
        </div>

        <div class="col-md-1 col-sm-1 col-xs-1" style="padding: 5px">
          <a href="{{ URL::route('tally_form.historytreatment', $temp_tally->id) }}" type="button" class="btn btn-primary btn-lg pull-right btn-block nomargin" style="padding: 10px 0;"><i class="fa fa-file-text-o faa-pulse animated"></i></a>
        </div>
        <div class="col-md-1 col-sm-1 col-xs-1" style="padding: 5px">
         <a id="back"  style="display: none" href="#" type="button" class="btn btn-warning btn-lg back" style="padding: 10px 0;"><i class="fa fa-arrow-circle-left faa-pulse animated"></i></a>

         <a id="back_salinity"  style="display: none" href="#" type="button" class="btn btn-warning btn-lg back_salinity" style="padding: 10px 0;"><i class="fa fa-arrow-circle-left faa-pulse animated"></i></a>

         <a id="back_duration"  style="display: none" href="#" type="button" class="btn btn-warning btn-lg back_duration" style="padding: 10px 0;"><i class="fa fa-arrow-circle-left faa-pulse animated"></i></a>

         <a id="back_salinity_rm"  style="display: none" href="#" type="button" class="btn btn-warning btn-lg back_salinity_rm" style="padding: 10px 0;"><i class="fa fa-arrow-circle-left faa-pulse animated"></i></a>

         <a id="back_collect"  style="display: none" href="#" type="button" class="btn btn-warning btn-lg back_collect" style="padding: 10px 0;"><i class="fa fa-arrow-circle-left faa-pulse animated"></i></a>
       </div>
     </div>
   </div>
 </div>
 <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: -30px">
   <hr>

   <p id="breadcrumb"></p>
 </div>  -->


 <form class="form-horizontal" role="form">
  {{ csrf_field() }}
    <div class="col-md-12">

      <div class="x_panel">
        <div class="x_content">
          <div id="peeling_type" class="peeling_type">
            <h3 class="float_breadcrumb">TYPE</h3>
            @foreach($tally_detail as $key => $tally_details)
            <!-- <div class="col-md-3 col-sm-3 col-xs-3" style="padding: 5px">
              <button type="button" class="btn  btn-lg btn-block btn-black"  style="padding: 30px 0; font-size: 24px" id="{{$tally_details->id}}" onclick="set_btn_{{$tally_details->id}}_active();" value="{{$tally_details->id}}"> <p id="{{$tally_details->name}}">{!! nl2br($tally_details->name) !!}</p></button>

            </div> -->
             <div class="col-md-4 col-sm-4 col-xs-4 button">
                <a  class="btn btn-black" id="{{$tally_details->id}}" onclick="set_btn_{{$tally_details->id}}_active();">
                    <img src="{{Config::get('constants.path.img')}}/icon/kg.svg">
                    <span id="{{$tally_details->name}}">{{$tally_details->name}}</span>
                </a> 
                <a href="asd" class="btn btn-block btn-close"><i class="fa fa-unlock"></i>&nbsp;&nbsp;Close</a>
            </div>

            <script type="text/javascript">
             function set_btn_{{$tally_details->id}}_active()
             {
              @foreach($tally_detail as $key => $tally_detail_btn)

              $("#{{$tally_detail_btn->id}}").removeClass("btn btn-black").addClass("btn btn-black");
              @endforeach
              $("#{{$tally_details->id}}").removeClass("btn btn-black").addClass("btn btn-black");


              var type_name = document.getElementById ("{{$tally_details->name}}").innerHTML;
              document.getElementById ("type_name").value = type_name;

              var type_name_act = document.getElementById ("type_name").value;

              document.getElementById ("title_form").innerHTML = type_name_act;
            };
          </script>
          @endforeach

          {{ Form::hidden('duration',old('duration'), array('required' => 'true', 'id' => 'duration')) }}
          {{ Form::hidden('type_name',old('type_name'), array('required' => 'true', 'id' => 'type_name')) }}
          {{ Form::hidden('tally_peeling_master_id',old('tally_peeling_master_id'), array('required' => 'true', 'id' => 'tally_peeling_master_id')) }}
          {{ Form::hidden('tally_peeling_master_id',old('tally_peeling_master_id'), array('required' => 'true', 'id' => 'tally_peeling_master_id')) }}
          {{ Form::hidden('purchase_id',old('purchase_id', $temp_tally->id_purchase), array('required' => 'true', 'id' => 'purchase_id')) }}
          {{ Form::hidden('temp_tally_id',old('temp_tally_id', $temp_tally->id), array('required' => 'true', 'id' => 'temp_tally_id')) }}
        </div>

        <div class="col-md-3 col-sm-3 col-xs-3" style="padding: 5px; display: none;">
          <button type="button" class="btn btn-default btn-lg btn-block treatment_option_active"  style="padding: 35px 0; font-size: 35px" id="treatment_option_active" onclick="treatment_option_active();" value="TREATMENT SALINITY"> TREATMENT SALINITY</button>
        </div>

        <!-- <div class="col-md-3 col-sm-3 col-xs-3" style="padding: 5px">
          <button type="button" id="btn_collect" class="btn  btn-lg btn-block btn-black btn_collect"  style="padding: 30px 0; font-size: 24px" >COLLECT</button>
        </div> -->
        <div class="col-md-4 col-sm-4 col-xs-4 button "> 
            <a id="btn_collect" class="btn btn-block btn-black btn_collect this_collect">
                <img src="{{Config::get('constants.path.img')}}/icon/collect.svg">
                <span>COLLECT</span>
            </a>
            <a class="btn btn-block btn-close btn_collect this_collect"><i class="fa fa-unlock"></i>&nbsp;&nbsp;Close</a>
        </div>


        <div class="row">
          <div class="">
            <div class="x_panel">
              <div class="x_content">
                <div id="text_weight" class="text_weight" style="display: none">

                    <h3 class="float_breadcrumb">Treatment</h3>
                    <div class="row">
                      <div class="col-md-4 col-sm-4 col-xs-4">
                        <h3>BEFORE</h3>
                        {{ Form::number('before_weight',old('before_weight'), array('class' => 'form-control', 'placeholder' => 'WEIGHT*', 'required' => 'true', 'id' => 'before_weight', 'style' => 'height:70px; font-size:30px;')) }}<br>

                        {{ Form::number('before_salinity_rm',old('before_salinity_rm'), array('class' => 'form-control', 'placeholder' => 'SALINITY RM*', 'required' => 'true', 'id' => 'before_salinity_rm', 'style' => 'height:70px; font-size:30px;')) }}<br>
                      </div>


                  <div class="col-md-4 col-sm-4 col-xs-4">

                   <h3>INGREDIENT</h3>
                   <div class="input-group">
                    {{ Form::number('before_salinity_water',old('before_salinity_water'), array('class' => 'form-control', 'placeholder' => 'SALINITY WATER*', 'required' => 'true', 'id' => 'before_salinity_water', 'style' => 'height:70px; font-size:30px;')) }} <span class="input-group-addon" style=" font-size:30px;">%</span>
                  </div><br>

                  <div class="input-group">
                    {{ Form::hidden('ingredient_salinity_rm',old('ingredient_salinity_rm',0), array('class' => 'form-control', 'placeholder' => 'SALINITY RM*', 'required' => 'true', 'id' => 'ingredient_salinity_rm', 'style' => 'height:70px; font-size:30px;')) }}
                  </div><br>

                  <div class="input-group">
                    {{ Form::number('before_duration',old('before_duration'), array('class' => 'form-control', 'placeholder' => 'DURATION*', 'required' => 'true', 'id' => 'before_duration', 'style' => 'height:70px; font-size:30px;')) }}<span class="input-group-addon" style=" font-size:30px;">Menit</span>
                  </div><br>

                  <div class="input-group">
                    {{ Form::number('ice',old('ice'), array('class' => 'form-control', 'placeholder' => 'ES*', 'required' => 'true', 'id' => 'ice', 'style' => 'height:70px; font-size:30px;')) }}<span class="input-group-addon" style=" font-size:30px;">KG</span>
                  </div><br>

                  <div class="input-group">

                    {{ Form::number('',old('salt'), array('class' => 'form-control', 'placeholder' => 'SALT*', 'required' => 'true', 'id' => 'salt', 'style' => 'height:70px; font-size:30px;')) }}<span class="input-group-addon" style=" font-size:30px;">KG</span>
                  </div><br>



                  <div class="input-group">


                        {{ Form::number('',old('temperature'), array('class' => 'form-control', 'placeholder' => 'TEMPERATURE*', 'required' => 'true', 'id' => 'temperature', 'style' => 'height:70px; font-size:30px;')) }}<span class="input-group-addon" style=" font-size:30px;">C</span>
                      </div><br>


                  <div class="input-group">


                        {{ Form::number('',old('other'), array('class' => 'form-control', 'placeholder' => 'ETC*', 'required' => 'true', 'id' => 'other', 'style' => 'height:70px; font-size:30px;')) }}<span class="input-group-addon" style=" font-size:30px;">%</span>
                      </div><br>

                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4">
                      <h3>AFTER</h3>
                      {{ Form::number('after_weight',old('after_weight'), array('class' => 'form-control', 'placeholder' => 'WEIGHT*', 'required' => 'true', 'id' => 'after_weight', 'style' => 'height:70px; font-size:30px;')) }}<br>

                      <div class="input-group">


                    {{ Form::number('after_salinity_rm',old('after_salinity_rm'), array('class' => 'form-control', 'placeholder' => 'SALINITY RM*', 'required' => 'true', 'id' => 'after_salinity_rm', 'style' => 'height:70px; font-size:30px;')) }}<span class="input-group-addon" style=" font-size:30px;">%</span>
                  </div><br/> </div>

                  <!--<div class="col-md-12 col-sm-12 col-xs-12">

                    <a href="#" class="btn btn-danger btn-lg" onclick="cleartext();" style="padding: 35px !important; font-size: 20px; margin-top: 20px"> <i class="fa fa-"></i> CLEAR</a>


                    <a href="#" class="btn btn-success btn-lg pull-right" onclick="store();" style="padding: 35px !important; font-size: 20px; margin-top: 20px"> <i class="fa fa-check"></i> SAVE</a>

                  </div> -->
                  </div>
                  <div class="row">
                        <div class="col-md-2 col-sm-2 col-xs-12"></div>
                        <div class="col-md-4 col-sm-4 col-xs-6">
                            <a href="#" class="btn btn-danger btn-lg btn-block" onclick="cleartext();"> <i class="fa fa-refresh"></i> CLEAR</a>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-6">
                            <a href="#" class="btn btn-success btn-lg  btn-block" onclick="store();"> <i class="fa fa-check"></i> SAVE</a>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div id="text_salinity" class="text_salinity" style="display: none">
            <h3>SALINITY</h3>
            {{ Form::number('salinity',old('salinity'), array('class' => 'form-control', 'placeholder' => 'Salinity*', 'required' => 'true', 'id' => 'salinity', 'style' => 'height:70px; font-size:36px;')) }}<br>
          </div>

          <a id="next_total_salinity" href="#" type="button" class="btn btn-primary btn-lg pull-right next_total_salinity" style="padding: 35px !important; font-size: 20px; display: none"><i class="fa fa-arrow-circle-right faa-pulse animated"></i><br/><p>NEXT</p></a>

          <div id="text_duration" class="text_duration" style="display: none">
            <h3>DURATION</h3>
            {{ Form::number('duration',old('duration'), array('class' => 'form-control', 'placeholder' => 'Duration*', 'required' => 'true', 'id' => 'duration', 'style' => 'height:70px; font-size:36px;')) }}<br>
          </div>

          <a id="next_duration" href="#" type="button" class="btn btn-primary btn-lg pull-right next_duration" style="padding: 35px !important; font-size: 20px; display: none"><i class="fa fa-arrow-circle-right faa-pulse animated"></i><br/><p>NEXT</p></a>

          <div id="text_salinity_rm" class="text_salinity_rm" style="display: none">
            <h3>SALINITY RM</h3>
            {{ Form::number('salinity_rm',old('salinity_rm'), array('class' => 'form-control', 'placeholder' => 'Salinity RM*', 'required' => 'true', 'id' => 'salinity_rm', 'style' => 'height:70px; font-size:36px;')) }}<br>
            <a href="#" class="btn btn-success btn-lg pull-right" onclick="storetreatmentoption();" style="padding: 35px !important; font-size: 20px;"> <i class="fa fa-check"></i> SAVE</a>
          </div>

          <div id="text_collect" class="text_collect" style="display: none">
            <h3>COLLECT</h3><br>
            <h4>TUBE</h4>
            {{ Form::number('collect_tube',old('collect_tube',0), array('class' => 'form-control', 'placeholder' => 'Collect*', 'required' => 'true', 'id' => 'collect_tube', 'style' => 'height:70px; font-size:36px;')) }}
            <h4>TENTACLE</h4>
            {{ Form::number('collect_tentacle',old('collect_tentacle',0), array('class' => 'form-control', 'placeholder' => 'Collect*', 'required' => 'true', 'id' => 'collect_tentacle', 'style' => 'height:70px; font-size:36px;')) }}
            <!-- <a href="#" class="btn btn-success btn-lg pull-right" onclick="storecollect();" style="padding: 35px; font-size: 20px"> <i class="fa fa-check"></i><br/> SAVE</a> -->

            <div class="row">
                <div class="col-md-8 col-sm-8 col-md-offset-2 col-sm-offset-2">
                    <br>
                    <a href="#" class="btn btn-success btn-lg btn-block" onclick="storecollect();"> <i class="fa fa-check"></i>&nbsp;&nbsp;SAVE</a>
                </div>
            </div> 
          </div>
        </div>
      </div>
    </div>

</form>
<div id="loader"></div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script type="text/javascript">
  $("button").click(function() {
    var fired_button = $(this).val();
    document.getElementById ("tally_peeling_master_id").value = fired_button;
    document.getElementById("weight").focus();
    document.getElementById("waste").focus();
  });


  function store()
  {
//alert("dasd");
var before_weight = document.getElementById ("before_weight").value;
var before_salinity_water = document.getElementById ("before_salinity_water").value;
var before_salinity_rm = document.getElementById ("before_salinity_rm").value;
var after_weight = document.getElementById ("after_weight").value;
var after_salinity_rm = document.getElementById ("after_salinity_rm").value;
var before_salinity_rm = document.getElementById ("before_salinity_rm").value;
var ice = document.getElementById ("ice").value;
var salt = document.getElementById ("salt").value;
var temperature = document.getElementById ("temperature").value;
var other = document.getElementById ("other").value;
var ingredient_salinity_rm = document.getElementById ("ingredient_salinity_rm").value;

if(before_weight=='' || before_salinity_water=='' || before_salinity_rm=='' || after_weight=='' || after_salinity_rm=='' || before_salinity_rm=='' || ice=='' || salt=='' || temperature=='' || other=='' || ingredient_salinity_rm=='')
{
  swal("Error!", "All data is required!", "error")
}
else{
  showLoader();
  $.ajax({
    type: 'POST',
    url: "{{ URL::route('storetreatment', $temp_tally->id) }}",
    data: {
      '_token': $('input[name=_token]').val(),
      'before_weight': $('#before_weight').val(),
      'before_salinity_water': $('#before_salinity_water').val(),
      'before_salinity_rm': $('#before_salinity_rm').val(),
      'after_weight': $('#after_weight').val(),
      'after_salinity_rm': $('#after_salinity_rm').val(),
      'temp_tally_id': $('#temp_tally_id').val(),
      'before_duration': $('#before_duration').val(),
      'before_salinity_rm': $('#before_salinity_rm').val() ,
      'ice': $('#ice').val() ,
      'salt': $('#salt').val(),
      'temperature': $('#temperature').val(),
      'other': $('#other').val(),
      'ingredient_salinity_rm': $('#ingredient_salinity_rm').val()
    },
    success: function(data) {
      if ((data.errors)) {
       swal("Error!", "Gat data failed!", "error")
     } else {

       swal("Success!", "Data has been saved!", "success")
       hideLoader();

    $('.btn_collect').show(100);
       $('.peeling_type').show(100);
       $('.text_weight').hide(100);
       $('.back_salinity_rm').hide(100);
       $('.text_salinity_rm').hide(100);
       $('.treatment_option_active').show(100);
    $('.back_collect').hide(100);
    $('.back').hide(100);

       cleartext();

       @foreach($tally_detail as $key => $tally_detail_btn)
       $("#{{$tally_detail_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default");
       @endforeach
     }
   },

 })
}
};

function cleartext()
{
  $('#before_weight').val('');
  $('#before_salinity_water').val('');
  $('#before_salinity_rm').val('');
  $('#before_duration').val('');
  $('#after_weight').val('');
  $('#after_salinity_rm').val('');
  $('#ice').val('');
  $('#salt').val('');
  $('#temperature').val('');
  $('#ingredient_salinity_rm').val('');
  $('#other').val('');
  $('#tally_additional_master_id').val('');
  $('#weight_add').val('');
  $('#size_name').val('');
  $('#additional_name').val('');
  $('#calculate_treatment').val('');
  $('#breadcrumb').innerHTML = "";
};

function storetreatmentoption()
{
 var salinity = document.getElementById ("salinity").value;
 var duration = document.getElementById ("duration").value;
 if(salinity=='')
 {
  swal("Error!", "All data is required!", "error")
}
else{
  showLoader();
  $.ajax({
    type: 'POST',
    url: "{{ URL::route('storepeelingtreatment', $temp_tally->id) }}",
    data: {
      '_token': $('input[name=_token]').val(),
      'salinity': $('#salinity').val(),
      'duration': $('#duration').val(),
      'salinity_rm': $('#salinity_rm').val(),
      'temp_tally_id': $('#temp_tally_id').val()
    },
    success: function(data) {
      if ((data.errors)) {
       swal("Error!", "Gat data failed!", "error")
     } else {

       swal("Success!", "Data has been saved!", "success")
       hideLoader();

       $('.peeling_type').show(100);
       $('.text_duration').hide(100);
       $('.back_duration').hide(100);
    $('.back_collect').hide(100);


       $('.back_salinity_rm').hide(100);
       $('.text_salinity_rm').hide(100);

       $('.treatment_option_active').show(100);

       document.getElementById ("salinity").value = "";
       document.getElementById ("duration").value = "";
       document.getElementById ("salinity_rm").value = "";
       document.getElementById ("id").value = "";


       @foreach($tally_detail as $key => $tally_detail_btn)
       $("#{{$tally_detail_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default");
       @endforeach
     }
   },

 })
}
};

function storeclose ()
{
  swal({
    title: 'Are you sure?',
    text: "You won't be able to revert this peeling!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#d33',
    cancelButtonColor: '#BDBDBD',
    confirmButtonText: 'Yes, close it!'
  }).then(function () {

    $('#form_close').submit();
  })
};

function storecollect()
{

 var collect_tube = document.getElementById("collect_tube").value;
 var collect_tentacle = document.getElementById("collect_tube").value;
 if(collect_tube =='' && collect_tentacle =='')
 {
  swal("Error!", "All data is required!", "error")
}
else{
  $.ajax({
    type: 'POST',
    url: "{{ URL::route('collectpeelingtreatment', $tally_details->id) }}",
    data: {
      '_token': $('input[name=_token]').val(),
      'collect_tube': $('#collect_tube').val(),
      'collect_tentacle': $('#collect_tentacle').val(),
      'temp_tally_id': $('#temp_tally_id').val()
    },
    success: function(data) {
      if ((data.errors)) {
       swal("Error!", "Gat data failed!", "error")
     } else {

       swal("Success!", "Data has been saved!", "success")
       $('.text_collect').hide(100);
       $('.peeling_type').show(100);
       $('.btn_collect').show(100);
    $('.back_collect').hide(100);

       $('#text_collect').val('');

       @foreach($tally_detail as $key => $tally_detail_btn)
       $("#{{$tally_detail_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default");
       @endforeach
     }
   },

 })
}
};

$(document).ready(function() {
  $('#peeling_type').click(function() {

    $('.peeling_type').hide(100);
    $('.text_weight').show(100);
    $('.treatment_option_active').hide(100);
    $('.btn_collect').hide(100);
    $('.text_collect').hide(100);
    $('.back').show(100);

  });

  $('#back').click(function() {
    $('.peeling_type').show(100);
    $('.text_weight').hide(100);
    $('.back').hide(100);
    $('.treatment_option_active').show(100);
     $('.btn_collect').show(100);
     $('.text_collect').hide(100);
  });

  $('#treatment_option_active').click(function() {
    $('.peeling_type').hide(100);
    $('.treatment_option_active').hide(100);
    $('.back').hide(100);
    $('.text_salinity').show(100);
    $('.next_total_salinity').show(100);
    $('.back_salinity').show(100);

    var treatment_option_active = document.getElementById ("treatment_option_active").value;
    document.getElementById ("title_form").innerHTML = treatment_option_active;


  });

  $('#next_total_salinity').click(function() {
    $('.next_total_salinity').hide(100);
    $('.text_duration').show(100);
    $('.text_salinity').hide(100);
    $('.back_salinity').hide(100);
    $('.back_duration').show(100);
    $('.next_duration').show(100);
  });

  $('#next_duration').click(function() {
    $('.next_total_salinity').hide(100);
    $('.text_duration').hide(100);
    $('.text_salinity').hide(100);
    $('.back_salinity').hide(100);
    $('.back_duration').hide(100);
    $('.next_duration').hide(100);
    $('.text_salinity_rm').show(100);
    $('.back_salinity_rm').show(100);
  });

  $('#back_salinity').click(function() {
    $('.receiving_treatment').hide(100);
    $('.back_salinity').hide(100);
    $('.peeling_type').show(100);
    $('.additional').hide(100);
    $('.back_width').hide(100);
    $('.text_salinity').hide(100);
    $('.next_total_salinity').hide(100);
    $('.treatment_option_active').show(100);
  });

  $('#back_duration').click(function() {
    $('.receiving_treatment').hide(100);
    $('.back_duration').hide(100);
    $('.back_salinity').show(100);
    $('.text_salinity').show(100);
    $('.text_duration').hide(100);
    $('.next_duration').hide(100);
    $('.next_total_salinity').show(100);
  });

  $('#back_salinity_rm').click(function() {
    $('.receiving_treatment').hide(100);
    $('.back_duration').show(100);
    $('.back_salinity_rm').hide(100);
    $('.text_salinity_rm').hide(100);
    $('.text_duration').show(100);
    $('.next_duration').show(100);
  });

  $('#btn_collect').click(function() {
    $('.peeling_type').hide(100);
    $('.btn_collect').hide(100);
    $('.back_collect').show(100);
    $('.text_collect').show(100);
      $('.back').show(100);

  });

     $('#back_collect').click(function() {
    $('.peeling_type').show(100);
    $('.back_collect').hide(100);
    $('.text_collect').hide(100);

    $('.btn_collect').show(100);

  });


});

document.getElementById("loader").style.display = "none";

function showLoader() {
  document.getElementById("loader").style.display = "block";
}

function hideLoader() {
  document.getElementById("loader").style.display = "none";
}
</script>

