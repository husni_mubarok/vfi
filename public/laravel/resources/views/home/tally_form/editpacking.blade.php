@extends('layouts.tablet.template')
<meta name="_token" content="{{ csrf_token() }}"/>
<section class="myheader brown">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-4 heading">
                <h1>PACKING</h1>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
                <section class="info">
                       <a href=""><span class="no_label active">{{$temp_tally->trans_code}}<p><i>34 : 13 Minutes</i></p></span></a>
                       <a href=""><span class="no_label">{{$temp_tally->trans_code}}<p><i>34 : 13 Minutes</i></p></span></a>
                       <a href=""><span class="no_label disabled"><i class="fa fa-lock"></i>&nbsp;&nbsp;{{$temp_tally->trans_code}}<p><i>34 : 13 Minutes</i></p></span></a>
            </section>
            </div>
        </div>
    </section>
    <section class="myheader brown float">
        <div class="row">
            <div class="col-md-8 col-sm-8">
                <ul>
                    <li>
                        <a class="HOME" href="{{ URL::route('tally_form.index') }}">
                            <i class="fa fa-home"></i>
                            <p>HOME</p>
                        </a>
                    </li>
                    <li>
                        <a class="" title="PACKING HISTORY" href="{{ URL::route('tally_form.viewpacking', $temp_tally->id) }}">
                            <img src="{{Config::get('constants.path.img')}}/icon/back.svg" style="height: 35px;margin-bottom:0px;">
                            <p style="margin-top:5px;">BACK</p>
                        </a>
                    </li>

                </ul>
            </div>
        </div>
    </section>
   <section class="info dark">
       <div class="row">
           <div class="col-md-8 col-sm-8">
               <h3 class="mybreadcrumb"><b>EDIT PACKING</b></h3>
           </div>
       </div>
    </section>

    <br>
 <!--  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
         <div class="x_panel"> 
        <div class="x_content">   
          <div class="col-md-4 col-sm-4 col-xs-4">
           <center><h3><b>EDIT PACKING</b></h3></center> 
         </div> 
         <div class="col-md-4 col-sm-4 col-xs-4">
           <center><h1>{{$temp_tally->trans_code}}</h1></center>   
         </div>  
         {!! Form::open(array('route' => ['tally_form.closegrading', $temp_tally->id], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_close'))!!}
         {{ csrf_field() }}  
         <div class="col-md-1 col-sm-1 col-xs-1" style="padding: 5px">  
          <button type="button" id="btn_close" onclick="storeclose();" class="btn btn-danger btn-lg btn-block nomargin" style="padding: 10px 0;"><i class="fa fa-close faa-pulse animated"></i></button> 
        </div>
        {!! Form::close() !!}   

        <div class="col-md-1 col-sm-1 col-xs-1" style="padding: 5px">  
          <a href="{{ URL::route('tally_form.index') }}" type="button" class="btn btn-primary btn-lg btn-block nomargin"  style="padding: 10px 0;"><i class="fa fa-home faa-pulse animated"></i></a>
        </div> 
       
       <div class="col-md-1 col-sm-1 col-xs-1" style="padding: 5px">  
        <a href="{{ URL::route('tally_form.grading', $temp_tally->id) }}" type="button" class="btn btn-primary btn-lg pull-right btn-block nomargin" style="padding: 10px 0;"><i class="fa fa-wpforms faa-pulse animated"></i></a>
      </div> 
       
   </div>
 </div>
   </div> 
  <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: -30px">
   <hr>
   <p id="breadcrumb"></p>
 </div> 
</div>
 -->
  <div class="row"> 
    {!! Form::model($temp_tally, array('route' => ['tally_form.updatepacking', $temp_tally->id], 'method' => 'PUT', 'class'=>'update', 'id' => 'form_tally'))!!}  
    <div class="col-md-12 col-sm-12 col-xs-12"> 
      <div class="x_panel"> 
        <div class="x_content" style="overflow-y: auto;">   
          <table id="itemTable" class="table table-striped dataTable">
            <thead>
              <tr> 
                <th rowspan="2">#</th>
                <th rowspan="2">TYPE</th>
                <th rowspan="2">GRADE</th>
                <th rowspan="2">SIZE</th>  
                <th rowspan="2">FREEZING</th> 
                <th rowspan="2">PRODUCTION CODE</th>
                <th rowspan="2">BLOCK</th>
                <th rowspan="2">TOTAL</th>
                <th rowspan="2">TOTAL BLOCK</th>
                <th rowspan="2">IC</th>
                <th colspan="4"><center>TO BE SET</center></th>
              </tr> 
              <tr> 
                <th>MC</th>
                <th>MIX</th>
                <th>REST</th>  
                <th>ICR</th>  
              </tr> 
            </thead> 
            <tbody>
             @foreach($tally_detail as $key => $tally_details)
             <tr>
              <td>{{$key+1}}</td> 
              <td>{{$tally_details->qty_type}} 
              </td>
              <td>{{$tally_details->grade}} 
              </td>  
              <td>{{$tally_details->size}} 
              </td>
              <td>{{$tally_details->freezing}} 
              </td>
              <td>{{$tally_details->purchase_code}} | {{$tally_details->code_size}} 
              </td>
              <td>{{$tally_details->block_weight}} 
              </td>
              <td>{{$tally_details->total}} 
                {{ Form::hidden('tally['.$tally_details->id.'][total]', old($tally_details->id.'[total]', $tally_details->total)) }}
              </td>
              <td>{{$tally_details->total_block}}
                {{ Form::hidden('tally['.$tally_details->id.'][total_block]', old($tally_details->id.'[total_block]', $tally_details->total_block)) }}
              </td>
              <td>
                {{ Form::number('tally['.$tally_details->id.'][ic]', old($tally_details->id.'[ic]', $tally_details->ic), ['class' => 'form-control', 'id' => 'ic_'.$tally_details->id, 'min' => '0', 'onchange' => 'change_total_ic_'.$tally_details->id.'();']) }} 
              </td>
              <td>
               {{ Form::number('tally['.$tally_details->id.'][mc]', old($tally_details->id.'[mc]', $tally_details->mc), ['class' => 'form-control', 'id' => 'mc_'.$tally_details->id, 'min' => '0', 'onchange' => 'change_total_mc_'.$tally_details->id.'();']) }} 
             </td>
             <td>
               {{ Form::number('tally['.$tally_details->id.'][mix]', old($tally_details->id.'[mix]', $tally_details->mix), ['class' => 'form-control', 'id' => 'mix_'.$tally_details->id, 'min' => '0']) }} 
             </td>
             <td>
              {{$tally_details->rest}} 
            </td>
            <td>
             {{ Form::number('tally['.$tally_details->id.'][icr]', old($tally_details->id.'[icr]', $tally_details->icr), ['class' => 'form-control', 'id' => 'icr_'.$tally_details->id, 'min' => '0']) }} 
           </td>
         </tr>
         <script type="text/javascript">
          function change_total_ic_{{$tally_details->id}}()
          { 
            $('#ic_{{$tally_details->id}}').css('background', 'red'); 
            document.getElementById('flag_change').value = 1;
          }

          function change_total_mc_{{$tally_details->id}}()
          { 
            $('#mc_{{$tally_details->id}}').css('background', 'red');
            document.getElementById('flag_change').value = 1;
          }
        </script>
        @endForeach
      </tbody> 
    </table>  
  </div>
</div>
</div> 
</div>
</div> 

<input type="hidden" id="flag_change"> 
<div class="col-md-12 col-sm-12 col-xs-12">
  <br>
    <br>
    <br>
  <hr>
  <button type="button" id="btn_confirm" onclick="submit_form();" class="btn btn-success btn-lg btn-block"><i class="fa fa-check"></i> Save</button> 
</div>
{!! Form::close() !!}  

 

<script>

 function submit_form ()
 {  
  var change_total = document.getElementById('flag_change').value;
   if(change_total == 1)
   {
     swal({
      title: 'Are you sure?',
      text: "Total is change!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#BDBDBD',
      confirmButtonText: 'Yes, change it!'
    }).then(function () {

      $('#form_tally').submit(); 
    })
   }else{
    swal({
      title: 'Are you sure?', 
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#009933',
      cancelButtonColor: '#BDBDBD',
      confirmButtonText: 'Save'
    }).then(function () {

      $('#form_tally').submit(); 
    })
   }
 };  
</script>


