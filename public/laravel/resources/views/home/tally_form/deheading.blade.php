@extends('layouts.tabletform.template')

<!-- <style type="text/css">
  .btn-big{
    padding: 0px;
    height: 120px;
    vertical-align: middle;
    font-size: 3vw !important;
    white-space: initial !important;
  }
</style> -->
<meta name="_token" content="{{ csrf_token() }}"/>

<div id="all_element">
    <section class="myheader green">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-4 heading">
                <h1>@if (substr($temp_tally->trans_code,2,1)=='A') DEHEADING @else HEADSPLIT @endif</h1>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
                <section class="info">
                       <a href="">
                           <span class="no_label active">{{$temp_tally->trans_code}}
                            <p><i>Tentacle : 34 : 13 Minutes</i></p>
                            <p><i>Tentacle : 34 : 13 Minutes</i></p>
                            </span>
                        </a>
                       <a href="">
                           <span class="no_label">{{$temp_tally->trans_code}}
                            <p><i>Tentacle : 34 : 13 Minutes</i></p>
                            <p><i>Tentacle : 34 : 13 Minutes</i></p>
                            </span>
                        </a>
                        <a href="">
                           <span class="no_label disabled"><i class="fa fa-lock"></i>{{$temp_tally->trans_code}}
                            <p><i>Tentacle : 34 : 13 Minutes</i></p>
                            <p><i>Tentacle : 34 : 13 Minutes</i></p>
                            </span>
                        </a>
            </section>
            </div>
        </div>
    </section>
    <section class="myheader green float">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <ul>
                    <li>
                        <a class="HOME" href="{{ URL::route('tally_form.index') }}">
                            <i class="fa fa-home"></i>
                            <p>HOME</p>
                        </a>
                    </li>
                    <li>
                        <a class="" title="HISTORY" href="{{ URL::route('tally_form.historydeheading', $temp_tally->id) }}">
                            <i class="fa fa-file-text-o"></i>
                             <p>HSTRY</p>
                        </a>
                    </li>
                    <li>
                       
                        <a class="back"  id="back"  style="display: none">
                            <img src="{{Config::get('constants.path.img')}}/icon/back.svg" style="height: 35px;margin-bottom:0px;">
                            <p style="margin-top:5px;">BACK</p>
                        </a>
                        <a class="back_weight"  id="back_weight" style="display: none">
                            <img src="{{Config::get('constants.path.img')}}/icon/back.svg" style="height: 35px;margin-bottom:0px;">
                            <p style="margin-top:5px;">BACK</p>
                        </a>
                        <a class="back_collect"  id="back_collect" style="display: none">
                            <img src="{{Config::get('constants.path.img')}}/icon/back.svg" style="height: 35px;margin-bottom:0px;">
                            <p style="margin-top:5px;">BACK</p>
                        </a>
                        

                    </li>
                </ul>
            </div>
        </div>
    </section>
    

  
</div> 
<!-- <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: -30px">
 <hr> 
  <p id="breadcrumb"></p>
</div> -->  

<form class="form-horizontal" role="form">
  {{ csrf_field() }}
  <div class="col-md-12 col-sm-12 col-xs-12"> 
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel"> 
        <div class="x_content">   
          <div id="deheading_type" class="deheading_type">
            <h3 class="float_breadcrumb">TYPE</h3>
            @foreach($tally_detail as $key => $tally_details) 
            <div class="col-md-4 col-sm-4 col-xs-4 button"> 
              <a type="button" class="btn btn-black" id="{{$tally_details->id}}" onclick="set_btn_{{$tally_details->id}}_active();" value="{{$tally_details->id}}">
              <img src="{{Config::get('constants.path.img')}}/icon/kg.svg">
              <span id="{{$tally_details->name}}"> {!! $tally_details->name !!}</span></a>
              <a href="asd" class="btn btn-block btn-close"><i class="fa fa-unlock"></i>&nbsp;&nbsp;Close</a>
            </div>
            <script type="text/javascript">
             function set_btn_{{$tally_details->id}}_active()
             { 
              @foreach($tally_detail as $key => $tally_detail_btn)
              $("#{{$tally_detail_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-black"); 
              @endforeach
              $("#{{$tally_details->id}}").removeClass("btn btn-default").addClass("btn btn-black"); 

              var type_name = document.getElementById ("{{$tally_details->name}}").innerHTML; 
              document.getElementById ("type_name").value = type_name;
            };  
          </script> 
          @endforeach  

          {{ Form::hidden('tally_dehading_master_id',old('tally_dehading_master_id'), array('required' => 'true', 'id' => 'tally_dehading_master_id')) }} 
          {{ Form::hidden('purchase_id',old('purchase_id', $temp_tally->id_purchase), array('required' => 'true', 'id' => 'purchase_id')) }}  
          {{ Form::hidden('temp_tally_id',old('temp_tally_id', $temp_tally->id), array('required' => 'true', 'id' => 'temp_tally_id')) }} 
          {{ Form::hidden('type_name',old('type_name'), array('required' => 'true', 'id' => 'type_name')) }} 
          <br/> 
        </div>

        <div class="col-md-4 col-sm-4 col-xs-4 button btn_collect"> 
            <a id="btn_collect" class="btn btn-block btn-black btn_collect this_collect">
                <img src="{{Config::get('constants.path.img')}}/icon/collect.svg">
                <span>COLLECT</span>
            </a>
            <a class="btn btn-block btn-close btn_collect this_collect"><i class="fa fa-unlock"></i>&nbsp;&nbsp;Close</a>
        </div>

        <div id="text_weight" class="text_weight" style="display: none">
          <h3 class="float_breadcrumb">WEIGHT</h3>
          {{ Form::number('weight',old('weight'), array('class' => 'form-control', 'placeholder' => 'Weight*', 'required' => 'true', 'id' => 'weight', 'style' => 'height:70px; font-size:36px;')) }}<br>  

          <div class="row">
              <div class="col-md-8 col-sm-8 col-md-offset-2 col-sm-offset-2">
                    <a id="next_weight" href="#" type="button" class="btn btn-primary btn-lg  btn-block next_weight">
                        <i class="fa fa-arrow-circle-right"></i>&nbsp;&nbsp;NEXT
                    </a>
              </div>
          </div>
          
        </div>
        <div id="text_waste" class="text_waste" style="display: none">
          <h3 class="float_breadcrumb">WASTE</h3>
          {{ Form::number('waste',old('waste',0), array('class' => 'form-control', 'placeholder' => 'Waste*', 'required' => 'true', 'id' => 'waste', 'style' => 'height:70px; font-size:36px;')) }}<br>

            <div class="row">
                <div class="col-md-8 col-sm-8 col-md-offset-2 col-sm-offset-2">
                    <br>
                    <a href="#" class="btn btn-success btn-lg btn-block" onclick="store();"> <i class="fa fa-check"></i>&nbsp;&nbsp;SAVE</a>
                </div>
            </div>
        </div> 

        <div id="text_collect" class="text_collect" style="display: none">
          <h3>COLLECT</h3>
          {{ Form::number('collect',old('collect',0), array('class' => 'form-control', 'placeholder' => 'Collect*', 'required' => 'true', 'id' => 'collect', 'style' => 'height:70px; font-size:36px;')) }}<br>  
          <a href="#" class="btn btn-success btn-lg pull-right" onclick="storecollect();" style="padding: 35px; font-size: 20px"> <i class="fa fa-check"></i><br/> SAVE</a>
        </div> 
      </div>  
    </div>
  </div> 
</div>   
</form>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">
  function storeclose ()
  { 
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this headsplit!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#BDBDBD',
      confirmButtonText: 'Yes, close it!'
    }).then(function () {

      $('#form_close').submit();
    })
  };  

  $("button").click(function() {
    var fired_button = $(this).val();
    document.getElementById ("tally_dehading_master_id").value = fired_button; 
    document.getElementById("weight").focus();
    document.getElementById("waste").focus();
  }); 

  function store()
  { 
   var waste = document.getElementById ("waste").value;
   var weight = document.getElementById ("weight").value;
   var tally_dehading_master_id = document.getElementById ("tally_dehading_master_id").value;
   if(weight=='' || tally_dehading_master_id=='' || waste=='')
   {
    swal("Error!", "All data is required!", "error")
  }
  else{  
    $.ajax({
      type: 'POST',
      url: "{{ URL::route('storedeheading', $tally_details->id) }}",
      data: {
        '_token': $('input[name=_token]').val(), 
        'tally_dehading_master_id': $('#tally_dehading_master_id').val(),
        'weight': $('#weight').val(),
        'waste': $('#waste').val(),
        'purchase_id': $('#purchase_id').val(), 
        'temp_tally_id': $('#temp_tally_id').val()   
      },
      success: function(data) { 
        if ((data.errors)) {
         swal("Error!", "Gat data failed!", "error")
       } else {

         swal("Success!", "Data has been saved!", "success") 
         $('.deheading_type').show(100);
         $('.text_weight').hide(100);
         $('.text_waste').hide(100);
         $('.back').hide(100); 
         $('.back_weight').hide(100);
         $('.btn_collect').show(100);
         $('.back_collect').hide(100);

         $('#weight').val('');
         $('#waste').val('0');
         $('#id').val(''); 
         $('#type_name').val('');

         @foreach($tally_detail as $key => $tally_detail_btn)
         $("#{{$tally_detail_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default"); 
         @endforeach
       } 
     },

   })
  }
};

function storecollect()
{ 
 var collect = document.getElementById ("collect").value; 
 if(weight=='')
 {
  swal("Error!", "All data is required!", "error")
}
else{  
  $.ajax({
    type: 'POST',
    url: "{{ URL::route('collectdeheading', $tally_details->id) }}",
    data: {
      '_token': $('input[name=_token]').val(), 
      'collect': $('#collect').val(),    
      'temp_tally_id': $('#temp_tally_id').val()   
    },
    success: function(data) { 
      if ((data.errors)) {
       swal("Error!", "Gat data failed!", "error")
     } else {

       swal("Success!", "Data has been saved!", "success") 
       $('.text_collect').hide(100);
       $('.deheading_type').show(100);
       $('.btn_collect').show(100); 
       $('.back_collect').hide(100);

       $('#text_collect').val(''); 

       @foreach($tally_detail as $key => $tally_detail_btn)
       $("#{{$tally_detail_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default"); 
       @endforeach
     } 
   },

 })
}
};

$(document).ready(function() {
  $('#deheading_type').click(function() { 
    $('.deheading_type').hide(100);
    $('.text_weight').show(100);
    $('.back').show(100); 
    $('.next_weight').show(100);
    $('.btn_collect').hide(100);
  });

  $('#back').click(function() { 
    $('.deheading_type').show(100);
    $('.text_weight').hide(100);
    $('.back').hide(100); 
    $('.btn_collect').show(100);
  }); 

  $('#next_weight').click(function() { 
    $('.next_weight').hide(100);
    $('.text_weight').hide(100);
    $('.text_waste').show(100); 
    $('.back').hide(100); 
    $('.back_weight').show(100); 

  }); 

  $('#back_weight').click(function() { 
    $('.back_weight').hide(100);
    $('.back').show(100);
    $('.text_weight').show(100);
    $('.text_waste').hide(100); 
    $('.next_weight').show(100); 

  }); 

  $('#btn_collect').click(function() {  
    $('.deheading_type').hide(100);
    $('.btn_collect').hide(100);
    $('.back_collect').show(100);
    $('.text_collect').show(100); 

  }); 

  $('#back_collect').click(function() { 
    $('.deheading_type').show(100); 
    $('.this_collect').show(100); 
    $('.back_collect').hide(100);
    $('.text_collect').hide(100); 
  }); 

  $('#all_element').click(function() {   

   var type_name = document.getElementById ("type_name").value;  

   document.getElementById ("breadcrumb").innerHTML = type_name;
 });

}); 

</script>

