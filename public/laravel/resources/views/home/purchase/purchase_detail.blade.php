@extends('layouts.home.template')
@section('content')
<div class="header_menu mobile-hide">
    <a href="{{ URL::route('purchase.index') }}">PURCHASE</a>
</div>
@actionStart('purchase_detail', 'create|update')
<section class="content">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="x_panel">
                    <h2>
                        <i class="fa fa-dropbox"></i>&nbsp;Purchase Detail
                        <small>{{$purchase->description}}</small>
                    </h2>
                    <hr>
                    <div class="row bs-wizard small" style="border-bottom:0;">
                        <!-- class name =  1. complete 2.active 3.disabled-->
                        <div class="col-xs-2 bs-wizard-step complete"><!-- active -->
                          {{-- <div class="text-center bs-wizard-stepnum">Step 2</div> --}}
                          <div class="progress sm"><div class="progress-bar"></div></div>
                          <a href="#" class="bs-wizard-dot"></a>
                          <div class="bs-wizard-info text-center">Start<br><i>Create Purchase</i></div>
                        </div>

                        <div class="col-xs-2 bs-wizard-step complete"><!-- active -->
                          {{-- <div class="text-center bs-wizard-stepnum">Step 2</div> --}}
                          <div class="progress sm"><div class="progress-bar"></div></div>
                          <a href="#" class="bs-wizard-dot"></a>
                          <div class="bs-wizard-info text-center">Step 1<br><i>Register Price</i></div>
                        </div>

                        <div class="col-xs-2 bs-wizard-step complete"><!-- active -->
                          {{-- <div class="text-center bs-wizard-stepnum">Step 2</div> --}}
                          <div class="progress sm"><div class="progress-bar"></div></div>
                          <a href="#" class="bs-wizard-dot"></a>
                          <div class="bs-wizard-info text-center">Step 2<br><i>Waiting COO</i></div>
                        </div>

                        <div class="col-xs-2 bs-wizard-step active"><!-- active -->
                          {{-- <div class="text-center bs-wizard-stepnum">Step 2</div> --}}
                          <div class="progress sm"><div class="progress-bar"></div></div>
                          <a href="#" class="bs-wizard-dot"></a>
                          <div class="bs-wizard-info text-center">Step 3<br><i>Actual Delivery</i></div>
                        </div>

                        <div class="col-xs-2 bs-wizard-step disabled"><!-- active -->
                          {{-- <div class="text-center bs-wizard-stepnum">Step 4</div> --}}
                          <div class="progress sm"><div class="progress-bar"></div></div>
                          <a href="#" class="bs-wizard-dot"></a>
                          <div class="bs-wizard-info text-center">Step 4<br><i>Waiting QC</i></div>
                        </div>
                        <div class="col-xs-2 bs-wizard-step disabled"><!-- active -->
                          {{-- <div class="text-center bs-wizard-stepnum">Finish</div> --}}
                          <div class="progress sm"><div class="progress-bar"></div></div>
                          <a href="#" class="bs-wizard-dot"></a>
                          <div class="bs-wizard-info text-center">Finish<br><i>Summary</i></div>
                        </div>
                    </div>
                    <hr>
                    <div class="x_content">
                        <div class="col-md-6 col-xs-12 col-sm-12">
                            {{ Form::label('product', 'Product') }}&nbsp;<i class="fa fa-caret-right"></i>&nbsp;{{$purchase->purchase_product->product->product_type->name}}&nbsp;{{$purchase->purchase_product->product->name}}<br/>

                            @php
                                $ptc = $purchase->trans_code;
                                $ptc3 = $ptc[2]; 
                            @endphp

                            @if($ptc3 == 'B')
                                <label for="size">Size</label>
                                <select name="sizeFrozen" id="sizeFrozen" class="form-control">
                                    <option value="0">- SELECT -</option>
                                    <option value="1">L</option>
                                    <option value="2">M</option>
                                    <option value="3">S</option>
                                </select>
                            @endif

                            @if($ptc3 != 'B')
                                {{ Form::label('size', 'Size') }}
                            @endif

                            @if($ptc3 == 'B')
                                {{ Form::hidden('size', old('size'), ['id' => 'size', 'min' => '0', 'class' => 'form-control', 'step' => '1']) }}
                            @else
                                {{ Form::number('size', old('size'), ['id' => 'size', 'min' => '0', 'class' => 'form-control', 'step' => '1']) }}
                            @endif

                            <br/>

                            {{ Form::label('amount', 'Weight') }}
                            {{ Form::number('amount', old('amount'), ['id' => 'amount', 'min' => '0', 'class' => 'form-control']) }}<br/>

                            {{ Form::label('price', 'Price') }}
                            {{ Form::text('price', old('price'), ['id' => 'price', 'class' => 'form-control', 'disabled' => 'true']) }}<br/>
                            {{-- <div id="price_text">0</div><br/> --}}

                            <button type="button" id="btn_add_product" class="btn btn-default"><i class="fa fa-cart-plus"></i>&nbsp;Add</button>
                            <br/>
                            @include('errors.error')
                            {!! Form::open(array('action' => ['Home\PurchaseController@store_purchase_detail', $purchase->id], 'id' => 'form_purchase_detail'))!!}  

                            <input type="hidden" name="trans_code" id="trans_code" value="{{$purchase->trans_code}}">

                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Size</th>
                                        <th>Price @ kg</th>
                                        <th>Amount (kg)</th>
                                        <th>Total</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="cart">
                                </tbody>
                            </table>
                            <button type="button" id="btn_submit" class="btn btn-success pull-right" data-toggle="modal" data-target="#confirmation_modal"><i class="fa fa-check"></i> Save</button>
                            {!! Form::close() !!}
                        </div>
                       
                        <div class="col-md-6 col-xs-12 col-sm-12 mobile-hide">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        @if($ptc3 != 'B')
                                            <th>Min.</th>
                                            <th>Max.</th>
                                        @endif
                                        
                                        @if($ptc3 == 'B')
                                            <th>Size</th>
                                        @else
                                            <th>Note</th>
                                        @endif
                                        <th>Price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($purchase->price as $price)
                                    <tr>
                                        @if($ptc3 != 'B')
                                            <td>{{$price->size->min_value}}</td>
                                            <td>{{$price->size->max_value}}</td>
                                        @endif
                                        <td>{{$price->size->note}}</td>
                                        <td>{{number_format($price->value)}}&nbsp;{{$purchase->currency}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@actionEnd
@stop
    
@section('modal')
<div class="modal fade" id="confirmation_modal" tabindex="-1" role="dialog" aria-labelledby="modal_label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal_label">Purchase Detail Summary</h4>
            </div>
            <div class="modal-body">
                Confirm Purchase Detail?
            </div>
            <div class="modal-footer">
                <button type="button" id="btn_confirm" class="btn btn-success"><i class="fa fa-check"></i> YES</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i>&nbsp;NO</button>
            </div>
        </div>
    </div>
</div>
@stop

@section('scripts')
@if(isset($purchase_details))
<script>
$(document).ready(function(){
    var cart_content = {!! $purchase_details !!}
    jQuery.each( cart_content, function( i, val ) {
        var prependTable = '';
        
        prependTable += '<tr>';
        prependTable += '<td>'+val['size']+'<input type="hidden" name="size[]" value="'+val['size']+'"></td>';
        prependTable += '<td align="left">'+val['price']+'<input type="hidden" name="price[]" value="'+val['price']+'"></td>';
        prependTable += '<td align="left">'+val['amount']+'<input type="hidden" name="amount[]" value="'+val['amount']+'"></td>';
        prependTable += '<td align="left">'+val['subtotal']+'<input type="hidden" class="subtotal" value="'+val['subtotal']+'"></td>';
        prependTable += '<td><button type="button" class="btn_remove_product btn"><i class="fa fa-remove"></i></button></td>';
        prependTable += '</tr>';
        $("#cart").append(prependTable);
        
        $(".btn_remove_product").click(function(){
            $(this).parent().parent().remove(); 
        });

        $("#submit_button").replaceWith('<button type="button" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>');
    });
})
</script>
@endif
<script>
$("#size").on('change', function()
{
    $("#price").val('Calculating Price...');
    $("#btn_add_product").attr('disabled', true);
    $.ajax({
        url : '{{ URL::route('get.price') }}',
        data : {'size':$("#size").val(), 'purchase_id':{!! $purchase->id !!}, 'product_type_id':{!! $purchase->purchase_product->product->product_type->id !!}},
        type : 'GET',
        headers : {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
        success : function(data, textStatus, jqXHR){
            // console.log(data);
            $("#price").val(data);
            $("#btn_add_product").attr('disabled', false);
        },
        error : function()
        {
            $("#price").val('Error!');
        },
    })
});


$("#sizeFrozen").on('change', function()
{
    var thisVal = $('#sizeFrozen').val();
    $("#size").val(thisVal);
    $("#price").val('Calculating Price...');
    $("#btn_add_product").attr('disabled', true);
    $.ajax({
        url : '{{ URL::route('get.price') }}',
        data : {'size':$("#size").val(), 'purchase_id':{!! $purchase->id !!}, 'product_type_id':{!! $purchase->purchase_product->product->product_type->id !!}},
        type : 'GET',
        headers : {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
        success : function(data, textStatus, jqXHR){
            // console.log(data);
            $("#price").val(data);
            $("#btn_add_product").attr('disabled', false);
        },
        error : function()
        {
            $("#price").val('Error!');
        },
    })
});
    
$("#btn_add_product").click(function(){
    if($("#size").val() && $("#amount").val() && $("#price").val())
    {
        var outputString = '';
        outputString += '<tr>';
        outputString += '<td>'+$("#size").val()+'<input type="hidden" name="size[]" value="'+$("#size").val()+'"></td>';
        outputString += '<td align="left">'+$("#price").val()+'<input type="hidden" name="price[]" value="'+$("#price").val()+'"></td>';
        outputString += '<td align="left">'+$("#amount").val()+'<input type="hidden" name="amount[]" value="'+$("#amount").val()+'"></td>';
        outputString += '<td align="left">'+$("#amount").val() * $("#price").val()+'<input type="hidden" class="subtotal" value="'+$("#amount").val() * $("#price").val()+'"></td>';
        outputString += '<td><button type="button" class="btn_remove_product btn btn-danger"><i class="fa fa-remove"></i></button></td>';
        outputString += '</tr>';

        $("#cart").append(outputString);
        $("#btn_submit").attr("disabled", false);
        $("#size").val('');
        $("#amount").val('');
        $("#price").val('');

        $(".btn_remove_product").click(function(){
            $(this).parent().parent().remove(); 
        });
    }
});

$("#btn_submit").on('click', function() {

});

$("#btn_confirm").on('click', function() {
    $("#form_purchase_detail").submit();
});
</script>
@stop