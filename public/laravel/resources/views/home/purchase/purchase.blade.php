@extends('layouts.home.template')
@section('content')
<div class="header_menu mobile-hide">
	<a href="{{ URL::route('purchase.index') }}">PURCHASE</a>
</div>
@actionStart('purchase', 'create|update')
<section class="content">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<div class="x_panel">
					<h2>
						@if(isset($purchase))
						<i class="fa fa-pencil"></i>
						@else
						<i class="fa fa-plus"></i> 
						@endif
						<i class="fa fa-shopping-cart"></i> Purchase
					</h2>
					<hr>
					<div class="row bs-wizard small" style="border-bottom:0;">
						<!-- class name =  1. complete 2.active 3.disabled-->
						<div class="col-xs-2 bs-wizard-step active"><!-- active -->
							{{-- <div class="text-center bs-wizard-stepnum">Step 2</div> --}}
							<div class="progress sm"><div class="progress-bar"></div></div>
							<a href="#" class="bs-wizard-dot"></a>
							<div class="bs-wizard-info text-center">Start<br><i>Create Purchase</i></div>
						</div>

						<div class="col-xs-2 bs-wizard-step disabled"><!-- active -->
							{{-- <div class="text-center bs-wizard-stepnum">Step 2</div> --}}
							<div class="progress sm"><div class="progress-bar"></div></div>
							<a href="#" class="bs-wizard-dot"></a>
							<div class="bs-wizard-info text-center">Step 1<br><i>Register Price</i></div>
						</div>

						<div class="col-xs-2 bs-wizard-step disabled"><!-- active -->
							{{-- <div class="text-center bs-wizard-stepnum">Step 2</div> --}}
							<div class="progress sm"><div class="progress-bar"></div></div>
							<a href="#" class="bs-wizard-dot"></a>
							<div class="bs-wizard-info text-center">Step 2<br><i>Waiting COO</i></div>
						</div>

						<div class="col-xs-2 bs-wizard-step disabled"><!-- active -->
							{{-- <div class="text-center bs-wizard-stepnum">Step 2</div> --}}
							<div class="progress sm"><div class="progress-bar"></div></div>
							<a href="#" class="bs-wizard-dot"></a>
							<div class="bs-wizard-info text-center">Step 3<br><i>Actual Delivery</i></div>
						</div>

						<div class="col-xs-2 bs-wizard-step disabled"><!-- active -->
							{{-- <div class="text-center bs-wizard-stepnum">Step 4</div> --}}
							<div class="progress sm"><div class="progress-bar"></div></div>
							<a href="#" class="bs-wizard-dot"></a>
							<div class="bs-wizard-info text-center">Step 4<br><i>Waiting QC</i></div>
						</div>
						<div class="col-xs-2 bs-wizard-step disabled"><!-- active -->
							{{-- <div class="text-center bs-wizard-stepnum">Finish</div> --}}
							<div class="progress sm"><div class="progress-bar"></div></div>
							<a href="#" class="bs-wizard-dot"></a>
							<div class="bs-wizard-info text-center">Finish<br><i>Summary</i></div>
						</div>
					</div>
					<hr>
					<div class="x_content">
						@include('errors.error')
						@if(isset($purchase))
						{!! Form::model($purchase, array('route' => ['purchase.update', $purchase->id], 'method' => 'PUT', 'id' => 'form_purchase'))!!}
						@else
						{!! Form::open(array('route' => 'purchase.store', 'id' => 'form_purchase'))!!}
						@endif
						{{ csrf_field() }}
						<div class="col-md-6 col-sm-12 col-xs-12 form-group"> 
							<input type="hidden" name="product_code" id="product_code">
							<input type="hidden" name="supplier_code" id="supplier_code">

							{{ Form::label('condition', 'Condition*') }}
							<select name="condition" id="condition" class="form-control">
								<option value="">Select a condition</option> 
								<option value="A">Fresh</option>
								<option value="B">Frozen</option>
							</select><br/>

							{{ Form::label('description', 'Description*') }}
							{{ Form::text('description', old('description'), array('class' => 'form-control', 'placeholder' => 'Description', 'required' => 'true')) }}<br/>

							{{ Form::label('project', 'Project*') }}
							<select name="project" id="project" class="form-control" disabled>
								<option value="" selected disabled>Select a Project</option>
							</select><br/>
							{{-- {{ Form::text('project', old('project'), array('class' => 'form-control', 'placeholder' => 'Project*', 'required' => 'true')) }}<br/> --}}

							{{ Form::label('supplier_id', 'Supplier*') }}
							@if(isset($purchase))
							<p>{{$purchase->purchase_supplier->supplier->name}}</p>
							{{ Form::hidden('supplier_id', $purchase->purchase_supplier->supplier->id) }}<br/>
							@else
							{{ Form::select('supplier_id', $suppliers, null, ['class' => 'form-control', 'placeholder' => 'Select a Supplier']) }}<br/>
							@endif

							{{-- {{ Form::label('product_id', 'Product') }}
							<select name="product_id" id="product_id" class="form-control">
								<option value="" selected disabled>Select a product</option>
								@foreach($products as $key => $product)
								<option value="{{ $key }}">{{ $product }}</option>
								@endforeach
							</select><br/>

							{{ Form::label('product_id', 'Type') }}
							<select name="product_id" id="product_id" class="form-control" disabled>
							</select><br/> --}}

							{{ Form::label('product_id', 'Product*') }}
							@if(isset($purchase))
							<p>{{$purchase->purchase_product->product->name}}</p>
							{{ Form::hidden('product_id', $purchase->purchase_product->product->id) }}<br/>
							@else
							<select name="product_id" id="product_id" class="form-control" disabled>
							</select><br/>
							@endif

							{{-- {{ Form::label('size', 'Size') }}
							{{ Form::number('size', old('size'), array('class' => 'form-control', 'placeholder' => 'Size*', 'required' => 'true')) }}<br/> --}}

							{{ Form::label('quantity', 'Quantity*') }}
							<div class="input-group">
								{{ Form::number('quantity', old('quantity'), array('class' => 'form-control', 'placeholder' => 'Quantity', 'required' => 'true')) }}
								<div class="input-group-addon">kg</div>
							</div>
							<br/>

							<div class="col-md-8">
								{{ Form::label('estimated_price', 'Estimated Price*') }}
								{{ Form::number('estimated_price', old('estimated_price'), array('class' => 'form-control', 'placeholder' => 'Estimated Price', 'min' => '0', 'required' => 'true')) }}<br/>
							</div>
							{{-- {{ Form::label('division', 'Division') }}
							{{ Form::text('division', old('division'), array('class' => 'form-control', 'placeholder' => 'Division*', 'required' => 'true')) }}<br/>

							{{ Form::label('reference', 'Reference') }}
							{{ Form::text('reference', old('reference'), array('class' => 'form-control', 'placeholder' => 'Reference*', 'required' => 'true')) }}<br/> --}}
							<div class="col-md-4">
								{{ Form::label('currency', 'Currency*') }}
								{{ Form::select('currency', ['IDR' => 'IDR', 'USD' => 'USD'], null, array('class' => 'form-control', 'placeholder' => 'Select a Currency')) }}<br/>
							</div>

							{{ Form::label('ppn', 'Include PPN*') }}
							{{ Form::select('ppn', ['on' => 'Yes', 'off' => 'No'], null, array('class' => 'form-control', 'placeholder' => 'Select PPN status')) }}<br/>

							{{ Form::label('discount', 'Discount*') }}
							{{ Form::number('discount', old('discount'), array('class' => 'form-control', 'placeholder' => 'Discount', 'min' => '0', 'required' => 'true')) }}<br/>

							{{ Form::label('down_payment', 'DP (Down Payment)*') }}
							{{ Form::number('down_payment', old('down_payment'), array('class' => 'form-control', 'placeholder' => 'Down Payment', 'min' => '0', 'required' => 'true')) }}<br/>

							{{ Form::label('term_condition', 'Terms of Conditions*') }}
							{{ Form::textarea('term_condition', old('term_condition'), array('class' => 'form-control', 'placeholder' => 'Terms of Conditions', 'required' => 'true', 'rows' => '3', 'id' => 'term_condition')) }}<br/>

							{{ Form::label('term_payment', 'Terms of Payment*') }}
							{{ Form::textarea('term_payment', old('term_payment'), array('class' => 'form-control', 'placeholder' => 'Terms of Payment', 'required' => 'true', 'rows' => '3', 'id' => 'term_payment')) }}<br/>

							{{ Form::label('purchase_time', 'Date & Time*') }}
							{{ Form::text('purchase_time', old('purchase_time'), array('class' => 'form-control', 'placeholder' => 'Date & Time', 'required' => 'true', 'id' => 'purchase_time')) }}<br/>

							{{ Form::label('notes', 'Notes') }}
							{{ Form::text('notes', old('notes'), array('class' => 'form-control', 'placeholder' => 'Notes')) }}<br/>

							<button type="button" id="btn_submit" data-toggle="modal" data-target="#confirmation_modal" class="btn btn-success pull-right"></i> Save</button>
						</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@actionEnd
@stop

@section('modal')
<div class="modal fade" id="confirmation_modal" tabindex="-1" role="dialog" aria-labelledby="modal_label" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="modal_label">Purchase Summary</h4>
				</div>
				<div class="modal-body">
					Confirm Purchase?
				</div>
				<div class="modal-footer">
					<button type="button" id="btn_confirm" class="btn btn-success"><i class="fa fa-check"></i> YES</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i>&nbsp;NO</button>
				</div>
			</div>
		</div>
	</div>
	@stop

	@section('scripts')
	<script>
		$(document).ready(function () {
			CKEDITOR.replace( 'term_condition' );
			CKEDITOR.replace( 'term_payment' );
			
			$('#purchase_time').datetimepicker({
				sideBySide: true,
				format: 'YYYY-MM-DD HH:mm',
			});

			$.ajax({
				url : 'http://36.68.225.13:85/ims99dummyvfi/general/project_api.php',
				data : null, 
				type : 'GET',
				headers : {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
				success : function(data, textStatus, jqXHR){
					if(data.length == 0)
					{
						$('#project').empty();
						$('#project').append('<option disabled selected value=""> No Approved Project </option>');
					} else {
						$('#project').empty();
						$('#project').append('<option disabled selected value=""> Select a Project </option>');
						jQuery.each(data, function(i, val)
						{
							$('#project').append($('<option>', { 
								value: val['name'],
								text : val['name']+' | Target: ['+val['target']+' kg]', 
							}));
						});
						$("#project").attr('disabled', false);
					}
				},
				error : function()
				{
					console.log('error');
					$('#project').empty();
					$('#project').append('<option disabled selected value=""> Connection error, please refresh browser! </option>');
				},
			})
		});

		$("#btn_submit").on('click', function()
		{

		});

		$("#btn_confirm").on('click', function()
		{
			$("#form_purchase").submit();
		});
	</script>

	<script>
		$("#supplier_id").on('change', function()
		{
			$.ajax({
				url : '{{ URL::route('get.product') }}',
				data : {'supplier_id':$("#supplier_id").val()},
				type : 'GET',
				headers : {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
				success : function(data, textStatus, jqXHR){
					console.log(data);
					$('#product_id').empty(); 
					$('#product_id').append($('<option disabled selected>', { 
						value: null,
						text : 'Select a Product' 
					}));

					jQuery.each(data, function(i, val)
					{
						$('#product_id').append($('<option>', { 
							value: val.id,
							text : val.parent+" "+val.name 
						}));
					});
					$("#product_id").attr('disabled', false);
				},
				error : function()
				{
					$('#product_id').empty();
					$('#product_id').attr('disabled', true);
				},
			})
		});
		$("#supplier_id").on('click', function()
		{
			$.ajax({
				url : '{{ URL::route('get.rs_supplier_data') }}',
				data : {'supplier_id':$("#supplier_id").val()},
				type : 'GET',
				headers : {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
				success : function(data, textStatus, jqXHR){  
					jQuery.each(data, function(i, val)
					{
						//console.log(data[].num_padded);
						document.getElementById("supplier_code").value = data[0].id; 
					}); 
				},
				error : function()
				{
					$('#supplier_code').empty();   
				},
			})
		});

		$("#product_id").on('click', function()
		{
			$.ajax({
				url : '{{ URL::route('get.rs_product_data') }}',
				data : {'product_id':$("#product_id").val()},
				type : 'GET',
				headers : {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
				success : function(data, textStatus, jqXHR){  
					jQuery.each(data, function(i, val)
					{
						document.getElementById("product_code").value = data[0].code; 
					}); 
				},
				error : function()
				{
					$('#product_code').empty();   
				},
			})
		});
	</script>
	@stop