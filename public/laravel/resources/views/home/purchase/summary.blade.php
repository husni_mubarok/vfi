@extends('layouts.home.template')
@section('content')
<div class="header_menu mobile-hide">
    <a href="{{ URL::route('purchase.index') }}">PURCHASE</a>
</div>
@actionStart('purchase', 'read')
<section class="content"> 
		        <div class="x_panel">
		        	<div class="row">
		        		<div class="col-md-8 col-sm-12">
		        			<h2>
			                	<i class="fa fa-file-text-o"></i> 
			                	{{$purchase->doc_code}}
			                	<i class="fa fa-caret-right"></i>
			                	{{$purchase->description}}
			                	@if($purchase->status == 'PO RM' || $purchase->status == 'Actual Delivery')
								<span class="label label-danger">{{$purchase->status}}</span> <i class="fa fa-caret-right"></i> {{$purchase->trans_code}}
								@elseif($purchase->status == 'Waiting COO' || $purchase->status == 'Waiting QC')
								<span class="label label-warning">{{$purchase->status}}</span>
								@elseif($purchase->status == 'Approved')
								<span class="label label-success">{{$purchase->status}}</span>
								@endif

								
		                	</h2>
		                	<br>
		        		</div>

		        		<div class="col-md-4 col-sm-12">
		        			<div class="pull-right">
		        					<a href="{{ URL::route('purchase.index') }}" class="btn btn-primary btn-lg"><i class="fa fa-list"></i> List</a>

									<button type="button" data-toggle="modal" data-target="#reject_history" class="btn btn-default btn-lg"><i class="fa fa-history"></i>&nbsp;Reject</button>

				                	@if($purchase->status == 'Approved')
				                	{{-- <a href="{{ URL::route('editor.purchase.pdf', $purchase->id) }}" class="btn btn-default btn-lg"><i class="fa fa-file-pdf-o"></i>&nbsp;Download</a> --}}
				                	@actionStart('purchase', 'approve')
				                	<button type="button" data-toggle="modal" data-target="#close_purchase" class="btn btn-danger btn-lg"><i class="fa fa-lock"></i>&nbsp;Close</button>
				                	@actionEnd

				                	@elseif($purchase->status == 'PO RM' || $purchase->status == 'Actual Delivery')
				                	@actionStart('purchase', 'update')
				                	<a href="{{ URL::route('purchase.edit', $purchase->id) }}" class="btn btn-default btn-lg"><i class="fa fa-pencil"></i>&nbsp;Edit</a>
				                	@actionEnd
				                	@endif
			                	</div>
		        		</div>
		        	</div>
	                
	                <hr>
	                <div class="row bs-wizard small" style="border-bottom:0;">
		                <!-- class name =  1. complete 2.active 3.disabled-->
		                <div class="col-xs-2 bs-wizard-step complete"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Step 2</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Start<br><i>Create Purchase</i></div>
		                </div>

		                <div class="col-xs-2 bs-wizard-step @if($purchase->status == 'PO RM') active @else complete @endif"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Step 2</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Step 1<br><i>Register Price</i></div>
		                </div>

		                <div class="col-xs-2 bs-wizard-step @if($purchase->status == 'Waiting COO') active @elseif($purchase->status == 'Actual Delivery' || $purchase->status == 'Waiting QC' || $purchase->status == 'Approved') complete @else disabled @endif"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Step 2</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Step 2<br><i>Waiting COO</i></div>
		                </div>

		                <div class="col-xs-2 bs-wizard-step @if($purchase->status == 'Actual Delivery') active @elseif($purchase->status == 'Waiting QC' || $purchase->status == 'Approved') complete @else disabled  @endif"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Step 2</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Step 3<br><i>Actual Delivery</i></div>
		                </div>

		                <div class="col-xs-2 bs-wizard-step @if($purchase->status == 'Waiting QC') active @elseif($purchase->status == 'Approved') complete @else disabled @endif"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Step 4</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Step 4<br><i>Waiting QC</i></div>
		                </div>

		                <div class="col-xs-2 bs-wizard-step @if($purchase->status == 'Approved') complete @else disabled @endif"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Finish</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Finish<br><i>Summary</i></div>
		                </div>
		            </div>
	                <hr>
		            <div class="x_content">
		            	<div class="col-md-6">
		            		<h4><i class="fa fa-info-circle"></i>&nbsp;Information</h4>
		            		<div style="overflow: auto;border: 1px solid #eaeaea">
			            		<table class="table table-bordered">
			            			<tr>
			            				<th>Supplier</th>
			            				<td>
			            					<button type="button" data-toggle="modal" data-target="#supplier_detail" class="btn btn-default btn-sm"><i class="fa fa-search"></i>&nbsp;
			            					{{$purchase->purchase_supplier->supplier->name}}
			            					</button>
		            					</td>
			            				<th>Estimated Price</th>
			            				<td>{{$purchase->currency}}&nbsp;{{number_format($purchase->estimated_price)}}</td>
			            			</tr>
			            			<tr>
			            				<th>Purchase Time</th>
			            				<td>{{date("D, d M Y", strtotime($purchase->purchase_time))}}</td>
			            				<th>PPN</th>
			            				<td>@if($purchase->ppn == 'on') Yes @else No @endif</td>
			            			</tr>
			            			<tr>
			            				<th>Product</th>
			            				<td>{{$purchase->purchase_product->product->product_type->name}}&nbsp;{{$purchase->purchase_product->product->name}}</td>
			            				<th>DP</th>
			            				<td>{{$purchase->currency}}&nbsp;{{number_format($purchase->down_payment)}}</td>
			            			</tr>
			            			<tr>
			            				<th>Project</th>
			            				<td>{{$purchase->project}}</td>
			            				<th>Discount</th>
			            				<td>{{$purchase->currency}}&nbsp;{{number_format($purchase->discount)}}</td>
			            			</tr>    
			            			<tr>
			            				<th>Terms of Condition</th>
			            				<td>
			            					<button type="button" data-toggle="modal" data-target="#term_condition" class="btn btn-default btn-sm"><i class="fa fa-list-ol"></i>&nbsp;Show</button>
			            				</td>
			            				<th>Terms of Paymet</th>
			            				<td>
			            					<button type="button" data-toggle="modal" data-target="#term_payment" class="btn btn-default btn-sm"><i class="fa fa-money"></i>&nbsp;Show</button>
			            				</td>
			            			</tr>         			
			            			{{-- <tr>
			            				@php
			            				$reject = '-';
			            				foreach($purchase->purchase_reject as $purchase_reject)
			            				{
			            					$reject = $purchase_reject->reason;	
			            				}

			            				@endphp
			            				<th>Reject</th>
			            				<td>@if($purchase->purchase_reject->count() == 0) No @else Yes @endif</td>
			            				<th>Reason</th>
			            				<td>{{$reject}}</td>
			            			</tr> --}}
			            		</table>
			            	</div>
		            		@if($purchase->price->count() > 0)
		            		@actionStart('price', 'read')
		            		<hr>

		            		@php
			            		$ptc = $purchase->trans_code;
			            		$ptc3 = $ptc[2]; 
			            	@endphp

		            		<h4><i class="fa fa-dollar"></i>&nbsp;Price List</h4>
		            		<div style="overflow: auto;border: 1px solid #eaeaea">
			            		<table class="table table-bordered">
			            			<tr>
			            				<th>No.</th>
			            				@if($ptc3 != 'B')
											<th>Min.</th>
			            					<th>Max.</th>
										@endif

										@if($ptc3 == 'B')
											<th>Size</th>
			            				@else
			            					<th>Note</th>
			            				@endif
			            				
			            				<th>Price ({{$purchase->currency}})</th>
			            			</tr>
			            			@foreach($purchase->price as $key => $price)
			            			<tr>
			            				<td>{{$key+1}}</td>
			            				
			            				@if($ptc3 != 'B')
											<td>{{$price->size->min_value}}</td>
			            					<td>{{$price->size->max_value}}</td>
										@endif

			            				<td>{{$price->size->note}}</td>
			            				<td>{{number_format($price->value)}}&nbsp;{{$purchase->currency}}</td>
			            			</tr>
			            			@endforeach
			            		</table>
			            	</div>
		            		@actionEnd
		            		@endif

							<div align="right">
								@if($purchase->status == 'PO RM')
									@if($purchase->price->count() == 0)
									@actionStart('price', 'create')
									<a href="{{ URL::route('price.create', $purchase->id) }}" style="display:inline-block;" class="btn btn-primary"><i class="fa fa-dollar"></i>&nbsp;Price</a>
									@actionEnd
									@else
									@actionStart('price', 'update')
									<a href="{{ URL::route('price.edit', $purchase->id) }}" style="display:inline-block;" class="btn btn-primary"><i class="fa fa-dollar"></i>&nbsp;Price</a>
									@actionEnd
									@actionStart('purchase', 'update')
									<a href="{{ URL::route('purchase.confirm_po_rm', $purchase->id) }}" class="btn btn-primary" onclick="return confirm('Confirm Purchase?')"><i class="fa fa-lock"></i>&nbsp;Confirm</a>
									@actionEnd
									@endif

								@elseif($purchase->status == 'Waiting COO')
									@actionStart('price', 'approve')
									<button type="button" class="btn btn-success" data-toggle="modal" data-target="#approve_po_rm_form"><i class="fa fa-check"></i>&nbsp;Approve</button>
									@actionEnd

									@actionStart('price', 'reject')
									<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#reject_po_rm_form"><i class="fa fa-remove"></i>&nbsp;Reject</button>
									@actionEnd

								@elseif($purchase->status == 'Actual Delivery')
									@if($purchase->purchase_detail->count() == 0)
									@actionStart('purchase_detail', 'create')
									<a href="{{ URL::route('purchase_detail.create', $purchase->id) }}" style="display:inline-block;" class="btn btn-primary"><i class="fa fa-shopping-cart"></i>&nbsp;Cart</a>
									@actionEnd
									@else
									@actionStart('purchase_detail', 'update')
									<a href="{{ URL::route('purchase_detail.edit', $purchase->id) }}" style="display:inline-block;" class="btn btn-primary"><i class="fa fa-shopping-cart"></i>&nbsp;Cart</a>
									@actionEnd
									@actionStart('purchase', 'update')
									<a href="{{ URL::route('purchase.confirm_actual_delivery', $purchase->id) }}" class="btn btn-primary" onclick="return confirm('Confirm Purchase?')"><i class="fa fa-lock"></i>&nbsp;Confirm</a>
									@actionEnd
									@endif

								@elseif($purchase->status == 'Waiting QC')
									@actionStart('purchase_detail', 'approve')
									<button type="button" class="btn btn-success" data-toggle="modal" data-target="#approve_actual_delivery_form"><i class="fa fa-check"></i>&nbsp;Approve</button>
									@actionEnd

									@actionStart('purchase_detail', 'reject')
									<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#reject_actual_delivery_form"><i class="fa fa-remove"></i>&nbsp;Reject</button>
									@actionEnd

								@endif
							</div>
		            	</div>
		            	<div class="col-md-6">
		            		@actionStart('purchase_detail', 'read')
							@if($purchase->purchase_detail->count() > 0)
							<h4><i class="fa fa-shopping-cart"></i>&nbsp;Purchase Detail</h4>
			            	@php $total = 0; $total_weight = 0; @endphp
			            	<div style="overflow: auto;border: 1px solid #eaeaea">
				                <table class="table table-bordered table-hover">
				                	<thead>
								  	  	<tr>
									      	<th>No.</th>
											<th>Size</th>
											<th>Amount (kg)</th>
											<th>Price ({{$purchase->currency}})</th>
											<th>Subtotal</th>
								    	</tr>
							    	</thead>
							    	<tbody>
				                	@foreach($purchase->purchase_detail as $key => $purchase_detail)
				                		@php
				                		$subtotal = $purchase_detail->amount * $purchase_detail->price;
				                		$total += $subtotal;
				                		$total_weight += $purchase_detail->amount;
				                		@endphp

				                		@php
			            					$ptc = $purchase->trans_code;
			            					$ptc3 = $ptc[2]; 
			            				@endphp

				                		@php
											$size_tag = array(1=>'L', 'M', 'S')
										@endphp

							    		<tr>
							    			<th scope="row">{{$key+1}}</th>
							    			@if($ptc3 == 'B')
							    				<td align="right">{{$size_tag[$purchase_detail->size]}}</td>
							    			@else
							    				<td align="right">{{$purchase_detail->size}}</td>
							    			@endif
							    			<td align="right">{{number_format($purchase_detail->amount, 2)}}&nbsp;{{$purchase_detail->uom}}</td>
							    			<td align="right">{{number_format($purchase_detail->price)}}&nbsp;{{$purchase->currency}}</td>
							    			<td align="right">{{number_format($subtotal)}}&nbsp;{{$purchase->currency}}</td>
							    		</tr>
							    	@endforeach
							    	</tbody>
							    	<tfoot>
							    		<tr>
							    			<td><b>Total</b></td>
							    			<td><b>Weight</b></td>
							    			<td align="right">{{number_format($total_weight, 2)}}</td>
							    			<td><b>Price</b></td>
							    			<td align="right">{{number_format($total)}}&nbsp;{{$purchase->currency}}</td>
							    		</tr>
							    	</tfoot>
								</table>
							</div>
							@endif
							@actionEnd

							@actionStart('production_result', 'read')
							@if($purchase->production_result->count() > 0)
							<hr>
							<h4><i class="fa fa-gears"></i> Production Result</h4>
							<table class="table">
							<thead>
								<tr>
									<th>Product</th>
									<th>Weight Used</th>
									<th>Result</th>
								</tr>
							</thead>
							<tbody>
								@foreach($purchase->production_result as $key => $result)
								<tr>
									<td>
										<a href="{{ URL::route('production.summary', [$result->id]) }}" class="btn btn-default">
										{{$result->production->name}}
										</a>
									</td>
									<td>{{number_format($result->weight, 2)}} kg</td>
									<td>
										@if($result->production_result_detail->count() > 0)
										<table class="table">
										<thead>
											<tr>
												<th colspan="3">Main Product</th>
											</tr>
										</thead>
										<tbody>
											@foreach($result->production_result_detail as $detail)
											<tr>
												<td>Size {{$detail->size}}</td>
												<td>{{$detail->block}} block</td>
												<td>@ {{number_format($result->block_weight, 2)}} kg</td>
											</tr>
											@endforeach
										</tbody>
										</table>
										@endif
										@if($result->production_result_addition->count() > 0)
										<table class="table">
										<thead>
											<tr>
												<th colspan="3">Additional Product</th>
											</tr>
										</thead>
										<tbody>
											@foreach($result->production_result_addition as $addition)
											<tr>
												<td>Size {{$addition->size}}</td>
												<td>{{$addition->block}} block</td>
												<td>@ {{number_format($addition->block_weight, 2)}} kg</td>
											</tr>
											@endforeach
										</tbody>
										</table>
										@endif
									</td>
								</tr>
								@endforeach
							</tbody>
							</table>
							@endif
							@actionEnd
		            	</div>
		            </div>
		        </div>
	        
	    
	
</section>
@actionEnd
@stop

@section('modal')
<div class="modal fade" id="reject_history" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="myModalLabel">Reject History</h4>
      		</div>
      		<table class="table">
      		<tr>
      			<th>Date</th>
      			<th>Rejected at</th>
      			<th>Reason</th>
      		</tr>
      		@foreach($purchase->purchase_reject as $purchase_reject)
      		<tr>
      			<th>{{date("D, d-m-Y", strtotime($purchase_reject->reject_date))}}</th>
      			<th>{{$purchase_reject->reject_step}}</th>
      			<th>{{$purchase_reject->reason}}</th>
      		</tr>
      		@endforeach
      		</table>
      		<div class="modal-footer">
      			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i>&nbsp;Close</button>
      		</div>
    	</div>
  	</div>
</div>

<div class="modal fade" id="close_purchase" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="myModalLabel">Close Purchase</h4>
      		</div>
      		<div class="modal-body">
      			<h3>
      				<i class="fa fa-warning"></i> ONCE PURCHASE IS CLOSED IT CAN NO LONGER BE USED ON PRODUCTION.
      				<br>
      				CONFIRM CLOSE?
      				<center>
      					{!! Form::open(array('route' => ['purchase.close', $purchase->id], 'method' => 'PUT'))!!}
	                		<button type="submit" class="btn btn-success"><i class="fa fa-check"></i> CONFIRM </button>
	                	{!! Form::close() !!}
      				</center>
      			</h3>
      		</div>
      		<div class="modal-footer">
      			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i>&nbsp;Close</button>
      		</div>
    	</div>
  	</div>
</div>

<div class="modal fade" id="supplier_detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="myModalLabel">Supplier Detail</h4>
      		</div>
      		<table class="table">
	      		<tr>
	      			<th>Name</th>
	      			<td>{{$purchase->purchase_supplier->supplier->name}}</td>
	      		</tr>
	      		<tr>
	      			<th>Address</th>
	      			<td>{{$purchase->purchase_supplier->supplier->address}}</td>
	      		</tr>
	      		<tr>
	      			<th>E-mail Address</th>
	      			<td>{{$purchase->purchase_supplier->supplier->email}}</td>
	      		</tr>
	      		<tr>
	      			<th>Phone Number</th>
	      			<td>{{$purchase->purchase_supplier->supplier->phone_number}}</td>
	      		</tr>
	      		<tr>
	      			<th>Fax</th>
	      			<td>{{$purchase->purchase_supplier->supplier->fax}}</td>
	      		</tr>
	      		<tr>
	      			<th>Website</th>
	      			<td>{{$purchase->purchase_supplier->supplier->website}}</td>
	      		</tr>
	      		<tr>
	      			<th>PIC</th>
	      			<td>{{$purchase->purchase_supplier->supplier->pic}}</td>
	      		</tr>
	      		<tr>
	      			<th>Bank Account</th>
	      			<td>{{$purchase->purchase_supplier->supplier->bank_account}}</td>
	      		</tr>
	      		<tr>
	      			<th>Note</th>
	      			<td>{{$purchase->purchase_supplier->supplier->note}}</td>
	      		</tr>
      		</table>
      		<div class="modal-footer">
      			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i>&nbsp;Close</button>
      		</div>
    	</div>
  	</div>
</div>
<div class="modal fade" id="term_condition" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="myModalLabel">Terms of Condition</h4>
      		</div>
      		<div class="modal-body">
      			{!! $purchase->term_condition !!}
      		</div>
      		<div class="modal-footer">
      			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i>&nbsp;Close</button>
      		</div>
    	</div>
  	</div>
</div>
<div class="modal fade" id="term_payment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="myModalLabel">Terms of Payment</h4>
      		</div>
      		<div class="modal-body">
      			{!! $purchase->term_payment !!}
      		</div>
      		<div class="modal-footer">
      			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i>&nbsp;Close</button>
      		</div>
    	</div>
  	</div>
</div>
<div class="modal fade" id="reject_po_rm_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="myModalLabel">Reject Form</h4>
      		</div>
      		{!! Form::open(array('route' => ['purchase.reject_po_rm', $purchase->id]))!!}
      		<div class="modal-body">
      			{{ Form::label('reason', 'Reason') }}
                {{ Form::text('reason', old('reason'), array('class' => 'form-control', 'placeholder' => 'Reason', 'required' => 'true')) }}<br/>
      		</div>
      		<div class="modal-footer">
      			<button type="submit" class="btn btn-success" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
      		</div>
      		{!! Form::close() !!}
    	</div>
  	</div>
</div>
<div class="modal fade" id="approve_po_rm_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="myModalLabel">Approval Form</h4>
      		</div>
      		{!! Form::open(array('route' => ['purchase.approve_po_rm', $purchase->id]))!!}
      		<div class="modal-body">
      			Approve PO RM?
      			<br/>
      		</div>
      		<div class="modal-footer">
      			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i>&nbsp;No</button>
      			<button type="submit" class="btn btn-primary" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> &nbsp;Yes</button>
      		</div>
      		{!! Form::close() !!}
    	</div>
  	</div>
</div>
<div class="modal fade" id="reject_actual_delivery_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="myModalLabel">Reject Form</h4>
      		</div>
      		{!! Form::open(array('route' => ['purchase.reject_actual_delivery', $purchase->id]))!!}
      		<div class="modal-body">
      			{{ Form::label('reason', 'Reason') }}
                {{ Form::text('reason', old('reason'), array('class' => 'form-control', 'placeholder' => 'Reason', 'required' => 'true')) }}<br/>
      		</div>
      		<div class="modal-footer">
      			<button type="submit" class="btn btn-success" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
      		</div>
      		{!! Form::close() !!}
    	</div>
  	</div>
</div>
<div class="modal fade" id="approve_actual_delivery_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="myModalLabel">Approval Form</h4>
      		</div>
      		{!! Form::open(array('route' => ['purchase.approve_actual_delivery', $purchase->id]))!!}
      		<div class="modal-body">
      			Approve Actual Delivery?
      			<br/>
      		</div>
      		<div class="modal-footer">
      			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i>&nbsp;No</button>
      			<button type="submit" class="btn btn-primary" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> &nbsp;Yes</button>
      		</div>
      		{!! Form::close() !!}
    	</div>
  	</div>
</div>
<div class="modal fade" id="hint" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="myModalLabel">Next Step</h4>
      		</div>
      		<div class="modal-body">
				<center><h3>
      			@if($purchase->status == 'PO RM')
	      			@if($purchase->price->count() == 0)
	      			Register price from Supplier.
	      			<br>
	      			<a href="{{ URL::route('price.create', $purchase->id) }}" class="btn btn-lg btn-primary"><i class="fa fa-money"></i> Price</a>
	      			@else
	      			Re-check the price list given by Supplier, then confirm.
	      			@endif
      			@elseif($purchase->status == 'Waiting COO')
      				Waiting for COO Approval.
      			@elseif($purchase->status == 'Actual Delivery')
      				@if($purchase->purchase_detail->count() == 0)
	      			Register detail of item delivered.
	      			<br>
	      			<a href="{{ URL::route('purchase_detail.create', $purchase->id) }}" class="btn btn-lg btn-primary"><i class="fa fa-shopping-cart"></i> Cart</a>
	      			@else
	      			Re-check the item details registered, then confirm.
	      			@endif
      			@elseif($purchase->status == 'Waiting QC')
      				Waiting for QC Approval.
      			@endif
      			</h3></center>
      		</div>
      		<div class="modal-footer">
      			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i>&nbsp;Close</button>
      		</div>
    	</div>
  	</div>
</div>
@stop

@section('scripts')
@if($purchase->status == 'PO RM' || $purchase->status == 'Waiting COO' || $purchase->status == 'Actual Delivery' || $purchase->status == 'Waiting QC')
<script>
$(document).ready(function(){
	$('#hint').modal('show');
})
</script>
@endif
@stop