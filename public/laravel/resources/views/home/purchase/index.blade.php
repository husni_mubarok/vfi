@extends('layouts.home.template')
@section('content')
<div class="header_menu mobile-hide">
    <a href="{{ URL::route('purchase.index') }}">PURCHASE</a>
</div>
@actionStart('purchase', 'read')
<section class="content">
	<div class="row">
	    <div class="col-md-12 col-sm-12 col-xs-12">
	    	<div class="col-md-1"></div>
	    	<div class="col-md-10">
		        <div class="x_panel">
	                <h2>
	                	<i class="fa fa-shopping-cart"></i>&nbsp;Purchase List
	                	@actionStart('purchase', 'create')
	                	<a href="{{ URL::route('purchase.create') }}" class="btn btn-lg btn-primary pull-right"><i class="fa fa-plus"></i> Add</a>
	                	@actionEnd
                	</h2>
	                <hr>
		            <div class="x_content">
		            	@foreach($project_list as $project)
		            	<button type="button" class="btn btn-default btn_project" value="{{$project}}">{{$project}}</button>
		            	@endforeach

		            	<table class="table table-hover table-striped" id="index_table">
		            	<thead>
			    			<tr>
			    				<th width="5%">No.</th>
						      	<th>Code</th>
						      	<th>Production Code</th>
						      	<th>Status</th>
								<th>Description</th>
								<th>Estimated Price</th>
								<th>Supplier</th>
								<th>Product</th>
								<th>Purchase Time</th>
								@actionStart('purchase', 'delete')
								<th></th>
								@actionEnd
			    			</tr>
			    		</thead>
			    		<tbody id="table_body">
			    			{{-- Content by Ajax --}}
			    		</tbody>
		            	</table>
		            </div>
		        </div>
	        </div>
	    </div>
	</div>
</section>
@actionEnd
@stop

@section('modal')
<div class="modal fade" id="notification_modal" tabindex="-1" role="dialog" aria-labelledby="modal_label" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h4 class="modal-title" id="modal_label">Purchase Summary</h4>
      		</div>
      		<div class="modal-body">
				<table class="table">
				<thead>
					<tr>
						<th>PO No.</th>
						<th>Status</th>
						<th>Description</th>
					</tr>
				</thead>
				<tbody>
					@if($pending_approval->count() > 0)
					@foreach($pending_approval as $pending)
					<tr>
						<td>
							<a href="{{ URL::route('purchase.summary', $pending->id) }}" class="btn btn-primary btn-md">{{$pending->doc_code}}</a>
						</td>
						<td>{{$pending->status}}</td>
						<td>{{$pending->description}}</td>
					</tr>
					@endforeach
					@else
					<td colspan="3">Tidak ada PO yang menunggu konfirmasi</td>
					@endif
				</tbody>
				</table>
      		</div>
      		<div class="modal-footer">
      			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i>&nbsp;Close</button>
      		</div>
    	</div>
  	</div>
</div>
@stop

@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
$(document).ready(function () {
    $("#index_table").DataTable(
    	{
    		scrollX: true
    	});

    $("#notification_modal").modal('show');
});

$('.btn_project').on('click', function()
{
	var button = $(this);
	$('.btn_project').removeClass('active').attr('disabled', true);
	$('#index_table').DataTable().destroy();
	$('#table_body').empty().append('<tr><td colspan="9" align="center">Loading Data...</td></tr>');
	$.ajax({
		url: '{{URL::route('get.purchase_by_project')}}', 
		data: {'project': $(this).val()}, 
		type: 'GET', 
		headers : {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
		success: function(data, textStatus, jqXHR)
		{
			var new_row = '';
			jQuery.each(data, function(i, val)
			{
				var url_summary = '{{ URL::route('purchase.summary', 'id') }}';
				url_summary = url_summary.replace('id', val['id']);
				var url_delete = '{{ URL::route('purchase.delete', 'id') }}';
				url_delete = url_delete.replace('id', val['id']);

				new_row += '<tr>';
				new_row += '<td>';
				new_row += i+1;
				new_row += '</td>';
				new_row += '<td>';
				new_row += '<a href="'+url_summary+'" class="btn btn-default btn-sm">';
				new_row += val['doc_code'];
				new_row += '</a>';
				new_row += '</td>'; 
				new_row += '<td>';
				new_row += val['trans_code'];
				new_row += '</td>';
				new_row += '<td>';
				new_row += val['status'];
				new_row += '</td>';
				new_row += '<td>';
				new_row += val['description'];
				new_row += '</td>';
				new_row += '<td>';
				new_row += val['estimated_price'];
				new_row += '</td>';
				new_row += '<td>';
				new_row += val['purchase_supplier']['supplier']['name'];
				new_row += '</td>';
				new_row += '<td>';
				new_row += val['purchase_product']['product']['name'];
				new_row += '</td>';
				new_row += '<td>';
				new_row += val['purchase_time'];
				new_row += '</td>';
				new_row += '<td>';
				new_row += '</td>';
				new_row += '</tr>';
			});

			$('#table_body').empty().append(new_row);
			$("#index_table").DataTable();
			$('.btn_project').removeClass('active').attr('disabled', false);
			button.addClass('active');
		}, 
		error: function()
		{
			$('.btn_project').removeClass('active').attr('disabled', false);
			alert('Gagal menarik data!');
		}
	});


	
});
</script>
@stop