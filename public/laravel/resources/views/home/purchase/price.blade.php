@extends('layouts.home.template')
@section('content')
<div class="header_menu mobile-hide">
    <a href="{{ URL::route('purchase.index') }}">PURCHASE</a>
</div>
@actionStart('price', 'create|update')
<section class="content box box-solid">
	<div class="row">
	    <div class="col-md-12 col-sm-12 col-xs-12">
	    	<div class="col-md-1"></div>
	    	<div class="col-md-10">
		        <div class="x_panel">
	                <h2>
	                	<i class="fa fa-dollar"></i>&nbsp;Price List
	                	<small>{{$purchase->description}}</small>
                	</h2>
	                <hr>
	                <div class="row bs-wizard small" style="border-bottom:0;">
		                <!-- class name =  1. complete 2.active 3.disabled-->
		                <div class="col-xs-2 bs-wizard-step complete"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Step 2</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Start<br><i>Create Purchase</i></div>
		                </div>

		                <div class="col-xs-2 bs-wizard-step active"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Step 2</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Step 1<br><i>Register Price</i></div>
		                </div>

		                <div class="col-xs-2 bs-wizard-step disabled"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Step 2</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Step 2<br><i>Waiting COO</i></div>
		                </div>

		                <div class="col-xs-2 bs-wizard-step disabled"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Step 2</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Step 3<br><i>Actual Delivery</i></div>
		                </div>

		                <div class="col-xs-2 bs-wizard-step disabled"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Step 4</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Step 4<br><i>Waiting QC</i></div>
		                </div>
		                <div class="col-xs-2 bs-wizard-step disabled"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Finish</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Finish<br><i>Summary</i></div>
		                </div>
		            </div>
	                <hr>
		            <div class="x_content">
		            	<div class="col-md-6 col-xs-12 col-sm-12">
		            		@include('errors.error')
		            		{{-- @if(isset($prices))
			            	{!! Form::open(array('route' => ['editor.price.update', $purchase->id], 'method' => 'PUT'))!!}
			            	@else --}}
			            	{!! Form::open(array('route' => ['price.store', $purchase->id], 'id' => 'form_price')) !!}
			            	{{-- @endif --}}

			            	@php
			            		$ptc = $purchase->trans_code;
			            		$ptc3 = $ptc[2]; 
			            	@endphp


			                <table class="table table-bordered">
							  	<thead>
							  	  	<tr>
							  	  		@if($ptc3 != 'B')
										<th>Size</th>
										@endif
										
										<th>
											@if($ptc3 == 'B')
											Size
											@else
											Tag
											@endif
										</th>
										
										<th>Price ({{$purchase->currency}})</th>
										@if(isset($previous_price) && $previous_price->count() > 0)
										<th>Latest Price</th>
										@endif
									</tr>
							  	</thead>
							  	<tbody>

							  	@if(isset($prices))
							  	@foreach($prices as $key => $price)
							  	<tr>
							  		@if($ptc3 != 'B')
							  		<td>{{$price->size->min_value}}&nbsp;~&nbsp;{{$price->size->max_value}}</td>
							  		@endif

							  		<td>{{$price->size->note}}</td>
							  		<td align="right">
						  				{{ Form::number('value['.$price->size->id.']', old('value['.$price->size->id.']', $price->value)) }}
							  		</td>
							  	</tr>
							  	@endforeach

							  	@else
							    @foreach($purchase->purchase_product->product->product_type->product_type_size as $key => $product_type_size)
							   
							    @if($ptc3 == 'B')
							    	@if($product_type_size->size->note == 'L')
							    		<tr>
											<td>{{$product_type_size->size->note}}</td>
											<td align="right">
												{{ Form::number('value['.$product_type_size->size->id.']', old('value['.$product_type_size->size->id.']', '0')) }}
											</td>
											@if(isset($previous_price) && $previous_price->count() > 0)
											<td>
												@if($key+1 <= $previous_price->count())
												{{$previous_price[$key]->value}}
												@else
												- 
												@endif
											</td>
											@endif
										</tr>
									@elseif($product_type_size->size->note == 'M')
										<tr>
											<td>{{$product_type_size->size->note}}</td>
											<td align="right">
												{{ Form::number('value['.$product_type_size->size->id.']', old('value['.$product_type_size->size->id.']', '0')) }}
											</td>
											@if(isset($previous_price) && $previous_price->count() > 0)
											<td>
												@if($key+1 <= $previous_price->count())
												{{$previous_price[$key]->value}}
												@else
												- 
												@endif
											</td>
											@endif
										</tr>
									@elseif($product_type_size->size->note == 'S')
										<tr>
											<td>{{$product_type_size->size->note}}</td>
											<td align="right">
												{{ Form::number('value['.$product_type_size->size->id.']', old('value['.$product_type_size->size->id.']', '0')) }}
											</td>
											@if(isset($previous_price) && $previous_price->count() > 0)
											<td>
												@if($key+1 <= $previous_price->count())
												{{$previous_price[$key]->value}}
												@else
												- 
												@endif
											</td>
											@endif
										</tr>
							    	@endif
							    @else
							    	@if($product_type_size->size->note == 'L')

							    	@elseif($product_type_size->size->note == 'M')

							    	@elseif($product_type_size->size->note == 'S')

							    	@else
							    	<tr>
										<td>{{$product_type_size->size->min_value}}&nbsp;~&nbsp;{{$product_type_size->size->max_value}}</td>
										<td>{{$product_type_size->size->note}}</td>
										<td align="right">
											{{ Form::number('value['.$product_type_size->size->id.']', old('value['.$product_type_size->size->id.']', '0')) }}
										</td>
										@if(isset($previous_price) && $previous_price->count() > 0)
										<td>
											@if($key+1 <= $previous_price->count())
											{{$previous_price[$key]->value}}
											@else
											- 
											@endif
										</td>
										@endif
									</tr>
									@endif
							    @endif
								
								@endforeach
								
								@endif
								</tbody>
							</table>
							<button type="button" id="btn_submit" data-toggle="modal" data-target="#confirmation_modal" class="btn btn-success pull-right"><i class="fa fa-check"></i> Save</button>
							{!! Form::close() !!}
						</div>
		            </div>
		        </div>
	        </div>
	    </div>
	</div>
</section>
@actionEnd
@stop

@section('modal')
<div class="modal fade" id="confirmation_modal" tabindex="-1" role="dialog" aria-labelledby="modal_label" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="modal_label">Price Summary</h4>
      		</div>
      		<div class="modal-body">
      			Confirm Price?
      		</div>
      		<div class="modal-footer">
      			<button type="button" id="btn_confirm" class="btn btn-success"><i class="fa fa-check"></i> YES</button>
      			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i>&nbsp;NO</button>
      		</div>
    	</div>
  	</div>
</div>
@stop

@section('scripts')
<script>
$("#btn_submit").on('click', function() {

});

$("#btn_confirm").on('click', function() {
	$("#form_price").submit();
});
</script>
@stop