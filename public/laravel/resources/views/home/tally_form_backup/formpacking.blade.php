@extends('layouts.tablet.template')
<meta name="_token" content="{{ csrf_token() }}"/>
 
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-12">
          <div class="x_panel"> 
            <div class="x_content">  
              <div> 
               <center><h2>FORM PACKING</h2></center>
               <center><h4>{{$temp_tally->trans_code}}</h4></center>  
               <hr>
             </div>  
           </div>
         </div>  
       </div> 

       <div class="col-md-12">
        <div class="x_panel"> 
          <div class="x_content">  
            <div>   
             
         


           <a href="{{ URL::route('tally_form.index') }}" type="button" class="btn btn-primary btn-lg"  style="padding: 12px; font-size: 20px; margin-left: -1px;  width: 70px;"><i class="fa fa-home faa-pulse animated"></i><br/><p style="font-size: 12px; margin-top: -0px;">HOME</p></a>
             <br/> 
             <hr>   
           </div>  
         </div>
       </div>  
     </div>  
   </div> 

 
  <div class="row"> 
    {!! Form::model($temp_tally, array('route' => ['tally_form.storepacking', $temp_tally->id], 'method' => 'PUT', 'class'=>'update', 'id' => 'form_tally'))!!}  
    <div class="col-md-12 col-sm-12 col-xs-12"> 
      <div class="x_panel"> 
        <div class="x_content">   
          <table id="itemTable" class="table table-striped dataTable">
            <thead>
              <tr> 
                <th rowspan="2">#</th>
                <th rowspan="2">TYPE</th>
                <th rowspan="2">GRADE</th>
                <th rowspan="2">SIZE</th>  
                <th rowspan="2">FREEZING</th> 
                <th rowspan="2">PRODUCTION CODE</th>
                <th rowspan="2">BLOCK</th>
                <th rowspan="2">TOTAL</th>
                <th rowspan="2">TOTAL BLOCK</th>
                <th rowspan="2">IC</th>
                <th colspan="4"><center>TO BE SET</center></th>
              </tr> 
              <tr> 
                <th>MC</th>
                <th>MIX</th>
                <th>REST</th>  
                <th>ICR</th>  
              </tr> 
            </thead> 
            <tbody>
             @foreach($tally_detail as $key => $tally_details)
             <tr>
              <td>{{$key+1}}</td> 
              <td>{{$tally_details->qty_type}}</td>
              <td>{{$tally_details->grade}}</td>  
              <td>{{$tally_details->size}}</td>
              <td>{{$tally_details->freezing}}</td>
              <td>{{$tally_details->purchase_code}} | {{$tally_details->code_size}}</td>
              <td>{{$tally_details->block_weight}}</td>
              <td>{{$tally_details->total}}</td>
              <td>{{$tally_details->total_block}}</td>
              <td>
                {{ Form::number('tally['.$tally_details->id.'][ic]', old($tally_details->id.'[ic]', $tally_details->ic), ['class' => 'form-control', 'id' => 'ic_'.$tally_details->id, 'min' => '0', 'onchange' => 'change_total_ic_'.$tally_details->id.'();']) }} 
              </td>
              <td>
               {{ Form::number('tally['.$tally_details->id.'][mc]', old($tally_details->id.'[mc]', $tally_details->mc), ['class' => 'form-control', 'id' => 'mc_'.$tally_details->id, 'min' => '0', 'onchange' => 'change_total_mc_'.$tally_details->id.'();']) }} 
             </td>
             <td>
               {{ Form::number('tally['.$tally_details->id.'][mix]', old($tally_details->id.'[mix]', $tally_details->mix), ['class' => 'form-control', 'id' => 'mix_'.$tally_details->id, 'min' => '0']) }} 
             </td>
             <td>
              {{$tally_details->rest}}
            </td>
            <td>
             {{ Form::number('tally['.$tally_details->id.'][icr]', old($tally_details->id.'[icr]', $tally_details->icr), ['class' => 'form-control', 'id' => 'icr_'.$tally_details->id, 'min' => '0']) }} 
           </td>
         </tr>
         <script type="text/javascript">
          function change_total_ic_{{$tally_details->id}}()
          { 
            $('#ic_{{$tally_details->id}}').css('background', 'red'); 
            document.getElementById('flag_change').value = 1;
          }

          function change_total_mc_{{$tally_details->id}}()
          { 
            $('#mc_{{$tally_details->id}}').css('background', 'red');
            document.getElementById('flag_change').value = 1;
          }
        </script>
        @endForeach
      </tbody> 
    </table>  
  </div>
</div>
</div> 
</div>
</div> 
<input type="hidden" id="flag_change"> 
<div class="col-md-12 col-sm-12 col-xs-12">
  <hr>
  <button type="button" id="btn_confirm" onclick="submit_form();" class="btn btn-success btn-lg pull-right"><i class="fa fa-check"></i> Save</button> 
</div>
{!! Form::close() !!}  

 

<script>

 function submit_form ()
 {  
  var change_total = document.getElementById('flag_change').value;
   if(change_total == 1)
   {
     swal({
      title: 'Are you sure?',
      text: "Total is change!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#BDBDBD',
      confirmButtonText: 'Yes, change it!'
    }).then(function () {

      $('#form_tally').submit(); 
    })
   }else{
    swal({
      title: 'Are you sure?', 
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#009933',
      cancelButtonColor: '#BDBDBD',
      confirmButtonText: 'Save'
    }).then(function () {

      $('#form_tally').submit(); 
    })
   }
 };  
</script>


