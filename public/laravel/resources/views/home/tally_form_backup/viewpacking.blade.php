@extends('layouts.tablet.template')
<meta name="_token" content="{{ csrf_token() }}"/>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="col-md-12">
      <div class="x_panel"> 
        <div class="x_content">  
          <div> 
           <center><h2>FORM PACKING</h2></center>
           <center><h4>{{$temp_tally->trans_code}}</h4></center>  
           <hr>
         </div>  
       </div>
     </div>  
   </div> 

   <div class="col-md-12">
    <div class="x_panel"> 
      <div class="x_content">  
        <div>   
          {!! Form::open(array('route' => ['tally_form.closepacking', $temp_tally->id], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_close'))!!}
          {{ csrf_field() }}   
          
           <button type="button" id="btn_close" onclick="storeclose();" class="btn btn-danger pull-right btn-lg" style="font-size: 8px; padding: 12px; font-size: 20px; width: 70px;"><i class="fa fa-lock faa-pulse animated"></i><br/><p style="font-size: 12px; margin-top: -0px;">CLOSE</p></button> 
          {!! Form::close() !!}  
           



           <a href="{{ URL::route('tally_form.index') }}" type="button" class="btn btn-primary btn-lg"  style="padding: 12px; font-size: 20px; margin-left: -1px;  width: 70px;"><i class="fa fa-home faa-pulse animated"></i><br/><p style="font-size: 12px; margin-top: -0px;">HOME</p></a>
          <br/> 
          <hr>   
        </div>  
      </div>
    </div>  
  </div>  
</div> 

<div class="row">  
  <div class="col-md-12 col-sm-12 col-xs-12"> 
    <div class="x_panel"> 
      <div class="x_content">   
        <table id="itemTable" class="table table-striped dataTable">
          <thead>
            <tr> 
              <th rowspan="2">#</th>
              <th rowspan="2">GRADE</th>
              <th rowspan="2">SIZE</th>  
              <th rowspan="2">FREEZING</th> 
              <th rowspan="2">PRODUCTION CODE</th>
              <th rowspan="2">BLOCK</th>
              <th rowspan="2">TOTAL</th>
              <th rowspan="2">TOTAL BLOCK</th>
              <th rowspan="2">IC</th>
              <th colspan="4"><center>TO BE SET</center></th>
            </tr> 
            <tr> 
              <th>MC</th>
              <th>MIX</th>
              <th>REST</th>  
              <th>ICR</th>  
            </tr> 
          </thead> 
          <tbody>
           @foreach($tally_detail as $key => $tally_details)
           <tr>
            <td>{{$key+1}}</td>  
            <td>{{$tally_details->grade}}</td>  
            <td>{{$tally_details->size}}</td>
            <td>{{$tally_details->freezing}}</td>
            <td>{{$tally_details->purchase_code}} | {{$tally_details->code_size}}</td>
            <td>{{$tally_details->block_weight}}</td>
            <td>{{$tally_details->total}}</td>
            <td>{{$tally_details->total_block}}</td>
            <td>{{$tally_details->ic}}
            </td>
            <td>
             {{$tally_details->mc}}
           </td>
           <td>
             {{$tally_details->mix}}
           </td>
           <td>
            {{$tally_details->rest}}
          </td>
          <td>
           {{$tally_details->icr}}
         </td>
       </tr> 
       @endForeach
     </tbody> 
   </table>  
 </div>
</div>
</div>   
 

 
<script>

function storeclose ()
  { 
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this packing!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#BDBDBD',
      confirmButtonText: 'Yes, close it!'
    }).then(function () {

      $('#form_close').submit();
    })
  };  
</script>


