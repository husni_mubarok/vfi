@extends('layouts.tablet.template')
<meta name="_token" content="{{ csrf_token() }}"/>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    
    <div class="col-md-12">
      <div class="x_panel"> 
        <div class="x_content">  
          <div> 
           <center><h2>FORM RECEIVING</h2></center>
           <center><h4>{{$temp_tally->trans_code}}</h4></center>  
           <hr>
         </div>  
       </div>
     </div>  
   </div> 

   <div class="col-md-12">
    <div class="x_panel"> 
      <div class="x_content">  
      <div>   
          {!! Form::open(array('route' => ['tally_form.closereceiving', $temp_tally->id], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_close'))!!}
          {{ csrf_field() }}  
          <button type="button" id="btn_close" onclick="storeclose();" class="btn btn-danger  btn-circle pull-right btn-lg"><i class="fa fa-lock"></i> <p style="font-size: 8px;">Close</p></button> 
          {!! Form::close() !!} 

          <a href="{{ URL::route('tally_form.index') }}" type="button" class="btn btn-primary btn-circle btn-lg" style="margin-left: 10px"><i class="fa fa-home faa-pulse animated"></i><br/><p style="font-size: 8px;">HOME</p></a>
          <a href="{{ URL::route('tally_form.historyreceiving', $temp_tally->id) }}" type="button" class="btn btn-primary btn-circle btn-lg" style="margin-left: 10px"><i class="fa fa-tasks faa-pulse animated"></i><br/><p style="font-size: 8px;">HTR</p></a> 
          <a href="{{ URL::route('tally_form.receiving', $temp_tally->id) }}" type="button" class="btn btn-success btn-circle btn-lg" style="margin-left: 10px"><i class="fa fa-refresh faa-pulse animated"></i><br/><p style="font-size: 9px;">REF</p></a> 
          <br/> 
          <hr>   
        </div>  
      </div>
    </div>  
  </div>  
</div> 


<form class="form-horizontal" role="form">
{{ csrf_field() }}
  <div class="col-md-12 col-sm-12 col-xs-12"> 
    <div class="col-md-12">
      <div class="x_panel"> 
        <div class="x_content">
          <h3>GROUP</h3>
          <a href="#" class="btn btn-default btn-lg" id="btn_fresh" onclick="set_btn_fresh_active();"> FRESH</a>
          <a href="#" class="btn btn-default btn-lg" id="btn_bekukapal" onclick="set_btn_bekukapal_active();"> BEKU KAPAL</a> 
          {{ Form::hidden('tally_group',old('tally_group'), array('required' => 'true', 'id' => 'tally_group')) }}    
          <br/>  
          <h3>TYPE</h3>
          <a href="#" class="btn btn-default btn-lg" id="btn_receiving" onclick="set_btn_receiving_active();"> RECEIVING</a>
          <a href="#" class="btn btn-default btn-lg" id="btn_treatment" onclick="set_btn_treatment_active();"> TREATMENT</a>  
          <button type="button" class="btn btn-default btn-lg" id="btn_additional" style="margin-top: 5px" onclick="set_btn_additional_active();"> ADDITIONAL</button>
          {{ Form::hidden('tally_type',old('tally_type'), array('required' => 'true', 'id' => 'tally_type')) }} 
          <br/> 

          <div id="receiving_treatment" class="receiving_treatment">
            <h3>SIZE</h3>

            <div id="size_fresh" class="size_fresh" style="display: none">
              @foreach($tally_detail as $key => $tally_details)
              <button type="button" class="btn btn-default btn-lg" style="margin-top: 5px" id="{{$tally_details->id}}" onclick="set_btn_{{$tally_details->id}}_active();" value="{{$tally_details->id}}"> {{$tally_details->note}}</button>  

              <script type="text/javascript">
               function set_btn_{{$tally_details->id}}_active()
               { 
                @foreach($tally_detail as $key => $tally_detail_btn)
                $("#{{$tally_detail_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default"); 
                @endforeach
                @foreach($tally_additional as $key => $tally_add_btn)
                $("#add_{{$tally_add_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default"); 
                @endforeach
                $("#{{$tally_details->id}}").removeClass("btn btn-default").addClass("btn btn-primary"); 
              };  
            </script> 
            @endforeach 
          </div>

          <div id="size_beku_kapal" class="size_beku_kapal" style="display: none">
            @foreach($size_beku_kapal as $key => $size_beku_kapals) 
            <button type="button" class="btn btn-default btn-lg" style="margin-top: 5px" id="{{$size_beku_kapals->id}}" onclick="set_btn_{{$size_beku_kapals->id}}_active();" value="{{$size_beku_kapals->id}}"> {{$size_beku_kapals->note}}</button> 

            <script type="text/javascript">
             function set_btn_{{$size_beku_kapals->id}}_active()
             { 
              @foreach($size_beku_kapal as $key => $tally_beku_kapal_btn)
              $("#{{$tally_beku_kapal_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default"); 
              @endforeach
              @foreach($tally_additional as $key => $tally_add_btn)
              $("#add_{{$tally_add_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default"); 
              @endforeach
              $("#{{$size_beku_kapals->id}}").removeClass("btn btn-default").addClass("btn btn-primary"); 
            };  
          </script> 
          @endforeach  
        </div> 

        {{ Form::hidden('size_id',old('size_id'), array('required' => 'true', 'id' => 'size_id')) }} 
        {{ Form::hidden('purchase_id',old('purchase_id', $temp_tally->id_purchase), array('required' => 'true', 'id' => 'purchase_id')) }}
        {{ Form::hidden('temp_tally_id',old('temp_tally_id', $temp_tally->id), array('required' => 'true', 'id' => 'temp_tally_id')) }} 
        <br/> 
        <h3>WEIGHT</h3>
        {{ Form::number('weight',old('weight'), array('class' => 'form-control', 'placeholder' => 'Weight*', 'required' => 'true', 'id' => 'weight')) }}<br>  
        <a href="#" class="btn btn-success btn-lg " onclick="store();"> <i class="fa fa-check"></i> Save</a>
      </div> 
      <div id="additional" class="additional" style="display: none">
        <h3>ADDITIONAL VALUE</h3>
        @foreach($tally_additional as $key => $tally_additionals)
        <button type="button" class="btn btn-default btn-lg" style="margin-top: 5px" id="add_{{$tally_additionals->id}}" onclick="set_btn_add_{{$tally_additionals->id}}_active();" value="{{$tally_additionals->id}}"> {{$tally_additionals->name}}</button>  
        <script type="text/javascript">
         function set_btn_add_{{$tally_additionals->id}}_active()
         { 
          @foreach($tally_additional as $key => $tally_add_btn)
          $("#add_{{$tally_add_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default"); 
          @endforeach
          @foreach($tally_detail as $key => $tally_detail_btn)
          $("#{{$tally_detail_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default"); 
          @endforeach
          $("#add_{{$tally_additionals->id}}").removeClass("btn btn-default").addClass("btn btn-primary"); 
        };  
      </script> 
      @endforeach  

      {{ Form::hidden('tally_additional_master_id',old('tally_additional_master_id'), array('required' => 'true', 'id' => 'tally_additional_master_id')) }}  
      <br/> 
      <h3>WEIGHT</h3>
      {{ Form::number('weight_add',old('weight_add'), array('class' => 'form-control', 'placeholder' => 'Weight*', 'required' => 'true', 'id' => 'weight_add')) }}<br>   
      <h3>PAN</h3>
      {{ Form::number('pan',old('pan'), array('class' => 'form-control', 'placeholder' => 'Pan*', 'required' => 'true', 'id' => 'pan')) }}<br>  
      <h3>REST</h3>
      {{ Form::number('rest',old('rest'), array('class' => 'form-control', 'placeholder' => 'Rest*', 'required' => 'true', 'id' => 'rest')) }}<br> 
      <a href="#" class="btn btn-success btn-lg " onclick="storeadd();"> <i class="fa fa-check"></i> Save Additional</a> 
    </div>  
    <hr> 
  </div>  
</div>
</div> 
</div>   

</form> 


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script type="text/javascript">
  $("button").click(function() {
    document.getElementById ("size_id").value = "";
    document.getElementById ("tally_additional_master_id").value = "";

    var fired_button = $(this).val();
    document.getElementById ("size_id").value = fired_button;
    document.getElementById ("tally_additional_master_id").value = fired_button;
    document.getElementById("weight").focus();
  });

  function set_btn_fresh_active()
  { 
    $("#btn_fresh").removeClass("btn btn-default").addClass("btn btn-primary");
    $("#btn_bekukapal").removeClass("btn btn-primary").addClass("btn btn-default"); 
    document.getElementById ("tally_group").value = "FRESH";
  };

  function set_btn_bekukapal_active()
  { 
    $("#btn_fresh").removeClass("btn btn-primary").addClass("btn btn-default");
    $("#btn_bekukapal").removeClass("btn btn-default").addClass("btn btn-primary"); 
    document.getElementById ("tally_group").value = "BEKU KAPAL";
  };

  function set_btn_receiving_active()
  { 
    $("#btn_receiving").removeClass("btn btn-default").addClass("btn btn-primary");
    $("#btn_treatment").removeClass("btn btn-primary").addClass("btn btn-default");
    $("#btn_additional").removeClass("btn btn-primary").addClass("btn btn-default");
    document.getElementById ("tally_type").value = "RECEIVING";
  };

  function set_btn_treatment_active()
  { 
    $("#btn_receiving").removeClass("btn btn-primary").addClass("btn btn-default");
    $("#btn_treatment").removeClass("btn btn-default").addClass("btn btn-primary");
    $("#btn_additional").removeClass("btn btn-primary").addClass("btn btn-default");
    document.getElementById ("tally_type").value = "TREATMENT";
  };

  function set_btn_additional_active()
  { 
    $("#btn_receiving").removeClass("btn btn-primary").addClass("btn btn-default");
    $("#btn_treatment").removeClass("btn btn-primary").addClass("btn btn-default");
    $("#btn_additional").removeClass("btn btn-default").addClass("btn btn-primary");
    document.getElementById ("tally_type").value = "ADDITIONAL";
  };


  function store()
  { 
   var tally_type = document.getElementById ("tally_type").value;
   var weight = document.getElementById ("weight").value;
   var size_id = document.getElementById ("size_id").value;
   var tally_group = document.getElementById ("tally_group").value;
   if(tally_type=='' || weight=='' || size_id=='' || tally_group=='')
   {
    swal("Error!", "All data is required!", "error")
  }
  else{
    $.ajax({
      type: 'POST',
      url: "{{ URL::route('store', $tally_details->id) }}",
      data: {
        '_token': $('input[name=_token]').val(),
        'tally_type': $('#tally_type').val(),
        'size_id': $('#size_id').val(),
        'weight': $('#weight').val(),
        'purchase_id': $('#purchase_id').val(),
        'temp_tally_id': $('#temp_tally_id').val(),
        'tally_group': $('#tally_group').val()  
      },
      success: function(data) { 
        if ((data.errors)) {
         swal("Error!", "Gat data failed!", "error")
       } else {

        swal("Success!", "Receiving/treatment data has been save!", "success")
        document.getElementById ("tally_type").value = "";
        document.getElementById ("weight").value = "";
        document.getElementById ("size_id").value = "";
        $("#btn_additional").removeClass("btn btn-primary").addClass("btn btn-default");
        $("#btn_receiving").removeClass("btn btn-primary").addClass("btn btn-default");
        $("#btn_treatment").removeClass("btn btn-primary").addClass("btn btn-default");

        @foreach($size_beku_kapal as $key => $tally_beku_kapal_btn)
        $("#{{$tally_beku_kapal_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default"); 
        @endforeach

        @foreach($tally_detail as $key => $tally_detail_btn)
        $("#{{$tally_detail_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default"); 
        @endforeach
      } 
    },
  })
  }
};

function storeadd()
{ 
 var tally_additional_master_id = document.getElementById ("tally_additional_master_id").value;
 var weight_add = document.getElementById ("weight_add").value;
 var pan = document.getElementById ("pan").value;
 var rest = document.getElementById ("rest").value;
 var tally_group = document.getElementById ("tally_group").value;
 if(tally_additional_master_id=='' || weight_add=='' || pan=='' || rest=='' || tally_group=='')
 {
  swal("Error!", "All data is required!", "error")
}
else{
  $.ajax({
    type: 'POST',
    url: "{{ URL::route('storeadditional', $tally_details->id) }}",
    data: {
      '_token': $('input[name=_token]').val(),
      'tally_additional_master_id': $('#tally_additional_master_id').val(),
      'weight_add': $('#weight_add').val(),
      'pan': $('#pan').val(),
      'rest': $('#rest').val(),
      'purchase_id': $('#purchase_id').val(),
      'temp_tally_id': $('#temp_tally_id').val(),
      'tally_group': $('#tally_group').val()  
    },
    success: function(data) { 
      if ((data.errors)) {
       swal("Error!", "Gat data failed!", "error")
     } else {

      swal("Success!", "Additional data has been save!", "success")
      document.getElementById ("tally_additional_master_id").value = "";
      document.getElementById ("weight_add").value = "";
      document.getElementById ("pan").value = "";
      document.getElementById ("rest").value = "";

      $("#btn_additional").removeClass("btn btn-primary").addClass("btn btn-default");
      $("#btn_receiving").removeClass("btn btn-primary").addClass("btn btn-default");
      $("#btn_treatment").removeClass("btn btn-primary").addClass("btn btn-default");

      @foreach($size_beku_kapal as $key => $tally_beku_kapal_btn)
      $("#{{$tally_beku_kapal_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default"); 
      @endforeach

      @foreach($tally_additional as $key => $tally_additional_btn)
      $("#add_{{$tally_additional_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default"); 
      @endforeach
    } 
  },

})
}
};

function storeclose ()
{ 
  swal({
    title: 'Are you sure?',
    text: "You won't be able to revert this receiving!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#d33',
    cancelButtonColor: '#BDBDBD',
    confirmButtonText: 'Yes, close it!'
  }).then(function () {

    $('#form_close').submit();
  })
};  

$(document).ready(function() {
  $('#btn_additional').click(function() { 
    $('.receiving_treatment').hide(100);
    $('.additional').show(100);
  });

  $('#btn_receiving').click(function() { 
    $('.receiving_treatment').show(100);
    $('.additional').hide(100);
  });

  $('#btn_treatment').click(function() { 
    $('.receiving_treatment').show(100);
    $('.additional').hide(100);
  });

  $('#btn_bekukapal').click(function() { 
    $('.size_fresh').hide(100);
    $('.size_beku_kapal').show(100);
  });

  $('#btn_fresh').click(function() { 
    $('.size_fresh').show(100);
    $('.size_beku_kapal').hide(100);
  });
}); 
</script>
