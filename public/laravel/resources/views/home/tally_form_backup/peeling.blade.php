@extends('layouts.tablet.template')
<meta name="_token" content="{{ csrf_token() }}"/>  
<div id="all_element">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12"> 
      <div class="x_panel"> 
        <div class="x_content">   
          <div class="col-md-4 col-sm-4 col-xs-4">
           <center><h1><b>PEELING</b></h1></center>
           <center><h4 id="breadcrumb"></h4></center> 
         </div> 
         <div class="col-md-4 col-sm-4 col-xs-4">
           <center><h1>{{$temp_tally->trans_code}}</h1></center>   
         </div>  
         {!! Form::open(array('route' => ['tally_form.closepeeling', $temp_tally->id], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_close'))!!}
         {{ csrf_field() }}  
         <div class="col-md-2 col-sm-2 col-xs-2" style="padding: 5px">  
          <button type="button" id="btn_close" onclick="storeclose();" class="btn btn-danger btn-lg btn-block nomargin" style="font-size: 20px; padding: 10px 0;"><i class="fa fa-lock faa-pulse animated"></i><br/><p>CLOSE</p></button> 
        </div>
        {!! Form::close() !!}   

        <div class="col-md-2 col-sm-2 col-xs-2" style="padding: 5px">  
          <a href="{{ URL::route('tally_form.index') }}" type="button" class="btn btn-primary btn-lg btn-block nomargin"  style="padding: 10px 0; font-size: 20px;"><i class="fa fa-home faa-pulse animated"></i><br/><p>HOME</p></a>
        </div> 
      </div>
    </div>
  </div>
</div>

<div class="row"> 
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel"> 
      <div class="x_content">  
       <div class="col-md-4 col-sm-4 col-xs-4">   
       </div>  
       <div class="col-md-4 col-sm-4 col-xs-4">   
       </div>  
       <div class="col-md-2 col-sm-2 col-xs-2" style="padding: 5px">  
        <a href="{{ URL::route('tally_form.historypeeling', $temp_tally->id) }}" type="button" class="btn btn-primary btn-lg pull-right btn-block nomargin" style="padding: 10px; font-size: 20px;"><i class="fa fa-tasks faa-pulse animated"></i><br/><p>HISTORY</p></a>
      </div> 
      <div class="col-md-2 col-sm-2 col-xs-2" style="padding: 5px">   
       <a id="back"  style="display: none" href="#" type="button" class="btn btn-warning btn-lg back" style="margin-left: 10px 0; padding: 10px; font-size: 20px"><i class="fa fa-arrow-circle-left faa-pulse animated"></i><br/><p>PREVIOUS</p></a> 

       <a id="back_salinity"  style="display: none" href="#" type="button" class="btn btn-warning btn-lg back_salinity" style="margin-left: 10px 0; padding: 10px; font-size: 20px"><i class="fa fa-arrow-circle-left faa-pulse animated" style="margin-left: 10px; padding: 5px !important; font-size: 20px"></i><br/><p>PREVIOUS</p></a> 

       <a id="back_duration"  style="display: none" href="#" type="button" class="btn btn-warning btn-lg back_duration" style="margin-left: 10px 0; padding: 10px; font-size: 20px"><i class="fa fa-arrow-circle-left faa-pulse animated"></i><br/><p>PREVIOUS</p></a>  
     </div>
   </div>
 </div>
</div> 
<div class="col-md-12 col-sm-12 col-xs-12">
 <hr>
</div>  

<form class="form-horizontal" role="form">
  {{ csrf_field() }}
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="col-md-12">
      <div class="x_panel"> 
        <div class="x_content">   
          <br/>  
          <div id="peeling_type" class="peeling_type"> 
            <h3>TYPE</h3>
            @foreach($tally_detail as $key => $tally_details)
            <div class="col-md-3 col-sm-3 col-xs-3" style="padding: 5px">
              <button type="button" class="btn btn-default btn-lg btn-block"  style="padding: 30px 0;" id="{{$tally_details->id}}" onclick="set_btn_{{$tally_details->id}}_active();" value="{{$tally_details->id}}"> <p id="{{$tally_details->name}}">{{$tally_details->name}}</p></button>  
            </div>
            <script type="text/javascript">
             function set_btn_{{$tally_details->id}}_active()
             { 
              @foreach($tally_detail as $key => $tally_detail_btn)
              $("#{{$tally_detail_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default"); 
              @endforeach
              $("#{{$tally_details->id}}").removeClass("btn btn-default").addClass("btn btn-primary"); 

              var type_name = document.getElementById ("{{$tally_details->name}}").innerHTML; 
              document.getElementById ("type_name").value = type_name;
            };  
          </script> 
          @endforeach   

          {{ Form::hidden('type_name',old('type_name'), array('required' => 'true', 'id' => 'type_name')) }} 
          {{ Form::hidden('tally_peeling_master_id',old('tally_peeling_master_id'), array('required' => 'true', 'id' => 'tally_peeling_master_id')) }} 
          {{ Form::hidden('purchase_id',old('purchase_id', $temp_tally->id_purchase), array('required' => 'true', 'id' => 'purchase_id')) }}  
          {{ Form::hidden('temp_tally_id',old('temp_tally_id', $temp_tally->id), array('required' => 'true', 'id' => 'temp_tally_id')) }} 
          <br/> 
        </div>
        <div class="col-md-3 col-sm-3 col-xs-3" style="padding: 5px">
        <button type="button" class="btn btn-default btn-lg btn-block treatment_option_active"  style="padding: 35px 0;" id="treatment_option_active" onclick="treatment_option_active();"> TREATMENT OPTION</button>  
        </div>
        <div id="text_weight" class="text_weight" style="display: none">
          <h3>WEIGHT</h3>
          {{ Form::number('weight',old('weight'), array('class' => 'form-control', 'placeholder' => 'Weight*', 'required' => 'true', 'id' => 'weight', 'style' => 'height:70px; font-size:36px;')) }}<br>   
          <a href="#" class="btn btn-success btn-lg pull-right" onclick="store();" style="padding: 35px !important; font-size: 20px;"> <i class="fa fa-check"></i> SAVE</a> 
        </div>

        <div id="text_salinity" class="text_salinity" style="display: none">
          <h3>SALINITY</h3>
          {{ Form::number('salinity',old('salinity'), array('class' => 'form-control', 'placeholder' => 'Salinity*', 'required' => 'true', 'id' => 'salinity', 'style' => 'height:70px; font-size:36px;')) }}<br> 
        </div>


        <a id="next_total_salinity" href="#" type="button" class="btn btn-primary btn-lg pull-right next_total_salinity" style="padding: 35px !important; font-size: 20px; display: none"><i class="fa fa-arrow-circle-right faa-pulse animated"></i><br/><p>NEXT</p></a>

        <div id="text_duration" class="text_duration" style="display: none">
          <h3>DURATION</h3>
          {{ Form::number('duration',old('duration'), array('class' => 'form-control', 'placeholder' => 'Duration*', 'required' => 'true', 'id' => 'duration', 'style' => 'height:70px; font-size:36px;')) }}<br>   
          <a href="#" class="btn btn-success btn-lg pull-right" onclick="storetreatmentoption();" style="padding: 35px !important; font-size: 20px;"> <i class="fa fa-check"></i> SAVE</a> 
        </div>
      </div>  
    </div>
  </div> 
</div>   
</form> 
<div id="loader"></div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script type="text/javascript">
  $("button").click(function() {
    var fired_button = $(this).val();
    document.getElementById ("tally_peeling_master_id").value = fired_button; 
    document.getElementById("weight").focus();
    document.getElementById("waste").focus();
  }); 

  function store()
  {  
   var weight = document.getElementById ("weight").value;
   var tally_peeling_master_id = document.getElementById ("tally_peeling_master_id").value;
   if(weight=='' || tally_peeling_master_id=='')
   {
    swal("Error!", "All data is required!", "error")
  }
  else{ 
    showLoader();
    $.ajax({
      type: 'POST',
      url: "{{ URL::route('storepeeling', $temp_tally->id) }}",
      data: {
        '_token': $('input[name=_token]').val(), 
        'tally_peeling_master_id': $('#tally_peeling_master_id').val(),
        'weight': $('#weight').val(), 
        'purchase_id': $('#purchase_id').val(), 
        'temp_tally_id': $('#temp_tally_id').val()  
      },
      success: function(data) { 
        if ((data.errors)) {
         swal("Error!", "Gat data failed!", "error")
       } else {

         swal("Success!", "Peeling data has been save!", "success")
         hideLoader();

         $('.peeling_type').show(100);
         $('.text_weight').hide(100);
         $('.back').hide(100);

         $('.treatment_option_active').show(100);

         document.getElementById ("weight").value = ""; 
         document.getElementById ("id").value = ""; 


         @foreach($tally_detail as $key => $tally_detail_btn)
         $("#{{$tally_detail_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default"); 
         @endforeach
       } 
     },

   })
  }
};


function storetreatmentoption()
{  
 var salinity = document.getElementById ("salinity").value;
 var duration = document.getElementById ("duration").value;
 if(salinity=='' || duration=='')
 {
  swal("Error!", "All data is required!", "error")
}
else{ 
  showLoader();
  $.ajax({
    type: 'POST',
    url: "{{ URL::route('storepeelingtreatment', $temp_tally->id) }}",
    data: {
      '_token': $('input[name=_token]').val(),  
      'salinity': $('#salinity').val(),  
      'duration': $('#duration').val(), 
      'temp_tally_id': $('#temp_tally_id').val()  
    },
    success: function(data) { 
      if ((data.errors)) {
       swal("Error!", "Gat data failed!", "error")
     } else {

       swal("Success!", "Peeling treatment has been save!", "success")
       hideLoader();

       $('.peeling_type').show(100);
       $('.text_duration').hide(100);
       $('.back_duration').hide(100);

       $('.treatment_option_active').show(100);

       document.getElementById ("salinity").value = "";
       document.getElementById ("duration").value = ""; 
       document.getElementById ("id").value = ""; 


       @foreach($tally_detail as $key => $tally_detail_btn)
       $("#{{$tally_detail_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default"); 
       @endforeach
     } 
   },

 })
}
};

function storeclose ()
{ 
  swal({
    title: 'Are you sure?',
    text: "You won't be able to revert this peeling!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#d33',
    cancelButtonColor: '#BDBDBD',
    confirmButtonText: 'Yes, close it!'
  }).then(function () {

    $('#form_close').submit();
  })
};  

$(document).ready(function() {
  $('#peeling_type').click(function() {  

    $('.peeling_type').hide(100);
    $('.text_weight').show(100);
    $('.treatment_option_active').hide(100);
    $('.back').show(100); 

  });

  $('#back').click(function() { 
    $('.peeling_type').show(100);
    $('.text_weight').hide(100);
    $('.back').hide(100); 
    $('.treatment_option_active').show(100);
  }); 

  $('#treatment_option_active').click(function() { 
    $('.peeling_type').hide(100);
    $('.treatment_option_active').hide(100);
    $('.back').hide(100); 
    $('.text_salinity').show(100);
    $('.next_total_salinity').show(100); 
    $('.back_salinity').show(100); 
  }); 

  $('#next_total_salinity').click(function() {  
    $('.next_total_salinity').hide(100); 
    $('.text_duration').show(100); 
    $('.text_salinity').hide(100);
    $('.back_salinity').hide(100);
    $('.back_duration').show(100);
  });

  $('#back_salinity').click(function() { 
    $('.receiving_treatment').hide(100);
    $('.back_salinity').hide(100);
    $('.peeling_type').show(100);
    $('.additional').hide(100);
    $('.back_width').hide(100);
    $('.text_salinity').hide(100);
    $('.next_total_salinity').hide(100);  
    $('.treatment_option_active').show(100);
  });

  $('#back_duration').click(function() { 
    $('.receiving_treatment').hide(100);
    $('.back_duration').hide(100);
    $('.back_salinity').show(100);
    $('.text_salinity').show(100);
    $('.text_duration').hide(100);
    $('.next_total_salinity').show(100); 
  });

  $('#all_element').click(function() {   

   var type_name = document.getElementById ("type_name").value;  

   document.getElementById ("breadcrumb").innerHTML = type_name;
 });


}); 

document.getElementById("loader").style.display = "none";

function showLoader() {
  document.getElementById("loader").style.display = "block";
}

function hideLoader() {
  document.getElementById("loader").style.display = "none";
}
</script>

