@extends('layouts.tablet.template') 
<style type="text/css">
  .spacer { 
    margin: 2px 2px !important;
  }
</style> 
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12"> 
    <div class="col-md-12">
      <div class="x_panel"> 
        <div class="x_content">  
          <div> 
           <center><h2>TALLY MASTER</h2></center> 
           <hr>
         </div>  
       </div>
     </div>      
     <center>
      @actionStart('tally_receiving', 'create')
      @if(isset($last_temp_tally_rec))
      <div class="col-md-3 col-sm-3 col-xs-3" style="padding: 5px">
       <a href="{{ URL::route('tally_form.receiving', $last_temp_tally_rec->id) }}" class="btn btn-primary btn-lg btn-block" style="padding: 30px 0;"> <i class="fa fa-level-down"></i> RECEIVING <br/>[ 
         {{$last_temp_tally_rec->trans_code}} 
         ]</a> 
       </div> 
       @else
       <div class="col-md-3 col-sm-3 col-xs-3" style="padding: 5px">
         <a href="#" class="btn btn-default btn-lg btn-block" style="padding: 30px 0;"> <i class="fa fa-level-down"></i> RECEIVING <br/>[ 
           KOSONG 
           ]</a> 
         </div>
         @endif
         @actionEnd

         @actionStart('tally_headsplit', 'create')
         @if(isset($last_temp_tally_headsplit))
         <div class="col-md-3 col-sm-3 col-xs-3" style="padding: 5px">
           <a href="{{ URL::route('tally_form.deheading', $last_temp_tally_headsplit->id) }}" class="btn btn-primary btn-lg btn-block" style="padding: 30px 0;"> <i class="fa fa-hand-scissors-o"></i> HEAD SPLIT <br/>[ 
             {{$last_temp_tally_headsplit->trans_code}} 
             ]</a>  
           </div>
           @else
           <div class="col-md-3 col-sm-3 col-xs-3" style="padding: 5px">
             <a href="#" class="btn btn-default btn-lg btn-block" style="padding: 30px 0;"> <i class="fa fa-hand-scissors-o"></i> HEAD SPLIT <br/>[ 
               KOSONG 
               ]</a> 
             </div>
             @endif
             @actionEnd

             @actionStart('tally_peeling', 'create')
             @if(isset($last_temp_tally_peeling))
             <div class="col-md-3 col-sm-3 col-xs-3" style="padding: 5px">
               <a href="{{ URL::route('tally_form.peeling', $last_temp_tally_peeling->id) }}" class="btn btn-primary btn-lg btn-block" style="padding: 30px 0;"> <i class="fa fa-paint-brush"></i> PEELING <br/>[ 
                 {{$last_temp_tally_peeling->trans_code}} 
                 ]</a>  
               </div>
               @else
               <div class="col-md-3 col-sm-3 col-xs-3" style="padding: 5px">
                 <a href="#" class="btn btn-default btn-lg btn-block" style="padding: 30px; font-size: 25px"> <i class="fa fa-paint-brush" style="padding: 30px 0;"></i> PEELING <br/>[ 
                   KOSONG 
                   ]</a>
                 </div> 
                 @endif
                 @actionEnd

                 @actionStart('tally_grading', 'create')
                 @if(isset($last_temp_tally_grading))
                 <div class="col-md-3 col-sm-3 col-xs-3" style="padding: 5px">
                   <a href="{{ URL::route('tally_form.grading', $last_temp_tally_grading->id) }}" class="btn btn-primary btn-lg btn-block" style="padding: 30px 0;"> <i class="fa fa-sliders"></i> GRADING <br/>[ 
                     {{$last_temp_tally_grading->trans_code}} 
                     ]</a> 
                   </div> 
                   @else
                   <div class="col-md-3 col-sm-3 col-xs-3" style="padding: 5px">
                     <a href="#" class="btn btn-default btn-lg btn-block" style="padding: 30px 0;"> <i class="fa fa-sliders"></i> GRADING <br/>[ 
                       KOSONG 
                       ]</a> 
                     </div>
                     @endif
                     @actionEnd

                     @actionStart('tally_packing', 'create')
                     @if(isset($last_temp_tally_packing))
                     <div class="col-md-3 col-sm-3 col-xs-3" style="padding: 5px">
                       <a href="{{ URL::route('tally_form.createpacking', $last_temp_tally_packing->id) }}" class="btn btn-primary btn-lg btn-block" style="padding: 30px 0;"> <i class="fa fa-suitcase"></i> PACKING <br/>[ 
                         {{$last_temp_tally_packing->trans_code}} 
                         ]</a>  
                       </div>
                       @else
                       <div class="col-md-3 col-sm-3 col-xs-3" style="padding: 5px">
                         <a href="#" class="btn btn-default btn-lg btn-block" style="padding: 30px 0;"> <i class="fa fa-suitcase"></i> PACKING <br/>[ 
                           KOSONG 
                           ]</a> 
                         </div>
                         @endif
                         @actionEnd
                       </center> 
                     </div> 

                     <div class="row"> 
                       <div class="col-md-12 col-sm-12 col-xs-12"> 
                         <hr>
                         <div class="col-md-12">
                          <div class="x_panel"> 
                            <div class="x_content">  
                             <center>
                               <a href="" type="button" class="btn btn-success btn-circle btn-xl" style="margin-left: 10px"><i class="fa fa-refresh faa-pulse animated"></i><br/><p style="font-size: 14px;">Refresh</p></a>
                             </center>
                           </div>
                         </div>
                       </div>
                     </div> 
                     </div> 
                 </div> 





