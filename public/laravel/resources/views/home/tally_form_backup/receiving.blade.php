@extends('layouts.tablet.template')
<style type="text/css">
  .nomargin {
    margin: -5px -5px 3px 3px !important;
    padding: 5px;
  }
</style>

<meta name="_token" content="{{ csrf_token() }}"/>
<div id="all_element">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12"> 
      <div class="x_panel"> 
        <div class="x_content">   
          <div class="col-md-4 col-sm-4 col-xs-4">
           <center><h1><b>RECEIVING</b></h1></center>
           <center><h4 id="breadcrumb"></h4></center> 
         </div> 
         <div class="col-md-4 col-sm-4 col-xs-4">
           <center><h1>{{$temp_tally->trans_code}}</h1></center>   
         </div>  
         {!! Form::open(array('route' => ['tally_form.closereceiving', $temp_tally->id], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_close'))!!}
         {{ csrf_field() }}  
         <div class="col-md-2 col-sm-2 col-xs-2" style="padding: 5px">  
          <button type="button" id="btn_close" onclick="storeclose();" class="btn btn-danger btn-lg btn-block nomargin" style="font-size: 20px; padding: 10px 0;"><i class="fa fa-lock faa-pulse animated"></i><br/><p>CLOSE</p></button> 
        </div>
        {!! Form::close() !!}   

        <div class="col-md-2 col-sm-2 col-xs-2" style="padding: 5px">  
          <a href="{{ URL::route('tally_form.index') }}" type="button" class="btn btn-primary btn-lg btn-block nomargin"  style="padding: 10px 0; font-size: 20px;"><i class="fa fa-home faa-pulse animated"></i><br/><p>HOME</p></a>
        </div> 
      </div>
    </div>
  </div>
</div>

<div class="row"> 
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel"> 
      <div class="x_content">  
       <div class="col-md-4 col-sm-4 col-xs-4">   
       </div>  
       <div class="col-md-4 col-sm-4 col-xs-4">   
       </div>  
       <div class="col-md-2 col-sm-2 col-xs-2" style="padding: 5px">  
        <a href="{{ URL::route('tally_form.historyreceiving', $temp_tally->id) }}" type="button" class="btn btn-primary btn-lg pull-right btn-block nomargin" style="padding: 10px; font-size: 20px;"><i class="fa fa-tasks faa-pulse animated"></i><br/><p>HISTORY</p></a>
      </div> 
      <div class="col-md-2 col-sm-2 col-xs-2" style="padding: 5px"> 
        <a id="back_receiving"  style="display: none" href="#" type="button" class="btn btn-warning btn-lg  btn-block back_receiving nomargin" style="margin-left: 10px 0; padding: 10px; font-size: 20px"><i class="fa fa-arrow-circle-left faa-pulse animated"></i><br/><p>PREVIOUS</p></a>

        <a id="back_size"  style="display: none" href="#" type="button" class="btn btn-warning btn-lg  btn-block back_size" style="margin-left: 10px; padding: 10px 0; font-size: 20px;"><i class="fa fa-arrow-circle-left faa-pulse animated"></i><br/><p>PREVIOUS</p></a>

        <a id="back_width"  style="display: none" href="#" type="button" class="btn btn-warning btn-lg  btn-block back_width" style="margin-left: 10px; padding: 10px 0; font-size: 20px;"><i class="fa fa-arrow-circle-left faa-pulse animated"></i><br/><p>PREVIOUS</p></a>

        <a id="back_additional"  style="display: none" href="#" type="button" class="btn btn-warning btn-lg  btn-block back_additional" style="margin-left: 10px; padding: 10px 0; font-size: 20px;"><i class="fa fa-arrow-circle-left faa-pulse animated"></i><br/><p>PREVIOUS</p></a>

        <a id="back_weight"  style="display: none" href="#" type="button" class="btn btn-warning btn-lg  btn-block back_weight" style="margin-left: 10px; padding: 10px 0; font-size: 20px;"><i class="fa fa-arrow-circle-left faa-pulse animated"></i><br/><p>PREVIOUS</p></a>

        <a id="back_pan"  style="display: none" href="#" type="button" class="btn btn-warning btn-lg  btn-block back_pan" style="margin-left: 10px; padding: 10px 0; font-size: 20px;"><i class="fa fa-arrow-circle-left faa-pulse animated"></i><br/><p>PREVIOUS</p></a>

        <a id="back_salinity"  style="display: none" href="#" type="button" class="btn btn-warning btn-lg  btn-block back_salinity" style="margin-left: 10px; padding: 10px 0; font-size: 20px"><i class="fa fa-arrow-circle-left faa-pulse animated"></i><br/><p>PREVIOUS</p></a> 

        <a id="back_duration"  style="display: none" href="#" type="button" class="btn btn-warning btn-lg back_duration" style="margin-left: 10px; padding: 10px 0; font-size: 20px"><i class="fa fa-arrow-circle-left faa-pulse animated"></i><br/><p>PREVIOUS</p></a>  
      </div>
    </div>
  </div>
</div> 
<div class="col-md-12 col-sm-12 col-xs-12">
 <hr>
</div> 

<form class="form-horizontal" role="form">
  {{ csrf_field() }}
  <div class="row"> 
    <div class="col-md-12 col-sm-12 col-xs-12">  
      <div class="x_panel"> 
        <div class="x_content">
          <div style="display: none">
            <h3>GROUP</h3>
            <a href="#" class="btn btn-default btn-lg" id="btn_fresh" onclick="set_btn_fresh_active();"> FRESH</a>
            <a href="#" class="btn btn-default btn-lg" id="btn_bekukapal" onclick="set_btn_bekukapal_active();"> BEKU KAPAL</a> 
            {{ Form::hidden('tally_group',old('tally_group'), array('required' => 'true', 'id' => 'tally_group')) }}    
            <br/>  
          </div>
          <div id="rec_type" class="rec_type">
            <h3>TYPE</h3>

            <div class="col-md-3 col-sm-3 col-xs-3" style="padding: 5px">
              <a href="#" class="btn btn-default btn-lg btn-block nomargin" style="padding: 70px 0" id="btn_receiving" onclick="set_btn_receiving_active();"> RECEIVING</a>
            </div>

            <div class="col-md-3 col-sm-3 col-xs-3" style="padding: 5px">
              <a href="#" class="btn btn-default btn-lg btn-block nomargin" style="padding: 70px 0" id="btn_treatment" onclick="set_btn_treatment_active();"> TREATMENT</a>  
            </div>

            <div class="col-md-3 col-sm-3 col-xs-3" style="padding: 5px">
              <button type="button" class="btn btn-default btn-lg btn-block nomargin" style="padding: 70px 0" id="btn_additional" style="margin-top: 5px" onclick="set_btn_additional_active();"> ADDITIONAL</button>
            </div> 
            {{ Form::hidden('tally_type',old('tally_type'), array('required' => 'true', 'id' => 'tally_type')) }}  
          </div>
          <div class="col-md-3 col-sm-3 col-xs-3" style="padding: 5px">
            <button type="button" class="btn btn-default btn-lg btn-block nomargin treatment_option_active"  style="padding: 70px 0; margin-top: 5px;margin-top: 5px" id="treatment_option_active" onclick="treatment_option_active();"> TREATMENT OPTION</button>  
          </div>

          <div id="receiving_treatment" class="receiving_treatment" style="display: none">
            <div class="row"> 
              <div class="col-md-12 col-sm-12 col-xs-12">  
                <div class="x_panel"> 
                  <div class="x_content">
                    <h3>SIZE</h3> 
                    <div id="size_fresh" class="size_fresh">
                      @foreach($tally_detail as $key => $tally_details)
                      <div class="col-md-3 col-sm-3 col-xs-3" style="padding: 5px"> 
                        <button type="button" class="btn btn-default btn-lg btn-block nomargin" style="padding: 30px 0;" id="{{$tally_details->id}}" onclick="set_btn_{{$tally_details->id}}_active();" value="{{$tally_details->id}}"><p id="{{$tally_details->note}}"> {{$tally_details->note}}</p></button>  
                      </div>
                      <script type="text/javascript">
                       function set_btn_{{$tally_details->id}}_active()
                       { 
                        @foreach($tally_detail as $key => $tally_detail_btn)
                        $("#{{$tally_detail_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default"); 
                        @endforeach
                        @foreach($tally_additional as $key => $tally_add_btn)
                        $("#add_{{$tally_add_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default"); 
                        @endforeach
                        $("#{{$tally_details->id}}").removeClass("btn btn-default").addClass("btn btn-primary");
                        var size_name = document.getElementById ("{{$tally_details->note}}").innerHTML; 
                        document.getElementById ("size_name").value = size_name;
                      };  
                    </script> 
                    @endforeach 
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div id="size_beku_kapal" class="size_beku_kapal" style="display: none">
            @foreach($size_beku_kapal as $key => $size_beku_kapals) 
            <button type="button" class="btn btn-default btn-lg" style="margin-top: 5px; padding: 30px;" id="{{$size_beku_kapals->id}}" onclick="set_btn_{{$size_beku_kapals->id}}_active();" value="{{$size_beku_kapals->id}}">{{$size_beku_kapals->note}}</button> 

            <script type="text/javascript">
             function set_btn_{{$size_beku_kapals->id}}_active()
             { 
              @foreach($size_beku_kapal as $key => $tally_beku_kapal_btn)
              $("#{{$tally_beku_kapal_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default"); 
              @endforeach
              @foreach($tally_additional as $key => $tally_add_btn)
              $("#add_{{$tally_add_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default"); 
              @endforeach
              $("#{{$size_beku_kapals->id}}").removeClass("btn btn-default").addClass("btn btn-primary"); 
            };  
          </script> 
          @endforeach  
        </div> 
      </div> 


      {{ Form::hidden('size_name',old('size_name'), array('required' => 'true', 'id' => 'size_name')) }} 
      {{ Form::hidden('size_id',old('size_id'), array('required' => 'true', 'id' => 'size_id')) }} 
      {{ Form::hidden('purchase_id',old('purchase_id', $temp_tally->id_purchase), array('required' => 'true', 'id' => 'purchase_id')) }}
      {{ Form::hidden('temp_tally_id',old('temp_tally_id', $temp_tally->id), array('required' => 'true', 'id' => 'temp_tally_id')) }} 
      <div id="text_weight" class="text_weight" style="display: none">
        <br/> 
        <h3>WEIGHT</h3>
        {{ Form::number('weight',old('weight'), array('class' => 'form-control', 'placeholder' => 'Weight*', 'required' => 'true', 'id' => 'weight', 'style' => 'height:70px; font-size:36px;')) }}<br>  
        <a href="#" class="btn btn-success btn-lg pull-right" onclick="store();" style="padding: 30px; margin-left: 3px;"> <i class="fa fa-check"></i><br/> SAVE</a>
      </div>

      <div id="additional" class="additional" style="display: none">
        <div class="row"> 
          <div class="col-md-12 col-sm-12 col-xs-12">  
            <div class="x_panel"> 
              <div class="x_content">
                <h3>ADDITIONAL VALUE</h3>
                @foreach($tally_additional as $key => $tally_additionals)
                <div class="col-md-3 col-sm-3 col-xs-3" style="padding: 5px"> 
                  <button type="button" class="btn btn-default btn-lg btn-block" style="padding: 30px 0" id="add_{{$tally_additionals->id}}" onclick="set_btn_add_{{$tally_additionals->id}}_active();" value="{{$tally_additionals->id}}"><p id="{{$tally_additionals->name}}"> {{$tally_additionals->name}}</p></button> 
                </div> 
                <script type="text/javascript">
                 function set_btn_add_{{$tally_additionals->id}}_active()
                 { 
                  @foreach($tally_additional as $key => $tally_add_btn)
                  $("#add_{{$tally_add_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default"); 
                  @endforeach
                  @foreach($tally_detail as $key => $tally_detail_btn)
                  $("#{{$tally_detail_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default"); 
                  @endforeach
                  $("#add_{{$tally_additionals->id}}").removeClass("btn btn-default").addClass("btn btn-primary"); 

                  var additional_name = document.getElementById ("{{$tally_additionals->name}}").innerHTML; 
                  document.getElementById ("additional_name").value = additional_name;
                };  
              </script> 
              @endforeach 
              {{ Form::hidden('additional_name',old('additional_name'), array('required' => 'true', 'id' => 'additional_name')) }} 
            </div>
          </div>
        </div>
      </div>
    </div>  
<div class="row"> 
          <div class="col-md-12 col-sm-12 col-xs-12">   
    <div id="weight_additional" class="weight_additional" style="display: none">
      {{ Form::hidden('tally_additional_master_id',old('tally_additional_master_id'), array('required' => 'true', 'id' => 'tally_additional_master_id')) }}  
      <br/> 

      <h3>WEIGHT</h3>
      {{ Form::number('weight_add',old('weight_add'), array('class' => 'form-control', 'placeholder' => 'Weight*', 'required' => 'true', 'id' => 'weight_add', 'style' => 'height:70px; font-size:36px;')) }}<br> 
    </div>  

    <div id="pan_additional" class="pan_additional" style="display: none">
      <h3>PAN</h3>
      {{ Form::number('pan',old('pan'), array('class' => 'form-control', 'placeholder' => 'Pan*', 'required' => 'true', 'id' => 'pan', 'style' => 'height:70px; font-size:36px;')) }}<br>  
    </div>

    <a id="next_weight" href="#" type="button" class="btn btn-primary btn-lg pull-right next_weight" style="padding: 30px !important; font-size: 20px; display: none"><i class="fa fa-arrow-circle-right faa-pulse animated"></i><br/><p>NEXT</p></a>

    <a id="next_pan" href="#" type="button" class="btn btn-primary btn-lg pull-right next_pan" style="padding: 30px !important; display: none"><i class="fa fa-arrow-circle-right faa-pulse animated"></i><br/><p>NEXT</p></a>

    <div id="rest_additional" class="rest_additional" style="display: none">
      <h3>REST</h3>
      {{ Form::number('rest',old('rest'), array('class' => 'form-control', 'placeholder' => 'Rest*', 'required' => 'true', 'id' => 'rest', 'style' => 'height:70px; font-size:36px;')) }}<br> 
      <a href="#" class="btn btn-success btn-lg pull-right" onclick="storeadd();" style="padding: 30px; font-size: 20px;"> <i class="fa fa-check"></i><br/> SAVE</a>  
    </div> 

    <div id="text_salinity" class="text_salinity" style="display: none">
      <h3>SALINITY</h3>
      {{ Form::number('salinity',old('salinity'), array('class' => 'form-control', 'placeholder' => 'Salinity*', 'required' => 'true', 'id' => 'salinity', 'style' => 'height:70px; font-size:36px;')) }}<br> 
    </div>


    <a id="next_total_salinity" href="#" type="button" class="btn btn-primary btn-lg pull-right next_total_salinity" style="padding: 30px !important; font-size: 20px; display: none"><i class="fa fa-arrow-circle-right faa-pulse animated"></i><br/><p>NEXT</p></a>

    <div id="text_duration" class="text_duration" style="display: none">
      <h3>DURATION</h3>
      {{ Form::number('duration',old('duration'), array('class' => 'form-control', 'placeholder' => 'Duration*', 'required' => 'true', 'id' => 'duration', 'style' => 'height:70px; font-size:36px;')) }}<br>   
      <a href="#" class="btn btn-success btn-lg pull-right" onclick="storetreatmentoption();" style="padding: 30px; font-size: 20px"> <i class="fa fa-check"></i> SAVE</a> 
      </div>
      </div>
    </div>
  </div>  
</div> 
</div> 
</form> 
<div id="loader"></div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script type="text/javascript">
  $("button").click(function() {
    document.getElementById ("size_id").value = "";
    document.getElementById ("tally_additional_master_id").value = "";
    var fired_button = $(this).val();
    document.getElementById ("size_id").value = fired_button;
    document.getElementById ("tally_additional_master_id").value = fired_button;
    document.getElementById("weight").focus();
  });

  function set_btn_fresh_active()
  { 
    $("#btn_fresh").removeClass("btn btn-default").addClass("btn btn-primary");
    $("#btn_bekukapal").removeClass("btn btn-primary").addClass("btn btn-default");  
    $('.treatment_option_active').hide(100);
    document.getElementById ("tally_group").value = "FRESH";
  };

  function set_btn_bekukapal_active()
  { 
    $("#btn_fresh").removeClass("btn btn-primary").addClass("btn btn-default");
    $("#btn_bekukapal").removeClass("btn btn-default").addClass("btn btn-primary"); 
    $('.treatment_option_active').hide(100); 
    document.getElementById ("tally_group").value = "BEKU KAPAL";
  };

  function set_btn_receiving_active()
  { 
    $("#btn_receiving").removeClass("btn btn-default").addClass("btn btn-primary");
    $("#btn_treatment").removeClass("btn btn-primary").addClass("btn btn-default");
    $("#btn_additional").removeClass("btn btn-primary").addClass("btn btn-default");
    $('.treatment_option_active').hide(100);
    document.getElementById ("tally_type").value = "RECEIVING";
  };

  function set_btn_treatment_active()
  { 
    $("#btn_receiving").removeClass("btn btn-primary").addClass("btn btn-default");
    $("#btn_treatment").removeClass("btn btn-default").addClass("btn btn-primary");
    $("#btn_additional").removeClass("btn btn-primary").addClass("btn btn-default");
    $('.treatment_option_active').hide(100);
    document.getElementById ("tally_type").value = "TREATMENT";
  };

  function set_btn_additional_active()
  { 
    $("#btn_receiving").removeClass("btn btn-primary").addClass("btn btn-default");
    $("#btn_treatment").removeClass("btn btn-primary").addClass("btn btn-default");
    $("#btn_additional").removeClass("btn btn-default").addClass("btn btn-primary");
    $('.treatment_option_active').hide(100);
    document.getElementById ("tally_type").value = "ADDITIONAL";
  };


  function store()
  { 
   var tally_type = document.getElementById ("tally_type").value;
   var weight = document.getElementById ("weight").value;
   var size_id = document.getElementById ("size_id").value;
   var tally_group = document.getElementById ("tally_group").value;
   if(tally_type=='' || weight=='' || size_id=='')
   {
    swal("Error!", "All data is required!", "error")
  }
  else{
   showLoader();
   $.ajax({
    type: 'POST',
    url: "{{ URL::route('store', $tally_details->id) }}",
    data: {
      '_token': $('input[name=_token]').val(),
      'tally_type': $('#tally_type').val(),
      'size_id': $('#size_id').val(),
      'weight': $('#weight').val(),
      'purchase_id': $('#purchase_id').val(),
      'temp_tally_id': $('#temp_tally_id').val(),
      'tally_group': $('#tally_group').val()  
    },
    success: function(data) { 
      if ((data.errors)) {
       swal("Error!", "Gat data failed!", "error")
     } else {

      swal("Success!", "Receiving/treatment data has been save!", "success")
      hideLoader();
      $('.receiving_treatment').hide(100);
      $('.back_receiving').hide(100);
      $('.rec_type').show(100);
      $('.additional').hide(100);
      $('.back_width').hide(100);
      $('.text_weight').hide(100);
      $('.weight_additional').hide(100); 
      $('.rest_additional').hide(100);  
      $('.treatment_option_active').show(100); 

      document.getElementById ("tally_type").value = "";
      document.getElementById ("weight").value = "";
      document.getElementById ("size_id").value = "";
      document.getElementById ("size_name").value ="";
      document.getElementById ("additional_name").value = "";
      document.getElementById ("breadcrumb").innerHTML = "";
      $("#btn_additional").removeClass("btn btn-primary").addClass("btn btn-default");
      $("#btn_receiving").removeClass("btn btn-primary").addClass("btn btn-default");
      $("#btn_treatment").removeClass("btn btn-primary").addClass("btn btn-default");

      @foreach($size_beku_kapal as $key => $tally_beku_kapal_btn)
      $("#{{$tally_beku_kapal_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default"); 
      @endforeach

      @foreach($tally_detail as $key => $tally_detail_btn)
      $("#{{$tally_detail_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default"); 
      @endforeach
    } 
  },
})
 }
};

function storeadd()
{ 
 var tally_additional_master_id = document.getElementById ("tally_additional_master_id").value;
 var weight_add = document.getElementById ("weight_add").value;
 var pan = document.getElementById ("pan").value;
 var rest = document.getElementById ("rest").value;
 var tally_group = document.getElementById ("tally_group").value;
 if(tally_additional_master_id=='' || weight_add=='' || pan=='' || rest=='')
 {
  swal("Error!", "All data is required!", "error")
}
else{
 showLoader();
 $.ajax({
  type: 'POST',
  url: "{{ URL::route('storeadditional', $tally_details->id) }}",
  data: {
    '_token': $('input[name=_token]').val(),
    'tally_additional_master_id': $('#tally_additional_master_id').val(),
    'weight_add': $('#weight_add').val(),
    'pan': $('#pan').val(),
    'rest': $('#rest').val(),
    'purchase_id': $('#purchase_id').val(),
    'temp_tally_id': $('#temp_tally_id').val(),
    'tally_group': $('#tally_group').val()  
  },
  success: function(data) { 
    if ((data.errors)) {
     swal("Error!", "Gat data failed!", "error")
   } else {


    swal("Success!", "Additional data has been save!", "success")
    hideLoader();
    document.getElementById ("tally_additional_master_id").value = "";
    document.getElementById ("weight_add").value = "";
    document.getElementById ("pan").value = "";
    document.getElementById ("rest").value = "";
    document.getElementById ("size_name").value ="";
    document.getElementById ("additional_name").value = "";
    document.getElementById ("breadcrumb").innerHTML = "";

    $('.treatment_option_active').show(100);

    $('.receiving_treatment').hide(100);
    $('.back_receiving').hide(100);
    $('.rec_type').show(100);
    $('.additional').hide(100);
    $('.back_width').hide(100);
    $('.text_weight').hide(100);
    $('.weight_additional').hide(100);  
    $('.back_size').hide(100);
    $('.back_additional').hide(100);
    $('.rest_additional').hide(100); 
    $('.back_pan').hide(100);

    $("#btn_additional").removeClass("btn btn-primary").addClass("btn btn-default");
    $("#btn_receiving").removeClass("btn btn-primary").addClass("btn btn-default");
    $("#btn_treatment").removeClass("btn btn-primary").addClass("btn btn-default");

    @foreach($size_beku_kapal as $key => $tally_beku_kapal_btn)
    $("#{{$tally_beku_kapal_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default"); 
    @endforeach

    @foreach($tally_additional as $key => $tally_additional_btn)
    $("#add_{{$tally_additional_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default"); 
    @endforeach
  } 
},

})
}
};

function storetreatmentoption()
{  
 var salinity = document.getElementById ("salinity").value;
 var duration = document.getElementById ("duration").value;
 if(salinity=='' || duration=='')
 {
  swal("Error!", "All data is required!", "error")
}
else{ 
  showLoader();
  $.ajax({
    type: 'POST',
    url: "{{ URL::route('storereceivingtreatment', $temp_tally->id) }}",
    data: {
      '_token': $('input[name=_token]').val(),  
      'salinity': $('#salinity').val(),  
      'duration': $('#duration').val(), 
      'temp_tally_id': $('#temp_tally_id').val()  
    },
    success: function(data) { 
      if ((data.errors)) {
       swal("Error!", "Gat data failed!", "error")
     } else {

       swal("Success!", "Peeling treatment has been save!", "success")
       hideLoader();


       $('.rec_type').show(100);
       $('.treatment_option_active').show(100);
       $('.text_duration').hide(100);
       $('.back_duration').hide(100);

       $('.treatment_option_active').show(100);

       document.getElementById ("salinity").value = "";
       document.getElementById ("duration").value = ""; 
       document.getElementById ("id").value = ""; 


       @foreach($tally_detail as $key => $tally_detail_btn)
       $("#{{$tally_detail_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default"); 
       @endforeach
     } 
   },

 })
}
};

function storeclose ()
{ 
  swal({
    title: 'Are you sure?',
    text: "You won't be able to revert this receiving!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#d33',
    cancelButtonColor: '#BDBDBD',
    confirmButtonText: 'Yes, close it!'
  }).then(function () {

    $('#form_close').submit();
  })
};  

$(document).ready(function() {
  $('#btn_additional').click(function() { 
    $('.receiving_treatment').hide(100);
    $('.back_receiving').show(100);
    $('.rec_type').hide(100);
    $('.back_size').hide(100);
    $('.back_width').hide(100);
    $('.text_weight').hide(100);
    $('.additional').show(100);
    $('.weight_additional').hide(100); 


  });

  $('#btn_receiving').click(function() { 
    $('.receiving_treatment').show(100);
    $('.back_receiving').show(100);
    $('.back_size').hide(100);
    $('.rec_type').hide(100);
    $('.additional').hide(100);
    $('.back_width').hide(100);
    $('.text_weight').hide(100);
    $('.additional').hide(100); 
    $('.weight_additional').hide(100); 

  });

  $('#btn_treatment').click(function() { 
    $('.receiving_treatment').show(100);
    $('.back_receiving').show(100);
    $('.back_size').hide(100);
    $('.rec_type').hide(100);
    $('.additional').hide(100);
    $('.back_width').hide(100);
    $('.text_weight').hide(100);
    $('.additional').hide(100); 
    $('.weight_additional').hide(100); 
  });

  $('#back_receiving').click(function() { 
    $('.receiving_treatment').hide(100);
    $('.back_receiving').hide(100);
    $('.rec_type').show(100);
    $('.additional').hide(100);
    $('.back_width').hide(100);
    $('.text_weight').hide(100);
    $('.weight_additional').hide(100);  
    $('.treatment_option_active').show(100);
  });

  $('#back_salinity').click(function() { 
    $('.receiving_treatment').hide(100);
    $('.back_salinity').hide(100);
    $('.rec_type').show(100);
    $('.additional').hide(100);
    $('.back_width').hide(100);
    $('.text_salinity').hide(100);
    $('.next_total_salinity').hide(100);  
    $('.treatment_option_active').show(100);
  });

  $('#back_duration').click(function() { 
    $('.receiving_treatment').hide(100);
    $('.back_duration').hide(100);
    $('.back_salinity').show(100);
    $('.text_salinity').show(100);
    $('.text_duration').hide(100);
    $('.next_total_salinity').show(100); 
  });

  $('#treatment_option_active').click(function() { 
    $('.peeling_type').hide(100);
    $('.treatment_option_active').hide(100);
    $('.back_receiving').hide(100); 
    $('.text_salinity').show(100);
    $('.next_total_salinity').show(100); 
    $('.back_salinity').show(100); 
    $('.rec_type').hide(100); 
  }); 

  $('#next_total_salinity').click(function() {  
    $('.next_total_salinity').hide(100); 
    $('.text_duration').show(100); 
    $('.text_salinity').hide(100);
    $('.back_salinity').hide(100);
    $('.back_duration').show(100);
  });

  $('#receiving_treatment').click(function() { 
    $('.receiving_treatment').hide(100);
    $('.text_weight').show(100);
    $('.back_width').show(100); 
    $('.back_receiving').hide(100);
    $('.additional').hide(100);
    $('.weight_additional').hide(100); 
  });

  $('#back_width').click(function() { 
   $('.receiving_treatment').show(100);
   $('.text_weight').hide(100);
   $('.back_width').hide(100);
   $('.back_receiving').show(100);
   $('.back_size').hide(100); 
   $('.additional').hide(100); 
   $('.weight_additional').hide(100); 
 });

  $('#btn_bekukapal').click(function() { 
    $('.size_fresh').hide(100);
    $('.size_beku_kapal').show(100);
  });

  $('#btn_fresh').click(function() { 
    $('.size_fresh').show(100);
    $('.size_beku_kapal').hide(100);
  });

  $('#additional').click(function() { 
    $('.additional').hide(100); 
    $('.weight_additional').show(100);
    $('.back_size').hide(100);
    $('.back_width').hide(100);
    $('.back_additional').show(100);
    $('.back_receiving').hide(100);
    $('.next_weight').show(100); 
  });

  $('#back_additional').click(function() { 
    $('.additional').show(100); 
    $('.weight_additional').hide(100); 
    $('.back_size').hide(100);
    $('.back_width').hide(100);
    $('.back_additional').hide(100);
    $('.back_receiving').show(100);
    $('.next_weight').hide(100); 
  });

  $('#next_weight').click(function() { 
    $('.next_weight').hide(100); 
    $('.pan_additional').show(100); 
    $('.weight_additional').hide(100); 
    $('.next_pan').show(100); 
    $('.back_additional').hide(100);
    $('.back_weight').show(100);
  });

  $('#next_pan').click(function() { 
    $('.next_pan').hide(100); 
    $('.back_pan').show(100); 
    $('.pan_additional').hide(100); 
    $('.weight_additional').hide(100);
    $('.back_weight').hide(100);
    $('.rest_additional').show(100); 
  });

  $('#back_weight').click(function() { 
    $('.next_weight').show(100); 
    $('.pan_additional').hide(100); 
    $('.weight_additional').show(100); 
    $('.next_pan').hide(100); 
    $('.back_additional').show(100);
    $('.back_weight').hide(100);
  });

  $('#back_pan').click(function() { 
    $('.next_pan').show(100); 
    $('.back_pan').hide(100); 
    $('.pan_additional').show(100); 
    $('.weight_additional').hide(100);
    $('.back_weight').show(100);
    $('.rest_additional').hide(100); 
  }); 

  $('#all_element').click(function() {   

   var tally_type = document.getElementById ("tally_type").value; 
   var size_name = document.getElementById ("size_name").value;
   var additional_name = document.getElementById ("additional_name").value;

   document.getElementById ("breadcrumb").innerHTML = tally_type + "/" + size_name + "/" + additional_name;
 });
}); 

document.getElementById("loader").style.display = "none";

function showLoader() {
  document.getElementById("loader").style.display = "block";
}

function hideLoader() {
  document.getElementById("loader").style.display = "none";
}

</script>
