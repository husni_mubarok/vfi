@extends('layouts.tablet.template')
<meta name="_token" content="{{ csrf_token() }}"/>    
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12"> 
    <div class="col-md-12">
      <div class="x_panel"> 
        <div class="x_content">  
          <div> 
           <center><h2>FORM GRADING</h2></center>
           <center><h4>{{$temp_tally->trans_code}}</h4></center>  
           <hr>
         </div>  
       </div>
     </div>  
   </div> 

   <div class="col-md-12">
    <div class="x_panel"> 
      <div class="x_content">  
        <div>      
 
          <a href="{{ URL::route('tally_form.index') }}" type="button" class="btn btn-primary btn-lg"  style="padding: 12px; font-size: 20px; margin-left: -1px;  width: 70px;"><i class="fa fa-home faa-pulse animated"></i><br/><p style="font-size: 12px; margin-top: -0px;">HOME</p></a>

          <a href="{{ URL::route('tally_form.grading', $temp_tally->id) }}" type="button" class="btn btn-primary btn-lg" style="margin-left: -1px; padding: 12px; font-size: 20px; width: 70px;"><i class="fa fa-tasks faa-pulse animated"></i><br/><p style="font-size: 12px; margin-top: -0px;">FORM</p></a> 
  
        </div>  
      </div>
    </div>  
  </div>  
</div>  


<div class="row">

  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="col-md-12">
      <div class="x_panel"> 
        <div class="x_content">  
          <table id="itemTable" class="table table-striped dataTable">
            <thead>
              <tr>
                <th>Qty Type</th>
                <th>Grade</th>
                <th>Size</th>
                <th>Freezing</th>  
                <th>Code Size</th>
                <th>Block Weight (Kg)</th>
                <th>Total Block</th>
                <th>Rest</th>
              </tr>
            </thead>
            <tbody>
            @if(isset($grading_history)) 
              @foreach($grading_history as $key => $history_grading)
              <tr>
                <td>{{$history_grading->qty_name}}</td>
                <td>{{$history_grading->grade}}</td>
                <td>{{$history_grading->size}}</td>
                <td>{{$history_grading->freezing}}</td>
                <td>{{$history_grading->code_size}}</td>
                <td>{{$history_grading->block}}</td>
                <td>{{$history_grading->total_block}}</td>
                <td>{{$history_grading->rest}}</td>
              </tr>
              @endforeach
              @endIf
            </tbody>
          </table>
        </div> 
      </div>
    </div>  
  </div>   

