@extends('layouts.tablet.template')
<meta name="_token" content="{{ csrf_token() }}"/>  
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12"> 
    <div class="col-md-12">
      <div class="x_panel"> 
        <div class="x_content">  
          <div> 
           <center><h2>HEADSPLIT HISTORY</h2></center>
           <center><h4>{{$temp_tally->trans_code}}</h4></center>  
           <hr>
         </div>  
       </div>
     </div>  
   </div> 

   <div class="col-md-12">
    <div class="x_panel"> 
      <div class="x_content">  
        <div>      

          <a href="{{ URL::route('tally_form.index') }}" type="button" class="btn btn-primary btn-lg"  style="padding: 12px; font-size: 20px; margin-left: -1px;  width: 70px;"><i class="fa fa-home faa-pulse animated"></i><br/><p style="font-size: 12px; margin-top: -0px;">HOME</p></a>

          <a href="{{ URL::route('tally_form.deheading', $temp_tally->id) }}" type="button" class="btn btn-primary btn-lg" style="margin-left: -1px; padding: 12px; font-size: 20px; width: 70px;"><i class="fa fa-tasks faa-pulse animated"></i><br/><p style="font-size: 12px; margin-top: -0px;">FORM</p></a>  
          <br/>  
        </div>  
      </div>
    </div>  
  </div>   


  <div class="row"> 
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-12">
          <div class="x_panel"> 
            <div class="x_content"> 
              <table id="itemTable" class="table table-bordered dataTable">
                <thead>
                  <tr>   
                    <th>Name</th>
                    <th><center>Weight</center></th> 
                    <th><center>Waste</center></th>   

                  </tr> 
                </thead>
                <tbody>  
                @if(isset($array_all))
                  @foreach($array_all AS $all)  
                   <tr>
                    <td colspan="3">{{$all["name"]}}</td> 
                    </tr>
                    @foreach($all["detail"] AS $detail)
                    <tr> 
                   <td></td>
                    <td>{{$detail["weight"]}}</td>    
                    <td>{{$detail["waste"]}}</td>  
                    </tr> 
@endforeach 
                  <tr>
                    <td><b>TOTAL</b></td>  
                    <td><b>{{$all["total_weight"]}}</b></td>    
                    <td><b>{{$all["total_waste"]}}</b></td>  
</tr>
                      @endforeach 
                      @endIf
                    </tbody> 
                  </table>   
                </div> 
              </div>
            </div>  
          </div>   
        </div> 
      </div> 

