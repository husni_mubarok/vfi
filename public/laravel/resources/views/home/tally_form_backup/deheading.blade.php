@extends('layouts.tablet.template')
<meta name="_token" content="{{ csrf_token() }}"/>
<div id="all_element">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12"> 
      <div class="x_panel"> 
        <div class="x_content">   
          <div class="col-md-4 col-sm-4 col-xs-4">
           <center><h1><b>HEADSPLIT</b></h1></center>
           <center><h4 id="breadcrumb"></h4></center> 
         </div> 
         <div class="col-md-4 col-sm-4 col-xs-4">
           <center><h1>{{$temp_tally->trans_code}}</h1></center>   
         </div>  
         {!! Form::open(array('route' => ['tally_form.closedeheading', $temp_tally->id], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_close'))!!}
         {{ csrf_field() }}  
         <div class="col-md-2 col-sm-2 col-xs-2" style="padding: 5px">  
          <button type="button" id="btn_close" onclick="storeclose();" class="btn btn-danger btn-lg btn-block nomargin" style="font-size: 20px; padding: 10px 0;"><i class="fa fa-lock faa-pulse animated"></i><br/><p>CLOSE</p></button> 
        </div>
        {!! Form::close() !!}   

        <div class="col-md-2 col-sm-2 col-xs-2" style="padding: 5px">  
          <a href="{{ URL::route('tally_form.index') }}" type="button" class="btn btn-primary btn-lg btn-block nomargin"  style="padding: 10px 0; font-size: 20px;"><i class="fa fa-home faa-pulse animated"></i><br/><p>HOME</p></a>
        </div> 
      </div>
    </div>
  </div>
</div>

<div class="row"> 
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel"> 
      <div class="x_content">  
       <div class="col-md-4 col-sm-4 col-xs-4">   
       </div>  
       <div class="col-md-4 col-sm-4 col-xs-4">   
       </div>  
       <div class="col-md-2 col-sm-2 col-xs-2" style="padding: 5px">  
        <a href="{{ URL::route('tally_form.historydeheading', $temp_tally->id) }}" type="button" class="btn btn-primary btn-lg pull-right btn-block nomargin" style="padding: 10px; font-size: 20px;"><i class="fa fa-tasks faa-pulse animated"></i><br/><p>HISTORY</p></a>
      </div> 
      <div class="col-md-2 col-sm-2 col-xs-2" style="padding: 5px">  
        <a id="back"  style="display: none" href="#" type="button" class="btn btn-warning btn-lg back" style="margin-left: 10px 0; padding: 10px; font-size: 20px"><i class="fa fa-arrow-circle-left faa-pulse animated"></i><br/><p>PREVIOUS</p></a>

        <a id="back_weight"  style="display: none" href="#" type="button" class="btn btn-warning btn-lg back_weight" style="margin-left: 10px 0; padding: 10px; font-size: 20px"><i class="fa fa-arrow-circle-left faa-pulse animated"></i><br/><p>PREVIOUS</p></a>
      </div>
    </div>
  </div>
</div> 
<div class="col-md-12 col-sm-12 col-xs-12">
 <hr>
</div>  

<form class="form-horizontal" role="form">
  {{ csrf_field() }}
  <div class="col-md-12 col-sm-12 col-xs-12"> 
    <div class="col-md-12">
      <div class="x_panel"> 
        <div class="x_content">   
          <br/> 
          <div id="deheading_type" class="deheading_type">
            <h3>TYPE</h3>
            @foreach($tally_detail as $key => $tally_details) 
            <div class="col-md-3 col-sm-3 col-xs-3" style="padding: 5px"> 
              <button type="button" class="btn btn-default btn-lg btn-block" style="padding: 30px 0;" id="{{$tally_details->id}}" onclick="set_btn_{{$tally_details->id}}_active();" value="{{$tally_details->id}}"><p id="{{$tally_details->name}}"> {{$tally_details->name}}</p></button>  
            </div>
            <script type="text/javascript">
             function set_btn_{{$tally_details->id}}_active()
             { 
              @foreach($tally_detail as $key => $tally_detail_btn)
              $("#{{$tally_detail_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default"); 
              @endforeach
              $("#{{$tally_details->id}}").removeClass("btn btn-default").addClass("btn btn-primary"); 

              var type_name = document.getElementById ("{{$tally_details->name}}").innerHTML; 
              document.getElementById ("type_name").value = type_name;
            };  
          </script> 
          @endforeach  
          {{ Form::hidden('tally_dehading_master_id',old('tally_dehading_master_id'), array('required' => 'true', 'id' => 'tally_dehading_master_id')) }} 
          {{ Form::hidden('purchase_id',old('purchase_id', $temp_tally->id_purchase), array('required' => 'true', 'id' => 'purchase_id')) }}  
          {{ Form::hidden('temp_tally_id',old('temp_tally_id', $temp_tally->id), array('required' => 'true', 'id' => 'temp_tally_id')) }} 
          {{ Form::hidden('type_name',old('type_name'), array('required' => 'true', 'id' => 'type_name')) }} 
          <br/> 
        </div>

        <div id="text_weight" class="text_weight" style="display: none">
          <h3>WEIGHT</h3>
          {{ Form::number('weight',old('weight'), array('class' => 'form-control', 'placeholder' => 'Weight*', 'required' => 'true', 'id' => 'weight', 'style' => 'height:70px; font-size:36px;')) }}<br>  

          <a id="next_weight" href="#" type="button" class="btn btn-primary btn-lg pull-right next_weight" style="padding: 35px !important; font-size: 20px;"><i class="fa fa-arrow-circle-right faa-pulse animated"></i><br/><p style="font-size: 12px;">NEXT</p></a>
        </div>
        <div id="text_waste" class="text_waste" style="display: none">
          <h3>WASTE</h3>
          {{ Form::number('waste',old('waste'), array('class' => 'form-control', 'placeholder' => 'Waste*', 'required' => 'true', 'id' => 'waste', 'style' => 'height:70px; font-size:36px;')) }}<br>  
          <a href="#" class="btn btn-success btn-lg pull-right" onclick="store();" style="padding: 35px; font-size: 20px"> <i class="fa fa-check"></i><br/> SAVE</a>
        </div> 
      </div>  
    </div>
  </div> 
</div>   
</form>
<div id="loader"></div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script type="text/javascript">
  function storeclose ()
  { 
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this headsplit!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#BDBDBD',
      confirmButtonText: 'Yes, close it!'
    }).then(function () {

      $('#form_close').submit();
    })
  };  

  $("button").click(function() {
    var fired_button = $(this).val();
    document.getElementById ("tally_dehading_master_id").value = fired_button; 
    document.getElementById("weight").focus();
    document.getElementById("waste").focus();
  }); 

  function store()
  { 
   var waste = document.getElementById ("waste").value;
   var weight = document.getElementById ("weight").value;
   var tally_dehading_master_id = document.getElementById ("tally_dehading_master_id").value;
   if(weight=='' || tally_dehading_master_id=='' || waste=='')
   {
    swal("Error!", "All data is required!", "error")
  }
  else{ 
    showLoader();
    $.ajax({
      type: 'POST',
      url: "{{ URL::route('storedeheading', $tally_details->id) }}",
      data: {
        '_token': $('input[name=_token]').val(), 
        'tally_dehading_master_id': $('#tally_dehading_master_id').val(),
        'weight': $('#weight').val(),
        'waste': $('#waste').val(),
        'purchase_id': $('#purchase_id').val(), 
        'temp_tally_id': $('#temp_tally_id').val()   
      },
      success: function(data) { 
        if ((data.errors)) {
         swal("Error!", "Gat data failed!", "error")
       } else {

         swal("Success!", "Deheading data has been save!", "success")
         hideLoader();
         $('.deheading_type').show(100);
         $('.text_weight').hide(100);
         $('.text_waste').hide(100);
         $('.back').hide(100); 
         $('.back_weight').hide(100);

         document.getElementById ("weight").value = "";
         document.getElementById ("waste").value = "";
         document.getElementById ("id").value = ""; 
         document.getElementById ("type_name").value = "";

         @foreach($tally_detail as $key => $tally_detail_btn)
         $("#{{$tally_detail_btn->id}}").removeClass("btn btn-primary").addClass("btn btn-default"); 
         @endforeach
       } 
     },

   })
  }
};



$(document).ready(function() {
  $('#deheading_type').click(function() { 
    $('.deheading_type').hide(100);
    $('.text_weight').show(100);
    $('.back').show(100); 
    $('.next_weight').show(100);
  });

  $('#back').click(function() { 
    $('.deheading_type').show(100);
    $('.text_weight').hide(100);
    $('.back').hide(100); 

  }); 

  $('#next_weight').click(function() { 
    $('.next_weight').hide(100);
    $('.text_weight').hide(100);
    $('.text_waste').show(100); 
    $('.back').hide(100); 
    $('.back_weight').show(100); 

  }); 

  $('#back_weight').click(function() { 
    $('.back_weight').hide(100);
    $('.back').show(100);
    $('.text_weight').show(100);
    $('.text_waste').hide(100); 
    $('.next_weight').show(100); 

  }); 

  $('#all_element').click(function() {   

   var type_name = document.getElementById ("type_name").value;  

   document.getElementById ("breadcrumb").innerHTML = type_name;
 });

}); 

document.getElementById("loader").style.display = "none";

function showLoader() {
  document.getElementById("loader").style.display = "block";
}

function hideLoader() {
  document.getElementById("loader").style.display = "none";
}
</script>

