@extends('layouts.home.template')
@section('content')

<section class="content">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<div class="x_panel">
					<h2>
					<i class="fa fa-truck"></i> Tally Packing Total Master 
						<a href="{{ URL::route('tally_packing_total_master.create') }}" class="btn btn-primary btn-lg pull-right"><i class="fa fa-plus"></i> Add</a> 
					</h2>
					<hr>
					<div class="x_content">
						<table id="supplierTable" class="table table-striped dataTable">
							<thead>
								<tr>
									<th>#</th>
									<th>Code Size</th>
									<th>Grade</th>
									<th>Qty Type</th>
									<th>Total</th> 
									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach($tally_packing_total_master as $key => $tally_packing_total_masters)
								<tr>
									<td>{{$key+1}}</td>
									<td>{{$tally_packing_total_masters->code_size}}</td>
									<td>{{$tally_packing_total_masters->grade}}</td>
									<td>{{$tally_packing_total_masters->qty_type}}</td>
									<td>{{$tally_packing_total_masters->total}}</td>  
									<td align="center"> 
										<div class="col-md-2 nopadding">
											<a href="{{ URL::route('tally_packing_total_master.edit', [$tally_packing_total_masters->id]) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
										</div>  
										<div class="col-md-2 nopadding">
											{!! Form::open(array('route' => ['tally_packing_total_master.delete', $tally_packing_total_masters->id], 'method' => 'delete'))!!}
											{{ csrf_field() }}	                    				
											<button type="submit" class="btn btn-primary btn-sm" onclick="return confirm('Confirm delete?')"><i class="fa fa-trash"></i></a></button>
											{!! Form::close() !!}
										</div> 
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section> 
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
	$(document).ready(function () {
		$("#supplierTable").DataTable(
		{
			"scrollX" : true
		});
	});
</script>
@stop