@extends('layouts.home.template')
@section('content')  
<section class="content">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-1"></div>
			<div class="col-md-5">
				<div class="x_panel">
					<h2>
						@if(isset($tally_packing_total_master))
						<i class="fa fa-pencil"></i>
						@else
						<i class="fa fa-plus"></i> 
						@endif
						<i class="fa fa-truck"></i> Supplier
					</h2>
					<hr>
					<div class="x_content">
						@include('errors.error')
						@if(isset($tally_packing_total_master))
						{!! Form::model($tally_packing_total_master, array('route' => ['tally_packing_total_master.update', $tally_packing_total_master->id], 'method' => 'PUT'))!!}
						@else
						{!! Form::open(array('route' => 'tally_packing_total_master.store'))!!}
						@endif
						{{ csrf_field() }}
						<div class="col-md-12 col-sm-12 col-xs-12 form-group">

							{{ Form::label('Qty Type') }}
							{{ Form::select('qty_type_id', $qty_type_list, old('qty_type_id'), array('class' => 'form-control', 'required' => 'true')) }}<br/>

							{{ Form::label('Code Size') }}
							{{ Form::select('code_size_id', $code_size_list, old('code_size_id'), array('class' => 'form-control', 'required' => 'true')) }}<br/>

							{{ Form::label('Grade') }}
							{{ Form::select('grade_id', $grade_list, old('grade_id'), array('class' => 'form-control', 'required' => 'true')) }}<br/>

							{{ Form::label('total', 'Total') }}
							{{ Form::number('total', old('total'), array('class' => 'form-control', 'placeholder' => 'Total', 'required' => 'true')) }}<br/>

							<button type="submit" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
						</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</section> 
@stop