@extends('layouts.home.template')
@section('content')
<div class="header_menu">
    <a href="{{ URL::route('end_product') }}">END PRODUCT</a>
</div>
<section class="content">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<div class="x_panel">
					<h2>
						@if(isset($end_product))
						<i class="fa fa-pencil"></i> 
						@else
	                	<i class="fa fa-plus"></i> 
	                	@endif
	                	<i class="fa fa-gift"></i> End Product <small>Start Project</small>
                	</h2>
					<hr>
					<div class="row bs-wizard small" style="border-bottom:0;">
		                <!-- class name =  1. complete 2.active 3.disabled-->

		                <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Step 2</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Step 1<br><i>Waste Product</i></div>
		                </div>

		                <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Step 3</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Step 2<br><i>Finish Product</i></div>
		                </div>

		                <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Step 4</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Step 3<br><i>Additional Product</i></div>
		                </div>
		                <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Finish</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Finish<br><i>Summary</i></div>
		                </div>
		            </div>
		    	  	<hr>
		    	  	<div class="col-md-12">
		    	  		@include('errors.error')
		    	  		@if(isset($end_product))
		    	  		{!! Form::model($end_product, ['route' => ['end_product.update', $end_product->id], 'method' => 'PUT', 'id' => 'form_create']) !!}
		    	  		@else
			            {!! Form::open(['route' => ['end_product.store', $cart->id], 'id' => 'form_create'])!!}
			            @endif
			            {{ csrf_field() }}
			   			<div class="col-md-6">
			   				{{ Form::label('item', 'Bahan') }}
			   				@if(isset($end_product))
			   				<p id="material">{{$end_product->cart->detail[0]->name}}</p>
			   				@else
			   				{{ Form::hidden('cart_id', $cart->id) }}
			   				<p id="material">{{$cart->detail[0]->name}}</p>
			   				@endif
			   				<br>

			   				{{ Form::label('production_id', 'Produk') }}
			   				{{ Form::select('production_id', $products, old('production_id'), ['class' => 'form-control', 'id' => 'production_id']) }}
			   				<br>

			   				{{ Form::label('amount','Jumlah Estimasi') }}
			   				@if(isset($end_product))
			   				<p>{{$end_product->amount}}</p>
			   				@else
			   				@php
			   				$estimated_qty = 0; 
			   				foreach($cart->detail as $detail)
			   				{
			   					$estimated_qty += $detail->quantity;
			   				}
			   				@endphp
			   				{{ Form::hidden('amount', $estimated_qty) }}
			   				<p>{{$estimated_qty}}</p>
			   				@endif
			   				<br>

			   				{{ Form::label('weight', 'Berat Estimasi') }}
			   				@if(isset($end_product))
			   				<p>{{number_format($end_product->weight, 2)}} kg</p>
			   				@else
			   				@php
			   				$estimated_weight = 0; 
			   				foreach($cart->detail as $detail)
			   				{
			   					$estimated_weight += ($detail->quantity * $detail->block_weight);
			   				}
			   				@endphp
			   				{{ Form::hidden('est_weight', $estimated_weight, ['id' => 'weight']) }}
			   				<p>{{number_format($estimated_weight, 2)}} kg</p>
			   				@endif
			   				<br>

			   				{{ Form::label('weight', 'Berat setelah Defrost') }}
			   				{{ Form::number('weight', old('weight'), ['class' => 'form-control', 'id' => 'input_weight']) }}
			   				<br>

			   				{{ Form::label('block_weight', 'Jumlah per Tray') }}
			   				{{ Form::number('block_weight', old('block_weight'), ['class' => 'form-control', 'id' => 'input_block_weight']) }}
			   				<br>

			   				{{ Form::label('started_at', 'Tanggal Mulai') }}
			   				{{ Form::text('started_at', old('started_at'), ['class' => 'form-control', 'id' => 'date']) }}
			   				<br>

			                <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#confirm_box" id="btn_submit"><i class="fa fa-check"></i> Save</button>
			   			</div>
			   			{!! Form::close() !!}

			   			<div class="col-md-6">
			   				<h4>Detil Bahan</h4>
			   				@if(isset($end_product))
			   				<table class="table table-bordered">
			   				<thead>
			   					<tr>
			   						<th>Doc Code</th>
			   						<td>{{$end_product->cart->doc_code}}</td>
			   						<th>Tgl</th>
			   						<td>{{$end_product->cart->date}}</td>
			   					</tr>
			   					<tr>
			   						<th>Nama</th>
			   						<th>Ukuran</th>
			   						<th>Berat / Block</th>
			   						<th>Jumlah Block</th>
			   					</tr>
			   				</thead>
			   				<tbody>
			   					@foreach($end_product->cart->detail as $detail)
			   					<tr>
			   						<td>{{$detail->name}}</td>
			   						<td>{{$detail->size}}</td>
			   						<td>{{$detail->block_weight}}</td>
			   						<td>{{$detail->quantity}}</td>
			   					</tr>
			   					@endforeach
			   				</tbody>
			   				</table>
			   				@else
			   				<table class="table table-bordered">
			   				<thead>
			   					<tr>
			   						<th>Doc Code</th>
			   						<td>{{$cart->doc_code}}</td>
			   						<th>Tgl</th>
			   						<td>{{$cart->date}}</td>
			   					</tr>
			   					<tr>
			   						<th>Nama</th>
			   						<th>Ukuran</th>
			   						<th>Berat / Block</th>
			   						<th>Jumlah Block</th>
			   					</tr>
			   				</thead>
			   				<tbody>
			   					@foreach($cart->detail as $detail)
			   					<tr>
			   						<td>{{$detail->name}}</td>
			   						<td>{{$detail->size}}</td>
			   						<td>{{$detail->block_weight}}</td>
			   						<td>{{$detail->quantity}}</td>
			   					</tr>
			   					@endforeach
			   				</tbody>
			   				</table>
			   				@endif

                          <hr>

                          <h4>Estimasi Bahan</h4>
                          <table class="table">
                          <thead>
                            <tr>
                              <th>Nama</th>
                              <th>Jumlah</th>
                              <th>Satuan</th>
                            </tr>
                          </thead>
                          <tbody id="ingredient_table">
                          </tbody>
                          </table>
			   			</div>
		    	  	</div>
				</div>
			</div>
		</div>
	</div>
</section>
@stop

@section('modal')
<div class="modal fade" id="confirm_box" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="myModalLabel">Production Summary</h4>
      		</div>
      		<div class="modal-body">
      			<table class="table">
      				<tr>
      					<th width="25%">Bahan</th>
      					<td width="75%" id="summary_material"></td>
      				</tr>
      				<tr>
      					<th>Product</th>
      					<td id="summary_product"></td>
      				</tr>
      				<tr>
      					<th>Berat setelah Defrost</th>
      					<td id="summary_defrost_weight"></td>
      				</tr>
      				<tr>
      					<th>Jumlah per Tray</th>
      					<td id="summary_qty_tray"></td>
      				</tr>
      				<tr>
      					<th>Start Date</th>
      					<td id="summary_start_date"></td>
      				</tr>
      			</table>
      		</div>
      		<div class="modal-footer">
      			<button type="button" id="yes" class="btn btn-success"><i class="fa fa-check"></i> YES</button>
      			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i>&nbsp;NO</button>
      		</div>
    	</div>
  	</div>
</div>
@stop

@section('scripts')
<script>
$(document).ready(function () {
	$('#date').datetimepicker({
		sideBySide: true,
		format: 'YYYY-MM-DD',
	});
});

$("#btn_submit").on('click', function() {
	$("#summary_material").text($("#material").text());
	$("#summary_product").text($("#production_id option:selected").text());
	$("#summary_defrost_weight").text($("#input_weight").val());
	$("#summary_qty_tray").text($("#input_block_weight").val());
	$("#summary_start_date").text($("#date").val());
});

$("#yes").on('click', function() 
{
	$("#form_create").submit();
});

$("#production_id").on('change', function()
{
	var ingredient_rows = '';
	$("#ingredient_table").empty();
    $.ajax({
        url : '{{ URL::route('get.ingredient_cost') }}',
        data : {'product_id':$("#production_id").val()},
        type : 'GET',
        headers : {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
        success : function(data, textStatus, jqXHR){
        	if(data)
			{        		
	            jQuery.each(data, function(i, val)
	            {
	            	ingredient_rows += '<tr>';
	            	ingredient_rows += '<td>';
	            	ingredient_rows += val.name;
	            	ingredient_rows += '</td>';
	            	ingredient_rows += '<td>';
	            	ingredient_rows += val.qty * document.getElementById('weight').value;
	            	ingredient_rows += '</td>';
	            	ingredient_rows += '<td>';
	            	ingredient_rows += val.uom;
	            	ingredient_rows += '</td>';
	            	ingredient_rows += '</tr>';
	            });
            } 
            else 
            {
            	ingredient_rows += '<tr>';
        		ingredient_rows += '<td colspan="3">';
	        	ingredient_rows += '<a href="{{ URL('/') }}/editor/production/'+$("#production_id").val()+'/detail" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i> Tulis resep</a>';
	        	ingredient_rows += '</td>';
	        	ingredient_rows += '</tr>';
            }            
            $("#ingredient_table").append(ingredient_rows);
        },
        error : function()
        {
        	ingredient_rows += '<tr>';
        	ingredient_rows += '<td colspan="3">';
        	ingredient_rows += 'Gagal menarik resep!';
        	ingredient_rows += '</td>';
        	ingredient_rows += '</tr>';
        	$("#ingredient_table").append(ingredient_rows);
        },
    })
});
</script>
@stop