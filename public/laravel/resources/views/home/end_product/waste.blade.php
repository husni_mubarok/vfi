@extends('layouts.home.template')
@section('content')
<div class="header_menu">
    <a href="{{ URL::route('end_product') }}">END PRODUCT</a>
</div>
<section class="content">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<div class="x_panel">
					<h2>
	                	@if(isset($end_product_waste))
	                	<i class="fa fa-pencil"></i>
	                	@else 
	                	<i class="fa fa-plus"></i>
	                	@endif
	                	<i class="fa fa-dollar"></i>&nbsp;
	                	@if($end_product->production_type == 'sashimi')
	                	Sashimi 
	                	@elseif($end_product->production_type == 'value_added') 
	                	Value Added
	                	@endif
	                	<small>Waste Product</small>
                	</h2>
					<hr>
					<div class="row bs-wizard small" style="border-bottom:0;">
		                <!-- class name =  1. complete 2.active 3.disabled-->

		                <div class="col-xs-3 bs-wizard-step active"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Step 2</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Step 1<br><i>Waste Product</i></div>
		                </div>

		                <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Step 3</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Step 2<br><i>Finish Product</i></div>
		                </div>

		                <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Step 4</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Step 3<br><i>Additional Product</i></div>
		                </div>
		                <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Finish</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Finish<br><i>Summary</i></div>
		                </div>
		            </div>

		    	  	<hr>
		    	  	<div class="row">
		    	  	@actionStart('end_product', 'create|update')
					{{-- Waste Product Form --}}
					  	<div class="col-md-6">
			        		<h4>Waste Products</h4>
			          		@include('errors.error')
			          		@if(isset($end_product_waste))
			          		{!! Form::open(['route' => ['end_product.update_waste', $end_product->id], 'method' => 'PUT']) !!}
			          		@else
			          		{!! Form::open(array('route' => ['end_product.store_waste', $end_product->id])) !!}
			          		@endif
			          		<table class="table table-bordered">
			          		<thead>
					            <tr>
						            <th width="48%">Name</th>
						            <th width="47%">Weight (kg)</th>
						            <td width="5%"></td>
			          			</tr>
				          	</thead>
				          	<tbody id="waste_body">
		          				{{-- Table Content --}}
			          		</tbody>
			          		<tfoot>
			          			<tr>
			          				<td><input type="text" id="waste_name" class="form-control" placeholder="Nama"></td>
			          				<td><input type="number" id="waste_value" class="form-control" min="0" step="0.01" placeholder="Berat"></td>
			          				<td><button type="button" id="waste_add" class="btn btn-primary"><i class="fa fa-plus"></i></button></td>
			          			</tr>
			          		</tfoot>
			        		</table>
			        		<button type="button" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
			        		{!! Form::close() !!}
				      	</div>
				      	{{-- Waste Product Form --}}
				      	@actionEnd

				      	@actionStart('end_product', 'read')
			      		{{-- Production Details --}}
			      		<div class="col-md-6">
			        		<h4>Material</h4>
			        		<table class="table table-bordered">
			          			<tr>
				            		<th>Product</th>
			            			<td>{{$end_product->production->name}}</td>
			          			</tr>
			          			<tr>
			            			<th>Weight (kg)</th>
			            			<td>{{number_format($end_product->weight, 2)}}</td>
			          			</tr>
			          			<tr>
			            			<th>Qty / Tray</th>
			            			<td>{{number_format($end_product->block_weight, 0)}}</td>
			          			</tr>
			          			<tr>
			            			<th>Production Date</th>
			            			<td>
			              				{{date("D, d M Y", strtotime($end_product->started_at))}}
			              				&nbsp;<i class="fa fa-long-arrow-right"></i>&nbsp;
			              				@if($end_product->finished_at == null)
			              				<i class="fa fa-question-circle-o"></i>
			              				@else
			              				{{date("D, d M Y", strtotime($end_product->finished_at))}} 
			              				@endif
			            			</td>
			          			</tr>
			          			<tr>
			            			<th>Criteria</th>
			            			<td>{{$end_product->criteria}}</td>
			          			</tr>
	        				</table>
			      		</div>
			      		{{-- Production Details --}}
			      		@actionEnd
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@stop

@section('scripts')
<script>
var waste = JSON.parse("{}");

$("#waste_add").on('click', function() {
	var name = ($("#waste_name").val()).toLowerCase().trim();
	var value = $("#waste_value").val();
	var key = name.replace(/ /g,"_");
	if(name && value)
	{
		if(waste[key] == undefined)
		{
			waste[key] = parseFloat(value);
			var new_row = '';
			new_row += '<tr>';
			new_row += '<td>';
			new_row += '<input type="hidden" name="waste['+key+'][name]" value="'+name+'">';
			new_row += name;
			new_row += '</td>';
			new_row += '<td id="waste_text_'+key+'">';
			new_row += waste[key];
			new_row += '</td>';
			new_row += '<td>';
			new_row += '<input type="hidden" name="waste['+key+'][value]" value="'+waste[key]+'" id="waste_'+key+'">';
			new_row += '<button type="button" id="waste_remove_'+key+'" class="btn btn-danger"><i class="fa fa-remove"></i></button>';
			new_row += '</td>';
			new_row += '</tr>';
			$("tbody#waste_body").append(new_row);

			$("#waste_remove_"+key).on('click', function() {
				waste[key] = null;
				$(this).parent().parent().remove();
			});
		} else {
			waste[key] = waste[key] + parseFloat(value);
			$("#waste_"+key).val(waste[key]);
			$("#waste_text_"+key).text(waste[key]);
		}

		$("#waste_name").val('');
		$("#waste_value").val('');
	}
});
</script>

@if(isset($end_product_waste))
<script>
$(document).ready(function(){
	jQuery.each({!! $end_product_waste !!}, function( i, val ) {
		var name = val['name'].toLowerCase().trim();
		var value = val['value'];
		var key = name.replace(/ /g,"_");
		waste[key] = parseFloat(value);
		var new_row = '';
		new_row += '<tr>';
		new_row += '<td>';
		new_row += '<input type="hidden" name="waste['+key+'][name]" value="'+name+'">';
		new_row += name;
		new_row += '</td>';
		new_row += '<td id="waste_text_'+key+'">';
		new_row += waste[key];
		new_row += '</td>';
		new_row += '<td>';
		new_row += '<input type="hidden" name="waste['+key+'][value]" value="'+waste[key]+'" id="waste_'+key+'">';
		new_row += '<button type="button" id="waste_remove_'+key+'" class="btn btn-danger"><i class="fa fa-remove"></i></button>';
		new_row += '</td>';
		new_row += '</tr>';
		$("tbody#waste_body").append(new_row);

		$("#waste_remove_"+key).on('click', function() {
			waste[key] = null;
			$(this).parent().parent().remove();
		});
	});
});
</script>
@endif
@stop
