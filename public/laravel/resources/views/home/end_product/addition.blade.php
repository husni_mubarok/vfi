@extends('layouts.home.template')
@section('content')
<div class="header_menu">
    <a href="{{ URL::route('end_product') }}">END PRODUCT</a>
</div>
<section class="content">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<div class="x_panel">
					<h2>
						@if(isset($end_product_addition))
	                	<i class="fa fa-pencil"></i> 
	                	@else
	                	<i class="fa fa-plus"></i> 
	                	@endif
	                	<i class="fa fa-plus-square-o"></i>
	                	@if($end_product->production_type == 'sashimi')
	                	Sashimi 
	                	@elseif($end_product->production_type == 'value_added') 
	                	Value Added
	                	@endif
	                	<small>Additional Product</small>
                	</h2>
					<hr>
					<div class="row bs-wizard small" style="border-bottom:0;">
		                <!-- class name =  1. complete 2.active 3.disabled-->

		                <div class="col-xs-3 bs-wizard-step complete"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Step 2</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Step 1<br><i>Waste Product</i></div>
		                </div>

		                <div class="col-xs-3 bs-wizard-step complete"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Step 3</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Step 2<br><i>Finish Product</i></div>
		                </div>

		                <div class="col-xs-3 bs-wizard-step active"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Step 4</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Step 3<br><i>Additional Product</i></div>
		                </div>
		                <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Finish</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Finish<br><i>Summary</i></div>
		                </div>
		            </div>
		    	  	<hr>
		    	  	<div class="row">
		    	  		@actionStart('end_product', 'create|update')
	    	  			{{-- Reject Products --}}
		            	<div class="col-md-6">
			                @include('errors.error')
			                @if(isset($end_product_addition))
			                {!! Form::model($end_product_addition, ['route' => ['end_product.update_addition', $end_product->id], 'method' => 'PUT', 'id' => 'form_additional']) !!}
			                @else
		                    {!! Form::open(array('route' => ['end_product.store_addition', $end_product->id], 'id' => 'form_additional'))!!}
		                    @endif
		                    {{ csrf_field() }}

	                    	{{ Form::label('Reject type') }}
	                    	{{ Form::text('', null, ['class' => 'form-control', 'id' => 'reject_type']) }}
	                    	{{-- {{ Form::select('', $reject_type_list, null, ['class' => 'form-control', 'id' => 'reject_type']) }} --}}
	                    	<br>

	                    	{{ Form::label('Reason') }}
	                    	{{ Form::text('', null, ['class' => 'form-control', 'id' => 'reject_reason']) }}
	                    	{{-- {{ Form::select('', $reject_reason_list, null, ['class' => 'form-control', 'id' => 'reject_reason']) }} --}}
		                    <br>

		                    <input type="hidden" name="block_weight" value="{{ $end_product->block_weight }}">
		                    {{-- {{ Form::label('Block Weight') }} --}}
		                    {{-- {{ Form::number('', null, ['class' => 'form-control', 'id' => 'block_weight', 'step' => '0.01', 'min' => '0']) }} --}}
		                    {{-- <br> --}}

                    		{{ Form::label('size', 'Size') }}
                    		<div class="row" >
                    			<div class="col-md-12">
                    				<table class="table">
                					<tr id="input_single">
                						<td colspan="3"><input type="number" id="weight" class="form-control"></td>
                						<td width="5%">
                							<button type="button" class="btn btn-primary btn_add"><i class="fa fa-plus"></i></button>
            							</td>
                					</tr>
                    				</table>
								</div>
							</div>
                    		<br>

		                    <table class="table table-bordered" width="100%">
		                    <thead>
		                    	<th width="25%">Type</th>
		                    	<th width="15%">Weight</th>
		                    	<th width="10%">Pcs</th>
		                    	<th width="30%">Reason</th>
		                    	<td width="20%"></td>
		                    </thead>
	                    	<tbody id="cart">
	                    	</tbody>
		                    </table>
		                    <button type="button" id="btn_submit" data-toggle="modal" data-target="#confirmation_modal" class="btn btn-success pull-right"><i class="fa fa-check"></i> Save</button>
                    		{!! Form::close() !!}
                		</div>
                		{{-- Reject Products --}}
                		@actionEnd

                		@actionStart('end_product', 'read')
                		{{-- Production Details --}}
                		<div class="col-md-6">
			        		<h4>Material</h4>
			        		<table class="table table-bordered">
			          			<tr>
				            		<th>Product</th>
			            			<td>{{$end_product->production->name}}</td>
			          			</tr>
			          			<tr>
			            			<th>Weight</th>
			            			<td>{{number_format($end_product->weight, 2)}} kg</td>
			          			</tr>
			          			<tr>
			            			<th>Qty / Tray</th>
			            			<td>{{number_format($end_product->block_weight, 0)}}</td>
			          			</tr>
			          			<tr>
			            			<th>Production Date</th>
			            			<td>
			              				{{date("D, d M Y", strtotime($end_product->started_at))}}
			              				&nbsp;<i class="fa fa-long-arrow-right"></i>&nbsp;
			              				@if($end_product->finished_at == null)
			              				<i class="fa fa-question-circle-o"></i>
			              				@else
			              				{{date("D, d M Y", strtotime($end_product->finished_at))}} 
			              				@endif
			            			</td>
			          			</tr>
			          			<tr>
			            			<th>Criteria</th>
			            			<td>{{$end_product->criteria}}</td>
			          			</tr>
	        				</table>
			      		</div>
                		{{-- Production Details --}}
                		@actionEnd
			        </div>
	    	  	</div>
    	  	</div>
	  	</div>
  	</div>
</section>
@stop

@section('modal')
<div class="modal fade" id="confirmation_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="myModalLabel">Additional Summary</h4>
      		</div>
      		<div class="modal-body">
      			{{-- <table class="table">
      				<thead>
      					<th width="20%">Type</th>
      					<th width="20%">Size</th>
      					<th width="20%">Block</th>
      					<th width="20%">Pcs</th>
      					<th width="20%">Reason</th>
      				</thead>
      				<tbody id="additional_list">
      				</tbody>
      			</table> --}}
      			Konfirmasi additional?
      		</div>
      		<div class="modal-footer">
      			<button type="button" id="btn_confirm" class="btn btn-success"><i class="fa fa-check"></i> YES</button>
      			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> NO</button>
      		</div>
    	</div>
  	</div>
</div>
@stop

@section('scripts')
<script>
var cart = JSON.parse("{}");

// ADD BUTTON EVENT
$(".btn_add").on('click', function() {
	var reject_type = $("#reject_type").val().trim();
	var size_val = $("#weight").val();
	var reject_reason = $("#reject_reason").val().trim();
	var key = reject_type.replace(/ /g,"xyz")+"_"+reject_reason.replace(/ /g,"xyz")+"_"+size_val;
	if(size_val)
	{
		if(cart[key] == undefined)
		{
			cart[key] = 1;
			var new_row = '';
			new_row += '<tr>';
			new_row += '<td>';
			new_row += '<input type="hidden" name="type['+key+']" value="'+reject_type+'">';
			new_row += $("#reject_type").val();
			new_row += '</td>';
			new_row += '<td>';
			new_row += '<input type="hidden" name="size['+key+']" value="'+size_val+'">';
			new_row += size_val;
			new_row += '</td>';
			new_row += '<td>';
			new_row += '<input type="hidden" name="block['+key+']" value="'+cart[key]+'" id="block_'+key+'">';
			new_row += '<div id="text_'+key+'">'+cart[key]+'</div>';
			new_row += '</td>';
			new_row += '<td>';
			new_row += '<input type="hidden" name="reason['+key+']" value="'+reject_reason+'">';
			new_row += $("#reject_reason").val();
			new_row += '</td>';
			new_row += '<td>';
			new_row += '<button type="button" class="btn btn-primary btn-sm" id="btn_add_'+key+'">';
			new_row += '<i class="fa fa-plus"></i>';
			new_row += '</button>';
			new_row += '<button type="button" class="btn btn-warning btn-sm" id="btn_reduce_'+key+'">';
			new_row += '<i class="fa fa-minus"></i>';
			new_row += '</button>';
			new_row += '<button type="button" class="btn btn-danger btn-sm" id="btn_remove_'+key+'">';
			new_row += '<i class="fa fa-remove"></i>';
			new_row += '</button>';
			new_row += '</td>';
			new_row += '</tr>';

			$("#cart").append(new_row);
			$("#weight").val('');
			$("#reject_reason").val('');
			$("#reject_type").val('');
			
			$("#btn_add_"+key).on('click', function() 
			{
				cart[key] += 1;
				$("#block_"+key).val(cart[key]);
				$("#text_"+key).text(cart[key]);
			});

			$("#btn_reduce_"+key).on('click', function() {
				if(cart[key] == 1)
				{
					cart[key] = null;
					$(this).parent().parent().remove();
				}
				else 
				{
					cart[key] -= 1;
					$("#block_"+key).val(cart[key]);
					$("#text_"+key).text(cart[key]);
				}
			});

			$("#btn_remove_"+key).on('click', function() 
			{
				cart[key] = null;
				$(this).parent().parent().remove();
			});
		} 
		else 
		{
			cart[key] += 1;
			$("#block_"+key).val(cart[key]);
			$("#text_"+key).text(cart[key]);
		}
	}
});

$("#btn_submit").on('click', function() 
{
	$("#additional_list").empty();
	var new_row = '';
	jQuery.each(cart, function(i, val)
	{
		new_row += '<tr>';
		new_row += '<td>';
		new_row += i;
		new_row += '</td>';
		new_row += '<td>';
		new_row += val;
		new_row += '</td>';
		new_row += '<td>';
		new_row += val;
		new_row += '</td>';
		new_row += '<td>';
		new_row += val;
		new_row += '</td>';
		new_row += '<td>';
		new_row += val;
		new_row += '</td>';
		new_row += '</tr>';
	});
	// $("#additional_list").append(new_row);
});

$("#btn_confirm").on('click', function() {
	$("#form_additional").submit();
});
</script>

@if(isset($end_product_addition))
<script>
jQuery.each({!! $end_product_addition !!}, function( i, val ) {
	var type = val['reject_type_id'].trim();
	var size = val['size'];
	var block = val['block'];
	var reason = val['reject_reason_id'].trim();
	var key = type.replace(/ /g,"xyz")+"_"+reason.replace(/ /g,"xyz")+"_"+size;
	cart[key] = block;
   	var new_row = '';
	new_row += '<tr>';
	new_row += '<td>';
	new_row += '<input type="hidden" name="type['+key+']" value="'+type+'">';
	new_row += $("#reject_type").val();
	new_row += '</td>';
	new_row += '<td>';
	new_row += '<input type="hidden" name="size['+key+']" value="'+size+'">';
	new_row += size;
	new_row += '</td>';
	new_row += '<td>';
	new_row += '<input type="hidden" name="block['+key+']" value="'+cart[key]+'" id="block_'+key+'">';
	new_row += '<div id="text_'+key+'">'+cart[key]+'</div>';
	new_row += '</td>';
	new_row += '<td>';
	new_row += '<input type="hidden" name="reason['+key+']" value="'+reason+'">';
	new_row += $("#reject_reason").val();
	new_row += '</td>';
	new_row += '<td>';
	new_row += '<button type="button" class="btn btn-primary btn-sm" id="btn_add_'+key+'">';
	new_row += '<i class="fa fa-plus"></i>';
	new_row += '</button>';
	new_row += '<button type="button" class="btn btn-warning btn-sm" id="btn_reduce_'+key+'">';
	new_row += '<i class="fa fa-minus"></i>';
	new_row += '</button>';
	new_row += '<button type="button" class="btn btn-danger btn-sm" id="btn_remove_'+key+'">';
	new_row += '<i class="fa fa-remove"></i>';
	new_row += '</button>';
	new_row += '</td>';
	new_row += '</tr>';

	$("#cart").append(new_row);

	$("#btn_add_"+key).on('click', function() 
	{
		cart[key] += 1;
		$("#block_"+key).val(cart[key]);
		$("#text_"+key).text(cart[key]);
	});

	$("#btn_reduce_"+key).on('click', function() {
		if(cart[key] == 1)
		{
			cart[key] = null;
			$(this).parent().parent().remove();
		}
		else 
		{
			cart[key] -= 1;
			$("#block_"+key).val(cart[key]);
			$("#text_"+key).text(cart[key]);
		}
	});

	$("#btn_remove_"+key).on('click', function() 
	{
		cart[key] = null;
		$(this).parent().parent().remove();
	});
});
</script>
@endif
@stop