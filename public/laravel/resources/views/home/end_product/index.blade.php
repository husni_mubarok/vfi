@extends('layouts.home.template')
@section('content')
<div class="header_menu">
    <a href="{{ URL::route('end_product') }}">END PRODUCT</a>
</div>
<section class="content">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<div class="x_panel">
					<h2>
						<i class="fa fa-gift"></i>&nbsp;End Product
					</h2>
		            <hr>

		            <ul class="nav nav-tabs">
		            	<li class="active"><a data-toggle="tab" href="#ongoing"> On Going </a></li>
		            	<li><a data-toggle="tab" href="#pending"> Pending </a></li>
					</ul>

					<div class="tab-content">
						<div id="ongoing" class="tab-pane fade in active">
				            <table class="table table-bordered" id="end_product_table">
			            	<thead>
			            		<tr>
			            			<th width="3%">#</th>
				            		<th width="17%">Product</th>
				            		<th width="5%">PO Code</th>
				            		<th width="8%">Weight (kg)</th>
				            		<th width="17%">Date</th>
				            		<th>Criteria</th>
				            		<th>Waste</th>
				            		<th>Result</th>
				            		<th>Additional</th>
			            		</tr>
			            	</thead>
			            	<tbody>
			            		@foreach($end_products as $end_product_key => $end_product)
			            		<tr>
									<td>{{$end_product_key+1}}</td>
									<td>
										<a href="{{ URL::route('production.summary', [$end_product->id]) }}">
											{{$end_product->production->name}}
										</a>
									</td>
									<td>{{$end_product->id}}</td>
									<td>{{number_format($end_product->weight, 2)}}</td>
									<td>
										{{date("D, d M Y", strtotime($end_product->started_at))}} 
										<i class="fa fa-arrow-right"></i>
										{{date("D, d M Y", strtotime($end_product->finished_at))}}
									</td>
									<td>@if($end_product->criteria) {{$end_product->criteria}} @else - @endif</td>
									<td>
										@if($end_product->status > 0)
											@if($end_product->production_result_variable->count() == 0)
											- 
											@else
											<table class="table">
												@foreach($end_product->production_result_variable as $waste)
												<tr>
													<td width="50%">{{$waste->name}}</td>
													<td width="50%">{{number_format($waste->value, 2)}} kg</td>
												</tr>
												@endforeach
											</table>
											@endif
										@else
											<a href="{{ URL::route('production.create_waste', [$end_product->id]) }}" class="btn btn-primary btn-md"> <i class="fa fa-plus"></i> Waste Product </a>
										@endif
									</td>
									<td>
										@if($end_product->status > 1)
											@if($end_product->production_result_detail->count() == 0)
											-
											@else
											<table class="table">
												@foreach($end_product->production_result_detail as $ready)
												<tr>
													<td width="25%">{{$ready->size}}</td>
													<td width="25%">{{number_format(($ready->block * $end_product->block_weight), 2)}} kg</td>
													<td width="25%">{{$ready->block}} block</td>
													<td width="25%">{{floor($ready->block / $end_product->mst_carton)}} MC</td>
												</tr>
												@endforeach
											</table>
											@endif
										@else
											<a href="{{ URL::route('production.create_result', [$end_product->id]) }}" class="btn btn-primary btn-md"> <i class="fa fa-plus"></i> Ready Product </a>
										@endif
									</td>
									<td>
										@if($end_product->status > 2)
											@if($end_product->production_result_addition->count() == 0)
											-
											@else
											<table class="table">
												@foreach($end_product->production_result_addition as $addition)
												<tr>
													<td width="25%">{{$addition->size}}</td>
													<td width="25%">{{number_format(($addition->block * $addition->block_weight), 2)}} kg</td>
													<td width="25%">{{$addition->block}} block</td>
													<td width="25%">{{floor($addition->block / $end_product->mst_carton)}} MC</td>
												</tr>
												@endforeach
											</table>
											@endif
										@else
											<a href="{{ URL::route('production.create_reject', [$end_product->id]) }}" class="btn btn-primary btn-md"> <i class="fa fa-plus"></i> Addition Product </a>
										@endif
									</td>
								</tr>
			            		@endforeach
			            	</tbody>
				            </table>
			            </div>

			            <div id="pending" class="tab-pane fade in">
			            	<table class="table table-bordered" id="cart_table">
			            	<thead>
			            		<tr>
			            			<th width="3%">#</th>
			            			<th width="12%">Code</th>
			            			<th width="85%">Detail</th>
			            		</tr>
			            	</thead>
			            	<tbody>
			            		@foreach($carts as $cart_key => $cart)
			            		@if($cart->production_result == null)
			            		<tr>
			            			<td>{{$cart_key+1}}</td>
			            			<td>
			            				<a href="{{ URL::route('end_product.create', [$cart->id]) }}">
			            				{{$cart->doc_code}}
			            				</a>
		            				</td>
			            			<td>
			            				<table class="table table-bordered">
			            					@foreach($cart->detail as $detail)
			            					<tr>
				                              	<td width="15%" align="right">{{date("D, d M Y", strtotime($detail->created_at))}}</td>
				                              	<td width="55%">{{$detail->name}}</td>
			                              	  	<td width="10%">{{$detail->size}}</td>
			                              	  	<td width="20%">{{$detail->block}} blok</td>
				                          	</tr>
				                          	@endforeach
			            				</table>
			            			</td>
			            		</tr>
			            		@endif
			            		@endforeach
			            	</tbody>
			            	</table>
			            </div>
		            </div>
				</div>
			</div>
		</div>
	</div>
</section>
@stop

@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
$(document).ready(function () {
    $("#end_product_table").DataTable(
    	{
    		//
    	});

    $("#cart_table").DataTable(
    	{
    		//
    	});
});
</script>
@stop