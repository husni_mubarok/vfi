@extends('layouts.home.template')
@section('content')
<div class="header_menu">
    <a href="{{ URL::route('end_product') }}">END PRODUCT</a>
</div>
<section class="content">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<div class="x_panel">
					<h2>
						@if(isset($end_product_result))
	                	<i class="fa fa-pencil"></i> 
	                	@else
	                	<i class="fa fa-plus"></i> 
	                	@endif
	                	<i class="fa fa-dropbox"></i>
	                	@if($end_product->production_type == 'sashimi')
	                	Sashimi 
	                	@elseif($end_product->production_type == 'value_added') 
	                	Value Added
	                	@endif
	                	<small>Finish Product</small>
                	</h2>
					<hr>
					<div class="row bs-wizard small" style="border-bottom:0;">
		                <!-- class name =  1. complete 2.active 3.disabled-->

		                <div class="col-xs-3 bs-wizard-step complete"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Step 2</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Step 1<br><i>Waste Product</i></div>
		                </div>

		                <div class="col-xs-3 bs-wizard-step active"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Step 3</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Step 2<br><i>Finish Product</i></div>
		                </div>

		                <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Step 4</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Step 3<br><i>Additional Product</i></div>
		                </div>
		                <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
		                  {{-- <div class="text-center bs-wizard-stepnum">Finish</div> --}}
		                  <div class="progress sm"><div class="progress-bar"></div></div>
		                  <a href="#" class="bs-wizard-dot"></a>
		                  <div class="bs-wizard-info text-center">Finish<br><i>Summary</i></div>
		                </div>
		            </div>
		    	  	<hr>
		    	  	<div class="row">
		    	  	@actionStart('end_product', 'create|update')
		    	  	@include('errors.error')
		    	  	@if(isset($end_product_result))
	                {!! Form::model($end_product, ['route' => ['end_product.update_result', $end_product->id], 'method' => 'PUT']) !!}
		    	  	@else
		    	  	{!! Form::open(['route' => ['end_product.store_result', $end_product->id]]) !!}
	                @endif
                    {{ csrf_field() }}
                    <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                        {{ Form::label('finished_at', 'Finish Date') }}
                        {{ Form::text('finished_at', old('finished_at'), array('class' => 'form-control', 'placeholder' => 'Date & Time*', 'required' => 'true', 'id' => 'finished_at')) }}
                        <br>

                        {{-- <div class="col-md-4">
	                        {{ Form::label('uom', 'Unit of Measurement') }}
	                        {{ Form::select('uom_id', $uom_list, old('uom_id'), array('class' => 'form-control')) }}
                        </div> --}}
                        <input type="hidden" name="uom_id" value="2">
                        <br/>

                        <table class="table table-bordered">
                        <thead>
                        	<tr>
                        		<th>Weight (gr)</th>
                        		<th>Pcs</th>
                        		<th width="25%"></th>
                        	</tr>
                        </thead>
                        <tbody id="cart">
                        	<tr>
                        	</tr>
                        </tbody>
                        <tfoot>
                        	<tr>
                    			<td colspan="2"><input type="number" class="form-control" id="size" min="0" step="1"></td>
                        		<td>
                        			<button type="button" class="btn btn-primary btn-md" id="btn_add"><i class="fa fa-plus"></i></button>
                    			</td>
                        	</tr>
                        </tfoot>
                        </table>
                        <button type="submit" class="btn btn-success pull-right" onclick="this.disabled=true; this.form.submit();"><i class="fa fa-check"></i> Save</button>
                    	{!! Form::close() !!}
		        	</div>
		        	@actionEnd

		        	@actionStart('end_product', 'read')
		        	<div class="col-md-6 col-sm-6 col-xs-6">
		        		<h4>Production Detail</h4>
				        <table class="table table-bordered">
				          <tr>
				            <th>Product</th>
				            <td>{{$end_product->production->name}}</td>
				          </tr>
				          <tr>
				            <th>Weight (kg)</th>
				            <td>{{number_format($end_product->weight, 2)}}</td>
				          </tr>
				          <tr>
				            <th>Qty / Tray</th>
				            <td>{{number_format($end_product->block_weight, 0)}}</td>
				          </tr>
				          <tr>
				            <th>Production Date</th>
				            <td>
				              {{date("D, d M Y", strtotime($end_product->started_at))}}
				              &nbsp;<i class="fa fa-long-arrow-right"></i>&nbsp;
				              @if($end_product->finished_at == null)
				              <i class="fa fa-question-circle-o"></i>
				              @else
				              {{date("D, d M Y", strtotime($end_product->finished_at))}} 
				              @endif
				            </td>
				          </tr>
				          <tr>
				            <th>Criteria</th>
				            <td>{{$end_product->criteria}}</td>
				          </tr>
				        </table>
		        	</div>
		        	@actionEnd
	    	  	</div>
    	  	</div>
	  	</div>
  	</div>
</section>
@stop

@section('scripts')
<script>
var cart = JSON.parse("{}");
$(document).ready(function () {
	$('#finished_at').datetimepicker({
		format: 'YYYY-MM-DD',
	});

	var size = [];
});
  
$("#btn_add").click(function() {
    var outputString = '';
    var size = $("#size").val();
    if(size)
    {
	    if(cart[size]==undefined){
	    	cart[size]=1;
	    	outputString += '<tr>';
		    outputString += '<td>';
		    outputString += size;
		    outputString += '<input type="hidden" id="size['+size+']" name="size['+size+']" value="'+size+'">';
		    outputString += '</td>';
		    outputString += '<td><div id="block_'+size+'">'+cart[size]+'</div><input type="hidden" id="block_value_'+size+'" name="block['+size+']" value="'+cart[size]+'" class="form-control"></td>';
		    outputString += '<td>';
		    outputString += '<button type="button" class="btn btn-primary btn-md" id="btn_add_'+size+'"><i class="fa fa-plus"></i></button>';
		    outputString += '<button type="button" class="btn btn-warning btn-md" id="btn_reduce_'+size+'"><i class="fa fa-minus"></i></button>';
		    outputString += '<button type="button" class="btn btn-danger btn-md" id="btn_remove_'+size+'"><i class="fa fa-remove"></i></button>';
		    outputString += '</td>';
		    outputString += '</tr>';

		    $("#cart").append(outputString);
		    $("#btn_reduce_"+size).on('click', function(){
		    	if(cart[size] > 1)
		        { 
			        cart[size] = cart[size]- 1;
					$("#block_"+size).html(cart[size]);
					$("#block_value_"+size).val(cart[size]);
				} else {
					cart[size]= null;
	        		$(this).parent().parent().remove();
				}
		    });

		    $("#btn_add_"+size).on('click', function(){
		        cart[size] =  cart[size]+1;
		    	$("#block_value_"+size).val(cart[size]);
				$("#block_"+size).html(cart[size]);
		    });

		    $("#btn_remove_"+size).on('click',function(){	   
				cart[size]= null;
	        	$(this).parent().parent().remove();
		    });

	    }else{
			cart[size]=cart[size]+1;	
			$("#block_value_"+size).val(cart[size]);
			$("#block_"+size).html(cart[size]);
	    }

	    $("#btn_submit").attr("disabled", false);
	    $("#size").val('');
    }
});
</script>

@if(isset($end_product_result))
<script>
jQuery.each({!! $end_product_result !!}, function( i, val ) {
	var size = val['size'];
	var block = val['block'];
	cart[size] = block;
    var outputString = '';

   	outputString += '<tr>';
    outputString += '<td>'+size;
    outputString += '<input type="hidden" id="size['+size+']" name="size['+size+']" value="'+size+'">';
    outputString += '</td>';
    outputString += '<td><div id="block_'+size+'">'+cart[size]+'</div><input type="hidden" id="block_value_'+size+'" name="block['+size+']" value="'+cart[size]+'" class="form-control"></td>';
    outputString += '<td>';
    outputString += '<button type="button" class="btn btn-primary btn-md" id="btn_add_'+size+'"><i class="fa fa-plus"></i></button>';
    outputString += '<button type="button" class="btn btn-warning btn-md" id="btn_reduce_'+size+'"><i class="fa fa-minus"></i></button>';
    outputString += '<button type="button" class="btn btn-danger btn-md" id="btn_remove_'+size+'"><i class="fa fa-remove"></i></button>';
    outputString += '</td>';
    outputString += '</tr>';

    $("#cart").append(outputString);
    $("#btn_reduce_"+size).on('click', function(){
    	if(cart[size] > 1)
        { 
	        cart[size] = cart[size]- 1;
			$("#block_"+size).html(cart[size]);
		} else {
			cart[size]= null;
    		$(this).parent().parent().remove();
		}
    });

    $("#btn_add_"+size).on('click', function(){
        cart[size] =  cart[size]+1;
    	$("#block_value_"+size).val(cart[size]);
		$("#block_"+size).html(cart[size]);
    });

    $("#btn_remove_"+size).on('click',function(){	   
		cart[size]= null;
    	$(this).parent().parent().remove();
    });
});
</script>
@endif
@stop