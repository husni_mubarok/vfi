@extends('layouts.home.template')
@section('content')
<div class="header_menu">
    <a href="{{ URL::route('end_product') }}">END PRODUCT</a>
</div>
<section class="content">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<div class="x_panel">
					<h2>
          	<i class="fa fa-file-text-o"></i> Summary
        	</h2>
					<hr>
					<div class="row bs-wizard small" style="border-bottom:0;">
            <!-- class name =  1. complete 2.active 3.disabled-->

            <div class="col-xs-3 bs-wizard-step complete"><!-- active -->
              {{-- <div class="text-center bs-wizard-stepnum">Step 2</div> --}}
              <div class="progress sm"><div class="progress-bar"></div></div>
              <a href="#" class="bs-wizard-dot"></a>
              <div class="bs-wizard-info text-center">Step 1<br><i>Waste Product</i></div>
            </div>

            <div class="col-xs-3 bs-wizard-step complete"><!-- active -->
              {{-- <div class="text-center bs-wizard-stepnum">Step 3</div> --}}
              <div class="progress sm"><div class="progress-bar"></div></div>
              <a href="#" class="bs-wizard-dot"></a>
              <div class="bs-wizard-info text-center">Step 2<br><i>Finish Product</i></div>
            </div>

            <div class="col-xs-3 bs-wizard-step complete"><!-- active -->
              {{-- <div class="text-center bs-wizard-stepnum">Step 4</div> --}}
              <div class="progress sm"><div class="progress-bar"></div></div>
              <a href="#" class="bs-wizard-dot"></a>
              <div class="bs-wizard-info text-center">Step 3<br><i>Additional Product</i></div>
            </div>

            <div class="col-xs-3 bs-wizard-step active"><!-- active -->
              {{-- <div class="text-center bs-wizard-stepnum">Finish</div> --}}
              <div class="progress sm"><div class="progress-bar"></div></div>
              <a href="#" class="bs-wizard-dot"></a>
              <div class="bs-wizard-info text-center">Finish<br><i>Summary</i></div>
            </div>
          </div>
    	  	<hr>
          @actionStart('end_product', 'read')
    	  	<div class="col-md-2"></div>          
				  <div class="col-md-8">
  				  @php
            $total_mtrl_weight = 0;
            $total_mtrl_qty = 0;
            $total_waste = 0;
            $total_addition = 0;
            $total_ready = 0;
            $total_miss = 0;
            foreach($end_product->cart->detail as $detail)
            {
              $total_mtrl_weight += ($detail->block * $detail->block_weight);
              $total_mtrl_qty += $detail->block;
            }
            foreach($end_product->production_result_variable as $waste)
            {
              $total_waste += $waste->value;
            }
            foreach($end_product->production_result_addition as $addition)
            {
              $total_addition += $addition->block;
            }
            foreach($end_product->production_result_detail as $detail)
            {
              $total_ready += floor($detail->block / $end_product->block_weight);
            }
            $total_miss = $end_product->weight - ($total_waste + $total_addition + $total_ready);
            @endphp
            <table class="table">
              <tr>
                <td colspan="3">
                  <b>{{$end_product->production->fish->name}}</b> 
                  <small>{{$end_product->status}}</small>
                </td>
                <td align="right">
                  <b>{{number_format($end_product->weight, 2)}} kg</b>
                </td>
                <td width="5%">
                  @if($end_product->status < 4)
                  <a href="{{ URL::route('end_product.edit', [$end_product->id]) }}" class="btn btn-sm btn-success"><i class="fa fa-pencil"></i></a>
                  @endif
                </td>
              </tr>
              <tr>
                <td colspan="5"><span>{{$end_product->production->name}}</span></td>
              </tr>
              <tr>
                <td colspan="5">Qty / Tray = {{number_format($end_product->block_weight, 0)}}</td>
              </tr>
              <tr>
                <td colspan="5">
                  {{date("D, d M Y", strtotime($end_product->started_at))}} 
                  <i class="fa fa-caret-right"></i> 
                  {{date("D, d M Y", strtotime($end_product->finished_at))}}
                </td>
              </tr>

              {{-- Materials --}}
              <tr>
                <td colspan="2"><b>Material</b></td>
                <td></td>
                <td align="right"><b>{{number_format($total_mtrl_qty, 0)}} block</b></td>
                <td>
                  <button type="button" class="btn btn-default btn-sm" data-toggle="collapse" data-target=".material_rows">
                  <i class="fa fa-chevron-down"></i></button>
                </td>
              </tr>
              <tr class="collapse material_rows">
                <th width="10%">Tgl. Masuk</th>
                <th width="15%">Nama</th>
                <th width="40%">Size</th>
                <th align="right">Block</th>
                <th></th>
              </tr>
              @foreach($end_product->cart->detail as $detail)
              <tr class="collapse material_rows">
                <td>{{date("d/m/Y", strtotime($detail->created_at))}}</td>
                <td>{{$detail->name}}</td>
                <td>{{$detail->size}}</td>
                <td align="right">{{$detail->block}}</td>
                <td></td>
              </tr>
              @endforeach
              {{-- Materials --}}

              @actionStart('recipe', 'read')
              @if($end_product->production->recipe)
              {{-- Ingredient Cost --}}
              <tr>
                <td colspan="2"><b>Ingredient</b></td>
                <td></td>
                <td align="right"></td>
                <td>
                  <button type="button" class="btn btn-default btn-sm" data-toggle="collapse" data-target=".ingredient_rows">
                  <i class="fa fa-chevron-down"></i></button>
                </td>
              </tr>
              <tr class="collapse ingredient_rows">
                <th width="10%">Nama</th>
                <th width="15%">Qty</th>
                <th width="40%">UoM</th>
                <th></th>
              </tr>
              @foreach($end_product->production->recipe->recipe as $ingredient)
              <tr class="collapse ingredient_rows">
                <td>{{$ingredient->item->name}}</td>
                <td>{{number_format((ceil($ingredient->qty / $ingredient->recipe->qty)) * $total_mtrl_weight, 2)}}</td>
                <td>gr</td>
              </tr>
              @endforeach
              {{-- Ingredient Cost --}}
              @endif
              @actionEnd

              {{-- Waste Products --}}
              <tr>
                <td colspan="2">
                  <b>Waste Product</b>
                  @if($end_product->status < 4)
                  <a href="{{ URL::route('end_product.edit_waste', $end_product->id) }}" class="btn btn-success btn-sm"> <i class="fa fa-pencil"></i> </a>
                  @endif
                </td>
                <td></td>
                <td align="right"><b>{{number_format($total_waste, 2)}} kg</b></td>
                <td>
                  <button type="button" class="btn btn-default btn-sm" data-toggle="collapse" data-target=".waste_rows">
                  <i class="fa fa-chevron-down"></i></button>
                </td>
              </tr>
              <tr class="collapse waste_rows">
                <th></th>
                <th>Part</th>
                <th></th>
                <th align="right">Weight</th>
                <th></th>
              </tr>
              @foreach($end_product->production_result_variable as $waste)
              <tr class="collapse waste_rows">
                <td></td>
                <td>{{$waste->name}}</td>
                <td></td>
                <td align="right">{{number_format($waste->value, 2)}} kg</td>
                <td></td>
              </tr>
              @endforeach
              {{-- Waste Products --}}

              {{-- Ready Products --}}
              <tr>
                <td colspan="2">
                  <b>Ready Product</b>
                  @if($end_product->status < 4)
                  <a href="{{ URL::route('end_product.edit_result', $end_product->id) }}" class="btn btn-success btn-sm"> <i class="fa fa-pencil"></i> </a>
                  @endif
                </td>
                <td></td>
                <td align="right"><b>{{$total_ready}} tray</b></td>
                <td>
                  <button type="button" class="btn btn-default btn-sm" data-toggle="collapse" data-target=".ready_rows">
                  <i class="fa fa-chevron-down"></i></button>
                </td>
              </tr>
              <tr class="collapse ready_rows">
                <th></th>
                <th>Size</th>
                <th>Pcs</th>
                <th>Tray</th>
                <th></th>
              </tr>
              @foreach($end_product->production_result_detail as $detail)
              <tr class="collapse ready_rows">
                <td></td>
                <td>{{$detail->size}}</td>
                <td>{{$detail->block}}</td>
                <td>{{floor($detail->block / $end_product->block_weight)}}</td>
                <td></td>
              </tr>
              @endforeach
              {{-- Ready Products --}}

              {{-- Additional Products --}}
              <tr>
                <td colspan="2">
                  <b>Additional Product</b>
                  @if($end_product->status < 4)
                  <a href="{{ URL::route('end_product.edit_addition', $end_product->id) }}" class="btn btn-success btn-sm"> <i class="fa fa-pencil"></i> </a>
                  @endif
                </td>
                <td></td>
                <td align="right"><b>{{$total_addition}} pcs</b></td>
                <td>
                  <button type="button" class="btn btn-default btn-sm" data-toggle="collapse" data-target=".additional_rows">
                  <i class="fa fa-chevron-down"></i></button>
                </td>
              </tr>
              <tr class="collapse additional_rows">
                <th></th>
                <th>Size</th>
                <th>Type, Reason</th>
                <th align="right">Pcs</th>
                <th></th>
              </tr>
              @foreach($end_product->production_result_addition as $addition)
              <tr class="collapse additional_rows">
                <td></td>
                <td>{{$addition->size}}</td>
                <td>{{$addition->reject_type}}, {{$addition->reject_reason}}</td>
                <td align="right">{{number_format($addition->block)}}</td>
                <td></td>
              </tr>
              @endforeach
              {{-- Additional Products --}}

              {{-- Miss Products --}}
              {{-- <tr>
                <td colspan="2"><b>Miss Product</b></td>
                <td></td>
                <td align="right"><b>{{number_format($total_miss, 2)}} kg</b></td>
                <td></td>
              </tr> --}}
              {{-- Miss Products --}}

              {{-- Yield --}}
              <tr>
                <td colspan="2"><b>Yield</b></td>
                <td></td>
                <td></td>
                <td>
                  <button type="button" class="btn btn-default btn-sm" data-toggle="collapse" data-target=".yield_rows">
                  <i class="fa fa-chevron-down"></i></button>
                </td>
              </tr>
              <tr class="collapse yield_rows">
                <td></td>
                <td>Waste Product</td>
                <td></td>
                <td align="right">{{number_format($total_waste / $end_product->weight * 100, 2)}} %</td>
                <td></td>
              </tr>
              <tr class="collapse yield_rows">
                <td></td>
                <td>Addition Product</td>
                <td></td>
                <td align="right">{{number_format($total_addition / $end_product->weight * 100, 2)}} %</td>
                <td></td>
              </tr>
              <tr class="collapse yield_rows">
                <td></td>
                <td>Ready Product</td>
                <td></td>
                <td align="right">
                  @if(($total_ready / $end_product->weight * 100) < 40)
                    <span class="label label-danger">
                    {{number_format($total_ready / $end_product->weight * 100, 2)}} %
                    </span>
                    @else
                    {{number_format($total_ready / $end_product->weight * 100, 2)}} %
                    @endif
                </td>
                <td></td>
              </tr>
              <tr class="collapse yield_rows">
                <td></td>
                <td>Miss Product</td>
                <td></td>
                <td align="right">{{number_format($total_miss / $end_product->weight * 100, 2)}} %</td>
                <td></td>
              </tr>
              {{-- Yield --}}   

            </table>
            <br>
            <div class="pull-right" >
              @if($end_product->status == 4)
              <a href="{{ URL::route('editor.end_product.pdf', $end_product->id) }}" class="btn btn-default btn-lg"><i class="fa fa-file-pdf-o"></i>&nbsp;PDF</a>
              @else
              {{ Form::open(['route' => ['end_product.close', $end_product->id], 'method' => 'PUT']) }}
              <button type="submit" class="btn btn-default btn-lg" onclick="return confirm('CLOSE PRODUCTION?')"><i class="fa fa-lock"></i> Close</button>
              {{ Form::close() }}
              @endif
              <a href="{{ URL::route('end_product') }}" class="btn btn-primary btn-lg" >
              <i class="fa fa-list"></i> Go to List</a> 
            </div>
  				  <br>
				  </div> 
          @actionEnd
		    </div>
		  </div>
	  </div>
  </div>
</section>
@stop