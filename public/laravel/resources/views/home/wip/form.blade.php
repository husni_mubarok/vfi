@extends('layouts.home.template')
@section('content')
<div class="header_menu mobile-hide">
  <a href="{{ URL::route('wip.index') }}">WIP</a>
</div> 
<section class="content">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
    {!! Form::model($purchase, array('route' => ['editor.purchase.update', $purchase->id], 'method' => 'PUT', 'class'=>'update', 'id' => 'form_wip'))!!} 
      <div class="col-md-3">
        <div class="x_panel"> 
          <div class="x_content">   

            {{ Form::label('trans_code', 'Trans Code') }} 
            {{ Form::text('trans_code',old('trans_code'), array('class' => 'form-control', 'placeholder' => 'Trans Code*', 'required' => 'true', 'id' => 'trans_code', 'disabled' => 'disabled')) }}<br/>

            {{ Form::label('uom', 'Type') }} 
            <select class="form-control" id="wip_type" name="wip_type"> 
              <option value="RECEIVING" selected="selected">RECEIVING</option>
              <option value="TREATMENT">TREATMENT</option>
            </select>
            <br/> 

            {{ Form::label('size_id', 'Size') }}  
            {{ Form::select('size_id', $size_list, old('size_id'), array('class' => 'form-control', 'required' => 'true')) }}<br/>  

            {{ Form::label('weight', 'Weight') }} 
            {{ Form::number('weight',old('weight'), array('class' => 'form-control', 'placeholder' => 'Weight*', 'required' => 'true', 'id' => 'weight')) }}<br/>  

            <button type="button" class="btn btn-sucess pull-right" id="btn_add_detail"><i class="fa fa-cart-plus"></i>&nbsp;Add</button> <br>  
          </div>  
        </div>
      </div>

      <div class="col-md-3">
        <h4>RECEIVING</h4>
        <hr>
        <div class="x_panel"> 
          <div class="x_content">  
            <div class="div_overflow">
              <table id="itemTable" class="table table-striped dataTable">
                <thead>
                  <tr> 
                    <th>Weight</th>
                    <th>Size</th> 
                  </tr>
                </thead>
                <tbody id="cart_receiving">
                </tbody>
              </table>
            </div> 
          </div>  
        </div> 
      </div>

      <div class="col-md-3">
        <h4>TREATMENT</h4>
        <hr>
        <div class="x_panel"> 
          <div class="x_content">  
            <div class="div_overflow">
              <table id="itemTable" class="table table-striped dataTable">
                <thead>
                  <tr> 
                    <th>Weight</th>
                    <th>Size</th>
                  </tr>
                </thead>
                <tbody id="cart_treatment">
                </tbody>
              </table>
            </div> 
          </div>  
        </div>
      </div>

      <div class="col-md-12">
        <hr>
        <button type="button" data-toggle="modal" class="btn btn-success pull-right" onclick="validate();" id="btn_save"><i class="fa fa-check"></i> Save</button>
        <a href="{{ URL::route('wip.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
      </div> 
      {!! Form::close() !!}
    </div>
  </section>
  @stop

@if(isset($wip))
<script>

  var cart_receiving = JSON.parse("{}"); 

// ADD BUTTON DETAIL
$("#btn_add_detail").on('click', function() { 

  console.log("asdas");
  
  var inpweight = $("#weight"); 

  if (inpweight.val().length < 1) {
    alert("Some data can't empty!");
  }else{
 
    var weight = $("#weight").val();  

    var size_id = $("#size_id option:selected").val(); 
    var wip_type = $("#wip_type option:selected").val(); 
    var key = size_id+"_"+weight+"_"+wip_type;

    if(cart_receiving[key] == undefined)
    {
      cart_receiving[key] = parseFloat(weight); 
      var new_row = '';
      new_row += '<tr>';
      new_row += '<td>';
      new_row += '<input type="hidden" name="size_id['+key+']" value="'+size_id+'">';
      new_row += $("#size_id option:selected").text();
      new_row += '</td>';

      new_row += '<td>';
      new_row += '<input type="hidden" name="wip_type['+key+']" value="'+wip_type+'">';
      new_row += wip_type;
      new_row += '</td>'; 

      new_row += '<td>'; 
      new_row += '<input type="hidden" name="weight['+key+']" value="'+cart_receiving[key]+'" id="weight_'+key+'">';
      new_row += '<div id="txtweight_'+key+'">'+weight+'</div>';
      new_row += '</td>';  

      new_row += '<td>'; 
      new_row += '<button type="button" class="btn btn-danger btn-sm" id="btn_remove_'+key+'">';
      new_row += '<i class="fa fa-remove"></i>';
      new_row += '</button>';
      new_row += '</td>';
      new_row += '</tr>';

      $("#cart_receiving").append(new_row); 

      $("#btn_remove_"+key).on('click', function() 
      {
        cart_receiving[key] == null;
        $(this).parent().parent().remove();
      });
    }
    else 
    {
    //alert("sadd");
    //console.log(cart_item[key]);

    cart_receiving[key] += parseFloat(weight);
    $("#weight_"+key).val(cart_receiving[key]);
    $("#txtweight_"+key).text(cart_receiving[key]); 

  } 

  document.getElementById ("weight").value = ""; 
}

});

jQuery.each({!! $wip !!}, function( i, val ) {  
  var size_id = val['size_id']; 
  var wip_type = val['wip_type'];  
  var weight = val['weight'];

  //var item_id = $("#item_id option:selected").val();  

  var key = size_id+"_"+"_"+wip_type+"_"+weight;

  $("#size_id").val(size_id);
  $("#wip_type").val(wip_type);
  //console.log(key);
 
  var new_row = '';
  new_row += '<tr>';
  new_row += '<td>';
  new_row += '<input type="hidden" name="size_id['+key+']" value="'+size_id+'">';
  new_row += $("#size_id option:selected").text();
  new_row += '</td>';

  new_row += '<td>';
  new_row += '<input type="hidden" name="wip_type['+key+']" value="'+wip_type+'">';
  new_row += $("#wip_type option:selected").text();
  new_row += '</td>';

  new_row += '<td>';
  new_row += '<input type="hidden" name="weight['+key+']" value="'+weight+'">';
  new_row += weight;
  new_row += '</td>'; 

  new_row += '<td>'; 
  new_row += '<button type="button" class="btn btn-danger btn-sm" id="btn_remove_'+key+'">';
  new_row += '<i class="fa fa-remove"></i>';
  new_row += '</button>';
  new_row += '</td>';
  new_row += '</tr>';

  $("#cart_receiving").append(new_row);

  $("#btn_remove_"+key).on('click', function() 
  {
    cart_receiving[key] == null;
    $(this).parent().parent().remove();
  });
  document.getElementById ("weight").value = "";
});
</script>
@endif 