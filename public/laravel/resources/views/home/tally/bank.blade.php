@extends('layouts.home.template')
@section('content')
<div class="header_menu mobile-hide">
	<a href="{{ URL::route('purchase.index') }}">PURCHASE</a>
</div>
<section class="content">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<div class="x_panel">
					<h2>
						<i class="fa fa-shopping-cart"></i>&nbsp;TALLY ACTIVATION
					</h2>
					<hr>
					<a href="{{ URL::route('tally.index') }}" class="btn btn-warning btn-md">OUTSTANDING</a>&nbsp;<a href="{{ URL::route('tally.bank') }}" class="btn btn-success btn-md">BANK</a>
					<hr>
					<div class="x_content">
						@foreach($project_list as $project)
						<button type="button" class="btn btn-default btn_project" value="{{$project}}">{{$project}}</button>
						@endforeach

						<table class="table table-hover table-striped" id="index_table">
							<thead>
								<tr>
									<th width="5%">No.</th>
									<th>Code</th>
									<th>Production Code</th>  
									<th>Supplier</th>
									<th>Product</th> 
									@actionStart('purchase', 'delete')
									<th></th>
									@actionEnd
									<th>Action</th>
								</tr>
							</thead>
							<tbody id="table_body">
								{{-- Content by Ajax --}}
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@stop



@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>  

	$('.btn_project').on('click', function()
	{
		var button = $(this);
		$('.btn_project').removeClass('active').attr('disabled', true);
		$('#index_table').DataTable().destroy();
		$('#table_body').empty().append('<tr><td colspan="9" align="center">Loading Data...</td></tr>');
		$.ajax({
			url: '{{URL::route('get.purchase_by_project')}}', 
			data: {'project': $(this).val()}, 
			type: 'GET', 
			headers : {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
			success: function(data, textStatus, jqXHR)
			{
				var new_row = '';
				jQuery.each(data, function(i, val)
				{
					var url_summary = '{{ URL::route('purchase.summary', 'id') }}';
					url_summary = url_summary.replace('id', val['id']);
					var url_delete = '{{ URL::route('purchase.delete', 'id') }}';
					url_delete = url_delete.replace('id', val['id']);

					//RECEIVING
					var url_create = '{{ URL::route('tally.create', 'id') }}';
					url_create = url_create.replace('id', val['id']);

					new_row += '<tr>';
					new_row += '<td>';
					new_row += i+1;
					new_row += '</td>';
					new_row += '<td>'; 
					new_row += val['doc_code']; 
					new_row += '</td>'; 
					new_row += '<td>';
					new_row += val['trans_code'];
					new_row += '</td>'; 
					new_row += '<td>';
					new_row += val['purchase_supplier']['supplier']['name'];
					new_row += '</td>';
					new_row += '<td>';
					new_row += val['purchase_product']['product']['name'];
					new_row += '</td>';  
					new_row += '<td>';
 
					new_row += '<a href="'+url_create+'" type="button" class="btn btn-danger btn-sm">';
					new_row += '<i class="fa fa-close"></i> Deactive</a>&nbsp;';  
 
					new_row += '</td>';
					new_row += '</tr>';
				});

				$('#table_body').empty().append(new_row);
				$("#index_table").DataTable();
				$('.btn_project').removeClass('active').attr('disabled', false);
				button.addClass('active');
			}, 
			error: function()
			{ 
				alert('Gagal menarik data!');
			}
		});



	});
</script>

<script>
  $(document).ready(function () {
    $("#index_table").DataTable(
    	{
    		"scrollX": true
    	});
    });
</script>

@stop