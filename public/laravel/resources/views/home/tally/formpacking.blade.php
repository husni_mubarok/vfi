@extends('layouts.home.template')
@section('content')

<div class="header_menu mobile-hide">
  <a href="{{ URL::route('tally.index') }}">MASTER TALLY</a>
</div> 
<section class="content">
  <div class="row"> 
    {!! Form::model($purchase, array('route' => ['tally.storepacking', $purchase->id], 'method' => 'PUT', 'class'=>'update', 'id' => 'form_tally'))!!}  
    <div class="col-md-12 col-sm-12 col-xs-12"> 
      <div class="x_panel"> 
        <div class="x_content">   
          <table id="itemTable" class="table table-striped dataTable">
            <thead>
              <tr> 
                <th rowspan="2">#</th>
                <th rowspan="2">TYPE</th>
                <th rowspan="2">GRADE</th>
                <th rowspan="2">SIZE</th>  
                <th rowspan="2">FREEZING</th> 
                <th rowspan="2">PRODUCTION CODE</th>
                <th rowspan="2">BLOCK</th>
                <th rowspan="2">TOTAL</th>
                <th rowspan="2">TOTAL BLOCK</th>
                <th rowspan="2">IC</th>
                <th colspan="4"><center>TO BE SET</center></th>
              </tr> 
              <tr> 
                <th>MC</th>
                <th>MIX</th>
                <th>REST</th>  
                <th>ICR</th>  
              </tr> 
            </thead> 
            <tbody>
             @foreach($tally_detail as $key => $tally_details)
             <tr>
              <td>{{$key+1}}</td> 
              <td>{{$tally_details->qty_type}}</td>
              <td>{{$tally_details->grade}}</td>  
              <td>{{$tally_details->size}}</td>
              <td>{{$tally_details->freezing}}</td>
              <td>{{$tally_details->purchase_code}} | {{$tally_details->code_size}}</td>
              <td>{{$tally_details->block}}</td>
              <td>{{$tally_details->total}}</td>
              <td>{{$tally_details->total_block}}</td>
              <td>
                {{ Form::number('tally['.$tally_details->id.'][ic]', old($tally_details->id.'[ic]', $tally_details->ic), ['class' => 'form-control', 'id' => 'ic_'.$tally_details->id, 'min' => '0', 'onchange' => 'change_total_ic_'.$tally_details->id.'();']) }} 
              </td>
              <td>
               {{ Form::number('tally['.$tally_details->id.'][mc]', old($tally_details->id.'[mc]', $tally_details->mc), ['class' => 'form-control', 'id' => 'mc_'.$tally_details->id, 'min' => '0', 'onchange' => 'change_total_mc_'.$tally_details->id.'();']) }} 
             </td>
             <td>
               {{ Form::number('tally['.$tally_details->id.'][mix]', old($tally_details->id.'[mix]', $tally_details->mix), ['class' => 'form-control', 'id' => 'mix_'.$tally_details->id, 'min' => '0']) }} 
             </td>
             <td>
              {{$tally_details->total_block}}
            </td>
            <td>
             {{ Form::number('tally['.$tally_details->id.'][icr]', old($tally_details->id.'[icr]', $tally_details->icr), ['class' => 'form-control', 'id' => 'icr_'.$tally_details->id, 'min' => '0']) }} 
           </td>
         </tr>
         <script type="text/javascript">
          function change_total_ic_{{$tally_details->id}}()
          { 
            $('#ic_{{$tally_details->id}}').css('background', 'red'); 
            document.getElementById('flag_change').value = 1;
          }

          function change_total_mc_{{$tally_details->id}}()
          { 
            $('#mc_{{$tally_details->id}}').css('background', 'red');
            document.getElementById('flag_change').value = 1;
          }
        </script>
        @endForeach
      </tbody> 
    </table>  
  </div>
</div>
</div> 
</div>
</div> 

<input type="hidden" id="flag_change"> 

<div class="col-md-12 col-sm-12 col-xs-12">
  <hr>
  <button type="button" id="btn_confirm" onclick="alert_change();" data-toggle="modal" data-target="#modal_confirm" class="btn btn-success pull-right"><i class="fa fa-check"></i> Save</button>
  <a href="{{ URL::route('tally.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>  
</div>
{!! Form::close() !!} 
</section>
@stop

@section('modal')
<div class="modal fade" id="modal_confirm">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="summary_cashbond_payroll_date"> Submit this form?</h4>
      </div> 
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
        <button type="button" id="btn_submit" onclick="submit_form();" class="btn btn-success"><i class="fa fa-check"></i> Submit</button>
      </div>
    </div>
  </div>
</div>
@stop

<script>
  function submit_form ()
  {  
   $('#form_tally').submit(); 
 };   

 function alert_change ()
 {  
  var change_total = document.getElementById('flag_change').value;
   if(change_total == 1)
   {
     alert("Total is change!");
   }
 };  
</script>


