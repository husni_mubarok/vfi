@extends('layouts.home.template')
@section('content')
<style type="text/css">
  .modal {
    text-align: center;
    padding: 0!important;
  }

  .modal:before {
    content: '';
    display: inline-block;
    height: 100%;
    vertical-align: middle;
    margin-right: -4px;
  }

  .modal-dialog {
    display: inline-block;
    text-align: left;
    vertical-align: middle;
  }
</style>
<div class="header_menu mobile-hide">
  <a href="{{ URL::route('tally.index') }}">MASTER TALLY</a>
</div> 
<section class="content">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      {!! Form::model($purchase, array('route' => ['tally.storedetail', $purchase->id], 'method' => 'PUT', 'class'=>'update', 'id' => 'form_tally'))!!} 
      <div class="col-md-4">
        <div class="x_panel"> 
          <div class="x_content">   

            {{ Form::label('trans_code', 'Trans Code') }} 
            {{ Form::text('trans_code_sw',old('trans_code', $purchase->trans_code), array('class' => 'form-control', 'placeholder' => 'Trans Code*', 'required' => 'true', 'id' => 'trans_code', 'disabled' => 'disabled')) }}
            {{ Form::hidden('trans_code',old('trans_code'), array('required' => 'true', 'id' => 'trans_code')) }}<br/>

            {{ Form::label('name', 'Product') }} 
            {{ Form::text('name',old('name'), array('class' => 'form-control', 'placeholder' => 'Product*', 'required' => 'true', 'id' => 'name', 'disabled' => 'disabled')) }}
            {{ Form::hidden('product_id',old('product_id'), array('required' => 'true', 'id' => 'product_id')) }}<br/> 
          </div>
        </div>
      </div> 

      {!! Form::close() !!} 
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-12">
          <div class="x_panel"> 
            <div class="x_content">  
              <table id="itemTable" class="table table-striped dataTable">
                <thead>
                  <tr>   
                    <th>Name</th>
                    <th><center>Weight</center></th> 
                  </tr> 
                </thead>
                <tbody>  
                    @foreach($array_all AS $all)  
                  <tr>  
                    <td>{{$all["name"]}}</td> 
                    @foreach($all["detail"] AS $detail_item) 
                    <td>{{$detail_item["weight"]}}</td>
                    @endforeach
                    <tr> 
                    @endforeach 
                    </tbody>
                  </table>   
                </div> 
              </div>
            </div>  
          </div>   
        </div>


        
            <div class="col-md-12 col-sm-12 col-xs-12">
              <hr>  
              {!! Form::open(array('route' => ['tally.close', $purchase->id], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_close'))!!}
              {{ csrf_field() }}  
              <a href="{{ URL::route('tally.viewpeeling', $purchase->id) }}" class="btn btn-default btn-lg pull-right" style="margin-right: 10px"><i class="fa fa-note"></i> Summary</a>
              <a href="{{ URL::route('tally.index') }}" class="btn btn-default btn-lg pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Exit</a>
              {!! Form::close() !!} 
            </div>
          </div> 
        </div> 
      </section>
      @stop

