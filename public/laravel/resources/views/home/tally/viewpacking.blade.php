@extends('layouts.home.template')
@section('content')

<div class="header_menu mobile-hide">
  <a href="{{ URL::route('tally.index') }}">MASTER TALLY</a>
</div> 
<section class="content">
  <div class="row"> 
 
    <div class="col-md-12 col-sm-12 col-xs-12"> 
        <div class="x_panel"> 
          <div class="x_content">   
            <table id="itemTable" class="table table-striped dataTable">
              <thead>
                <tr> 
                  <th rowspan="2">#</th>
                  <th rowspan="2">GRADE</th>
                  <th rowspan="2">SIZE</th>  
                  <th rowspan="2">FREEZING</th> 
                  <th rowspan="2">PRODUCTION CODE</th>
                  <th rowspan="2">BLOCK</th>
                  <th rowspan="2">TOTAL</th>
                  <th rowspan="2">TOTAL BLOCK</th>
                  <th rowspan="2">IC</th>
                  <th colspan="4"><center>TO BE SET</center></th>
                </tr> 
                <tr> 
                  <th>MC</th>
                  <th>MIX</th>
                  <th>REST</th>  
                  <th>ICR</th>  
                </tr> 
              </thead> 
              <tbody>
               @foreach($tally_detail as $key => $tally_details)
               <tr>
                <td>{{$key+1}}</td>  
                <td>{{$tally_details->grade}}</td>  
                <td>{{$tally_details->size}}</td>
                <td>{{$tally_details->freezing}}</td>
                <td>{{$tally_details->purchase_code}} | {{$tally_details->code_size}}</td>
                <td>{{$tally_details->block}}</td>
                <td></td>
                <td>{{$tally_details->ic}}</td>
                <td>{{$tally_details->total_block}}
                </td>
                <td>
                   {{$tally_details->mc}}
                </td>
                <td>
                   {{$tally_details->mix}}
                </td>
                <td>
                    {{$tally_details->total_block}}
                </td>
                <td>
                   {{$tally_details->icr}}
                </td>
              </tr> 
              @endForeach
            </tbody> 
          </table>  
        </div>
      </div>
    </div> 
  </div>
</div> 

<div class="col-md-12 col-sm-12 col-xs-12">
{!! Form::open(array('route' => ['tally.close', $purchase->id], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_close'))!!}
                {{ csrf_field() }}  
               <button type="button" id="btn_close" data-toggle="modal" data-target="#modal_close" class="btn btn-danger pull-right"><i class="fa fa-lock"></i> Close</button>
               <a href="{{ URL::route('tally.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Exit</a> 
               {!! Form::close() !!}
 
</section>
@stop

@section('modal')
 

 <div class="modal fade" id="modal_close">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Close form packing? </h4>
        </div> 
         
       <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
        <button type="button" id="btn_close" onclick="close_form();" class="btn btn-danger"><i class="fa fa-lock"></i> Close</button>
      </div>
    </div>
  </div>
</div>
@stop

<script>
  function close_form ()
  {
    //console.log("asd");
    $('#form_close').submit();
  };

</script>


