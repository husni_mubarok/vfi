@extends('layouts.home.template')
@section('content')
<div class="header_menu mobile-hide">
	<a href="{{ URL::route('purchase.index') }}">PURCHASE</a>
</div>
<section class="content">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<div class="x_panel">
					<h2>
						<i class="fa fa-shopping-cart"></i>&nbsp;MASTER TALLY
					</h2>
					<hr>
					
					<div class="x_content">
					{{ csrf_field() }}
					
					<ul class="nav nav-tabs" role="tablist">
    					<li role="presentation" class="active"><a href="#wait" aria-controls="wait" role="tab" data-toggle="tab">Waiting COO</a></li>
    					<li role="presentation"><a href="#outstanding" aria-controls="outstanding" role="tab" data-toggle="tab">Outstanding</a></li>
    					<li role="presentation"><a href="#bank" aria-controls="bank" role="tab" data-toggle="tab">Bank</a></li>
    					
    				</ul>

    				<div class="tab-content">
    					<div role="tabpanel" class="tab-pane active" id="wait">
    						<br><br>
    						<table class="table table-hover table-striped" id="index_table_approve">
								<thead>
									<tr>
										<th width="5%">No.</th>
										<th>Code</th>
										<th>Production Code</th>  
										<th>Supplier</th>
										<th>Product</th> 
										<th>action</th>
									</tr>
								</thead>
								<tbody id="table_body">

									@foreach($purchase_approved as $pa)
									<tr>
										<td>1</td>
										<td>{{ $pa->doc_code}}</td>
										<td>{{ $pa->trans_code}}</td>
										<td>{{ $pa->purchase_supplier->supplier->name }}</td>
										<td>{{ $pa->purchase_product->product->name }}</td>
										<td>
											@php
												$foo = $pa->trans_code;
											@endphp
											
											@if($foo[2] == 'A')
											<a href="javascript:void(0)" onclick="getTally({{$pa->id}}, 'A')" class="btn btn-success" >Action</a>
											@elseif($foo[2] == 'B')
											<a href="javascript:void(0)" onclick="getTally({{$pa->id}}, 'B')" class="btn btn-success" >Action</a>
											@endif
											
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
    					<div role="tabpanel" class="tab-pane" id="outstanding">
    						<br><br>
    						<table class="table table-hover table-striped" id="index_table_wait">
								<thead>
									<tr>
										<th width="5%">No.</th>
										<th>Code</th>
										<th>Production Code</th>
										<th>Quantity</th>  
										<th>Supplier</th>
										<th>Product</th> 
										<th>action</th>
									</tr>
								</thead>
								<tbody id="table_body">
									@foreach($execQuery as $eq)
    									<tr>
    										<td>{{$eq->id}}</td>
    										<td>{{$eq->doc_code}}</td>
    										<td>{{$eq->trans_code}}</td>
    										<td>{{$eq->quantity}}</td>
    										<td>{{$eq->name_supplier}}</td>
    										<td>{{$eq->name_product}}</td>
    										<td><a href="javascript:void(0)" class="btn btn-danger" onclick="setDeactive({{$eq->id}}, {{$eq->id_purchase}})">Deactive</a></td>
    									</tr>
    								@endforeach
								</tbody>
							</table>
    					</div>
    					<div role="tabpanel" class="tab-pane" id="bank">
    						<br><br>
    						<table class="table table-hover table-striped" id="index_table_bank">
								<thead>
									<tr>
										<th width="5%">No.</th>
										<th>Code</th>
										<th>Production Code</th>
										<th>Quantity</th>  
										<th>Supplier</th>
										<th>Product</th> 
									</tr>
								</thead>
								<tbody id="table_body">
									@foreach($execQueryBank as $eqb)
    									<tr>
    										<td>{{$eqb->id}}</td>
    										<td>{{$eqb->doc_code}}</td>
    										<td>{{$eqb->trans_code}}</td>
    										<td>{{$eqb->quantity}}</td>
    										<td>{{$eqb->name_supplier}}</td>
    										<td>{{$eqb->name_product}}</td>
    									</tr>
    								@endforeach
								</tbody>
							</table>
    					</div>
    				</div>
						
						
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<div class="modal fade" id="myModalFresh" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Type Fresh A</h4>
      </div>
      <div class="modal-body">
      	
      	<form class="form-horizontal">
      	
      	<input type="hidden" class="form-control" id="inputIdPurchaseA" name="inputIdPurchaseA">
      	<input type="hidden" class="form-control" id="inputQuantityA" name="inputQuantityA">
      	<input type="hidden" class="form-control" id="inputDefaultCodeA" name="inputDefaultCodeA">
		  
		  <div class="form-group">
		    <label for="inputDateA" class="col-sm-2 control-label">Date</label>
		    <div class="col-sm-7">
		      <input type="text" class="form-control" id="inputDateA" name="inputDateA" placeholder="DD/MM/YYYY" required="true">
		    </div>
		    <div class="col-sm-3">
		      <button type="button" class="btn btn-success" onclick="saveSetTally('A')">Active</button>
		    </div>
		  </div>
		</form>

      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="myModalFrozen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Type Frozen B</h4>
      </div>
      <div class="modal-body">


      	<form class="form-horizontal">
      	<input type="hidden" class="form-control" id="inputIdPurchaseB" name="inputIdPurchaseB">



		  <div class="form-group">
		    <label for="inputDefaultCodeB" class="col-sm-3 control-label">Default Doc Code</label>
		    <div class="col-sm-9">
		      <input type="text" class="form-control" id="inputDefaultCodeB" disabled="true">
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="inputQuantityRemaining" class="col-sm-3 control-label">Remaining Quantity</label>
		    <div class="col-sm-9">
		      <input type="text" class="form-control" id="inputQuantityRemaining" disabled="true">
		    </div>
		  </div>
		  <br>
		  <div class="form-group">
		    <label for="inputDate" class="col-sm-3 control-label">Date</label>
		    <div class="col-sm-9">
		      <input type="text" class="form-control" id="inputDateB" name="inputDateB" placeholder="DD/MM/YYYY" required="true">
		    </div>
		  </div>
		  <br>

		  <strong>Size</strong>
		  <hr>
		  <div id="appendElement"></div>
		  
		  <div class="form-group">
		    <label for="inputQuantityB" class="col-sm-3 control-label">Quantity</label>
		    <div class="col-sm-9">
		      <input type="text" class="form-control" id="inputQuantityB" name="inputQuantityB" readonly="true" required="true">
		    </div>
		  </div>
		  <hr>
		  <div class="form-group">
		    <div class="col-sm-offset-3 col-sm-9">
		      <button type="button" class="btn btn-success" onclick="saveSetTally('B')">Active</button>
		    </div>
		  </div>
		</form>
		<hr>

		
		<table class="table table-striped">
			<thead>
				<tr>
					<th>NO</th>
					<th>Date</th>
					<th>Doc Code</th>
					<th>Quantity</th>
				</tr>
			</thead>
			<tbody  id='elementTR'>
				
			</tbody>
		</table>
		
		<br><br>


      </div>
    </div>
  </div>
</div>

@stop



@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
	
	function setDeactive(id, id_purchase){
		console.log(id)
		var urlV = '{{ URL::to("/tally/deactive") }}';
		$.ajax({
			data : 'id='+id+'&id_purchase='+id_purchase,
			url : urlV,
			type : 'GET',
			success : function(res){
				if (res) {
					location.reload();
				}
			}
		})
	}

	function getTally(id, type){
		console.log(id);
		console.log(type);

		var urlV = '{{ URL::to("/tally/set") }}';

		$.ajax({
			data : 'id='+id,
			url : urlV,
			type : 'GET',
			success : function(res){
				console.log(res);
				var resJSON = JSON.parse(res);
				console.log(resJSON[0].purchase_time);
				var foox = 0;
				for (var x = 0;  x < resJSON[0].purchase_detail.length; x++) {
					var foox = foox + resJSON[0].purchase_detail[x].amount
				}

				// var qtyDefault = resJSON[0].quantity;
				var qtyDefault = foox;
				var temptally = resJSON[0].temptally;
				var nPlus = 0;
				if (temptally.length > 0) {
					for (var i = 0;  i < temptally.length; i++) {
						nPlus = nPlus + parseInt(temptally[i].quantity);
					}
				}
				var nPlusCalc = qtyDefault - nPlus;


				if (type == "A") {
					$('#inputDateA').val(resJSON[0].purchase_time);
					$('#inputIdPurchaseA').val(resJSON[0].id)
					$('#inputQuantityA').val(resJSON[0].quantity);
					$('#inputDefaultCodeA').val(resJSON[0].trans_code)
					$('#myModalFresh').modal();

				} else {
					var elementAppend = '';
					$('#inputDefaultCodeB').val(resJSON[0].trans_code);
					$('#inputIdPurchaseB').val(resJSON[0].id);
					$('#inputQuantityRemaining').val(nPlusCalc);
					
					var size_tag = ['L', 'M', 'S'];

					for (var x = 0;  x < resJSON[0].purchase_detail.length; x++) {
						elementAppend += '<div class="form-group"><label for="inputQuantity" class="col-sm-3 control-label">Size '+size_tag[resJSON[0].purchase_detail[x].size - 1]+' | '+ resJSON[0].purchase_detail[x].amount+'</label><div class="col-sm-9"><input type="number" onkeyup="calcSize()" class="form-control" id="inputQuantity'+x+'" name="'+resJSON[0].purchase_detail[x].id+'" placeholder="Quantity"></div></div>';
					}

					var elementAppendTR = '';
					for (var i = 0; i < resJSON[0].temptally.length; i++) {
						console.log(resJSON[0].temptally[i].date_time);
						elementAppendTR += '<tr><td>1</td><td>'+resJSON[0].temptally[i].date_time+'</td><td>'+resJSON[0].temptally[i].trans_code+'</td><td>'+resJSON[0].temptally[i].quantity+'</td></tr>'
					}

					$('#elementTR').html(elementAppendTR)
					$('#appendElement').html('<div id="form_size">'+elementAppend+'</div>');
					$('#myModalFrozen').modal();

				}
			}
		})
	}

	function calcSize(){
		var countV = 0;
		$('#form_size input[type=number]').each(function(){
			
			var valueEL = $(this).val();
			console.log(valueEL);
			if (valueEL == '' ) {
				valueEL = 0;
			}
			countV = parseInt(countV) + parseInt(valueEL);
		});
		$('#inputQuantityB').val(countV);
	}


	function saveSetTally(type){

		var jsonPass = null;
		if (type == 'A') {
			var id_purchase = $('#inputIdPurchaseA').val();
			var date_time = $('#inputDateA').val();
			var quantity = $('#inputQuantityA').val();
			var trans_code = $('#inputDefaultCodeA').val();
			if (date_time == '') {
				alert('Date Time Required');
				return false;
			} 
		}else {
			var id_purchase = $('#inputIdPurchaseB').val();
			var date_time = $('#inputDateB').val();
			var quantity = $('#inputQuantityB').val();
			var trans_code = $('#inputDefaultCodeB').val();
			
			var jsonDetail = {}
			$('#form_size input[type=number]').each(function(){
				var keyName = $(this).attr('name');
				jsonDetail[keyName] = $(this).val(); 
			});

			if (date_time == '') {
				alert('Date Time Required');
				return false;
			}

			if(quantity == 0 || quantity==''){
				alert('Quantity Required');
				return false;
			}

			var jsonPass = '&jsonPass='+ JSON.stringify(jsonDetail);
		}
		
		var typeF = type;

		var urlV = '{{ URL::to("/tally/set/post") }}';

		$.ajax({
			data : 'id_purchase='+id_purchase+'&date_time='+date_time+'&quantity='+quantity+'&typeF='+typeF+'&trans_code='+trans_code+jsonPass,
			url : urlV,
			type : 'GET',
			success : function(res){
				if (res) {
					location.reload();
				}
			}
		});

	}

	$(document).ready(function() {
		$("#index_table_approve").DataTable();
		$("#index_table_wait").DataTable();	

		$('#inputDateA').datetimepicker({
			sideBySide: true,
			format: 'YYYY-MM-DD HH:mm',
		});

		$('#inputDateB').datetimepicker({
			sideBySide: true,
			format: 'YYYY-MM-DD HH:mm',
		});

	});

</script>
@stop