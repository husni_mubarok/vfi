@extends('layouts.home.template')
@section('content')
<style type="text/css">
  .modal {
    text-align: center;
    padding: 0!important;
  }

  .modal:before {
    content: '';
    display: inline-block;
    height: 100%;
    vertical-align: middle;
    margin-right: -4px;
  }

  .modal-dialog {
    display: inline-block;
    text-align: left;
    vertical-align: middle;
  }
</style>
<div class="header_menu mobile-hide">
  <a href="{{ URL::route('tally.index') }}">MASTER TALLY</a>
</div> 
<section class="content">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      {!! Form::model($purchase, array('route' => ['tally.storedetail', $purchase->id], 'method' => 'PUT', 'class'=>'update', 'id' => 'form_tally'))!!} 
      <div class="col-md-4">
        <div class="x_panel"> 
          <div class="x_content">   

            {{ Form::label('trans_code', 'Trans Code') }} 
            {{ Form::text('trans_code_sw',old('trans_code', $purchase->trans_code), array('class' => 'form-control', 'placeholder' => 'Trans Code*', 'required' => 'true', 'id' => 'trans_code', 'disabled' => 'disabled')) }}
            {{ Form::hidden('trans_code',old('trans_code'), array('required' => 'true', 'id' => 'trans_code')) }}<br/>

            {{ Form::label('name', 'Product') }} 
            {{ Form::text('name',old('name'), array('class' => 'form-control', 'placeholder' => 'Product*', 'required' => 'true', 'id' => 'name', 'disabled' => 'disabled')) }}
            {{ Form::hidden('product_id',old('product_id'), array('required' => 'true', 'id' => 'product_id')) }}<br/> 
          </div>
        </div>
      </div>  
      {!! Form::close() !!} 

      @if(isset($array_all_receiving))
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-12">
          <div class="x_panel"> 
            <div class="x_content"> 
              <hr>
              <b>RECEIVING</b>
              <hr> 
              <table id="itemTable" class="table table-striped dataTable">
                <thead>
                  <tr>   
                    <th>Size</th>
                    <th><center>Weight</center></th> 
                    @foreach($array_all_receiving AS $all_receiving)  
                  </tr> 
                </thead>
                <tbody> 
                  <tr>  
                    <td>{{$all_receiving["note"]}}</td> 
                    @foreach($all_receiving["detail"] AS $detail_item_receiving) 
                    <td>{{$detail_item_receiving["weight"]}}</td>
                    @endforeach
                    <tr> 
                    </tbody>
                    @endforeach 
                  </table>   
                </div> 
              </div>
            </div>  
          </div>   
        </div>
        @endif 

        @if(isset($array_all_treatment))
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="col-md-12">
            <div class="x_panel"> 
              <div class="x_content">
                <hr> 
                <b>TREATMENT</b>
                <hr> 
                <table id="itemTable" class="table table-striped dataTable">
                  <thead>
                    <tr>   
                      <th>Size</th>
                      <th><center>Weight</center></th>  
                      @foreach($array_all_treatment AS $all_treatment)  
                    </tr> 
                  </thead>
                  <tbody> 
                    <tr>  
                      <td>{{$all_treatment["note"]}}</td> 
                      @foreach($all_treatment["detail"] AS $detail_item_treatment) 
                      <td>{{$detail_item_treatment["weight"]}}</td>
                      @endforeach
                      <tr> 
                      </tbody>
                      @endforeach  
                    </table>   
                  </div> 
                </div>
              </div>  
            </div>   
          </div> 
          @endif 
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="col-md-12">
              <div class="x_panel"> 
                <div class="x_content">
                  <hr> 
                  <b>ADDITIONAL</b>
                  <hr> 
                  <table id="itemTable" class="table table-bordered dataTable">
                    <thead>
                      <tr>   
                        <th>Name</th>
                        <th colspan="{{$count_add}}"><center>Weight</center></th>   
                        <th colspan="{{$count_add}}"><center>Pan</center></th>   
                        <th colspan="{{$count_add}}"><center>Rest</center></th>   

                      </tr> 
                    </thead>
                    <tbody> 
                    
                      @foreach($array_all_add AS $all_add)
                      <tr>   
                        <td>{{$all_add["name"]}}</td> 
                        @foreach($all_add["detail"] AS $detail_add)  
                        <td>{{$detail_add["weight"]}}</td>  
                        @endforeach 
                        @foreach($all_add["detail"] AS $detail_add)  
                        <td>{{$detail_add["pan"]}}</td>  
                        @endforeach
                        @foreach($all_add["detail"] AS $detail_add)  
                        <td>{{$detail_add["rest"]}}</td>  
                        @endforeach
                        <tr> 
                         @endforeach 

                         </tbody> 
                     </table>   
                   </div> 
                 </div>
               </div>  
             </div>   
           </div>
           <div class="col-md-12 col-sm-12 col-xs-12">
            <hr>  
            {!! Form::open(array('route' => ['tally.close', $purchase->id], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_close'))!!}
            {{ csrf_field() }}  
            <a href="{{ URL::route('tally.view', $purchase->id) }}" class="btn btn-default btn-lg pull-right" style="margin-right: 10px"><i class="fa fa-note"></i> Summary</a>
            <a href="{{ URL::route('tally.index') }}" class="btn btn-default btn-lg pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Exit</a>
            {!! Form::close() !!} 
          </div>
        </div> 
      </div> 
    </section>
    @stop

