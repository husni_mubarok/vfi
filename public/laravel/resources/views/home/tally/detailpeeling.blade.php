@extends('layouts.home.template')
@section('content')
<style type="text/css">
  .modal {
    text-align: center;
    padding: 0!important;
  }

  .modal:before {
    content: '';
    display: inline-block;
    height: 100%;
    vertical-align: middle;
    margin-right: -4px;
  }

  .modal-dialog {
    display: inline-block;
    text-align: left;
    vertical-align: middle;
  }
</style>
<div class="header_menu mobile-hide">
  <a href="{{ URL::route('tally.index') }}">MASTER TALLY</a>
</div> 
<section class="content">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      {!! Form::model($purchase, array('route' => ['tally.storedetailpeeling', $purchase->id], 'method' => 'PUT', 'class'=>'update', 'id' => 'form_tally'))!!} 
      <div class="col-md-4">
        <div class="x_panel"> 
          <div class="x_content">   

            {{ Form::label('trans_code', 'Trans Code') }} 
            {{ Form::text('trans_code_sw',old('trans_code', $purchase->trans_code), array('class' => 'form-control', 'placeholder' => 'Trans Code*', 'required' => 'true', 'id' => 'trans_code', 'disabled' => 'disabled')) }}
            {{ Form::hidden('trans_code',old('trans_code'), array('required' => 'true', 'id' => 'trans_code')) }}<br/>

            {{ Form::label('name', 'Product') }} 
            {{ Form::text('name',old('name'), array('class' => 'form-control', 'placeholder' => 'Product*', 'required' => 'true', 'id' => 'name', 'disabled' => 'disabled')) }}
            {{ Form::hidden('product_id',old('product_id'), array('required' => 'true', 'id' => 'product_id')) }}<br/>
            {{ Form::label('project', 'Project') }} 

            {{ Form::text('project',old('project'), array('class' => 'form-control', 'placeholder' => 'Project*', 'required' => 'true', 'id' => 'project', 'disabled' => 'disabled')) }}<br>

            
            <table id="itemTable" class="table table-striped dataTable">
              <thead>
                <tr> 
                  <th>#</th>
                  <th>Name</th>
                  <th>Weight</th> 
                  <th>Action</th> 
                </tr>
              </thead>
              <tbody>
               @foreach($tally_detail as $key => $tally_details)
               <tr>
                <td>
                  {{$key+1}}</td>  
                  <td> 
                    {{$tally_details->name}}
                  </td>  
                  <td> 
                    {{ Form::number('tally['.$tally_details->id.'][weight]', old($tally_details->id.'[weight]', 0), ['class' => 'form-control', 'id' => 'weight_'.$tally_details->id, 'min' => '0']) }} 
                  </td> 
                  <td> 
                    <button type="button" class="btn btn-primary btn-sm" id="btn_add_{{$tally_details->id}}" onclick="add_{{$tally_details->id}}();">
                     <i class="fa fa-plus"></i>
                   </button> 
                   <button type="button"  class="btn btn-warning btn-sm" id="btn_reduce_{{$tally_details->id}}" onclick="reduce_{{$tally_details->id}}();"><i class="fa fa-minus"></i>
                   </button>
                 </td> 
               </tr>

               <script type="text/javascript">  
                function reduce_{{$tally_details->id}}() { 
                  var weight_{{$tally_details->id}} = document.getElementById('weight_{{$tally_details->id}}').value; 
                  document.getElementById('weight_{{$tally_details->id}}').value = parseInt(weight_{{$tally_details->id}})-parseInt(1); 
                };

                function add_{{$tally_details->id}}() {  
                  var weight_{{$tally_details->id}} = document.getElementById('weight_{{$tally_details->id}}').value; 
                  document.getElementById('weight_{{$tally_details->id}}').value = parseInt(weight_{{$tally_details->id}})+parseInt(1); 
                }; 

              </script>
              @endForeach
            </tbody>
          </table>  
          <hr>
          <button type="button" id="btn_confirm" data-toggle="modal" data-target="#modal_confirm" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Add</button>
          <a href="{{ URL::route('tally.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Exit</a>  
          <br>  
        </div>  
      </div>
    </div> 
    {!! Form::close() !!}

    <div class="col-md-4">
    <h3>SUMMARY</h3>
    <hr>  
      <div class="x_panel"> 
        <div class="x_content">  
          <div class="div_overflow">
            <table id="itemTable" class="table table-striped dataTable">
              <thead>
                <tr> 
                  <th>#</th>
                  <th>Size</th>
                  <th>Weight</th> 
                </tr>
              </thead>
              <tbody>
                @foreach($tally_detail_view as $key => $tally_detail_views)
                <tr>
                  <td>{{$key+1}}</td>
                  <td>{{$tally_detail_views->name}}</td>
                  <td>{{$tally_detail_views->weight}}</td>
                </tr>
                @endForeach
              </tbody>
            </table>  
            <hr>
             
          </div> 
        </div>  
      </div> 
    </div>

    <div class="col-md-4">
    <h3>HISTORY</h3>
    <hr>  
      <div class="x_panel"> 
        <div class="x_content">  
          <div class="div_overflow">
           <table id="itemTable" class="table table-striped dataTable">
                <thead>
                  <tr>   
                    <th>Name</th>
                    <th><center>Weight</center></th> 
                  </tr> 
                </thead>
                <tbody>  
                    @foreach($array_all AS $all)  
                  <tr>  
                    <td>{{$all["name"]}}</td> 
                    @foreach($all["detail"] AS $detail_item) 
                    <td>{{$detail_item["weight"]}}</td>
                    @endforeach
                    <tr> 
                    @endforeach 
                    </tbody>
                  </table>   
            <hr>
            {!! Form::open(array('route' => ['tally.closepeeling', $purchase->id], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_close'))!!}
            {{ csrf_field() }}  
            <button type="button" id="btn_close" data-toggle="modal" data-target="#modal_close" class="btn btn-danger pull-right btn-lg"><i class="fa fa-lock"></i> Close</button> 
            <a href="{{ URL::route('tally.historypeeling', $purchase->id) }}" target="_blank" type="button" class="btn btn-default pull-right btn-lg" style="margin-right: 10px;"><i class="fa fa-bars"></i> History</a>
            {!! Form::close() !!}
          </div> 
        </div>  
      </div> 
    </div>
  </div> 
</section>
@stop

@section('modal')
<div class="modal fade" id="modal_confirm">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Add to form peeling? </h4>
      </div> 

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
        <button type="button" id="btn_submit" onclick="submit_form();" class="btn btn-success"><i class="fa fa-check"></i> Add</button>
      </div>
    </div>
  </div>
</div> 
<div class="modal fade" id="modal_close">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Close form peeling? </h4>
      </div> 

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
        <button type="button" id="btn_close" onclick="close_form();" class="btn btn-danger"><i class="fa fa-lock"></i> Close</button>
      </div>
    </div>
  </div>
</div>
@stop

<script>

  function checkbox_toggle(id)
  {
    if($('#checkbox_'+id).is(":checked"))
    {
      $('#weight_'+id).attr('disabled', false); 
    } else {
      $('#weight_'+id).val('');
      $('#weight_'+id).attr('disabled', true); 
    }
  };



  function submit_form ()
  {
    //console.log("asd");
    $('#form_tally').submit();
  };

  function close_form ()
  {
    //console.log("asd");
    $('#form_close').submit();
  };

  function set_btn_receiving_active()
  { 
    $("#btn_receiving").removeClass("btn btn-default").addClass("btn btn-primary");
    $("#btn_treatment").removeClass("btn btn-primary").addClass("btn btn-default");

    document.getElementById ("tally_type").value = "RECEIVING";

  };
  function set_btn_treatment_active()
  { 
    $("#btn_treatment").removeClass("btn btn-default").addClass("btn btn-primary");
    $("#btn_receiving").removeClass("btn btn-primary").addClass("btn btn-default");

    document.getElementById ("tally_type").value = "TREATMENT";

  };
</script> 

<script> 
  jQuery.each({!! $tally_detail !!}, function(key, value)
  {
    $('#checkbox_'+value['size_id']).attr('checked', true);
    checkbox_toggle(value['size_id']);
    $('#weight_'+value['size_id']).val(value['weight']);
  });
</script>
