<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tally extends Model
{
    protected $table = 'tally';
	protected $dates = ['deleted_at'];

	public function purchase()
	{
		return $this->hasMany('App\Model\Purchase', 'purchase_id', 'id');
	} 
}
