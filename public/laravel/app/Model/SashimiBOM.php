<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SashimiBOM extends Model
{
    protected $table = 'sashimi_bom';
    protected $dates = ['deleted_at'];

    public function storage()
    {
    	return $this->belongsTo('App\Model\TempStorage', 'storage_id', 'id');
    }

    public function sashimi()
    {
    	return $this->belongsTo('App\Model\Sashimi', 'sashimi_id', 'id');
    }

    public function sashimi_result()
    {
    	return $this->hasMany('App\Model\SashimiResult', 'sashimi_bom_id', 'id');
    }

    public function cart_detail()
    {
        return $this->belongsTo('App\Model\CartDet', 'storage_id', 'id');
    }
}
