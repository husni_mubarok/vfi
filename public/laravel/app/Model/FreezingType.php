<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FreezingType extends Model
{
	use SoftDeletes;
	protected $table = 'freezing_type';
	protected $dates = ['deleted_at']; 
}