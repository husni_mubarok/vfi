<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomDetail extends Model
{
	protected $table = 'custom_detail';
	protected $dates = ['deleted_at'];

	public function master_item()
	{
		return $this->belongsTo('App\Model\MasterItem', 'id_master_item', 'id');
	}
}
