<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Purchase extends Model
{
    protected $table = 'purchases';
	protected $dates = ['deleted_at'];

	public function purchase_detail()
	{
		return $this->hasMany('App\Model\PurchaseDetail', 'purchase_id', 'id');
	}

    public function purchase_supplier()
    {
    	return $this->hasOne('App\Model\PurchaseSupplier', 'purchase_id', 'id');
    }

    public function price()
    {
    	return $this->hasMany('App\Model\Price', 'purchase_id', 'id');
    }

    public function purchase_product()
    {
    	return $this->hasOne('App\Model\PurchaseProduct', 'purchase_id', 'id');
    }

    public function purchase_reject()
    {
        return $this->hasMany('App\Model\PurchaseReject', 'purchase_id', 'id');
    }

    public function production_result_func()
    {
        return $this->hasOne('App\Model\ProductionResult', 'id', 'production_id');
    }

    public function production_result()
    {
        return $this->hasMany('App\Model\ProductionResult', 'purchase_id', 'id');
    }

    public function temp_tally()
    {
        return $this->belongsTo('App\Model\TempTally', 'id_purchase', 'id');
    }

    public function temptally()
    {
        return $this->hasMany('App\Model\TempTally', 'id_purchase', 'id');
    }
}
