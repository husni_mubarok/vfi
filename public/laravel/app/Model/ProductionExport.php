<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductionExport extends Model
{
    protected $table = 'export';
    protected $dates = ['deleted_at'];

    public function productionresult()
    {
    	return $this->belongsTo('App\Model\ProductionResult', 'storage_id', 'id');
    }

    public function cart()
    {
    	return $this->belongsTo('App\Model\Cart', 'cart_id', 'id');
    }
}
