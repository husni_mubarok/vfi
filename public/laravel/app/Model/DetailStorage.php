<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class DetailStorage extends Model
{
  protected $table = 'detail_storage';
  protected $dates = ['deleted_at'];


}
