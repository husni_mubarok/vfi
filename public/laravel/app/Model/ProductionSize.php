<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductionSize extends Model
{
    protected $table = 'productions_sizes';
    protected $dates = ['deleted_at'];

    public function production()
    {
    	return $this->belongsTo('App\Model\Production', 'production_id', 'id');
    }

    public function size()
    {
    	return $this->belongsTo('App\Model\Size', 'size_id', 'id');
    }
}
