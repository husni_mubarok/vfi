<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SashimiCost extends Model
{
   protected $table = 'sashimi_cost';
   protected $dates = ['deleted_at'];

   public function storage()
   {
       return $this->belongsTo('App\Model\TempStorage', 'storage_id', 'id');
   }

   public function sashimi()
   {
       return $this->belongsTo('App\Model\Sashimi', 'sashimi_id', 'id');
   }

   public function sashimi_result()
   {
       return $this->hasMany('App\Model\SashimiResult', 'sashimi_cost_id', 'id');
   }
}