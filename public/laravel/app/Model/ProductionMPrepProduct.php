<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductionMPrepProduct extends Model
{
    protected $table = 'productions_m_prep_products';
    protected $dates = ['deleted_at'];

    public function m_prep_product()
    {
    	return $this->belongsTo('App\Model\MPrepProduct', 'm_prep_product_id', 'id');
    }

    public function production()
    {
    	return $this->belongsTo('App\Model\Production', 'production_id', 'id');
    }
}
