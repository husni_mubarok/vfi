<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DocCode extends Model
{
    protected $table = 'doc_codes';
    protected $dates = ['deleted_at'];
}
