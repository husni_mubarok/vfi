<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TallyReceivingTreatment extends Model
{
    protected $table = 'tally_receiving_treatment';
	protected $dates = ['deleted_at'];

}
