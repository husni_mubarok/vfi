<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductionResultDetail extends Model
{
    use SoftDeletes;
    
    protected $table = 'production_result_details';
    protected $dates = ['deleted_at'];

    public function production_result()
    {
    	return $this->belongsTo('App\Model\ProductionResult', 'production_result_id', 'id');
    }

    public function storage()
    {
    	return $this->belongsTo('App\Model\TempStorage', 'id', 'source_id');
    }

    public function uom()
    {
    	return $this->hasOne('App\Model\Uom', 'uom_id', 'id');
    }

    public function from()
    {
        return $this->hasMany('App\Model\DetailTransactionHistory', 'from_id', 'id');
    }

    public function to()
    {
        return $this->hasMany('App\Model\DetailTransactionHistory', 'to_id', 'id');
    }
}
