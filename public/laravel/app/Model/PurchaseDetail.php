<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseDetail extends Model
{
    protected $dates = ['deleted_at'];

    public function purchase()
    {
    	return $this->belongsTo('App\Model\Purchase', 'purchase_id', 'id');
    }

    public function temp_tally_detail()
    {
    	return $this->belongsTo('App\Model\TempTalyDetail', 'id_purchase_details', 'id');
    }

    public function temp_tally()
    {
    	return $this->belongsTo('App\Model\TempTally', 'id_purchase', 'id');
    }

    // public function price()
    // {
    // 	return $this->belongsTo('App\Model\Price', 'price_id', 'id');
    // }
}
