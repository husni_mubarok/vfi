<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TallyDetailAdditional extends Model
{
    protected $table = 'tally_detail_additional';
	protected $dates = ['deleted_at'];

	 
}
