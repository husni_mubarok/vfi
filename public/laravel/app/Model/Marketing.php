<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class marketing extends Model
{
	use SoftDeletes;
	protected $table = 'marketing';
    protected $dates = ['deleted_at'];

    public function marketing_cost()
    {
    	return $this->belongsTo('App\Model\MarketingCost', 'id_marketing', 'id');
    }

}
