<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SupplierProduct extends Model
{
    protected $table = 'suppliers_products';
    protected $dates = ['deleted_at'];

    public function supplier()
    {
    	return $this->belongsTo('App\Model\Supplier', 'supplier_id', 'id');
    }

    public function product()
    {
    	return $this->belongsTo('App\Model\Product', 'product_id', 'id');
    }
}
