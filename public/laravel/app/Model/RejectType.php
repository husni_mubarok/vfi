<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RejectType extends Model
{
    protected $table = 'reject_types';
    protected $dates = ['deleted_at'];

    public function product_additional()
    {
    	return $this->belongsTo('App\Model\ProductionResultAddition', 'reject_type_id', 'id');
    }
}
