<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Revenue extends Model
{
    use SoftDeletes;

    protected $table = 'revenue';
    protected $dates = ['deleted_at'];

    public function branch()
	{
		return $this->belongsTo('App\Model\Branch', 'branch_id', 'id');
	}

	public function revenue_attachment()
	{
		return $this->hasOne('App\Model\RevenueAttachment', 'revenue_id', 'id');
	}
}
