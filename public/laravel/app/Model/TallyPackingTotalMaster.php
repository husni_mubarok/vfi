<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TallyPackingTotalMaster extends Model
{
	protected $table = 'tally_packing_total_master';
    protected $dates = ['deleted_at']; 
}
