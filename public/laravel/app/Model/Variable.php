<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Variable extends Model
{
    protected $table = 'variables';
    protected $dates = ['deleted_at'];

    public function production_variable()
    {
    	return $this->hasMany('App\Model\ProductionVariable', 'variable_id', 'id');
    }

    public function production_result_variable()
    {
    	return $this->hasMany('App\Model\ProductionResultVariable', 'variable_id', 'id');
    }
}
