<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TallyAdditionalMaster extends Model
{
	use SoftDeletes;
	protected $table = 'tally_additional_master';
	protected $dates = ['deleted_at']; 
}
