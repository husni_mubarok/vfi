<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetailUom extends Model
{
	use SoftDeletes;
	protected $table = 'detail_uom';
	protected $dates = ['deleted_at'];

	public function item()
	{
		return $this->belongsTo('App\Model\Item', 'id_item', 'id');
	}

	public function uom()
	{
		return $this->belongsTo('App\Model\DetailUom', 'id_uom', 'id');
	}

	public function recipe_item()
	{
		return $this->hasMany('App\Model\RecipeItem', 'detail_uom_id', 'id');
	}
}
