<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MasterItem extends Model
{
	use SoftDeletes;
	protected $table = 'master_item';
	protected $dates = ['deleted_at'];

	public function type_item()
	{
		return $this->belongsTo('App\Model\TypeItem','id_type_item', 'id');
	}

	public function category_item()
	{
		return $this->belongsTo('App\Model\CategoryItem', 'id_category_item', 'id');
	}

	public function item()
	{
		return $this->hasMany('App\Model\Item', 'id_master_item', 'id');
	}
}
