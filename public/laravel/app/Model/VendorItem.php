<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VendorItem extends Model
{
	use SoftDeletes;
	protected $table = 'vendor_item';
	protected $dates = ['deleted_at'];

	public function item()
	{
		return $this->belongsTo('App\Model\Item', 'item_id', 'id');
	}

	public function vendor()
	{
		return $this->belongsTo('App\Model\Vendor', 'invoice_type_id', 'id');
	} 
}
