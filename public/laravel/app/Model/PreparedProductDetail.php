<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PreparedProductDetail extends Model
{
    protected $table = 'prep_prod_details';
    protected $dates = ['deleted_at'];

    public function prepared_product()
    {
    	return $this->belongsTo('App\Model\PreparedProduct', 'prep_prod_id', 'id');
    }

    public function prepared_product_detail_ingredient()
    {
    	return $this->hasMany('App\Model\PrepProdDetailIngredient', 'prep_prod_dtl_id', 'id');
    }
}
