<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TallyPeelingTreatment extends Model
{
    protected $table = 'tally_peeling_treatment';
	protected $dates = ['deleted_at'];

}
