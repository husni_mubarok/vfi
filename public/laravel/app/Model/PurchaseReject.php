<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseReject extends Model
{
	protected $table = 'purchase_rejects';
    protected $dates = ['deleted_at'];

    public function purchase()
    {
    	return $this->belongsTo('App\Model\Purchase', 'purchase_id', 'id');
    }

    public function user()
    {
        return $this->hasOne('App\Model\User', 'id', 'user_id');
    }
}
