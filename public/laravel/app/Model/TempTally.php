<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TempTally extends Model
{
    protected $table = 'temp_tally';
	protected $dates = ['deleted_at'];

	public function temp_tally_detail()
	{
		return $this->hasMany('App\Model\TempTallyDetail', 'id_temp_tally', 'id');
	}

    public function getpurchase()
    {
        return $this->hasOne('App\Model\Purchase', 'id', 'id_purchase');
    }

    public function purchase_details()
    {
        return $this->hasMany('App\Model\PurchaseDetail', 'purchase_id', 'id_purchase');
    }

    public function purchase()
    {
        return $this->belongsTo('App\Model\Purchase', 'id_purchase', 'id');
    }
}
