<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Production extends Model
{
    protected $table = 'productions';
    protected $dates = ['deleted_at'];

    public function fish()
    {
    	return $this->belongsTo('App\Model\Fish', 'fish_id', 'id');
    }

    public function production_result()
    {
    	return $this->hasMany('App\Model\ProductionResult', 'production_id', 'id');
    }

    public function production_variable()
    {
    	return $this->hasMany('App\Model\ProductionVariable', 'production_id', 'id');
    }

    public function production_size()
    {
        return $this->hasMany('App\Model\ProductionSize', 'production_id', 'id');
    }

    public function production_m_prep_product()
    {
        return $this->hasMany('App\Model\ProductionMPrepProduct', 'm_prep_product_id', 'id');
    }

    public function production_ingredient()
    {
        return $this->hasMany('App\Model\ProductionIngredient', 'production_id', 'id');
    }

    public function recipe()
    {
        return $this->hasOne('App\Model\ProductionRecipe', 'production_id', 'id');
    }
}
