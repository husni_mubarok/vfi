<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cashbond extends Model
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];
    protected $table = 'cashbond';

    public function cashbond_detail()
	{
		return $this->hasMany('App\Model\CashbondDetail', 'cashbond_id', 'id');
	}

	public function branch()
	{
		return $this->belongsTo('App\Model\Branch', 'branch_id', 'id');
	}
}



