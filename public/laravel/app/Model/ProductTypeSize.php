<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductTypeSize extends Model
{
	protected $table = 'product_types_sizes';
    protected $dates = ['deleted_at'];

    public function product()
    {
    	return $this->belongsTo('App\Model\Product_Types', 'product_type_id', 'id');
    }

    public function size()
    {
    	return $this->belongsTo('App\Model\Size', 'size_id', 'id');
    }
}
