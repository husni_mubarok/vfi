<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Uom extends Model
{
    protected $table = 'uom';
    protected $dates = ['deleted_at'];

    public function uom_category()
    {
    	return $this->belongsTo('App\Model\UomCategory', 'uom_category_id', 'id');
    }

    public function parent_uom()
    {
    	return $this->belongsTo('App\Model\Uom', 'parent_id', 'id');
    }

    public function child_uom()
    {
    	return $this->hasMany('App\Model\Uom', 'parent_id', 'id');
    }

    public function convert($uom, $value)
    {
        // if($this->parent_uom)
        // {
        //     $parent_value = $value / $
        //     $current_uom = $this->parent_uom;
        // }

        // $desired_uom = Uom::where('name', $uom)->first();
        // return $this->parent_uom->value * $desired_uom->value;
    }
}
