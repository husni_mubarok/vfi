<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TallyRecToday extends Model
{
    protected $table = 'tally_rectoday';
	protected $dates = ['deleted_at'];

	public function purchase()
	{
		return $this->hasMany('App\Model\Purchase', 'purchase_id', 'id');
	} 
}
