<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductionResultAddition extends Model
{
    use SoftDeletes;
    
    protected $table = 'production_result_additions';
    protected $dates = ['deleted_at'];

    public function storage()
    {
    	return $this->belongsTo('App\Model\TempStorage', 'id', 'source_id');
    }

    public function production_result()
    {
    	return $this->belongsTo('App\Model\ProductionResult', 'production_result_id', 'id');
    }

    public function reject_type_func()
    {
    	return $this->hasOne('App\Model\RejectType', 'id', 'reject_type_id');
    }

    public function reject_reason_func()
    {
    	return $this->hasOne('App\Model\RejectReason', 'id', 'reject_reason_id');
    }

    public function from()
    {
        return $this->hasMany('App\Model\AdditionTransactionHistory', 'from_id', 'id');
    }

    public function to()
    {
        return $this->hasMany('App\Model\AdditionTransactionHistory', 'to_id', 'id');
    }
}
