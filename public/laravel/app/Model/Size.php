<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Size extends Model
{
	protected $table = 'sizes';
    protected $dates = ['deleted_at'];

    public function product_size()
    {
    	return $this->hasMany('App\Model\ProductTypeSize', 'size_id', 'id');
    }

    public function production_result_size()
    {
        return $this->hasMany('App\Model\ProductionResultSize', 'size_id', 'id');
    }

    public function price()
    {
    	return $this->hasMany('App\Model\Price', 'id', 'size_id');
    }
}
