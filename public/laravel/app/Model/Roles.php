<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Roles extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'roles';

	public function user()
	{
		return $this->hasOne('App\User', 'id', 'id_users');
	}

	public function modules()
    {
    	return $this->hasOne('App\Model\Modules', 'id', 'id_modules');
    }

    public function actions()
    {
        return $this->hasOne('App\Model\Actions', 'id', 'id_actions');
    }
}
