<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DefaultRoles extends Model
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];
	protected $table = 'default_roles';

	public function privilege()
	{
		return $this->hasOne('App\Model\Privilege', 'id', 'id_privilege');
	}

  public function modules()
	{
		return $this->hasOne('App\Model\Modules', 'id', 'id_modules');
	}

  public function actions()
	{
		return $this->hasOne('App\Model\Actions', 'id', 'id_actions');
	}

}
