<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sashimi extends Model
{
   protected $table = 'sashimi';
   protected $dates = ['deleted_at'];

   public function production_result()
   {
       return $this->belongsTo('App\Model\ProductionResult', 'production_result_id', 'id');
   }

    public function sashimi_bom()
    {
    	return $this->hasMany('App\Model\SashimiBOM', 'sashimi_id', 'id');
    }
}
