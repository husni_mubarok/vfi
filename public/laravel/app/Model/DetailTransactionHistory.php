<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetailTransactionHistory extends Model
{
    protected $table = 'detail_transaction_history';
    protected $dates = ['deleted_at'];

    public function from()
    {
    	return $this->belongsTo('App\Model\ProductionResultDetail', 'from_id', 'id');
    }

    public function to()
    {
    	return $this->belongsTo('App\Model\ProductionResultDetail', 'to_id', 'id');
    }
}
