<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SashimiResult extends Model
{
    protected $table = 'sashimi_result';
    protected $dates = ['deleted_at'];

    public function bom()
    {
    	return $this->belongsTo('App\Model\SashimiBOM', 'sashimi_bom_id', 'id');
    }
}
