<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
	use SoftDeletes;
	protected $table = 'item';
	protected $dates = ['deleted_at'];

	public function master_item()
	{
		return $this->belongsTo('App\Model\MasterItem', 'id_master_item', 'id');
	}

	public function item_custom_detail()
	{
		return $this->hasMany('App\Model\ItemCustomDetail', 'id_item', 'id');
	}

	public function production_ingredient()
	{
		return $this->hasMany('App\Model\ProductionIngredient', 'id_item', 'id');
	}

	public function production_result_ingredient()
	{
		return $this->hasMany('App\Model\ProductionResultIngredient', 'id_item', 'id');
	}

	public function recipe()
	{
		return $this->hasMany('App\Model\RecipeItem', 'item_id', 'id');
	}

	public function detail_uom()
	{
		return $this->hasMany('App\Model\DetailUom', 'id_item', 'id');
	}
}
