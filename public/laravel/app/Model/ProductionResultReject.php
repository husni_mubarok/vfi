<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductionResultReject extends Model
{
    protected $table = 'production_result_rejects';
    protected $dates = ['deleted_at'];

    public function production_result()
    {
    	return $this->belongsTo('App\Model\ProductionResult', 'production_result_id', 'id');
    }

    public function reject_reason()
    {
    	return $this->belongsTo('App\Model\RejectReason', 'reject_reason_id', 'id');
    }
}
