<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SystemOption extends Model
{
    protected $table = 'system_options';
    protected $dates = ['deleted_at'];
}
