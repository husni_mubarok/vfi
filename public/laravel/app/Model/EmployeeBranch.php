<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EmployeeBranch extends Model
{
    protected $table = 'employee_branch';

    public function employee()
	{
		return $this->belongsTo('App\Model\Employee', 'employee_id', 'id');
	}

	public function branch()
	{
		return $this->belongsTo('App\Model\Branch', 'branch_id', 'id');
	}
 
}



