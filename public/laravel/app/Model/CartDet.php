<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CartDet extends Model
{
    use SoftDeletes;
    protected $table = 'cartdet';
    protected $dates = ['deleted_at'];

	public function cart()
	{
		return $this->belongsTo('App\Model\Cart', 'id_cart', 'id');
	}

	public function production_result()
	{
		return $this->belongsTo('App\Model\ProductionResult', 'source_id', 'id');
	}
}
