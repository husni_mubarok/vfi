<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SourceStorage extends Model
{
  protected $table = 'source_storage';
  protected $dates = ['deleted_at'];
}
