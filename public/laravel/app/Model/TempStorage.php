<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class TempStorage extends Model
{
  protected $table = 'temp_storage';
  protected $dates = ['deleted_at'];




  public function production_result()
  {
    return $this->hasOne('App\Model\ProductionResult', 'id', 'production_id');
  }


  public function production_result_detail()
  {
      return $this->hasMany('App\Model\ProductionResultDetail', 'production_result_id', 'production_id');
  }

  public function production_result_addition()
  {
      return $this->hasMany('App\Model\ProductionResultAddition', 'production_result_id', 'production_id');
  }

  public function source()
  {
    if($this->source_type == 1)
    {
      return $this->belongsTo('App\Model\ProductionResultDetail', 'source_id', 'id');
    }
    elseif($this->source_type == 2)
    {
      return $this->belongsTo('App\Model\ProductionResultAddition', 'source_id', 'id');
    }
    elseif($this->source_type == 3)
    {
      return $this->belongsTo('App\Model\ProductionExport', 'source_id', 'id');
    }
    elseif($this->source_type == 4)
    {
      return $this->belongsTo('App\Model\SashimiResult', 'source_id', 'id');
    }
  }


}
