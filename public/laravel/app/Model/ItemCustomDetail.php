<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemCustomDetail extends Model
{
	use SoftDeletes;
	protected $table = 'item_custom_detail';
	protected $dates = ['deleted_at'];

	public function item()
	{
		return $this->belongsTo('App\Model\Item');
	}

	public function custom_detail()
	{
		return $this->belongsTo('App\Model\CustomDetail', 'id_custom_detail', 'id');
	}
}
