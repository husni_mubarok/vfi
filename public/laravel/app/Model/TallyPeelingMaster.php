<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TallyPeelingMaster extends Model
{
	use SoftDeletes;
	protected $table = 'tally_peeling_master';
	protected $dates = ['deleted_at']; 
}
