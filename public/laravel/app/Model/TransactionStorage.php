<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransactionStorage extends Model
{
	use SoftDeletes;
  protected $table = 'transaction_storage';
  protected $dates = ['deleted_at'];

  public function trasaction()
  {
  	return $this->belongsTo('App\Model\Transaction', 'id_transaction', 'id');
  }
}
