<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductionResult extends Model
{
    use SoftDeletes;
    
    protected $table = 'production_results';
    protected $dates = ['deleted_at'];

    public function production()
    {
    	return $this->belongsTo('App\Model\Production', 'production_id', 'id');
    }

    public function purchase_func()
    {
        return $this->hasOne('App\Model\Purchase', 'production_id', 'id');
    }

    public function purchase()
    {
        return $this->belongsTo('App\Model\Purchase', 'purchase_id', 'id');
    }

    public function storage()
    {

    	return $this->belongsTo('App\Model\TempStorage');
    }

    public function cart()
    {
        if($this->cart_id > 0)
        {
            return $this->belongsTo('App\Model\Cart', 'cart_id', 'id');
        } else {
            return null;
        }
    }

    // public function raw_material()
    // {
    // 	return $this->belongsTo('App\Model\RawMaterial', 'id', 'raw_material_id');
    // }

    public function waste()
    {
        return $this->hasMany('App\Model\ProductionResultWaste', 'production_result_id', 'id');
    }

    public function production_result_variable()
    {
    	return $this->hasMany('App\Model\ProductionResultVariable', 'production_result_id', 'id');
    }

    public function production_result_detail()
    {
        return $this->hasMany('App\Model\ProductionResultDetail', 'production_result_id', 'id');
    }

    public function production_result_addition()
    {
        return $this->hasMany('App\Model\ProductionResultAddition', 'production_result_id', 'id');
    }

    public function production_result_reject()
    {
        return $this->hasMany('App\Model\ProductionResultReject', 'production_result_id', 'id');
    }

    public function reject()
    {
    	return $this->hasOne('App\Model\Reject', 'production_result_id', 'id');
    }

    public function production_result_prepared_product()
    {
        return $this->hasOne('App\Model\ProductionResultPreparedProduct', 'production_rslt_id', 'id');
    }
}
