<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemCentralImport extends Model
{
	use SoftDeletes;
	protected $table = 'item_central_import';
	protected $dates = ['deleted_at'];
 
}
