<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class InvoiceDetail extends Model
{
    protected $table = 'invoice_detail';

    public function invoice()
	{
		return $this->belongsTo('App\Model\Invoice', 'invoice_id', 'id');
	}
}



