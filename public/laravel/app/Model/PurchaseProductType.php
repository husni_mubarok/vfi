<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseProductType extends Model
{
	protected $table = 'purchases_product_types';
    protected $dates = ['deleted_at'];

    public function purchase()
    {
    	return $this->belongsTo('App\Model\Purchase', 'purchase_id', 'id');
    }

    public function product_type()
    {
    	return $this->belongsTo('App\Model\ProductType', 'product_type_id', 'id');
    }
}
