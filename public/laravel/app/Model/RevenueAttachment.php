<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RevenueAttachment extends Model
{
    use SoftDeletes;

    protected $table = 'revenue_attachment';
    protected $dates = ['deleted_at'];

    public function revenue()
    {
    	return $this->belongsTo('App\Model\Revenue', 'revenue_id', 'id');
    }
}
