<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TallyDeheadingMaster extends Model
{
	use SoftDeletes;
	protected $table = 'tally_deheading_master';
	protected $dates = ['deleted_at']; 
}
