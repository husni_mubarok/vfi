<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseSupplier extends Model
{
	protected $table = 'purchases_suppliers';
    protected $dates = ['deleted_at'];

    public function purchase()
    {
    	return $this->belongsTo('App\Model\Purchase', 'purchase_id', 'id');
    }

    public function supplier()
    {
    	return $this->belongsTo('App\Model\Supplier', 'supplier_id', 'id');
    }
}
