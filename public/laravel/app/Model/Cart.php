<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cart extends Model
{
    use SoftDeletes;
    protected $table = 'cart';
    protected $dates = ['deleted_at'];

    public function production_result()
    {
    	return $this->hasOne('App\Model\ProductionResult', 'cart_id', 'id');
    }

    public function detail()
    {
    	return $this->hasMany('App\Model\CartDet', 'id_cart', 'id');
    }

    public function cart_customer()
    {
        return $this->hasOne('App\Model\Customer', 'customer_id', 'id');
    }

    public function export()
    {
        return $this->hasOne('App\Model\ProductionExport', 'cart_id', 'id');
    }
}
