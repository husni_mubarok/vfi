<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RejectReason extends Model
{
    protected $table = 'reject_reasons';
    protected $dates = ['deleted_at'];

    public function reject_reject_reason()
    {
    	return $this->hasMany('App\Model\RejectRejectReason', 'reject_reason_id', 'id');
    }

    public function production_result_reject()
    {
    	return $this->hasMany('App\Model\ProductionResultReject', 'reject_reason_id', 'id');
    }
}
