<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MPrepProductIngredient extends Model
{
    protected $table = 'm_prep_products_ingredients';
    protected $dates = ['deleted_at'];

    public function m_prep_product()
    {
    	return $this->belongsTo('App\Model\MPrepProduct', 'm_prep_product_id', 'id');
    }

    public function ingredient()
    {
    	return $this->belongsTo('App\Model\Ingredient', 'ingredient_id', 'id');
    }
}
