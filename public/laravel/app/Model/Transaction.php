<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class Transaction extends Model
{
  protected $table = 'transaction';
  protected $dates = ['deleted_at'];

  public function transaction_storage()
  {
      return $this->hasMany('App\Model\TransactionStorage', 'id_transaction', 'id');
  }

}
