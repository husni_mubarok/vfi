<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemCentral extends Model
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];
    protected $table = 'item_central';

    public function item_central_detail()
	{
		return $this->hasMany('App\Model\ItemCentralDetail', 'item_central_id', 'id');
	}

	public function branch()
	{
		return $this->belongsTo('App\Model\Branch', 'branch_id', 'id');
	}
}



