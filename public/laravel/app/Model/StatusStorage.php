<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StatusStorage extends Model
{
  protected $table = 'status_storage';
  protected $dates = ['deleted_at'];
}
