<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
	protected $table = 'item';
	protected $dates = ['deleted_at'];

	public function master_item()
	{
		return $this->belongsTo('App\Model\MasterItem', 'id_master_item', 'id');
	}

	public function item_custom_detail(){
		return $this->hasMany('App\Model\ItemCustomDetail', 'id_item', 'id');
	}

}
