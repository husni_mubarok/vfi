<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Privilege extends Model
{
  use SoftDeletes;
	protected $dates = ['deleted_at'];
	protected $table = 'privileges';

  public function user()
  {
      return $this->belongsTo('App\Model\User');
  }
}
