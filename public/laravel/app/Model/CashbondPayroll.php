<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CashbondPayroll extends Model
{
    use SoftDeletes;

    protected $table = 'cashbond_payroll';
    protected $dates = ['deleted_at'];

    public function cashbond_payroll_detail()
    {
    	return $this->hasMany('App\Model\CashbondPayrollDetail', 'cashbond_payroll_id', 'id');
    }
}
