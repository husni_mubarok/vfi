<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CodeSize extends Model
{
	use SoftDeletes;
	protected $table = 'code_size';
	protected $dates = ['deleted_at'];

}