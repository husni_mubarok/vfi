<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemUom extends Model
{
	use SoftDeletes;
	protected $table = 'detail_uom';
	protected $dates = ['deleted_at'];

	public function master_item()
	{
		return $this->belongsTo('App\Model\MasterItem', 'id_master_item', 'id');
	}
}
