<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductionResultPreparedProduct extends Model
{
    protected $table = 'production_results_prepared_products';
    protected $dates = ['deleted_at'];

    public function production_result()
    {
    	return $this->belongsTo('App\Model\ProductionResult', 'production_rslt_id', 'id');
    }

    public function prepared_product()
    {
    	return $this->belongsTo('App\Model\PreparedProduct', 'prepared_product_id', 'id');
    }
}
