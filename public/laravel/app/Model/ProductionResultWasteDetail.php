<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductionResultWasteDetail extends Model
{
    use SoftDeletes;

    protected $table = 'production_result_waste_detail';
    protected $dates = ['deleted_at'];

    public function waste()
    {
    	return $this->belongsTo('App\Model\ProductionResultWaste', 'waste_id', 'id');
    }
}
