<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductionResultSize extends Model
{
    protected $table = 'production_results_sizes';
    protected $dates = ['deleted_at'];

    public function production_result()
    {
    	return $this->belongsTo('App\Model\ProductionResult', 'production_result_id', 'id');
    }

    public function size()
    {
    	return $this->belongsTo('App\Model\Size', 'size_id', 'id');
    }
}
