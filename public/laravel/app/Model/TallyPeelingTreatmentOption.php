<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TallyPeelingTreatmentOption extends Model
{
    protected $table = 'tally_peeling_treatment_option';
	protected $dates = ['deleted_at'];

}
