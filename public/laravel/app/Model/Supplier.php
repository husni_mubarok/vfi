<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supplier extends Model
{
	protected $table = 'suppliers';
    protected $dates = ['deleted_at'];

    public function purchase_supplier()
    {
    	return $this->hasMany('App\Model\PurchaseSupplier', 'supplier_id', 'id');
    }

    public function supplier_product()
    {
    	return $this->hasMany('App\Model\SupplierProduct', 'supplier_id', 'id');
    }
}
