<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductSize extends Model
{
	protected $table = 'products_sizes';
    protected $dates = ['deleted_at'];

    public function product()
    {
    	return $this->belongsTo('App\Model\Product', 'product_id', 'id');
    }

    public function size()
    {
    	return $this->belongsTo('App\Model\Size', 'size_id', 'id');
    }
}
