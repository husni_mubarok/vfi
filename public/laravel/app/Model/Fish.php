<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fish extends Model
{
    protected $table = 'fishes';
    protected $dates = ['deleted_at'];

    // public function raw_material()
    // {
    // 	return $this->hasMany('App\Model\RawMaterial', 'fish_id', 'id');
    // }

    public function production()
    {
    	return $this->hasMany('App\Model\Production', 'fish_id', 'id');
    }

    public function product_fish()
    {
        return $this->hasMany('App\Model\ProductFish', 'fish_id', 'id');
    }
}
