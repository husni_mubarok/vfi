<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QtyType extends Model
{
	use SoftDeletes;
	protected $table = 'qty_type';
	protected $dates = ['deleted_at']; 
}