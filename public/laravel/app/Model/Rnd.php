<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rnd extends Model
{
    protected $table = 'rnd';
    protected $dates = ['deleted_at'];

    public function productionresult()
    {
    	return $this->belongsTo('App\Model\ProductionResult', 'storage_id', 'id');
    }

    public function cart()
    {
    	return $this->belongsTo('App\Model\Cart', 'cart_id', 'id');
    }
}
