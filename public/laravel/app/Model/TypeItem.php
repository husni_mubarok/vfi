<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeItem extends Model
{
	use SoftDeletes;
	protected $table = 'type_item';
	protected $dates = ['deleted_at'];

	public function master_item()
	{
		return $this->hasMany('app\Model\MasterItem', 'id_type_item', 'id');
	}

}
