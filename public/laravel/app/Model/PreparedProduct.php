<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PreparedProduct extends Model
{
    protected $table = 'prepared_products';
    protected $dates = ['deleted_at'];

    public function m_prep_product()
    {
    	return $this->belongsTo('App\Model\MPrepProduct', 'm_prep_product_id', 'id');
    }

    public function production()
    {
    	return $this->belongsTo('App\Model\Production', 'production_id', 'id');
    }

    public function size()
    {
    	return $this->belongsTo('App\Model\Size', 'size_id', 'id');
    }

    public function prepared_product_detail()
    {
        return $this->hasMany('App\Model\PreparedProductDetail', 'prep_prod_id', 'id');
    }

    public function production_result_prepared_product()
    {
        return $this->hasOne('App\Model\ProductionResultPreparedProduct', 'prepared_product_id', 'id');
    }
}
