<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseProduct extends Model
{
	protected $table = 'purchases_products';
    protected $dates = ['deleted_at'];

    public function purchase()
    {
    	return $this->belongsTo('App\Model\Purchase', 'purchase_id', 'id');
    }

    public function product()
    {
    	return $this->belongsTo('App\Model\Product', 'product_id', 'id');
    }
}
