<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdditionTransactionHistory extends Model
{
    protected $table = 'addition_transaction_history';
    protected $dates = ['deleted_at'];

    public function from()
    {
    	return $this->belongsTo('App\Model\ProductionResultAddition', 'from_id', 'id');
    }

    public function to()
    {
    	return $this->belongsTo('App\Model\ProductionResultAddition', 'to_id', 'id');
    }
}
