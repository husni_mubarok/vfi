<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RecipeItem extends Model
{
    protected $table = 'recipes_items';
    protected $dates = ['deleted_at'];

    public function recipe()
    {
    	return $this->belongsTo('App\Model\ProductionRecipe', 'recipe_id', 'id');
    }

    public function item()
    {
    	return $this->belongsTo('App\Model\Item', 'item_id', 'id');
    }

    public function detail_uom()
    {
        return $this->belongsTo('App\Model\DetailUom', 'detail_uom_id', 'id');
    }
}
