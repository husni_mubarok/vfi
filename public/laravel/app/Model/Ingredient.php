<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ingredient extends Model
{
    protected $table = 'ingredients';
    protected $dates = ['deleted_at'];

    public function master_prepared_product_ingredient()
    {
    	return $this->hasMany('App\Model\MasterPreparedProductIngredient', 'ingredient_id', 'id');
    }

    public function prep_prod_detail_ingredient()
    {
    	return $this->hasMany('App\Model\PrepProdDetailIngredient', 'ingredient_id', 'id');
    }
}
