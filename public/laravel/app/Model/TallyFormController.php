<?php

namespace App\Http\Controllers\Home; 
use DB;
use Illuminate\Http\Request; 
use App\Http\Requests\TallyRequest;
use App\Http\Controllers\Controller;
use App\Repository\PurchaseRepository;
use App\Repository\ProductRepository;
use App\Repository\SupplierRepository;
use App\Repository\PriceRepository;
use App\Repository\PurchaseDetailRepository;
use App\Model\Purchase;
use App\Model\Size;

//Temp Tally
use App\Model\TempTally;

//Receiving
use App\Model\Tally;
use App\Model\TallyDetail;
use App\Model\TallyAdditionalMaster; 
use App\Model\TallyDetailAdditional;
use App\Model\TallyReceivingTreatment;

//Heading
use App\Model\TallyDeheading;
use App\Model\TallyDetailDeheading;
use App\Model\TallyDeheadingMaster;

//Peeling
use App\Model\TallyPeeling;
use App\Model\TallyDetailPeeling;
use App\Model\TallyPeelingMaster;
use App\Model\TallyPeelingTreatment;

//Grading
use App\Model\TallyGradingMaster;
use App\Model\TallyDetailGrading; 
use App\Model\TallyGrading;
use App\Model\CodeSize;
use App\Model\FreezingType;
use App\Model\Grade;
use App\Model\QtyType;

//Packing
use App\Model\TallyPackingDetail;

use Auth;
use App\Model\User;

class TallyFormController extends Controller
{
  protected $PurchaseRepository;
  protected $ProductRepository;
  protected $SupplierRepository;
  protected $PriceRepository;
  protected $PurchaseDetailRepository;

  public function __construct(PurchaseRepository $purchase_repository, ProductRepository $product_repository, SupplierRepository $supplier_repository, PriceRepository $price_repository, PurchaseDetailRepository $purchase_detail_repository)
  {
    $this->PurchaseRepository = $purchase_repository;
    $this->ProductRepository = $product_repository;
    $this->SupplierRepository = $supplier_repository;
    $this->PriceRepository = $price_repository;
    $this->PurchaseDetailRepository = $purchase_detail_repository;
  }

  public function index()
  {

    $last_temp_tally_rec =  DB::table('temp_tally')
    ->where('id', DB::raw("(SELECT
      Max(temp_tally.id) AS id 
      FROM
      temp_tally
      INNER JOIN purchases ON temp_tally.id_purchase = purchases.id
      WHERE
      temp_tally.`status` = 'active'
      AND (
      temp_tally.form_status = ''
      OR temp_tally.form_status IS NULL
      )
      AND MID(purchases.trans_code, 3, 1) = 'A')")) 
    ->first();

    $last_temp_tally_rec_fresh =  DB::table('temp_tally')
    ->where('id', DB::raw("(SELECT
      Max(temp_tally.id) AS id 
      FROM
      temp_tally
      INNER JOIN purchases ON temp_tally.id_purchase = purchases.id
      WHERE
      temp_tally.`status` = 'active'
      AND (
      temp_tally.form_status = 'receiving'
      OR temp_tally.form_status IS NULL
      )
      AND MID(purchases.trans_code, 3, 1) = 'A')")) 
    ->first();  



    if(isset($last_temp_tally_rec_fresh))
    { 
      $last_temp_tally_headsplit =  DB::table('temp_tally')
      ->where('id', DB::raw("(select max(`id`) from temp_tally WHERE status='active' AND form_status='receiving')")) 
      ->first(); 
    }else{
      $last_temp_tally_headsplit =  DB::table('temp_tally')
      ->where('id', DB::raw("(select max(`id`) from temp_tally WHERE status='active' AND form_status='' OR temp_tally.form_status IS NULL)")) 
      ->first(); 

    };

        //dd($last_temp_tally_headsplit);

    $last_temp_tally_peeling =  DB::table('temp_tally')
    ->where('id', DB::raw("(select max(`id`) from temp_tally WHERE status='active' AND form_status='headsplit')")) 
    ->first(); 

    $last_temp_tally_treatment =  DB::table('temp_tally')
    ->where('id', DB::raw("(select max(`id`) from temp_tally WHERE status='active' AND form_status='peeling')")) 
    ->first(); 

    $last_temp_tally_grading =  DB::table('temp_tally')
    ->where('id', DB::raw("(select max(`id`) from temp_tally WHERE status='active' AND form_status='treatment')")) 
    ->first();

    $last_temp_tally_packing =  DB::table('temp_tally')
    ->where('id', DB::raw("(select max(`id`) from temp_tally WHERE status='active' AND form_status='grading')")) 
    ->first();  

        // session_start();
        // $user = User::where('username', $_SESSION['dummy_vffiM_name'])->first();

        // dd($user);

       // dd($last_temp_tally_packing);

     $last_packing =  DB::table('temp_tally')
    ->where('id', DB::raw("(select max(`id`) from temp_tally WHERE status='active' AND form_status='packing')")) 
    ->first();  

    dd($last_packing);

    $tally_packing = TallyPackingDetail::where('temp_tally_id', $last_packing);

    return view ('home.tally_form.index', compact('last_temp_tally_rec', 'last_temp_tally_headsplit', 'last_temp_tally_peeling', 'last_temp_tally_grading', 'last_temp_tally_packing', 'tally_packing'));
  }

    //RECEIVING 
  public function createreceiving($id)
  { 

   $temp_tally = DB::table('temp_tally') 
   ->where('id', $id) 
   ->first();  

        //dd($temp_tally);

       // $tally_detail = DB::table('product_types')
       //  ->join('product_types_sizes', 'product_types_sizes.product_type_id', '=', 'product_types.id')
       //  ->join('sizes', 'product_types_sizes.size_id', '=', 'sizes.id')
       //  ->join('products', 'products.product_type_id', '=', 'product_types.id')
       //  ->leftjoin('purchases_products', 'purchases_products.product_id', '=', 'products.id')
       //  ->leftjoin('temp_tally', 'purchases_products.product_id', '=', 'temp_tally.id_purchase')
       //  ->leftjoin('tally_detail', 'temp_tally.id', '=', 'tally_detail.temp_tally_id')
       //  ->select('product_types.name', 'sizes.note', 'sizes.id', DB::raw('0 as weight'), 'tally_detail.tally_type')
       //  ->where('purchases_products.purchase_id', $id)
       //  ->groupBy('product_types.name', 'sizes.note', 'sizes.id', 'tally_detail.weight', 'tally_detail.tally_type')
       //  ->orderBy('sizes.note2', 'ASC')
       //  ->get();

   $tally_detail_sql = 'SELECT
   product_types.name,
   sizes.note,
   sizes.id,
   0 AS weight 
   FROM
   product_types
   INNER JOIN product_types_sizes ON product_types_sizes.product_type_id = product_types.id
   INNER JOIN sizes ON product_types_sizes.size_id = sizes.id
   INNER JOIN products ON products.product_type_id = product_types.id
   INNER JOIN purchases_products ON purchases_products.product_id = products.id
   INNER JOIN temp_tally ON purchases_products.purchase_id = temp_tally.id_purchase 
   WHERE
   temp_tally.id = '.$id.'
   GROUP BY
   product_types.name,
   sizes.note,
   sizes.id 
   ORDER BY
   sizes.note ASC';

   $tally_detail = DB::table(DB::raw("($tally_detail_sql) as rs_tally_detail_sql"))->get();

       // dd($tally_detail);

   $size_beku_kapal = DB::table('sizes') 
   ->where('beku_kapal', 1) 
   ->get(); 

        //dd($size_beku_kapal);   

   $tally_additional = TallyAdditionalMaster::orderBy('name', 'ASC')->get();  

   return view ('home.tally_form.receiving', compact('tally_detail', 'temp_tally', 'tally_additional', 'size_beku_kapal'));
 }

 public function store($id, Request $request)
 { 
  $post = new TallyDetail();
  $post->tally_type = $request->tally_type;
  $post->size_id = $request->size_id;
  $post->weight = $request->weight;
  $post->purchase_id = $request->purchase_id;
  $post->temp_tally_id = $request->temp_tally_id;
  $post->tally_group = $request->tally_group;
  $post->save();
  return response()->json($post); 
}

public function storeadditional($id, Request $request)
{ 
  $post = new TallyDetailAdditional();
  $post->tally_type = "ADDITIONAL";
  $post->tally_additional_master_id = $request->tally_additional_master_id;
  $post->weight = $request->weight_add;
  $post->pan = $request->pan;
  $post->rest = $request->rest;
  $post->purchase_id = $request->purchase_id;
  $post->temp_tally_id = $request->temp_tally_id;
  $post->tally_group = $request->tally_group;
  $post->save();
  return response()->json($post); 
}

public function storereceivingtreatment($id, Request $request)
{ 
  $post = new TallyReceivingTreatment();  
  $post->temp_tally_id = $request->temp_tally_id;
  $post->salinity = $request->salinity;
  $post->duration = $request->duration;
  $post->save();
  return response()->json($post); 
}

public function closereceiving($id)
{   
  $purchase = TempTally::find($id);  
  $purchase->form_status = 'receiving'; 
  $purchase->save();

  return redirect()->action('Home\TallyFormController@index');
}


public function historyreceiving($id)
{  
 $temp_tally = DB::table('temp_tally') 
 ->where('id', $id) 
 ->first();

 $tally_detail_view_treatment = DB::table('tally_detail')
 ->join('sizes', 'tally_detail.size_id', '=', 'sizes.id')  
 ->select('tally_detail.size_id AS id', 'sizes.note', DB::raw('SUM(tally_detail.weight) as total_weight'))
 ->where('tally_detail.temp_tally_id', $id) 
 ->where('tally_detail.tally_type', 'TREATMENT')   
 ->groupBy('tally_detail.size_id', 'sizes.note')
 ->get(); 

 foreach ($tally_detail_view_treatment as $tdvt) {
   $tally_detail_view_treatment_detail = DB::table('tally_detail')
   ->join('sizes', 'tally_detail.size_id', '=', 'sizes.id')  
   ->select('tally_detail.size_id AS id', 'sizes.note', 'tally_detail.weight')
   ->where('tally_detail.temp_tally_id', $id) 
   ->where('tally_detail.size_id', '=', $tdvt->id)
   ->where('tally_detail.tally_type', 'TREATMENT')  
   ->get(); 
   $array_val["note"] = $tdvt->note;
   $array_val["total_weight"] = $tdvt->total_weight;
   $tdvtd_arrays = null;
   foreach ($tally_detail_view_treatment_detail as $tdvtd) {
    $tdvtd_array = null;
    $tdvtd_array["weight"] = $tdvtd->weight;
    $tdvtd_arrays[] = $tdvtd_array;
  }

  $array_val["detail"] = $tdvtd_arrays;
  $array_all_treatment[] = $array_val;
} 

$tally_detail_view_receiving = DB::table('tally_detail')
->join('sizes', 'tally_detail.size_id', '=', 'sizes.id')  
->select('tally_detail.size_id AS id', 'sizes.note', DB::raw('SUM(tally_detail.weight) as total_weight'))
->where('tally_detail.temp_tally_id', $id) 
->where('tally_detail.tally_type', 'RECEIVING') 
->groupBy('tally_detail.size_id', 'sizes.note')
->get(); 

foreach ($tally_detail_view_receiving as $tdvr) {
  $tally_detail_view_receiving_detail = DB::table('tally_detail')
  ->join('sizes', 'tally_detail.size_id', '=', 'sizes.id')  
  ->select('tally_detail.size_id AS id', 'sizes.note', 'tally_detail.weight')
  ->where('tally_detail.temp_tally_id', $id) 
  ->where('tally_detail.size_id', '=', $tdvr->id)
  ->where('tally_detail.tally_type', 'RECEIVING')  
  ->get(); 
  $array_val_rec["note"] = $tdvr->note;
  $array_val_rec["total_weight"] = $tdvr->total_weight;
  $tdvrd_arrays = null;
  foreach ($tally_detail_view_receiving_detail as $tdvrd) {
    $tdvrd_array = null;
    $tdvrd_array["weight"] = $tdvrd->weight;
    $tdvrd_arrays[] = $tdvrd_array;
  }

  $array_val_rec["detail"] = $tdvrd_arrays;
  $array_all_receiving[] = $array_val_rec;
}

$tally_additional_view = DB::table('tally_detail_additional')
->join('tally_additional_master', 'tally_detail_additional.tally_additional_master_id', '=', 'tally_additional_master.id') 
->join('temp_tally', 'tally_detail_additional.temp_tally_id', '=', 'temp_tally.id')
->select('tally_additional_master.id','tally_additional_master.name', DB::raw('SUM(tally_detail_additional.weight) as total_weight'), DB::raw('SUM(tally_detail_additional.pan) as total_pan'), DB::raw('SUM(tally_detail_additional.rest) as total_rest'))
->where('temp_tally.id', $id) 
->groupBy('tally_detail_additional.tally_additional_master_id', 'tally_additional_master.name')
->get();


$tally_additional_view_c = DB::table('tally_detail_additional')
->join('tally_additional_master', 'tally_detail_additional.tally_additional_master_id', '=', 'tally_additional_master.id')  
->select('tally_additional_master.id','tally_additional_master.name', DB::raw('COUNT(tally_detail_additional.rest) as rest_count'))
->where('tally_detail_additional.temp_tally_id', $id) 
->groupBy('tally_detail_additional.tally_additional_master_id', 'tally_additional_master.name')
->first();

foreach ($tally_additional_view as $tav) {
 $tally_additional_view_detail = DB::table('tally_detail_additional')
 ->join('tally_additional_master', 'tally_detail_additional.tally_additional_master_id', '=', 'tally_additional_master.id') 
 ->select('tally_detail_additional.id','tally_additional_master.name', 'tally_detail_additional.weight', 'tally_detail_additional.pan', 'tally_detail_additional.rest')
 ->where('tally_detail_additional.temp_tally_id', $id)  
 ->where('tally_additional_master.id', '=', $tav->id)  
 ->get();


 $array_val_add["id"] = $tav->id;
 $array_val_add["name"] = $tav->name;
 $array_val_add["total_weight"] = $tav->total_weight;
 $array_val_add["total_pan"] = $tav->total_pan;
 $array_val_add["total_rest"] = $tav->total_rest;
 $tavd_arrays = null;
 foreach ($tally_additional_view_detail as $tavd) {
  $tavd_array = null;
  $tavd_array["id"] = $tavd->id;
  $tavd_array["weight"] = $tavd->weight;
  $tavd_array["pan"] = $tavd->pan;
  $tavd_array["rest"] = $tavd->rest;
  $tavd_arrays[] = $tavd_array;
}

$array_val_add["detail"] = $tavd_arrays;
$array_all_add[] = $array_val_add;
}

$treatment_option = DB::table('tally_receiving_treatment') 
->where('temp_tally_id', $id) 
->get();

        //dd($array_all_add);

return view ('home.tally_form.historyreceiving', compact('tally_detail_view_treatment','tally_detail_view_receiving','tally_additional_view', 'purchase','array_all_treatment','array_all_receiving','array_all_add', 'tally_additional_view_c', 'temp_tally','treatment_option'));
}

    //DEHEADING
public function createdeheading($id)
{ 

  $temp_tally = DB::table('temp_tally') 
  ->where('id', $id) 
  ->first(); 

  $tally_detail = TallyDeheadingMaster::all();  

  $tally = Tally::where('purchase_id', $id)->first();  

  return view ('home.tally_form.deheading', compact('tally_detail', 'temp_tally'));
}

public function storedeheading($id, Request $request)
{ 
  $post = new TallyDetailDeheading(); 
  $post->tally_dehading_master_id = $request->tally_dehading_master_id;
  $post->weight = $request->weight;
  $post->waste = $request->waste;
  $post->purchase_id = $request->purchase_id;
  $post->temp_tally_id = $request->temp_tally_id;
  $post->save();
  return response()->json($post); 
}



public function closedeheading($id)
{   
  $purchase = TempTally::find($id);  
  $purchase->form_status = 'headsplit'; 
  $purchase->save();

  return redirect()->action('Home\TallyFormController@index');
}

public function historydeheading($id)
{ 

 $temp_tally = DB::table('temp_tally') 
 ->where('id', $id) 
 ->first();
 
 $tally_detail = TallyDeheadingMaster::all(); 

 $tally_detail_view = DB::table('tally_detail_deheading')
 ->join('tally_deheading_master', 'tally_detail_deheading.tally_dehading_master_id', '=', 'tally_deheading_master.id')  
 ->select('tally_deheading_master.id', 'tally_deheading_master.name', DB::raw('SUM(tally_detail_deheading.weight) as total_weight'), DB::raw('SUM(tally_detail_deheading.waste) as total_waste'))
 ->where('tally_detail_deheading.temp_tally_id', $id) 
 ->groupBy('tally_deheading_master.id', 'tally_deheading_master.name')
 ->get(); 

        //dd($tally_detail_view);

 $tally_detail_view_count = DB::table('tally_detail_deheading')
 ->join('tally_deheading_master', 'tally_detail_deheading.tally_dehading_master_id', '=', 'tally_deheading_master.id')  
 ->select('tally_deheading_master.id', 'tally_deheading_master.name', DB::raw('COUNT(tally_detail_deheading.weight) as count_weight'))
 ->where('tally_detail_deheading.temp_tally_id', $id) 
 ->groupBy('tally_deheading_master.id', 'tally_deheading_master.name')
 ->first();

        //dd($tally_detail_view_count->id);
 foreach ($tally_detail_view as $t_d_v) {
   $tally_detail_view_det = DB::table('tally_detail_deheading')
   ->join('tally_deheading_master', 'tally_detail_deheading.tally_dehading_master_id', '=', 'tally_deheading_master.id')  
   ->select('tally_deheading_master.id', 'tally_deheading_master.name', 'tally_detail_deheading.weight', 'tally_detail_deheading.waste')
   ->where('tally_detail_deheading.temp_tally_id', $id)  
   ->where('tally_deheading_master.id', '=', $t_d_v->id)
   ->get(); 

                        //dd($tally_detail_view_det);

   $array_val["name"] = $t_d_v->name;
   $array_val["total_weight"] = $t_d_v->total_weight;
   $array_val["total_waste"] = $t_d_v->total_waste;
   $tdvtd_arrays = null;
   foreach ($tally_detail_view_det as $tdvtd) {
    $tdvtd_array = null;
    $tdvtd_array["weight"] = $tdvtd->weight;
    $tdvtd_array["waste"] = $tdvtd->waste;
    $tdvtd_arrays[] = $tdvtd_array;
  }

  $array_val["detail"] = $tdvtd_arrays;
  $array_all[] = $array_val;

            //dd($array_all);
}

return view ('home.tally_form.historydeheading', compact('tally_detail', 'tally_detail_view', 'temp_tally', 'array_all', 'tally_detail_view_count'));
}

    //PEELING
public function createpeeling($id)
{ 

  $temp_tally = DB::table('temp_tally') 
  ->where('id', $id) 
  ->first(); 

  $tally_detail = TallyPeelingMaster::all(); 

  $tally = Tally::where('purchase_id', $id)->first();  

  return view ('home.tally_form.peeling', compact('tally_detail', 'temp_tally'));
}

public function storepeeling($id, Request $request)
{ 
  $post = new TallyDetailPeeling(); 
  $post->tally_peeling_master_id = $request->tally_peeling_master_id;
  $post->weight = $request->weight; 
  $post->purchase_id = $request->purchase_id;
  $post->temp_tally_id = $request->temp_tally_id;
  $post->save();
  return response()->json($post); 
}

public function storepeelingtreatment($id, Request $request)
{ 
  $post = new TallyPeelingTreatment();  
  $post->temp_tally_id = $request->temp_tally_id;
  $post->salinity = $request->salinity;
  $post->duration = $request->duration;
  $post->save();
  return response()->json($post); 
}

public function closepeelingtreatment($id)
{   
  $purchase = TempTally::find($id);  
  $purchase->form_status = 'treatment'; 
  $purchase->save();

  return redirect()->action('Home\TallyFormController@index');
}

public function closepeeling($id)
{   
  $purchase = TempTally::find($id);  
  $purchase->form_status = 'peeling'; 
  $purchase->save();

  return redirect()->action('Home\TallyFormController@index');
}

public function historypeeling($id)
{ 

  $temp_tally = DB::table('temp_tally') 
  ->where('id', $id) 
  ->first();

  $tally_detail_view = DB::table('tally_detail_peeling')
  ->join('tally_peeling_master', 'tally_detail_peeling.tally_peeling_master_id', '=', 'tally_peeling_master.id')  
  ->select('tally_detail_peeling.tally_peeling_master_id AS id', 'tally_peeling_master.name', DB::raw('SUM(tally_detail_peeling.weight) as total_weight'))
  ->where('tally_detail_peeling.temp_tally_id', $id) 
  ->groupBy('tally_detail_peeling.tally_peeling_master_id', 'tally_peeling_master.name')
  ->get(); 

  foreach ($tally_detail_view as $t_d_v) {
   $tally_detail_view = DB::table('tally_detail_peeling')
   ->join('tally_peeling_master', 'tally_detail_peeling.tally_peeling_master_id', '=', 'tally_peeling_master.id')  
   ->select('tally_detail_peeling.tally_peeling_master_id AS id', 'tally_peeling_master.name', 'tally_detail_peeling.weight')
   ->where('tally_detail_peeling.temp_tally_id', $id)  
   ->where('tally_detail_peeling.tally_peeling_master_id', '=', $t_d_v->id)
   ->get(); 
   $array_val["name"] = $t_d_v->name;
   $array_val["total_weight"] = $t_d_v->total_weight;
   $tdvtd_arrays = null;
   foreach ($tally_detail_view as $tdvtd) {
    $tdvtd_array = null;
    $tdvtd_array["weight"] = $tdvtd->weight;
    $tdvtd_arrays[] = $tdvtd_array;
  }

  $array_val["detail"] = $tdvtd_arrays;
  $array_all[] = $array_val;
}

$treatment_option = DB::table('tally_peeling_treatment') 
->where('temp_tally_id', $id) 
->get();

  //dd($treatment_option);

return view ('home.tally_form.historypeeling', compact('tally_detail_view', 'purchase', 'array_all', 'temp_tally', 'treatment_option'));
}

public function creategrading($id)
{
 $temp_tally = DB::table('temp_tally') 
 ->where('id', $id) 
 ->first(); 

 $tally = Tally::where('purchase_id', $id)->first();

 $grades = Grade::orderBy('name', 'ASC')->get();

        // $code_sizes = CodeSize::all();

 $code_sizes = DB::table('code_size')
 ->select('code_size.id', 'code_size.name', 'code_size.block_weight', 'sizes.note', 'freezing_type.name AS freezing_name')
 ->join('sizes', 'sizes.id', '=', 'code_size.id_size')
 ->join('freezing_type', 'freezing_type.id', '=', 'code_size.id_freezing_type')
 ->orderBy('code_size.name', 'ASC')
 ->get();

 $qty_types = QtyType::all();

 $grading_view_sql = '
 SELECT
 purchases.trans_code AS trans_code,
 qty_type.name AS qty_name,
 grade.name AS grade,
 sizes.note AS size,
 freezing_type.name AS freezing,
 code_size.name AS code_size,
 SUM(code_size.block_weight) AS block,
 tally_grading_master.id AS tally_grading_master_id,
 tally_grading_master.purchase_id AS grading_purchase_id,
 tally_grading_detail.id AS tally_grading_detail_id,
 SUM(tally_grading_detail.total_block) AS total_block,
 SUM(tally_grading_detail.rest) AS rest
 FROM
 tally_grading_master
 INNER JOIN tally_grading_detail ON tally_grading_detail.id_tally_grading_master = tally_grading_master.id
 LEFT JOIN grade ON tally_grading_detail.id_grade = grade.id
 LEFT JOIN code_size ON code_size.id = tally_grading_detail.id_code_size
 LEFT JOIN sizes ON code_size.id_size = sizes.id
 LEFT JOIN freezing_type ON code_size.id_freezing_type = freezing_type.id
 LEFT JOIN qty_type ON tally_grading_detail.id_qty_type = qty_type.id
 INNER JOIN purchases ON tally_grading_master.purchase_id = purchases.id
 GROUP BY 
 purchases.trans_code,
 qty_type.name,
 grade.name,
 sizes.note,
 freezing_type.name,
 code_size.name
 ORDER BY grade.name ASC';

 $grading_view = DB::table(DB::raw("($grading_view_sql) as rs_grading_view_sql"))->where('grading_purchase_id', $id)->get();

 $grading_history_sql = '
 SELECT
 purchases.trans_code AS trans_code,
 qty_type.name AS qty_name,
 grade.name AS grade,
 sizes.note AS size,
 freezing_type.name AS freezing,
 code_size.name AS code_size,
 code_size.block_weight AS block,
 tally_grading_master.id AS tally_grading_master_id,
 tally_grading_master.purchase_id AS grading_purchase_id,
 tally_grading_detail.id AS tally_grading_detail_id,
 tally_grading_detail.total_block AS total_block,
 tally_grading_detail.rest AS rest
 FROM
 tally_grading_master
 INNER JOIN tally_grading_detail ON tally_grading_detail.id_tally_grading_master = tally_grading_master.id
 LEFT JOIN grade ON tally_grading_detail.id_grade = grade.id
 LEFT JOIN code_size ON code_size.id = tally_grading_detail.id_code_size
 LEFT JOIN sizes ON code_size.id_size = sizes.id
 LEFT JOIN freezing_type ON code_size.id_freezing_type = freezing_type.id
 LEFT JOIN qty_type ON tally_grading_detail.id_qty_type = qty_type.id
 INNER JOIN purchases ON tally_grading_master.purchase_id = purchases.id';

 $grading_history = DB::table(DB::raw("($grading_history_sql) as rs_grading_history_sql"))
 ->where('grading_purchase_id', $id)->orderBy('tally_grading_detail_id', 'desc')->get();

        // dd($grading_view);

 return view ('home.tally_form.grading', compact('temp_tally', 'tally', 'grades', 'qty_types', 'code_sizes', 'grading_view', 'grading_history'));
}

public function storegrading($id, Request $request)
{ 
  $post = new TallyDetailGrading(); 
  $post->id_qty_type = $request->qty_type;
  $post->id_grade = $request->grade_id; 
  $post->id_code_size = $request->code_size_id;
  $post->rest = $request->rest;
  $post->total_block = $request->total_block;
  $post->purchase_id = $request->purchase_id;
  $post->temp_tally_id = $request->temp_tally_id;
  $post->save();
  return response()->json($post); 
}

public function closegrading($id)
{   
  $purchase = TempTally::find($id);  
  $purchase->form_status = 'grading'; 
  $purchase->save();

  return redirect()->action('Home\TallyFormController@index');
}


public function historygrading($id)
{
  $temp_tally = DB::table('temp_tally') 
  ->where('id', $id) 
  ->first(); 

  $grades = Grade::all(); 

  $code_sizes = DB::table('code_size')
  ->select('code_size.id', 'code_size.name', 'code_size.block_weight', 'sizes.note', 'freezing_type.name AS freezing_name')
  ->join('sizes', 'sizes.id', '=', 'code_size.id_size')
  ->join('freezing_type', 'freezing_type.id', '=', 'code_size.id_freezing_type')
  ->get();

  $qty_types = QtyType::all(); 

  $grading_history_sql = '
  SELECT
  temp_tally.trans_code AS trans_code,
  qty_type.name AS qty_name,
  grade.name AS grade,
  sizes.note AS size,
  freezing_type.name AS freezing,
  code_size.name AS code_size,
  code_size.block_weight,
  code_size.block_weight AS block, 
  tally_grading_detail.temp_tally_id AS temp_tally_id,
  tally_grading_detail.id AS tally_grading_detail_id,
  tally_grading_detail.total_block AS total_block,
  tally_grading_detail.rest AS rest
  FROM  tally_grading_detail
  LEFT JOIN grade ON tally_grading_detail.id_grade = grade.id
  LEFT JOIN code_size ON code_size.id = tally_grading_detail.id_code_size
  LEFT JOIN sizes ON code_size.id_size = sizes.id
  LEFT JOIN freezing_type ON code_size.id_freezing_type = freezing_type.id
  LEFT JOIN qty_type ON tally_grading_detail.id_qty_type = qty_type.id
  INNER JOIN temp_tally ON tally_grading_detail.temp_tally_id = temp_tally.id';

  $grading_history = DB::table(DB::raw("($grading_history_sql) as rs_grading_history_sql"))
  ->where('temp_tally_id', $id)->orderBy('tally_grading_detail_id', 'desc')->get();

        //dd($grading_history);

  return view ('home.tally_form.historygrading', compact('temp_tally', 'grades', 'qty_types', 'code_sizes', 'grading_view', 'grading_history'));
}


    //PACKING 
public function createpacking($id)
{  

  $temp_tally = DB::table('temp_tally') 
  ->where('id', $id) 
  ->first(); 

  $tally = TallyGradingMaster::where('purchase_id', $id)->first();  

  $sql_detail = 'SELECT
  CONCAT(tally_grading_detail.temp_tally_id, "_",
  grade.id, "_",
  sizes.id, "_",
  freezing_type.id, "_",
  code_size.id, "_",
  qty_type.id, "_") AS id,
  temp_tally.trans_code AS purchase_code,
  grade.`name` AS grade,
  tally_grading_detail.id_grade,
  sizes.note AS size,
  freezing_type.`name` AS freezing,
  freezing_type.id AS id_freezing_type,
  code_size.`name` AS code_size,
  code_size.id AS id_code_size,
  SUM(tally_grading_detail.block) AS block,
  SUM(tally_grading_detail.total_block) AS total_block,
  SUM(tally_grading_detail.mix) AS mix,
  SUM(tally_grading_detail.icr) AS icr, 
  SUM(tally_grading_detail.rest) AS rest,
  SUM(code_size.block_weight) AS block_weight,
  qty_type.id AS id_qty_type,
  qty_type.`name` AS qty_type,
  SUM(view_tally_packing_master.total) AS total,
  CASE WHEN tally_grading_detail.edit_flag = 1 THEN SUM(tally_grading_detail.mc) ELSE SUM(FLOOR(
  tally_grading_detail.total_block / view_tally_packing_master.total
  )) END AS mc, CASE WHEN 
  tally_grading_detail.edit_flag = 1 THEN SUM(tally_grading_detail.ic) ELSE  FORMAT(CONCAT(
  "0.",
  RIGHT (
  FORMAT(
  SUM(tally_grading_detail.total_block / view_tally_packing_master.total),
  2
  ),
  2
  )
  ) * SUM(view_tally_packing_master.total),0) END AS ic
  FROM 
  tally_grading_detail  
  LEFT JOIN grade ON tally_grading_detail.id_grade = grade.id
  LEFT JOIN code_size ON code_size.id = tally_grading_detail.id_code_size
  LEFT JOIN sizes ON code_size.id_size = sizes.id
  LEFT JOIN freezing_type ON code_size.id_freezing_type = freezing_type.id
  LEFT JOIN qty_type ON tally_grading_detail.id_qty_type = qty_type.id
  INNER JOIN temp_tally ON tally_grading_detail.temp_tally_id = temp_tally.id
  LEFT JOIN view_tally_packing_master ON tally_grading_detail.id = view_tally_packing_master.id
  WHERE tally_grading_detail.temp_tally_id = '.$id.'
  GROUP BY
  tally_grading_detail.temp_tally_id,
  temp_tally.trans_code,
  grade.`name`,
  sizes.note,
  freezing_type.`name`,
  code_size.`name`,
  qty_type.`name`
  ORDER BY
  grade.`name` ASC';
  $sql_order = 'rs_sql_detail.qty_type, rs_sql_detail.grade';
  $tally_detail = DB::table(DB::raw("($sql_detail) as rs_sql_detail"))->get();

        //dd($tally_detail);

        //dd($tally_detail);

  return view ('home.tally_form.formpacking', compact('tally_detail', 'purchase', 'tally', 'temp_tally'));
}


public function storepacking(Request $request, $id)
{

  $purchase = Purchase::find($id);  
  $purchase->action_form_status = 'Edit';
  $purchase->save();

  foreach($request->input('tally') as $detail_key => $detail_value)
  { 
    $tally_detail = new TallyPackingDetail;    
    $tally_detail->temp_tally_id = $id; 
    $tally_detail->id_qty_type = $detail_value['id_qty_type']; 
    $tally_detail->id_grade = $detail_value['id_grade']; 
    $tally_detail->size = $detail_value['size']; 
    $tally_detail->id_freezing_type = $detail_value['id_freezing_type']; 
    $tally_detail->id_code_size = $detail_value['id_code_size']; 
    $tally_detail->block = $detail_value['block_weight']; 
    $tally_detail->total = $detail_value['total']; 
    $tally_detail->total_block = $detail_value['total_block']; 
    $tally_detail->mc = $detail_value['mc']; 
    $tally_detail->mix = $detail_value['mix']; 
    $tally_detail->rest = $detail_value['rest'];  
    $tally_detail->icr = $detail_value['icr'];
    $tally_detail->save();
  }
  return redirect()->action('Home\TallyFormController@viewpacking', $id);
} 

public function editpacking($id)
{ 

 $temp_tally = DB::table('temp_tally') 
 ->where('id', $id) 
 ->first();

        //dd($purchase);
 $sql_detail = 'SELECT
 tally_packing_detail.id,
 temp_tally.trans_code AS purchase_code,
 grade.`name` AS grade,
 sizes.note AS size,
 freezing_type.`name` AS freezing,
 code_size.`name` AS code_size,
 tally_packing_detail.block,
 tally_packing_detail.total_block,
 tally_packing_detail.mix,
 tally_packing_detail.icr,
 tally_packing_detail.rest,
 code_size.block_weight,
 qty_type.`name` AS qty_type,
 view_tally_packing_master.total,
 tally_packing_detail.ic AS ic,
 tally_packing_detail.mc
 FROM
 tally_packing_detail
 LEFT JOIN grade ON tally_packing_detail.id_grade = grade.id
 LEFT JOIN code_size ON code_size.id = tally_packing_detail.id_code_size
 LEFT JOIN sizes ON code_size.id_size = sizes.id
 LEFT JOIN freezing_type ON code_size.id_freezing_type = freezing_type.id
 LEFT JOIN qty_type ON tally_packing_detail.id_qty_type = qty_type.id
 INNER JOIN temp_tally ON tally_packing_detail.temp_tally_id = temp_tally.id
 LEFT JOIN view_tally_packing_master ON tally_packing_detail.id = view_tally_packing_master.id
 WHERE tally_packing_detail.temp_tally_id = '.$id.'
 ORDER BY
 grade.`name` ASC';
 $tally_detail = DB::table(DB::raw("($sql_detail) as rs_sql_detail"))->get();

 return view ('home.tally_form.editpacking', compact('tally_detail', 'temp_tally'));
}

public function updatepacking(Request $request, $id)
{

  $purchase = Purchase::find($id);  
  $purchase->action_form_status = 'Edit';
  $purchase->save();

  foreach($request->input('tally') as $detail_key => $detail_value)
  { 
    $tally_detail = TallyPackingDetail::where('id', $detail_key)->first();  
    $tally_detail->ic = $detail_value['ic']; 
    $tally_detail->mc = $detail_value['mc'];
    $tally_detail->mix = $detail_value['mix'];
    $tally_detail->icr = $detail_value['icr'];
    $tally_detail->edit_flag = '1';
    $tally_detail->save();
  }
  return redirect()->action('Home\TallyFormController@viewpacking', $id);
} 

public function closepacking($id)
{   
  $purchase = TempTally::find($id);  
  $purchase->form_status = 'packing'; 
  $purchase->save();

  return redirect()->action('Home\TallyFormController@index');
}


public function viewpacking($id)
{ 

 $temp_tally = DB::table('temp_tally') 
 ->where('id', $id) 
 ->first();

        //dd($purchase);
 
 $sql_detail = 'SELECT
 tally_packing_detail.id,
 temp_tally.trans_code AS purchase_code,
 grade.`name` AS grade,
 sizes.note AS size,
 freezing_type.`name` AS freezing,
 code_size.`name` AS code_size,
 tally_packing_detail.block,
 tally_packing_detail.total_block,
 tally_packing_detail.mix,
 tally_packing_detail.icr,
 tally_packing_detail.rest,
 code_size.block_weight,
 qty_type.`name` AS qty_type,
 view_tally_packing_master.total,
 tally_packing_detail.ic AS ic,
 tally_packing_detail.mc
 FROM
 tally_packing_detail
 LEFT JOIN grade ON tally_packing_detail.id_grade = grade.id
 LEFT JOIN code_size ON code_size.id = tally_packing_detail.id_code_size
 LEFT JOIN sizes ON code_size.id_size = sizes.id
 LEFT JOIN freezing_type ON code_size.id_freezing_type = freezing_type.id
 LEFT JOIN qty_type ON tally_packing_detail.id_qty_type = qty_type.id
 INNER JOIN temp_tally ON tally_packing_detail.temp_tally_id = temp_tally.id
 LEFT JOIN view_tally_packing_master ON tally_packing_detail.id = view_tally_packing_master.id
 WHERE tally_packing_detail.temp_tally_id = '.$id.'
 ORDER BY
 grade.`name` ASC';
 $tally_detail = DB::table(DB::raw("($sql_detail) as rs_sql_detail"))->get();

 return view ('home.tally_form.viewpacking', compact('tally_detail', 'temp_tally'));
}
}
