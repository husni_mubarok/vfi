<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Price extends Model
{
    protected $dates = ['deleted_at'];

    public function purchase()
    {
    	return $this->belongsTo('App\Model\Purchase', 'purchase_id', 'id');
    }

    public function purchase_detail()
    {
    	return $this->belongsTo('App\Model\PurchaseDetail', 'id', 'price_id');
    }

    public function size()
    {
    	return $this->belongsTo('App\Model\Size', 'size_id', 'id');
    }
}
