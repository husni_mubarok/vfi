<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ItemCentralDetail extends Model
{
    protected $table = 'item_central_detail';

    public function item_central()
	{
		return $this->belongsTo('App\Model\ItemCentral', 'item_central_id', 'id');
	}

	public function item()
	{
		return $this->belongsTo('App\Model\Item', 'item_cb_id', 'id');
	}
}



