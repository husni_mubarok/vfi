<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductFish extends Model
{
    protected $table = 'products_fishes';
    protected $dates = ['deleted_at'];

    public function product()
    {
    	return $this->belongsTo('App\Model\Product', 'product_id', 'id');
    }

    public function fish()
    {
    	return $this->belongsTo('App\Model\Fish', 'fish_id', 'id');
    }
}
