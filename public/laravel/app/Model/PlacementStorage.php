<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class PlacementStorage extends Model
{
  protected $table = 'placement_storage';
  protected $dates = ['deleted_at'];


}
