<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RejectRejectReason extends Model
{
    protected $table = 'rejects_reject_reasons';
    protected $dates = ['deleted_at'];

    public function reject()
    {
    	return $this->belongsTo('App\Model\Reject', 'reject_id', 'id');
    }

    public function reject_reason()
    {
    	return $this->belongsTo('App\Model\RejectReason', 'reject_reason_id', 'id');
    }
}
