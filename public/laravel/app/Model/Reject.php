<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reject extends Model
{
    protected $table = 'rejects';
    protected $dates = ['deleted_at'];

    public function production_result()
    {
    	return $this->belongsTo('App\Model\ProductionResult', 'production_result_id', 'id');
    }

    public function reject_reject_reason()
    {
    	return $this->hasMany('App\Model\RejectRejectReason', 'reject_id', 'id');
    }
}
