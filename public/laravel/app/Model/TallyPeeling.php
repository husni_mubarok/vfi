<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TallyPeeling extends Model
{
    protected $table = 'tally_peeling';
	protected $dates = ['deleted_at'];

	public function purchase()
	{
		return $this->hasMany('App\Model\Purchase', 'purchase_id', 'id');
	} 
}
