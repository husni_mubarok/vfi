<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductionResultWaste extends Model
{
    use SoftDeletes;

    protected $table = 'production_result_waste';
    protected $dates = ['deleted_at'];

    public function production_result()
    {
    	return $this->belongsTo('App\Model\ProductionResult', 'production_result_id', 'id');
    }

    public function waste_detail()
    {
    	return $this->hasMany('App\Model\ProductionResultWasteDetail', 'waste_id', 'id');
    }
}
