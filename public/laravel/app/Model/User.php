<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    //
    protected $dates = ['deleted_at'];

    // public function user_role()
    // {
    // 	return $this->hasOne('App\Model\UserRole', 'user_id', 'id');
    // }

    public function privilege()
    {
    	return $this->hasOne('App\Model\Privilege', 'id', 'id_privilege');
    }
}
