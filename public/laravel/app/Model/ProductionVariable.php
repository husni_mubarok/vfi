<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductionVariable extends Model
{
    protected $table = 'productions_variables';
    protected $dates = ['deleted_at'];

    public function production()
    {
    	return $this->belongsTo('App\Model\Production', 'production_id', 'id');
    }
}
