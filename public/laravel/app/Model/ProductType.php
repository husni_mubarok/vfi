<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductType extends Model
{
    protected $table = 'product_types';
    protected $dates = ['deleted_at'];

    public function product()
    {
        return $this->hasMany('App\Model\Product', 'product_type_id', 'id');
    }

    public function product_type_size()
    {
        return $this->hasMany('App\Model\ProductTypeSize', 'product_type_id', 'id');
    }
}
