<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
	use SoftDeletes;
	protected $table = 'customers';
    protected $dates = ['deleted_at'];

    public function cart()
    {
    	return $this->belongsTo('App\Model\Cart', 'customer_id', 'id');
    }

}
