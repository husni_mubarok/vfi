<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MPrepProduct extends Model
{
    protected $table = 'm_prep_products';
    protected $dates = ['deleted_at'];

    public function m_prep_product_ingredient()
    {
    	return $this->hasMany('App\Model\MPrepProductIngredient', 'm_prep_product_id', 'id');
    }

    public function production_m_prep_product()
    {
    	return $this->hasMany('App\Model\ProductionMPrepProduct', 'm_prep_product_id', 'id');
    }
}
