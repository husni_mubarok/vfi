<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PayrollDetail extends Model
{
	use SoftDeletes;

	protected $table = 'payroll_detail';
	protected $dates = ['deleted_at'];

	public function payroll()
	{
		return $this->belongsTo('App\Model\Payroll', 'payroll_id', 'id');
	}

	public function employee()
	{
		return $this->belongsTo('App\Model\Employee', 'employee_id', 'id');
	}
}
