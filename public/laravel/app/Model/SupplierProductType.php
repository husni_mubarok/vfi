<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SupplierProductType extends Model
{
    protected $table = 'suppliers_product_types';
    protected $dates = ['deleted_at'];

    public function supplier()
    {
    	return $this->belongsTo('App\Model\Supplier', 'supplier_id', 'id');
    }

    public function product_type()
    {
    	return $this->belongsTo('App\Model\ProductType', 'product_type_id', 'id');
    }
}
