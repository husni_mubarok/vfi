<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class UomCategory extends Model
{
	protected $table = 'uom_categories';
	protected $dates = ['deleted_at'];

    public function uom()
    {
    	return $this->hasMany('App\Model\Uom', 'uom_category_id', 'id');
    }
}
