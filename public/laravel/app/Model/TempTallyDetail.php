<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TempTallyDetail extends Model
{
	protected $table = 'temp_tally_detail';
    protected $dates = ['deleted_at'];

    public function temp_telly()
    {
    	return $this->belongsTo('App\Model\TempTelly', 'id_temp_telly', 'id');
    }

    public function purchase_details()
    {
        return $this->hasOne('App\Model\PurchaseDetail', 'id', 'id_purchase_details');
    }

}
