<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductionResultVariable extends Model
{
    protected $table = 'production_results_variables';
    protected $dates = ['deleted_at'];

    public function production_result()
    {
    	return $this->belongsTo('App\Model\ProductionResult', 'production_result_id', 'id');
    }
}
