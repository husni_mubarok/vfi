<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CategoryItem extends Model
{
	use SoftDeletes;
	protected $table = 'category_item';
	protected $dates = ['deleted_at'];

	public function master_item()
	{
		return $this->hasMany('App\Model\MasterItem', 'id_category_item', 'id');
	}

}
