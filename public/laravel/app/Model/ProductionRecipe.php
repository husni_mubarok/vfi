<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductionRecipe extends Model
{
    protected $table = 'productions_recipes';
    protected $dates = ['deleted_at'];

    public function product()
    {
    	return $this->belongsTo('App\Model\Production', 'production_id', 'id');
    }

    public function recipe()
    {
    	return $this->hasMany('App\Model\RecipeItem', 'recipe_id', 'id');
    }
}
