<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    protected $table = 'products';
    protected $dates = ['deleted_at'];

    public function product_type()
    {
        return $this->belongsTo('App\Model\ProductType', 'product_type_id', 'id');
    }

    public function supplier_product()
    {
        return $this->hasMany('App\Model\SupplierProduct', 'product_id', 'id');
    }

    public function product_fish() 
    {
        return $this->hasMany('App\Model\ProductFish', 'product_id', 'id');
    }
}
