<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CashbondDetailItem extends Model
{
    protected $table = 'cashbond_detail_item';

    public function cashbond_detail()
	{
		return $this->belongsTo('App\Model\CashbondDetail', 'cashbond_detail_id', 'id');
	}
  
	public function item()
	{
		return $this->belongsTo('App\Model\Item', 'item_id', 'id');
	}
}



