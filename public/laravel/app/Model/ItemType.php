<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemType extends Model
{
	use SoftDeletes;
	protected $table = 'item_type';
	protected $dates = ['deleted_at'];

	public function item_category()
	{
		return $this->hasMany('App\Model\ItemCategory', 'item_type_id', 'id');
	}

}
