<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PrepProdDetailIngredient extends Model
{
    protected $table = 'prep_prod_details_ingredients';
    protected $dates = ['deleted_at'];

    public function prepared_product_detail()
    {
    	return $this->belongsTo('App\Model\PreparedProductDetail', 'prep_prod_dtl_id', 'id');
    }

    public function ingredient()
    {
    	return $this->belongsTo('App\Model\Ingredient', 'ingredient_id', 'id');
    }
}
