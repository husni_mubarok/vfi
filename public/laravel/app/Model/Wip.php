<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Wip extends Model
{
    protected $table = 'wip';
	protected $dates = ['deleted_at'];

	public function purchase()
	{
		return $this->hasMany('App\Model\Purchase', 'purchase_id', 'id');
	} 
}
