<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Unit extends Model
{
	use SoftDeletes;
	protected $table = 'unit';
	protected $dates = ['deleted_at'];

	public function master_item()
	{
		return $this->hasMany('app\Model\MasterItem', 'id_unit', 'id');
	}

}
