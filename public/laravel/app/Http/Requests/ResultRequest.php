<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResultRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'finished_at' => 'required|date',
            'uom_id' => 'required',
            'block.*' => 'required',
            'size.*' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'finished_at.required' => 'Finish date is required',
            'finished_at.date' => 'Finish date must be a valid date',

            'uom_id.required' => 'UoM is required', 

            'block.required' => 'Cart must not be empty',

            'size.required' => 'Cart must not be empty',
        ];
    }
}
