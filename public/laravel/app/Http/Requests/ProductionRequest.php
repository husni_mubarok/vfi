<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fish_id' => 'required|integer',
            'name' => 'required',
            'description' => '',
        ];
    }

    public function messages()
    {
        return [
            'fish_id.required' => 'Fish is required', 
            'fish_id.integer' => 'Please select a valid Fish', 
            
            'name.required' => 'Name is required',
        ];
    }
}
