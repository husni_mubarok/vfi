<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PreparedProductIngredientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ingredient.*' => 'required|numeric',
        ];
    }

    public function messages()
    {
        return [
            'ingredient.required' => 'Ingredient value is required', 
            'ingredient.numeric' => 'Ingredient value must be of valid number',
        ];
    }
}
