<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PreparedProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'm_prep_product_id' => 'required|integer',
            'production_id' => 'required|integer', 
            'avg_size' => 'required|integer', 
            'daily_target' => 'required|integer', 
            'work_days' => 'required|integer',
            'start_date' => 'required|date',
            'finish_date' => 'required|date',
            'flavor' => '',

            // 'size_id' => 'required|integer',
            // 'estimated_result' => 'required|numeric',
        ];
    }

    public function messages()
    {
        return [
            'm_prep_product_id.required' => 'Prepared Product is required',
            'm_prep_product_id.integer' => 'Select a valid product',

            'production_id.required' => 'Material is required',
            'production_id.integer' => 'Select a valid material',

            'avg_size.required' => 'Average size is required', 
            'avg_size.integer' => 'Average size must be a valid number', 

            'daily_target.required' => 'Daily target is required', 
            'daily_target.integer' => 'Daily target must be a valid number', 

            'work_days.required' => 'Work days is required', 
            'work_days.integer' => 'Work days must be a valid number', 

            'start_date.required' => 'Start date is required', 
            'start_date.date' => 'Start date must be a valid number', 

            'finish_date.required' => 'Finish date is required', 
            'finish_date.date' => 'Finish date must be a valid number', 

            // 'size_id.required' => 'Size is required', 
            // 'size_id.integer' => 'Select a valid size',

            // 'estimated_result.required' => 'Estimated Result is required', 
            // 'estimated_result.numeric' => 'Estimated Result must be a valid number'
        ];
    }
}
