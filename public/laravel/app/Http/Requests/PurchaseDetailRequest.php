<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PurchaseDetailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'size.*' => 'required|numeric',
            'price.*' => 'required|numeric',
            'amount.*' => 'required|numeric',
        ];
    }

    public function messages()
    {
        return [
            'size.required' => 'Size is required', 
            'size.numeric' => 'Size must be of valid number',

            'price.required' => 'Price is required', 
            'price.numeric' => 'Price must be of valid number',

            'amount.required' => 'Amount is required', 
            'amount.numeric' => 'Amount must be of valid number',
        ];
    }
}
