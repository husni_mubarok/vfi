<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MPrepProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'description' => '',
            'uom' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Name is required',

            'uom.required' => 'Select a proper Unit'
        ];
    }
}
