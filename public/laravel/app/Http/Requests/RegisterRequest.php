<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|unique:users,username',
            'email' => 'required|email',
            'first_name' => 'required',
            'last_name' => '',
            // 'role_id' => 'required|integer',
            'image' => 'image',
            'password' => 'required|confirmed'
        ];
    }

    public function messages()
    {
        return [
            'username.required' => 'Username is required', 

            'email.required' => 'E-mail is required',
            'email.email' => 'E-mail must be a valid address',

            'first_name.required' => 'First name is required', 

            'last_name.required' => '',

            // 'role_id.required' => 'Role is required',
            // 'role_id.integer' => 'Role is required', 

            'image.image' => 'Image must be a valid file', 

            'password.required' => 'Password is required', 
            'password.confirmed' => 'Password confirmation mismatch'
        ];
    }
}
