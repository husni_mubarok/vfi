<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SizeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'min_value' => 'required|integer',
            'max_value' => 'required|integer',
            'note' => '',
        ];
    }

    public function messages()
    {
        return [
            'min_value.required' => 'Minimum value is required',
            'min_value.integer' => 'Minimum value must be a valid number',

            'max_value.required' => 'Maximum value is required', 
            'max_value.integer' => 'Maximum value must be a valid number'
        ];
    }
}
