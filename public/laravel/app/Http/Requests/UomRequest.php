<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UomRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'uom_category_id' => 'required|integer', 
            'parent_id' => 'required|integer',
            'name' => 'required',
            'value' => 'required|numeric',
        ];
    }

    public function messages()
    {
        return [
            'uom_category_id.required' => 'Category is required',
            'uom_category_id.integer' => 'Category must be a valid choice',

            'parent_id.required' => 'Parent is required', 
            'parent_id.integer' => 'Parent must be a valid choice',

            'name.required' => 'Name is required',

            'value.required' => 'Value is required', 
            'value.numeric' => 'Value must be a valid number', 
        ];
    }
}
