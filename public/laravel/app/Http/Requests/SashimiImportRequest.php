<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SashimiImportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required|date',
            'tray_qty' => 'required|integer',
            'cart_id.*' => 'required|integer', 
            'qty.*' => 'required|integer',
        ];
    }

    public function messages()
    {
        return [
            'date.required' => 'Date is required', 
            'date.date' => 'Date must be of valid format', 

            'tray_qty.required' => 'Quantity per Tray is required', 
            'tray_qty.integer' => 'Quantity per Tray must be an integer', 

            'cart_id.required' => 'Cart must not be empty', 
            'cart_id.integer' => 'Cart is invalid', 

            'qty.required' => 'Cart must not be empty', 
            'qty.integer' => 'Cart must not be empty',
        ];
    }
}
