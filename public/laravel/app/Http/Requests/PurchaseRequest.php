<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PurchaseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => 'required',
            'supplier_id' => 'required|integer',
            'product_id' => 'required|integer',
            // 'size' => 'required|integer',
            'quantity' => 'required|numeric',
            'estimated_price' => 'required|integer',
            'purchase_time' => 'required|date',
            // 'division' => 'required',
            // 'reference' => 'required', 
            'project' => 'required',
            'currency' => 'required',
            'ppn' => 'required',
            'discount' => 'required|integer',
            'down_payment' => 'required|integer',
            'term_condition' => 'required',
            'term_payment' => 'required',
            'notes' => '',
        ];
    }

    public function messages()
    {
        return [
            'description.required' => 'Description is required',

            'supplier_id.required' => 'Supplier is required',
            'supplier_id.integer' => 'Please insert a valid supplier',

            'product_id.required' => 'Product is required',
            'product_id.integer' => 'Please insert a valid product',

            // 'size.required' => 'Size is required', 
            // 'size.integer' => 'Size must be a valid number', 

            'quantity.required' => 'Quantity is required',
            'quantity.numeric' => 'Quantity must be a valid number',

            'estimated_price.required' => 'Estimated Price is required',
            'estimated_price.integer' => 'Please insert a valid price',

            'purchase_time.required' => 'Purchase time is required', 
            'purchase_time.date' => 'Purchase time must be a valid date & time format',

            // 'division.required' => 'Division is required',

            // 'reference.required' => 'Reference is required', 

            'project.required' => 'Project is required',

            'currency.required' => 'Currency is required', 

            'ppn.required' => 'PPN is required',

            'discount.required' => 'Discount is required',
            'discount.integer' => 'Discount must be a valid number',

            'down_payment.required' => 'DP is required',
            'down_payment.integer' => 'DP must be a valid number',

            'term_condition.required' => 'Terms of Condition is required',

            'term_payment.required' => 'Terms of Payment is required',
        ];
    }
}
