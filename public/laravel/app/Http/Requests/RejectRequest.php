<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RejectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'reject_product' => 'required|numeric',
        ];
    }

    public function messages()
    {
        return [
            'reject_product.required' => 'Reject Product is required',
            'reject_product.numeric' => 'Reject Product must be a valid number'
        ];
    }
}
