<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductionResultRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'production_id' => 'required|integer',
            // 'amount' => 'required|integer',
            'weight' => 'required|numeric',
            'block_weight' => 'required|numeric', 
            'started_at' => 'required|date',
            'criteria' => '',
        ];
    }

    public function messages()
    {
        return [
            'production_id.required' => 'Production is required',
            'production_id.integer' => 'Please select a valid production',

            // 'amount.required' => 'Amount is required',
            // 'amount.integer' => 'Amount must be a valid number',

            'weight.required' => 'Weight is required', 
            'weight.numeric' => 'Weight must be a valid number',

            'block_weight.required' => 'Block weight is required', 
            'block_weight.numeric' => 'Block weight must be a valid number',

            'started_at.required' => 'Start date is required',
            'started_at.date' => 'Please insert a valid date',
        ];
    }
}
