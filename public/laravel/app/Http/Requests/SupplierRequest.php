<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SupplierRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'address' => 'required',
            'email' => 'required|email',
            'phone_number' => 'required',
            'fax' => '',
            'website' => '', 
            'pic' => 'required', 
            'bank_account' => '',
            'note' => '',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Name is required',

            'address.required' => 'Address is required', 

            'email.required' => 'E-mail is required', 
            'email.email' => 'Please use a valid e-mail address', 

            'phone_number.required' => 'Phone number is required',

            'pic.required' => 'Person In Charge is required', 
        ];
    }
}
