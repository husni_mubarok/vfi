<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class HomeController extends Controller
{
    public function index()
    {
        return view ('welcome');
    }

    public function portal()
    {
        session_start();
        $user = User::where('username', $_SESSION['vffiM_name'])->first();
        if($user)
        {
            Auth::login($user);
            return redirect()->action('HomeController@index');
        } else {
            return view('portal');
        }
    }

    public function test()
    {
        
    }
}
