<?php

namespace App\Http\Controllers\API;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repository\PurchaseRepository;
use App\Repository\ProductRepository;
use App\Repository\ProductTypeRepository;
use App\Repository\SupplierRepository;
use App\Repository\SizeRepository;
use App\Repository\PriceRepository;

class PurchaseController extends Controller
{
    protected $PurchaseRepository;
    protected $ProductRepository;
    protected $ProductTypeRepository;
    protected $SupplierRepository;
    protected $SizeRepository;
    protected $PriceRepository;

    public function __construct(PurchaseRepository $purchase_repository, ProductRepository $product_repository, ProductTypeRepository $product_type_repository, SupplierRepository $supplier_repository, SizeRepository $size_repository, PriceRepository $price_repository)
    {
        $this->PurchaseRepository = $purchase_repository;
        $this->ProductRepository = $product_repository;
        $this->ProductTypeRepository = $product_type_repository;
        $this->SupplierRepository = $supplier_repository;
        $this->SizeRepository = $size_repository;
        $this->PriceRepository = $price_repository;
    }

    public function index()
    {
        $purchases = $this->PurchaseRepository->get_all();
        for($i = 0; $i < $purchases->count(); $i++)
        {
            $json[$i] = 
            [
                'id' => $purchases[$i]->id, 
                'doc_code' => $purchases[$i]->doc_code,
                'description' => $purchases[$i]->description, 
                'status' => $purchases[$i]->status, 
                'supplier' => $purchases[$i]->purchase_supplier->supplier->name, 
                'product' => $purchases[$i]->purchase_product->product->product_type->name.' '.$purchases[$i]->purchase_product->product->name, 
                'estimated_price' => $purchases[$i]->estimated_price, 
                'purchase_time' => date("d M Y", strtotime($purchases[$i]->purchase_time)),
            ];
        }
        return response()->json($json);
    }

    public function tally_collect()
	{
		$collect = DB::table('temp_tally')
		->whereNotNull('collect_receiving')
		->orWhereNotNull('collect_headsplit')
		->orWhereNotNull('collect_peeling_peeling')
		->orWhereNotNull('collect_peeling_trimming')
		->orWhereNotNull('collect_treatment_tube')
		->orWhereNotNull('collect_treatment_tentacle')
		->orWhereNotNull('collect_grading')
		->count(); 

		return response()->json($collect);
	}

    public function index_code()
    {
        $purchases = $this->PurchaseRepository->get_all();
        for($i = 0; $i < $purchases->count(); $i++)
        {
            $json[$i] = 
            [
                'id' => $purchases[$i]->id, 
                'trans_code' => $purchases[$i]->trans_code, 
            ];
        }
        return response()->json($json);
    }

    public function detail(Request $request)
    {
        if($request->input('doc_code'))
        {
            $purchase = $this->PurchaseRepository->get_by_doc_code($request->input('doc_code'));
        } elseif ($request->input('id')) {
            $purchase = $this->PurchaseRepository->get_one($request->input('id'));
        }
        $json = 
        [   
            'id' => $purchase->id,
            'doc_code' => $purchase->doc_code, 
            'trans_code' => $purchase->trans_code,
            'description' => $purchase->description, 
            'status' => $purchase->status,
            'product' => $purchase->purchase_product->product->product_type->name.' '.$purchase->purchase_product->product->name, 
            'quantity' => $purchase->quantity,

            // 'division' => $purchase->division, 
            // 'reference' => $purchase->reference, 
            'project' => $purchase->project, 
            'currency' => $purchase->currency, 
            'ppn' => $purchase->ppn,
            'discount' => $purchase->discount,
            'down_payment' => $purchase->down_payment,
            'term_condition' => $purchase->term_condition,
            'term_payment' => $purchase->term_payment,
            'notes' => $purchase->notes, 
            'purchase_time' => date("d M Y", strtotime($purchase->purchase_time)),  
            'total_weight' => 0,
            'total_price' => 0,
        ];
         
        if($purchase->purchase_detail->count() > 0)
        {
            $json['estimated_price'] = 0;
            foreach($purchase->purchase_detail as $purchase_detail)
            {
                $json['estimated_price'] += $purchase_detail->price * $purchase_detail->amount;
            }
        } else {
            $json['estimated_price'] = $purchase->estimated_price;
        }

        if($purchase->ppn == 'on')
        {
            $json['price_ppn'] = $json['estimated_price'] * 11 / 10;
            $json['price_discount'] = $json['estimated_price'] - $json['discount'];
            $json['price_ppn_discount'] = $json['price_discount'] * 11 / 10;            
        } elseif ($purchase->ppn == 'off') {
            $json['price_ppn'] = $json['estimated_price'];
            $json['price_discount'] = $json['estimated_price'] - $json['discount'];
            $json['price_ppn_discount'] = $json['price_discount'];            
        }


        $json['supplier']['id'] = $purchase->purchase_supplier->supplier->id;
        $json['supplier']['name'] = $purchase->purchase_supplier->supplier->name;
        $json['supplier']['address'] = $purchase->purchase_supplier->supplier->address;
        $json['supplier']['email'] = $purchase->purchase_supplier->supplier->email;
        $json['supplier']['phone_number'] = $purchase->purchase_supplier->supplier->phone_number;
        $json['supplier']['fax'] = $purchase->purchase_supplier->supplier->fax;
        $json['supplier']['website'] = $purchase->purchase_supplier->supplier->website;
        $json['supplier']['pic'] = $purchase->purchase_supplier->supplier->pic;
        $json['supplier']['bank_account'] = $purchase->purchase_supplier->supplier->bank_account;
        $json['supplier']['note'] = $purchase->purchase_supplier->supplier->note;

        foreach($purchase->price as $key => $price)
        {
            $json['price_list'][$key]['min'] = $price->size->min_value;
            $json['price_list'][$key]['max'] = $price->size->max_value;
            $json['price_list'][$key]['note'] = $price->size->note;
            $json['price_list'][$key]['value'] = $price->value;
        }

        foreach($purchase->purchase_detail as $key => $purchase_detail)
        {
            $json['purchase_detail'][$key]['trans_code'] = $purchase_detail->trans_code;
            $json['purchase_detail'][$key]['size'] = $purchase_detail->size;
            $json['purchase_detail'][$key]['amount'] = $purchase_detail->amount;
            $json['purchase_detail'][$key]['price'] = $purchase_detail->price; 
            $json['total_weight'] += $purchase_detail->amount;
            $json['total_price'] += $purchase_detail->price * $purchase_detail->amount;
        }

        return response()->json($json);
    }
}