<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ProductionResult;
use App\Model\ProductionResultVariable;
use App\Model\ProductionResultAddition;
use App\Model\ProductionresultDetail;

class ProductionController extends Controller
{
    public function index()
    {
    	$results = ProductionResult::all();
    	$i = 0;
    	foreach($results as $key => $result)
    	{
   //  		$json[$key] = 
   //  		[
   //  			'production_id' => $result->production_id,
   //  			'production' => $result->production->name,
   //  			'weight' => $result->weight,
   //  			'block_weight' => $result->block_weight,
   //  			'started_at' => $result->started_at,
   //  			'finished_at' => $result->finished_at,
   //  			'criteria' => $result->criteria,
			// ];

			foreach($result->production_result_detail as $detail)
			{
				$i += 1;
				$json[$i] = 
				[
					'name' => $result->production->name,
					'size' => $detail->size,
					'block' => $detail->block,
					'block_weight' => $result->block_weight,
					'start_date' => $result->started_at,
					'finish_date' => $result->finished_at,
				];
			}
    	}

    	return response()->json($json);
    }
}
