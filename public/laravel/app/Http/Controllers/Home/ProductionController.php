<?php

namespace App\Http\Controllers\Home;

use Auth;
use Illuminate\Http\Request;
use App\Http\Requests\ProductionResultRequest;
use App\Http\Requests\ProductionResultVariableRequest;
use App\Http\Requests\ResultRequest;
use App\Http\Requests\RejectRequest;
use App\Http\Controllers\Controller;
use App\Repository\ProductionResultRepository;
use App\Repository\FishRepository;
use App\Repository\ProductionResultVariableRepository;
use App\Repository\RejectRepository;
use App\Repository\RejectReasonRepository;
use App\Repository\RejectRejectReasonRepository;
use App\Model\Purchase;
use App\Model\ProductionResult;
use App\Model\ProductionResultVariable;
use App\Model\ProductionResultWaste;
use App\Model\ProductionResultWasteDetail;
use App\Model\ProductionResultDetail;
use App\Model\DetailTransactionHistory;
use App\Model\ProductionResultAddition;
use App\Model\AdditionTransactionHistory;
use App\Model\RejectType;
use App\Model\Uom;
use App\Model\ProductionExport;
use App\Model\TransactionStorage;
use PDF;
use DB;

use App\Http\Controllers\Editor\StorageController;
use App\Model\PlacementStorage;

class ProductionController extends Controller
{
    protected $ProductionResultRepository;
    protected $FishRepository;
    protected $ProductionResultVariableRepository;
    protected $RejectRepository;
    protected $RejectReasonRepository;
    protected $RejectRejectReasonRepository;

    public function __construct(ProductionResultRepository $production_result_repository, FishRepository $fish_repository, ProductionResultVariableRepository $production_result_variable_repository, RejectRepository $reject_repository, RejectReasonRepository $reject_reason_repository, RejectRejectReasonRepository $reject_reject_reason_repository)
    {
        $this->ProductionResultRepository = $production_result_repository;
        $this->FishRepository = $fish_repository;
        $this->ProductionResultVariableRepository = $production_result_variable_repository;
        $this->RejectRepository = $reject_repository;
        $this->RejectReasonRepository = $reject_reason_repository;
        $this->RejectRejectReasonRepository = $reject_reject_reason_repository;
    }

    public function index()
    {
        $today_data = ProductionResult::where('status', '<', 4)->where('production_type', 'regular')->with('production_result_detail')->with('production_result_addition')->with('production_result_reject')->with('production')->get();
        $waiting_closure = ProductionResult::where('status', 3)->where('production_type', 'regular')->latest()->get();
        return view ('home.production.production', compact('today_data', 'waiting_closure'));
    }

    public function unstored()
    {
        $production_results = ProductionResult::where('production_type', 'regular')->orderBy('started_at', 'desc')->where('status', 4)->get();
        $temp_list = [];
        foreach($production_results as $key_result => $result)
        {
            $temp_list[$key_result] = [
                'id' => $result->id,
                'material' => $result->production->fish->name,
                'product' => $result->production->name,
                'start_date' => $result->started_at, 
                'finish_date' => $result->finished_at,
                'status' => $result->status,
                'weight' => $result->weight, 
                'mst_carton' => $result->mst_carton,
                'remaining' => 0,
            ];

            foreach($result->production_result_variable as $key_waste => $waste)
            {
                $temp_list[$key_result]['waste'][$key_waste] = [
                    'name' => $waste->name,
                    'value' => $waste->value,
                ];
            }

            foreach($result->production_result_detail as $key_detail => $detail)
            {
                $stored_details = TransactionStorage::where('source_type', 1)->where('source_id', $detail->id)->where('approved', 1)->get();
                $total_detail_stored = 0;
                foreach($stored_details as $stored_detail)
                {
                    $total_detail_stored += $stored_detail->quantity;
                }

                $added_detail = 0;
                foreach($detail->to as $to)
                {
                    $added_detail += $to->amount;
                }

                $used_detail = 0;
                foreach($detail->from as $from)
                {
                    $used_detail += $from->amount;
                }

                $temp_list[$key_result]['detail'][$key_detail] = [
                    'size' => $detail->size, 
                    'block' => $detail->block + $added_detail - $used_detail, 
                    'stored' => $total_detail_stored, 
                    'added_detail' => $added_detail,
                    'used_detail' => $used_detail,
                ];

                $temp_list[$key_result]['remaining'] += ($temp_list[$key_result]['detail'][$key_detail]['block'] - $temp_list[$key_result]['detail'][$key_detail]['stored']);
            }

            foreach($result->production_result_addition as $key_addition => $addition)
            {
                $total_addition_stored = 0;

                $stored_additions = TransactionStorage::where('source_type', 2)->where('source_id', $addition->id)->where('approved', 1)->get();
                foreach($stored_additions as $stored_addition)
                {
                    $total_addition_stored += $stored_addition->quantity;
                }

                $added_addition = 0;
                foreach($addition->to as $to)
                {
                    $added_addition += $to->amount;
                }

                $used_addition = 0;
                foreach($addition->from as $from)
                {
                    $used_addition += $from->amount;
                }

                $temp_list[$key_result]['addition'][$key_addition] = [
                    'size' => $addition->size, 
                    'block' => $addition->block + $added_addition - $used_addition, 
                    'stored' => $total_addition_stored, 
                    'added_addition' => $added_addition,
                    'used_addition' => $used_addition,
                ];
            }

            if($temp_list[$key_result]['remaining'] != 0)
            {
                $production_result_list[$key_result] = $temp_list[$key_result];
            }
        }
        return view ('home.production.unstored', compact('production_result_list'));
    }

    public function bank()
    {
        $production_results = ProductionResult::where('production_type', 'regular')->orderBy('started_at', 'desc')->where('status', '=', 4)->get();

        $temp_list = [];
        foreach($production_results as $key_result => $result)
        {
            $temp_list[$key_result] = [
                'id' => $result->id,
                'material' => $result->production->fish->name,
                'product' => $result->production->name,
                'start_date' => $result->started_at, 
                'finish_date' => $result->finished_at,
                'status' => $result->status,
                'weight' => $result->weight, 
                'remaining' => 0,
            ];

            foreach($result->production_result_variable as $key_waste => $waste)
            {
                $temp_list[$key_result]['waste'][$key_waste] = [
                    'name' => $waste->name,
                    'value' => $waste->value,
                ];
            }

            foreach($result->production_result_detail as $key_detail => $detail)
            {
                $stored_details = TransactionStorage::where('source_type', 1)->where('source_id', $detail->id)->where('approved', 1)->get();
                $total_detail_stored = 0;
                foreach($stored_details as $stored_detail)
                {
                    $total_detail_stored += $stored_detail->quantity;
                }

                $added_detail = 0;
                foreach($detail->to as $to)
                {
                    $added_detail += $to->amount;
                }

                $used_detail = 0;
                foreach($detail->from as $from)
                {
                    $used_detail += $from->amount;
                }

                $temp_list[$key_result]['detail'][$key_detail] = [
                    'size' => $detail->size, 
                    'block' => $detail->block + $added_detail - $used_detail, 
                    'stored' => $total_detail_stored, 
                ];

                $temp_list[$key_result]['remaining'] += ($temp_list[$key_result]['detail'][$key_detail]['block'] - $temp_list[$key_result]['detail'][$key_detail]['stored']);
            }

            foreach($result->production_result_addition as $key_addition => $addition)
            {
                $total_addition_stored = 0;

                $stored_additions = TransactionStorage::where('source_type', 2)->where('source_id', $addition->id)->where('approved', 1)->get();
                foreach($stored_additions as $stored_addition)
                {
                    $total_addition_stored += $stored_addition->quantity;
                }

                $added_addition = 0;
                foreach($addition->to as $to)
                {
                    $added_addition += $to->amount;
                }

                $used_addition = 0;
                foreach($addition->from as $from)
                {
                    $used_addition += $from->amount;
                }

                $temp_list[$key_result]['addition'][$key_addition] = [
                    'size' => $addition->size, 
                    'block' => $addition->block, 
                    'stored' => $total_addition_stored, 
                ];

                $temp_list[$key_result]['remaining'] += ($temp_list[$key_result]['addition'][$key_addition]['block'] - $temp_list[$key_result]['addition'][$key_addition]['stored']);
            }

            if($temp_list[$key_result]['remaining'] == 0)
            {
                $production_result_list[$key_result] = $temp_list[$key_result];
            }
        }

        return view ('home.production.bank', compact('production_result_list'));
    }

    public function create_production()
    {
        $fishes = $this->FishRepository->get_list();
        $production_results = $this->ProductionResultRepository->get_regular();
        $purchases = Purchase::where('status', 'Approved')->latest()->get();
        $purchase_list[0] = 'Pilih Purchase';
        foreach($purchases as $purchase)
        {
            $purchase_weight = 0;
            foreach($purchase->purchase_detail as $detail)
            {
                $purchase_weight += $detail->amount;
            }

            $production_weight = 0;
            foreach($purchase->production_result as $result)
            {
                $production_weight += $result->weight;
            }

            if($production_weight < $purchase_weight)
            {
                $purchase_list[$purchase->id] = '['.$purchase->doc_code.' | '.date("d F Y h:i", strtotime($purchase->purchase_time)).'] '.$purchase->purchase_product->product->name.' '.number_format($production_weight, 2).' kg / '.number_format($purchase_weight, 2).'kg';
            }
        }
        // dd($purchase_list);
        return view ('home.production.form', compact('fishes', 'production_results', 'purchase_list'));
    }

    public function store_production(ProductionResultRequest $request)
    {
        $production_result = new ProductionResult;
        $production_result->purchase_id = $request->input('purchase_id');
        $production_result->production_id = $request->input('production_id');
        $production_result->weight = $request->input('weight');
        $production_result->block_weight = $request->input('block_weight');
        $production_result->started_at = $request->input('started_at');
        $production_result->criteria = $request->input('criteria');
        $production_result->mst_carton = $request->input('mst_carton');
        $production_result->created_by = Auth::id();
        $production_result->save();
        return redirect()->action('Home\ProductionController@create_results', $production_result->id);
    }

    public function edit_production($id)
    {
        $production_result = $this->ProductionResultRepository->get_one($id);
        return view ('home.production.form', compact('production_result'));
    }

    public function update_production($id, Request $request)
    {
        $production_result = ProductionResult::find($id);
        $production_result->weight = $request->input('weight');
        $production_result->block_weight = $request->input('block_weight');
        $production_result->started_at = $request->input('started_at');
        $production_result->criteria = $request->input('criteria');
        $production_result->mst_carton = $request->input('mst_carton');
        $production_result->updated_by = Auth::id();
        $production_result->save();

        return redirect()->action('Home\ProductionController@summary', $id);
    }

    public function create_results($id)
    {
        $production_result = ProductionResult::find($id);
        return view ('home.production.form_results', compact('production_result'));
    }

    public function store_results($id, Request $request)
    {
        $production_result = ProductionResult::find($id);
        $production_result->finished_at = date("Y-m-d", strtotime($request->input('finish_date')));
        $production_result->save();

        if($request->input('main_product_cart'))
        {
            foreach($request->input('main_product_cart') as $main_key => $main_product)
            {
                $production_result_detail = new ProductionResultDetail;
                $production_result_detail->production_result_id = $id;
                $production_result_detail->size = $main_product['size'];
                $production_result_detail->block = $main_product['block'];
                $production_result_detail->created_by = Auth::id();
                $production_result_detail->save();
            }
        }

        if($request->input('additional_product_cart'))
        {
            foreach($request->input('additional_product_cart') as $additional_key => $additional_product)
            {
                $production_result_addition = new ProductionResultAddition;
                $production_result_addition->production_result_id = $id;
                $production_result_addition->reject_type = $additional_product['reject_type'];
                $production_result_addition->reject_reason = $additional_product['reject_reason'];
                $production_result_addition->size = $additional_product['size'];
                $production_result_addition->block_weight = $additional_product['block_weight'];
                $production_result_addition->block = $additional_product['block'];
                $production_result_addition->created_by = Auth::id();
                $production_result_addition->save();
            }
        }

        if($request->input('waste_product_cart'))
        {
            $production_result_waste = new ProductionResultWaste;
            $production_result_waste->production_result_id = $id;
            $production_result_waste->created_by = Auth::id();
            $production_result_waste->save();
            foreach($request->input('waste_product_cart') as $waste_key => $waste_product)
            {
                $production_result_waste_detail = new ProductionResultWasteDetail;
                $production_result_waste_detail->waste_id = $production_result_waste->id;
                $production_result_waste_detail->name = $waste_product['name'];
                $production_result_waste_detail->value = $waste_product['weight'];
                $production_result_waste_detail->created_by = Auth::id();
                $production_result_waste_detail->save();
            }   
        }

        $production_result = ProductionResult::find($id);
        $production_result->status = 3;
        $production_result->save();

        return redirect()->action('Home\ProductionController@summary', $id);
    }

    public function edit_results($id)
    {
        $production_result = ProductionResult::find($id);
        foreach($production_result->production_result_detail as $main_key => $main_product)
        {
            $results['main_product'][$main_key] = $main_product;
        }
        foreach($production_result->production_result_addition as $additional_key => $additional_product)
        {
            $results['additional_product'][$additional_key] = $additional_product;
        }
        foreach($production_result->waste as $waste_key => $waste)
        {
            foreach($waste->waste_detail as $waste_detail_key => $waste_detail)
            {
                $results['waste_product'][$waste_key][$waste_detail_key] = $waste_detail;
            }
        }

        return view ('home.production.form_results', compact('production_result', 'results'));
    }

    public function update_results($id, Request $request)
    {
        $production_result = ProductionResult::find($id);
        $production_result->finished_at = date("Y-m-d", strtotime($request->input('finish_date')));
        $production_result->save();

        $production_result_details = ProductionResultDetail::where('production_result_id', $id)->get();
        foreach($production_result_details as $deleted_detail)
        {
            $deleted_detail->deleted_by = Auth::id();
            $deleted_detail->save();
            $deleted_detail->delete();
        }

        if($request->input('main_product_cart'))
        {
            foreach($request->input('main_product_cart') as $main_key => $main_product)
            {
                $production_result_detail = new ProductionResultDetail;
                $production_result_detail->production_result_id = $id;
                $production_result_detail->size = $main_product['size'];
                $production_result_detail->block = $main_product['block'];
                $production_result_detail->created_by = Auth::id();
                $production_result_detail->save();
            }
        }

        $production_result_additions = ProductionResultAddition::where('production_result_id', $id)->get();
        foreach($production_result_additions as $deleted_addition)
        {
            $deleted_addition->deleted_by = Auth::id();
            $deleted_addition->save();
            $deleted_addition->delete();
        }

        if($request->input('additional_product_cart'))
        {
            foreach($request->input('additional_product_cart') as $additional_key => $additional_product)
            {
                $production_result_addition = new ProductionResultAddition;
                $production_result_addition->production_result_id = $id;
                $production_result_addition->reject_type = $additional_product['reject_type'];
                $production_result_addition->reject_reason = $additional_product['reject_reason'];
                $production_result_addition->size = $additional_product['size'];
                $production_result_addition->block_weight = $additional_product['block_weight'];
                $production_result_addition->block = $additional_product['block'];
                $production_result_addition->created_by = Auth::id();
                $production_result_addition->save();
            }
        }

        if($request->input('waste_product_cart'))
        {
            $production_result_waste = new ProductionResultWaste;
            $production_result_waste->production_result_id = $id;
            $production_result_waste->updated_by = Auth::id();
            $production_result_waste->save();
            foreach($request->input('waste_product_cart') as $waste_key => $waste_product)
            {
                $production_result_waste_detail = new ProductionResultWasteDetail;
                $production_result_waste_detail->waste_id = $production_result_waste->id;
                $production_result_waste_detail->name = $waste_product['name'];
                $production_result_waste_detail->value = $waste_product['weight'];
                $production_result_waste_detail->created_by = Auth::id();
                $production_result_waste_detail->save();
            }   
        }

        return redirect()->action('Home\ProductionController@summary', $id);
    }

    public function create_waste($id)
    {
        $production_results = $this->ProductionResultRepository->get_regular();
        $production_result = $this->ProductionResultRepository->get_one($id);
        return view ('home.production.variable', compact('production_results', 'production_result'));
    }

    public function store_waste($id, Request $request)
    {
        $this->ProductionResultVariableRepository->store($id, $request->input('waste'));
        $production_result = ProductionResult::find($id);
        $production_result->status = 1;
        $production_result->save();
        return redirect()->action('Home\ProductionController@create_result', $id);
    }

    public function edit_waste($id)
    {
        $production_result = $this->ProductionResultRepository->get_one($id);
        $production_result_variables = $production_result->production_result_variable;

        return view ('home.production.variable', compact('production_result', 'production_result_variables'));
    }

    public function update_waste($id, Request $request)
    {
        $this->ProductionResultVariableRepository->store($id, $request->input('waste'));
        return redirect()->action('Home\ProductionController@summary', $id);
    }

    public function create_result($id)
    {
        $production_result = $this->ProductionResultRepository->get_one($id);
        return view ('home.production.result', compact('production_result'));
    }

    public function store_result($id, ResultRequest $request)
    {
        $production_result = ProductionResult::find($id);
        $production_result->status = 2;
        $production_result->save();
        if(!$request->input('size') || !$request->input('block'))
        {
            return redirect()->action('Home\ProductionController@create_result', $id)->withErrors('Cart must not be empty!');
        } else {
            $production_result = $this->ProductionResultRepository->store_result($id, $request->input('finished_at'), $request->input('uom_id'), $request->input('size'), $request->input('block'));
            return redirect()->action('Home\ProductionController@create_reject', $id);
        }
    }

    public function edit_result($id)
    {
        $production_result = $this->ProductionResultRepository->get_one($id);
        $details = $production_result->production_result_detail;
        return view ('home.production.result', compact('production_result', 'details'));
    }

    public function update_result($id, ResultRequest $request)
    {
        if(!$request->input('size') || !$request->input('block'))
        {
            return redirect()->action('Home\ProductionController@create_result', $id)->withErrors('Cart must not be empty!');
        } else {
            $production_result = $this->ProductionResultRepository->store_result($id, $request->input('finished_at'), $request->input('uom_id'), $request->input('size'), $request->input('block'));
            return redirect()->action('Home\ProductionController@summary', $id);
        }
    }

    public function create_reject($id)
    {
        $production_result = $this->ProductionResultRepository->get_one($id);
        $reject_reason_list = $this->RejectReasonRepository->get_list();
        $reject_type_list = RejectType::pluck('name', 'id');
        return view ('home.production.reject', compact('production_result', 'reject_reason_list', 'reject_type_list'));
    }

    public function store_reject($id, Request $request)
    {
        $this->ProductionResultRepository->store_addition($id, $request->input());
        $production_result = ProductionResult::find($id);
        $production_result->status = 3;
        $production_result->save();
        return redirect()->action('Home\ProductionController@summary', $id);
    }

    public function edit_reject($id)
    {
        $production_result = $this->ProductionResultRepository->get_one($id);
        $reject_reason_list = $this->RejectReasonRepository->get_list();
        $reject_type_list = RejectType::pluck('name', 'id');
        $additionals = $production_result->production_result_addition;
        return view ('home.production.reject', compact('production_result', 'reject_reason_list', 'reject_type_list', 'additionals'));
    }

    public function update_reject($id, Request $request)
    {
        $this->ProductionResultRepository->store_addition($id, $request->input());
        return redirect()->action('Home\ProductionController@summary', $id);
    }

    public function summary($id)
    {
        $sc = new StorageController;

        $production_result = $this->ProductionResultRepository->get_one($id);
        $result = [
            'id' => $production_result->id,
            'material' => $production_result->production->fish->name,
            'product' => $production_result->production->name, 
            'status' => $production_result->status,
            'weight' => $production_result->weight, 
            'block_weight' => $production_result->block_weight, 
            'mst_carton' => $production_result->mst_carton, 
            'start_date' => date("D, d M Y", strtotime($production_result->started_at)),
            'finish_date' => date("D, d M Y", strtotime($production_result->finished_at)), 
            'total_waste' => 0,
            'total_addition' => 0, 
            'total_detail' => 0, 
            'total_miss' => 0, 
        ];

        $arrayValStore['production_id'] = $production_result->id;
        $arrayValStore['production_name'] = $production_result->production->name;
        $arrayValStore['production_date'] = $production_result->finished_at;
        $arrayValStore['doc_code'] = 'AUTO_STORE_'.$production_result->id;

        foreach($production_result->waste as $waste_key => $waste)
        {
            foreach($waste->waste_detail as $waste_detail_key => $waste_detail)
            {
                $result['waste'][$waste_key][$waste_detail_key] = [
                    'name' => $waste_detail->name, 
                    'value' => $waste_detail->value,
                ];

                $result['total_waste'] += $result['waste'][$waste_key][$waste_detail_key]['value'];
            }
        }

        // dd($result['waste']);

        foreach($production_result->production_result_addition as $addition_key => $addition)
        {
            $used_addition = 0; 
            $add_addition = 0;
            foreach($addition->from as $from)
            {
                $used_addition += $from->amount;
            }
            foreach($addition->to as $to)
            {
                $add_addition += $to->amount;
            }
            $result['addition'][$addition_key] = [
                'id' => $addition->id,
                'size' => $addition->size, 
                'reject_type' => $addition->reject_type, 
                'reject_reason' => $addition->reject_reason, 
                'block' => $addition->block - $used_addition + $add_addition, 
                'block_weight' => $addition->block_weight,
                'mc' => floor(($addition->block - $used_addition + $add_addition) / $production_result->mst_carton),
                'unpack' => ($addition->block - $used_addition + $add_addition) % $production_result->mst_carton, 
            ];

            $arrayValStoreDetail['source_type'] = 2;
            $arrayValStoreDetail['source_id'] = $addition->id;
            $arrayValStoreDetail['id_item'] = $sc->getItem($production_result->production->name, $addition->size, $addition->reject_type);
            $arrayValStoreDetail['unit'] = "Block";
            $arrayValStoreDetail['quantity'] = $addition->block;

            $arrayValStore['detail'][] = $arrayValStoreDetail;

            $result['total_addition'] += $result['addition'][$addition_key]['block'] * $result['addition'][$addition_key]['block_weight'];
        }

        foreach($production_result->production_result_detail as $detail_key => $detail)
        {
            $used_detail = 0;
            $add_detail = 0;
            foreach($detail->from as $from)
            {
                $used_detail += $from->amount;
            }
            foreach($detail->to as $to)
            {
                $add_detail += $to->amount;
            }
            $result['detail'][$detail_key] = [
                'id' => $detail->id,
                'size' => $detail->size, 
                'value' => $detail->block - $used_detail + $add_detail, 
                'mc' => floor(($detail->block - $used_detail + $add_detail) / $production_result->mst_carton), 
                'unpack' => ($detail->block - $used_detail + $add_detail) % $production_result->mst_carton,
            ];

            $arrayValStoreDetail['source_type'] = 1;
            $arrayValStoreDetail['source_id'] = $detail->id;
            $arrayValStoreDetail['id_item'] = $sc->getItem($production_result->production->name, $detail->size, "Good");
            $arrayValStoreDetail['unit'] = "Block";
            $arrayValStoreDetail['quantity'] = $detail->block;

            $arrayValStore['detail'][] = $arrayValStoreDetail;

            $result['total_detail'] += $result['detail'][$detail_key]['value'] * $result['block_weight'];
        }
        $result['total_miss'] = $result['weight'] - ($result['total_waste'] + $result['total_addition'] + $result['total_detail']);

        // dd($result);
        // dd($arrayValStore);
        $pid = $production_result->id;
        $pStatus = $production_result->status;
        $tsByPID = TransactionStorage::where('production_id', $pid)->get();
        $varEnable = 1;
        $disable = NULL;
        $ctsByPID = TransactionStorage::where('production_id', $pid)->count();
        foreach ($tsByPID as $ts) {
            if($ts->approved == 0){
                $varEnable = 0;
            }
        }
        if ($ctsByPID < 1) {
            $varEnable = 0;
        }
        if (($pStatus >= 4) AND ($varEnable==1)) {
            $disable = 'disabled';
        }

        

        $ps = PlacementStorage::pluck('address', 'id');
        $avr = json_encode($arrayValStore);
        return view ('home.production.summary', compact('result'), compact('avr'))->with('ds', $ps)->with('disable', $disable);
    }   

    public function transaction($id)
    {
        $result = $this->ProductionResultRepository->get_one($id);
        return view ('home.production.transaction', compact('result'));
    }

    public function transaction_det($id)
    {
        $detail = ProductionResultDetail::find($id);
        $details = ProductionResultDetail::where('size', $detail->size)->where('id', '!=', $id)->get();
        foreach($details as $key => $item)
        {
            $used_total = 0;
            foreach($item->from as $from)
            {
                $used_total += $from->amount;
            }
            $add_total = 0;
            foreach($item->to as $to)
            {
                $add_total += $to->amount;
            }
            if((($detail->block - $used_total + $add_total) % $item->production_result->mst_carton) > 0)
            {
                $detail_list[$key] = [
                    'id' => $item->id,
                    'product' => $item->production_result->production->name,
                    'size' => $item->size, 
                    'start_date' => date("D, d M Y", strtotime($item->production_result->started_at)),
                    'finish_date' => date("D, d M Y", strtotime($item->production_result->finished_at)),
                    'mst_carton' => $item->production_result->mst_carton,
                    'block' => $item->block - $used_total + $add_total,
                    'carton' => floor(($item->block - $used_total + $add_total) / $item->production_result->mst_carton),
                    'unpack' => ($item->block - $used_total + $add_total) % $item->production_result->mst_carton,
                ];
            }
        }
        // dd($detail_list);
        return view ('home.production.transaction_det', compact('detail', 'detail_list'));
    }

    public function transaction_det_store($id, Request $request)
    {
        if($request->input('amount') > 0)
        {
            $det_trs_history = new DetailTransactionHistory;
            $det_trs_history->from_id = $id;
            $det_trs_history->to_id = $request->input('detail_id');
            $det_trs_history->amount = $request->input('amount');
            $det_trs_history->save();
        }

        return redirect()->action('Home\ProductionController@transaction_det_report', $det_trs_history->id);
    }

    public function transaction_det_report($id)
    {
        $det_trs_history = DetailTransactionHistory::find($id);

        $before = $det_trs_history->from->block;
        foreach($det_trs_history->from->from as $used)
        {
            $before -= $used->amount;
        }
        foreach($det_trs_history->from->to as $add)
        {
            $before += $add->amount;
        }

        $after = $det_trs_history->to->block;
        foreach($det_trs_history->to->from as $used)
        {
            $after -= $used->amount;
        }
        foreach($det_trs_history->to->to as $add)
        {
            $after += $add->amount;
        }

        return view ('home.production.transaction_det_report', compact('det_trs_history', 'before', 'after'));
    }

    public function transaction_add($id)
    {
        $addition = ProductionResultAddition::find($id);
        $additions = ProductionResultAddition::where('size', $addition->size)->where('id', '!=', $id)->get();
        foreach($additions as $key => $item)
        {
            $used_total = 0;
            foreach($item->from as $from)
            {
                $used_total += $from->amount;
            }
            $add_total = 0;
            foreach($item->to as $to)
            {
                $add_total += $to->amount;
            }
            if((($addition->block - $used_total + $add_total) % $item->production_result->mst_carton) > 0)
            {
                $addition_list[$key] = [
                    'id' => $item->id,
                    'product' => $item->production_result->production->name,
                    'size' => $item->size, 
                    'start_date' => date("D, d M Y", strtotime($item->production_result->started_at)),
                    'finish_date' => date("D, d M Y", strtotime($item->production_result->finished_at)),
                    'mst_carton' => $item->production_result->mst_carton,
                    'block' => $item->block - $used_total + $add_total,
                    'carton' => floor(($item->block - $used_total + $add_total) / $item->production_result->mst_carton),
                    'unpack' => ($item->block - $used_total + $add_total) % $item->production_result->mst_carton,
                ];
            }
        }
        // dd($addition_list);
        return view ('home.production.transaction_add', compact('addition', 'addition_list'));
    }

    public function transaction_add_store($id, Request $request)
    {
        if($request->input('amount') > 0)
        {
            $add_trs_history = new AdditionTransactionHistory;
            $add_trs_history->from_id = $id;
            $add_trs_history->to_id = $request->input('addition_id');
            $add_trs_history->amount = $request->input('amount');
            $add_trs_history->save();
        }

        return redirect()->action('Home\ProductionController@transaction_add_report', $add_trs_history->id);
    }

    public function transaction_add_report($id)
    {
        $add_trs_history = AdditionTransactionHistory::find($id);

        $before = $add_trs_history->from->block;
        foreach($add_trs_history->from->from as $used)
        {
            $before -= $used->amount;
        }
        foreach($add_trs_history->from->to as $add)
        {
            $before += $add->amount;
        }

        $after = $add_trs_history->to->block;
        foreach($add_trs_history->to->from as $used)
        {
            $after -= $used->amount;
        }
        foreach($add_trs_history->to->to as $add)
        {
            $after += $add->amount;
        }

        return view ('home.production.transaction_add_report', compact('add_trs_history', 'before', 'after'));
    }

    public function close_production_result($id)
    {
        $production_result = ProductionResult::find($id);
        $production_result->status = 4;
        $production_result->save();
        // return redirect()->action('XHRController@autoStore', [$production_result->production->name.' '.$production_result->criteria, $production_result->production_result_detail]);
        return redirect()->action('Home\ProductionController@summary', $id);
    }

    public function pdf($id)
    {
        $production_result = $this->ProductionResultRepository->get_one($id);
        //Generate the view
        $pdf = \PDF::loadView('editor.production_result.pdf', compact('production_result'));
        //Setup the pdf name & formatting
        $name = "Summary ".$production_result->id.".pdf";
        $pdf->setPaper('a4', 'portrait');
        //Return download link
        return $pdf->download($name);
    }

    public function all_productions()
    {
        $production_results = $this->ProductionResultRepository->get_regular_paginate();
        return view ('home.production.productions', compact('production_results'));
    }

    public function create_export($id)
    {

    }

    public function store_export($id, Request $request)
    {

    }

    public function create_sashimi($id)
    {
        $product_rslt = $this->ProductionResultRepository->get_one($id);
        return view ('home.production.sashimi_form', compact('product_rslt'));
    }

    public function store_sashimi($id, Request $request)
    {
        dd($request->input());
    }
}
