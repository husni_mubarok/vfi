<?php

namespace App\Http\Controllers\Home;

use DB;
use Illuminate\Http\Request;
use App\Http\Requests\PurchaseRequest;
use App\Http\Requests\PriceRequest;
use App\Http\Requests\PurchaseDetailRequest;
use App\Http\Controllers\Controller;
use App\Repository\PurchaseRepository;
use App\Repository\ProductRepository;
use App\Repository\SupplierRepository;
use App\Repository\PriceRepository;
use App\Repository\PurchaseDetailRepository;
use App\Model\Purchase;
use App\Model\Size;
use App\Model\Wip;

class WipController extends Controller
{
    protected $PurchaseRepository;
    protected $ProductRepository;
    protected $SupplierRepository;
    protected $PriceRepository;
    protected $PurchaseDetailRepository;

    public function __construct(PurchaseRepository $purchase_repository, ProductRepository $product_repository, SupplierRepository $supplier_repository, PriceRepository $price_repository, PurchaseDetailRepository $purchase_detail_repository)
    {
        $this->PurchaseRepository = $purchase_repository;
        $this->ProductRepository = $product_repository;
        $this->SupplierRepository = $supplier_repository;
        $this->PriceRepository = $price_repository;
        $this->PurchaseDetailRepository = $purchase_detail_repository;
    }

    public function index()
    {
        $project_list = Purchase::groupBy('project')->pluck('project')->toArray();
        array_unshift($project_list, 'Actual Delivery');

        $pending_approval = Purchase::where('status', 'Waiting COO')->orWhere('status', 'Waiting QC')->latest()->get();

        return view ('home.wip.index', compact('pending_approval', 'project_list'));
    }

    public function create($id)
    { 
        $size_list = Size::all()->pluck('note', 'id');

        //dd($id);
        $purchase = Purchase::find($id);
        //dd($purchase);
        $wip = Wip::where('purchase_id', $id)->first();
        //dd($wip);
        return view ('home.wip.form', compact('size_list', 'purchase', 'wip'));
    }
}
