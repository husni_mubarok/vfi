<?php

namespace App\Http\Controllers\Home;

use DB;
use Auth;

use Illuminate\Http\Request;
use App\Http\Requests\TallyPackingTotalMasterRequest;
use App\Http\Controllers\Controller; 
use App\Model\TallyPackingTotalMaster;
use App\Model\QtyType;
use App\Model\CodeSize;
use App\Model\Grade;

class TallyPackingTotalMasterController extends Controller
{
  
    public function index()
    { 

       $sql_detail = 'SELECT
       tally_packing_total_master.id,
       code_size.`name` AS code_size,
       grade.`name` AS grade,
       qty_type.`name` AS qty_type,
       tally_packing_total_master.total
       FROM
       tally_packing_total_master
       LEFT JOIN code_size ON tally_packing_total_master.code_size_id = code_size.id
       LEFT JOIN grade ON tally_packing_total_master.grade_id = grade.id
       LEFT JOIN qty_type ON tally_packing_total_master.qty_type_id = qty_type.id'; 
       $tally_packing_total_master = DB::table(DB::raw("($sql_detail) as rs_sql_detail"))->get();

        //dd($tally_packing_total_master); 

       return view ('home.tally_packing_total_master.index', compact('tally_packing_total_master')); 
   }

   public function create()
   {
    $qty_type_list = QtyType::pluck('name', 'id');
    $code_size_list = CodeSize::pluck('name', 'id');
    $grade_list = Grade::pluck('name', 'id');

    return view ('home.tally_packing_total_master.form', compact('qty_type_list', 'code_size_list', 'grade_list'));
}

public function store(TallyPackingTotalMasterRequest $request)
{
   $tally_packing_total_master = new TallyPackingTotalMaster;
   $tally_packing_total_master->code_size_id = $request->input('code_size_id');
   $tally_packing_total_master->grade_id = $request->input('grade_id');
   $tally_packing_total_master->qty_type_id = $request->input('qty_type_id');
   $tally_packing_total_master->total = $request->input('total');
   $tally_packing_total_master->created_by = Auth::id();
   $tally_packing_total_master->save();

   return redirect()->action('Home\TallyPackingTotalMasterController@index');
}

public function edit($id)
{
   $qty_type_list = QtyType::pluck('name', 'id');
   $code_size_list = CodeSize::pluck('name', 'id');
   $grade_list = Grade::pluck('name', 'id');

   $tally_packing_total_master = TallyPackingTotalMaster::Find($id);

   return view ('home.tally_packing_total_master.form', compact('qty_type_list', 'code_size_list', 'grade_list', 'tally_packing_total_master'));

}

public function update($id, TallyPackingTotalMasterRequest $request)
{
   $tally_packing_total_master = TallyPackingTotalMaster::Find($id);
   $tally_packing_total_master->code_size_id = $request->input('code_size_id');
   $tally_packing_total_master->grade_id = $request->input('grade_id');
   $tally_packing_total_master->qty_type_id = $request->input('qty_type_id');
   $tally_packing_total_master->total = $request->input('total');
   $tally_packing_total_master->created_by = Auth::id();
   $tally_packing_total_master->save();

   return redirect()->action('Home\TallyPackingTotalMasterController@index');
}

public function delete($id)
{ 
   TallyPackingTotalMaster::Find($id)->delete(); 
   return redirect()->action('Home\TallyPackingTotalMasterController@index');
}

}
