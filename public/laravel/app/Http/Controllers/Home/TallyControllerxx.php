<?php

namespace App\Http\Controllers\Home;

use DB;
use Illuminate\Http\Request; 
use App\Http\Requests\TallyRequest;
use App\Http\Controllers\Controller;
use App\Repository\PurchaseRepository;
use App\Repository\ProductRepository;
use App\Repository\SupplierRepository;
use App\Repository\PriceRepository;
use App\Repository\PurchaseDetailRepository;
use App\Model\Purchase;
use App\Model\Size;

//Receiving
use App\Model\Tally;
use App\Model\TallyDetail;
use App\Model\TallyAdditionalMaster; 
use App\Model\TallyDetailAdditional;

class TallyController extends Controller
{
    protected $PurchaseRepository;
    protected $ProductRepository;
    protected $SupplierRepository;
    protected $PriceRepository;
    protected $PurchaseDetailRepository;

    public function __construct(PurchaseRepository $purchase_repository, ProductRepository $product_repository, SupplierRepository $supplier_repository, PriceRepository $price_repository, PurchaseDetailRepository $purchase_detail_repository)
    {
        $this->PurchaseRepository = $purchase_repository;
        $this->ProductRepository = $product_repository;
        $this->SupplierRepository = $supplier_repository;
        $this->PriceRepository = $price_repository;
        $this->PurchaseDetailRepository = $purchase_detail_repository;
    }

    public function index()
    {
        $project_list = Purchase::groupBy('project')->pluck('project')->toArray();
        array_unshift($project_list, 'Actual Delivery');

        $pending_approval = Purchase::where('status', 'Waiting COO')->where('form_status', '0')->orwhere('form_status','')->latest()->get(); 

        //dd($pending_approval);

        return view ('home.tally.index', compact('pending_approval', 'project_list'));
    }

    public function bank()
    {
        $project_list = Purchase::groupBy('project')->pluck('project')->toArray();
        array_unshift($project_list, 'Actual Delivery');

        $pending_approval = Purchase::where('form_status', 1)->latest()->get(); 

        return view ('home.tally.bank', compact('pending_approval', 'project_list'));
    }

    //RECEIVING COLLECT
    public function create($id)
    { 
       
         $purchase = DB::table('products')
        ->join('purchases_products', 'purchases_products.product_id', '=', 'products.id')
        ->join('purchases', 'purchases_products.purchase_id', '=', 'purchases.id')  
        ->select('purchases.id', 'purchases.doc_code', 'purchases.trans_code', 'products.name', 'products.id AS product_id', 'purchases.project')
        ->where('purchases.id', $id) 
        ->first();  

        $tally = Tally::where('purchase_id', $id)->first();  

        return view ('home.tally.form', compact('tally_detail', 'purchase'));
    }

     public function store(Request $request, $id)
    {
          
  
        $purchase = Purchase::find($id);  
        $purchase->action_form_status = 'Edit';
        $purchase->form_status = 1;
        $purchase->save();
 
        return redirect()->action('Home\TallyController@index');
    } 
}
