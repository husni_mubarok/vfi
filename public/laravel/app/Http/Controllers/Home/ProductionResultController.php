<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Requests\ProductionResultRequest;
use App\Http\Requests\ProductionResultVariableRequest;
use App\Http\Requests\RejectRequest;
use App\Http\Requests\ResultRequest;
use App\Http\Controllers\Controller;
use App\Repository\ProductionResultRepository;
use App\Repository\FishRepository;
use App\Repository\ProductionResultVariableRepository;
use App\Repository\RejectRepository;
use App\Repository\RejectReasonRepository;
use App\Repository\RejectRejectReasonRepository;
use PDF;

class ProductionResultController extends Controller
{
    protected $ProductionResultRepository;
    protected $FishRepository;
    protected $ProductionResultVariableRepository;
    protected $RejectRepository;
    protected $RejectReasonRepository;
    protected $RejectRejectReasonRepository;

    public function __construct(ProductionResultRepository $production_result_repository, FishRepository $fish_repository, ProductionResultVariableRepository $production_result_variable_repository, RejectRepository $reject_repository, RejectReasonRepository $reject_reason_repository, RejectRejectReasonRepository $reject_reject_reason_repository)
    {
        $this->ProductionResultRepository = $production_result_repository;
        $this->FishRepository = $fish_repository;
        $this->ProductionResultVariableRepository = $production_result_variable_repository;
        $this->RejectRepository = $reject_repository;
        $this->RejectReasonRepository = $reject_reason_repository;
        $this->RejectRejectReasonRepository = $reject_reject_reason_repository;
    }

    public function index()
    {
        $production_results = $this->ProductionResultRepository->get_all();
        return view ('production', compact('production_results'));
    }

    public function create_production()
    {
        $fishes = $this->FishRepository->get_list();
        $production_results = $this->ProductionResultRepository->get_all();
        return view ('form', compact('fishes', 'production_results'));
    }

    public function store_production(ProductionResultRequest $request)
    {
        $production_result = $this->ProductionResultRepository->store($request->input());
        $this->ProductionResultRepository->update_status($production_result->id, 'On Going');
        return redirect()->action('Home\ProductionResultController@create_waste', $production_result->id);
    }

    public function create_waste($id)
    {
        $production_results = $this->ProductionResultRepository->get_all();
        $production_result = $this->ProductionResultRepository->get_one($id);
        return view ('variable', compact('production_results', 'production_result'));
    }

    public function store_waste($id, ProductionResultVariableRequest $request)
    {
        $this->ProductionResultVariableRepository->store($id, $request->input('value'));
        return redirect()->action('Home\ProductionResultController@create_reject', $id);
    }

    public function edit_waste($id)
    {
        $production_result = $this->ProductionResultRepository->get_one($id);
        $production_result_variables = $production_result->production_result_variable;

        return view ('editor.production_result.variable', compact('production_result', 'production_result_variables'));
    }

    public function update_waste($id, ProductionResultVariableRequest $request)
    {

    }

    public function create_reject($id)
    {
        $production_results = $this->ProductionResultRepository->get_all();
        $production_result = $this->ProductionResultRepository->get_one($id);
        $reject_reasons = $this->RejectReasonRepository->get_list();
        return view ('reject', compact('production_results', 'production_result', 'reject_reasons'));
    }

    public function store_reject($id, RejectRequest $request)
    {
        $reject = $this->RejectRepository->store($id, $request->input());
        if(!empty($request->input('reject_reason')))
        {
            $this->RejectRejectReasonRepository->store($id, $request->input('reject_reason'));
        }
        return redirect()->action('Home\ProductionResultController@create_result', $id);
    }

    public function edit_reject($id)
    {
        $production_result = $this->ProductionResultRepository->get_one($id);
        $reject_reasons = $this->RejectReasonRepository->get_list();
        $reject = $production_result->reject;
        return view ('editor.production_result.reject', compact('production_result', 'reject_reasons', 'reject'));
    }

    public function update_reject($id, RejectRequest $request)
    {
        $reject = $this->RejectRepository->update($id, $request->input());
        if(!empty($request->input('reject_reason')))
        {
            $this->RejectRejectReasonRepository->store($id, $request->input('reject_reason'));
        }
        return redirect()->action('Home\ProductionResultController@index');
    }

    public function create_result($id)
    {
        $production_results = $this->ProductionResultRepository->get_all();
        $production_result = $this->ProductionResultRepository->get_one($id);
        return view ('result', compact('production_results', 'production_result'));
    }

    public function store_result($id, ResultRequest $request)
    {
        $production_result = $this->ProductionResultRepository->store_result($id, $request->input());
        return redirect()->action('Home\ProductionResultController@summary', $id);
    }

    public function edit_result($id)
    {
        $production_result = $this->ProductionResultRepository->get_one($id);
        return view ('production_result.result', compact('production_result'));
    }

    public function update_result(ResultRequest $request)
    {
        $production_result = $this->ProductionResultRepository->store_result($id, $request->input());
        return redirect()->action('Home\ProductionResultRepository@index');
    }

    public function summary($id)
    {
        $production_results = $this->ProductionResultRepository->get_all();
        $production_result = $this->ProductionResultRepository->get_one($id);
        return view ('summary', compact('production_results', 'production_result'));
    }   

    public function close_production_result($id)
    {
        $this->ProductionResultRepository->update_status($id, 'Done');
        return redirect()->action('Home\ProductionResultController@summary', $id);
    }

    public function pdf($id)
    {
        $production_result = $this->ProductionResultRepository->get_one($id);
        //Generate the view
        $pdf = \PDF::loadView('editor.production_result.pdf', compact('production_result'));
        //Setup the pdf name & formatting
        $name = "Summary ".$production_result->id.".pdf";
        $pdf->setPaper('a4', 'portrait');
        //Return download link
        return $pdf->download($name);
    }

    public function all_productions()
    {
        $production_results = $this->ProductionResultRepository->get_all();
        return view ('productions', compact('production_results'));
    }
}
