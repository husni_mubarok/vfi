<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ProductionResult;
use App\Model\ProductionResultAddition;
use App\Model\ProductionResultDetail;
use App\Model\TempStorage;
use App\Model\Production;
use App\Model\SashimiBOM;
use App\Model\SashimiResult;
use App\Model\RejectType;
use App\Model\RejectReason;
use Datatables;

class StorageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      // $rejectType = RejectType::all()->pluck('name', 'id');
      // $rejectReason = RejectReason::all()->pluck('name', 'id');
      // // dd($rejectReason);
      // $storage = new TempStorage;
      //
      // $storageResult = $storage->with('production_result')
      //         ->with('production_result_detail')
      //         ->with('production_result_addition')->where('transaction_type', 'In')->get();
      // // dd($storageResult);
      // $pr = Production::all()->pluck('name', 'id');
      //
      // $ts = $storage->selectRaw('sum(quantity) as total, source_id, transaction_type, production_id')
      // ->groupBy('transaction_type')
      // ->groupBy('source_id')
      // ->groupBy('production_id')->get();
      //
      // $rslty="";
      //
      // $idsk = $storage->selectRaw('source_id')->groupBy('source_id')->get();
      // // dd($idsk);
      // foreach ($idsk as $ik) {
      //   // print_r($ik->source_id);
      //   $stkin = 0;
      //   $stkout = 0;
      //   foreach ($ts as $tse) {
      //     if($tse->source_id == $ik->source_id){
      //       if($tse->transaction_type == "In"){
      //         $stkin = $stkin + $tse->total;
      //       }
      //       if($tse->transaction_type == "Out"){
      //         $stkout = $stkout + $tse->total;
      //       }
      //     }
      //   }
      //   $curstok = $stkin - $stkout;
      //   $st = $storage->with('production_result')
      //       ->with('production_result_detail')
      //       ->with('production_result_addition')
      //       ->where('source_id', $ik->source_id)
      //       ->first();
      //   $nameProduct = Production::where('id', $st->production_result->production_id)->first();
      //   // dd($st);
      //
      //   // print_r($ik->source_id.'\n');
      //   $rsltx['nameProduct'] = $nameProduct->name;
      //   $rsltx['size'] = 0;
      //   if($st->source_type == 1){
      //     $rsltx['type'] = "detail/Good";
      //     $cprx = ProductionResultDetail::where('id', $ik->source_id)->count();
      //     if($cprx > 0){
      //       $prx = ProductionResultDetail::where('id', $ik->source_id)->first();
      //       $rsltx['size'] = $prx->size;
      //     }
      //   }else if($st->source_type == 2){
      //     $rsltx['type'] = "addition/Bad";
      //     $cpra = ProductionResultAddition::where('id', $ik->source_id)->count();
      //     if($cpra > 0){
      //       $pra = ProductionResultAddition::where('id', $ik->source_id)->first();
      //       $rsltx['size'] = $pra->size;
      //     }
      //   }else if($st->source_type == 5){
      //     $rsltx['type'] = "detail/Good";
      //     $csr = SashimiResult::where('id', $ik->source_id)->count();
      //     if($csr > 0){
      //       $sr = SashimiResult::where('id', $ik->source_id)->first();
      //       $rsltx['size'] = $sr->size;
      //     }
      //   }else if($st->source_type == 6){
      //     $rsltx['type'] = "detail/Good";
      //     $csb = SashimiBOM::where('id', $ik->source_id)->count();
      //     if($csb > 0){
      //       $sb = SashimiBOM::where('id', $ik->source_id)->first();
      //       $rsltx['size'] = $sb->size;
      //     }
      //   }else{
      //     $rsltx['type'] = "detail/Good";
      //     $rsltx['size'] = "-";
      //   }
      //
      //   // dd($rsltx);
      //   $rsltx['source_id'] = $ik->source_id;
      //
      //   $rsltx['stokout_export'] = 0;
      //   $rsltx['stokout_valueadded'] = 0;
      //   $rsltx['stokout_sashimi'] = 0;
      //   $rsltx['stokout_sashimiBo'] = 0;
      //   $cstorageOut = TempStorage::where('source_id', $ik->source_id)->where('transaction_type', 'Out')->count();
      //   // print($cstorageOut.'$');
      //   if($cstorageOut > 0){
      //     $storageOut = TempStorage::where('source_id', $ik->source_id)->where('transaction_type', 'Out')->get();
      //     // print_r($storageOut);
      //     foreach ($storageOut as $so) {
      //       // print_r($so->source_type.'|');
      //       if($so->source_type == 3){
      //         $rsltx['stokout_export'] = $so->quantity;
      //       }
      //       if($so->source_type == 4){
      //         $rsltx['stokout_valueadded'] = $so->quantity;
      //       }
      //       if($so->source_type == 5){
      //         $rsltx['stokout_sashimi'] = $so->quantity;
      //       }
      //       if($so->source_type == 6){
      //         $rsltx['stokout_sashimiBo'] = $so->quantity;
      //       }
      //     }
      //   }
      //
      //
      //   $rsltx['stokin'] = $stkin;
      //   $rsltx['stokout'] = $stkout;
      //   $rsltx['stokcur'] = $curstok;
      //   $rsltx['dateProduksiStart'] = $st->production_result->started_at;
      //   $rsltx['dateProduksiFinish'] = $st->production_result->finished_at;
      //   // print_r($nameProduct->name);
      //   // print_r("|");
      //   // print_r($st->source_type);
      //   // print_r("|");
      //   // print_r($ik->source_id);
      //   // print_r("|");
      //   // print_r($stkin);
      //   // print_r("-");
      //   // print_r($stkout);
      //   // print_r("=");
      //   // print_r($curstok);
      //   // print_r('<br>');
      //   $rslty[] = $rsltx;
      // }
      // // dd($rslty);
      // // dd($storageResult);

      return view('home.storage.index');
              // ->with('storageResult', $storageResult)
              // ->with('product', $pr)
              // ->with('rslt', $rslty)
              // ->with('rejectType', $rejectType)
              // ->with('rejectReason', $rejectReason);
    }


    public function dataindex($prefix){

      $rejectType = RejectType::all()->pluck('name', 'id');
      $rejectReason = RejectReason::all()->pluck('name', 'id');
      // dd($rejectReason);
      $storage = new TempStorage;

      $storageResult = $storage->with('production_result')
              ->with('production_result_detail')
              ->with('production_result_addition')->where('transaction_type', 'In')->get();
      // dd($storageResult);
      $pr = Production::all()->pluck('name', 'id');

      $ts = $storage->selectRaw('sum(quantity) as total, source_id, transaction_type, production_id')
      ->groupBy('transaction_type')
      ->groupBy('source_id')
      ->groupBy('production_id')->get();

      $rslty="";

      $idsk = $storage->selectRaw('source_id')->groupBy('source_id')->get();
      // dd($idsk);
      foreach ($idsk as $ik) {
        // print_r($ik->source_id);
        $stkin = 0;
        $stkout = 0;
        foreach ($ts as $tse) {
          if($tse->source_id == $ik->source_id){
            if($tse->transaction_type == "In"){
              $stkin = $stkin + $tse->total;
            }
            if($tse->transaction_type == "Out"){
              $stkout = $stkout + $tse->total;
            }
          }
        }
        $curstok = $stkin - $stkout;
        $st = $storage->with('production_result')
            ->with('production_result_detail')
            ->with('production_result_addition')
            ->where('source_id', $ik->source_id)
            ->first();
        $nameProduct = Production::where('id', $st->production_result->production_id)->first();
        // dd($st);

        // print_r($ik->source_id.'\n');
        $rsltx['nameProduct'] = $nameProduct->name;
        $rsltx['size'] = 0;
        if($st->source_type == 1){
          $rsltx['type'] = "detail/Good";
          $cprx = ProductionResultDetail::where('id', $ik->source_id)->count();
          if($cprx > 0){
            $prx = ProductionResultDetail::where('id', $ik->source_id)->first();
            $rsltx['size'] = $prx->size;
          }
        }else if($st->source_type == 2){
          $rsltx['type'] = "addition/Bad";
          $cpra = ProductionResultAddition::where('id', $ik->source_id)->count();
          if($cpra > 0){
            $pra = ProductionResultAddition::where('id', $ik->source_id)->first();
            $rsltx['size'] = $pra->size;
          }
        }else if($st->source_type == 5){
          $rsltx['type'] = "detail/Good";
          $csr = SashimiResult::where('id', $ik->source_id)->count();
          if($csr > 0){
            $sr = SashimiResult::where('id', $ik->source_id)->first();
            $rsltx['size'] = $sr->size;
          }
        }else if($st->source_type == 6){
          $rsltx['type'] = "detail/Good";
          $csb = SashimiBOM::where('id', $ik->source_id)->count();
          if($csb > 0){
            $sb = SashimiBOM::where('id', $ik->source_id)->first();
            $rsltx['size'] = $sb->size;
          }
        }else{
          $rsltx['type'] = "detail/Good";
          $rsltx['size'] = "-";
        }

        // dd($rsltx);
        $rsltx['source_id'] = $ik->source_id;

        $rsltx['stokout_export'] = 0;
        $rsltx['stokout_valueadded'] = 0;
        $rsltx['stokout_sashimi'] = 0;
        $rsltx['stokout_sashimiBo'] = 0;
        $cstorageOut = TempStorage::where('source_id', $ik->source_id)->where('transaction_type', 'Out')->count();
        // print($cstorageOut.'$');
        if($cstorageOut > 0){
          $storageOut = TempStorage::where('source_id', $ik->source_id)->where('transaction_type', 'Out')->get();
          // print_r($storageOut);
          foreach ($storageOut as $so) {
            // print_r($so->source_type.'|');
            if($so->source_type == 3){
              $rsltx['stokout_export'] = $so->quantity;
            }
            if($so->source_type == 4){
              $rsltx['stokout_valueadded'] = $so->quantity;
            }
            if($so->source_type == 5){
              $rsltx['stokout_sashimi'] = $so->quantity;
            }
            if($so->source_type == 6){
              $rsltx['stokout_sashimiBo'] = $so->quantity;
            }
          }
        }


        $rsltx['stokin'] = $stkin;
        $rsltx['stokout'] = $stkout;
        $rsltx['stokcur'] = $curstok;
        $rsltx['dateProduksiStart'] = $st->production_result->started_at;
        $rsltx['dateProduksiFinish'] = $st->production_result->finished_at;
        $rslty[] = $rsltx;
      }

      // dd($rslty);


      foreach ($storageResult as $strgRslt) {
        $dataRow["id"] = $strgRslt->id;
        $dataRow["name"] = $pr[$strgRslt->production_result->production_id];

        //sizeBlock
        $totalQty = 0;
        $stringBlock = "";
        if($strgRslt->source_type == 1){
          foreach($strgRslt->production_result_detail as $sprd){
            $totalQty = $totalQty + $sprd->block;
            if($strgRslt->source_id == $sprd->id){
              $stringBlock .= $sprd->size."->".$strgRslt->quantity." Block";
              $stringCondition = "Good";
            }
          }
        }
        if($strgRslt->source_type == 2){
          foreach($strgRslt->production_result_addition as $spra){
            $totalQty = $totalQty + $spra->block;
            if($strgRslt->source_id == $spra->id){
              $stringBlock .= $spra->size." -> ".$strgRslt->quantity." Block ";
              $stringCondition = $rejectType[$spra->reject_type_id]." | ".$rejectReason[$spra->reject_reason_id];
            }
          }
        }
        $dataRow["condition"] = $stringCondition;
        $dataRow["sizeblock"] =  $stringBlock;
        $dataRow["block"] = $strgRslt->production_result->block_weight;
        $dataRow["dateProduksiStart"] = $strgRslt->production_result->started_at;
        $dataRow["dateProduksiFinish"] = $strgRslt->production_result->finished_at;
        $dataRow["datetime"] = $strgRslt->datetime;
        $dataRow["quantity"] = $strgRslt->quantity;

        $arrayDataRow[] = $dataRow;
      }

      if($prefix == 1){
        return Datatables::of(collect($rslty))->make(true);
      }else{
        return Datatables::of(collect($arrayDataRow))->make(true);
      }


      // foreach ($storageResult as $key => $value) {
      //   # code...
      // }
      //
      // @foreach($storageResult as $strgRslt){
      //   $dataRow = null;
      //   $dataRow['id'] = $strgRslt->id;
      //   $dataRow['name'] =
      //
      // }

    }

    public function data_trans_index(){

      // $rejectType = RejectType::all()->pluck('name', 'id');
      // $rejectReason = RejectReason::all()->pluck('name', 'id');
      // // dd($rejectReason);
      // $storage = new TempStorage;
      //
      // $storageResult = $storage->with('production_result')
      //         ->with('production_result_detail')
      //         ->with('production_result_addition')->where('transaction_type', 'In')->get();
      // // dd($storageResult);
      // $pr = Production::all()->pluck('name', 'id');
      //
      // $ts = $storage->selectRaw('sum(quantity) as total, source_id, transaction_type, production_id')
      // ->groupBy('transaction_type')
      // ->groupBy('source_id')
      // ->groupBy('production_id')->get();
      //
      // $rslty="";
      //
      // $idsk = $storage->selectRaw('source_id')->groupBy('source_id')->get();
      // // dd($idsk);
      // foreach ($idsk as $ik) {
      //   // print_r($ik->source_id);
      //   $stkin = 0;
      //   $stkout = 0;
      //   foreach ($ts as $tse) {
      //     if($tse->source_id == $ik->source_id){
      //       if($tse->transaction_type == "In"){
      //         $stkin = $stkin + $tse->total;
      //       }
      //       if($tse->transaction_type == "Out"){
      //         $stkout = $stkout + $tse->total;
      //       }
      //     }
      //   }
      //   $curstok = $stkin - $stkout;
      //   $st = $storage->with('production_result')
      //       ->with('production_result_detail')
      //       ->with('production_result_addition')
      //       ->where('source_id', $ik->source_id)
      //       ->first();
      //   $nameProduct = Production::where('id', $st->production_result->production_id)->first();
      //   // dd($st);
      //
      //   // print_r($ik->source_id.'\n');
      //   $rsltx['nameProduct'] = $nameProduct->name;
      //   $rsltx['size'] = 0;
      //   if($st->source_type == 1){
      //     $rsltx['type'] = "detail/Good";
      //     $cprx = ProductionResultDetail::where('id', $ik->source_id)->count();
      //     if($cprx > 0){
      //       $prx = ProductionResultDetail::where('id', $ik->source_id)->first();
      //       $rsltx['size'] = $prx->size;
      //     }
      //   }else if($st->source_type == 2){
      //     $rsltx['type'] = "addition/Bad";
      //     $cpra = ProductionResultAddition::where('id', $ik->source_id)->count();
      //     if($cpra > 0){
      //       $pra = ProductionResultAddition::where('id', $ik->source_id)->first();
      //       $rsltx['size'] = $pra->size;
      //     }
      //   }else if($st->source_type == 5){
      //     $rsltx['type'] = "detail/Good";
      //     $csr = SashimiResult::where('id', $ik->source_id)->count();
      //     if($csr > 0){
      //       $sr = SashimiResult::where('id', $ik->source_id)->first();
      //       $rsltx['size'] = $sr->size;
      //     }
      //   }else if($st->source_type == 6){
      //     $rsltx['type'] = "detail/Good";
      //     $csb = SashimiBOM::where('id', $ik->source_id)->count();
      //     if($csb > 0){
      //       $sb = SashimiBOM::where('id', $ik->source_id)->first();
      //       $rsltx['size'] = $sb->size;
      //     }
      //   }else{
      //     $rsltx['type'] = "detail/Good";
      //     $rsltx['size'] = "-";
      //   }
      //
      //   // dd($rsltx);
      //   $rsltx['source_id'] = $ik->source_id;
      //
      //   $rsltx['stokout_export'] = 0;
      //   $rsltx['stokout_valueadded'] = 0;
      //   $rsltx['stokout_sashimi'] = 0;
      //   $rsltx['stokout_sashimiBo'] = 0;
      //   $cstorageOut = TempStorage::where('source_id', $ik->source_id)->where('transaction_type', 'Out')->count();
      //   // print($cstorageOut.'$');
      //   if($cstorageOut > 0){
      //     $storageOut = TempStorage::where('source_id', $ik->source_id)->where('transaction_type', 'Out')->get();
      //     // print_r($storageOut);
      //     foreach ($storageOut as $so) {
      //       // print_r($so->source_type.'|');
      //       if($so->source_type == 3){
      //         $rsltx['stokout_export'] = $so->quantity;
      //       }
      //       if($so->source_type == 4){
      //         $rsltx['stokout_valueadded'] = $so->quantity;
      //       }
      //       if($so->source_type == 5){
      //         $rsltx['stokout_sashimi'] = $so->quantity;
      //       }
      //       if($so->source_type == 6){
      //         $rsltx['stokout_sashimiBo'] = $so->quantity;
      //       }
      //     }
      //   }
      //
      //
      //   $rsltx['stokin'] = $stkin;
      //   $rsltx['stokout'] = $stkout;
      //   $rsltx['stokcur'] = $curstok;
      //   $rsltx['dateProduksiStart'] = $st->production_result->started_at;
      //   $rsltx['dateProduksiFinish'] = $st->production_result->finished_at;
      //   $rslty[] = $rsltx;
      // }
      //
      // // return view('home.storage.index')
      // //         ->with('storageResult', $storageResult)
      // //         ->with('product', $pr)
      // //         ->with('rslt', $rslty)
      // //         ->with('rejectType', $rejectType)
      // //         ->with('rejectReason', $rejectReason);
      //
      // // dd($rslty);
      //
      //
      //
      //
      //
      // // dd($arrayDataRow);
      // return Datatables::of(collect($arrayDataRow))->make(true);
      // foreach ($storageResult as $key => $value) {
      //   # code...
      // }
      //
      // @foreach($storageResult as $strgRslt){
      //   $dataRow = null;
      //   $dataRow['id'] = $strgRslt->id;
      //   $dataRow['name'] =
      //
      // }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pr = new ProductionResult;
        $detail_pr = $pr->with('production')->with('production_result_detail')->with('production_result_addition')->where('status', 'Done')->get();
        // dd($detail_pr);
        $tp = TempStorage::all();
        return view('home.storage.form')
                ->with('detail_pr', $detail_pr)
                ->with('tp', $tp);
    }

    /**
     * load lookup data
     * @return [type] [description]
     */
    public function data(Request $request){
      if($request->ajax()){
        //Get all data that needed
        $pr = new ProductionResult;
        $detail_pr = $pr->with('production')->with('production_result_detail')->with('production_result_addition')
        ->where('status', 'Done')->orderBy('started_at', 'desc')->get();
        $tp = TempStorage::all();

        $data=[];
        foreach($detail_pr as $key=>$dpr ){
          $production_result = $dpr;
          $totalBlock = 0;
          $quantity_details=[];
          //Production Result Regular product

          $detailHTML="";
          if(count($dpr->production_result_detail)>0){
            $detailHTML.="<strong>Good</strong><br/>";
          }
          foreach($dpr->production_result_detail as $prd){
            $totalkurang = 0;
            foreach($tp as $t){
              if($t->source_type == 1){
                if($t->source_id == $prd->id){
                  $totalkurang = $totalkurang + $t->quantity;
                }
              }
            }

            $actTotal = $prd->block - $totalkurang;
            $detailHTML.=$prd->size."->".$actTotal."<br/>";
            $totalBlock= $totalBlock + $actTotal ;

          }


          //Production Result Additional product
          if(count($dpr->production_result_addition)>0){
            $detailHTML.="<strong>Addition</strong><br/>";
          }
          foreach($dpr->production_result_addition as $pra){
                $totalkurang = 0;
                foreach($tp as $t){
                  if($t->source_type == 2){
                    if($t->source_id == $pra->id){
                      $totalkurang = $totalkurang + $t->quantity;
                    }

                  }
                }
                $actTotal = $pra->block - $totalkurang;
                $detailHTML.=$pra->size."->".$actTotal."<br/>";
                $totalBlock= $totalBlock + $actTotal;

          }
          $production_result['detail_html'] = $detailHTML;
          $production_result['total_block'] = $totalBlock;
          if($totalBlock>0){
            $data[]=$production_result;
          }

          }

          // dd($data);
        // return response()->json($data);
        return Datatables::of(collect($data))
          ->addColumn('detail_html', function ($data) {
                return $data->detail_html;
          })
          ->addColumn('date', function ($data) {
                return 'Start Date: '.$data->started_at.'<br/>Finish Date: '.$data->finished_at;
          })
          ->addColumn('action', function ($data) {
                return '<a href="javascript:void(0)" class="btn btn-success" id="btnstore" onclick="addValue(this,'.$data->id.')">Choose</a>';
          })->make(true);

      }else{
        exit("No data available");
      }


  }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $vas = json_decode($request->input('valArySend'));
        // dd($vas);
        $productionId = $request->input('production');
      foreach ($vas as $v) {
        // print_r($v->srctype);

        $storage = new TempStorage;
        $storage->source_type = $v->srctype;
        $storage->source_id = $v->idval;
        $storage->production_id = $productionId;
        $storage->quantity = $v->storeval;
        $storage->unit = $request->input('unit');
        $storage->weight = $request->input('weight');
        // $storege->transection_type = "In";
        $storage->status = $request->input('status');
        $storage->datetime = $request->input('datetime');
        $storage->status = 0;
        $storage->status_storage_id = 1;
        $storage->save();

      }


      return redirect()->action('Home\StorageController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
