<?php

namespace App\Http\Controllers\Home; 
use DB;
use Illuminate\Http\Request; 
use App\Http\Requests\TallyRequest;
use App\Http\Controllers\Controller;
use App\Repository\PurchaseRepository;
use App\Repository\ProductRepository;
use App\Repository\SupplierRepository;
use App\Repository\PriceRepository;
use App\Repository\PurchaseDetailRepository;
use App\Model\Purchase;
use App\Model\Size;

//Temp Tally
use App\Model\TempTally;

//Receiving
use App\Model\Tally;
use App\Model\TallyDetail;
use App\Model\TallyAdditionalMaster; 
use App\Model\TallyDetailAdditional;
use App\Model\TallyReceivingTreatment;

//Heading
use App\Model\TallyDeheading;
use App\Model\TallyDetailDeheading;
use App\Model\TallyDeheadingMaster;

//Peeling
use App\Model\TallyPeeling;
use App\Model\TallyDetailPeeling;
use App\Model\TallyPeelingMaster;
use App\Model\TallyPeelingTreatment;

//Grading
use App\Model\TallyGradingMaster;
use App\Model\TallyDetailGrading; 
use App\Model\TallyGrading;
use App\Model\CodeSize;
use App\Model\FreezingType;
use App\Model\Grade;
use App\Model\QtyType;

//Packing
use App\Model\TallyPackingDetail;

use Auth;
use App\Model\User;

class TallyReportController extends Controller
{
   

  public function index()
  {

    $temp_tally =  DB::table('temp_tally')->get(); 
    return view ('home.tally_report.index', compact('temp_tally'));
  }
  
public function historyreceiving($id)
{  
 $temp_tally = DB::table('temp_tally') 
 ->where('id', $id) 
 ->first();

 $tally_detail_view_treatment = DB::table('tally_detail')
 ->join('sizes', 'tally_detail.size_id', '=', 'sizes.id')  
 ->select('tally_detail.size_id AS id', 'sizes.note', DB::raw('SUM(tally_detail.weight) as total_weight'))
 ->where('tally_detail.temp_tally_id', $id) 
 ->where('tally_detail.tally_type', 'TREATMENT')   
 ->groupBy('tally_detail.size_id', 'sizes.note')
 ->get(); 

 foreach ($tally_detail_view_treatment as $tdvt) {
   $tally_detail_view_treatment_detail = DB::table('tally_detail')
   ->join('sizes', 'tally_detail.size_id', '=', 'sizes.id')  
   ->select('tally_detail.size_id AS id', 'sizes.note', 'tally_detail.weight')
   ->where('tally_detail.temp_tally_id', $id) 
   ->where('tally_detail.size_id', '=', $tdvt->id)
   ->where('tally_detail.tally_type', 'TREATMENT')  
   ->get(); 
   $array_val["note"] = $tdvt->note;
   $array_val["total_weight"] = $tdvt->total_weight;
   $tdvtd_arrays = null;
   foreach ($tally_detail_view_treatment_detail as $tdvtd) {
    $tdvtd_array = null;
    $tdvtd_array["weight"] = $tdvtd->weight;
    $tdvtd_arrays[] = $tdvtd_array;
  }

  $array_val["detail"] = $tdvtd_arrays;
  $array_all_treatment[] = $array_val;
} 

$tally_detail_view_receiving = DB::table('tally_detail')
->join('sizes', 'tally_detail.size_id', '=', 'sizes.id')  
->select('tally_detail.size_id AS id', 'sizes.note', DB::raw('SUM(tally_detail.weight) as total_weight'))
->where('tally_detail.temp_tally_id', $id) 
->where('tally_detail.tally_type', 'RECEIVING') 
->groupBy('tally_detail.size_id', 'sizes.note')
->get(); 

foreach ($tally_detail_view_receiving as $tdvr) {
  $tally_detail_view_receiving_detail = DB::table('tally_detail')
  ->join('sizes', 'tally_detail.size_id', '=', 'sizes.id')  
  ->select('tally_detail.size_id AS id', 'sizes.note', 'tally_detail.weight')
  ->where('tally_detail.temp_tally_id', $id) 
  ->where('tally_detail.size_id', '=', $tdvr->id)
  ->where('tally_detail.tally_type', 'RECEIVING')  
  ->get(); 
  $array_val_rec["note"] = $tdvr->note;
  $array_val_rec["total_weight"] = $tdvr->total_weight;
  $tdvrd_arrays = null;
  foreach ($tally_detail_view_receiving_detail as $tdvrd) {
    $tdvrd_array = null;
    $tdvrd_array["weight"] = $tdvrd->weight;
    $tdvrd_arrays[] = $tdvrd_array;
  }

  $array_val_rec["detail"] = $tdvrd_arrays;
  $array_all_receiving[] = $array_val_rec;
}

$tally_additional_view = DB::table('tally_detail_additional')
->join('tally_additional_master', 'tally_detail_additional.tally_additional_master_id', '=', 'tally_additional_master.id') 
->join('temp_tally', 'tally_detail_additional.temp_tally_id', '=', 'temp_tally.id')
->select('tally_additional_master.id','tally_additional_master.name', DB::raw('SUM(tally_detail_additional.weight) as total_weight'), DB::raw('SUM(tally_detail_additional.pan) as total_pan'), DB::raw('SUM(tally_detail_additional.rest) as total_rest'))
->where('temp_tally.id', $id) 
->groupBy('tally_detail_additional.tally_additional_master_id', 'tally_additional_master.name')
->get();


$tally_additional_view_c = DB::table('tally_detail_additional')
->join('tally_additional_master', 'tally_detail_additional.tally_additional_master_id', '=', 'tally_additional_master.id')  
->select('tally_additional_master.id','tally_additional_master.name', DB::raw('COUNT(tally_detail_additional.rest) as rest_count'))
->where('tally_detail_additional.temp_tally_id', $id) 
->groupBy('tally_detail_additional.tally_additional_master_id', 'tally_additional_master.name')
->first();

foreach ($tally_additional_view as $tav) {
 $tally_additional_view_detail = DB::table('tally_detail_additional')
 ->join('tally_additional_master', 'tally_detail_additional.tally_additional_master_id', '=', 'tally_additional_master.id') 
 ->select('tally_detail_additional.id','tally_additional_master.name', 'tally_detail_additional.weight', 'tally_detail_additional.pan', 'tally_detail_additional.rest')
 ->where('tally_detail_additional.temp_tally_id', $id)  
 ->where('tally_additional_master.id', '=', $tav->id)  
 ->get();


 $array_val_add["id"] = $tav->id;
 $array_val_add["name"] = $tav->name;
 $array_val_add["total_weight"] = $tav->total_weight;
 $array_val_add["total_pan"] = $tav->total_pan;
 $array_val_add["total_rest"] = $tav->total_rest;
 $tavd_arrays = null;
 foreach ($tally_additional_view_detail as $tavd) {
  $tavd_array = null;
  $tavd_array["id"] = $tavd->id;
  $tavd_array["weight"] = $tavd->weight;
  $tavd_array["pan"] = $tavd->pan;
  $tavd_array["rest"] = $tavd->rest;
  $tavd_arrays[] = $tavd_array;
}

$array_val_add["detail"] = $tavd_arrays;
$array_all_add[] = $array_val_add;
}

$treatment_option = DB::table('tally_receiving_treatment') 
->where('temp_tally_id', $id) 
->get();

        //dd($array_all_add);

return view ('home.tally_report.historyreceiving', compact('tally_detail_view_treatment','tally_detail_view_receiving','tally_additional_view', 'purchase','array_all_treatment','array_all_receiving','array_all_add', 'tally_additional_view_c', 'temp_tally','treatment_option'));
}

 
public function historydeheading($id)
{ 

 $temp_tally = DB::table('temp_tally') 
 ->where('id', $id) 
 ->first();
 
 $tally_detail = TallyDeheadingMaster::all(); 

 $tally_detail_view = DB::table('tally_detail_deheading')
 ->join('tally_deheading_master', 'tally_detail_deheading.tally_dehading_master_id', '=', 'tally_deheading_master.id')  
 ->select('tally_deheading_master.id', 'tally_deheading_master.name', DB::raw('SUM(tally_detail_deheading.weight) as total_weight'), DB::raw('SUM(tally_detail_deheading.waste) as total_waste'))
 ->where('tally_detail_deheading.temp_tally_id', $id) 
 ->groupBy('tally_deheading_master.id', 'tally_deheading_master.name')
 ->get(); 

        //dd($tally_detail_view);

 $tally_detail_view_count = DB::table('tally_detail_deheading')
 ->join('tally_deheading_master', 'tally_detail_deheading.tally_dehading_master_id', '=', 'tally_deheading_master.id')  
 ->select('tally_deheading_master.id', 'tally_deheading_master.name', DB::raw('COUNT(tally_detail_deheading.weight) as count_weight'))
 ->where('tally_detail_deheading.temp_tally_id', $id) 
 ->groupBy('tally_deheading_master.id', 'tally_deheading_master.name')
 ->first();

        //dd($tally_detail_view_count->id);

 foreach ($tally_detail_view as $t_d_v) {
   $tally_detail_view_det = DB::table('tally_detail_deheading')
   ->join('tally_deheading_master', 'tally_detail_deheading.tally_dehading_master_id', '=', 'tally_deheading_master.id')  
   ->select('tally_deheading_master.id', 'tally_deheading_master.name', 'tally_detail_deheading.weight', 'tally_detail_deheading.waste')
   ->where('tally_detail_deheading.temp_tally_id', $id)  
   ->where('tally_deheading_master.id', '=', $t_d_v->id)
   ->get(); 

                        //dd($tally_detail_view_det);

   $array_val["name"] = $t_d_v->name;
   $array_val["total_weight"] = $t_d_v->total_weight;
   $array_val["total_waste"] = $t_d_v->total_waste;
   $tdvtd_arrays = null;
   foreach ($tally_detail_view_det as $tdvtd) {
    $tdvtd_array = null;
    $tdvtd_array["weight"] = $tdvtd->weight;
    $tdvtd_array["waste"] = $tdvtd->waste;
    $tdvtd_arrays[] = $tdvtd_array;
  }

  $array_val["detail"] = $tdvtd_arrays;
  $array_all[] = $array_val;

            //dd($array_all);
}

return view ('home.tally_report.historydeheading', compact('tally_detail', 'tally_detail_view', 'temp_tally', 'array_all', 'tally_detail_view_count'));
}
 

public function historypeeling($id)
{ 

  $temp_tally = DB::table('temp_tally') 
  ->where('id', $id) 
  ->first();

  $tally_detail_view = DB::table('tally_detail_peeling')
  ->join('tally_peeling_master', 'tally_detail_peeling.tally_peeling_master_id', '=', 'tally_peeling_master.id')  
  ->select('tally_detail_peeling.tally_peeling_master_id AS id', 'tally_peeling_master.name', DB::raw('SUM(tally_detail_peeling.weight) as total_weight'))
  ->where('tally_detail_peeling.temp_tally_id', $id) 
  ->groupBy('tally_detail_peeling.tally_peeling_master_id', 'tally_peeling_master.name')
  ->get(); 

  foreach ($tally_detail_view as $t_d_v) {
   $tally_detail_view = DB::table('tally_detail_peeling')
   ->join('tally_peeling_master', 'tally_detail_peeling.tally_peeling_master_id', '=', 'tally_peeling_master.id')  
   ->select('tally_detail_peeling.tally_peeling_master_id AS id', 'tally_peeling_master.name', 'tally_detail_peeling.weight')
   ->where('tally_detail_peeling.temp_tally_id', $id)  
   ->where('tally_detail_peeling.tally_peeling_master_id', '=', $t_d_v->id)
   ->get(); 
   $array_val["name"] = $t_d_v->name;
   $array_val["total_weight"] = $t_d_v->total_weight;
   $tdvtd_arrays = null;
   foreach ($tally_detail_view as $tdvtd) {
    $tdvtd_array = null;
    $tdvtd_array["weight"] = $tdvtd->weight;
    $tdvtd_arrays[] = $tdvtd_array;
  }

  $array_val["detail"] = $tdvtd_arrays;
  $array_all[] = $array_val;
}

$treatment_option = DB::table('tally_peeling_treatment') 
->where('temp_tally_id', $id) 
->get();

  //dd($treatment_option);

return view ('home.tally_report.historypeeling', compact('tally_detail_view', 'purchase', 'array_all', 'temp_tally', 'treatment_option'));
}
  
public function historygrading($id)
{
  $temp_tally = DB::table('temp_tally') 
  ->where('id', $id) 
  ->first(); 

  $grades = Grade::all(); 

  $code_sizes = DB::table('code_size')
  ->select('code_size.id', 'code_size.name', 'code_size.block_weight', 'sizes.note', 'freezing_type.name AS freezing_name')
  ->join('sizes', 'sizes.id', '=', 'code_size.id_size')
  ->join('freezing_type', 'freezing_type.id', '=', 'code_size.id_freezing_type')
  ->get();

  $qty_types = QtyType::all(); 

  $grading_history_sql = '
  SELECT
  temp_tally.trans_code AS trans_code,
  qty_type.name AS qty_name,
  grade.name AS grade,
  sizes.note AS size,
  freezing_type.name AS freezing,
  code_size.name AS code_size,
  code_size.block_weight,
  code_size.block_weight AS block, 
  tally_grading_detail.temp_tally_id AS temp_tally_id,
  tally_grading_detail.id AS tally_grading_detail_id,
  tally_grading_detail.total_block AS total_block,
  tally_grading_detail.rest AS rest
  FROM  tally_grading_detail
  LEFT JOIN grade ON tally_grading_detail.id_grade = grade.id
  LEFT JOIN code_size ON code_size.id = tally_grading_detail.id_code_size
  LEFT JOIN sizes ON code_size.id_size = sizes.id
  LEFT JOIN freezing_type ON code_size.id_freezing_type = freezing_type.id
  LEFT JOIN qty_type ON tally_grading_detail.id_qty_type = qty_type.id
  INNER JOIN temp_tally ON tally_grading_detail.temp_tally_id = temp_tally.id';

  $grading_history = DB::table(DB::raw("($grading_history_sql) as rs_grading_history_sql"))
  ->where('temp_tally_id', $id)->orderBy('tally_grading_detail_id', 'desc')->get();

        //dd($grading_history);

  return view ('home.tally_report.historygrading', compact('temp_tally', 'grades', 'qty_types', 'code_sizes', 'grading_view', 'grading_history'));
}
 

public function closepacking($id)
{   
  $purchase = TempTally::find($id);  
  $purchase->form_status = 'packing'; 
  $purchase->save();

  return redirect()->action('Home\TallyFormController@index');
}


public function viewpacking($id)
{ 

 $temp_tally = DB::table('temp_tally') 
 ->where('id', $id) 
 ->first();

        //dd($purchase);
 
 $sql_detail = 'SELECT
 tally_packing_detail.id,
 temp_tally.trans_code AS purchase_code,
 grade.`name` AS grade,
 sizes.note AS size,
 freezing_type.`name` AS freezing,
 code_size.`name` AS code_size,
 tally_packing_detail.block,
 tally_packing_detail.total_block,
 tally_packing_detail.mix,
 tally_packing_detail.icr,
 tally_packing_detail.rest,
 code_size.block_weight,
 qty_type.`name` AS qty_type,
 view_tally_packing_master.total,
 tally_packing_detail.ic AS ic,
 tally_packing_detail.mc
 FROM
 tally_packing_detail
 LEFT JOIN grade ON tally_packing_detail.id_grade = grade.id
 LEFT JOIN code_size ON code_size.id = tally_packing_detail.id_code_size
 LEFT JOIN sizes ON code_size.id_size = sizes.id
 LEFT JOIN freezing_type ON code_size.id_freezing_type = freezing_type.id
 LEFT JOIN qty_type ON tally_packing_detail.id_qty_type = qty_type.id
 INNER JOIN temp_tally ON tally_packing_detail.temp_tally_id = temp_tally.id
 LEFT JOIN view_tally_packing_master ON tally_packing_detail.id = view_tally_packing_master.id
 WHERE tally_packing_detail.temp_tally_id = '.$id.'
 ORDER BY
 grade.`name` ASC';
 $tally_detail = DB::table(DB::raw("($sql_detail) as rs_sql_detail"))->get();

 return view ('home.tally_report.viewpacking', compact('tally_detail', 'temp_tally'));
}

public function finalreport($id)
{ 

 $temp_tally = DB::table('temp_tally') 
 ->where('id', $id) 
 ->first();

        //dd($purchase);
 
 $sql_ft = 'SELECT
  treatment.temp_tally_id,
  SUM(
    tally_receiving_treatment.salinity
  ) AS salinity,
  SUM(
    tally_receiving_treatment.duration
  ) AS duration,
  SUM(
    tally_receiving_treatment.salinity_rm
  ) AS salinity_rm,
  receiving.weight AS tot_rm,
  treatment.weight AS result
FROM
  tally_receiving_treatment
RIGHT JOIN (
  SELECT
    SUM(tally_detail.weight) AS weight,
    tally_detail.temp_tally_id
  FROM
    tally_detail
  WHERE
    tally_type = "RECEIVING"
    AND tally_detail.temp_tally_id = '.$id.'
) AS receiving ON tally_receiving_treatment.temp_tally_id = receiving.temp_tally_id
RIGHT JOIN (
  SELECT
    SUM(tally_detail.weight) AS weight,
    tally_detail.temp_tally_id
  FROM
    tally_detail
  WHERE
    tally_type = "TREATMENT"
    AND tally_detail.temp_tally_id = '.$id.'
  GROUP BY
    tally_detail.temp_tally_id
) AS treatment ON receiving.temp_tally_id = treatment.temp_tally_id
WHERE tally_receiving_treatment.temp_tally_id = '.$id.'
GROUP BY
  treatment.temp_tally_id,
  receiving.weight,
  treatment.weight';
 $process_report_ft = DB::table(DB::raw("($sql_ft) as sql_ft"))->first();


 $sql_ft_tube = 'SELECT
  treatment.temp_tally_id,
  SUM(
    tally_peeling_treatment.salinity
  ) AS salinity,
  SUM(
    tally_peeling_treatment.duration
  ) AS duration,
  SUM(
    tally_peeling_treatment.salinity_rm
  ) AS salinity_rm,
  receiving.weight AS tot_rm,
  treatment.weight AS result
FROM
  tally_peeling_treatment
RIGHT JOIN (
  SELECT
    Sum(
      tally_detail_peeling.weight
    ) AS weight,
    tally_detail_peeling.temp_tally_id
  FROM
    tally_detail_peeling
  INNER JOIN tally_peeling_master ON tally_detail_peeling.tally_peeling_master_id = tally_peeling_master.id
  WHERE
    tally_peeling_master.`name` = "RECEIVING" AND tally_detail_peeling.temp_tally_id = '.$id.'
  GROUP BY
    tally_detail_peeling.temp_tally_id
) AS receiving ON tally_peeling_treatment.temp_tally_id = receiving.temp_tally_id
RIGHT JOIN (
  SELECT
    Sum(
      tally_detail_peeling.weight
    ) AS weight,
    tally_detail_peeling.temp_tally_id
  FROM
    tally_detail_peeling
  INNER JOIN tally_peeling_master ON tally_detail_peeling.tally_peeling_master_id = tally_peeling_master.id
  WHERE
    tally_peeling_master.`name` = "TRIMMING" AND tally_detail_peeling.temp_tally_id = '.$id.'
  GROUP BY
    tally_detail_peeling.temp_tally_id
) AS treatment ON receiving.temp_tally_id = treatment.temp_tally_id
 AND tally_peeling_treatment.temp_tally_id = '.$id.'
GROUP BY
  treatment.temp_tally_id,
  receiving.weight,
  treatment.weight';
 $process_report_ft_tube = DB::table(DB::raw("($sql_ft_tube) as sql_ft"))->first();


 $sql_ft_tentacle = 'SELECT
  treatment.temp_tally_id,
  SUM(
    tally_peeling_treatment.salinity
  ) AS salinity,
  SUM(
    tally_peeling_treatment.duration
  ) AS duration,
  SUM(
    tally_peeling_treatment.salinity_rm
  ) AS salinity_rm,
  receiving.weight AS tot_rm,
  treatment.weight AS result
FROM
  tally_peeling_treatment
RIGHT JOIN (
  SELECT
  SUM(
    tally_detail_deheading.weight
  ) AS weight,
  tally_detail_deheading.temp_tally_id
FROM
  tally_detail_deheading

  WHERE tally_detail_deheading.temp_tally_id = '.$id.'
GROUP BY
  tally_detail_deheading.temp_tally_id 
) AS receiving ON tally_peeling_treatment.temp_tally_id = receiving.temp_tally_id
RIGHT JOIN (
  SELECT
    Sum(
      tally_detail_peeling.weight
    ) AS weight,
    tally_detail_peeling.temp_tally_id
  FROM
    tally_detail_peeling
  INNER JOIN tally_peeling_master ON tally_detail_peeling.tally_peeling_master_id = tally_peeling_master.id
  WHERE
    tally_peeling_master.`name` = "TREATMENT" AND tally_detail_peeling.temp_tally_id = '.$id.'
  GROUP BY
    tally_detail_peeling.temp_tally_id
) AS treatment ON receiving.temp_tally_id = treatment.temp_tally_id
 AND tally_peeling_treatment.temp_tally_id = '.$id.'
GROUP BY
  treatment.temp_tally_id,
  receiving.weight,
  treatment.weight';
 $process_report_ft_tentacle = DB::table(DB::raw("($sql_ft_tentacle) as sql_ft"))->first();


 $sql_wr = 'SELECT
  tube.total_block AS tube,
  tentacle.total_block AS tentacle
FROM
  (
    SELECT
      tally_grading_detail.temp_tally_id,
      SUM(
        tally_grading_detail.total_block
      ) AS total_block
    FROM
      tally_grading_detail
    INNER JOIN qty_type ON tally_grading_detail.id_qty_type = qty_type.id
    WHERE
      qty_type.`name` = "TUBE" AND tally_grading_detail.temp_tally_id = '.$id.'
    GROUP BY
      tally_grading_detail.temp_tally_id
  ) AS tube,
  (
    SELECT
      tally_grading_detail.temp_tally_id,
      SUM(
        tally_grading_detail.total_block
      ) AS total_block
    FROM
      tally_grading_detail
    INNER JOIN qty_type ON tally_grading_detail.id_qty_type = qty_type.id
    WHERE
      qty_type.`name` = "TENTACLE" AND tally_grading_detail.temp_tally_id = '.$id.'
    GROUP BY
      tally_grading_detail.temp_tally_id
  ) AS tentacle';
 $process_wr = DB::table(DB::raw("($sql_wr) as sql_ft"))->first();


 $sql_rec_add = 'SELECT
  tally_additional_master.`name` AS name,
  SUM(tally_detail_additional.weight) AS weight
FROM
  tally_detail_additional
INNER JOIN tally_additional_master ON tally_detail_additional.tally_additional_master_id = tally_additional_master.id
WHERE tally_detail_additional.temp_tally_id = '.$id.'
GROUP BY tally_additional_master.`name`';
 $process_rec_add = DB::table(DB::raw("($sql_rec_add) as sql_ft"))->get();


 $sql_rec_dhd = 'SELECT
  tally_deheading_master.`name`,
  SUM(
    tally_detail_deheading.weight
  ) AS weight,
  SUM(
    tally_detail_deheading.waste
  ) AS waste
FROM
  tally_detail_deheading
INNER JOIN tally_deheading_master ON tally_detail_deheading.tally_dehading_master_id = tally_deheading_master.id
WHERE tally_detail_deheading.temp_tally_id = '.$id.'
GROUP BY
  tally_deheading_master.`name`';
 $process_rec_dhd = DB::table(DB::raw("($sql_rec_dhd) as sql_ft"))->get();

 return view ('home.tally_report.finalreport', compact('process_report_ft','process_report_ft_tube', 'process_report_ft_tentacle', 'temp_tally','process_report_ft_tentacle', 'process_wr','process_rec_add', 'process_rec_dhd'));
}
}
