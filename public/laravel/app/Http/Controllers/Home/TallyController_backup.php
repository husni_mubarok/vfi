<?php

namespace App\Http\Controllers\Home;

use DB;
use Illuminate\Http\Request; 
use App\Http\Requests\TallyRequest;
use App\Http\Controllers\Controller;
use App\Repository\PurchaseRepository;
use App\Repository\ProductRepository;
use App\Repository\SupplierRepository;
use App\Repository\PriceRepository;
use App\Repository\PurchaseDetailRepository;
use App\Model\Purchase;
use App\Model\Size;

//Receiving
use App\Model\Tally;
use App\Model\TallyDetail;
use App\Model\TallyAdditionalMaster; 
use App\Model\TallyDetailAdditional;

//Heading
use App\Model\TallyDeheading;
use App\Model\TallyDetailDeheading;
use App\Model\TallyDeheadingMaster;

//Peeling
use App\Model\TallyPeeling;
use App\Model\TallyDetailPeeling;
use App\Model\TallyPeelingMaster;

//Grading
use App\Model\TallyGradingMaster;
use App\Model\TallyDetailGrading; 
use App\Model\TallyGrading;
use App\Model\CodeSize;
use App\Model\FreezingType;
use App\Model\Grade;
use App\Model\QtyType;

class TallyController extends Controller
{
    protected $PurchaseRepository;
    protected $ProductRepository;
    protected $SupplierRepository;
    protected $PriceRepository;
    protected $PurchaseDetailRepository;

    public function __construct(PurchaseRepository $purchase_repository, ProductRepository $product_repository, SupplierRepository $supplier_repository, PriceRepository $price_repository, PurchaseDetailRepository $purchase_detail_repository)
    {
        $this->PurchaseRepository = $purchase_repository;
        $this->ProductRepository = $product_repository;
        $this->SupplierRepository = $supplier_repository;
        $this->PriceRepository = $price_repository;
        $this->PurchaseDetailRepository = $purchase_detail_repository;
    }

    public function index()
    {
        $project_list = Purchase::groupBy('project')->pluck('project')->toArray();
        array_unshift($project_list, 'Actual Delivery');

        $pending_approval = Purchase::where('status', 'Waiting COO')->latest()->get(); 

        return view ('home.tally.index', compact('pending_approval', 'project_list'));
    }

    public function bank()
    {
        $project_list = Purchase::groupBy('project')->pluck('project')->toArray();
        array_unshift($project_list, 'Actual Delivery');

        $pending_approval = Purchase::where('form_status', 5)->latest()->get(); 

        return view ('home.tally.bank', compact('pending_approval', 'project_list'));
    }

    //RECEIVING COLLECT
    public function create($id)
    { 
       
         $purchase = DB::table('products')
        ->join('purchases_products', 'purchases_products.product_id', '=', 'products.id')
        ->join('purchases', 'purchases_products.purchase_id', '=', 'purchases.id')  
        ->select('purchases.id', 'purchases.doc_code', 'purchases.trans_code', 'products.name', 'products.id AS product_id', 'purchases.project')
        ->where('purchases.id', $id) 
        ->first();  

        $tally = Tally::where('purchase_id', $id)->first(); 

        $tally_detail = DB::table('product_types')
        ->join('product_types_sizes', 'product_types_sizes.product_type_id', '=', 'product_types.id')
        ->join('sizes', 'product_types_sizes.size_id', '=', 'sizes.id')
        ->join('products', 'products.product_type_id', '=', 'product_types.id')
        ->leftjoin('purchases_products', 'purchases_products.product_id', '=', 'products.id')
        ->leftjoin('tally', 'purchases_products.product_id', '=', 'tally.purchase_id')
        ->leftjoin('tally_detail', 'tally.id', '=', 'tally_detail.tally_id')
        ->select('product_types.name', 'sizes.note', 'sizes.id', DB::raw('0 as weight'), 'tally.tally_type')
        ->where('purchases_products.purchase_id', $id)
        ->groupBy('product_types.name', 'sizes.note', 'sizes.id', 'tally_detail.weight', 'tally.tally_type')
        ->get();  

        $tally_additional = TallyAdditionalMaster::all();  

        return view ('home.tally.form', compact('tally_detail', 'purchase', 'tally', 'tally_additional'));
    }

     public function store(TallyRequest $request, $id)
    {
          
        $tally = new Tally;  
        $tally->trans_code = $request->input('trans_code');
        $tally->tally_type = $request->input('tally_type');
        $tally->product_id = $request->input('product_id');
        $tally->purchase_id = $id;
        $tally->save();

        $purchase = Purchase::find($id);  
        $purchase->action_form_status = 'Edit';
        $purchase->form_status = 0;
        $purchase->save();


       foreach($request->input('tally') as $detail_key => $detail_value)
        {
            $tally_detail = new TallyDetail;
            $tally_detail->tally_id = $tally->id;
            $tally_detail->size_id = $detail_key;
            $tally_detail->weight = $detail_value['weight']; 
            $tally_detail->tally_type = $request->input('tally_type');
            $tally_detail->created_by = \Auth::id();
            $tally_detail->save();
        }

        foreach($request->input('tally_additional') as $detail_key => $detail_value)
        {
            $tally_detail = new TallyDetailAdditional;
            $tally_detail->tally_id = $tally->id;
            $tally_detail->tally_additional_master_id = $detail_key;
            $tally_detail->weight = $detail_value['weight_add']; 
            $tally_detail->pan = $detail_value['pan']; 
            $tally_detail->rest = $detail_value['rest']; 
            $tally_detail->tally_type = $request->input('tally_type');
            $tally_detail->created_by = \Auth::id();
            $tally_detail->save();
        }

       
        return redirect()->action('Home\TallyController@detail', $id);
    }

     public function detail($id)
    { 

         $purchase = DB::table('tally')
        ->join('products', 'tally.product_id', '=', 'products.id') 
         ->join('purchases', 'tally.purchase_id', '=', 'purchases.id')  
        ->select('tally.purchase_id AS id', 'tally.trans_code', 'products.name', 'products.id AS product_id', 'tally.tally_type', 'purchases.project')
        ->where('tally.purchase_id', $id) 
        ->first(); 
 
 
        $tally_detail = DB::table('tally_detail')
        ->join('sizes', 'tally_detail.size_id', '=', 'sizes.id') 
        ->join('tally', 'tally_detail.tally_id', '=', 'tally.id')
        ->select('tally_detail.size_id AS id', 'sizes.note', DB::raw('0 as weight'))
        ->where('tally.purchase_id', $id) 
        ->groupBy('tally_detail.size_id', 'sizes.note')
        ->get();

        $tally_additional = DB::table('tally_detail_additional')
        ->join('tally_additional_master', 'tally_detail_additional.tally_additional_master_id', '=', 'tally_additional_master.id') 
        ->join('tally', 'tally_detail_additional.tally_id', '=', 'tally.id')
        ->select('tally_detail_additional.tally_additional_master_id AS id', 'tally_additional_master.name')
        ->where('tally.purchase_id', $id) 
        ->groupBy('tally_detail_additional.tally_additional_master_id', 'tally_additional_master.name')
        ->get();

        $tally_detail_view_treatment = DB::table('tally_detail')
        ->join('sizes', 'tally_detail.size_id', '=', 'sizes.id') 
        ->join('tally', 'tally_detail.tally_id', '=', 'tally.id')
        ->select('tally_detail.size_id AS id', 'sizes.note', DB::raw('SUM(tally_detail.weight) as weight'))
        ->where('tally.purchase_id', $id) 
        ->where('tally_detail.tally_type', 'TREATMENT') 
        ->groupBy('tally_detail.size_id', 'sizes.note')
        ->get(); 

        foreach ($tally_detail_view_treatment as $tdvt) {
             $tally_detail_view_treatment_detail = DB::table('tally_detail')
                ->join('sizes', 'tally_detail.size_id', '=', 'sizes.id') 
                ->join('tally', 'tally_detail.tally_id', '=', 'tally.id')
                ->select('tally_detail.size_id AS id', 'sizes.note', 'tally_detail.weight')
                ->where('tally.purchase_id', $id) 
                ->where('tally_detail.size_id', '=', $tdvt->id)
                ->where('tally_detail.tally_type', 'TREATMENT')  
                ->get(); 
            $array_val["note"] = $tdvt->note;
            $tdvtd_arrays = null;
            foreach ($tally_detail_view_treatment_detail as $tdvtd) {
                $tdvtd_array = null;
                $tdvtd_array["weight"] = $tdvtd->weight;
                $tdvtd_arrays[] = $tdvtd_array;
            }

            $array_val["detail"] = $tdvtd_arrays;
            $array_all_treatment[] = $array_val;
        } 

        $tally_detail_view_receiving = DB::table('tally_detail')
        ->join('sizes', 'tally_detail.size_id', '=', 'sizes.id') 
        ->join('tally', 'tally_detail.tally_id', '=', 'tally.id')
        ->select('tally_detail.size_id AS id', 'sizes.note', DB::raw('SUM(tally_detail.weight) as weight'))
        ->where('tally.purchase_id', $id) 
        ->where('tally_detail.tally_type', 'RECEIVING') 
        ->groupBy('tally_detail.size_id', 'sizes.note')
        ->get(); 

        foreach ($tally_detail_view_receiving as $tdvr) {
            $tally_detail_view_receiving_detail = DB::table('tally_detail')
                ->join('sizes', 'tally_detail.size_id', '=', 'sizes.id') 
                ->join('tally', 'tally_detail.tally_id', '=', 'tally.id')
                ->select('tally_detail.size_id AS id', 'sizes.note', 'tally_detail.weight')
                ->where('tally.purchase_id', $id) 
                ->where('tally_detail.size_id', '=', $tdvr->id)
                ->where('tally_detail.tally_type', 'RECEIVING')  
                ->get(); 
            $array_val_rec["note"] = $tdvr->note;
            $tdvrd_arrays = null;
            foreach ($tally_detail_view_receiving_detail as $tdvrd) {
                $tdvrd_array = null;
                $tdvrd_array["weight"] = $tdvrd->weight;
                $tdvrd_arrays[] = $tdvrd_array;
            }

            $array_val_rec["detail"] = $tdvrd_arrays;
            $array_all_receiving[] = $array_val_rec;
        }

        //dd($array_all_receiving);

        $tally_additional_view = DB::table('tally_detail_additional')
        ->join('tally_additional_master', 'tally_detail_additional.tally_additional_master_id', '=', 'tally_additional_master.id') 
        ->join('tally', 'tally_detail_additional.tally_id', '=', 'tally.id')
        ->select('tally_additional_master.id','tally_additional_master.name', DB::raw('SUM(tally_detail_additional.weight) as weight'), DB::raw('SUM(tally_detail_additional.pan) as pan'), DB::raw('SUM(tally_detail_additional.rest) as rest'))
        ->where('tally.purchase_id', $id) 
        ->groupBy('tally_detail_additional.tally_additional_master_id', 'tally_additional_master.name')
        ->get();


        $tally_additional_view_c = DB::table('tally_detail_additional')
        ->join('tally_additional_master', 'tally_detail_additional.tally_additional_master_id', '=', 'tally_additional_master.id') 
        ->join('tally', 'tally_detail_additional.tally_id', '=', 'tally.id')
        ->select('tally_additional_master.id','tally_additional_master.name', DB::raw('COUNT(tally_detail_additional.rest) as rest_count'))
        ->where('tally.purchase_id', $id) 
        ->groupBy('tally_detail_additional.tally_additional_master_id', 'tally_additional_master.name')
        ->first();

        $count_add = $tally_additional_view_c->rest_count;

        foreach ($tally_additional_view as $tav) {
           $tally_additional_view_detail = DB::table('tally_detail_additional')
                ->join('tally_additional_master', 'tally_detail_additional.tally_additional_master_id', '=', 'tally_additional_master.id') 
                ->join('tally', 'tally_detail_additional.tally_id', '=', 'tally.id')
                ->select('tally_detail_additional.id','tally_additional_master.name', 'tally_detail_additional.weight', 'tally_detail_additional.pan', 'tally_detail_additional.rest')
                ->where('tally.purchase_id', $id)  
                ->where('tally_additional_master.id', '=', $tav->id)  
                ->get();


            $array_val_add["id"] = $tav->id;
            $array_val_add["name"] = $tav->name;
            $tavd_arrays = null;
            foreach ($tally_additional_view_detail as $tavd) {
                $tavd_array = null;
                $tavd_array["id"] = $tavd->id;
                $tavd_array["weight"] = $tavd->weight;
                $tavd_array["pan"] = $tavd->pan;
                $tavd_array["rest"] = $tavd->rest;
                $tavd_arrays[] = $tavd_array;
            }

            $array_val_add["detail"] = $tavd_arrays;
            $array_all_add[] = $array_val_add;
        }

        //dd($tally_additional_view);

        return view ('home.tally.detail', compact('tally_detail', 'tally_detail_view_treatment', 'tally_detail_view_receiving', 'tally_additional_view', 'purchase', 'tally_additional', 'array_all_receiving', 'array_all_treatment', 'tally_additional_view', 'count_add', 'array_all_add'));
    }

    public function storedetail(TallyRequest $request, $id)
    {
          
        $tally = Tally::where('purchase_id', $id)->first(); 
        $tally->trans_code = $request->input('trans_code');
        $tally->tally_type = $request->input('tally_type');
        $tally->product_id = $request->input('product_id');  
        $tally->save();  
 
        foreach($request->input('tally') as $detail_key => $detail_value)
        {
            $tally_detail = new TallyDetail;
            $tally_detail->tally_id = $tally->id;
            $tally_detail->size_id = $detail_key;
            $tally_detail->weight = $detail_value['weight']; 
             $tally_detail->tally_type = $request->input('tally_type');
            $tally_detail->created_by = \Auth::id();
            $tally_detail->save();
        }

        foreach($request->input('tally_additional') as $detail_key => $detail_value)
        {
            $tally_detail = new TallyDetailAdditional;
            $tally_detail->tally_id = $tally->id;
            $tally_detail->tally_additional_master_id = $detail_key;
            $tally_detail->weight = $detail_value['weight_add']; 
            $tally_detail->pan = $detail_value['pan']; 
            $tally_detail->rest = $detail_value['rest'];
            $tally_detail->tally_type = $request->input('tally_type'); 
            $tally_detail->created_by = \Auth::id();
            $tally_detail->save();
        }

        return redirect()->action('Home\TallyController@detail', $id);

    }

    public function close($id)
    {
          
        $purchase = Purchase::find($id);  
        $purchase->form_status = 1;
        $purchase->action_form_status = 'Done';
        $purchase->save();
 
        return redirect()->action('Home\TallyController@index');

    }

    public function view($id)
    { 

         $purchase = DB::table('tally')
        ->join('products', 'tally.product_id', '=', 'products.id') 
        ->select('tally.purchase_id AS id', 'tally.trans_code', 'products.name', 'products.id AS product_id', 'tally.tally_type')
        ->where('tally.purchase_id', $id) 
        ->first();  

       $tally_detail_view_treatment = DB::table('tally_detail')
        ->join('sizes', 'tally_detail.size_id', '=', 'sizes.id') 
        ->join('tally', 'tally_detail.tally_id', '=', 'tally.id')
        ->select('tally_detail.size_id AS id', 'sizes.note', DB::raw('SUM(tally_detail.weight) as weight'))
        ->where('tally.purchase_id', $id) 
        ->where('tally_detail.tally_type', 'TREATMENT') 
        ->groupBy('tally_detail.size_id', 'sizes.note')
        ->get();  

        $tally_detail_view_receiving = DB::table('tally_detail')
        ->join('sizes', 'tally_detail.size_id', '=', 'sizes.id') 
        ->join('tally', 'tally_detail.tally_id', '=', 'tally.id')
        ->select('tally_detail.size_id AS id', 'sizes.note', DB::raw('SUM(tally_detail.weight) as weight'))
        ->where('tally.purchase_id', $id) 
        ->where('tally_detail.tally_type', 'RECEIVING') 
        ->groupBy('tally_detail.size_id', 'sizes.note')
        ->get(); 

        $tally_additional_view = DB::table('tally_detail_additional')
        ->join('tally_additional_master', 'tally_detail_additional.tally_additional_master_id', '=', 'tally_additional_master.id') 
        ->join('tally', 'tally_detail_additional.tally_id', '=', 'tally.id')
        ->select('tally_additional_master.name', DB::raw('SUM(tally_detail_additional.weight) as weight'), DB::raw('SUM(tally_detail_additional.pan) as pan'), DB::raw('SUM(tally_detail_additional.rest) as rest'))
        ->where('tally.purchase_id', $id) 
        ->groupBy('tally_detail_additional.tally_additional_master_id', 'tally_additional_master.name')
        ->get();

        return view ('home.tally.view', compact('tally_detail_view_treatment','tally_detail_view_receiving','tally_additional_view', 'purchase'));
    }

    public function history($id)
    { 
 
        $purchase = DB::table('tally')
        ->join('products', 'tally.product_id', '=', 'products.id') 
        ->select('tally.purchase_id AS id', 'tally.trans_code', 'products.name', 'products.id AS product_id', 'tally.tally_type')
        ->where('tally.purchase_id', $id) 
        ->first(); 

       $tally_detail_view_treatment = DB::table('tally_detail')
        ->join('sizes', 'tally_detail.size_id', '=', 'sizes.id') 
        ->join('tally', 'tally_detail.tally_id', '=', 'tally.id')
        ->select('tally_detail.size_id AS id', 'sizes.note', DB::raw('SUM(tally_detail.weight) as weight'))
        ->where('tally.purchase_id', $id) 
        ->where('tally_detail.tally_type', 'TREATMENT')   
        ->groupBy('tally_detail.size_id', 'sizes.note')
        ->get(); 

        foreach ($tally_detail_view_treatment as $tdvt) {
             $tally_detail_view_treatment_detail = DB::table('tally_detail')
                ->join('sizes', 'tally_detail.size_id', '=', 'sizes.id') 
                ->join('tally', 'tally_detail.tally_id', '=', 'tally.id')
                ->select('tally_detail.size_id AS id', 'sizes.note', 'tally_detail.weight')
                ->where('tally.purchase_id', $id) 
                ->where('tally_detail.size_id', '=', $tdvt->id)
                ->where('tally_detail.tally_type', 'TREATMENT')  
                ->get(); 
            $array_val["note"] = $tdvt->note;
            $tdvtd_arrays = null;
            foreach ($tally_detail_view_treatment_detail as $tdvtd) {
                $tdvtd_array = null;
                $tdvtd_array["weight"] = $tdvtd->weight;
                $tdvtd_arrays[] = $tdvtd_array;
            }

            $array_val["detail"] = $tdvtd_arrays;
            $array_all_treatment[] = $array_val;
        }
        //dd($array_all_treatment);

        $tally_detail_view_receiving = DB::table('tally_detail')
        ->join('sizes', 'tally_detail.size_id', '=', 'sizes.id') 
        ->join('tally', 'tally_detail.tally_id', '=', 'tally.id')
        ->select('tally_detail.size_id AS id', 'sizes.note', DB::raw('SUM(tally_detail.weight) as weight'))
        ->where('tally.purchase_id', $id) 
        ->where('tally_detail.tally_type', 'RECEIVING') 
        ->groupBy('tally_detail.size_id', 'sizes.note')
        ->get(); 

        foreach ($tally_detail_view_receiving as $tdvr) {
            $tally_detail_view_receiving_detail = DB::table('tally_detail')
                ->join('sizes', 'tally_detail.size_id', '=', 'sizes.id') 
                ->join('tally', 'tally_detail.tally_id', '=', 'tally.id')
                ->select('tally_detail.size_id AS id', 'sizes.note', 'tally_detail.weight')
                ->where('tally.purchase_id', $id) 
                ->where('tally_detail.size_id', '=', $tdvr->id)
                ->where('tally_detail.tally_type', 'RECEIVING')  
                ->get(); 
            $array_val_rec["note"] = $tdvr->note;
            $tdvrd_arrays = null;
            foreach ($tally_detail_view_receiving_detail as $tdvrd) {
                $tdvrd_array = null;
                $tdvrd_array["weight"] = $tdvrd->weight;
                $tdvrd_arrays[] = $tdvrd_array;
            }

            $array_val_rec["detail"] = $tdvrd_arrays;
            $array_all_receiving[] = $array_val_rec;
        }

        //dd($array_all_receiving);

        $tally_additional_view = DB::table('tally_detail_additional')
        ->join('tally_additional_master', 'tally_detail_additional.tally_additional_master_id', '=', 'tally_additional_master.id') 
        ->join('tally', 'tally_detail_additional.tally_id', '=', 'tally.id')
        ->select('tally_additional_master.id','tally_additional_master.name', DB::raw('SUM(tally_detail_additional.weight) as weight'), DB::raw('SUM(tally_detail_additional.pan) as pan'), DB::raw('SUM(tally_detail_additional.rest) as rest'))
        ->where('tally.purchase_id', $id) 
        ->groupBy('tally_detail_additional.tally_additional_master_id', 'tally_additional_master.name')
        ->get();


        $tally_additional_view_c = DB::table('tally_detail_additional')
        ->join('tally_additional_master', 'tally_detail_additional.tally_additional_master_id', '=', 'tally_additional_master.id') 
        ->join('tally', 'tally_detail_additional.tally_id', '=', 'tally.id')
        ->select('tally_additional_master.id','tally_additional_master.name', DB::raw('COUNT(tally_detail_additional.rest) as rest_count'))
        ->where('tally.purchase_id', $id) 
        ->groupBy('tally_detail_additional.tally_additional_master_id', 'tally_additional_master.name')
        ->first();

        $count_add = $tally_additional_view_c->rest_count;

       // dd($count_add);

        foreach ($tally_additional_view as $tav) {
           $tally_additional_view_detail = DB::table('tally_detail_additional')
                ->join('tally_additional_master', 'tally_detail_additional.tally_additional_master_id', '=', 'tally_additional_master.id') 
                ->join('tally', 'tally_detail_additional.tally_id', '=', 'tally.id')
                ->select('tally_detail_additional.id','tally_additional_master.name', 'tally_detail_additional.weight', 'tally_detail_additional.pan', 'tally_detail_additional.rest')
                ->where('tally.purchase_id', $id)  
                ->where('tally_additional_master.id', '=', $tav->id)  
                ->get();


            $array_val_add["id"] = $tav->id;
            $array_val_add["name"] = $tav->name;
            $tavd_arrays = null;
            foreach ($tally_additional_view_detail as $tavd) {
                $tavd_array = null;
                $tavd_array["id"] = $tavd->id;
                $tavd_array["weight"] = $tavd->weight;
                $tavd_array["pan"] = $tavd->pan;
                $tavd_array["rest"] = $tavd->rest;
                $tavd_arrays[] = $tavd_array;
            }

            $array_val_add["detail"] = $tavd_arrays;
            $array_all_add[] = $array_val_add;
        }

        //dd($array_all_add);

        return view ('home.tally.history', compact('tally_detail_view_treatment','tally_detail_view_receiving','tally_additional_view', 'purchase','array_all_treatment','array_all_receiving','array_all_add','count_add'));
    }
 

    //DEHEADING 
    public function createdeheading($id)
    {  
         $purchase = DB::table('products')
        ->join('purchases_products', 'purchases_products.product_id', '=', 'products.id')
        ->join('purchases', 'purchases_products.purchase_id', '=', 'purchases.id')  
        ->select('purchases.id', 'purchases.doc_code', 'purchases.trans_code', 'products.name', 'products.id AS product_id', 'purchases.project')
        ->where('purchases.id', $id) 
        ->first();   

        $tally_detail = TallyDeheadingMaster::all();  

        return view ('home.tally.formdeheading', compact('tally_detail', 'purchase', 'tally'));
    }

     public function storedeheading(Request $request, $id)
    {
         
        $tally = new TallyDeheading; 
        $tally->trans_code = $request->input('trans_code'); 
        $tally->product_id = $request->input('product_id'); 
        $tally->purchase_id = $id;
        $tally->save();


        $purchase = Purchase::find($id);  
        $purchase->action_form_status = 'Edit';
        $purchase->save();

        foreach($request->input('tally') as $detail_key => $detail_value)
        {
            $tally_detail = new TallyDetailDeheading;
            $tally_detail->tally_id = $tally->id;
            $tally_detail->tally_dehading_master_id = $detail_key;
            $tally_detail->weight = $detail_value['weight'];
            $tally_detail->waste = $detail_value['waste'];
            $tally_detail->created_by = \Auth::id();
            $tally_detail->save();
        }
        return redirect()->action('Home\TallyController@detaildeheading', $id);
    }

     public function detaildeheading($id)
    { 

         $purchase = DB::table('tally')
        ->join('products', 'tally.product_id', '=', 'products.id') 
         ->join('purchases', 'tally.purchase_id', '=', 'purchases.id')  
        ->select('tally.purchase_id AS id', 'tally.trans_code', 'products.name', 'products.id AS product_id', 'tally.tally_type', 'purchases.project')
        ->where('tally.purchase_id', $id) 
        ->first(); 
 
 
        $tally_detail = TallyDeheadingMaster::all(); 

        $tally_detail_view = DB::table('tally_detail_deheading')
        ->join('tally_deheading_master', 'tally_detail_deheading.tally_dehading_master_id', '=', 'tally_deheading_master.id') 
        ->join('tally_deheading', 'tally_detail_deheading.tally_id', '=', 'tally_deheading.id')
        ->select('tally_deheading_master.id', 'tally_deheading_master.name', DB::raw('SUM(tally_detail_deheading.weight) as weight'), DB::raw('SUM(tally_detail_deheading.waste) as waste'))
        ->where('tally_deheading.purchase_id', $id) 
        ->groupBy('tally_deheading_master.id', 'tally_deheading_master.name')
        ->get();  


        $tally_detail_view_count = DB::table('tally_detail_deheading')
        ->join('tally_deheading_master', 'tally_detail_deheading.tally_dehading_master_id', '=', 'tally_deheading_master.id') 
        ->join('tally_deheading', 'tally_detail_deheading.tally_id', '=', 'tally_deheading.id')
        ->select('tally_deheading_master.id', 'tally_deheading_master.name', DB::raw('COUNT(tally_detail_deheading.weight) as count_weight'))
        ->where('tally_deheading.purchase_id', $id) 
        ->groupBy('tally_deheading_master.id', 'tally_deheading_master.name')
        ->first();

        //dd($tally_detail_view);

        foreach ($tally_detail_view as $t_d_v) {
             $tally_detail_view_deheading = DB::table('tally_detail_deheading')
                        ->join('tally_deheading_master', 'tally_detail_deheading.tally_dehading_master_id', '=', 'tally_deheading_master.id') 
                        ->join('tally_deheading', 'tally_detail_deheading.tally_id', '=', 'tally_deheading.id')
                        ->select('tally_deheading_master.id', 'tally_deheading_master.name', 'tally_detail_deheading.weight', 'tally_detail_deheading.waste')
                        ->where('tally_deheading.purchase_id', $id)  
                        ->where('tally_deheading_master.id', '=', $t_d_v->id)
                        ->get(); 
            $array_val["name"] = $t_d_v->name;
            $tdvtd_arrays = null;
            foreach ($tally_detail_view_deheading as $tdvtd) {
                $tdvtd_array = null;
                $tdvtd_array["weight"] = $tdvtd->weight;
                $tdvtd_array["waste"] = $tdvtd->waste;
                $tdvtd_arrays[] = $tdvtd_array;
            }

            $array_val["detail"] = $tdvtd_arrays;
            $array_all[] = $array_val;
        }

        //dd($tally_detail_view);

        return view ('home.tally.detaildeheading', compact('tally_detail', 'tally_detail_view', 'purchase', 'tally_detail_view_count','array_all'));
    }

    public function storedetaildeheading(Request $request, $id)
    {
          
        $tally = TallyDeheading::where('purchase_id', $id)->first(); 
        $tally->trans_code = $request->input('trans_code'); 
        $tally->product_id = $request->input('product_id');  
        $tally->save(); 

        foreach($request->input('tally') as $detail_key => $detail_value)
        {
            $tally_detail = new TallyDetailDeheading;
            $tally_detail->tally_id = $tally->id;
            $tally_detail->tally_dehading_master_id = $detail_key;
            $tally_detail->weight = $detail_value['weight'];
            $tally_detail->waste = $detail_value['waste'];
            $tally_detail->created_by = \Auth::id();
            $tally_detail->save();
        }

        return redirect()->action('Home\TallyController@detaildeheading', $id);

    }

    public function closedeheading($id)
    {
          
        $purchase = Purchase::find($id);  
        $purchase->form_status = 2;
        $purchase->action_form_status = 'Done';
        $purchase->save();
        return redirect()->action('Home\TallyController@index');

    }

    public function viewdeheading($id)
    { 

        $purchase = DB::table('tally_deheading')
        ->join('products', 'tally_deheading.product_id', '=', 'products.id') 
        ->select('tally_deheading.purchase_id AS id', 'tally_deheading.trans_code', 'products.name', 'products.id AS product_id', 'tally_deheading.tally_type')
        ->where('tally_deheading.purchase_id', $id) 
        ->first(); 
 
        $tally_detail = TallyDeheadingMaster::all(); 

        $tally_detail_view = DB::table('tally_detail_deheading')
        ->join('tally_deheading_master', 'tally_detail_deheading.tally_dehading_master_id', '=', 'tally_deheading_master.id') 
        ->join('tally_deheading', 'tally_detail_deheading.tally_id', '=', 'tally_deheading.id')
        ->select('tally_deheading_master.id', 'tally_deheading_master.name', DB::raw('SUM(tally_detail_deheading.weight) as weight'), DB::raw('SUM(tally_detail_deheading.waste) as waste'))
        ->where('tally_deheading.purchase_id', $id) 
        ->groupBy('tally_deheading_master.id', 'tally_deheading_master.name')
        ->get();  

        return view ('home.tally.viewdeheading', compact('tally_detail', 'tally_detail_view', 'purchase'));
    }

    public function historydeheading($id)
    { 

        $purchase = DB::table('tally_deheading')
        ->join('products', 'tally_deheading.product_id', '=', 'products.id') 
        ->select('tally_deheading.purchase_id AS id', 'tally_deheading.trans_code', 'products.name', 'products.id AS product_id', 'tally_deheading.tally_type')
        ->where('tally_deheading.purchase_id', $id) 
        ->first(); 
 
        $tally_detail = TallyDeheadingMaster::all(); 

        $tally_detail_view = DB::table('tally_detail_deheading')
        ->join('tally_deheading_master', 'tally_detail_deheading.tally_dehading_master_id', '=', 'tally_deheading_master.id') 
        ->join('tally_deheading', 'tally_detail_deheading.tally_id', '=', 'tally_deheading.id')
        ->select('tally_deheading_master.id', 'tally_deheading_master.name', DB::raw('SUM(tally_detail_deheading.weight) as weight'), DB::raw('SUM(tally_detail_deheading.waste) as waste'))
        ->where('tally_deheading.purchase_id', $id) 
        ->groupBy('tally_deheading_master.id', 'tally_deheading_master.name')
        ->get(); 

        //dd($tally_detail_view);

        $tally_detail_view_count = DB::table('tally_detail_deheading')
        ->join('tally_deheading_master', 'tally_detail_deheading.tally_dehading_master_id', '=', 'tally_deheading_master.id') 
        ->join('tally_deheading', 'tally_detail_deheading.tally_id', '=', 'tally_deheading.id')
        ->select('tally_deheading_master.id', 'tally_deheading_master.name', DB::raw('COUNT(tally_detail_deheading.weight) as count_weight'))
        ->where('tally_deheading.purchase_id', $id) 
        ->groupBy('tally_deheading_master.id', 'tally_deheading_master.name')
        ->first();

        //dd($tally_detail_view);

        foreach ($tally_detail_view as $t_d_v) {
             $tally_detail_view = DB::table('tally_detail_deheading')
                        ->join('tally_deheading_master', 'tally_detail_deheading.tally_dehading_master_id', '=', 'tally_deheading_master.id') 
                        ->join('tally_deheading', 'tally_detail_deheading.tally_id', '=', 'tally_deheading.id')
                        ->select('tally_deheading_master.id', 'tally_deheading_master.name', 'tally_detail_deheading.weight', 'tally_detail_deheading.waste')
                        ->where('tally_deheading.purchase_id', $id)  
                        ->where('tally_deheading_master.id', '=', $t_d_v->id)
                        ->get(); 
            $array_val["name"] = $t_d_v->name;
            $tdvtd_arrays = null;
            foreach ($tally_detail_view as $tdvtd) {
                $tdvtd_array = null;
                $tdvtd_array["weight"] = $tdvtd->weight;
                $tdvtd_array["waste"] = $tdvtd->waste;
                $tdvtd_arrays[] = $tdvtd_array;
            }

            $array_val["detail"] = $tdvtd_arrays;
            $array_all[] = $array_val;
        }

        //dd($array_all);

        return view ('home.tally.historydeheading', compact('tally_detail', 'tally_detail_view', 'purchase', 'array_all', 'tally_detail_view_count'));
    }

    //PEELING 
    public function createpeeling($id)
    { 
       
         $purchase = DB::table('products')
        ->join('purchases_products', 'purchases_products.product_id', '=', 'products.id')
        ->join('purchases', 'purchases_products.purchase_id', '=', 'purchases.id')  
        ->select('purchases.id', 'purchases.doc_code', 'purchases.trans_code', 'products.name', 'products.id AS product_id', 'purchases.project')
        ->where('purchases.id', $id) 
        ->first();  

        $tally = Tally::where('purchase_id', $id)->first(); 

        $tally_detail = TallyPeelingMaster::all(); 

        return view ('home.tally.formpeeling', compact('tally_detail', 'purchase', 'tally'));
    }

     public function storepeeling(Request $request, $id)
    {
          
        $tally = new TallyPeeling;  
        $tally->trans_code = $request->input('trans_code'); 
        $tally->product_id = $request->input('product_id');
        $tally->purchase_id = $id;
        $tally->save(); 

        $purchase = Purchase::find($id);  
        $purchase->action_form_status = 'Edit';
        $purchase->save();

        foreach($request->input('tally') as $detail_key => $detail_value)
        {
            $tally_detail = new TallyDetailPeeling;
            $tally_detail->tally_id = $tally->id;
            $tally_detail->tally_peeling_master_id = $detail_key;
            $tally_detail->weight = $detail_value['weight'];
            $tally_detail->created_by = \Auth::id();
            $tally_detail->save();
        }
        return redirect()->action('Home\TallyController@detailpeeling', $id);
    }

     public function detailpeeling($id)
    { 

        $purchase = DB::table('tally')
        ->join('products', 'tally.product_id', '=', 'products.id') 
         ->join('purchases', 'tally.purchase_id', '=', 'purchases.id')  
        ->select('tally.purchase_id AS id', 'tally.trans_code', 'products.name', 'products.id AS product_id', 'tally.tally_type', 'purchases.project')
        ->where('tally.purchase_id', $id) 
        ->first(); 

        $tally_detail = TallyPeelingMaster::all(); 

        //dd($tally_detail);
 
        $tally_detail_view = DB::table('tally_detail_peeling')
        ->join('tally_peeling_master', 'tally_detail_peeling.tally_peeling_master_id', '=', 'tally_peeling_master.id') 
        ->join('tally_peeling', 'tally_detail_peeling.tally_id', '=', 'tally_peeling.id')
        ->select('tally_detail_peeling.tally_peeling_master_id AS id', 'tally_peeling_master.name', DB::raw('SUM(tally_detail_peeling.weight) as weight'))
        ->where('tally_peeling.purchase_id', $id) 
        ->groupBy('tally_detail_peeling.tally_peeling_master_id', 'tally_peeling_master.name')
        ->get(); 

        foreach ($tally_detail_view as $t_d_v) {
             $tally_detail_view_peeling = DB::table('tally_detail_peeling')
                ->join('tally_peeling_master', 'tally_detail_peeling.tally_peeling_master_id', '=', 'tally_peeling_master.id') 
                ->join('tally_peeling', 'tally_detail_peeling.tally_id', '=', 'tally_peeling.id')
                ->select('tally_detail_peeling.tally_peeling_master_id AS id', 'tally_peeling_master.name', 'tally_detail_peeling.weight')
                ->where('tally_peeling.purchase_id', $id)  
                ->where('tally_detail_peeling.tally_peeling_master_id', '=', $t_d_v->id)
                ->get(); 
            $array_val["name"] = $t_d_v->name;
            $tdvtd_arrays = null;
            foreach ($tally_detail_view_peeling as $tdvtd) {
                $tdvtd_array = null;
                $tdvtd_array["weight"] = $tdvtd->weight;
                $tdvtd_arrays[] = $tdvtd_array;
            }

            $array_val["detail"] = $tdvtd_arrays;
            $array_all[] = $array_val;
        }

        return view ('home.tally.detailpeeling', compact('tally_detail', 'tally_detail_view', 'purchase', 'array_all'));
    }

    public function storedetailpeeling(Request $request, $id)
    {
          
       
        $tally = TallyPeeling::where('purchase_id', $id)->first();  
        $tally->trans_code = $request->input('trans_code'); 
        $tally->product_id = $request->input('product_id');
        $tally->purchase_id = $id;
        $tally->save(); 



        foreach($request->input('tally') as $detail_key => $detail_value)
        {
            $tally_detail = new TallyDetailPeeling;
            $tally_detail->tally_id = $tally->id;
            $tally_detail->tally_peeling_master_id = $detail_key;
            $tally_detail->weight = $detail_value['weight'];
            $tally_detail->created_by = \Auth::id();
            $tally_detail->save();
        }

        return redirect()->action('Home\TallyController@detailpeeling', $id);

    }

    public function closepeeling($id)
    {
          
        $purchase = Purchase::find($id);  
        $purchase->form_status = 3;
        $purchase->action_form_status = 'Done';
        $purchase->save();
 
        return redirect()->action('Home\TallyController@index');

    }

    public function viewpeeling($id)
    { 

         $purchase = DB::table('tally_peeling')
        ->join('products', 'tally_peeling.product_id', '=', 'products.id') 
        ->select('tally_peeling.purchase_id AS id', 'tally_peeling.trans_code', 'products.name', 'products.id AS product_id')
        ->where('tally_peeling.purchase_id', $id) 
        ->first(); 
 
        $tally_detail_view = DB::table('tally_detail_peeling')
        ->join('tally_peeling_master', 'tally_detail_peeling.tally_peeling_master_id', '=', 'tally_peeling_master.id') 
        ->join('tally_peeling', 'tally_detail_peeling.tally_id', '=', 'tally_peeling.id')
        ->select('tally_detail_peeling.tally_peeling_master_id AS id', 'tally_peeling_master.name', DB::raw('SUM(tally_detail_peeling.weight) as weight'))
        ->where('tally_peeling.purchase_id', $id) 
        ->groupBy('tally_detail_peeling.tally_peeling_master_id', 'tally_peeling_master.name')
        ->get(); 

        return view ('home.tally.viewpeeling', compact('tally_detail_view', 'purchase'));
    }

    public function historypeeling($id)
    { 

         $purchase = DB::table('tally_peeling')
        ->join('products', 'tally_peeling.product_id', '=', 'products.id') 
        ->select('tally_peeling.purchase_id AS id', 'tally_peeling.trans_code', 'products.name', 'products.id AS product_id')
        ->where('tally_peeling.purchase_id', $id) 
        ->first(); 
 
        $tally_detail_view = DB::table('tally_detail_peeling')
        ->join('tally_peeling_master', 'tally_detail_peeling.tally_peeling_master_id', '=', 'tally_peeling_master.id') 
        ->join('tally_peeling', 'tally_detail_peeling.tally_id', '=', 'tally_peeling.id')
        ->select('tally_detail_peeling.tally_peeling_master_id AS id', 'tally_peeling_master.name', DB::raw('SUM(tally_detail_peeling.weight) as weight'))
        ->where('tally_peeling.purchase_id', $id) 
        ->groupBy('tally_detail_peeling.tally_peeling_master_id', 'tally_peeling_master.name')
        ->get(); 

         foreach ($tally_detail_view as $t_d_v) {
             $tally_detail_view = DB::table('tally_detail_peeling')
                ->join('tally_peeling_master', 'tally_detail_peeling.tally_peeling_master_id', '=', 'tally_peeling_master.id') 
                ->join('tally_peeling', 'tally_detail_peeling.tally_id', '=', 'tally_peeling.id')
                ->select('tally_detail_peeling.tally_peeling_master_id AS id', 'tally_peeling_master.name', 'tally_detail_peeling.weight')
                ->where('tally_peeling.purchase_id', $id)  
                ->where('tally_detail_peeling.tally_peeling_master_id', '=', $t_d_v->id)
                ->get(); 
            $array_val["name"] = $t_d_v->name;
            $tdvtd_arrays = null;
            foreach ($tally_detail_view as $tdvtd) {
                $tdvtd_array = null;
                $tdvtd_array["weight"] = $tdvtd->weight;
                $tdvtd_arrays[] = $tdvtd_array;
            }

            $array_val["detail"] = $tdvtd_arrays;
            $array_all[] = $array_val;
        }

        //dd($array_all_treatment);

        return view ('home.tally.historypeeling', compact('tally_detail_view', 'purchase', 'array_all'));
    }

    //GRADING
    public function creategrading($id)
    {
        $purchase = DB::table('products')
        ->join('purchases_products', 'purchases_products.product_id', '=', 'products.id')
        ->join('purchases', 'purchases_products.purchase_id', '=', 'purchases.id')  
        ->select('purchases.id', 'purchases.doc_code', 'purchases.trans_code', 'products.name', 'products.id AS product_id', 'purchases.form_status', 'purchases.action_form_status', 'purchases.project')
        ->where('purchases.id', $id)->first();

        $tally = Tally::where('purchase_id', $id)->first();

        $grades = Grade::all();

        // $code_sizes = CodeSize::all();

        $code_sizes = DB::table('code_size')
        ->select('code_size.id', 'code_size.name', 'code_size.block_weight', 'sizes.note', 'freezing_type.name AS freezing_name')
        ->join('sizes', 'sizes.id', '=', 'code_size.id_size')
        ->join('freezing_type', 'freezing_type.id', '=', 'code_size.id_freezing_type')
        ->get();

        $qty_types = QtyType::all();

        $grading_view_sql = '
        SELECT
            purchases.trans_code AS trans_code,
            qty_type.name AS qty_name,
            grade.name AS grade,
            sizes.note AS size,
            freezing_type.name AS freezing,
            code_size.name AS code_size,
            SUM(code_size.block_weight) AS block,
            tally_grading_master.id AS tally_grading_master_id,
            tally_grading_master.purchase_id AS grading_purchase_id,
            tally_grading_detail.id AS tally_grading_detail_id,
            SUM(tally_grading_detail.total_block) AS total_block,
            SUM(tally_grading_detail.rest) AS rest
        FROM
            tally_grading_master
        INNER JOIN tally_grading_detail ON tally_grading_detail.id_tally_grading_master = tally_grading_master.id
        LEFT JOIN grade ON tally_grading_detail.id_grade = grade.id
        LEFT JOIN code_size ON code_size.id = tally_grading_detail.id_code_size
        LEFT JOIN sizes ON code_size.id_size = sizes.id
        LEFT JOIN freezing_type ON code_size.id_freezing_type = freezing_type.id
        LEFT JOIN qty_type ON tally_grading_detail.id_qty_type = qty_type.id
        INNER JOIN purchases ON tally_grading_master.purchase_id = purchases.id
        GROUP BY 
            purchases.trans_code,
            qty_type.name,
            grade.name,
            sizes.note,
            freezing_type.name,
            code_size.name
        ORDER BY grade.name ASC';

        $grading_view = DB::table(DB::raw("($grading_view_sql) as rs_grading_view_sql"))->where('grading_purchase_id', $id)->get();

        $grading_history_sql = '
        SELECT
            purchases.trans_code AS trans_code,
            qty_type.name AS qty_name,
            grade.name AS grade,
            sizes.note AS size,
            freezing_type.name AS freezing,
            code_size.name AS code_size,
            code_size.block_weight AS block,
            tally_grading_master.id AS tally_grading_master_id,
            tally_grading_master.purchase_id AS grading_purchase_id,
            tally_grading_detail.id AS tally_grading_detail_id,
            tally_grading_detail.total_block AS total_block,
            tally_grading_detail.rest AS rest
        FROM
            tally_grading_master
        INNER JOIN tally_grading_detail ON tally_grading_detail.id_tally_grading_master = tally_grading_master.id
        LEFT JOIN grade ON tally_grading_detail.id_grade = grade.id
        LEFT JOIN code_size ON code_size.id = tally_grading_detail.id_code_size
        LEFT JOIN sizes ON code_size.id_size = sizes.id
        LEFT JOIN freezing_type ON code_size.id_freezing_type = freezing_type.id
        LEFT JOIN qty_type ON tally_grading_detail.id_qty_type = qty_type.id
        INNER JOIN purchases ON tally_grading_master.purchase_id = purchases.id';

        $grading_history = DB::table(DB::raw("($grading_history_sql) as rs_grading_history_sql"))
        ->where('grading_purchase_id', $id)->orderBy('tally_grading_detail_id', 'desc')->get();

        // dd($grading_view);

        return view ('home.tally.formgrading', compact('purchase', 'tally', 'grades', 'qty_types', 'code_sizes', 'grading_view', 'grading_history'));
    }

    public function storegrading(Request $request, $id)
    {
        $exist_master = TallyGradingMaster::where('purchase_id', '=', $id)->first();

        if(!$exist_master)
        {
            $grading = new TallyGradingMaster;
            $grading->purchase_id = $id;
            $grading->save();

            $grading_detail = new TallyDetailGrading;
            $grading_detail->id_tally_grading_master = $grading->id;
            $grading_detail->id_qty_type = $request->input('qty_type');
            $grading_detail->id_grade = $request->input('grade_id');
            $grading_detail->id_code_size = $request->input('code_size_id');
            // $grading_detail->block = $request->input('block_weight');
            $grading_detail->total_block = $request->input('total_block');
            $grading_detail->rest = $request->input('rest');
            $grading_detail->save();

            $purchase = DB::table('purchases')->where('id', $id)->update(['form_status' => 4, 'action_form_status' => 'Edit']);
        }
        else
        {
            $grading_detail = new TallyDetailGrading;
            $grading_detail->id_tally_grading_master = $exist_master->id;
            $grading_detail->id_qty_type = $request->input('qty_type');
            $grading_detail->id_grade = $request->input('grade_id');
            $grading_detail->id_code_size = $request->input('code_size_id');
            // $grading_detail->block = $request->input('block_weight');
            $grading_detail->total_block = $request->input('total_block');
            $grading_detail->rest = $request->input('rest');
            $grading_detail->save();
        }

        return redirect()->action('Home\TallyController@creategrading', $id);
    }

    public function closegrading(Request $request, $id)
    {
        $purchase = DB::table('purchases')->where('id', $id)->update(['form_status' => 4, 'action_form_status' => 'Done']);
        
        return redirect()->action('Home\TallyController@index');
    }


    //PACKING 
    public function createpacking($id)
    { 
       
         $purchase = DB::table('products')
        ->join('purchases_products', 'purchases_products.product_id', '=', 'products.id')
        ->join('purchases', 'purchases_products.purchase_id', '=', 'purchases.id')  
        ->select('purchases.id', 'purchases.doc_code', 'purchases.trans_code', 'products.name', 'products.id AS product_id')
        ->where('purchases.id', $id) 
        ->first();  

        $tally = TallyGradingMaster::where('purchase_id', $id)->first(); 

        //$tally_detail = TallyPeelingMaster::all(); 

        $sql_detail = 'SELECT
                        tally_grading_detail.id,
                        purchases.trans_code AS purchase_code,
                        grade.`name` AS grade,
                        sizes.note AS size,
                        freezing_type.`name` AS freezing,
                        code_size.`name` AS code_size,
                        tally_grading_detail.block,
                        tally_grading_detail.total_block,
                        tally_grading_detail.mix,
                        tally_grading_detail.icr,
                        qty_type.`name` AS qty_type,
                        view_tally_packing_master.total,
                        FLOOR(
                            tally_grading_detail.total_block / view_tally_packing_master.total
                        ) AS mc,
                        CONCAT(
                            "0.",
                            RIGHT (
                                FORMAT(
                                    tally_grading_detail.total_block / view_tally_packing_master.total,
                                    2
                                ),
                                2
                            )
                        ) * view_tally_packing_master.total AS ic
                    FROM
                        tally_grading_master
                    INNER JOIN tally_grading_detail ON tally_grading_detail.id_tally_grading_master = tally_grading_master.id
                    LEFT JOIN grade ON tally_grading_detail.id_grade = grade.id
                    LEFT JOIN code_size ON code_size.id = tally_grading_detail.id_code_size
                    LEFT JOIN sizes ON code_size.id_size = sizes.id
                    LEFT JOIN freezing_type ON code_size.id_freezing_type = freezing_type.id
                    LEFT JOIN qty_type ON tally_grading_detail.id_qty_type = qty_type.id
                    INNER JOIN purchases ON tally_grading_master.purchase_id = purchases.id
                    LEFT JOIN view_tally_packing_master ON tally_grading_detail.id = view_tally_packing_master.id
                        WHERE purchases.id = '.$id.'';
        $sql_order = 'rs_sql_detail.qty_type, rs_sql_detail.grade';
        $tally_detail = DB::table(DB::raw("($sql_detail) as rs_sql_detail"))
                        ->orderByRaw($sql_order . " ASC")->get();

        //dd($tally_detail);

        //dd($tally_detail);

        return view ('home.tally.formpacking', compact('tally_detail', 'purchase', 'tally'));
    }

     public function storepacking(Request $request, $id)
    {
          
        $purchase = Purchase::find($id);  
        $purchase->action_form_status = 'Edit';
        $purchase->save();

        foreach($request->input('tally') as $detail_key => $detail_value)
        { 
            $tally_detail = TallyDetailGrading::where('id', $detail_key)->first();  
            $tally_detail->ic = $detail_value['ic']; 
            $tally_detail->mc = $detail_value['mc'];
            $tally_detail->mix = $detail_value['mix'];
            $tally_detail->icr = $detail_value['icr'];
            $tally_detail->save();
        }
        return redirect()->action('Home\TallyController@viewpacking', $id);
    }
 
   
    public function closepacking($id)
    {
          
        $purchase = Purchase::find($id);  
        $purchase->form_status = 5;
        $purchase->action_form_status = 'Done';
        $purchase->save();
 
        return redirect()->action('Home\TallyController@index');

    }

    public function viewpacking($id)
    { 

         $purchase = DB::table('tally_grading_master') 
        ->select('tally_grading_master.purchase_id AS id', 'tally_grading_master.trans_code')
        ->where('tally_grading_master.purchase_id', $id) 
        ->first(); 

        //dd($purchase);
 
        $sql_detail = 'SELECT
                            tally_grading_detail.id,
                            purchases.trans_code AS purchase_code,
                            grade.`name` AS grade,
                            sizes.note AS size,
                            freezing_type.`name` AS freezing,
                            code_size.`name` AS code_size,
                            tally_grading_detail.block,
                            tally_grading_detail.total_block,
                            tally_grading_detail.ic,
                            tally_grading_detail.mc,
                            tally_grading_detail.mix,
                            tally_grading_detail.icr
                        FROM
                            tally_grading_master
                        INNER JOIN tally_grading_detail ON tally_grading_detail.id_tally_grading_master = tally_grading_master.id
                        LEFT JOIN grade ON tally_grading_detail.id_grade = grade.id
                        LEFT JOIN code_size ON code_size.id = tally_grading_detail.id_code_size
                        LEFT JOIN sizes ON code_size.id_size = sizes.id
                        LEFT JOIN freezing_type ON code_size.id_freezing_type = freezing_type.id
                        LEFT JOIN qty_type ON tally_grading_detail.id_qty_type = qty_type.id
                        INNER JOIN purchases ON tally_grading_master.purchase_id = purchases.id
                        WHERE purchases.id = '.$id.'
                        ORDER BY
                            grade.`name` ASC';
        $tally_detail = DB::table(DB::raw("($sql_detail) as rs_sql_detail"))->get();

        return view ('home.tally.viewpacking', compact('tally_detail', 'purchase'));
    }

}
