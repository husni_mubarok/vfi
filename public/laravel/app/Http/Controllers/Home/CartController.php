<?php

namespace App\Http\Controllers\Home;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\CartRequest;
use App\Http\Requests\CartdetRequest;
use App\Http\Requests\ProductionExportRequest;
use App\Http\Requests\ProductionExportdetRequest;
use App\Http\Controllers\Controller;
use App\Model\Cart;
use App\Model\CartDet;
use App\Model\ProductionExport;
use App\Model\ProductionExportDet;
use App\Model\Rnd;
use App\Model\RndDet;
use App\Model\TempStorage;
use App\Model\SourceStorage;
use Datatables;

class CartController extends Controller
{
    public function index()
    {
        $carts = Cart::all();
        //dd($carts);
        return view ('home.cart.index', compact('carts'));
    }
    public function data(Request $request)
    {   
        if($request->ajax()){
            $cart = Cart::select('id', 'doc_code', 'status', 'date')
            ->get();

            //return response()->json($cart);
            return Datatables::of($cart)
            ->addColumn('action', function ($cart) {
                if ($cart->status==0) {
                    $swstatus = "<span class='label label-danger'>Waiting</span>";
                } elseif ($cart->status==1) {
                    $swstatus = "<span class='label label-success'>Confirmed</span>";
                } elseif ($cart->status==2) {
                    $swstatus = "<span class='label label-success'>Exported</span>";
                } elseif ($cart->status==3) {
                    $swstatus = "<span class='label label-success'>Sashimi</span>";
                } elseif ($cart->status==4) {
                    $swstatus = "<span class='label label-success'>Added Value</span>";
                } elseif ($cart->status==5) {
                    $swstatus = "<span class='label label-success'>R n D</span>";
                }
                return ''.$swstatus.'';
            })

            //add status coloumn
            ->addColumn('dnstatus', function ($cart) {
                if ($cart->status>1) {
                    $dnstatus = "<span class='label label-success'>Done</span>";
                } else {
                    $dnstatus = "<span class='label label-danger'>Waiting</span>";                    
                }
                return ''.$dnstatus.'';
            })

            //add delete coloumn
            ->addColumn('actiondl', function ($cart) {
                if ($cart->status==0) {
                    return \Form::open(array('method'=>'DELETE', 'class'=>'delete', 'route' => array('home.cart.delete',"$cart->id"))) .
                    \Form::submit('Delete', array('class'=>'btn btn-xs btn-default btn-sm', 'id'=>'delete')) .
                    \Form::close();

                } elseif ($cart->status==1 or $cart->status) {
                    return  \Form::open(array('method'=>'DELETE', 'class'=>'delete', 'route' => array('home.cart.delete',"$cart->id"))) .
                    \Form::submit('Delete', array('class'=>'btn btn-xs btn-default btn-sm', 'disabled'=>'disabled')) .
                    \Form::close();
                }
            })   
            ->addColumn('actioned', function ($cart) {
                
                if ($cart->status==0) {
                return '<a href="cart/'.$cart->id.'/editdet" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-edit"></i></a>
                 <a href="cart/'.$cart->id.'/search" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-search"></i></a>';
                 } else {
                return '<a href="#" class="btn btn-xs btn-default" disabled="disabled"><i class="glyphicon glyphicon-edit"></i></a>
                 <a href="cart/'.$cart->id.'/search" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-search"></i></a>';
                 }
            })
          
            ->addColumn('process', function ($cart) {
                if ($cart->status==1) {

                return '<a href="cart/'.$cart->id.'/export" class="btn btn-xs btn-default"><i class="fa fa-external-link-square"></i> EXP</a>
                    <a href="cart/'.$cart->id.'/rnd" class="btn btn-xs btn-default"><i class="fa fa-external-link-square"></i> R n D</a>
                    <a href="sashimi/'.$cart->id.'/import" class="btn btn-xs btn-default"><i class="fa fa-plus-circle"></i> End Product</a> ';
                } else {

                return '<a href="#" class="btn btn-xs btn-default" disabled="disabled"><i class="fa fa-external-link-square"></i> EXP</a>
                    <a href="#" class="btn btn-xs btn-default" disabled="disabled"><i class="fa fa-external-link-square"></i> R n D</a>
                    <a href="#" class="btn btn-xs btn-default" disabled="disabled"><i class="fa fa-plus-circle"></i> End Product</a> ';

                }
            })

           
            //$data     = $cart->getData(true);
            //return new JsonResponse($data);
            // ->filter(function ($instance) use ($request) {
            //     if ($request->has('datefrom')) {
            //         $datefrom->collection = $instance->collection->filter(function ($row) use ($request) {
            //             return Str::contains($row['date'], $request->get('date')) ? true : false;
            //         });
            //     }

            // })
            ->make(true);

        } else {
            exit("No data available");
        }
    }

    public function getDetailsData()
    {
        $posts = CartDet::all();

        //dd($posts);
        return response()->json($posts);
        //return Datatables::of($posts)->make(true);
    }
    public function create()
    {
        $cartdets = CartDet::where('id_cart', '')  
            ->get();
        return view ('home.cart.form', compact('cartdets'));
    }

    public function store(CartRequest $request)
    {
        DB::beginTransaction();
        try {   
            //get last transaction id
            $maxlastid = DB::table('cart')->max('id')+1;
            
            $Cart = new Cart;
            $Cart->doc_code = 'DOC/EXP/'.$maxlastid;
            $Cart->date = $request->input('date');
            $Cart->save();
            
            //get last header id
            $lastheaderid = $Cart->id;
     
            //insert export detail
            $productionresult = new CartDet;
            $productionresult->quantity = $request->input('quantity');
            $productionresult->name = $request->input('name');
            $productionresult->source_id = $request->input('source_id');
            $productionresult->size = $request->input('size');
            $productionresult->unit = $request->input('unitedit');
            $productionresult->block = $request->input('block');
            $productionresult->weight = $request->input('weight');
            $productionresult->block_weight = $request->input('block_weight');
            $productionresult->criteria = $request->input('criteria');
            $productionresult->block_weight = $request->input('block_weight');
            $productionresult->production_id = $request->input('production_id');
            $productionresult->id_cart = $lastheaderid;
            $productionresult->save();

            DB::commit();

            // Add temp to stock
            // $lastdetailid = $productionresult->id;
               
            // insert temp storage
            // $tempstorage = new TempStorage;     
            // $tempstorage->datetime = $request->input('date');
            // $tempstorage->quantity = $request->input('quantity');
            // $tempstorage->source_id = $request->input('source_id');
            // $tempstorage->production_id = $request->input('production_id');
            // $tempstorage->source_type = 1;
            // $tempstorage->unit = $request->input('unitedit');
            // $tempstorage->weight = $request->input('weight');
            // $tempstorage->status = 0;
            // $tempstorage->status_storage_id = 1;
            // $tempstorage->id_export = $lastdetailid;
            // $tempstorage->transaction_type = 'Out';
            // $tempstorage->save();
            return redirect('export/cart/'.$Cart->id.'/edit');
            } catch (\Exception $e) {
                DB::rollback();
                DB::statement('execute error!');
        }
        
    }
    
    public function edit($id)
    {
        $cartdets = CartDet::where('id_cart', $id)  
            ->get();
        $cart = Cart::Find($id);
        return view ('home.cart.form', compact('cart', 'cartdets'));
    }

    public function update($id, CartRequest $request)
    {
        DB::beginTransaction();
        try {       
            $Cart = Cart::Find($id);
            $Cart->date = $request->input('date');
            $Cart->save();

            //Insert export detail
            $productionresult = new CartDet;
            $productionresult->quantity = $request->input('quantity');
            $productionresult->name = $request->input('name');
            $productionresult->source_id = $request->input('source_id');
            $productionresult->size = $request->input('size');
            $productionresult->unit = $request->input('unitedit');
            $productionresult->block = $request->input('block');
            $productionresult->weight = $request->input('weight');
            $productionresult->block_weight = $request->input('block_weight');
            $productionresult->criteria = $request->input('criteria');
            $productionresult->production_id = $request->input('production_id');
            $productionresult->id_cart = $id;
            $productionresult->save();

            DB::commit();
            
            //$lastdetailid = $productionresult->id;

            //update temp storage
            // $tempstorage = new TempStorage;     
            // $tempstorage->datetime = $request->input('date');
            // $tempstorage->quantity = $request->input('quantity');
            // $tempstorage->source_id = $request->input('source_id');
            // $tempstorage->production_id = $request->input('production_id');
            // $tempstorage->source_type = 1;
            // $tempstorage->unit = $request->input('unitedit');
            // $tempstorage->weight = $request->input('weight');
            // $tempstorage->status = 0;
            // $tempstorage->status_storage_id = 1;
            // $tempstorage->id_export = $lastdetailid;
            // $tempstorage->transaction_type = 'Out';
            // $tempstorage->save();
            return redirect('export/cart/'.$Cart->id.'/edit');

        } catch (\Exception $e) {
                DB::rollback();
                DB::statement('execute error!');
        }
    }

    public function updatedetail($id, CartdetRequest $requestdet)
    {
        
        //dd($requestdet->input());
        $iddetail = $requestdet->input('iddetail');

        //update detail
        $productionresult = CartDet::Find($iddetail);
        $productionresult->quantity = $requestdet->input('quantityedit');
        $productionresult->name = $requestdet->input('nameedit');
        $productionresult->size = $requestdet->input('sizeedit');
        $productionresult->block = $requestdet->input('blockedit');
        $productionresult->weight = $requestdet->input('weightedit');
        $productionresult->criteria = $requestdet->input('criteriaedit');
        $productionresult->save();

        //update temp storage
        // $tempstorage = TempStorage::Where('id_export',$iddetail)->first();
        // $tempstorage->quantity = $requestdet->input('quantityedit');
        // $tempstorage->save();

        return redirect('export/cart/'.$productionresult->id_cart.'/edit');
    }

    public function export($id)
    {

        $cartdets = CartDet::where('id_cart', $id)  
            ->get();
        $cart = Cart::Find($id);
        return view ('home.cart.export', compact('tempstorage', 'cart', 'cartdets'));
    }
    public function search($id)
    {

        $cartdets = CartDet::where('id_cart', $id)  
            ->get();
        $cart = Cart::Find($id);
        return view ('home.cart.search', compact('tempstorage', 'cart', 'cartdets'));
    }
    public function storeexport($id)
    {
        DB::beginTransaction();
        try {   
            //get last transaction id
            $maxlastid = DB::table('export')->max('id')+1;
            $now = Carbon::now();

            $productionexport = new ProductionExport;
            $productionexport->doc_code = 'DOC/EXP/'.$maxlastid;
            $productionexport->date = $now;
            $productionexport->cart_id = $id;
            $productionexport->save();
           
           //get last header id
            $lastheaderid = $productionexport->id;

            //insert export detail
            $selectdetail = CartDet::where('id_cart', $id)
                      ->select(array('id', 'quantity','name','source_id', 'unit', 'size','block','weight','criteria', 'production_id'))->get();        
            // dd($selectdetail);
            for($i=0;$i<count($selectdetail);$i++){
                $exportdet = new ProductionExportDet();
                $exportdet->id_export = $lastheaderid;
                $exportdet->name = $selectdetail[$i]->name;
                $exportdet->quantity = $selectdetail[$i]->quantity;
                $exportdet->source_id = $selectdetail[$i]->source_id;
                $exportdet->size = $selectdetail[$i]->size;
                $exportdet->block = $selectdetail[$i]->block;
                $exportdet->weight = $selectdetail[$i]->weight;
                $exportdet->criteria = $selectdetail[$i]->criteria;
                $exportdet->production_id = $selectdetail[$i]->production_id;
                $exportdet->save();
            }

            for($i=0;$i<count($selectdetail);$i++){
                $tempstorage = new TempStorage;     
                $tempstorage->datetime = $now;
                $tempstorage->quantity = $selectdetail[$i]->quantity;
                $tempstorage->source_id = $selectdetail[$i]->source_id;
                $tempstorage->production_id = $selectdetail[$i]->production_id;
                $tempstorage->source_type = 1;
                $tempstorage->unit = $selectdetail[$i]->unit;
                $tempstorage->weight = $selectdetail[$i]->weight;
                $tempstorage->status = 0;
                $tempstorage->status_storage_id = 1;
                $tempstorage->id_export = $selectdetail[$i]->id;
                $tempstorage->transaction_type = 'Out';
                $tempstorage->save();
            }

            $cart = Cart::Find($id);
            $cart->status = 2;
            $cart->save();

            DB::commit();

            return redirect()->action('Home\CartController@index');

            } catch (\Exception $e) {
                DB::rollback();
                DB::statement('execute error!');
        }

    }


    public function rnd($id)
    {

        $cartdets = CartDet::where('id_cart', $id)  
            ->get();
        $cart = Cart::Find($id);
        return view ('home.cart.rnd', compact('tempstorage', 'cart', 'cartdets'));
    }
    public function storernd($id)
    {
        
        DB::beginTransaction();
        try {
 
            //get last transaction id
            $maxlastid = DB::table('rnd')->max('id')+1;
            $now = Carbon::now();

            $productionexport = new Rnd;
            $productionexport->doc_code = 'DOC/RND/'.$maxlastid;
            $productionexport->date = $now;
            $productionexport->cart_id = $id;
            $productionexport->save();
           
           //get last header id
            $lastheaderid = $productionexport->id;

            //insert export detail
            $selectdetail = CartDet::where('id_cart', $id)
                      ->select(array('id', 'quantity','name','source_id', 'unit', 'size','block','weight','criteria', 'production_id'))->get();         
            // dd($selectdetail);
            for($i=0;$i<count($selectdetail);$i++){
                $exportdet = new RndDet();
                $exportdet->id_rnd = $lastheaderid;
                $exportdet->name = $selectdetail[$i]->name;
                $exportdet->quantity = $selectdetail[$i]->quantity;
                $exportdet->source_id = $selectdetail[$i]->source_id;
                $exportdet->size = $selectdetail[$i]->size;
                $exportdet->block = $selectdetail[$i]->block;
                $exportdet->weight = $selectdetail[$i]->weight;
                $exportdet->criteria = $selectdetail[$i]->criteria;
                $exportdet->production_id = $selectdetail[$i]->production_id;
                $exportdet->save();
            }

            for($i=0;$i<count($selectdetail);$i++){
                $tempstorage = new TempStorage;     
                $tempstorage->datetime = $now;
                $tempstorage->quantity = $selectdetail[$i]->quantity;
                $tempstorage->source_id = $selectdetail[$i]->source_id;
                $tempstorage->production_id = $selectdetail[$i]->production_id;
                $tempstorage->source_type = 7;
                $tempstorage->unit = $selectdetail[$i]->unit;
                $tempstorage->weight = $selectdetail[$i]->weight;
                $tempstorage->status = 0;
                $tempstorage->status_storage_id = 1;
                $tempstorage->id_export = $selectdetail[$i]->id;
                $tempstorage->transaction_type = 'Out';
                $tempstorage->save();
            }

            $cart = Cart::Find($id);
            $cart->status = 5;
            $cart->save();

            DB::commit();

            return redirect()->action('Home\CartController@index');

            } catch (\Exception $e) {
                DB::rollback();
                DB::statement('execute error!');
        }
    }

    /**
     * [valueadded description]
     * @param  [int] $id [cart_id]
     * @return [view]     [description]
     */
     public function valueadded($id)
    {

        $cartdets = CartDet::where('id_cart', $id)  
            ->get();
        $cart = Cart::Find($id);
        return view ('home.cart.value_added', compact('tempstorage', 'cart', 'cartdets'));
    }


   //change status to confirm
   public function storeconfirm($id)
    {
        
        $Cart = Cart::Find($id);
        $Cart->status = 1;
        $Cart->save();
        
        return redirect()->action('Home\CartController@index');
    }
    
   //delete header
   public function delete($id)
    {
        Cart::Find($id)->delete();
        CartDet::Where('id_cart',$id)
                ->first()
                ->delete();
        //TempStorage::Where('id_export',$id)
          //      ->first()
            //    ->delete();

        return redirect()->back();
    }

    //delete detail
    public function deletedet($id)
    {
        CartDet::Find($id)->delete();
        // TempStorage::Where('id_export',$id)
        //         ->first()
        //         ->delete();
        return redirect()->back();
    }

    public function sashimi($id)
    {
        $cart = Cart::Find($id);
        return redirect()->action('Editor\SashimiController@import', $id)->with('cart', $cart);
    }
}