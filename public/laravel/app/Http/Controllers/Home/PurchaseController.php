<?php

namespace App\Http\Controllers\Home;

use DB;
use Illuminate\Http\Request;
use App\Http\Requests\PurchaseRequest;
use App\Http\Requests\PriceRequest;
use App\Http\Requests\PurchaseDetailRequest;
use App\Http\Controllers\Controller;
use App\Repository\PurchaseRepository;
use App\Repository\ProductRepository;
use App\Repository\SupplierRepository;
use App\Repository\PriceRepository;
use App\Repository\PurchaseDetailRepository;
use App\Model\Purchase;

class PurchaseController extends Controller
{
    protected $PurchaseRepository;
    protected $ProductRepository;
    protected $SupplierRepository;
    protected $PriceRepository;
    protected $PurchaseDetailRepository;

    public function __construct(PurchaseRepository $purchase_repository, ProductRepository $product_repository, SupplierRepository $supplier_repository, PriceRepository $price_repository, PurchaseDetailRepository $purchase_detail_repository)
    {
        $this->PurchaseRepository = $purchase_repository;
        $this->ProductRepository = $product_repository;
        $this->SupplierRepository = $supplier_repository;
        $this->PriceRepository = $price_repository;
        $this->PurchaseDetailRepository = $purchase_detail_repository;
    }

    public function index()
    {
        $project_list = Purchase::groupBy('project')->pluck('project')->toArray();
        array_unshift($project_list, 'Actual Delivery');

        $pending_approval = Purchase::where('status', 'Waiting COO')->orWhere('status', 'Waiting QC')->latest()->get();

        return view ('home.purchase.index', compact('pending_approval', 'project_list'));
    }

    public function create_purchase()
    {
        $products = $this->ProductRepository->get_list();
        $suppliers = $this->SupplierRepository->get_list();
        return view ('home.purchase.purchase', compact('products', 'suppliers'));
    }

    public function store_purchase(PurchaseRequest $request)
    {
        $purchase = $this->PurchaseRepository->store($request->input());
        return redirect()->action('Home\PurchaseController@summary', $purchase->id);
    }

    public function edit_purchase($id)
    {
        $purchase = $this->PurchaseRepository->get_one($id);
        return view ('home.purchase.purchase', compact('purchase'));
    }

    public function update_purchase($id, Request $request)
    {
        $purchase = $this->PurchaseRepository->update($id, $request->input());
        return redirect()->action('Home\PurchaseController@summary', $id);
    }


    public function create_price($id)
    {
        $purchase = $this->PurchaseRepository->get_one($id);
        $previous_purchase = $this->PurchaseRepository->get_previous($id, $purchase->purchase_product->product_id);
        // dd($purchase);
        if($previous_purchase)
        {
            $previous_price = $this->PriceRepository->get_previous($previous_purchase->purchase_id);
            return view ('home.purchase.price', compact('purchase', 'previous_price'));
        }

        return view ('home.purchase.price', compact('purchase'));
    }

    public function store_price($id, PriceRequest $request)
    {
        $this->PriceRepository->store($id, $request->input('value'));
        return redirect()->action('Home\PurchaseController@summary', $id);
    }

    public function edit_price($id)
    {
        $purchase = $this->PurchaseRepository->get_one($id);
        $prices = $purchase->price;
        return view ('home.purchase.price', compact('purchase', 'prices'));
    }

    public function create_purchase_detail($id)
    {
        $purchase = $this->PurchaseRepository->get_one($id);
        return view ('home.purchase.purchase_detail', compact('purchase'));
    }

    public function store_purchase_detail($id, PurchaseDetailRequest $request)
    {
       

        $purchase = $this->PurchaseRepository->get_one($id);

        $purchase_detail = $this->PurchaseDetailRepository->store($id, $request->input('size'), $request->input('price'), $request->input('amount'), $purchase->trans_code);
        return redirect()->action('Home\PurchaseController@summary', $id);
    }

    public function edit_purchase_detail($id)
    {
        $purchase = $this->PurchaseRepository->get_one($id);
        $purchase_details = $purchase->purchase_detail;

        foreach($purchase->purchase_detail as $key => $purchase_detail)
        {
            $purchase_details[$key] = collect([
                'size' => $purchase_detail->size,
                'price' => $purchase_detail->price,
                'amount' => $purchase_detail->amount,
                'trans_code' => $purchase_detail->trans_code,
                'subtotal' => $purchase_detail->amount * $purchase_detail->price
                ]);
        }

        return view ('home.purchase.purchase_detail', compact('purchase', 'purchase_details'));
    }

    public function summary($id)
    {
        $purchase = $this->PurchaseRepository->get_one($id);
        return view ('home.purchase.summary', compact('purchase'));
    }

    public function confirm_po_rm($id)
    {
        $purchase = $this->PurchaseRepository->update_status($id, 'Waiting COO');
        return redirect()->action('Home\PurchaseController@summary', $id);
    }

    public function confirm_actual_delivery($id)
    {
        $purchase = $this->PurchaseRepository->update_status($id, 'Waiting QC');
        return redirect()->action('Home\PurchaseController@summary', $id);
    }

    public function reject($id, Request $request)
    {
        $reject = $this->PurchaseRepository->reject($id, $request->input());
        return redirect()->action('Home\PurchaseController@summary', $id);
    }

    public function approve($id)
    {
        $this->PurchaseRepository->approve($id);
        return redirect()->action('Home\PurchaseController@summary', $id);
    }

    public function close($id)
    {
        $purchase = Purchase::find($id);
        $purchase->status = 'Processed';
        $purchase->save();
        return redirect()->action('Home\PurchaseController@summary', $purchase->id);
    }

    public function delete($id)
    {
        Purchase::find($id)->delete();
        return redirect()->action('Home\PurchaseController@index');
    }
}
