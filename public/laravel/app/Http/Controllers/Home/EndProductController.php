<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Requests\ProductionResultRequest;
use App\Http\Requests\ProductionResultVariableRequest;
use App\Http\Requests\ResultRequest;
use App\Http\Requests\RejectRequest;
use App\Http\Controllers\Controller;
use App\Model\TempStorage;
use App\Model\Cart;
use App\Model\CartDet;
use App\Model\Production;
use App\Model\ProductionResult;
use App\Model\ProductionResultVariable;
use App\Model\ProductionResultDetail;
use App\Model\ProductionResultAddition;
use App\Model\RejectReason;
use App\Model\RejectType;

class EndProductController extends Controller
{
    public function index()
    {
    	$carts = Cart::where('status', 1)->orWhere('status', 3)->orWhere('status', 4)->get();
        $end_products = ProductionResult::where('production_type', 'sashimi')->orWhere('production_type', 'value_added')->orderBy('id', 'desc')->get();
    	return view ('home.end_product.index', compact('carts', 'end_products'));
    }

    public function create($id)
    {
    	$cart = Cart::Find($id);
    	$products = Production::where('type', 'sashimi')->orwhere('type', 'value_added')->pluck('name', 'id');
    	return view ('home.end_product.form', compact('cart', 'products'));
    }

    public function store($id, ProductionResultRequest $request)
    {
        ProductionResult::where('cart_id', $id)->delete();
        $product_type = Production::find($request->input('production_id'))->type;

        $cart = Cart::find($request->input('cart_id'));
        if($product_type == 'sashimi')
        {
            $cart->status = '3';
        } 
        elseif($product_type == 'value_added') 
        {
            $cart->status = '4';
        }

        $cart->save();

    	$end_product = new ProductionResult;
    	$end_product->cart_id = $request->input('cart_id');
    	$end_product->production_id = $request->input('production_id');
    	$end_product->production_type = $product_type;
    	$end_product->started_at = $request->input('started_at');
    	$end_product->block_weight = $request->input('block_weight');
    	$end_product->amount = $request->input('amount');
    	$end_product->weight = $request->input('weight');
    	$end_product->save();

    	return redirect()->action('Home\EndProductController@create_waste', $end_product->id);
    }

    public function edit($id)
    {
        $end_product = ProductionResult::find($id);
        $products = Production::where('type', 'sashimi')->orwhere('type', 'value_added')->pluck('name', 'id');
        return view ('home.end_product.form', compact('end_product', 'products'));
    }

    public function update($id, Request $request)
    {
        $end_product = ProductionResult::find($id);
        $end_product->production_id = $request->input('production_id');
        $end_product->started_at = $request->input('started_at');
        $end_product->block_weight = $request->input('block_weight');
        $end_product->save();

        return redirect()->action('Home\EndProductController@summary', $id);
    }

    public function create_waste($id)
    {
    	$end_product = ProductionResult::find($id);
    	return view ('home.end_product.waste', compact('end_product'));
    }

    public function store_waste($id, ProductionResultVariableRequest $request)
    {
        $end_product = ProductionResult::find($id);
        $end_product->status = 1;
        $end_product->save();
    	ProductionResultVariable::where('production_result_id', $id)->delete();
        if($request->input('waste'))
        {
            foreach($request->input('waste') as $key => $waste)
            {
                $production_result_variable = new ProductionResultVariable;
                $production_result_variable->name = $waste['name'];
                $production_result_variable->value = $waste['value'];
                $production_result_variable->production_result_id = $id;

                $production_result_variable->save();
            }
        }
        return redirect()->action('Home\EndProductController@create_result', $id);
    }

    public function edit_waste($id)
    {
        $end_product = ProductionResult::find($id);
        $end_product_waste = $end_product->production_result_variable;
        return view ('home.end_product.waste', compact('end_product', 'end_product_waste'));
    }

    public function update_waste($id, ProductionResultVariableRequest $request)
    {
        ProductionResultVariable::where('production_result_id', $id)->delete();
        if($request->input('value'))
        {
            foreach($request->input('value') as $key => $value)
            {
                $production_result_variable = new ProductionResultVariable;
                $production_result_variable->value = $value;
                $production_result_variable->variable_id = $key;
                $production_result_variable->production_result_id = $id;

                $production_result_variable->save();
            }
        }
        return redirect()->action('Home\EndProductController@summary', $id);
    }

    public function create_result($id)
    {
    	$end_product = ProductionResult::find($id);
    	return view ('home.end_product.result', compact('end_product'));
    }

    public function store_result($id, ResultRequest $request)
    {
        // dd($request->input());
        $end_product = ProductionResult::find($id);
        $end_product->status = 2;
        $end_product->save();
    	ProductionResultDetail::where('production_result_id', $id)->delete();
        $production_result = ProductionResult::FindOrFail($id);
        $production_result->finished_at = $request->input('finished_at');
        foreach($request->input('size') as $key => $size)
        {
            $production_result_detail = new ProductionResultDetail;
            $production_result_detail->production_result_id = $id;
            $production_result_detail->size = $request->input('size.'.$key);
            $production_result_detail->block = $request->input('block.'.$key);
            $production_result_detail->uom_id = $request->input('uom_id');
            $production_result_detail->save();
        }

        $production_result->save();
        return redirect()->action('Home\EndProductController@create_addition', $id);
    }

    public function edit_result($id)
    {
        $end_product = ProductionResult::find($id);
        $end_product_result = $end_product->production_result_detail;
        return view ('home.end_product.result', compact('end_product', 'end_product_result'));
    }

    public function update_result($id, ResultRequest $request)
    {
        ProductionResultDetail::where('production_result_id', $id)->delete();
        $production_result = ProductionResult::FindOrFail($id);
        $production_result->finished_at = $request->input('finished_at');
        foreach($request->input('size') as $key => $size)
        {
            $production_result_detail = new ProductionResultDetail;
            $production_result_detail->production_result_id = $id;
            $production_result_detail->size = $request->input('size.'.$key);
            $production_result_detail->block = $request->input('block.'.$key);
            $production_result_detail->uom_id = $request->input('uom_id');
            $production_result_detail->save();
        }

        $production_result->save();
        return redirect()->action('Home\EndProductController@summary', $id);
    }

    public function create_addition($id)
    {
    	$end_product = ProductionResult::find($id);
    	$reject_reason_list = RejectReason::pluck('name', 'id');
        $reject_type_list = RejectType::pluck('name', 'id');
    	return view ('home.end_product.addition', compact('end_product', 'reject_reason_list', 'reject_type_list'));
    }

    public function store_addition($id, Request $request)
    {
        $end_product = ProductionResult::find($id);
        $end_product->status = 3;
        $end_product->save();
    	ProductionResultAddition::where('production_result_id', $id)->delete();
    	$cart = $request->input('size');
        if($cart != null)
        {
            foreach($request->input('size') as $key => $size)
            {
                $addition = new ProductionResultAddition;
                $addition->production_result_id = $id;
                $addition->size = $request->input('size.'.$key);
                $addition->block = $request->input('block.'.$key);
                $addition->reject_type = $request->input('type.'.$key);
                $addition->reject_reason = $request->input('reason.'.$key);
                $addition->save();
            }
        }
        return redirect()->action('Home\EndProductController@summary', $id);
    }

    public function edit_addition($id)
    {
        $end_product = ProductionResult::find($id);
        $reject_reason_list = RejectReason::pluck('name', 'id');
        $reject_type_list = RejectType::pluck('name', 'id');
        $end_product_addition = $end_product->production_result_addition;
        return view ('home.end_product.addition', compact('end_product', 'reject_reason_list', 'reject_type_list', 'end_product_addition'));
    }

    public function update_addition($id, Request $request)
    {
        ProductionResultAddition::where('production_result_id', $id)->delete();
        $cart = $request->input('size');
        if($cart != null)
        {
            foreach($request->input('size') as $key => $size)
            {
                $addition = new ProductionResultAddition;
                $addition->production_result_id = $id;
                $addition->size = $request->input('size.'.$key);
                $addition->block = $request->input('block.'.$key);
                $addition->reject_type = $request->input('type.'.$key);
                $addition->reject_reason = $request->input('reason.'.$key);
                $addition->save();
            }
        }
        return redirect()->action('Home\EndProductController@summary', $id);
    }

    public function summary($id)
    {
    	$end_product = ProductionResult::find($id);
    	return view ('home.end_product.summary', compact('end_product'));
    }

    public function close($id)
    {
        $end_product = ProductionResult::find($id);
        $end_product->status = 4;
        $end_product->save();

        return redirect()->action('Home\EndProductController@summary', $id);
    }
}
