<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Requests\ProductionResultRequest;
use App\Http\Requests\ProductionResultVariableRequest;
use App\Http\Requests\ResultRequest;
use App\Http\Requests\RejectRequest;
use App\Http\Controllers\Controller;
use App\Repository\Inventory\StorageRepository;
use App\Repository\Inventory\DetailStorageRepository;

class DetailStorageController extends Controller
{
    protected $StorageRepository;
    protected $DetailStorageRepository;

    public function __construct(StorageRepository $storage_repository, DetailStorageRepository $detail_storage_repository)
    {
        $this->StorageRepository = $storage_repository;
        $this->DetailStorageRepository = $detail_storage_repository;
    }

    public function index()
    {
        $detail_storages = $this->DetailStorageRepository->index();
        return view ('home.detail_storage.index', compact('detail_storages'));
    }

    public function create()
    {
        $storages = $this->StorageRepository->index();
        return view ('home.detail_storage.form', compact('storages'));
    }

    public function store(Request $request)
    {
        $this->DetailStorageRepository->store($request->input());
        return redirect()->action('Home\StorageController@index');
    }

    public function edit($id)
    {
        $storages = $this->StorageRepository->index();
        $detail_storage = $this->DetailStorageRepository->detail($id);
        return view ('home.detail_storage.form', compact('storages', 'detail_storage'));
    }    

    public function update($id, Request $request)
    {
        $this->DetailStorageRepository->update($id, $request->input());
        return redirect()->action('Home\StorageController@index');
    }

    public function delete($id)
    {
        $this->DetailStorageRepository->delete($id);
        return redirect()->action('Home\StorageController@delete');
    }
}
