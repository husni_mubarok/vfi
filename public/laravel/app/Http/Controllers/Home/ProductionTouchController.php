<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Requests\ProductionResultRequest;
use App\Http\Requests\ProductionResultVariableRequest;
use App\Http\Requests\RejectRequest;
use App\Http\Requests\ResultRequest;
use App\Http\Controllers\Controller;
use App\Repository\ProductionResultRepository;
use App\Repository\FishRepository;
use App\Repository\ProductionResultVariableRepository;
use App\Repository\RejectRepository;
use App\Repository\RejectReasonRepository;
use App\Repository\RejectRejectReasonRepository;
use PDF;
use App\Model\Uom;
use App\Model\RejectType;

class ProductionTouchController extends Controller
{
    protected $ProductionResultRepository;
    protected $FishRepository;
    protected $ProductionResultVariableRepository;
    protected $RejectRepository;
    protected $RejectReasonRepository;
    protected $RejectRejectReasonRepository;

    public function __construct(ProductionResultRepository $production_result_repository, FishRepository $fish_repository, ProductionResultVariableRepository $production_result_variable_repository, RejectRepository $reject_repository, RejectReasonRepository $reject_reason_repository, RejectRejectReasonRepository $reject_reject_reason_repository)
    {
        $this->ProductionResultRepository = $production_result_repository;
        $this->FishRepository = $fish_repository;
        $this->ProductionResultVariableRepository = $production_result_variable_repository;
        $this->RejectRepository = $reject_repository;
        $this->RejectReasonRepository = $reject_reason_repository;
        $this->RejectRejectReasonRepository = $reject_reject_reason_repository;
    }

    public function index()
    {
        $production_results = $this->ProductionResultRepository->get_regular_paginate();
        $today_data = $this->ProductionResultRepository->get_today();
        return view ('home.touch.production', compact('production_results', 'today_data'));
    }

    public function create_production()
    {
        $fishes = $this->FishRepository->get_list();
        $production_results = $this->ProductionResultRepository->get_regular();
        return view ('home.touch.form', compact('fishes', 'production_results'));
    }

    public function store_production(ProductionResultRequest $request)
    {
        $production_result = $this->ProductionResultRepository->store($request->input());
        $this->ProductionResultRepository->update_status($production_result->id, 'On Going');
        return redirect()->action('Home\ProductionTouchController@create_waste', $production_result->id);
    }

    public function edit_production($id)
    {
        $production_results = $this->ProductionResultRepository->get_regular();
        $production_result = $this->ProductionResultRepository->get_one($id);
        return view ('home.touch.form', compact('production_results', 'production_result'));
    }

    public function update_production($id, Request $request)
    {
        $this->ProductionResultRepository->update($id, $request->input());
        return redirect()->action('Home\ProductionTouchController@summary', $id);
    }

    public function create_waste($id)
    {
        $production_results = $this->ProductionResultRepository->get_regular();
        $production_result = $this->ProductionResultRepository->get_one($id);
        return view ('home.touch.variable', compact('production_results', 'production_result'));
    }

    public function store_waste($id, ProductionResultVariableRequest $request)
    {
        $this->ProductionResultVariableRepository->store($id, $request->input('value'));
        return redirect()->action('Home\ProductionTouchController@create_reject', $id);
    }

    public function edit_waste($id)
    {
        $production_results = $this->ProductionResultRepository->get_regular();
        $production_result = $this->ProductionResultRepository->get_one($id);
        $production_result_variables = $production_result->production_result_variable;

        return view ('home.touch.variable', compact('production_results', 'production_result', 'production_result_variables'));
    }

    public function update_waste($id, ProductionResultVariableRequest $request)
    {

    }

    public function create_reject($id)
    {
        $production_results = $this->ProductionResultRepository->get_regular();
        $production_result = $this->ProductionResultRepository->get_one($id);
        $reject_reason_list = $this->RejectReasonRepository->get_list();
        $reject_type_list = RejectType::pluck('name', 'id');
        return view ('home.touch.reject', compact('production_results', 'production_result', 'reject_reason_list', 'reject_type_list'));
    }

    public function store_reject($id, Request $request)
    {
        $this->ProductionResultRepository->store_addition($id, $request->input());
        return redirect()->action('Home\ProductionTouchController@create_result', $id);
    }

    public function edit_reject($id)
    {
        $production_results = $this->ProductionResultRepository->get_regular();
        $production_result = $this->ProductionResultRepository->get_one($id);
        $reject_reason_list = $this->RejectReasonRepository->get_list();
        $reject_type_list = RejectType::pluck('name', 'id');
        $additionals = $production_result->production_result_addition;
        return view ('home.touch.reject', compact('production_results', 'production_result', 'reject_reason_list', 'reject_type_list', 'additionals'));
    }

    public function update_reject($id, Request $request)
    {
        $reject = $this->RejectRepository->update($id, $request->input());
        if(!empty($request->input('reject_reason')))
        {
            $this->RejectRejectReasonRepository->store($id, $request->input('reject_reason'));
        }
        return redirect()->action('Home\ProductionTouchController@index');
    }

    public function create_result($id)
    {
        $production_results = $this->ProductionResultRepository->get_regular();
        $production_result = $this->ProductionResultRepository->get_one($id);
        $uom_list = Uom::pluck('name', 'id');
        return view ('home.touch.result', compact('production_results', 'production_result', 'uom_list'));
    }

    public function store_result($id, ResultRequest $request)
    {
        if(!$request->input('size') || !$request->input('block'))
        {
            return redirect()->action('Home\ProductionTouchController@create_result', $id)->withErrors('Cart must not be empty!');
        } else {
            $production_result = $this->ProductionResultRepository->store_result($id, $request->input('finished_at'), $request->input('uom_id'), $request->input('size'), $request->input('block'));
            return redirect()->action('Home\ProductionTouchController@summary', $id);
        }
    }

    public function edit_result($id)
    {
        $production_results = $this->ProductionResultRepository->get_regular();
        $production_result = $this->ProductionResultRepository->get_one($id);
        $uom_list = Uom::pluck('name', 'id');
        $details = $production_result->production_result_detail;
        return view ('home.touch.result', compact('production_results', 'production_result', 'uom_list', 'details'));
    }

    public function update_result(ResultRequest $request)
    {
        if(!$request->input('size') || !$request->input('block'))
        {
            return redirect()->action('Home\ProductionTouchController@create_result', $id)->withErrors('Cart must not be empty!');
        } else {
            $production_result = $this->ProductionResultRepository->store_result($id, $request->input('finished_at'), $request->input('uom_id'), $request->input('size'), $request->input('block'));
            return redirect()->action('Home\ProductionTouchController@summary', $id);
        }
    }

    public function summary($id)
    {
        $production_results = $this->ProductionResultRepository->get_regular();
        $production_result = $this->ProductionResultRepository->get_one($id);
        return view ('home.touch.summary', compact('production_results', 'production_result'));
    }   

    public function close_production_result($id)
    {
        $this->ProductionResultRepository->update_status($id, 'Done');
        return redirect()->action('Home\ProductionTouchController@summary', $id);
    }

    public function pdf($id)
    {
        $production_result = $this->ProductionResultRepository->get_one($id);
        //Generate the view
        $pdf = \PDF::loadView('editor.production_result.pdf', compact('production_result'));
        //Setup the pdf name & formatting
        $name = "Summary ".$production_result->id.".pdf";
        $pdf->setPaper('a4', 'portrait');
        //Return download link
        return $pdf->download($name);
    }

    public function all_productions()
    {
        $production_results = $this->ProductionResultRepository->get_regular_paginate();
        return view ('home.touch.productions', compact('production_results'));
    }
}
