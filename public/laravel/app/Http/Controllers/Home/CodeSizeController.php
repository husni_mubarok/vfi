<?php

namespace App\Http\Controllers\Home;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\CodeSize;
use App\Model\Grade;
use App\Model\Size;
use App\Model\FreezingType;

class CodeSizeController extends Controller
{
    public function createcodesize()
    {
        $sizes = Size::all();
        $grades = Grade::all();
        // $freezing_types = FreezingType::all();

        $codesizes = DB::table('code_size')
        ->select('code_size.id AS id_code_size', 'code_size.name', 'code_size.block_weight', 'sizes.note AS size_name', 'grade.name AS grade_name')
        ->join('grade', 'grade.id', '=', 'code_size.id_grade')
        ->join('sizes', 'sizes.id', '=', 'code_size.id_size')
        // ->join('freezing_type', 'freezing_type.id', '=', 'code_size.id_freezing_type')
        ->get();

        return view ('home.code_size.index',compact('sizes', 'grades', 'codesizes'));
    }

    public function storecodesize(Request $request)
    {
		$code_size = new CodeSize;
		$code_size->id_grade = $request->input('grade_id');
        $code_size->id_size = $request->input('size_id');
		// $code_size->id_freezing_type = $request->input('freezing_type_id');
		$code_size->name = $request->input('name');
		$code_size->block_weight = $request->input('block_weight');
		$code_size->save();

		return redirect()->action('Home\CodeSizeController@createcodesize');
    }

    public function deletecodesize($id)
    {
    	// CodeSize::Find($id)->delete();
    	$delete = DB::table('code_size')->where('id', $id)->delete();

    	return redirect()->action('Home\CodeSizeController@createcodesize');
    }
}
