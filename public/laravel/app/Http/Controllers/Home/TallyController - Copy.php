<?php

namespace App\Http\Controllers\Home;

use DB;
use Illuminate\Http\Request; 
use App\Http\Requests\TallyRequest;
use App\Http\Controllers\Controller;
use App\Repository\PurchaseRepository;
use App\Repository\ProductRepository;
use App\Repository\SupplierRepository;
use App\Repository\PriceRepository;
use App\Repository\PurchaseDetailRepository;
use App\Model\Purchase;
use App\Model\Size;
use App\Model\Tally;
use App\Model\TallyDetail;
use App\Model\TallyRecToday;
use App\Model\TallyDetailRecToday;

class TallyController extends Controller
{
    protected $PurchaseRepository;
    protected $ProductRepository;
    protected $SupplierRepository;
    protected $PriceRepository;
    protected $PurchaseDetailRepository;

    public function __construct(PurchaseRepository $purchase_repository, ProductRepository $product_repository, SupplierRepository $supplier_repository, PriceRepository $price_repository, PurchaseDetailRepository $purchase_detail_repository)
    {
        $this->PurchaseRepository = $purchase_repository;
        $this->ProductRepository = $product_repository;
        $this->SupplierRepository = $supplier_repository;
        $this->PriceRepository = $price_repository;
        $this->PurchaseDetailRepository = $purchase_detail_repository;
    }

    public function index()
    {
        $project_list = Purchase::groupBy('project')->pluck('project')->toArray();
        array_unshift($project_list, 'Actual Delivery');

        $pending_approval = Purchase::where('status', 'Waiting COO')->latest()->get(); 

        return view ('home.tally.index', compact('pending_approval', 'project_list'));
    }

    //RECEIVING COLLECT
    public function create($id)
    { 
       
         $purchase = DB::table('products')
        ->join('purchases_products', 'purchases_products.product_id', '=', 'products.id')
        ->join('purchases', 'purchases_products.purchase_id', '=', 'purchases.id')  
        ->select('purchases.id', 'purchases.doc_code', 'purchases.trans_code', 'products.name', 'products.id AS product_id')
        ->where('purchases.id', $id) 
        ->first();  

        $tally = Tally::where('purchase_id', $id)->first(); 

        $tally_detail = DB::table('product_types')
        ->join('product_types_sizes', 'product_types_sizes.product_type_id', '=', 'product_types.id')
        ->join('sizes', 'product_types_sizes.size_id', '=', 'sizes.id')
        ->join('products', 'products.product_type_id', '=', 'product_types.id')
        ->leftjoin('purchases_products', 'purchases_products.product_id', '=', 'products.id')
        ->leftjoin('tally', 'purchases_products.product_id', '=', 'tally.purchase_id')
        ->leftjoin('tally_detail', 'tally.id', '=', 'tally_detail.tally_id')
        ->select('product_types.name', 'sizes.note', 'sizes.id', DB::raw('0 as weight'), 'tally.tally_type')
        ->where('purchases_products.purchase_id', $id)
        ->groupBy('product_types.name', 'sizes.note', 'sizes.id', 'tally_detail.weight', 'tally.tally_type')
        ->get();  

        return view ('home.tally.form', compact('tally_detail', 'purchase', 'tally'));
    }

     public function store(TallyRequest $request, $id)
    {
          
        $tally = new Tally;  
        $tally->trans_code = $request->input('trans_code');
        $tally->tally_type = $request->input('tally_type');
        $tally->product_id = $request->input('product_id');
        $tally->purchase_id = $id;
        $tally->save();

        $purchase = Purchase::find($id);  
        $purchase->form_status = 1;
        $purchase->save();

        foreach($request->input('tally') as $detail_key => $detail_value)
        {
            $tally_detail = new TallyDetail;
            $tally_detail->tally_id = $tally->id;
            $tally_detail->size_id = $detail_key;
            $tally_detail->weight = $detail_value['weight'];
            $tally_detail->created_by = \Auth::id();
            $tally_detail->save();
        }
        return redirect()->action('Home\TallyController@detail', $id);
    }

     public function detail($id)
    { 

         $purchase = DB::table('tally')
        ->join('products', 'tally.product_id', '=', 'products.id') 
        ->select('tally.purchase_id AS id', 'tally.trans_code', 'products.name', 'products.id AS product_id', 'tally.tally_type')
        ->where('tally.purchase_id', $id) 
        ->first(); 
 
        $tally_detail = DB::table('tally_detail')
        ->join('sizes', 'tally_detail.size_id', '=', 'sizes.id') 
        ->join('tally', 'tally_detail.tally_id', '=', 'tally.id')
        ->select('tally_detail.size_id AS id', 'sizes.note', DB::raw('0 as weight'))
        ->where('tally.purchase_id', $id) 
        ->groupBy('tally_detail.size_id', 'sizes.note')
        ->get();

        $tally_detail_view = DB::table('tally_detail')
        ->join('sizes', 'tally_detail.size_id', '=', 'sizes.id') 
        ->join('tally', 'tally_detail.tally_id', '=', 'tally.id')
        ->select('tally_detail.size_id AS id', 'sizes.note', DB::raw('SUM(tally_detail.weight) as weight'))
        ->where('tally.purchase_id', $id) 
        ->groupBy('tally_detail.size_id', 'sizes.note')
        ->get();  

        return view ('home.tally.detail', compact('tally_detail', 'tally_detail_view', 'purchase'));
    }

    public function storedetail(TallyRequest $request, $id)
    {
          
        $tally = Tally::where('purchase_id', $id)->first(); 
        $tally->trans_code = $request->input('trans_code');
        $tally->tally_type = $request->input('tally_type');
        $tally->product_id = $request->input('product_id');  
        $tally->save();

        //TallyDetail::where('tally_id', $tally->id)->delete(); 

        foreach($request->input('tally') as $detail_key => $detail_value)
        {
            $tally_detail = new TallyDetail;
            $tally_detail->tally_id = $tally->id;
            $tally_detail->size_id = $detail_key;
            $tally_detail->weight = $detail_value['weight']; 
            $tally_detail->created_by = \Auth::id();
            $tally_detail->save();
        }

        return redirect()->action('Home\TallyController@detail', $id);

    }

    public function close($id)
    {
          
        $purchase = Purchase::find($id);  
        $purchase->form_status = 1;
        $purchase->save();

        //dd($purchase);
        return redirect()->action('Home\TallyController@index');

    }

    public function view($id)
    { 

         $purchase = DB::table('tally')
        ->join('products', 'tally.product_id', '=', 'products.id') 
        ->select('tally.purchase_id AS id', 'tally.trans_code', 'products.name', 'products.id AS product_id', 'tally.tally_type')
        ->where('tally.purchase_id', $id) 
        ->first();  

        $tally_detail_view = DB::table('tally_detail')
        ->join('sizes', 'tally_detail.size_id', '=', 'sizes.id') 
        ->join('tally', 'tally_detail.tally_id', '=', 'tally.id')
        ->select('tally_detail.size_id AS id', 'sizes.note', DB::raw('SUM(tally_detail.weight) as weight'))
        ->where('tally.purchase_id', $id) 
        ->groupBy('tally_detail.size_id', 'sizes.note')
        ->get();  

        return view ('home.tally.view', compact('tally_detail_view', 'purchase'));
    }





    //RECEIVING COLLECT
    public function createrectoday($id)
    { 
       
         $purchase = DB::table('products')
        ->join('purchases_products', 'purchases_products.product_id', '=', 'products.id')
        ->join('purchases', 'purchases_products.purchase_id', '=', 'purchases.id')  
        ->select('purchases.id', 'purchases.doc_code', 'purchases.trans_code', 'products.name', 'products.id AS product_id')
        ->where('purchases.id', $id) 
        ->first();  

        $tally = TallyRecToday::where('purchase_id', $id)->first(); 

        $tally_detail = DB::table('product_types')
        ->join('product_types_sizes', 'product_types_sizes.product_type_id', '=', 'product_types.id')
        ->join('sizes', 'product_types_sizes.size_id', '=', 'sizes.id')
        ->join('products', 'products.product_type_id', '=', 'product_types.id')
        ->leftjoin('purchases_products', 'purchases_products.product_id', '=', 'products.id')
        ->leftjoin('tally_rectoday', 'purchases_products.product_id', '=', 'tally_rectoday.purchase_id')
        ->leftjoin('tally_detail_rectoday', 'tally_rectoday.id', '=', 'tally_detail_rectoday.tally_id')
        ->select('product_types.name', 'sizes.note', 'sizes.id', DB::raw('0 as weight'), 'tally_rectoday.tally_type')
        ->where('purchases_products.purchase_id', $id)
        ->groupBy('product_types.name', 'sizes.note', 'sizes.id', 'tally_detail_rectoday.weight', 'tally_rectoday.tally_type')
        ->get();  

        return view ('home.tally.formrectoday', compact('tally_detail', 'purchase', 'tally'));
    }

     public function storerectoday(TallyRequest $request, $id)
    {
          
        $tally = new TallyRecToday;  
        $tally->trans_code = $request->input('trans_code');
        $tally->tally_type = $request->input('tally_type');
        $tally->product_id = $request->input('product_id');
        $tally->purchase_id = $id;
        $tally->save();

        $purchase = Purchase::find($id);  
        $purchase->form_status = 1;
        $purchase->save();

        foreach($request->input('tally') as $detail_key => $detail_value)
        {
            $tally_detail = new TallyDetailRecToday;
            $tally_detail->tally_id = $tally->id;
            $tally_detail->size_id = $detail_key;
            $tally_detail->weight = $detail_value['weight']; 
            $tally_detail->save();
        }
        return redirect()->action('Home\TallyController@detailrectoday', $id);
    }

     public function detailrectoday($id)
    { 

         $purchase = DB::table('tally_rectoday')
        ->join('products', 'tally_rectoday.product_id', '=', 'products.id') 
        ->select('tally_rectoday.purchase_id AS id', 'tally_rectoday.trans_code', 'products.name', 'products.id AS product_id', 'tally_rectoday.tally_type')
        ->where('tally_rectoday.purchase_id', $id) 
        ->first(); 
 
        $tally_detail = DB::table('tally_detail_rectoday')
        ->join('sizes', 'tally_detail_rectoday.size_id', '=', 'sizes.id') 
        ->join('tally_rectoday', 'tally_detail_rectoday.tally_id', '=', 'tally_rectoday.id')
        ->select('tally_detail_rectoday.id', 'tally_detail_rectoday.size_id', 'sizes.note', 'tally_detail_rectoday.weight')
        ->where('tally_rectoday.purchase_id', $id) 
        ->get(); 

        return view ('home.tally.detailrectoday', compact('tally_detail', 'purchase', 'tally'));
    }

    public function closerectoday(TallyRequest $request, $id)
    {
          
        $tally = TallyRecToday::where('purchase_id', $id)->first(); 
        $tally->trans_code = $request->input('trans_code');
        $tally->tally_type = $request->input('tally_type');
        $tally->product_id = $request->input('product_id'); 
        $tally->status = 1;
        $tally->save();

        TallyDetailRecToday::where('tally_id', $tally->id)->delete(); 

        foreach($request->input('tally') as $detail_key => $detail_value)
        {
            $tally_detail = new TallyDetailRecToday;
            $tally_detail->tally_id = $tally->id;
            $tally_detail->size_id = $detail_key;
            $tally_detail->weight = $detail_value['weight']; 
            $tally_detail->save();
        }

        return redirect()->action('Home\TallyController@index');

    }

}
