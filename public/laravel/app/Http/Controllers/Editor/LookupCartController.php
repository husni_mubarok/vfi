<?php

namespace App\Http\Controllers\Editor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Model\TempStorage;
use Datatables;

class LookupCartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('editor.lookupcart.index');
    }

    public function data(Request $request)
    {   
        if($request->ajax()){
            $sql = 'SELECT
                                                DERIVEDTBL.resultdetailid,
                                                DERIVEDTBL.`name`,
                                                DERIVEDTBL.unit,
                                                DERIVEDTBL.weight,
                                                DERIVEDTBL.id,
                                                DERIVEDTBL.criteria,
                                                DERIVEDTBL.block_weight,
                                                DERIVEDTBL.started_at,
                                                DERIVEDTBL.finished_at,
                                                DERIVEDTBL.production_id,
                                                DERIVEDTBL.size,
                                                DERIVEDTBL.block,
                                                SUM(DERIVEDTBL.prodqty) AS prodqty,
                                                SUM(DERIVEDTBL.exportqty) AS cartqty,
                                                SUM(IFNULL(DERIVEDTBL.prodqty, 0)) - SUM(IFNULL(DERIVEDTBL.exportqty, 0)) AS remainqty
                                        FROM
                                                (
                                                        SELECT
                                                                productions.`name`,
                                                                temp_storage.unit,
                                                                temp_storage.weight,
                                                                temp_storage.`datetime`,
                                                                production_results.id,
                                                                production_results.criteria,
                                                                production_results.block_weight,
                                                                production_results.started_at,
                                                                production_results.finished_at,
                                                                temp_storage.production_id,
                                                                temp_storage.quantity AS prodqty,
                                                                "0" AS exportqty,
                                                                production_result_details.size,
                                                                production_result_details.block,
                                                                production_result_details.id resultdetailid,
                                                                temp_storage.id_export AS getkey
                                                        FROM
                                                                productions
                                                        INNER JOIN production_results ON productions.id = production_results.production_id
                                                        INNER JOIN production_result_details ON production_results.id = production_result_details.production_result_id
                                                        INNER JOIN temp_storage ON production_result_details.id = temp_storage.source_id
                                                        WHERE
                                                                temp_storage.transaction_type IS NULL
                                                        OR temp_storage.transaction_type = "In"
                                                        UNION
                                                                SELECT
                                                                        productions.`name`,
                                                                        temp_storage.unit,
                                                                        temp_storage.weight,
                                                                        temp_storage.`datetime`,
                                                                        production_results.id,
                                                                        production_results.criteria,
                                                                        production_results.block_weight,
                                                                        production_results.started_at,
                                                                        production_results.finished_at,
                                                                        temp_storage.production_id,
                                                                        "0" AS prodqty,
                                                                        temp_storage.quantity AS exportqty,
                                                                        production_result_details.size,
                                                                        production_result_details.block,
                                                                        production_result_details.id resultdetailid,
                                                                        temp_storage.id_export AS getkey
                                                                FROM
                                                                        productions
                                                                INNER JOIN production_results ON productions.id = production_results.production_id
                                                                INNER JOIN production_result_details ON production_results.id = production_result_details.production_result_id
                                                                INNER JOIN temp_storage ON production_result_details.id = temp_storage.source_id
                                                                WHERE
                                                                        temp_storage.transaction_type = "Out"
                                                ) AS DERIVEDTBL
                                        GROUP BY
                                                DERIVEDTBL.resultdetailid,
                                                DERIVEDTBL.`name`,
                                                DERIVEDTBL.unit,
                                                DERIVEDTBL.weight,
                                                DERIVEDTBL.id,
                                                DERIVEDTBL.criteria,
                                                DERIVEDTBL.block_weight,
                                                DERIVEDTBL.started_at,
                                                DERIVEDTBL.finished_at,
                                                DERIVEDTBL.production_id,
                                                DERIVEDTBL.size,
                                                DERIVEDTBL.block
                                        HAVING
                                                SUM(IFNULL(DERIVEDTBL.prodqty, 0)) - SUM(IFNULL(DERIVEDTBL.exportqty, 0)) > 0';
            $lookupcart = DB::table(DB::raw("($sql) as temp_storage"))->get();
            return Datatables::of($lookupcart)
            ->addColumn('action', function ($lookupcart) {
                return '<a href="javascript:void(0)" id="btnstore" onclick="addValue(this, '.$lookupcart->id.')" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-hand-up"></i> Store</a>';
            })
            ->make(true);
        } else {
            exit("No data available");
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
