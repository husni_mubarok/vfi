<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ProductionResult;

class InventoryController extends Controller
{
    public function index()
    {
    	$results = ProductionResult::all();
    	$i = 0;
    	foreach($results as $key => $result)
    	{
			foreach($result->production_result_detail as $detail)
			{
				$products[$i] = 
				[
					'name' => $result->production->name,
					'size' => $detail->size,
					'block' => $detail->block,
					'block_weight' => $result->block_weight,
					'start_date' => $result->started_at,
					'finish_date' => $result->finished_at,
				];
				$i += 1;
			}
    	}
    	return view ('editor.inventory.index', compact('products'));
    }
}