<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\ModulesRequest;
use App\Model\Modules;
use Illuminate\Support\Facades\Auth;

class ModulesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modules = Modules::all();
        return view ('editor.modules.index', compact('modules'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('editor.modules.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ModulesRequest $request)
    {
        $module = new Modules;
        $module->name = $request->input('name');
        $module->description = $request->input('description');
        $module->save();
        return redirect()->action('Editor\ModulesController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $module = Modules::FindOrFail($id);
        return view ('editor.modules.form', compact('module'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ModulesRequest $request, $id)
    {
        $module = Modules::FindOrFail($id);
        $module->name = $request->input('name');
        $module->description = $request->input('description');
        $module->save();
        return redirect()->action('Editor\ModulesController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $module = Modules::FindOrFail($id);
        $module->deleted_by = Auth::user()->id;
        $module->save();
        $module->delete();
        return redirect()->action('Editor\ModulesController@index');
    }
}
