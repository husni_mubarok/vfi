<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Requests\PurchaseRequest;
use App\Http\Controllers\Controller;
use App\Repository\PurchaseRepository;
use App\Repository\ProductRepository;
use App\Repository\ProductTypeRepository;
use App\Repository\SupplierRepository;
use App\Repository\SizeRepository;
use App\Repository\PriceRepository;
use PDF;

class PurchaseController extends Controller
{
    protected $PurchaseRepository;
    protected $ProductRepository;
    protected $ProductTypeRepository;
    protected $SupplierRepository;
    protected $SizeRepository;
    protected $PriceRepository;

    public function __construct(PurchaseRepository $purchase_repository, ProductRepository $product_repository, ProductTypeRepository $product_type_repository, SupplierRepository $supplier_repository, SizeRepository $size_repository, PriceRepository $price_repository)
    {
    	$this->PurchaseRepository = $purchase_repository;
    	$this->ProductRepository = $product_repository;
        $this->ProductTypeRepository = $product_type_repository;
    	$this->SupplierRepository = $supplier_repository;
        $this->SizeRepository = $size_repository;
        $this->PriceRepository = $price_repository;
    }

    public function index()
    {
    	$purchases = $this->PurchaseRepository->get_all();
    	return view ('editor.purchase.index', compact('purchases'));
    }

    public function create()
    {
    	$products = $this->ProductRepository->get_list();
    	$suppliers = $this->SupplierRepository->get_list();
    	return view ('editor.purchase.form', compact('products', 'suppliers'));
    }

    public function store(PurchaseRequest $request)
    {
        $this->PurchaseRepository->store($request->input());
        return redirect()->action('Editor\PurchaseController@index');
    }

    public function edit($id)
    {
    	$purchase = $this->PurchaseRepository->get_one($id);
        $suppliers = $this->SupplierRepository->get_list();
    	return view ('editor.purchase.form', compact('purchase', 'suppliers'));
    }

    public function update($id, PurchaseRequest $request)
    {
        $purchase = $this->PurchaseRepository->update($id, $request->input());
        return redirect()->action('Editor\PurchaseController@summary', $id);
    }

    public function delete($id)
    {
    	$this->PurchaseRepository->delete($id);
    	return redirect()->action('Editor\PurchaseController@index');
    }

    public function reject($id, Request $request)
    {
        $reject = $this->PurchaseRepository->reject($id, $request->input());
        return redirect()->action('Editor\PurchaseController@summary', $id);
    }

    public function approve($id)
    {
        $this->PurchaseRepository->approve($id);
        return redirect()->action('Editor\PurchaseController@summary', $id);
    }

    public function history($id)
    {
        $purchase = $this->PurchaseRepository->get_one($id);
        return view ('editor.purchase.history', compact('purchase'));
    }

    public function summary($id)
    {
        $purchase = $this->PurchaseRepository->get_one($id);
        // $size = $this->SizeRepository->get_size($purchase->size);
        // $price = $this->PriceRepository->get_price($purchase->id, $size->id);
        // $price = $this->PriceRepository->get_price($purchase->id, $size->id, $purchase->size);
        return view ('editor.purchase.summary', compact('purchase'));
    }

    public function pdf($id)
    {
        $purchase = $this->PurchaseRepository->get_one($id);
        return view ('editor.purchase.pdf', compact('purchase'));
        //Generate the view
        $pdf = \PDF::loadView('editor.purchase.pdf', compact('purchase'));
        //Setup the pdf name & formatting
        $name = "Summary ".$purchase->id.".pdf";
        $pdf->setPaper('a4', 'portrait');
        //Return download link
        return $pdf->download($name);
    }

    public function confirm_po_rm($id)
    {
        $purchase = $this->PurchaseRepository->update_status($id, 'Waiting COO');
        return redirect()->action('Editor\PurchaseController@summary', $id);
    }

    public function confirm_actual_delivery($id)
    {
        $purchase = $this->PurchaseRepository->update_status($id, 'Waiting QC');
        return redirect()->action('Editor\PurchaseController@summary', $id);
    }
}
