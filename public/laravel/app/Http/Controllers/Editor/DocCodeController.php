<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Requests\DocCodeRequest;
use App\Http\Controllers\Controller;
use App\Repository\DocCodeRepository;

class DocCodeController extends Controller
{
    protected $DocCodeRepository;

    public function __construct(DocCodeRepository $doc_code_repository)
    {
    	$this->DocCodeRepository = $doc_code_repository;
    }

    public function index()
    {
    	$doc_codes = $this->DocCodeRepository->get_all();
    	return view ('editor.doc_code.index', compact('doc_codes'));
    }

    public function create()
    {
    	return view ('editor.doc_code.form');
    }

    public function store(DocCodeRequest $request)
    {
    	$doc_code = $this->DocCodeRepository->store($request->input());
    	return redirect()->action('Editor\DocCodeController@index');
    }

    public function edit($id)
    {
    	$doc_code = $this->DocCodeRepository->get_one($id);
    	return view ('editor.doc_code.form', compact('doc_codes'));
    }

    public function update($id, DocCodeRequest $request)
    {
    	$doc_code = $this->DocCodeRepository->update($id, $request->input());
    	return redirect()->action('Editor\DocCodeController@index');
    }

    public function reset($id)
    {
        $doc_code = $this->DocCodeRepository->reset_count($id);
        return redirect()->action('Editor\DocCodeController@index');
    }

    public function delete($id)
    {
    	$doc_code = $this->DocCodeRepository->delete($id);
    	return redirect()->action('Editor\DocCodeController@index');
    }
}
