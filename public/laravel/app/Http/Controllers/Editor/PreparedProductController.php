<?php

namespace App\Http\Controllers\Editor;

use DB;
use Illuminate\Http\Request;
use App\Http\Requests\PreparedProductRequest;
use App\Http\Requests\PreparedProductIngredientRequest;
use App\Http\Controllers\Controller;
use App\Repository\ProductionRepository;
use App\Repository\MPrepProductRepository;
use App\Repository\IngredientRepository;
use App\Repository\PreparedProductRepository;
use App\Model\ProductionResultSize;

class PreparedProductController extends Controller
{
    protected $ProductionRepository;
	protected $MPrepProductRepository;
    protected $IngredientRepository;
    protected $PreparedProductRepository;

    public function __construct(ProductionRepository $production_repository, MPrepProductRepository $m_prep_product_repository, IngredientRepository $ingredient_repository, PreparedProductRepository $prepared_product_repository)
    {
        $this->ProductionRepository = $production_repository;
        $this->MPrepProductRepository = $m_prep_product_repository;
        $this->IngredientRepository = $ingredient_repository;
        $this->PreparedProductRepository = $prepared_product_repository;
    }

    public function index()
    {
    	$prepared_products = $this->PreparedProductRepository->get_all();
    	return view ('editor.prepared_product.index', compact('prepared_products'));
    }

    public function create()
    {
    	$prepared_product_list = $this->MPrepProductRepository->get_list();
        $production_list = $this->ProductionRepository->get_list();
    	return view ('editor.prepared_product.form', compact('prepared_product_list', 'production_list'));
    }

    public function store(PreparedProductRequest $request)
    {
        $this->PreparedProductRepository->store($request->input());
        return redirect()->action('Editor\PreparedProductController@index');
    }

    public function summary($id)
    {
        $prepared_product = $this->PreparedProductRepository->get_one($id);
        return view ('editor.prepared_product.summary', compact('prepared_product'));
    }

    public function edit($id)
    {
        $m_prep_products = $this->MPrepProductRepository->get_list();
        $prepared_product = $this->PreparedProductRepository->get_one($id);
        return view ('editor.prepared_product.form', compact('m_prep_products', 'prepared_product'));
    }

    public function update($id, PreparedProductRequest $request)
    {
        $this->PreparedProductRepository->update($id, $request->input());
        return redirect()->action('Editor\PreparedProductController@index');
    }

    public function delete($id)
    {   
        $this->PreparedProductRepository->delete($id);
        return redirect()->action('Editor\PreparedProductController@index');
    }

    public function create_daily($id)
    {
        $prepared_product = $this->PreparedProductRepository->get_one($id);
        return view ('editor.prepared_product.production', compact('prepared_product'));
    }

    public function store_daily($id, Request $request)
    {
        $data_detail = [
            'date' => $request->input('date'), 
            'processed_amount' => $request->input('processed_amount')
            ];
        $prepared_product_detail_id = $this->PreparedProductRepository->store_daily($id, $data_detail);

        $this->PreparedProductRepository->save_ingredient($prepared_product_detail_id, $request->input('ingredient'));

        return redirect()->action('Editor\PreparedProductController@index');
    }

    public function edit_daily($id)
    {
        $prepared_product = $this->PreparedProductRepository->get_one($id);
        return view ('editor.prepared_product.production', compact('prepared_product'));
    }  

    // public function edit_material($id)
    // {
    //     $prepared_product = $this->PreparedProductRepository->get_one($id);
    //     return view ('editor.prepared_product.material', compact('prepared_product'));
    // }

    // public function update_material($id, Request $request)
    // {
    //     $this->PreparedProductRepository->save_material($id, $request->input('material'));
    //     return redirect()->action('Editor\PreparedProductController@index');
    // }

    // public function edit_ingredient($id)
    // {
    //     $prepared_product = $this->PreparedProductRepository->get_one($id);
    //     $stock_items = DB::table('production_results_sizes')
    //                 ->join('production_results', 'production_results.id', '=', 'production_results_sizes.production_result_id')
    //                 ->where('production_results.production_id', '=', $prepared_product->production_id)
    //                 ->where('production_results_sizes.size_id', '=', $prepared_product->size_id)
    //                 ->get();
    //     $consumed_items = DB::table('prepared_products')
    //                     ->where('prepared_products.size_id', '=', $prepared_product->size_id)
    //                     ->where('prepared_products.production_id', '=', $prepared_product->production_id)
    //                     ->get();
    //     return view ('editor.prepared_product.ingredient', compact('prepared_product', 'stock_items', 'consumed_items'));
    // }

    // public function update_ingredient($id, PreparedProductIngredientRequest $request)
    // {
    //     $this->PreparedProductRepository->save_ingredient($id, $request->input('ingredient'));
    //     return redirect()->action('Editor\PreparedProductController@index');
    // }
}
