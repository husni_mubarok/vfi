<?php

namespace App\Http\Controllers\Editor\Inventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repository\Inventory\UomRepository;

class UomController extends Controller
{
	protected $UomRepository;

	public function __construct(UomRepository $uom_repository)
    {
    	$this->UomRepository = $uom_repository;
    }

    public function index()
    {
    	$uoms = $this->UomRepository->index();
    	return view ('editor.uom.index', compact('uoms'));
    }

    public function create()
    {
    	return view ('editor.uom.form');
    }

    public function store(Request $request)
    {
    	$this->UomRepository->store($request->input());
    	return redirect()->action('Editor\Inventory\UomController@index');
    }

    public function edit($id)
    {
    	$uom = $this->UomRepository->detail($id);
    	return view ('editor.uom.form', $uom);
    }

    public function update($id, Request $request)
    {
    	$this->UomRepository->update($id, $request->input());
    	return redirect()->action('Editor\Inventory\UomController@update');
    }

    public function delete($id)
    {
    	$this->UomRepository->delete($id);
    	return redirect()->action('Editor\Inventory\UomController@delete');
    }
}
