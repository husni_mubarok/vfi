<?php

namespace App\Http\Controllers\Editor\Inventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repository\Inventory\ItemRepository;
use App\Repository\Inventory\UomRepository;
use App\Repository\Inventory\DetailUomRepository;

class DetailUomController extends Controller
{
	protected $DetailUomRepository;

	public function __construct(ItemRepository $item_repository, UomRepository $uom_repository, DetailUomRepository $detail_uom_repository)
    {
    	$this->ItemRepository = $item_repository;
    	$this->UomRepository = $uom_repository;
    	$this->DetailUomRepository = $detail_uom_repository;
    }

    public function index()
    {
    	$detail_uoms = $this->DetailUomRepository->index();
    	return view ('editor.detail_uom.index', compact('detail_uoms'));
    }

    public function create()
    {
    	$item_list = $this->ItemRepository->get_list();
    	$uom_list = $this->UomRepository->get_list();
    	$detail_uom_list = $this->DetailUomRepository->get_list();
    	return view ('editor.detail_uom.form', compact('item_list', 'uom_list', 'detail_uom_list'));
    }

    public function store(Request $request)
    {
    	$this->DetailUomRepository->store($request->input());
    	return redirect()->action('Editor\Inventory\DetailUomController@index');
    }

    public function edit($id)
    {
    	$item_list = $this->ItemRepository->get_list();
    	$uom_list = $this->UomRepository->get_list();
    	$detail_uom_list = $this->DetailUomRepository->get_list();
    	$detail_uom = $this->DetailUomRepository->detail($id);
    	return view ('editor.detail_uom.form', compact('item_list', 'uom_list', 'detail_uom_list', 'detail_uom'));
    }

    public function update($id, Request $request)
    {
    	$this->DetailUomRepository->update($id, $request->input());
    	return redirect()->action('Editor\Inventory\DetailUomController@update');
    }

    public function delete($id)
    {
    	$this->DetailUomRepository->delete($id);
    	return redirect()->action('Editor\Inventory\DetailUomController@delete');
    }
}
