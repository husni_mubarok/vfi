<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\SystemOption;

class SystemOptionController extends Controller
{
    public function index()
    {
    	$system_options = SystemOption::all();
    	return view ('editor.system_option.index', compact('system_options'));
    }

    public function create()
    {
    	return view ('editor.system_option.form');
    }

    public function store(Request $request)
    {
    	$system_option = new SystemOption;
    	$system_option->name = $request->input('name');
    	$system_option->description = $request->input('description');
    	$system_option->value = $request->input('value');
    	$system_option->save();

    	return redirect()->action('Editor\SystemOptionController@index');
    }

    public function edit($id)
    {
    	$system_option = SystemOption::Find($id);
    	return view ('editor.system_option.form', compact('system_option'));
    }

    public function update($id, Request $request)
    {
    	$system_option = SystemOption::Find($id);
    	$system_option->name = $request->input('name');
    	$system_option->description = $request->input('description');
    	$system_option->value = $request->input('value');
    	$system_option->save();

    	return redirect()->action('Editor\SystemOptionController@index');
    }

    public function delete($id)
    {
    	SystemOption::Find($id)->delete();
    	return redirect()->action('Editor\SystemOptionController@index');
    }
}
