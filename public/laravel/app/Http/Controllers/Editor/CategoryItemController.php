<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Illuminate\Http\Request;
use App\Http\Requests\CategoryItemRequest;
use App\Http\Controllers\Controller;
use App\Model\CategoryItem;

class CategoryItemController extends Controller
{
    public function index()
    {
    	$category_items = CategoryItem::all();
    	return view ('editor.category_item.index', compact('category_items'));
    }

    public function create()
    {
    	$category_item_list = CategoryItem::pluck('name', 'id');
        return view ('editor.category_item.form', compact('category_item_list'));
    }

    public function store(CategoryItemRequest $request)
    {
    	$category_item = new CategoryItem;
      $category_item->parent = $request->input('parent');
    	$category_item->name = $request->input('name');
    	$category_item->description = $request->input('description');
      $category_item->created_by = \Auth::id();
    	$category_item->save();

    	return redirect()->action('Editor\CategoryItemController@index');
    }

    public function edit($id)
    {
    	$category_item = CategoryItem::Find($id);
        $category_item_list = CategoryItem::pluck('name', 'id');
    	return view ('editor.category_item.form', compact('category_item', 'category_item_list'));
    }

    public function update($id, CategoryItemRequest $request)
    {
    	$category_item = CategoryItem::Find($id);
        $category_item->parent = $request->input('parent');
		$category_item->name = $request->input('name');
    	$category_item->description = $request->input('description');
        $category_item->updated_by = Auth::id();
    	$category_item->save();

    	return redirect()->action('Editor\CategoryItemController@index');
    }

    public function delete($id)
    {
    	CategoryItem::Find($id)->delete();
    	return redirect()->action('Editor\CategoryItemController@index');
    }
}
