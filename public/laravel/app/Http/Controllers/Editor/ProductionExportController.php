<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\ProductionExportRequest;
use App\Http\Requests\ProductionExportdetRequest;
use App\Http\Controllers\Controller;
use App\Model\ProductionExport;
use App\Model\ProductionExportDet;
use App\Model\ProductionResult;
use App\Model\TempStorage;
use App\Model\SourceStorage;

    class ProductionExportController extends Controller
    {
        public function index()
        {
        	$production_exports = ProductionExport::all();

        	return view ('editor.production_export.index', compact('production_exports'));
        }

        public function create()
        {

            //$tempstorage = TempStorage::pluck('id', 'id')->toArray();         
            //get stock
        	$tempstorage = DB::select( DB::raw("SELECT
                                                        DERIVEDTBL.resultdetailid,
                                                        DERIVEDTBL.`name`,
                                                        DERIVEDTBL.unit,
                                                        DERIVEDTBL.weight,
                                                        DERIVEDTBL.id,
                                                        DERIVEDTBL.criteria,
                                                        DERIVEDTBL.block_weight,
                                                        DERIVEDTBL.started_at,
                                                        DERIVEDTBL.finished_at,
                                                        DERIVEDTBL.production_id,
                                                        DERIVEDTBL.size,
                                                        DERIVEDTBL.block,
                                                        SUM(DERIVEDTBL.prodqty) AS prodqty,
                                                        SUM(DERIVEDTBL.exportqty) AS exportqty,
                                                        SUM(IFNULL(DERIVEDTBL.prodqty, 0)) - SUM(IFNULL(DERIVEDTBL.exportqty, 0)) AS remainqty
                                                FROM
                                                        (
                                                                SELECT
                                                                        productions.`name`,
                                                                        temp_storage.unit,
                                                                        temp_storage.weight,
                                                                        temp_storage.`datetime`,
                                                                        production_results.id,
                                                                        production_results.criteria,
                                                                        production_results.block_weight,
                                                                        production_results.started_at,
                                                                        production_results.finished_at,
                                                                        temp_storage.production_id,
                                                                        temp_storage.quantity AS prodqty,
                                                                        '0' AS exportqty,
                                                                        production_result_details.size,
                                                                        production_result_details.block,
                                                                        production_result_details.id resultdetailid,
                                                                        temp_storage.id_export AS getkey
                                                                FROM
                                                                        productions
                                                                INNER JOIN production_results ON productions.id = production_results.production_id
                                                                INNER JOIN production_result_details ON production_results.id = production_result_details.production_result_id
                                                                INNER JOIN temp_storage ON production_result_details.id = temp_storage.source_id
                                                                WHERE
                                                                        temp_storage.transaction_type IS NULL
                                                                OR temp_storage.transaction_type = 'In'
                                                                UNION
                                                                        SELECT
                                                                                productions.`name`,
                                                                                temp_storage.unit,
                                                                                temp_storage.weight,
                                                                                temp_storage.`datetime`,
                                                                                production_results.id,
                                                                                production_results.criteria,
                                                                                production_results.block_weight,
                                                                                production_results.started_at,
                                                                                production_results.finished_at,
                                                                                temp_storage.production_id,
                                                                                '0' AS prodqty,
                                                                                temp_storage.quantity AS exportqty,
                                                                                production_result_details.size,
                                                                                production_result_details.block,
                                                                                production_result_details.id resultdetailid,
                                                                                temp_storage.id_export AS getkey
                                                                        FROM
                                                                                productions
                                                                        INNER JOIN production_results ON productions.id = production_results.production_id
                                                                        INNER JOIN production_result_details ON production_results.id = production_result_details.production_result_id
                                                                        INNER JOIN temp_storage ON production_result_details.id = temp_storage.source_id
                                                                        WHERE
                                                                                temp_storage.transaction_type = 'Out'
                                                        ) AS DERIVEDTBL
                                                GROUP BY
                                                        DERIVEDTBL.resultdetailid,
                                                        DERIVEDTBL.`name`,
                                                        DERIVEDTBL.unit,
                                                        DERIVEDTBL.weight,
                                                        DERIVEDTBL.id,
                                                        DERIVEDTBL.criteria,
                                                        DERIVEDTBL.block_weight,
                                                        DERIVEDTBL.started_at,
                                                        DERIVEDTBL.finished_at,
                                                        DERIVEDTBL.production_id,
                                                        DERIVEDTBL.size,
                                                        DERIVEDTBL.block
                                                HAVING
                                                        SUM(IFNULL(DERIVEDTBL.prodqty, 0)) - SUM(IFNULL(DERIVEDTBL.exportqty, 0)) > 0") );

            
            $production_exportdets = ProductionExportDet::where('id_export', '')  
                ->get();
            return view ('editor.production_export.form', compact('tempstorage'), compact('production_exportdets'));

        }

        public function store(ProductionExportRequest $request)
        {
            
            
            //get last transaction id
            $maxlastid = DB::table('export')->max('id')+1;
            
            $productionexport = new ProductionExport;
        	$productionexport->doc_code = 'DOC/EXP/'.$maxlastid;
        	$productionexport->date = $request->input('date');
        	$productionexport->save();
            
            //get last header id
            $lastheaderid = $productionexport->id;
     
            //insert export detail
            $productionresult = new ProductionExportDet;
            $productionresult->quantity = $request->input('quantity');
            $productionresult->name = $request->input('name');
            $productionresult->source_id = $request->input('source_id');
            $productionresult->size = $request->input('size');
            $productionresult->block = $request->input('block');
            $productionresult->criteria = $request->input('criteria');
            $productionresult->id_export = $lastheaderid;
            $productionresult->save();

            //$lastid= DB::table('export')->insertGetId('id');
            $lastdetailid = $productionresult->id;
               
            //insert temp storage
            $tempstorage = new TempStorage;   	
        	$tempstorage->datetime = $request->input('date');
        	$tempstorage->quantity = $request->input('quantity');
            $tempstorage->source_id = $request->input('source_id');
        	$tempstorage->production_id = 1;
            $tempstorage->source_type = 3;
            $tempstorage->unit = 'PCS';
            $tempstorage->weight = 0;
            $tempstorage->status = 0;
            $tempstorage->status_storage_id = 1;
            $tempstorage->id_export = $lastdetailid;
            $tempstorage->transaction_type = 'Out';
        	$tempstorage->save();
           
        	//return redirect()->action('Editor\ProductionExportController@index');
            return redirect('editor/production_export/'.$productionexport->id.'/edit');
        }
        
        public function edit($id)
        {

            //Calculate stock
            $tempstorage = DB::select( DB::raw("SELECT
                                                    DERIVEDTBL.resultdetailid,
                                                    DERIVEDTBL.`name`,
                                                    DERIVEDTBL.unit,
                                                    DERIVEDTBL.weight,
                                                    DERIVEDTBL.id,
                                                    DERIVEDTBL.criteria,
                                                    DERIVEDTBL.block_weight,
                                                    DERIVEDTBL.started_at,
                                                    DERIVEDTBL.finished_at,
                                                    DERIVEDTBL.production_id,
                                                    DERIVEDTBL.size,
                                                    DERIVEDTBL.block,
                                                    SUM(DERIVEDTBL.prodqty) AS prodqty,
                                                    SUM(DERIVEDTBL.exportqty) AS exportqty,
                                                    SUM(IFNULL(DERIVEDTBL.prodqty, 0)) - SUM(IFNULL(DERIVEDTBL.exportqty, 0)) AS remainqty
                                            FROM
                                                    (
                                                            SELECT
                                                                    productions.`name`,
                                                                    temp_storage.unit,
                                                                    temp_storage.weight,
                                                                    temp_storage.`datetime`,
                                                                    production_results.id,
                                                                    production_results.criteria,
                                                                    production_results.block_weight,
                                                                    production_results.started_at,
                                                                    production_results.finished_at,
                                                                    temp_storage.production_id,
                                                                    temp_storage.quantity AS prodqty,
                                                                    '0' AS exportqty,
                                                                    production_result_details.size,
                                                                    production_result_details.block,
                                                                    production_result_details.id resultdetailid,
                                                                    temp_storage.id_export AS getkey
                                                            FROM
                                                                    productions
                                                            INNER JOIN production_results ON productions.id = production_results.production_id
                                                            INNER JOIN production_result_details ON production_results.id = production_result_details.production_result_id
                                                            INNER JOIN temp_storage ON production_result_details.id = temp_storage.source_id
                                                            WHERE
                                                                    temp_storage.transaction_type IS NULL
                                                            OR temp_storage.transaction_type = 'In'
                                                            UNION
                                                                    SELECT
                                                                            productions.`name`,
                                                                            temp_storage.unit,
                                                                            temp_storage.weight,
                                                                            temp_storage.`datetime`,
                                                                            production_results.id,
                                                                            production_results.criteria,
                                                                            production_results.block_weight,
                                                                            production_results.started_at,
                                                                            production_results.finished_at,
                                                                            temp_storage.production_id,
                                                                            '0' AS prodqty,
                                                                            temp_storage.quantity AS exportqty,
                                                                            production_result_details.size,
                                                                            production_result_details.block,
                                                                            production_result_details.id resultdetailid,
                                                                            temp_storage.id_export AS getkey
                                                                    FROM
                                                                            productions
                                                                    INNER JOIN production_results ON productions.id = production_results.production_id
                                                                    INNER JOIN production_result_details ON production_results.id = production_result_details.production_result_id
                                                                    INNER JOIN temp_storage ON production_result_details.id = temp_storage.source_id
                                                                    WHERE
                                                                            temp_storage.transaction_type = 'Out'
                                                    ) AS DERIVEDTBL
                                            GROUP BY
                                                    DERIVEDTBL.resultdetailid,
                                                    DERIVEDTBL.`name`,
                                                    DERIVEDTBL.unit,
                                                    DERIVEDTBL.weight,
                                                    DERIVEDTBL.id,
                                                    DERIVEDTBL.criteria,
                                                    DERIVEDTBL.block_weight,
                                                    DERIVEDTBL.started_at,
                                                    DERIVEDTBL.finished_at,
                                                    DERIVEDTBL.production_id,
                                                    DERIVEDTBL.size,
                                                    DERIVEDTBL.block
                                            HAVING
                                                    SUM(IFNULL(DERIVEDTBL.prodqty, 0)) - SUM(IFNULL(DERIVEDTBL.exportqty, 0)) > 0") );
      
            $production_exportdets = ProductionExportDet::where('id_export', $id)  
                ->get();
            $production_export = ProductionExport::Find($id);
        	return view ('editor.production_export.form', compact('tempstorage', 'production_export', 'production_exportdets'));
        }


        public function update($id, ProductionExportRequest $request)
        {
        	
            //add or edit form condition
            // $iddetail = $request->input('iddetail');
            // $quantityedit = $request->input('quantityedit');

            //if ($iddetail == '') {

            //Update production export
            $productionexport = ProductionExport::Find($id);
        	$productionexport->date = $request->input('date');
        	$productionexport->save();

            //Insert export detail
            $productionresult = new ProductionExportDet;
        	$productionresult->quantity = $request->input('quantity');
            $productionresult->name = $request->input('name');
            $productionresult->source_id = $request->input('source_id');
            $productionresult->size = $request->input('size');
            $productionresult->block = $request->input('block');
            $productionresult->criteria = $request->input('criteria');
        	$productionresult->id_export = $id;
            $productionresult->save();
            
            $lastdetailid = $productionresult->id;

            //update temp storage
            $tempstorage = new TempStorage;   	
        	$tempstorage->datetime = $request->input('date');
        	$tempstorage->quantity = $request->input('quantity');
            $tempstorage->source_id = $request->input('source_id');
        	$tempstorage->production_id = 1;
            $tempstorage->source_type = 3;
            $tempstorage->unit = 'PCS';
            $tempstorage->weight = 0;
            $tempstorage->status = 0;
            $tempstorage->status_storage_id = 1;
            $tempstorage->id_export = $lastdetailid;
            $tempstorage->transaction_type = 'Out';
        	$tempstorage->save();

            //}else{

            //!bug
            //update detail
            //$productionresult = ProductionExportDet::Find($iddetail, $quantityedit);
           // $productionresult->quantity = $quantityedit;
            //$productionresult->name = $requestdet->input('nameedit');
            //$productionresult->size = $requestdet->input('sizeedit');
            //$productionresult->block = $requestdet->input('blockedit');
            //$productionresult->criteria = $requestdet->input('criteriaedit');
            //$productionresult->id_export = $id;
            //$productionresult->save();

            //update temp storage
            //$tempstorage = TempStorage::Where('id_export',$iddetail)->first();
            //$tempstorage->datetime = $requestdet->input('date');
            //$tempstorage->quantity = $requestdet->input('quantityedit');
            //$tempstorage->save();

            //}   
        
        	//return redirect()->action('Editor\ProductionExportController@index');
            return redirect('editor/production_export/'.$productionexport->id.'/edit');
        }

        public function updatedetail($id, ProductionExportdetRequest $requestdet)
        {
            
            // dd($requestdet->input());
            $iddetail = $requestdet->input('iddetail');
            $quantityedit = $requestdet->input('quantityedit');

            //update detail
            $productionresult = ProductionExportDet::Find($iddetail);
            $productionresult->quantity = $requestdet->input('quantityedit');
            $productionresult->name = $requestdet->input('nameedit');
            $productionresult->size = $requestdet->input('sizeedit');
            $productionresult->block = $requestdet->input('blockedit');
            $productionresult->criteria = $requestdet->input('criteriaedit');
            $productionresult->save();

            //update temp storage
            $tempstorage = TempStorage::Where('id_export',$iddetail)->first();
            $tempstorage->quantity = $requestdet->input('quantityedit');
            $tempstorage->save();

            return redirect('editor/production_export/'.$productionresult->id_export.'/edit');
        }

       //change status to confirm
       public function storeconfirm($id)
        {
        	
            $productionexport = ProductionExport::Find($id);
            $productionexport->status = 1;
        	$productionexport->save();
            
        	return redirect()->action('Editor\ProductionExportController@index');
        }
        
       //delete header
       public function delete($id)
        {
        	ProductionExport::Find($id)->delete();
            ProductionExportDet::Where('id_export',$id)
                    ->first()
                    ->delete();
            TempStorage::Where('id_export',$id)
                    ->first()
                    ->delete();

            return redirect()->back();
        }

        //delete detail
        public function deletedet($id)
        {
            ProductionExportDet::Find($id)->delete();
            TempStorage::Where('id_export',$id)
                    ->first()
                    ->delete();
            return redirect()->back();
        }
    }