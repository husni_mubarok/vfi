<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Requests\PriceRequest;
use App\Http\Controllers\Controller;
use App\Repository\PurchaseRepository;
use App\Repository\PriceRepository;

class PriceController extends Controller
{
    protected $PurchaseRepository;
    protected $PriceRepository;

    public function __construct(PurchaseRepository $purchase_repository, PriceRepository $price_repository)
    {
        $this->PurchaseRepository = $purchase_repository;
    	$this->PriceRepository = $price_repository;
    }

    public function create($id)
    {
        $purchase = $this->PurchaseRepository->get_one($id);
        $previous_purchase = $this->PurchaseRepository->get_previous($id, $purchase->purchase_product->product_id);
        if($previous_purchase)
        {
            $previous_price = $this->PriceRepository->get_previous($previous_purchase->purchase_id);
            return view ('editor.price.form', compact('purchase', 'previous_price'));
        }
        return view ('editor.price.form', compact('purchase'));
    }

    public function store($id, PriceRequest $request)
    {
        $this->PriceRepository->store($id, $request->input('value'));
        return redirect()->action('Editor\PurchaseController@summary', $id);
    }

    public function edit($id)
    {
    	$purchase = $this->PurchaseRepository->get_one($id);
        $prices = $purchase->price;
    	return view ('editor.price.form', compact('purchase', 'prices'));
    }

    public function update($id, PriceRequest $request)
    {
        $this->PriceRepository->store($id, $request->input('value'));
        return redirect()->action('Editor\PurchaseController@summary', $id);
    }
}
