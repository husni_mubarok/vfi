<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\PrivilegeRequest;
use App\Model\Privilege;
use Illuminate\Support\Facades\Auth;

class PrivilegeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $privileges = Privilege::all();
        return view ('editor.privilege.index', compact('privileges'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('editor.privilege.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PrivilegeRequest $request)
    {
        $privilege = new Privilege;
        $privilege->name = $request->input('name');
        $privilege->description = $request->input('description');
        $privilege->save();
        return redirect()->action('Editor\PrivilegeController@index');    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $privilege = Privilege::FindOrFail($id);
        return view ('editor.privilege.form', compact('privilege'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PrivilegeRequest $request, $id)
    {
        $privilege = Privilege::FindOrFail($id);
        $privilege->name = $request->input('name');
        $privilege->description = $request->input('description');
        $privilege->save();
        return redirect()->action('Editor\PrivilegeController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $privilege = Privilege::FindOrFail($id);
        $privilege->deleted_by = Auth::user()->id;
        $privilege->save();
        $privilege->delete();
        return redirect()->action('Editor\PrivilegeController@index');
    }
}
