<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Requests\SupplierRequest;
use App\Http\Controllers\Controller;
use App\Repository\SupplierRepository;
use App\Repository\ProductRepository;

class SupplierController extends Controller
{
    protected $SupplierRepository;
	protected $ProductRepository;

	public function __construct(SupplierRepository $supplier_repository, ProductRepository $product_repository)
	{
        $this->SupplierRepository = $supplier_repository;
		$this->ProductRepository = $product_repository;
	}

    public function index()
    {
    	$suppliers = $this->SupplierRepository->get_all();
    	return view ('editor.supplier.index', compact('suppliers'));
    }

    public function create()
    {
    	return view ('editor.supplier.form');
    }

    public function store(SupplierRequest $request)
    {
    	$supplier = $this->SupplierRepository->store($request->input());
    	return redirect()->action('Editor\SupplierController@index');
    }

    public function edit($id)
    {
    	$supplier = $this->SupplierRepository->get_one($id);
    	return view ('editor.supplier.form', compact('supplier'));
    }

    public function update($id, SupplierRequest $request)
    {
    	$supplier = $this->SupplierRepository->update($id, $request->input());
    	return redirect()->action('Editor\SupplierController@index');
    }

    public function delete($id)
    {
    	$this->SupplierRepository->delete($id);
    	return redirect()->action('Editor\SupplierController@index');
    }

    public function edit_product($id)
    {
        $supplier = $this->SupplierRepository->get_one($id);
        $products = $this->ProductRepository->get_all();
        return view ('editor.supplier.product', compact('supplier', 'products'));
    }

    public function update_product($id, Request $request)
    {
        $this->SupplierRepository->save_product($id, $request->input());
        return redirect()->action('Editor\SupplierController@index');
    }
}
