<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Requests\UomRequest;
use App\Http\Controllers\Controller;
use App\Model\UomCategory;
use App\Model\Uom;

class UomController extends Controller
{
    public function index()
    {
    	$uoms = Uom::all();
    	return view ('editor.uom.index', compact('uoms'));
    }

    public function create()
    {
    	$uom_category_list = UomCategory::pluck('name', 'id');
    	$uom_data_list = Uom::pluck('name', 'id')->toArray();
    	// $uom_list = ['0' => '-'];
    	// array_push($uom_data_list, $uom_list);
    	$uom_list = Uom::pluck('name', 'id');
    	return view ('editor.uom.form', compact('uom_category_list', 'uom_list'));
    }

    public function store(UomRequest $request)
    {
    	$uom = new Uom;
    	$uom->uom_category_id = $request->input('uom_category_id');
    	$uom->parent_id = $request->input('parent_id');
    	$uom->name = $request->input('name');
    	$uom->value = $request->input('value');
    	$uom->save();

    	return redirect()->action('Editor\UomController@index');
    }

    public function edit($id)
    {
    	$uom_category_list = UomCategory::pluck('name', 'id');
    	$uom_list = Uom::pluck('name', 'id');
    	$uom = Uom::Find($id);
    	return view ('editor.uom.form', compact('uom_category_list', 'uom_list', 'uom'));
    }

    public function update($id, UomRequest $request)
    {
    	$uom = Uom::Find($id);
    	$uom->uom_category_id = $request->input('uom_category_id');
    	$uom->parent_id = $request->input('parent_id');
    	$uom->name = $request->input('name');
    	$uom->value = $request->input('value');
    	$uom->save();

    	return redirect()->action('Editor\UomController@index');
    }

    public function delete($id)
    {
    	Uom::Find($id)->delete();
    	return redirect()->action('Editor\UomController@index');
    }
}
