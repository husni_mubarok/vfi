<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Illuminate\Http\Request;
use App\Http\Requests\ItemRequest;
use App\Http\Controllers\Controller;
use App\Model\Item;
use App\Model\MasterItem;

class ItemController extends Controller
{
    public function index()
    {
    	$items = Item::all();
    	return view ('editor.item.index', compact('items'));
    }

    public function create()
    {
    	$master_item_list = MasterItem::pluck('name', 'id');

        return view ('editor.item.form', compact('item', 'master_item_list'));
    }

    public function store(ItemRequest $request)
    {
    	$item = new Item;
    	$item->name = $request->input('name');
        $item->id_master_item = $request->input('id_master_item');
    	$item->description = $request->input('description');
        $item->created_by = Auth::id();
    	$item->save();

    	return redirect()->action('Editor\ItemController@index');
    }

    public function edit($id)
    {

        $master_item_list = MasterItem::pluck('name', 'id');

    	$item = Item::Find($id);
    	return view ('editor.item.form', compact('item', 'master_item_list'));
    }

    public function update($id, ItemRequest $request)
    {
    	$item = Item::Find($id);
		$item->name = $request->input('name');
        $item->id_master_item = $request->input('id_master_item');
    	$item->description = $request->input('description');
        $item->updated_by = Auth::id();
    	$item->save();

    	return redirect()->action('Editor\ItemController@index');
    }

    public function delete($id)
    {
    	Item::Find($id)->delete();
    	return redirect()->action('Editor\ItemController@index');
    }
}
