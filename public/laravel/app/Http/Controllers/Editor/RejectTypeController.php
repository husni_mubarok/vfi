<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\RejectType;

class RejectTypeController extends Controller
{
    public function index()
    {
    	$reject_types = RejectType::all();
    	return view ('editor.reject_type.index', compact('reject_types'));
    }

    public function create()
    {
    	return view ('editor.reject_type.form');
    }

    public function store(Request $request)
    {
    	$reject_type = new RejectType;
    	$reject_type->name = $request->input('name');
    	$reject_type->description = $request->input('description');
    	$reject_type->save();

    	return redirect()->action('Editor\RejectTypeController@index');
    }

    public function edit($id)
    {
    	$reject_type = RejectType::Find($id);
    	return view ('edtior.reject_type.form', compact('reject_type'));
    }

    public function update($id, Request $request)
    {
    	$reject_type = RejectType::Find($id);
    	$reject_type->name = $request->input('name');
    	$reject_type->description = $request->input('description');
    	$reject_type->save();

    	return redirect()->action('Editor\RejectTypeController@index');
    }

    public function delete($id)
    {
    	RejectType::Find($id)->delete();
    	return redirect()->action('Editor\RejectTypeController@index');
    }
}
