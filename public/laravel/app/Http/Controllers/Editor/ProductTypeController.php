<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Requests\ProductTypeRequest;
use App\Http\Controllers\Controller;
use App\Repository\ProductTypeRepository;
use App\Repository\SizeRepository;

class ProductTypeController extends Controller
{
    protected $ProductTypeRepository;
    protected $SizeRepository;

    public function __construct(ProductTypeRepository $product_type_repository, SizeRepository $size_repository)
    {
        $this->ProductTypeRepository = $product_type_repository;
        $this->SizeRepository = $size_repository;
    }

    public function index()
    {
        $product_types = $this->ProductTypeRepository->get_all();
    	return view ('editor.product_type.index', compact('product_types'));
    }

    public function create()
    {
    	return view ('editor.product_type.form');
    }

    public function store(ProductTypeRequest $request)
    {
        $product_type = $this->ProductTypeRepository->store($request->input());
        return redirect()->action('Editor\ProductTypeController@index');
    }

    public function detail($id)
    {
        $product_type = $this->ProductTypeRepository->get_one($id);
        return view ('editor.product_type.detail', compact('product_type'));
    }

    public function edit($id)
    {
        $product_type = $this->ProductTypeRepository->get_one($id);
    	return view ('editor.product_type.form', compact('product_type'));
    }

    public function update($id, ProductTypeRequest $request)
    {
        $product_type = $this->ProductTypeRepository->update($id, $request->input());
        return redirect()->action('Editor\ProductTypeController@index');
    }

    public function delete($id)
    {
        $this->ProductTypeRepository->delete($id);
        return redirect()->action('Editor\ProductTypeController@index');
    }

    public function edit_size($id)
    {
        $product_type = $this->ProductTypeRepository->get_one($id);
        $sizes = $this->SizeRepository->get_all();
        return view ('editor.product_type.size', compact('product_type', 'sizes'));
    }

    public function update_size($id, Request $request)
    {
        $this->ProductTypeRepository->save_size($id, $request->input());
        return redirect()->action('Editor\ProductTypeController@index');
    }
}
