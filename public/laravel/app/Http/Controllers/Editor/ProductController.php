<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Requests\ProductTypeRequest;
use App\Http\Controllers\Controller;
use App\Repository\ProductTypeRepository;
use App\Repository\ProductRepository;

class ProductController extends Controller
{
    protected $ProductTypeRepository;
    protected $ProductRepository;

    public function __construct(ProductTypeRepository $product_type_repository, ProductRepository $product_repository)
    {
    	$this->ProductTypeRepository = $product_type_repository;
        $this->ProductRepository = $product_repository;
    }

    public function index()
    {
        $products = $this->ProductRepository->get_all();
        return view ('editor.product.index', compact('products'));
    }

    public function create(Request $request)
    {
        $product_types = $this->ProductTypeRepository->get_list();
        $product_type_id = 0;
        if($request->input('product_type_id'))
        {
            $product_type_id = $request->input('product_type_id');
        }
        return view ('editor.product.form', compact('product_types', 'product_type_id'));
    }

    public function store(ProductTypeRequest $request)
    {
    	$product = $this->ProductRepository->store($request->input());
    	return redirect()->action('Editor\ProductTypeController@detail', $product->product_type_id);
    }

    public function edit($id)
    {
        $product_types = $this->ProductTypeRepository->get_list();
        $product = $this->ProductRepository->get_one($id);
    	return view ('editor.product.form', compact('product_types', 'product'));
    }

    public function update($id, ProductTypeRequest $request)
    {
    	$product = $this->ProductRepository->update($id, $request->input());
    	return redirect()->action('Editor\ProductTypeController@detail', $product->product_type->id);
    }

    public function delete($id)
    {
    	$this->ProductRepository->delete($id);
    	return redirect()->action('Editor\ProductController@index');
    }
}
