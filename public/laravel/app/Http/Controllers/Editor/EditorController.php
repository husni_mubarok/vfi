<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Requests\PurchaseRequest;
use App\Http\Requests\PriceRequest;
use App\Http\Requests\PurchaseDetailRequest;
use App\Repository\PurchaseRepository;
use App\Repository\ProductTypeRepository;
use App\Repository\ProductRepository;
use App\Repository\SupplierRepository;
use App\Repository\SizeRepository;
use App\Repository\PriceRepository;
use App\Repository\PurchaseDetailRepository;
use App\Repository\FishRepository;
use App\Repository\ProductionRepository;
use App\Repository\VariableRepository;
use App\Repository\RejectReasonRepository;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class EditorController extends Controller
{
    protected $PurchaseRepository;
    protected $ProductRepository;
    protected $ProductTypeRepository;
    protected $SupplierRepository;
    protected $SizeRepository;
    protected $PriceRepository;
    protected $PurchaseDetailRepository;
    protected $FishRepository;
    protected $ProductionRepository;
    protected $VariableRepository;
    protected $RejectReasonRepository;

    public function __construct(PurchaseRepository $purchase_repository, ProductTypeRepository $product_type_repository, ProductRepository $product_repository, SupplierRepository $supplier_repository, SizeRepository $size_repository, PriceRepository $price_repository, PurchaseDetailRepository $purchase_detail_repository, FishRepository $fish_repository, ProductionRepository $production_repository, VariableRepository $variable_repository, RejectReasonRepository $reject_reason_repository)
    {
        $this->PurchaseRepository = $purchase_repository;
        $this->ProductTypeRepository = $product_type_repository;
        $this->ProductRepository = $product_repository;
        $this->SupplierRepository = $supplier_repository;
        $this->SizeRepository = $size_repository;
        $this->PriceRepository = $price_repository;
        $this->PurchaseDetailRepository = $purchase_detail_repository;
        $this->FishRepository = $fish_repository;
        $this->ProductionRepository = $production_repository;
        $this->VariableRepository = $variable_repository;
        $this->RejectReasonRepository = $reject_reason_repository;
    }

    public function index()
    {
    	$purchases = $this->PurchaseRepository->get_all();
        $product_types = $this->ProductTypeRepository->get_all();
    	$products = $this->ProductRepository->get_all();
    	$suppliers = $this->SupplierRepository->get_all();
        $sizes = $this->SizeRepository->get_all();

        $fishes = $this->FishRepository->get_all();
        $productions = $this->ProductionRepository->get_all();
        $variables = $this->VariableRepository->get_all();
        $reject_reasons = $this->RejectReasonRepository->get_all();

    	return view ('editor.index', compact('purchases','product_types', 'products', 'suppliers', 'sizes', 'fishes', 'productions', 'variables', 'reject_reasons'));
    }
}
