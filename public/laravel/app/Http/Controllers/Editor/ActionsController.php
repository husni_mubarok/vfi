<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\ActionsRequest;
use App\Model\Actions;
use Illuminate\Support\Facades\Auth;

class ActionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $actions = Actions::all();
        return view ('editor.actions.index', compact('actions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('editor.actions.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ActionsRequest $request)
    {
        $action = new Actions;
        $action->name = $request->input('name');
        $action->description = $request->input('description');
        $action->save();
        return redirect()->action('Editor\ActionsController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $action = Actions::FindOrFail($id);
        return view ('editor.actions.form', compact('action'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ActionsRequest $request, $id)
    {
        $action = Actions::FindOrFail($id);
        $action->name = $request->input('name');
        $action->description = $request->input('description');
        $action->save();
        return redirect()->action('Editor\ActionsController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $action = Actions::FindOrFail($id);
        $action->deleted_by = Auth::user()->id;
        $action->save();
        $action->delete();
        return redirect()->action('Editor\ActionsController@index');
    }
}
