<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Requests\FishRequest;
use App\Http\Controllers\Controller;
use App\Repository\FishRepository;
use App\Repository\ProductRepository;

class FishController extends Controller
{
    protected $FishRepository;
    protected $ProductRepository;

    public function __construct(FishRepository $fish_repository, ProductRepository $product_repository)
    {
        $this->FishRepository = $fish_repository;
        $this->ProductRepository = $product_repository;
    }

    public function index()
    {
    	$fishes = $this->FishRepository->get_all();
    	return view ('editor.fish.index', compact('fishes'));
    }

    public function create()
    {
    	return view ('editor.fish.form');
    }

    public function store(FishRequest $request)
    {
    	$fish = $this->FishRepository->store($request->input());
    	return redirect()->action('Editor\FishController@index');
    }

    public function edit($id)
    {
    	$fish = $this->FishRepository->get_one($id);
    	return view ('editor.fish.form', compact('fish'));
    }

    public function update($id, FishRequest $request)
    {
    	$fish = $this->FishRepository->update($id, $request->input());
    	return redirect()->action('Editor\FishController@index');
    }

    public function edit_product($id)
    {
        $fish = $this->FishRepository->get_one($id);
        $products = $this->ProductRepository->get_all();
        return view ('editor.fish.product', compact('fish', 'products'));
    }

    public function update_product($id, Request $request)
    {
        $this->FishRepository->save_product($id, $request->input('product'));
        return redirect()->action('Editor\FishController@index');
    }

    public function delete($id)
    {
    	$this->FishRepository->delete($id);
    	return redirect()->action('Editor\FishController@index');
    }
}
