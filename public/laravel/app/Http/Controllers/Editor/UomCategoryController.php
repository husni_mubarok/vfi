<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Requests\UomCategoryRequest;
use App\Http\Controllers\Controller;
use App\Model\UomCategory;
use App\Model\Uom;

class UomCategoryController extends Controller
{
    public function index()
    {
    	$uom_categories = UomCategory::all();
    	return view ('editor.uom_category.index', compact('uom_categories'));
    }

    public function create()
    {
    	return view ('editor.uom_category.form');
    }

    public function store(UomCategoryRequest $request)
    {
    	$uom_category = new UomCategory;
    	$uom_category->name = $request->input('name');
    	$uom_category->description = $request->input('description');
    	$uom_category->save();

    	return redirect()->action('Editor\UomCategoryController@index');
    }

    public function edit($id)
    {
    	$uom_category = UomCategory::Find($id);
    	return view ('editor.uom_category.form', compact('uom_category'));
    }

    public function update($id, UomCategoryRequest $request)
    {
    	$uom_category = UomCategory::Find($id);
		$uom_category->name = $request->input('name');
    	$uom_category->description = $request->input('description');
    	$uom_category->save();

    	return redirect()->action('Editor\UomCategoryController@index');
    }

    public function delete($id)
    {
    	UomCategory::Find($id)->delete();
    	return redirect()->action('Editor\UomCategoryController@index');
    }
}
