<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Requests\PurchaseDetailRequest;
use App\Http\Controllers\Controller;
use App\Repository\PurchaseRepository;
use App\Repository\PurchaseDetailRepository;

class PurchaseDetailController extends Controller
{
    protected $PurchaseRepository;
    protected $PurchaseDetailRepository;

    public function __construct(PurchaseRepository $purchase_repository, PurchaseDetailRepository $purchase_detail_repository)
    {
        $this->PurchaseRepository = $purchase_repository;
    	$this->PurchaseDetailRepository = $purchase_detail_repository;
    }

    public function create($id)
    {
    	$purchase = $this->PurchaseRepository->get_one($id);
    	return view ('editor.purchase_detail.form', compact('purchase'));
    }

    public function store($id, PurchaseDetailRequest $request)
    {
        $purchase_detail = $this->PurchaseDetailRepository->store($id, $request->input('size'), $request->input('price'), $request->input('amount'));
        return redirect()->action('Editor\PurchaseController@summary', $id);
    }

    public function edit($id)
    {
    	$purchase = $this->PurchaseRepository->get_one($id);
        $purchase_details = $purchase->purchase_detail;

        foreach($purchase->purchase_detail as $key => $purchase_detail)
        {
            $purchase_details[$key] = collect([
                'size' => $purchase_detail->size,
                'price' => $purchase_detail->price,
                'amount' => $purchase_detail->amount,
                'subtotal' => $purchase_detail->amount * $purchase_detail->price
                ]);
        }
        
    	return view ('editor.purchase_detail.form', compact('purchase', 'purchase_details'));
    }

    public function update($id, PurchaseDetailRequest $request)
    {
        dd($request);
    }
}
