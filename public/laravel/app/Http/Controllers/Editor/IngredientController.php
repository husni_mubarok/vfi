<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Requests\IngredientRequest;
use App\Http\Controllers\Controller;
use App\Repository\IngredientRepository;

class IngredientController extends Controller
{
	protected $IngredientRepository;

    public function __construct(IngredientRepository $ingredient_repository)
    {
        $this->IngredientRepository = $ingredient_repository;
    }

    public function index()
    {
    	$ingredients = $this->IngredientRepository->get_all();
    	return view ('editor.ingredient.index', compact('ingredients'));
    }

    public function create()
    {
    	return view ('editor.ingredient.form');
    }

    public function store(IngredientRequest $request)
    {
    	$ingredient = $this->IngredientRepository->store($request->input());
    	return redirect()->action('Editor\IngredientController@index');
    }

    public function edit($id)
    {
    	$ingredient = $this->IngredientRepository->get_one($id);
    	return view ('editor.ingredient.form', compact('ingredient'));
    }

    public function update($id, IngredientRequest $request)
    {
    	$ingredient = $this->IngredientRepository->update($id, $request->input());
    	return redirect()->action('Editor\IngredientController@index');
    }

    public function delete($id)
    {
    	$this->IngredientRepository->delete($id);
    	return redirect()->action('Editor\IngredientController@index');
    }
}
