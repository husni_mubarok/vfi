<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\RolesRequest;
use App\Model\User;
use App\Model\Modules;
use App\Model\Roles;
use App\Model\Privilege;
use App\Model\Actions;

class RolesController extends Controller
{
    public function __construct()
    {
        // $this->middleware('role');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $users = User::all();
      foreach ($users as $user) {
        $id = $user->id;
        $userHasModules = Roles::where('id_users', $id)->groupBy('id_users')->count();
        if($userHasModules > 0) {
          $username = $user->username;
          $privilege = $user->privilege->name;
          $roles = Roles::where('id_users', $id)->get();
          $module = NULL;
          $roleArray = [];
          foreach ($roles as $role) {
            if ($module != $role->id_modules) {
              $keyArray = $role->modules->name;
            }

            $roleArray[$keyArray][] = $role->actions->name;
            $module = $role->id_modules;
          }
          $roleArrays[] = array($id, $username, $privilege, json_encode($roleArray));
        }
      }
      // print_r($roleArrays);
      return view('editor.roles.index')
              ->with('roleArrays', $roleArrays);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        $username_array[0] = "Select a user";

        foreach ($users as $user) {
            $id = $user->id;
            $userHasModules = Roles::where('id_users', $id)->groupBy('id_users')->count();
            if($userHasModules < 1) {
              $username = $user->username;
              $username_array[$id] = $username;
            }
        }
        $modules = Modules::all()->pluck('name', 'id');
        $actions = Actions::all()->pluck('name', 'id');

        return view('editor.roles.form')
                ->with('username_array', $username_array)
                ->with('modules', $modules)
                ->with('actions', $actions);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RolesRequest $request)
    {
        $user = $request->input('user');
        $module = $request->input('module');
        $countModule = count($module);
        foreach ($module as $key => $value) {
            foreach ($module[$key] as $mk) {
                $dataRow['id_users'] = $user;
                $dataRow['id_modules'] = $key;
                $dataRow['id_actions'] = $mk;
                $data[] = $dataRow;
            }
        }
        if(Roles::insert($data))
        {
            $response = array(
                'status' => 'Success',
                'msg' => 'Roles created successfully',
            );
        }

        return redirect()->action('Editor\RolesController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Roles::where('id_users', $id)->get();
        $md = "";
        foreach ($roles as $r){
          if ($md != $r->id_modules) {
            $moduleUse[] = $r->id_modules;
          }
          $md = $r->id_modules;
          $actionUse[$md][] = $r->id_actions;
        }

        $modules = Modules::all()->pluck('name', 'id');
        $actions = Actions::all()->pluck('name', 'id');

        $user = User::find($id);
        $id = $user->id;
        $username = $user->username;

        return view('editor.roles.form')
            ->with('roles', $roles)
            ->with('modules', $modules)
            ->with('actions', $actions)
            ->with('moduleUse', $moduleUse)
            ->with('actionUse', $actionUse)
            ->with('username', $username)
            ->with('id', $id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RolesRequest $request)
    {
      $user = $request->input('user');
      $module = $request->input('module');

      foreach ($module as $key => $value) {
          foreach ($module[$key] as $mk) {
              $dataRow['id_users'] = $user;
              $dataRow['id_modules'] = $key;
              $dataRow['id_actions'] = $mk;
              $data[] = $dataRow;
          }
      }

      if(Roles::where('id_users', $user)->delete())
      {
        if(Roles::insert($data))
        {
            $response = array(
                'status' => 'Success',
                'msg' => 'Roles created successfully',
            );
        }
      }
      return redirect()->action('Editor\RolesController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
      $role = Roles::FindOrFail($id);
      $role->deleted_by = Auth::user()->id;
      $role->save();
      $role->delete();
      return redirect()->action('Editor\RolesController@index');
    }
}
