<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Requests\VariableRequest;
use App\Http\Controllers\Controller;
use App\Repository\VariableRepository;

class VariableController extends Controller
{
    protected $VariableRepository;

    public function __construct(VariableRepository $variable_repository)
    {
        $this->VariableRepository = $variable_repository;
    }

    public function index()
    {
    	$variables = $this->VariableRepository->get_all();
    	return view ('editor.variable.index', compact('variables'));
    }

    public function create()
    {
    	return view ('editor.variable.form');
    }

    public function store(VariableRequest $request)
    {
    	$variable = $this->VariableRepository->store($request->input());
    	return redirect()->action('Editor\VariableController@index');
    }

    public function edit($id)
    {
    	$variable = $this->VariableRepository->get_one($id);
    	return view ('editor.variable.form', compact('variable'));
    }

    public function update($id, VariableRequest $request)
    {
    	$variable = $this->VariableRepository->update($id, $request->input());
    	return redirect()->action('Editor\VariableController@index');
    }

    public function delete($id)
    {
    	$this->VariableRepository->delete($id);
    	return redirect()->action('Editor\VariableController@index');
    }
}
