<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Inventory\Storage;
use App\Http\Requests\StorageDevRequest;
use App\Repository\Inventory\StorageRepository;

class StorageDevController extends Controller
{
    protected $StorageRepository;

    public function __construct(StorageRepository $storage_dev_repository)
    {
        $this->StorageRepository = $storage_dev_repository;
    }

    public function index()
    {
        $storage_dev = Storage::all();
        return view ('editor.storage_dev.index', compact('storage_dev'));
    }

    public function create()
    {
        return view ('editor.storage_dev.form');
    }

    public function store(StorageDevRequest $request)
    {
        // dd($request->input());
        $storage_dev = $this->StorageRepository->store($request->input());
        return redirect()->action('Editor\StorageDevController@index');
    }

    public function edit($id)
    {
        $storage_dev = Storage::FindOrFail($id);
        // dd($storage_dev);
        return view ('editor.storage_dev.form', compact('storage_dev'));
    }

    public function update($id, StorageDevRequest $request)
    {
        $storage_dev = $this->StorageRepository->update($id, $request->input());
        return redirect()->action('Editor\StorageDevController@index');
    }

    public function delete($id)
    {
        $this->StorageRepository->delete($id);
        return redirect()->action('Editor\StorageDevController@index');
    }
}
