<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StorageDetailDevRequest;
use App\Repository\Inventory\DetailStorageRepository;
use App\Model\Inventory\DetailStorage;
use App\Model\Inventory\Storage;

class StorageDetailDevController extends Controller
{
    protected $DetailStorageRepository;

    public function __construct(DetailStorageRepository $storage_detail_dev_repository)
    {
        $this->DetailStorageRepository = $storage_detail_dev_repository;
    }

    public function index()
    {
        $storage_detail_dev = DetailStorage::all();

        // dd($parent_detail_name);
        return view ('editor.storage_detail_dev.index', compact('storage_detail_dev'));
    }

    public function create()
    {
        $select_storage_detail[0] = '-Choose-';
        $detail_storage = Storage::all();
        foreach($detail_storage as $storage_detail)
        {
            $select_storage_detail[$storage_detail->id] = $storage_detail->name;
        }

        $select_parent_detail[0] = '-Choose-';
        $detail_parent = DetailStorage::all();
        foreach($detail_parent as $parent_detail)
        {
            $select_parent_detail[$parent_detail->id] = $parent_detail->name;
        }

        // dd($select_parent_detail);

        return view ('editor.storage_detail_dev.form', compact('select_storage_detail'))->with('parent_detail',$select_parent_detail);
    }

    public function store(StorageDetailDevRequest $request)
    {
        // dd($request->input());
        $storage_detail_dev = $this->DetailStorageRepository->store($request->input());
        return redirect()->action('Editor\StorageDetailDevController@index');
    }

    public function edit($id)
    {
        $select_storage_detail[0] = '-Choose-';
        $detail_storage = Storage::all();
        foreach($detail_storage as $storage_detail)
        {
            $select_storage_detail[$storage_detail->id] = $storage_detail->name;
        }

        $select_parent_detail[0] = '-Choose-';
        $detail_parent = DetailStorage::all();
        foreach($detail_parent as $parent_detail)
        {
            $select_parent_detail[$parent_detail->id] = $parent_detail->name;
        }

        $storage_detail_dev = DetailStorage::FindOrFail($id);
        return view ('editor.storage_detail_dev.form', compact('storage_detail_dev'))->with('select_storage_detail',$select_storage_detail)->with('parent_detail',$select_parent_detail);
    }

    public function update($id, StorageDetailDevRequest $request)
    {
        $storage_detail_dev = $this->DetailStorageRepository->update($id, $request->input());
        return redirect()->action('Editor\StorageDetailDevController@index');
    }

    public function delete($id)
    {
        $this->DetailStorageRepository->delete($id);
        return redirect()->action('Editor\StorageDetailDevController@index');
    }
}
