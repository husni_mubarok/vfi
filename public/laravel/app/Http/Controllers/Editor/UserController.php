<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\ProfileRequest;
use App\Http\Controllers\Controller;
use App\Repository\UserRepository;
use App\Repository\RoleRepository;
use App\Repository\UserRoleRepository;

class UserController extends Controller
{
    //
    protected $UserRepository;

    public function __construct(UserRepository $user_repository, RoleRepository $role_repository, UserRoleRepository $user_role_repository)
    {
    	$this->UserRepository = $user_repository;
    	$this->RoleRepository = $role_repository;
    	$this->UserRoleRepository = $user_role_repository;
    }

    public function index()
    {
    	$users = $this->UserRepository->get_all();
    	return view ('editor.user.index', compact('users'));
    }

    public function create()
    {
    	$roles = $this->RoleRepository->get_list();
    	return view ('editor.user.form', compact('roles'));
    }

    public function store(RegisterRequest $request)
    {
        $user = $this->UserRepository->store($request->input());
        // $user_role = $this->UserRoleRepository->store($user->id, $request->input('role_id'));

        if($request->image)
        {
            $this->UserRepository->update_image($user->id, $request->image);
        }

        return redirect()->action('Editor\UserController@index');
    }

    public function show($id)
    {
    	$user = $this->UserRepository->get_one($id);
    	return view ('editor.user.detail', compact('user'));
    }

    public function edit($id)
    {
        $user = $this->UserRepository->get_one($id);
        return view ('editor.user.form', compact('user'));
    }

    public function delete_image($id)
    {
        $this->UserRepository->delete_image($id);
        return redirect()->action('Editor\UserController@show', $id);
    }

    public function update($id, ProfileRequest $request)
    {
        $this->UserRepository->update($id, $request->input());

        if($request->image)
        {
            $this->UserRepository->update_image($id, $request->image);
        }

        return redirect()->action('Editor\UserController@show', $id);  
    }

    public function edit_password($id)
    {
        $user = $this->UserRepository->get_one($id);
        return view ('editor.user.password', compact('user'));
    }

    public function update_password($id, Request $request)
    {
        $data = array(
            'password_new' => $request->input('password_new'),
            'password_new_confirmation' => $request->input('password_new_confirmation'),
            );

        $rules = [
            'password_new' => 'required|confirmed', 
            'password_new_confirmation' => 'required', 
            ];

        $validator = Validator::make($data, $rules);
        if($validator->fails())
        {
            return redirect()->action('Editor\UserController@create')->withInput()->withErrors($validator);
        } else {
            $this->UserRepository->change_password($id, $request->input('password_new'));

            return redirect()->action('Editor\UserController@index');
        }
    }

    public function delete($id)
    {
        $this->UserRepository->delete($id);
        return redirect()->action('Editor\UserController@index');
    }
}
