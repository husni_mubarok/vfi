<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
// use App\Http\Requests\PrivilegeRequest;
use App\User;
use App\Privilege;
use Illuminate\Support\Facades\Auth;

class UsersPrivilegeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $users_privilege = User::all();
        $users_privilege = User::with('privilege')->where('id_privilege', '<>', 2)->where('id_privilege', '<>', NULL)->get();
        return view ('editor.users_privilege.index', compact('users_privilege'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $privilege = Privilege::all()->pluck('name', 'id');
        // $emaillist = User::all()->where('id_privilege', '<>', NULL)->pluck('email', 'id');
        $emaillist = User::all()->where('id_privilege', 2)->pluck('email', 'id');
        return view ('editor.users_privilege.form')
                    ->with('emaillist', $emaillist)
                    ->with('privilege', $privilege);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $email = $request->input('email');
        $id_privilege = $request->input('id_privilege');

        $users_privilege = User::where('id', $email)->first();
        // dd($users_privilege);
        $users_privilege->id_privilege = $request->input('id_privilege');
        $users_privilege->save();
        return redirect()->action('UsersPrivilegeController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users_privilege = User::FindOrFail($id);
        $privilege = Privilege::all()->pluck('name', 'id');
        return view ('editor.users_privilege.form', compact('users_privilege'))
                ->with('privilege', $privilege);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $users_privilege = User::FindOrFail($id);
        $users_privilege->id_privilege = $request->input('id_privilege');
        $users_privilege->save();
        return redirect()->action('UsersPrivilegeController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $privilege = Privilege::FindOrFail($id);
        $privilege->deleted_by = Auth::user()->id;
        $privilege->save();
        $privilege->delete();
        return redirect()->action('PrivilegeController@index');
    }
}
