<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Requests\SizeRequest;
use App\Http\Controllers\Controller;
use App\Repository\SizeRepository;

class SizeController extends Controller
{
    protected $SizeRepository;

    public function __construct(SizeRepository $size_repository)
    {
    	$this->SizeRepository = $size_repository;
    }

    public function index()
    {
    	$sizes = $this->SizeRepository->get_all();
    	return view ('editor.size.index', compact('sizes'));
    }

    public function create()
    {
    	return view ('editor.size.form');
    }

    public function store(SizeRequest $request)
    {
    	$size = $this->SizeRepository->store($request->input());
    	return redirect()->action('Editor\SizeController@index');
    }

    public function edit($id)
    {
    	$size = $this->SizeRepository->get_one($id);
    	return view ('editor.size.form', compact('size'));
    }

    public function update($id, SizeRequest $request)
    {
    	$size = $this->SizeRepository->update($id, $request->input());
    	return redirect()->action('Editor\SizeController@index');
    }

    public function delete($id)
    {
    	$this->SizeRepository->delete($id);
    	return redirect()->action('Editor\SizeController@index');
    }
}
