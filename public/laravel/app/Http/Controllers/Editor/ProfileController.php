<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Validator;
use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Repository\UserRepository;

class ProfileController extends Controller
{
    //
    protected $UserRepository;

    public function __construct(UserRepository $user_repository)
    {
    	$this->UserRepository = $user_repository;
    }

    public function show()
    {
    	return view ('editor.profile.detail');
    }

    public function delete_image()
    {
        $this->UserRepository->delete_image(Auth::user()->id);
        return redirect()->action('Editor\ProfileController@show');
    }

    public function edit()
    {
    	return view ('editor.profile.edit');
    }

    public function update(Request $request)
    {
    	$data = array(
            'email' => $request->input('email'), 
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'), 
            );

        $rules = [
            'email' => 'required|email',
            'first_name' => 'required',
            'last_name' => '',
        ];

        if($request->image)
        {
            $data['image'] = $request->image;
            $rules['image'] = 'image|between:0, 5000';
        }

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {   
            return redirect()->action('Editor\ProfileController@edit')->withInput()->withErrors($validator);
        } else {
            $this->UserRepository->update(Auth::user()->id, $request->input());

            if($request->image)
            {
                $this->UserRepository->update_image(Auth::user()->id, $request->image);
            }
 
            return redirect()->action('Editor\ProfileController@show');
        }
    }

    public function edit_password()
    {
        return view ('editor.profile.password');
    }

    public function update_password(Request $request)
    {
        $data = array(
            'password_current' => $request->input('password_current'), 
            'password_new' => $request->input('password_new'),
            'password_new_confirmation' => $request->input('password_new_confirmation'), 
            );

        $rules = [
            'password_current' => 'required',
            'password_new' => 'required|confirmed',
            'password_new_confirmation' => 'required',
        ];

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {   
            return redirect()->action('Editor\ProfileController@edit_password')->withInput()->withErrors(['New password confirmation failed!']);
        } else {
            
            $user = $this->UserRepository->get_one(Auth::user()->id);
            
            if(Hash::check($request->input('password_current'), $user->password))
            {
                $this->UserRepository->change_password(Auth::user()->id, $request->input('password_new'));

                return redirect()->action('Editor\ProfileController@show');
            } else {
                return redirect()->action('Editor\ProfileController@edit_password')->withInput()->withErrors(['Current password mismatch!']);
            }
        }
    }
}
