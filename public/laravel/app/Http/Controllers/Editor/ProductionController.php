<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Requests\ProductionRequest;
use App\Http\Controllers\Controller;
use App\Repository\ProductionRepository;
use App\Repository\FishRepository;
use App\Repository\VariableRepository;
use App\Repository\SizeRepository;

class ProductionController extends Controller
{
    protected $ProductionRepository;
    protected $FishRepository;
    protected $VariableRepository;
    protected $SizeRepository;

    public function __construct(ProductionRepository $production_repository, FishRepository $fish_repository, VariableRepository $variable_repository, SizeRepository $size_repository)
    {
        $this->ProductionRepository = $production_repository;
        $this->FishRepository = $fish_repository;
        $this->VariableRepository = $variable_repository;
        $this->SizeRepository = $size_repository;
    }

    public function index()
    {
    	$productions = $this->ProductionRepository->get_all();
    	return view ('editor.production.index', compact('productions'));
    }

    public function create()
    {
    	$fishes = $this->FishRepository->get_list();
    	return view ('editor.production.form', compact('fishes'));
    }

    public function store(ProductionRequest $request)
    {
    	$production = $this->ProductionRepository->store($request->input());
    	return redirect()->action('Editor\ProductionController@edit_variable', $production->id);
    }

    public function edit($id)
    {
    	$fishes = $this->FishRepository->get_list();
    	$production = $this->ProductionRepository->get_one($id);
    	return view ('editor.production.form', compact('fishes', 'production'));
    }

    public function update($id, ProductionRequest $request)
    {
    	$production = $this->ProductionRepository->update($id, $request->input());
    	return redirect()->action('Editor\ProductionController@index');
    }

    public function delete($id)
    {
    	$this->ProductionRepository->delete($id);
    	return redirect()->action('Editor\ProductionController@index');
    }

    public function edit_variable($id)
    {
    	$production = $this->ProductionRepository->get_one($id);
    	$variables = $this->VariableRepository->get_all();
    	return view ('editor.production.variable', compact('production', 'variables'));
    }

    public function update_variable($id, Request $request)
    {
    	$this->ProductionRepository->save_variable($id, $request->input('variable'));
    	return redirect()->action('Editor\ProductionController@index');
    }

    public function edit_size($id)
    {
        $production = $this->ProductionRepository->get_one($id);
        $sizes = $this->SizeRepository->get_all();
        return view ('editor.production.size', compact('production', 'sizes'));
    }

    public function update_size($id, Request $request)
    {
        $this->ProductionRepository->save_size($id, $request->input('size'));
        return redirect()->action('Editor\ProductionController@index');
    }
}
