<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Model\ProductionExportDet;

class ProductionExportDetController extends Controller
{
    public function index()
    {
    	$production_exports = ProductionExport::all();
    	return view ('editor.production_export.index', compact('production_exports'));
    }
}