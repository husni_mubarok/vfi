<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Requests\ProductionResultRequest;
use App\Http\Requests\ProductionResultVariableRequest;
use App\Http\Requests\RejectRequest;
use App\Http\Requests\ResultRequest;
use App\Http\Controllers\Controller;
use App\Repository\PurchaseRepository;
use App\Repository\ProductionResultRepository;
use App\Repository\FishRepository;
use App\Repository\ProductionResultVariableRepository;
use App\Repository\RejectRepository;
use App\Repository\RejectReasonRepository;
use App\Repository\RejectRejectReasonRepository;
use App\Model\RejectType;
use App\Model\Uom;
use App\Model\SystemOption;
use PDF;

class ProductionResultController extends Controller
{
    protected $PurchaseRepository;
	protected $ProductionResultRepository;
	protected $FishRepository;
	protected $ProductionResultVariableRepository;
	protected $RejectRepository;
	protected $RejectReasonRepository;
	protected $RejectRejectReasonRepository;

    public function __construct(PurchaseRepository $purchase_repository, ProductionResultRepository $production_result_repository, FishRepository $fish_repository, ProductionResultVariableRepository $production_result_variable_repository, RejectRepository $reject_repository, RejectReasonRepository $reject_reason_repository, RejectRejectReasonRepository $reject_reject_reason_repository)
    {
        $this->PurchaseRepository = $purchase_repository;
        $this->ProductionResultRepository = $production_result_repository;
        $this->FishRepository = $fish_repository;
        $this->ProductionResultVariableRepository = $production_result_variable_repository;
        $this->RejectRepository = $reject_repository;
        $this->RejectReasonRepository = $reject_reason_repository;
        $this->RejectRejectReasonRepository = $reject_reject_reason_repository;
    }

    public function index()
    {
    	$production_results = $this->ProductionResultRepository->get_regular();
    	return view ('editor.production_result.index', compact('production_results'));
    }

    public function create()
    {
        // $purchases = $this->PurchaseRepository->get_list();
        // return view ('editor.production_result.form', compact('purchases'));
        $fishes = $this->FishRepository->get_list();
    	return view ('editor.production_result.form', compact('fishes'));
    }

    public function store(ProductionResultRequest $request)
    {
    	$production_result = $this->ProductionResultRepository->store($request->input());
    	$this->ProductionResultRepository->update_status($production_result->id, 'On Going');
    	return redirect()->action('Editor\ProductionResultController@index');
    }

    public function edit($id)
    {
        $production_result = $this->ProductionResultRepository->get_one($id);
        return view ('editor.production_result.form', compact('production_result'));
    }

    public function update($id, Request $request)
    {
        $this->ProductionResultRepository->update($id, $request->input());
        return redirect()->action('Editor\ProductionResultController@summary', $id);
    }

    public function create_waste($id)
    {
    	$production_result = $this->ProductionResultRepository->get_one($id);
    	return view ('editor.production_result.variable', compact('production_result'));
    }

    public function store_waste($id, ProductionResultVariableRequest $request)
    {
    	$this->ProductionResultVariableRepository->store($id, $request->input('value'));
    	return redirect()->action('Editor\ProductionResultController@index');
    }

    public function edit_waste($id)
    {
    	$production_result = $this->ProductionResultRepository->get_one($id);
    	$production_result_variables = $production_result->production_result_variable;

    	return view ('editor.production_result.variable', compact('production_result', 'production_result_variables'));
    }

    public function update_waste($id, ProductionResultVariableRequest $request)
    {

    }

    public function create_reject($id)
    {
    	$production_result = $this->ProductionResultRepository->get_one($id);
    	$reject_reason_list = $this->RejectReasonRepository->get_list();
        $reject_type_list = RejectType::pluck('name', 'id');
    	return view ('editor.production_result.reject', compact('production_result', 'reject_reason_list', 'reject_type_list'));
    }

    public function store_reject($id, Request $request)
    {
    	$this->ProductionResultRepository->store_addition($id, $request->input());
    	return redirect()->action('Editor\ProductionResultController@summary', $id);
    }

    public function edit_reject($id)
    {
        $production_result = $this->ProductionResultRepository->get_one($id);
        $reject_reason_list = $this->RejectReasonRepository->get_list();
        $reject_type_list = RejectType::pluck('name', 'id');
    	$additionals = $production_result->production_result_addition;
        return view ('editor.production_result.reject', compact('production_result', 'reject_reason_list', 'reject_type_list', 'additionals'));
    }

    public function update_reject($id, Request $request)
    {
        $reject = $this->RejectRepository->update($id, $request->input());
        if(!empty($request->input('reject_reason')))
        {
            $this->RejectRejectReasonRepository->store($id, $request->input('reject_reason'));
        }
        return redirect()->action('Editor\ProductionResultController@index');
    }

    public function create_result($id)
    {
    	$production_result = $this->ProductionResultRepository->get_one($id);
        $uom_list = Uom::pluck('name', 'id');
    	return view ('editor.production_result.result', compact('production_result', 'uom_list'));
    }

    public function store_result($id, ResultRequest $request)
    {
        if(!$request->input('size') || !$request->input('block'))
        {
            return redirect()->action('Editor\ProductionResultController@create_result', $id)->withErrors('Cart must not be empty!');
        } else {
            $production_result = $this->ProductionResultRepository->store_result($id, $request->input('finished_at'), $request->input('uom_id'), $request->input('size'), $request->input('block'));
            return redirect()->action('Editor\ProductionResultController@summary', $id);
        }
    }

    public function edit_result($id)
    {
        $production_result = $this->ProductionResultRepository->get_one($id);
        $uom_list = Uom::pluck('name', 'id');
        $details = $production_result->production_result_detail;
        return view ('editor.production_result.result', compact('production_result', 'uom_list', 'details'));
    }

    public function update_result($id, ResultRequest $request)
    {
        if(!$request->input('size') || !$request->input('block'))
        {
            return redirect()->action('Editor\ProductionResultController@create_result', $id)->withErrors('Cart must not be empty!');
        } else {
            $production_result = $this->ProductionResultRepository->store_result($id, $request->input('finished_at'), $request->input('uom_id'), $request->input('size'), $request->input('block'));
            return redirect()->action('Editor\ProductionResultController@summary', $id);
        }
    }

    public function summary($id)
    {
        $production_result = $this->ProductionResultRepository->get_one($id);
        return view ('editor.production_result.summary', compact('production_result'));
    }   

    public function close_production_result($id)
    {
        $this->ProductionResultRepository->update_status($id, 'Done');
        return redirect()->action('Editor\ProductionResultController@summary', $id);
    }

    public function pdf($id)
    {
        $production_result = $this->ProductionResultRepository->get_one($id);
        //Generate the view
        $pdf = \PDF::loadView('editor.production_result.pdf', compact('production_result'));
        //Setup the pdf name & formatting
        $name = "Summary ".$production_result->id.".pdf";
        $pdf->setPaper('a4', 'portrait');
        //Return download link
        return $pdf->download($name);
    }
}
