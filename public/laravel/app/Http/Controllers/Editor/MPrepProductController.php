<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Requests\MPrepProductRequest;
use App\Http\Controllers\Controller;
use App\Repository\MPrepProductRepository;
use App\Repository\ProductionRepository;
use App\Repository\IngredientRepository;

class MPrepProductController extends Controller
{
	protected $MPrepProductRepository;
    protected $ProductionRepository;
    protected $IngredientRepository;

    public function __construct(MPrepProductRepository $master_prepared_product_repository, ProductionRepository $production_repository, IngredientRepository $ingredient_repository)
    {
        $this->MPrepProductRepository = $master_prepared_product_repository;
        $this->ProductionRepository = $production_repository;
        $this->IngredientRepository = $ingredient_repository;
    }

    public function index()
    {
    	$m_prep_products = $this->MPrepProductRepository->get_all();
    	return view ('editor.m_prep_product.index', compact('m_prep_products'));
    }

    public function create()
    {
    	return view ('editor.m_prep_product.form');
    }

    public function store(MPrepProductRequest $request)
    {
    	$this->MPrepProductRepository->store($request->input());
    	return redirect()->action('Editor\MPrepProductController@index');
    }

    public function edit($id)
    {
    	$m_prep_product = $this->MPrepProductRepository->get_one($id);
    	return view ('editor.m_prep_product.form', compact('m_prep_product'));
    }

    public function update($id, MPrepProductRequest $request)
    {
    	$this->MPrepProductRepository->update($id, $request->input());
    	return redirect()->action('Editor\MPrepProductController@index');
    }

    public function delete($id)
    {
    	$this->MPrepProductRepository->delete($id);
    	return redirect()->action('Editor\MPrepProductController@index');
    }

    public function edit_material($id)
    {
        $productions = $this->ProductionRepository->get_all();
        $m_prep_product = $this->MPrepProductRepository->get_one($id);
        return view ('editor.m_prep_product.material', compact('productions', 'm_prep_product'));
    }

    public function update_material($id, Request $request)
    {
        $this->MPrepProductRepository->save_material($id, $request->input('material'));
        return redirect()->action('Editor\MPrepProductController@index');
    }

    public function edit_ingredient($id)
    {
        $ingredients = $this->IngredientRepository->get_all();
        $m_prep_product = $this->MPrepProductRepository->get_one($id);
        return view ('editor.m_prep_product.ingredient', compact('ingredients', 'm_prep_product'));
    }

    public function update_ingredient($id, Request $request)
    {
        $this->MPrepProductRepository->save_ingredient($id, $request->input('ingredient'));
        return redirect()->action('Editor\MPrepProductController@index');
    }
}
