<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Illuminate\Http\Request;
use App\Http\Requests\TypeItemRequest;
use App\Http\Controllers\Controller;
use App\Model\TypeItem;

class TypeItemController extends Controller
{
    public function index()
    {
    	$type_items = TypeItem::all();
    	return view ('editor.type_item.index', compact('type_items'));
    }

    public function create()
    {
    	$type_item_list = TypeItem::all()->pluck('name', 'id');
        //$type_item_list->all();
        //dd($type_item_list);
        return view ('editor.type_item.form', ['type_item_list' => $type_item_list]);

    }

    public function store(TypeItemRequest $request)
    {
    	$type_item = new TypeItem;
        $type_item->parent = $request->input('parent');
    	$type_item->name = $request->input('name');
    	$type_item->description = $request->input('description');
        $type_item->created_by = Auth::id();
    	$type_item->save();

    	return redirect()->action('Editor\TypeItemController@index');
    }

    public function edit($id)
    {
    	$type_item = TypeItem::Find($id);
        $type_item_list = TypeItem::pluck('name', 'id');
    	return view ('editor.type_item.form', compact('type_item', 'type_item_list'));
    }

    public function update($id, TypeItemRequest $request)
    {
    	$type_item = TypeItem::Find($id);
		$type_item->parent = $request->input('parent');
        $type_item->name = $request->input('name');
    	$type_item->description = $request->input('description');
        $type_item->updated_by = Auth::id();
    	$type_item->save();

    	return redirect()->action('Editor\TypeItemController@index');
    }

    public function delete($id)
    {
    	TypeItem::Find($id)->delete();
    	return redirect()->action('Editor\TypeItemController@index');
    }
}
