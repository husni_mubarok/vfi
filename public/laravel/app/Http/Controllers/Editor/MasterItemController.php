<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Illuminate\Http\Request;
use App\Http\Requests\MasterItemRequest;
use App\Http\Controllers\Controller;
use App\Model\MasterItem;
use App\Model\CategoryItem;
use App\Model\TypeItem;

class MasterItemController extends Controller
{
    public function index()
    {
    	$master_items = MasterItem::all();
    	return view ('editor.master_item.index', compact('master_items'));
    }

    public function create()
    {
    	$category_item_list = CategoryItem::pluck('name', 'id');
        $type_item_list = TypeItem::pluck('name', 'id');

        return view ('editor.master_item.form', compact('category_item_list', 'type_item_list'));
    }

    public function store(MasterItemRequest $request)
    {
    	$master_item = new MasterItem;
    	$master_item->name = $request->input('name');
        $master_item->id_category_item = $request->input('id_category_item');
        $master_item->id_type_item = $request->input('id_type_item');
    	$master_item->description = $request->input('description');
        $master_item->created_by = Auth::id();
    	$master_item->save();

    	return redirect()->action('Editor\MasterItemController@index');
    }

    public function edit($id)
    {

        $category_item_list = CategoryItem::pluck('name', 'id');
        $type_item_list = TypeItem::pluck('name', 'id');

    	$master_item = MasterItem::Find($id);
    	return view ('editor.master_item.form', compact('master_item', 'category_item_list', 'type_item_list'));
    }

    public function update($id, MasterItemRequest $request)
    {
    	$master_item = MasterItem::Find($id);
		$master_item->name = $request->input('name');
        $master_item->id_category_item = $request->input('id_category_item');
        $master_item->id_type_item = $request->input('id_type_item');
    	$master_item->description = $request->input('description');
        $master_item->updated_by = Auth::id();
    	$master_item->save();

    	return redirect()->action('Editor\MasterItemController@index');
    }

    public function delete($id)
    {
    	MasterItem::Find($id)->delete();
    	return redirect()->action('Editor\MasterItemController@index');
    }
}
