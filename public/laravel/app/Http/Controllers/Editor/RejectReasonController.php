<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Requests\RejectReasonRequest;
use App\Http\Controllers\Controller;
use App\Repository\RejectReasonRepository;

class RejectReasonController extends Controller
{
    protected $RejectReasonRepository;

    public function __construct(RejectReasonRepository $reject_reason_repository)
    {
        $this->RejectReasonRepository = $reject_reason_repository;
    }

    public function index()
    {
    	$reject_reasons = $this->RejectReasonRepository->get_all();
    	return view ('editor.reject_reason.index', compact('reject_reasons'));
    }

    public function create()
    {
    	return view ('editor.reject_reason.form');
    }

    public function store(RejectReasonRequest $request)
    {
    	$reject_reason = $this->RejectReasonRepository->store($request->input());
    	return redirect()->action('Editor\RejectReasonController@index');
    }

    public function edit($id)
    {
    	$reject_reason = $this->RejectReasonRepository->get_one($id);
    	return view ('editor.reject_reason.form', compact('fish'));
    }

    public function update($id, RejectReasonRequest $request)
    {
    	$reject_reason = $this->RejectReasonRepository->update($id, $request->input());
    	return redirect()->action('Editor\RejectReasonController@index');
    }

    public function delete($id)
    {
    	$this->FishRepository->delete($id);
    	return redirect()->action('Editor\RejectReasonController@index');
    }
}
