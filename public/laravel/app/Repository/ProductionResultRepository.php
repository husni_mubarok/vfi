<?php

namespace App\Repository;
use DB;
use Carbon\Carbon;
use App\Model\ProductionResult;
use App\Model\ProductionResultVariable;
use App\Model\ProductionResultSize;
use App\Model\ProductionResultAddition;
use App\Model\ProductionResultReject;
use App\Model\ProductionResultDetail;

class ProductionResultRepository
{
    public function get_all()
    {
        return ProductionResult::orderBy('id', 'desc')->get();
    }

    public function get_regular()
    {
        return ProductionResult::where('production_type', 'regular')->get();
    }

    public function get_sashimi()
    {
        return ProductionResult::where('production_type', 'sashimi')->get();
    }

    public function get_value_added()
    {
        return ProductionResult::where('production_type', 'value_added')->get();
    }

    public function get_today()
    {
        $date = Carbon::now();
        $data = ProductionResult::where('started_at', $date)->where('status', 'Pending')->orWhere('status', 'On Going')->where('production_type', 'regular')->get();
        return $data;
    }

    public function get_regular_paginate()
    {
        return ProductionResult::where('production_type', 'regular')->paginate(10);
    }

    public function get_all_paginate()
    {
        return ProductionResult::orderBy('id', 'desc')->paginate(10);
    }

    public function get_list()
    {
        return ProductionResult::all()->sortBy('id')->pluck('name', 'id');
    }

    public function get_one($id)
    {
        return ProductionResult::FindOrFail($id);
    }

    public function store($data)
    {
        $production_result = new ProductionResult;
        $production_result->purchase_id = $data['purchase_id'];
        $production_result->production_id = $data['production_id'];
        $production_result->weight = $data['weight'];
        $production_result->block_weight = $data['block_weight'];
        $production_result->started_at = $data['started_at'];
        $production_result->criteria = $data['criteria'];
        $production_result->mst_carton = $data['mst_carton'];
        $production_result->save();

        return $production_result;
    }

    public function update($id, $data)
    {
        $production_result = ProductionResult::FindOrFail($id);
        // $production_result->purchase_id = $data['purchase_id'];
        // $production_result->production_id = $data['production_id'];
        $production_result->weight = $data['weight'];
        $production_result->block_weight = $data['block_weight'];
        $production_result->started_at = $data['started_at'];
        $production_result->criteria = $data['criteria'];
        $production_result->mst_carton = $data['mst_carton'];
        $production_result->save();

        return $production_result;
    }

    public function delete($id)
    {
        ProductionResult::FindOrFail($id)->delete();
    }

    public function update_status($id, $status)
    {
        $production_result = ProductionResult::FindOrFail($id);
        $production_result->status = $status;
        if($status == 'Done')
        {
            $production_result->finished_at = Carbon::now();

            $total_waste = 0;
            $total_addition = 0;
            $total_ready = 0;
            $total_miss = 0;
            foreach($production_result->production_result_variable as $waste)
            {
              if($waste->variable->type == 'Reduction')
              {
                $total_waste += $waste->value;
              }
            }
            foreach($production_result->production_result_addition as $addition)
            {
              $total_addition += ($addition->block * $production_result->block_weight);
            }
            foreach($production_result->production_result_detail as $detail)
            {
              $total_ready += ($detail->block * $production_result->block_weight);
            }
            
            $production_result->miss_product = $production_result->weight - ($total_waste + $total_addition + $total_ready);
        }
        $production_result->save();
    }

    public function store_addition($id, $data)
    {
        ProductionResultAddition::where('production_result_id', $id)->delete();
        if(isset($data['size']))
        {
            foreach($data['size'] as $key => $data_input)
            {
                $addition = new ProductionResultAddition;
                $addition->production_result_id = $id;
                $addition->size = $data['size'][$key];
                $addition->block = $data['block'][$key];
                $addition->block_weight = $data['block_weight'][$key];
                $addition->reject_type = $data['type'][$key];
                $addition->reject_reason = $data['reason'][$key];
                $addition->save();
            }
        }
    }

    public function store_reject($id, $data)
    {
        ProductionResultReject::where('production_result_id', $id)->delete();
        if(isset($data['weight']))
        {
            foreach($data['weight'] as $key => $weight)
            {
                $reject = new ProductionResultReject;
                $reject->production_result_id = $id;
                $reject->size = $key;
                $reject->value = $data['weight'][$key];
                $reject->reject_reason_id = $data['reason'][$key];
                $reject->save();
            }
        }
    }

    public function store_result($id, $time, $uom_id, $data_size, $data_block)
    {
        ProductionResultDetail::where('production_result_id', $id)->delete();
        $production_result = ProductionResult::FindOrFail($id);
        $production_result->finished_at = $time;
        foreach($data_size as $key => $size)
        {
            $production_result_detail = new ProductionResultDetail;
            $production_result_detail->production_result_id = $id;
            $production_result_detail->size = $data_size[$key];
            $production_result_detail->block = $data_block[$key];
            $production_result_detail->uom_id = $uom_id;
            $production_result_detail->save();
        }

        $production_result->save();
        return $production_result;
    }
}