<?php

namespace App\Repository;
use DB;
use App\Model\Size;

class SizeRepository
{
    public function get_all()
    {
        return Size::all();
    }

    public function get_list()
    {
        return Size::all()->sortBy('id')->pluck('name', 'id');
    }

    public function get_one($id)
    {
        return Size::FindOrFail($id);
    }

    public function store($data)
    {
        $size = new Size;
        $size->min_value = $data['min_value'];
        $size->max_value = $data['max_value'];
        $size->note = $data['note'];
        
        $size->save();
        return $size;
    }

    public function update($id, $data)
    {
        $size = Size::FindOrFail($id);
        $size->min_value = $data['min_value'];
        $size->max_value = $data['max_value'];
        $size->note = $data['note'];
        
        $size->save();
        return $size;
    }

    public function delete($id)
    {
        Size::FindOrFail($id)->delete();
    }

    public function get_size($product_type_id, $size)
    {
        $size = DB::table('sizes')
                ->join('product_types_sizes', 'product_types_sizes.size_id', '=', 'sizes.id')
                ->join('product_types', 'product_types.id', '=', 'product_types_sizes.product_type_id')
                ->where('product_types_sizes.product_type_id', '=', $product_type_id)
                ->where('sizes.min_value', '<=', $size)
                ->where('sizes.max_value', '>=', $size)
                ->take(1)
                ->get();

        return $size;
    }
}