<?php

namespace App\Repository;
use DB;
use App\Model\ProductionExport;

class ProductionExportRepository
{
    public function get_all()
    {
        return ProductionExport::all();
    }

    public function get_list()
    {
        return ProductionExport::all()->sortBy('id')->pluck('doc_code', 'date', 'quantity', 'id');
    }

    public function get_one($id)
    {
        return ProductionExport::FindOrFail($id);
    }

    public function store($data)
    {
        $production_export = new ProductionExport;
        $production_export->doc_code = $data['doc_code'];
        $production_export->date = $data['date'];
        $production_export->quantity = $data['quantity'];
     
        
        $production_export->save();
        return $production_export;
    }

    public function update($id, $data)
    {
        $production_export = ProductionExport::FindOrFail($id);
        $production_export->doc_code = $data['doc_code'];
        $production_export->date = $data['date'];
        $production_export->quantity = $data['quantity'];
       
        
        $production_export->save();
        return $production_export;
    }

    public function delete($id)
    {
        ProductionExport::FindOrFail($id)->delete();
    }
 
}