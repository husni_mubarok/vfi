<?php

namespace App\Repository;
use DB;
use App\Model\Supplier;
use App\Model\SupplierProduct;

class SupplierRepository
{
    public function get_all()
    {
        return Supplier::all();
    }

    public function get_list()
    {
        return Supplier::all()->sortBy('id')->pluck('name', 'id');
    }

    public function get_one($id)
    {
        return Supplier::FindOrFail($id);
    }

    public function store($data)
    {
        $supplier = new Supplier;
        $supplier->code = $data['code'];
        $supplier->name = $data['name'];
        $supplier->address = $data['address'];
        $supplier->email = $data['email'];
        $supplier->phone_number = $data['phone_number'];
        $supplier->fax = $data['fax'];
        $supplier->website = $data['website'];
        $supplier->pic = $data['pic'];
        $supplier->bank_account = $data['bank_account'];
        $supplier->note = $data['note'];
        
        $supplier->save();
        return $supplier;
    }

    public function update($id, $data)
    {
        $supplier = Supplier::FindOrFail($id);
        $supplier->code = $data['code'];
        $supplier->name = $data['name'];
        $supplier->address = $data['address'];
        $supplier->email = $data['email'];
        $supplier->phone_number = $data['phone_number'];
        $supplier->fax = $data['fax'];
        $supplier->website = $data['website'];
        $supplier->pic = $data['pic'];
        $supplier->bank_account = $data['bank_account'];
        $supplier->note = $data['note'];
        
        $supplier->save();
        return $supplier;
    }

    public function delete($id)
    {
        Supplier::FindOrFail($id)->delete();
    }

    public function save_product($id, $data)
    {
        SupplierProduct::where('supplier_id', $id)->delete();
        if(!empty($data['product']))
        {
            foreach($data['product'] as $product)
            {
                $supplier_product = new SupplierProduct;
                $supplier_product->supplier_id = $id;
                $supplier_product->product_id = $product;
                $supplier_product->save();
            }
        }
    }

    public function get_product($id)
    {
        $supplier = Supplier::FindOrFail($id);
        $product_types = $supplier->supplier_product;
    }
}