<?php

namespace App\Repository;
use DB;
use App\Model\DocCode;

class DocCodeRepository
{
    public function get_all()
    {
        return DocCode::all();
    }

    public function get_list()
    {
        return DocCode::all()->sortBy('id')->pluck('name', 'id');
    }

    public function get_one($id)
    {
        return DocCode::FindOrFail($id);
    }

    public function reset_count($id)
    {
        $doc_code = DocCode::FindOrFail($id);
        $doc_code->last_num = 1;
        $doc_code->save();
    }

    public function store($data)
    {
        $doc_code = new DocCode;
        $doc_code->name = $data['name'];
        $doc_code->save();

        return $doc_code;
    }

    public function update($id, $data)
    {
        $doc_code = DocCode::FindOrFail($id);
        $doc_code->name = $data['name'];
        $doc_code->save();

        return $doc_code;
    }

    public function delete($id)
    {
        DocCode::FindOrFail($id)->delete();
    }
}