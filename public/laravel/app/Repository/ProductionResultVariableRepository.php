<?php

namespace App\Repository;
use DB;
use App\Model\ProductionResultVariable;

class ProductionResultVariableRepository
{
    public function get_all()
    {
        return ProductionResultVariable::all();
    }

    public function get_list()
    {
        return ProductionResultVariable::all()->sortBy('id')->pluck('name', 'id');
    }

    public function get_one($id)
    {
        return ProductionResultVariable::FindOrFail($id);
    }

    public function get_latest($id)
    {
        $price = ProductionResultVariable::where('purchase_id', '<', $id)->orderBy('id')->get();
        return $price;
    }

    public function store($id, $data)
    {
        ProductionResultVariable::where('production_result_id', $id)->delete();
        if($data)
        {
            foreach($data as $key => $value)
            {
                $production_result_variable = new ProductionResultVariable;
                $production_result_variable->production_result_id = $id;
                $production_result_variable->name = $key;
                $production_result_variable->value = $value;

                $production_result_variable->save();
            }
        }
    }

    public function get_price($purchase_id, $size_id)
    {
        return ProductionResultVariable::where('purchase_id', $purchase_id)->where('size_id', $size_id)->first();
    }
}