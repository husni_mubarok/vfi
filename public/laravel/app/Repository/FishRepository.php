<?php

namespace App\Repository;
use DB;
use App\Model\Fish;
use App\Model\ProductFish;

class FishRepository
{
    public function get_all()
    {
        return Fish::all();
    }

    public function get_list()
    {
        return Fish::all()->sortBy('id')->pluck('name', 'id');
    }

    public function get_one($id)
    {
        return Fish::FindOrFail($id);
    }

    public function store($data)
    {
        $fish = new Fish;
        $fish->name = $data['name'];
        $fish->save();

        return $fish;
    }

    public function update($id, $data)
    {
        $fish = Fish::FindOrFail($id);
        $fish->name = $data['name'];
        $fish->save();

        return $fish;
    }

    public function delete($id)
    {
        Fish::FindOrFail($id)->delete();
    }

    public function save_product($id, $data)
    {
        ProductFish::where('fish_id', $id)->delete();

        foreach($data as $product)
        {
            $product_fish = new ProductFish;
            $product_fish->fish_id = $id;
            $product_fish->product_id = $product;
            $product_fish->save();
        }
    }
}