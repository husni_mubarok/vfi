<?php

namespace App\Repository;
use DB;
use App\Model\PurchaseDetail;

class PurchaseDetailRepository
{
    public function get_all()
    {
        return PurchaseDetail::all();
    }

    public function get_list()
    {
        return PurchaseDetail::all()->sortBy('id')->pluck('name', 'id');
    }

    public function get_one($id)
    {
        return PurchaseDetail::FindOrFail($id);
    }

    public function store($id, $data_size, $data_price, $data_amount, $data_trans_code)
    {
        PurchaseDetail::Where('purchase_id', $id)->delete();
        for($i = 0; $i < count($data_size); $i++)
        {
            $purchase_detail = new PurchaseDetail;
            $purchase_detail->purchase_id = $id;
            $purchase_detail->price = $data_price[$i];
            $purchase_detail->size = $data_size[$i];
            $purchase_detail->amount = $data_amount[$i];
            $purchase_detail->trans_code = $data_trans_code.$data_size[$i];

            $purchase_detail->save();
        }
    }
}