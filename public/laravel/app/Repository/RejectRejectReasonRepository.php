<?php

namespace App\Repository;
use DB;
use App\Model\RejectRejectReason;

class RejectRejectReasonRepository
{
    public function get_all()
    {
        return RejectRejectReason::all();
    }

    public function get_list()
    {
        return RejectRejectReason::all()->sortBy('id')->pluck('name', 'id');
    }

    public function get_one($id)
    {
        return RejectRejectReason::FindOrFail($id);
    }

    public function store($id, $reasons)
    {
        RejectRejectReason::where('reject_id', $id)->delete();
        foreach($reasons as $reason)
        {
            $reject_reason = new RejectRejectReason;
            $reject_reason->reject_id = $id;
            $reject_reason->reject_reason_id = $reason;
            $reject_reason->save();
        }
    }

    public function update($id, $data)
    {
        $reject_reason = RejectRejectReason::FindOrFail($id);
        $reject_reason->name = $data['name'];
        $reject_reason->description = $data['description'];
        $reject_reason->save();

        return $reject_reason;
    }

    public function delete($id)
    {
        RejectRejectReason::FindOrFail($id)->delete();
    }
}