<?php

namespace App\Repository;
use DB;
use App\Model\RejectReason;

class RejectReasonRepository
{
    public function get_all()
    {
        return RejectReason::all();
    }

    public function get_list()
    {
        return RejectReason::all()->sortBy('id')->pluck('name', 'id');
    }

    public function get_one($id)
    {
        return RejectReason::FindOrFail($id);
    }

    public function store($data)
    {
        $reject_reason = new RejectReason;
        $reject_reason->name = $data['name'];
        $reject_reason->description = $data['description'];
        $reject_reason->save();

        return $reject_reason;
    }

    public function update($id, $data)
    {
        $reject_reason = RejectReason::FindOrFail($id);
        $reject_reason->name = $data['name'];
        $reject_reason->description = $data['description'];
        $reject_reason->save();

        return $reject_reason;
    }

    public function delete($id)
    {
        RejectReason::FindOrFail($id)->delete();
    }
}