<?php

namespace App\Repository;
use DB;
use App\Model\MPrepProduct;
use App\Model\Ingredient;
use App\Model\MPrepProductIngredient;
use App\Model\ProductionMPrepProduct;

class MPrepProductRepository
{
    public function get_all()
    {
        return MPrepProduct::all();
    }

    public function get_list()
    {
        return MPrepProduct::all()->sortBy('id')->pluck('name', 'id');
    }

    public function get_one($id)
    {
        return MPrepProduct::FindOrFail($id);
    }

    public function store($data)
    {
        $m_prep_product = new MPrepProduct;
        $m_prep_product->name = $data['name'];
        $m_prep_product->description = $data['description'];
        $m_prep_product->uom = $data['uom'];
        
        $m_prep_product->save();
        return $m_prep_product;
    }

    public function update($id, $data)
    {
        $m_prep_product = MPrepProduct::FindOrFail($id);
        $m_prep_product->name = $data['name'];
        $m_prep_product->description = $data['description'];
        $m_prep_product->uom = $data['uom'];
        
        $m_prep_product->save();
        return $m_prep_product;
    }

    public function delete($id)
    {
        MPrepProduct::FindOrFail($id)->delete();
    }

    public function save_material($id, $data)
    {
        ProductionMPrepProduct::where('m_prep_product_id', $id)->delete();

        foreach($data as $material)
        {
            $production_m_prep_product = new ProductionMPrepProduct;
            $production_m_prep_product->m_prep_product_id = $id;
            $production_m_prep_product->production_id = $material;
            $production_m_prep_product->save();
        }
    }

    public function save_ingredient($id, $data)
    {
        MPrepProductIngredient::where('m_prep_product_id', $id)->delete();

        foreach($data as $ingredient)
        {
            $m_prep_product_ingredient = new MPrepProductIngredient;
            $m_prep_product_ingredient->m_prep_product_id = $id;
            $m_prep_product_ingredient->ingredient_id = $ingredient;
            $m_prep_product_ingredient->save();
        }
    }
}