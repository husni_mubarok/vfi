<?php

namespace App\Repository;
use DB;
use App\Model\Price;

class PriceRepository
{
    public function get_all()
    {
        return Price::all();
    }

    public function get_list()
    {
        return Price::all()->sortBy('id')->pluck('name', 'id');
    }

    public function get_one($id)
    {
        return Price::FindOrFail($id);
    }

    public function get_latest($id)
    {
        $price = Price::where('purchase_id', '<', $id)->orderBy('id')->get();
        dd($price);
        return $price;
    }

    public function get_previous($id)
    {
        $price = Price::where('purchase_id', '=', $id)->orderBy('id')->get();
        return $price;
    }

    public function store($id, $data)
    {
        foreach($data as $key => $value)
        {
            Price::where('size_id', $key)->where('purchase_id', $id)->delete();

            $price = new Price;
            $price->value = $value;
            $price->size_id = $key;
            $price->purchase_id = $id;

            $price->save();
        }
    }

    public function get_price($purchase_id, $size_id, $size)
    {
        $current_price = Price::where('purchase_id', $purchase_id)->where('size_id', $size_id)->first();
        $next_price = Price::where('purchase_id', $purchase_id)->where('size_id', $size_id + 1)->first();
        if($next_price == null || $next_price->value == 0)
        {
            $previous_price = Price::where('purchase_id', $purchase_id)->where('size_id', $size_id - 1)->first();
            $previous_price = $previous_price->value;
            $range = ($current_price->size->max_value - $current_price->size->min_value) + 1;
            $n = ($current_price->size->min_value - $size);
            $price = $current_price->value + ((($previous_price - $current_price->value) / $range) * $n);
            $price = round($price, 2);
            return $price;
        } else {
            $next_price = $next_price->value;
            $range = ($current_price->size->max_value - $current_price->size->min_value) + 1;
            $n = ($current_price->size->min_value - $size);
            $price = $current_price->value + ((($current_price->value - $next_price) / $range) * $n);
            $price = round($price, 2);
            return $price;
        }
    }
}