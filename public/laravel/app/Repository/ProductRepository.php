<?php

namespace App\Repository;
use DB;
use App\Model\Product;

class ProductRepository
{
    public function get_all()
    {
        return Product::all();
    }

    public function get_list()
    {
        return Product::all()->sortBy('id')->pluck('name', 'id');
    }

    public function get_one($id)
    {
        return Product::FindOrFail($id);
    }

    public function get_child($id)
    {
        return Product::where('product_type_id', $id)->get();
    }

    public function store($data)
    {
        $product = new Product;
        $product->product_type_id = $data['product_type_id'];
        $product->code = $data['code'];
        $product->name = $data['name'];
        $product->description = $data['description'];

        $product->save();
        return $product;
    }

    public function update($id, $data)
    {
        $product = Product::FindOrFail($id);
        $product->code = $data['code'];
        $product->name = $data['name'];
        $product->description = $data['description'];
        
        $product->save();
        return $product;
    }

    public function delete($id)
    {
        Product::FindOrFail($id)->delete();
    }
}