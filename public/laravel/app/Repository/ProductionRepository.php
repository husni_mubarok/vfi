<?php

namespace App\Repository;
use DB;
use App\Model\Production;
use App\Model\ProductionVariable;
use App\Model\ProductionSize;

class ProductionRepository
{
    public function get_all()
    {
        return Production::all();
    }

    public function get_list()
    {
        return Production::all()->where('type', 'regular')->sortBy('id')->pluck('name', 'id');
    }

    public function get_one($id)
    {
        return Production::FindOrFail($id);
    }

    public function store($data)
    {
        $production = new Production;
        $production->fish_id = $data['fish_id'];
        $production->name = $data['name'];
        $production->description = $data['description'];
        $production->type = $data['type'];
        
        $production->save();
        return $production;
    }

    public function update($id, $data)
    {
        $production = Production::FindOrFail($id);
        $production->fish_id = $data['fish_id'];
        $production->name = $data['name'];
        $production->description = $data['description'];
        $production->type = $data['type'];
        
        $production->save();
        return $production;
    }

    public function delete($id)
    {
        Production::FindOrFail($id)->delete();
    }

    public function save_variable($id, $data)
    {
        ProductionVariable::where('production_id', $id)->delete();
        if($data)
        {
            foreach($data as $variable)
            {
                $production_variable = new ProductionVariable;
                $production_variable->production_id = $id;
                $production_variable->variable_id = $variable;
                $production_variable->save();
            }
        }
    }

    public function save_size($id, $data)
    {
        ProductionSize::where('production_id', $id)->delete();
        if($data)
        {
            foreach($data as $size)
            {
                $production_size = new ProductionSize;
                $production_size->production_id = $id;
                $production_size->size_id = $size;
                $production_size->save();
            }
        }
    }
}