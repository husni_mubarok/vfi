<?php

namespace App\Repository;
use DB;
use App\Model\Ingredient;

class IngredientRepository
{
    public function get_all()
    {
        return Ingredient::all();
    }

    public function get_list()
    {
        return Ingredient::all()->sortBy('id')->pluck('name', 'id');
    }

    public function get_one($id)
    {
        return Ingredient::FindOrFail($id);
    }

    public function store($data)
    {
        $ingredient = new Ingredient;
        $ingredient->name = $data['name'];
        $ingredient->description = $data['description'];
        $ingredient->uom = $data['uom'];
        $ingredient->save();

        return $ingredient;
    }

    public function update($id, $data)
    {
        $ingredient = Ingredient::FindOrFail($id);
        $ingredient->name = $data['name'];
        $ingredient->description = $data['description'];
        $ingredient->uom = $data['uom'];
        $ingredient->save();

        return $ingredient;
    }

    public function delete($id)
    {
        Ingredient::FindOrFail($id)->delete();
    }
}