<?php

namespace App\Repository;
use DB;
use App\Model\Variable;

class VariableRepository
{
    public function get_all()
    {
        return Variable::all();
    }

    public function get_reduction()
    {
        return Variable::where('type', 'Reduction')->get();
    }

    public function get_list()
    {
        return Variable::all()->sortBy('id')->pluck('name', 'id');
    }

    public function get_one($id)
    {
        return Variable::FindOrFail($id);
    }

    public function store($data)
    {
        $variable = new Variable;
        $variable->name = $data['name'];
        $variable->uom = $data['uom'];
        $variable->type = $data['type'];
        $variable->save();
    }

    public function update($id, $data)
    {
        $variable = Variable::FindOrFail($id);
        $variable->name = $data['name'];
        $variable->uom = $data['uom'];
        $variable->type = $data['type'];
        $variable->save();
    }

    public function delete($id)
    {
        Variable::FindOrFail($id)->delete();
    }
}