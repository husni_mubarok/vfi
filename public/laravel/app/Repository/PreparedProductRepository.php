<?php

namespace App\Repository;
use DB;
use App\Model\PreparedProduct;
use App\Model\ProductionResult;
use App\Model\ProductionResultPreparedProduct;
use App\Model\PreparedProductDetail;
use App\Model\PrepProdDetailIngredient;

class PreparedProductRepository
{
    public function get_all()
    {
        return PreparedProduct::all();
    }

    public function get_list()
    {
        return PreparedProduct::all()->sortBy('id')->pluck('name', 'id');
    }

    public function get_one($id)
    {
        return PreparedProduct::FindOrFail($id);
    }

    public function get_today_detail($id)
    {
        return PreparedProductDetail::where('date', date("Y-m-d"))->where('prep_prod_id', $id)->first();
    }

    public function store($data)
    {
        $prepared_product = new PreparedProduct;
        $prepared_product->m_prep_product_id = $data['m_prep_product_id'];
        $prepared_product->production_id = $data['production_id'];
        $prepared_product->avg_size = $data['avg_size'];
        $prepared_product->daily_target = $data['daily_target'];
        $prepared_product->work_days = $data['work_days'];
        $prepared_product->start_date = $data['start_date'];
        $prepared_product->finish_date = $data['finish_date'];
        $prepared_product->flavor = $data['flavor'];
        $prepared_product->save();
    }

    public function update($id, $data)
    {
        $prepared_product = PreparedProduct::FindOrFail($id);
        $prepared_product->m_prep_product_id = $data['m_prep_product_id'];
        $prepared_product->production_id = $data['production_id'];
        $prepared_product->size_id = $data['size_id'];
        $prepared_product->estimated_result = $data['estimated_result'];
        $prepared_product->create_date = $data['create_date'];
        $prepared_product->expire_date = $data['expire_date'];
        
        $prepared_product->save();
        return $prepared_product;
    }

    public function delete($id)
    {
        PreparedProduct::FindOrFail($id)->delete();
    }

    public function save_material($id, $production_result_id)
    {
        ProductionResultPreparedProduct::where('prepared_product_id', $id)->delete();
        $production_result_prepared_product = new ProductionResultPreparedProduct;
        $production_result_prepared_product->production_rslt_id = $production_result_id;
        $production_result_prepared_product->prepared_product_id = $id;

        $production_result = ProductionResult::Find($production_result_id);
        $production_result->available = 0;
        $production_result->save();

        $production_result_prepared_product->save();
        // foreach($data as $key => $ingredient)
        // {
        //     $prepared_product_ingredient = new PreparedProductIngredient;
        //     $prepared_product_ingredient->prepared_product_id = $id;
        //     $prepared_product_ingredient->ingredient_id = $key;
        //     $prepared_product_ingredient->value = $ingredient;
        //     $prepared_product_ingredient->save();
        // }   
    }

    // public function save_ingredient($id, $data)
    // {
    //     PreparedProductIngredient::where('prepared_product_id', $id)->delete();

    //     foreach($data as $key => $ingredient)
    //     {
    //         $prepared_product_ingredient = new PreparedProductIngredient;
    //         $prepared_product_ingredient->prepared_product_id = $id;
    //         $prepared_product_ingredient->ingredient_id = $key;
    //         $prepared_product_ingredient->value = $ingredient;
    //         $prepared_product_ingredient->save();
    //     }
    // }

    public function store_daily($id, $data)
    {
        $prepared_product_detail = new PreparedProductDetail;
        $prepared_product_detail->prep_prod_id = $id;
        $prepared_product_detail->date = $data['date'];
        $prepared_product_detail->processed_amount = $data['processed_amount'];
        $prepared_product_detail->save();

        return $prepared_product_detail->id;
    }

    public function update_daily($id, $data)
    {

    }

    public function save_ingredient($id, $data)
    {
        PrepProdDetailIngredient::where('prep_prod_dtl_id', $id)->delete();
        foreach($data as $key => $value)
        {
            $ingredient = new PrepProdDetailIngredient;
            $ingredient->prep_prod_dtl_id = $id;
            $ingredient->ingredient_id = $key;
            $ingredient->value = $value;
            $ingredient->save();
        }
    }
}