<?php

namespace App\Repository;
use DB;
use Auth;
use Carbon\Carbon;
use App\Model\Purchase;
use App\Model\PurchaseSupplier;
use App\Model\PurchaseProduct;
use App\Model\PurchaseReject;
use App\Model\DocCode;

class PurchaseRepository
{
    public function get_all()
    {
        return Purchase::orderBy('created_at', 'desc')->get();
    }

    public function get_list()
    {
        $purchases = Purchase::where('status', 'Approved')->get();
        $purchase_list = [];
        foreach($purchases as $purchase)
        {
            if(!$purchase->production_result)
            {
                $purchase_list[$purchase->id] = $purchase->description;
            }
        }

        return $purchase_list;
    }

    public function get_one($id)
    {
        return Purchase::FindOrFail($id);
    }

    public function get_by_doc_code($doc_code)
    {
        return Purchase::where('doc_code', $doc_code)->first();
    }

    public function get_latest($id)
    {
        $purchase = DB::table('purchases')
                    ->join('purchases_products', 'purchases_products.purchase_id', '=', 'purchases.id')
                    ->where('purchases_products.product_type_id', '=', $id)
                    ->orderBy('purchases.purchase_time', 'desc')
                    ->get();
        return $purchase;                
    }

    public function get_previous($id, $product_id)
    {
        $purchase = DB::table('purchases')
                    ->join('purchases_products', 'purchases_products.purchase_id', '=', 'purchases.id')
                    ->where('purchases_products.product_id', '=', $product_id)
                    ->where('purchases.id', '<', $id)
                    ->orderBy('purchases.id', 'desc')
                    ->first();

        return $purchase;
    }

    public function store($data)
    {
        $doc_code = DocCode::where('name', 'Purchase')->first();
        switch(date("m", strtotime($data['purchase_time']))) {
            case '1':
                $doc_code_month = 'I';
                break;
            case '2':
                $doc_code_month = 'II';
                break;
            case '3':
                $doc_code_month = 'III';
                break;
            case '4':
                $doc_code_month = 'IV';
                break;
            case '5':
                $doc_code_month = 'V';
                break;
            case '6':
                $doc_code_month = 'VI';
                break;
            case '7':
                $doc_code_month = 'VII';
                break;
            case '8':
                $doc_code_month = 'VIII';
                break;
            case '9':
                $doc_code_month = 'IX';
                break;
            case '10':
                $doc_code_month = 'X';
                break;
            case '11':
                $doc_code_month = 'XI';
                break;
            case '12':
                $doc_code_month = 'XII';
                break;
        }
        $doc_code_year = date("y", strtotime($data['purchase_time']));

        $purchase = new Purchase;

        $purchase->doc_code = $doc_code->last_num.'/FISH/'.$doc_code->code.'/'.$doc_code_month.'/'.$doc_code_year;
        $purchase->trans_code = $data['product_code'].$data['condition'].date("dmy", strtotime($data['purchase_time'])).$data['supplier_code'];
        $purchase->description = $data['description'];
        // $purchase->size = $data['size'];
        $purchase->quantity = $data['quantity'];
        $purchase->estimated_price = $data['estimated_price'];
        $purchase->purchase_time = $data['purchase_time'];
        // $purchase->division = $data['division'];
        // $purchase->reference = $data['reference'];
        $purchase->project = $data['project'];
        $purchase->currency = $data['currency'];
        $purchase->ppn = $data['ppn'];
        $purchase->discount = $data['discount'];
        $purchase->down_payment = $data['down_payment'];
        $purchase->term_condition = $data['term_condition'];
        $purchase->term_payment = $data['term_payment'];
        $purchase->notes = $data['notes'];

        $purchase->save();

        $doc_code->last_num += 1;
        $doc_code->save();

        $purchase_supplier = new PurchaseSupplier;
        $purchase_supplier->purchase_id = $purchase->id;
        $purchase_supplier->supplier_id = $data['supplier_id'];

        $purchase_supplier->save();

        $purchase_product = new PurchaseProduct;
        $purchase_product->purchase_id = $purchase->id;
        $purchase_product->product_id = $data['product_id'];

        $purchase_product->save();
        return $purchase;
    }

    public function update($id, $data)
    {
        $purchase = Purchase::Find($id);
        $purchase->description = $data['description'];
        $purchase->quantity = $data['quantity'];
        $purchase->estimated_price = $data['estimated_price'];
        $purchase->purchase_time = $data['purchase_time'];
        // $purchase->division = $data['division'];
        // $purchase->reference = $data['reference'];
        $purchase->project = $data['project'];
        $purchase->currency = $data['currency'];
        $purchase->ppn = $data['ppn'];
        $purchase->discount = $data['discount'];
        $purchase->down_payment = $data['down_payment'];
        $purchase->term_condition = $data['term_condition'];
        $purchase->term_payment = $data['term_payment'];
        $purchase->notes = $data['notes'];
        
        $purchase->save();

        return $purchase;
    }

    public function approve($id)
    {
        $purchase = Purchase::Find($id);
        if($purchase->status == 'Waiting COO') {
            $purchase->status = 'Actual Delivery';
        } elseif($purchase->status == 'Waiting QC') {
            $purchase->status = 'Approved';
        }
        $purchase->save();
    }

    public function reject($id, $data)
    {
        $purchase = Purchase::Find($id);
        $reject = new PurchaseReject;
        $reject->purchase_id = $id;
        $reject->reason = $data['reason'];
        $reject->user_id = Auth::user()->id;
        $reject->reject_step = $purchase->status;
        $reject->reject_date = Carbon::now();
        $reject->save();

        if($purchase->status == 'Waiting COO'){
            $purchase->status = 'PO RM';
        } elseif($purchase->status == 'Waiting QC'){
            $purchase->status = 'Actual Delivery';
        }
        $purchase->save();
        
        return $reject;
    }

    public function update_status($id, $status)
    {
        $purchase = Purchase::FindOrFail($id);
        $purchase->status = $status;
        $purchase->save();

        return $purchase;
    }

    public function delete($id)
    {
        $purchase = Purchase::Find($id);
        $purchase->delete();
    }
}