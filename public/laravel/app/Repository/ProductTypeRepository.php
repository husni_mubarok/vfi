<?php

namespace App\Repository;
use DB;
use App\Model\ProductType;
use App\Model\ProductTypeSize;

class ProductTypeRepository
{
    public function get_all()
    {
        return ProductType::all();
    }

    public function get_list()
    {
        return ProductType::all()->sortBy('id')->pluck('name', 'id');
    }

    public function get_one($id)
    {
        return ProductType::FindOrFail($id);
    }

    public function store($data)
    {
        $product_type = new ProductType;
        $product_type->name = $data['name'];
        $product_type->description = $data['description'];
        
        $product_type->save();
        return $product_type;
    }

    public function update($id, $data)
    {
        $product_type = ProductType::FindOrFail($id);
        $product_type->name = $data['name'];
        $product_type->description = $data['description'];
        
        $product_type->save();
        return $product_type;
    }

    public function delete($id)
    {
        ProductType::FindOrFail($id)->delete();
    }

    public function save_size($id, $data)
    {
        ProductTypeSize::where('product_type_id', $id)->delete();
        if(!empty($data['size']))
        {
            foreach($data['size'] as $size)
            {
                $product_type_size = new ProductTypeSize;
                $product_type_size->product_type_id = $id;
                $product_type_size->size_id = $size;
                $product_type_size->save();
            }
        }
    }
}