<?php

namespace App\Repository;
use DB;
use App\Model\Reject;
use App\Model\RejectRejectReason;

class RejectRepository
{
    public function get_all()
    {
        return Reject::all();
    }

    public function get_list()
    {
        return Reject::all()->sortBy('id')->pluck('name', 'id');
    }

    public function get_one($id)
    {
        return Reject::FindOrFail($id);
    }

    public function store($id, $data)
    {
        $reject = new Reject;
        $reject->production_result_id = $id;
        $reject->reject_product = $data['reject_product'];
        
        $reject->save();
        return $reject;
    }

    public function update($id, $data)
    {
        $reject = Reject::FindOrFail($id);
        $reject->production_result_id = $id;
        $reject->reject_product = $data['reject_product'];
        
        $reject->save();
        return $reject;
    }

    public function delete($id)
    {
        Reject::FindOrFail($id)->delete();
    }
}