<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('production_results');
        Schema::create('production_results', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('purchase_id')->unsigned();
            $table->foreign('purchase_id')->references('id')->on('purchases')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->integer('production_id')->unsigned();
            $table->foreign('production_id')->references('id')->on('productions')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->integer('cart_id')->unsigned();
            $table->foreign('cart_id')->references('id')->on('cart')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->enum('production_type', ['regular', 'sashimi', 'value_added']);
            $table->string('doc_code');
            $table->integer('amount')->default(0);
            $table->decimal('weight', 15, 3)->default(0);
            $table->boolean('status')->default(0);
            $table->string('criteria')->nullable();
            $table->decimal('miss_product',15,3)->default(0);
            $table->decimal('sample_kg',15,3)->default(0);
            $table->integer('sample_pack')->default(0);
            $table->decimal('production_result_kg',15,3)->default(0);
            $table->integer('production_result_pack')->default(0);
            $table->float('block_weight');
            $table->integer('mst_carton');
            $table->dateTime('started_at');
            $table->dateTime('finished_at')->nullable();
            $table->timestamps();
            $table->softDeletes();

            //Original schema
            // $table->increments('id');
            // $table->integer('raw_material_id')->unsigned();
            // $table->foreign('raw_material_id')->references('id')->on('raw_materials')
            //     ->onUpdate('cascade')->onDelete('cascade');
            // $table->integer('production_id')->unsigned();
            // $table->foreign('production_id')->references('id')->on('productions')
            //     ->onUpdate('cascade')->onDelete('cascade');
            // $table->string('criteria')->nullable();
            // $table->decimal('sample_kg',15,3)->default(0);
            // $table->decimal('sample_pack')->default(0);
            // $table->decimal('miss_product',15,3)->default(0);
            // $table->decimal('production_result_kg',15,3)->default(0);
            // $table->decimal('production_result_pack')->default(0);
            // $table->decimal('weight',15,3)->default(0);
            // $table->enum('status',['Pending', 'On Going', 'Done'])->default('Pending');
            // $table->dateTime('started_at');
            // $table->dateTime('finished_at')->nullable();
            // $table->timestamps();
            // $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('production_results');
    }
}
