<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRawMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('raw_materials');
        Schema::create('raw_materials', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->decimal('weight', 15, 3);
            $table->integer('amount');
            $table->string('criteria')->nullable();
            $table->string('notes')->nullable();
            $table->integer('fish_id')->unsigned();
            $table->foreign('fish_id')->references('id')->on('fishes')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->enum('status',['Pending', 'On Going', 'Done'])->default('Pending');
            $table->dateTime('arrived_at');
            $table->dateTime('finished_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('raw_materials');
    }
}
