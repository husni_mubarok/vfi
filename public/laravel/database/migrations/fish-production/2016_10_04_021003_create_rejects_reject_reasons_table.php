<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRejectsRejectReasonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('rejects_reject_reasons');
        Schema::create('rejects_reject_reasons', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reject_id')->unsigned();
            $table->foreign('reject_id')->references('id')->on('rejects')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->integer('reject_reason_id')->unsigned();
            $table->foreign('reject_reason_id')->references('id')->on('reject_reasons')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rejects_reject_reasons');
    }
}
