<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRejectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('rejects');
        Schema::create('rejects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('production_result_id')->unsigned();
            $table->foreign('production_result_id')->references('id')->on('production_results')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->decimal('reject_size', 15, 3)->default(0);
            $table->decimal('reject_raw', 15, 3)->default(0);
            $table->decimal('reject_product', 15, 3)->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rejects');
    }
}
