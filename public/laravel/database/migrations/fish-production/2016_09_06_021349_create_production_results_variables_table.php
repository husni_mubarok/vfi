<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionResultsVariablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('production_results_variables');
        Schema::create('production_results_variables', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('production_result_id')->unsigned();
            $table->foreign('production_result_id')->references('id')->on('production_results')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->string('name');
            $table->decimal('value', 15, 3)->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('production_results_variables');
    }
}
