<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionsMasterPrepProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('productions_m_prep_products');
        Schema::create('productions_m_prep_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('m_prep_product_id')->unsigned();
            $table->foreign('m_prep_product_id')->references('id')->on('master_prepared_products')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->integer('production_id')->unsigned();
            $table->foreign('production_id')->references('id')->on('productions')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('productions_m_prep_products');
    }
}
