<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionResultRejectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('production_result_rejects');
        Schema::create('production_result_rejects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('production_result_id')->unsigned();
            $table->foreign('production_result_id')->references('id')->on('production_results')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->integer('reject_reason_id')->unsigned();
            $table->foreign('reject_reason_id')->references('id')->on('reject_reasons')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->string('size');
            $table->decimal('value', 15, 3);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('production_result_rejects');
    }
}
