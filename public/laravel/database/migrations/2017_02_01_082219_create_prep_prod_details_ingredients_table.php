<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrepProdDetailsIngredientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('prep_prod_details_ingredients');
        Schema::create('prep_prod_details_ingredients', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('prep_prod_dtl_id')->unsigned();
            $table->foreign('prep_prod_dtl_id')->references('id')->on('prep_prod_details')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->integer('ingredient_id')->unsigned();
            $table->foreign('ingredient_id')->references('id')->on('ingredients')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->float('value', 15, 3);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('prep_prod_details_ingredients');
    }
}
