<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionResultsPreparedProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('production_results_prepared_products');
        Schema::create('production_results_prepared_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('production_rslt_id')->unsigned();
            $table->foreign('production_rslt_id')->references('id')->on('production_results')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->integer('prepared_product_id')->unsigned();
            $table->foreign('prepared_product_id')->references('id')->on('prepared_products')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('production_results_prepared_products');
    }
}
