<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSashimiResultTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('sashimi_result');
        Schema::create('sashimi_result', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sashimi_bom_id')->unsigned();
            $table->foreign('sashimi_bom_id')->references('id')->on('sashimi_bom')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->integer('tray');
            $table->integer('size');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sashimi_result');
    }
}
