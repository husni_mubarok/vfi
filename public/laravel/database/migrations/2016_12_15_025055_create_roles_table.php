<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('roles');
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_users')->unsigned();
            $table->foreign('id_users')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->integer('id_modules')->unsigned();
            $table->foreign('id_modules')->references('id')->on('modules')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->integer('id_actions')->unsigned();
            $table->foreign('id_actions')->references('id')->on('actions')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('roles');
    }
}
