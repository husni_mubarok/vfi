<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('purchases');
        Schema::create('purchases', function (Blueprint $table) {
            $table->increments('id');            
            $table->string('doc_code');
            $table->datetime('purchase_time');
            $table->string('description');
            $table->integer('estimated_price');
            // $table->string('division');
            // $table->string('reference');
            $table->string('project');
            $table->enum('currency', ['IDR', 'USD']);
            $table->enum('ppn', ['on', 'off']);
            $table->integer('discount')->default(0);
            $table->integer('down_payment')->default(0);
            $table->string('term_condition', 1000);
            $table->string('term_payment', 1000);
            $table->string('notes')->nullable();
            // $table->integer('size');
            $table->integer('quantity');
            // $table->boolean('status')->default(0);
            $table->enum('status', ['PO RM', 'Waiting COO', 'Actual Delivery', 'Waiting QC', 'Approved', 'Used'])->default('PO RM');
            $table->boolean('production_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('purchases');
    }
}
