<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreparedProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('prepared_products');
        Schema::create('prepared_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('m_prep_product_id')->unsigned();
            $table->foreign('m_prep_product_id')->references('id')->on('m_prep_products')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->integer('production_id')->unsigned();
            $table->foreign('production_id')->references('id')->on('productions')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->float('avg_size', 15, 3);
            $table->float('daily_target', 15, 3);
            $table->integer('work_days');
            $table->date('start_date');
            $table->date('finish_date');
            $table->string('flavor')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('prepared_products');
    }
}
