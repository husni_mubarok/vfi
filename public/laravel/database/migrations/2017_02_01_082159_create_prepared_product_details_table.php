<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreparedProductDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('prep_prod_details');
        Schema::create('prep_prod_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('prep_prod_id')->unsigned();
            $table->foreign('prep_prod_id')->references('id')->on('prepared_products')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->date('date');
            $table->float('processed_amount', 15, 3);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('prep_prod_details');
    }
}
