<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempStorageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('temp_storage');
        Schema::create('temp_storage', function (Blueprint $table) {
            $table->increments('id'); 
            $table->integer('source_id')->unsigned();
            $table->foreign('source_id')->references('id')->on('source_storage')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->integer('production_id')->unsigned();
            $table->foreign('production_id')->references('id')->on('production_results')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->integer('quantity');
            $table->string('unit');
            $table->string('x_foo')->nullable();
            $table->float('weight');
            $table->date('datetime');
            $table->boolean('status');
            $table->integer('status_storage_id')->unsigned();
            $table->foreign('status_storage_id')->references('id')->on('status_storage')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('temp_storage');
    }
}
