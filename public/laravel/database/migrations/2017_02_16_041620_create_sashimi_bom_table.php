<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSashimiBOMTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('sashimi_bom');
        Schema::create('sashimi_bom', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('storage_id')->unsigned();
            $table->foreign('storage_id')->references('id')->on('cartdet')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->integer('sashimi_id')->unsigned();
            $table->foreign('sashimi_id')->references('id')->on('sashimi')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->integer('qty');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sashimi_bom');
    }
}
