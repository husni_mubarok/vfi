<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionResultDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('production_result_details');
        Schema::create('production_result_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('production_result_id')->unsigned();
            $table->foreign('production_result_id')->references('id')->on('production_results')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->string('size');
            $table->integer('block');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('production_result_details');
    }
}
