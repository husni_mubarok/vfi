<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngredientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('ingredients');
        Schema::create('ingredients', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent')->default(0)->unsigned();
            $table->string('name');
            $table->string('description')->nullable();
            $table->enum('uom', ['gram', 'pcs', 'ml']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ingredients');
    }
}
