<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('export');
        Schema::create('export', function (Blueprint $table) {
            $table->increments('id'); 
            $table->string('doc_code');
            $table->date('date');
            $table->integer('quantity');
            $table->integer('storage_id')->unsigned();
            $table->foreign('storage_id')->references('id')->on('temp_storage')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('export');
    }
}
