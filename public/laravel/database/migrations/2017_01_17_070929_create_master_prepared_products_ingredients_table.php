<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterPreparedProductsIngredientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('m_prep_products_ingredients');
        Schema::create('m_prep_products_ingredients', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('m_prep_product_id')->unsigned();
            $table->foreign('m_prep_product_id')->references('id')->on('m_prep_products')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->integer('ingredient_id')->unsigned();
            $table->foreign('ingredient_id')->references('id')->on('ingredients')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('m_prep_products_ingredients');
    }
}
