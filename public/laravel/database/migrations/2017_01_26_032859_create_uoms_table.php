<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('uoms');
        Schema::create('uoms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('uom_category_id')->unsigned();
            $table->foreign('uom_category_id')->references('id')->on('uom_categories')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->integer('parent_id');
            $table->string('name');
            $table->float('value');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('uoms');
    }
}
