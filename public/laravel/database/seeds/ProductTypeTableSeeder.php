<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductTypeTableSeeder extends Seeder {

   public function run()
   {
       DB::table('product_types')->delete();

       DB::table('product_types')->insert([
           'name' => 'Prawn',
       ]);

       DB::table('product_types')->insert([
          'name' => 'Fish',
        ]);
    }
}
