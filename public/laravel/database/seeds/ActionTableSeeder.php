<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ActionTableSeeder extends Seeder {

   	public function run()
   	{
       	DB::table('actions')->delete();

       	DB::table('actions')->insert([
    	   	'name' => 'create',
           	'description' => 'create',
       	]);

		    DB::table('actions')->insert([
           	'name' => 'read',
           	'description' => 'read',
       	]);

		    DB::table('actions')->insert([
           	'name' => 'update',
           	'description' => 'update',
       	]);

       	DB::table('actions')->insert([
           	'name' => 'delete',
           	'description' => 'delete',
       	]);

        DB::table('actions')->insert([
            'name' => 'approve',
            'description' => 'approve',
        ]);

        DB::table('actions')->insert([
            'name' => 'reject',
            'description' => 'reject',
        ]);
    }
}
