<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductionTableSeeder extends Seeder {

   public function run()
   {
       DB::table('productions')->delete();

       DB::table('productions')->insert([
           'fish_id' => '1',
           'name' => 'Udang Vannamei Peeled and Deveined',
           'description' => 'Udang buang kulit dan buang usus',
       ]);

       DB::table('productions')->insert([
           'fish_id' => '4',
           'name' => 'Ikan Bawal Hitam Whole round IQF',
           'description' => 'Ikan utuh beku',
       ]);

       DB::table('productions')->insert([
           'fish_id' => '3',
           'name' => 'Ikan Tenggiri whole round IQF',
           'description' => 'Ikan Utuh beku',
       ]);

       DB::table('productions')->insert([
           'fish_id' => '5',
           'name' => 'Udang Black Tiger Headless',
           'description' => 'Udang tanpa kepala',
       ]);

       DB::table('productions')->insert([
           'fish_id' => '6',
           'name' => 'Udang Black Tiger Headless',
           'description' => 'Udang tanpa kepala',
       ]);
    }
}

