<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ModuleTableSeeder extends Seeder {

   	public function run()
   	{
       	DB::table('modules')->delete();

        //=================//
        //     DEFAULT     //
        //=================//
       	DB::table('modules')->insert([
  	   	    'name' => 'user',
           	'description' => 'user',
       	]);

		    DB::table('modules')->insert([
           	'name' => 'role',
           	'description' => 'role',
       	]);

		    DB::table('modules')->insert([
           	'name' => 'action',
           	'description' => 'action',
       	]);

       	DB::table('modules')->insert([
           	'name' => 'module',
           	'description' => 'module',
       	]);


        //=================//
        //     PURCHASE    //
        //=================//
        DB::table('modules')->insert([
            'name' => 'size',
            'description' => 'size',
        ]);

        DB::table('modules')->insert([
            'name' => 'product_type',
            'description' => 'product_type',
        ]);

        DB::table('modules')->insert([
            'name' => 'product',
            'description' => 'product',
        ]);

        DB::table('modules')->insert([
            'name' => 'supplier',
            'description' => 'supplier',
        ]);

        DB::table('modules')->insert([
            'name' => 'purchase',
            'description' => 'purchase',
        ]);

        DB::table('modules')->insert([
            'name' => 'price',
            'description' => 'price',
        ]);

        DB::table('modules')->insert([
            'name' => 'purchase_detail',
            'description' => 'purchase_detail',
        ]);

        //=================//
        //    PRODUCTION   //
        //=================//
        DB::table('modules')->insert([
            'name' => 'fish',
            'description' => 'fish',
        ]);

        DB::table('modules')->insert([
            'name' => 'variable',
            'description' => 'variable',
        ]);

        DB::table('modules')->insert([
            'name' => 'production',
            'description' => 'production',
        ]);

        DB::table('modules')->insert([
            'name' => 'reject_reason',
            'description' => 'reject_reason',
        ]);

        DB::table('modules')->insert([
            'name' => 'reject_type', 
            'description' => 'reject_type',
        ]);

        DB::table('modules')->insert([
            'name' => 'production_result',
            'description' => 'production_result',
        ]);

        DB::table('modules')->insert([
            'name' => 'storage', 
            'description' => 'storage',
        ]);

        DB::table('modules')->insert([
            'name' => 'export', 
            'description' => 'export',
        ]);

        DB::table('modules')->insert([
            'name' => 'end_product', 
            'description' => 'end_product',
        ]);
    }
}
