<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductTypeSizeTableSeeder extends Seeder {

   public function run()
   {
       DB::table('product_types_sizes')->delete();

       DB::table('product_types_sizes')->insert([
           'product_type_id' => '1',
           'size_id' => '1'
       ]);

       DB::table('product_types_sizes')->insert([
           'product_type_id' => '1',
           'size_id' => '2'
       ]);

       DB::table('product_types_sizes')->insert([
           'product_type_id' => '1',
           'size_id' => '3'
       ]);

       DB::table('product_types_sizes')->insert([
           'product_type_id' => '1',
           'size_id' => '4'
       ]);

       DB::table('product_types_sizes')->insert([
           'product_type_id' => '1',
           'size_id' => '5'
       ]);

       DB::table('product_types_sizes')->insert([
           'product_type_id' => '1',
           'size_id' => '6'
       ]);

       DB::table('product_types_sizes')->insert([
           'product_type_id' => '1',
           'size_id' => '7'
       ]);

       DB::table('product_types_sizes')->insert([
           'product_type_id' => '1',
           'size_id' => '8'
       ]);

       DB::table('product_types_sizes')->insert([
           'product_type_id' => '1',
           'size_id' => '9'
       ]);

       DB::table('product_types_sizes')->insert([
           'product_type_id' => '1',
           'size_id' => '10'
       ]);

       DB::table('product_types_sizes')->insert([
           'product_type_id' => '1',
           'size_id' => '11'
       ]);

       DB::table('product_types_sizes')->insert([
           'product_type_id' => '1',
           'size_id' => '12'
       ]);

       DB::table('product_types_sizes')->insert([
           'product_type_id' => '1',
           'size_id' => '13'
       ]);

       DB::table('product_types_sizes')->insert([
           'product_type_id' => '1',
           'size_id' => '14'
       ]);
    }
}
