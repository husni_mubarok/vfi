<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SupplierProductTableSeeder extends Seeder {

   public function run()
   {
       DB::table('suppliers_products')->delete();

       DB::table('suppliers_products')->insert([
           'supplier_id' => '1',
           'product_id' => '1'
       ]);
    }
}
