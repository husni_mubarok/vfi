<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductTableSeeder extends Seeder {

   public function run()
   {
       DB::table('products')->delete();

       DB::table('products')->insert([
           'product_type_id' => '1',
           'name' => 'Vannamei HO',
           'description' => 'Udang Vannamei dengan kepala',
       ]);
    }
}
