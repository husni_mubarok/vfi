<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SizeTableSeeder extends Seeder {

   public function run()
   {
       DB::table('sizes')->delete();

       DB::table('sizes')->insert([
           'min_value' => '1',
           'max_value' => '5',
       ]);

       DB::table('sizes')->insert([
           'min_value' => '6',
           'max_value' => '8',
       ]);

       DB::table('sizes')->insert([
           'min_value' => '9',
           'max_value' => '12',
       ]);

       DB::table('sizes')->insert([
           'min_value' => '13',
           'max_value' => '15',
       ]);

       DB::table('sizes')->insert([
           'min_value' => '16',
           'max_value' => '20',
       ]);

       DB::table('sizes')->insert([
           'min_value' => '21',
           'max_value' => '25',
       ]);

       DB::table('sizes')->insert([
           'min_value' => '26',
           'max_value' => '30',
       ]);

       DB::table('sizes')->insert([
           'min_value' => '31',
           'max_value' => '40',
       ]);

       DB::table('sizes')->insert([
           'min_value' => '41',
           'max_value' => '50',
       ]);

       DB::table('sizes')->insert([
           'min_value' => '51',
           'max_value' => '60',
       ]);

       DB::table('sizes')->insert([
           'min_value' => '61',
           'max_value' => '70',
       ]);

       DB::table('sizes')->insert([
           'min_value' => '71',
           'max_value' => '90',
       ]);

       DB::table('sizes')->insert([
           'min_value' => '91',
           'max_value' => '120',
       ]);

       DB::table('sizes')->insert([
           'min_value' => '121',
           'max_value' => '200',
       ]);
    }
}
