<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder {

   public function run()
   {
       DB::table('users')->delete();

       DB::table('users')->insert([
           'username' => 'superuser',
           'email' => 'super@user.com', 
           'first_name' => 'Superuser',
           'last_name' => '', 
           'id_privilege' => '1',
           'password' => bcrypt('super@123'),
       ]);
    }
}
