<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleTableSeeder extends Seeder {

   public function run()
   {
       DB::table('roles')->delete();

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '1',
           'id_actions' => '1',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '1',
           'id_actions' => '2',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '1',
           'id_actions' => '3',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '1',
           'id_actions' => '4',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '2',
           'id_actions' => '1',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '2',
           'id_actions' => '2',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '2',
           'id_actions' => '3',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '2',
           'id_actions' => '4',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '3',
           'id_actions' => '1',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '3',
           'id_actions' => '2',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '3',
           'id_actions' => '3',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '3',
           'id_actions' => '4',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '4',
           'id_actions' => '1',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '4',
           'id_actions' => '2',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '4',
           'id_actions' => '3',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '4',
           'id_actions' => '4',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '5',
           'id_actions' => '1',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '5',
           'id_actions' => '2',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '5',
           'id_actions' => '3',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '5',
           'id_actions' => '4',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '6',
           'id_actions' => '1',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '6',
           'id_actions' => '2',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '6',
           'id_actions' => '3',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '6',
           'id_actions' => '4',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '7',
           'id_actions' => '1',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '7',
           'id_actions' => '2',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '7',
           'id_actions' => '3',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '7',
           'id_actions' => '4',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '8',
           'id_actions' => '1',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '8',
           'id_actions' => '2',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '8',
           'id_actions' => '3',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '8',
           'id_actions' => '4',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '9',
           'id_actions' => '1',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '9',
           'id_actions' => '2',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '9',
           'id_actions' => '3',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '9',
           'id_actions' => '4',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '10',
           'id_actions' => '1',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '10',
           'id_actions' => '2',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '10',
           'id_actions' => '3',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '10',
           'id_actions' => '4',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '11',
           'id_actions' => '1',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '11',
           'id_actions' => '2',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '11',
           'id_actions' => '3',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '11',
           'id_actions' => '4',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '12',
           'id_actions' => '1',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '12',
           'id_actions' => '2',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '12',
           'id_actions' => '3',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '12',
           'id_actions' => '4',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '13',
           'id_actions' => '1',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '13',
           'id_actions' => '2',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '13',
           'id_actions' => '3',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '13',
           'id_actions' => '4',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '14',
           'id_actions' => '1',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '14',
           'id_actions' => '2',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '14',
           'id_actions' => '3',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '14',
           'id_actions' => '4',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '15',
           'id_actions' => '1',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '15',
           'id_actions' => '2',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '15',
           'id_actions' => '3',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '15',
           'id_actions' => '4',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '16',
           'id_actions' => '1',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '16',
           'id_actions' => '2',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '16',
           'id_actions' => '3',
       ]);

       DB::table('roles')->insert([
           'id_users' => '1',
           'id_modules' => '16',
           'id_actions' => '4',
       ]);
    }
}
