<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SupplierTableSeeder extends Seeder {

   public function run()
   {
       DB::table('suppliers')->delete();

       DB::table('suppliers')->insert([
           'name' => 'Hj. Ninik',
           'address' => 'Lampung',
           'email' => 'ninik@lampung.com',
           'phone_number' => '123456',
           'note' => ''
       ]);
    }
}
