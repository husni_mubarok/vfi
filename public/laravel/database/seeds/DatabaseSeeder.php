<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Default Seeder
        $this->call(PrivilegeTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(ActionTableSeeder::class);
        $this->call(ModuleTableSeeder::class);
        $this->call(RoleTableSeeder::class);

        //Purchasing Seeder
        $this->call(ProductTypeTableSeeder::class);
        $this->call(SizeTableSeeder::class);
        $this->call(ProductTypeSizeTableSeeder::class);
        $this->call(ProductTableSeeder::class);
        $this->call(SupplierTableSeeder::class);
        $this->call(SupplierProductTableSeeder::class);

        //Production Seeder
        $this->call(FishTableSeeder::class);
        $this->call(VariableTableSeeder::class);
        $this->call(ProductionTableSeeder::class);
        $this->call(ProductionVariableTableSeeder::class);
        $this->call(RejectReasonTableSeeder::class);
    }
}
