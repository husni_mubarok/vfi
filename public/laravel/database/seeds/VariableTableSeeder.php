<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VariableTableSeeder extends Seeder {

   public function run()
   {
       DB::table('variables')->delete();

       DB::table('variables')->insert([
           'name' => 'Kepala',
           'uom' => 'kg',
           'type' => 'Reduction',
       ]);

       DB::table('variables')->insert([
           'name' => 'Kulit',
           'uom' => 'kg',
           'type' => 'Reduction',
       ]);

       DB::table('variables')->insert([
           'name' => '-',
           'uom' => 'pcs',
           'type' => 'Addition',
       ]);
    }
}

