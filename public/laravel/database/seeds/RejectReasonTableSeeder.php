<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RejectReasonTableSeeder extends Seeder {

   public function run()
   {
       DB::table('reject_reasons')->delete();

       DB::table('reject_reasons')->insert([
           'name' => 'Human error',
           'description' => 'Human error',
       ]);

       DB::table('reject_reasons')->insert([
           'name' => 'Soft shell',
           'description' => 'Soft shell',
       ]);
    }
}

