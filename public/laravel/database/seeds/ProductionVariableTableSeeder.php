<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductionVariableTableSeeder extends Seeder {

   public function run()
   {
       DB::table('productions_variables')->delete();

       DB::table('productions_variables')->insert([
           'production_id' => '1',
       		'variable_id' => '1',
       ]);

       DB::table('productions_variables')->insert([
           'production_id' => '1',
       		'variable_id' => '2',
       ]);

       DB::table('productions_variables')->insert([
           'production_id' => '2',
       		'variable_id' => '3',
       ]);

       DB::table('productions_variables')->insert([
           'production_id' => '3',
       		'variable_id' => '3',
       ]);

       DB::table('productions_variables')->insert([
           'production_id' => '4',
       		'variable_id' => '3',
       ]);

       DB::table('productions_variables')->insert([
       	   'production_id' => '5',
   	   		'variable_id' => '3',
   	   ]);
    }
}

