<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FishTableSeeder extends Seeder {

   public function run()
   {
       DB::table('fishes')->delete();

       DB::table('fishes')->insert([
           'name' => 'Udang Vannamei',
       ]);

       DB::table('fishes')->insert([
           'name' => 'Ikan Bawal',
       ]);

       DB::table('fishes')->insert([
           'name' => 'Ikan Tenggiri',
       ]);

       DB::table('fishes')->insert([
           'name' => 'Ikan Bawal Hitam (Parastromateus niger)',
       ]);

       DB::table('fishes')->insert([
           'name' => 'Udang Black Tiger',
       ]);

       DB::table('fishes')->insert([
       	   'name' => 'Udang Black Tiger ( Raw material Headless)',
   	   ]);
    }
}

