<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PrivilegeTableSeeder extends Seeder {

   public function run()
   {
       DB::table('privileges')->delete();

       DB::table('privileges')->insert([
           'name' => 'Superuser',
           'description' => 'Superuser', 
       ]);

       DB::table('privileges')->insert([
           'name' => 'User',
           'description' => 'User', 
       ]);
    }
}
